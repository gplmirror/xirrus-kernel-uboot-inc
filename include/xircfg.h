//******************************************************************************
/** @file xircfg.h
 *
 * Structures and constants for the configuration request interface
 *
 * <I>Copyright (C) 2005 Xirrus. All Rights Reserved</I>
 *
 * This file contains constants and structures that are used between the
 * configuration module and client applications such as WMI, CLI, SNMPD etc.
 *
 * Only constants and structures defined for the cfg request interface should
 * be in here.
 *
 **/
//------------------------------------------------------------------------------
#ifndef _XIRCFG_H
#define _XIRCFG_H
#ifndef _XIRDHCP_H
#include "common.h"
#include "xirstats.h"
#include "xirver.h"
#include "xiriap.h"
#include "xirmac.h"
#include "xirlldp.h"
#include "../license/xirkey.h"
#include <time.h>
#include <float.h>
#include <limits.h>

//****************************************************************
// Arch dependent definitions
//----------------------------------------------------------------
#define SW_PREFIX   "XS"
#define HW_PREFIX   "XR"
#define XD_PREFIX   "XD"
#define XH_PREFIX   "XH"
#define XA_PREFIX   "XA"
#define AV_PREFIX   "WA"

//****************************************************************
// Type definitions
//----------------------------------------------------------------
#define CFG_TRUE                        1
#define CFG_FALSE                       0

#define HTON                            0
#define NTOH                            1

#define RS_AID                          0
#define RS_REQ                          1
#define RS_MOC                          2

//****************************************************************
// Client module identifiers
//----------------------------------------------------------------
#define MOD_NONE                        0
#define MOD_WMI                         1   // Web interface
#define MOD_CLI                         2   // Command line
#define MOD_DHCPCD                      3   // DHCP client daemon
#define MOD_INTERNAL                    4   // Internal cfg calls
#define MOD_BOOT                        5   // Boot time cfg calls
#define MOD_SNMPD                       6   // SNMP daemon
#define MOD_HEALTH_MON                  7   // Health monitor
#define MOD_OOPME                       8   // OOPME
#define MOD_HOSTAPD                     9   // Hostapd
#define MOD_MONITOR                    10   // Monitor
#define MOD_CLUSTER                    11   // Cluster of remote arrays
#define MOD_CONFIG                     12   // Internal cfg calls
#define MOD_RCFGD                      13   // Rcfgd login call
#define MOD_WPRD                       14   // Wprd
#define MOD_DOOR_MON                   15   // Door monitor
#define MOD_INT_THREAD                 16   // Internal calls made by a non-main thread
#define MOD_WEB_PROXY                  17   // Web Proxy
#define MOD_CLOUD                      18   // Cloud
#define MOD_LLDPD                      19   // LLDPD
#define MOD_PROXY_MGMT                 20   // Management Traffic Proxy Client
#define MOD_SHOW_ERR_NUM           0x8000   // Show error number flag

//****************************************************************
// Command types
//----------------------------------------------------------------
#define CMD_READ                        0   // Read a single object (OID)
#define CMD_WRITE                       1   // Write a single object (OID)
#define CMD_CONNECT                     2   // Initiate a connection
#define CMD_CLOSE                       3   // Close a connection
#define CMD_ADD                         4   // Add an entry (OID) to a list
#define CMD_DELETE                      5   // Delete an entry (OID) from a list
#define CMD_READ_STRUCT                 6   // Read a structure (SID)
#define CMD_EXECUTE                     7   // Execute a command (EID)
#define CMD_ENABLE                      8   // Enable an object
#define CMD_INIT                       10   // Initialize OOPME
#define CMD_START                      11   // Start OOPME
#define CMD_READ_STRUCT_EXT            12   // Read a structure (SID) with array ID             (deprecated, here for backward compatibility)
#define CMD_READ_STRUCT_XML            13   // Read a structure (SID) in XML                    (deprecated, here for backward compatibility)
#define CMD_READ_STRUCT_XML_RAW        14   // Read a structure (SID) in XML, unformatted       (deprecated, here for backward compatibility)
#define CMD_EDIT                       15   // Edit an entry (OID) in a list (alias of ADD)
#define CMD_READ_DELTA_XML             16   // Read a structure delta (SID) in XML              (deprecated, here for backward compatibility)
#define CMD_READ_DELTA_XML_RAW         17   // Read a structure delta (SID) in XML, unformatted (deprecated, here for backward compatibility)
#define CMD_CLUSTER                0x8000   // Cluster command flag bit
#define CMD_CONNECTED_ONLY         0x4000   // Cluster connected only flag bit
#define CMD_COMBINE_READ_RESULTS   0x2000   // Cluster connected only flag bit
#define CMD_CLUSTER_FLAGS          0xf000   // Cluster flag bits

//****************************************************************
// Execute identifiers (EID)
//----------------------------------------------------------------
#define EID_NONE                        0
#define EID_RESET_ETH                   1   // intvalue = iface
#define EID_CLEAR_ETH_STATS             2   // intvalue = iface, -1 = all
#define EID_CLEAR_RADIO_STATS           3   // intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define EID_CHECK_USER                  4   // strvalue = struct login_user_info
#define EID_SHOW_CUR_CFG                5   // intvalue = include defaults flag
#define EID_SAVE_CUR_CFG                6   // i64value = 0 => eeprom, 1 => flash; for eeprom: intvalue = restore point flag, strvalue = restore filename; for flash: intvalue = warn flag, strvalue = filename for save to flash
#define EID_REBOOT                      7   // intvalue = check save image complete flag, int64value = delay in seconds
#define EID_RESET_CFG                   8   // intvalue = check save image complete flag
#define EID_ENABLE_ALL_RADIOS           9   // intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define EID_DISABLE_ALL_RADIOS         10   // intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define EID_ENABLE_AUTO_CH             11   // intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz, int64value = assign wds channels flag
#define EID_DEAUTH_STA                 12   // strvalue = sta mac addr
#define EID_CLEAR_LOG                  13   //
#define EID_SAVE_IMAGE                 14   // intvalue = save image command (see below), strvalue = filename
#define EID_SHOW_CFG_DIFF              15   // intvalue = config 1 index (see below), int64value = config 2 index, strvalue = filename & section
#define EID_CLEAR_STA_STATS            16   // strvalue = sta mac addr
#define EID_SET_DATE                   17   // strvalue = date/time string (as passed to date)
#define EID_IMPORT_CFG                 18   // strvalue = config file name
#define EID_RESET_CONSOLE              19   // intvalue = display console warning message flag
#define EID_DENY_STA                   20   // strvalue = sta mac addr
#define EID_LOGOUT_USER                21   // strvalue = user name, int64value = wmi session id
#define EID_MOVE_FILTER_UP             22   // strvalue = filter name
#define EID_MOVE_FILTER_DOWN           23   // strvalue = filter name
#define EID_RESET_WDS_CLIENTS          24   //
#define EID_CLEAR_VLAN_STATS           25   // intvalue = vlan, -1 = all, or strvalue = vlan name
#define EID_AUTO_CELL                  26   // intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define EID_ENV_CTRL_SET_STATUS        27   // strvalue = struct env_ctrl_info
#define EID_ENV_CTRL_SET_ALARM         28   // intvalue = env ctrl alarm type (see below)
#define EID_ENV_CTRL_SET_VERSION       29   // strvalue = software version string
//#define EID_WMI_FILE_UPLOAD          30   // strvalue = filename   !DEPRECATED!
//#define EID_WMI_FILE_ERASE           31   // strvalue = filename   !DEPRECATED!
#define EID_CLEAR_WDS_STATS            32   // intvalue = wds index code (see below)
#define EID_SITE_SURVEY                33   // intvalue = on/off (0/1)
#define EID_TXPOWER_ALL_RADIOS         34   // intvalue = tx_power,     int64value = iface, 1 = all, 2 = all 5GHz, 3 = all 2.4GHz
#define EID_RXTHRESHOLD_ALL_RADIOS     35   // intvalue = rx_threshold, int64value = iface, 1 = all, 2 = all 5GHz, 3 = all 2.4GHz
#define EID_RESET_ALL_CHANNELS         36   // intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define EID_SET_UBOOT_ENV_VAR          37   // strvalue = 'variable=value'
#define EID_SAVE_UBOOT_ENV             38   //
#define EID_DIAGNOSTIC_LOG             39   // strvalue = filename to save to flash, 0 for save to ram for wmi, int64value returns pid to wait for completion
#define EID_RESET_CFG_PRESERVE_ETH     40   // intvalue = check save image complete flag
#define EID_RESET_SNMPD_LINK           41   //
#define EID_WDS_LOOPBACK_LINK_INIT     42   // intvalue = source iface, int64value = destination iface
#define EID_WDS_LOOPBACK_LINK_TEST     43   // intvalue = source iface, int64value = destination iface, strvalue = filename for results
#define EID_WDS_LOOPBACK_LINK_DONE     44   //
#define EID_SELF_TEST                  45   // strvalue = filename to save to flash, 0 for save to ram for wmi, int64value returns pid to wait for completion
#define EID_CELLSIZE_ALL_RADIOS        46   // intvalue = cellsize, int64value = iface, 1 = all, 2 = all 5GHz, 3 = all 2.4GHz
#define EID_SYSLOG_MAIL_TEST           47   // strvalue = test message
#define EID_BOND_40MHZ_ALL_RADIOS      48   // intvalue = -1, 0, 1, or 2 (auto),  int64value = iface, 1 = all, 2 = all 5GHz, 3 = all 2.4GHz
#define EID_MOVE_FILTER_LIST_UP        49   // intvalue = filter list index, strvalue = filter name
#define EID_MOVE_FILTER_LIST_DOWN      50   // intvalue = filter list index, strvalue = filter name
#define EID_LOCK_ALL_RADIOS            51   // intvalue = unlock/lock (0/1), int64value = iface, 1 = all, 2 = all 5GHz, 3 = all 2.4GHz
#define EID_WMI_RESTART                52   //
#define EID_WMI_CLEAR_CERT             53   // intvalue = wmi cert action (see below)
#define EID_PCI_AUDIT                  54   //
#define EID_XCFG_CONNECT               55   //
#define EID_XCFG_DISCONNECT            56   //
#define EID_RESTART_SNMPD              57   //
#define EID_SHOW_FACTORY_CFG           58   // intvalue = include defaults flag (for xcfg, ignored in cfg)
#define EID_SHOW_STARTUP_CFG           59   // intvalue = include defaults flag (for xcfg, ignored in cfg)
#define EID_SHOW_SAVED_CFG             60   // intvalue = include defaults flag (for xcfg, ignored in cfg)
#define EID_SYSTEM_CALL                61   // strvalue = system command to execute
#define EID_CLI_COMMAND                62   // strvalue = cli command to execute
#define EID_WRITE_FILE                 63   // intvalue = filesize, strvalue = struct file_write_info (filename & contents)
#define EID_RESTORE_ALL_RADIOS         64   // intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define EID_SYSLOG                     65   // intvalue = syslog level, strvalue = syslog string
#define EID_ENABLE_AUTO_CH_NEG         66   // intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define EID_RESET_DELTA_XML            67   //
#define EID_RESET_DELTA_XML_RAW        68   //
#define EID_SHOW_CFG_DELTA             69   // intvalue = config 1 index (see below), int64value = config 2 index, strvalue = filename & section
#define EID_WIFI_MODE_ALL_RADIOS       70   // intvalue = wifimode, int64value = iface, 1 = all, 2 = all 5GHz, 3 = all 2.4GHz
#define EID_RESTORE_CFG                71   // strvalue = restore file name
#define EID_AUTO_BAND                  72   //
#define EID_DOOR_STATUS                73   // intvalue = closed/opened (0/1)
#define EID_FILE_ERASE                 74   // strvalue = file name
#define EID_FORCE_PRIV                 75   // intvalue = priv level
#define EID_CLEAR_LICENSE              76   //
#define EID_RESET_STA_ASSURANCE        77   //
#define EID_CAPTURE_ENABLE             78   // intvalue = bitmask of radios by sw index
#define EID_CAPTURE_DISABLE            79   // intvalue = bitmask of radios by sw index
#define EID_WDS_ROAM                   80   // intvalue = wds link, strvalue = wds target bssid
#define EID_CLEAR_STA_ASSURANCE        81   //
#define EID_CAPTURE_MONITOR_START      82   // strvalue = struct capture_monitor_cfg
#define EID_CAPTURE_MONITOR_STOP       83   //
#define EID_CLEAR_FILTER_STATS         84   //
#define EID_RESET_ROAMING_ASSIST       85   //
#define EID_REVERT                     86   // intvalue = seconds to revert to saved image; -1 = cancel revert
#define EID_WRITE_XTABLES              87   //
#define EID_WRITE_SECURITY_FILE        88   //
#define EID_WRITE_DHCP_FILE            89   //
#define EID_CLEAR_ACTIVE_VLAN_TABLE    90   //
#define EID_LOAD_QUICK_CONFIG          91   // strvalue = base name of quick config file
#define EID_ACTIVATION_ENABLE          92   // intvalue = 1:start, 0:stop, -1:reset, 2:pause
#define EID_CLEAR_DPI_STATS            93   //
#define EID_ACTIVATION_RESET           94   // intvalue = don't care
#define EID_DISABLE_OVERLORD_MODE      95   //
#define EID_DRIVER_RECONFIG            96   //
#define EID_BOND_80MHZ_ALL_RADIOS      97   // intvalue = -1, 0, 1, or 2 (auto),  int64value = iface, 1 = all, 2 = all 5GHz, 3 = all 2.4GHz
#define EID_ACTIVE_DIRECTORY_JOIN      98   //
#define EID_ACTIVE_DIRECTORY_LEAVE     99   //
#define EID_RESET_DELTA_JSON          100   //
#define EID_RESET_DELTA_JSON_RAW      101   //
#define EID_LLDP_PWR_ALLOCATED        102   // lldp power allocated, enable iaps
#define EID_ASSIGN_STA_GROUP          103   // intvalue = group index, strvalue = sta mac address
#define EID_ARCHLOG_COMMAND           104   // intvalue = -1, 0, 1, or 2; -1 = clean, 0 = stop, 1 = start, 2 = build
#define EID_BOND_160MHZ_ALL_RADIOS    105   // intvalue = -1, 0, 1, or 2 (auto),  int64value = iface, 1 = all, 2 = all 5GHz, 3 = all 2.4GHz
#define EID_CLEAR_IDS_EVENT_LOG       106   //
#define EID_CLEAR_IDS_STATS           107   //
#define EID_FLASH_RECOVERY            108   //

//****************************************************************
// Structure identifiers (SID) definitions
//----------------------------------------------------------------
#define SID_NONE                        0
#define SID_ETH_STATS                   1   // Ethernet statistics                 , intvalue = iface, -1 = all
#define SID_RADIO_STATS                 2   // Radio statistics                    , intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define SID_SUMMARY                     3   // Module summary (see below)          ,
#define SID_STA_TABLE                   4   // Station table                       , intvalue = iface, -1 = all, -2 = sorted by iface, -3 = sorted by ssid, -4 = sorted by group
#define SID_NUM_STATIONS                5   // Number of associated stations       , intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define SID_CHANNELS                    6   // Channel list                        , intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define SID_LOG                         7   // Syslog file                         ,
#define SID_DRIVER_VERSION              8   // Version information                 ,
#define SID_STA_STATS                   9   // Station statistics                  , strvalue = sta mac addr
#define SID_GEOGRAPHY_INFO             10   // Geography information               ,
#define SID_IDS_DATABASE               11   // IDS known/approved list             ,
#define SID_WDS_CLIENT_LINKS           12   // WDS client link number by radio     , intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define SID_VLAN_STATS                 13   // VLAN statistics                     , intvalue = vlan , -1 = all, or strvalue = vlan name
#define SID_SPANNING_TREE              14   // Spanning tree info                  ,
#define SID_NEIGHBOR_ARRAY_INFO        15   // Neighbor array info (network map)   ,
#define SID_ROGUE_AP_TABLE             16   // Rogue ap table                      ,
#define SID_WDS_STATUS                 17   // WDS status (client and host)        ,
#define SID_WDS_HOST_LINKS             18   // WDS host link status                ,
#define SID_STATS_TIME_PERIOD          19   // Time periods for various statistics , intvalue = 1 for ethernet, 2 for radios, 3 for vlans
#define SID_AUTOCELL_RSSI_TABLE        20   // Autocell rssi information by radio  , intvalue = 0 for return data, 1 for external data
#define SID_CFG_MESSAGES               21   // Accumulated cfg debug messages      ,
#define SID_DHCPD_LEASE_TABLE          22   // Dhcp lease table                    ,
#define SID_FLASH_FILE_LIST            23   // List of files on the CF, single str ,
#define SID_SPECTRUM_ANALYZER          24   // Spectrum analyzer table             ,
#define SID_UNASSOC_STA_TABLE          25   // Unassociated station table          ,
#define SID_PACKED_RADIO_STATS         26   // Radio statistics, w/o holes         , intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define SID_PACKED_AUTOCELL_RSSI_TABLE 27   // Autocell rssi information, w/o holes, intvalue = 0 for return data, 1 for external data
#define SID_MAX_STATIONS               28   // Max associated stas by time period  , intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define SID_ADMIN_HISTORY              29   // History of admin logins / logouts   ,
#define SID_READ_FILE                  30   // Read file                           ,
#define SID_FILE_SIZE                  31   // Return file size                    ,
#define SID_FLASH_FILE_TABLE           32   // Table of files on the CF            ,
#define SID_UBOOT_ENV_VAR_TABLE        33   // Table of uboot env variables        ,
#define SID_DBG_SETTINGS               34   // List of debug settings              ,
#define SID_NEIGHBOR_TUNNEL_INFO       35   // Neighbor array tunnel info          ,
#define SID_ROAM_STA_TABLE             36   // Roaming station table               , intvalue = 1 for roam-from-array, 2 for roam-to-array
#define SID_NETWORK_ASSURANCE_INFO     37   // Network assurance status            ,
#define SID_STA_TYPE_CNTS              38   // Station counts by media type        , intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define SID_STA_TYPE_TOTAL             39   // Station counts by media type, total , intvalue = 1 for assoc, 2 for unassoc, 3 for all, +4 for op vs. cap
#define SID_STA_TYPE_TABLE             40   // Station media type table            , intvalue = 1 for assoc, 2 for unassoc, 3 for all, +4 for op vs. cap
#define SID_ROGUE_AP_TYPE_CNTS         41   // Rogue ap counts by category
#define SID_ROGUE_AP_TYPE_TABLE        42   // Rogue ap types table
#define SID_LOG_CNTS                   43   // Syslog type counts
#define SID_IDS_EVENT_LOG              44   // Log of IDS events detected          ,
#define SID_IDS_STATS                  45   // IDS statistics                      ,
#define SID_STA_ASSURANCE_INFO         46   // Station assurance status            ,
#define SID_DEVICE_ID_TOKENS           47   // Device Type & Class tokens          ,
#define SID_RADIO_ASSURE_CNTS          48   // Radio assurance reset counts        , intvalue = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define SID_ACTIVE_VLAN_TABLE          49   // Active VLAN table
#define SID_QUICK_CONFIG_LIST          50   // Quick configuration file list
#define SID_DPI_STATISTICS             51   // DPI statistics                      , intvalue = vlan/ssid, int64value = flags, strvalue = station mac
#define SID_NEIGHBOR_TRAFFIC_INFO      52   // Neighbor traffic map by channel
#define SID_SPECTRUM_ANALYZER_TRAFFIC  53   // Spectrum analyzer traffic map by channel
#define SID_UNDEF_VLAN_TABLE           54   // Undefined VLAN table
#define SID_STA_LOCATION_TABLE         55   // Station (assoc & unassoc) location
#define SID_RAP_LOCATION_TABLE         56   // Rogue AP location
#define SID_NEIGHBOR_STA_TABLE         57   // Neighbor Station RSSI (and SSID/Group) Table
#define SID_NEIGHBOR_RAP_TABLE         58   // Neighbor Rogue RSSI Table

#define SID_XML                0x80000000   // xml flag
#define SID_XML_RAW            0x40000000   // raw xml flag
#define SID_XML_DELTA          0x20000000   // delta xml flag
#define SID_XML_DELTA_RESET    0x10000000   // reset delta xml flag
#define SID_XML_DISP_BY_ARRAY  0x08000000   // display cluster xml by array
#define SID_XML_JSON           0x04000000   // convert xml to json
#define SID_XML_NO_HDR         0x02000000   // do not include array header
#define SID_XML_SHIFT          0x01000000   // shift output to right
#define SID_XML_CAMEL          0x00800000   // use camel case output
#define SID_XML_LAST           0x00400000   // flag as last section of running config
#define SID_XML_CLR_TXT        0x00200000   // display passwords & secrets as clear text
#define SID_XML_SPARE          0x00100000   // spare xml flag bit
#define SID_XML_VERSION        0x000f0000   // xml/json version to use (defaults to 1)
#define SID_XML_VERSION_SHIFT          16
#define SID_XML_FLAGS          0xffff0000

//****************************************************************
// SID_SUMMARY module identifiers
//----------------------------------------------------------------
#define SUMMARY_NONE                    0
#define SUMMARY_HEADER                  0
#define SUMMARY_RADIO                   1   // int64value = iface, -1 = all, -2 = all 5GHz, -3 = all 2.4GHz
#define SUMMARY_ETHERNET                2   // int64value = iface, -1 = all
#define SUMMARY_SECURITY                3   //
#define SUMMARY_SSID                    4   //
#define SUMMARY_DHCP                    5   //
#define SUMMARY_DNS                     6   //
#define SUMMARY_SNMP                    7   //
#define SUMMARY_DATE_TIME               8   //
#define SUMMARY_SYSLOG                  9   //
#define SUMMARY_RADIUS                 10   //
#define SUMMARY_LOCAL_RADIUS           11   //
#define SUMMARY_ACL                    12   // int64value = null termination flag (1 = add null termination to mac strs, 0 = no null terminations)
#define SUMMARY_ADMIN                  13   //
#define SUMMARY_VERSION                14   //
#define SUMMARY_RADIO_GLB              15   //
#define SUMMARY_RADIO_GLB_A            16   //
#define SUMMARY_RADIO_GLB_G            17   //
#define SUMMARY_CONSOLE                18   //
#define SUMMARY_CONTACT                19   //
#define SUMMARY_FILTER                 20   // int64value = raw flag (1 = do not sort by priority, 0 = sort by priority)
#define SUMMARY_WDS_CLIENTS            21   //
#define SUMMARY_IAP_DECODER            22   //
#define SUMMARY_VLAN                   23   //
#define SUMMARY_SSID_LIMIT             24   // deprecated (here for backward compatibility)
#define SUMMARY_IDS_DATABASE           25   //
#define SUMMARY_XRP_TARGETS            26   //
#define SUMMARY_MANAGEMENT             27   //
#define SUMMARY_ENV_CTRL               28   //
#define SUMMARY_SSID_WPR               29   // deprecated (here for backward compatibility)
#define SUMMARY_RADIUS_ACCT            30   //
#define SUMMARY_STANDBY                31   //
#define SUMMARY_LOCAL_RADIUS_PW        32   // deprecated (here for backward compatibility)
#define SUMMARY_UBOOT_ENV_VARS         33   //
#define SUMMARY_CDP                    34   //
#define SUMMARY_CDP_LIST               35   //
#define SUMMARY_RADIO_GLB_N            36   //
#define SUMMARY_GROUP                  37   //
#define SUMMARY_FILTER_LIST            38   // int64value = filter list index or strvalue = filter list name
#define SUMMARY_FILTER_LIST_RAW        39   // int64value = filter list index or strvalue = filter list name
#define SUMMARY_FILTER_LIST_HEADERS    40   //
#define SUMMARY_FILTER_LIST_ALL        41   //
#define SUMMARY_NETFLOW                42   //
#define SUMMARY_PACKED_RADIO           43   //
#define SUMMARY_ADMIN_RADIUS           44   //
#define SUMMARY_SECTIONS               45   //
#define SUMMARY_LOCAL_RADIUS_PW_ENC    46   //
#define SUMMARY_WEP_KEYS_ENC           47   //
#define SUMMARY_SSID_PARAMS_ENC        48   //
#define SUMMARY_VLAN_PARAMS_ENC        49   //
#define SUMMARY_CLUSTERS               50   //
#define SUMMARY_CLUSTER_ARRAYS         51   //
#define SUMMARY_EXT_MGMT               52   //
#define SUMMARY_RADIUS_ALL             53   //
#define SUMMARY_WIFI_TAG               54   //
#define SUMMARY_PRIVILEGE              55   //
#define SUMMARY_PRIV_LEVELS            56   //
#define SUMMARY_PRIV_SECTIONS          57   //
#define SUMMARY_AUTOCHANNEL            58   //
#define SUMMARY_LOAD_BALANCE           59   //
#define SUMMARY_CLUSTER_ARRAYS_ENC     60   //
#define SUMMARY_FILTER_LIST_ALL_RAW    61   //
#define SUMMARY_IDS                    62   //
#define SUMMARY_STA_ASSURANCE          63   //
#define SUMMARY_TUNNEL                 64   //
#define SUMMARY_ROAMING_ASSIST         65   //
#define SUMMARY_VAP                    66   //
#define SUMMARY_FILTER_LIST_NOSTATS    67   // int64value = filter list index or strvalue = filter list name
#define SUMMARY_ETHERNET_BONDS         68   //
#define SUMMARY_LOCATION               69   //
#define SUMMARY_GROUP_WPR_WHITELIST    70   //
#define SUMMARY_SSID_WPR_WHITELIST     71   //
#define SUMMARY_MDM                    72   //
#define SUMMARY_OAUTH                  73   //
#define SUMMARY_SSID_HONEYPOT_WLIST    74   //
#define SUMMARY_SSID_HONEYPOT_BCAST    75   //
#define SUMMARY_RADIO_GLB_AC           76   //
#define SUMMARY_PROXY_FWD              77   //
#define SUMMARY_ACTIVE_DIRECTORY       78   //
#define SUMMARY_LLDP                   79   //
#define SUMMARY_LLDP_LIST              80   //
#define SUMMARY_PROXY_MGMT             81   //
#define SUMMARY_APP_LIST               82   // int64value = app list index or strvalue = app list name
#define SUMMARY_APP_LIST_HEADERS       83   //
#define SUMMARY_APP_LIST_ALL           84   //
#define SUMMARY_VLAN_POOL              85   //
#define SUMMARY_POSITION               86   //
#define SUMMARY_GROUP_PARAMS_ENC       87   //
#define SUMMARY_FIXED_ADDRS            88   //
#define SUMMARY_BLUETOOTH              89   //
#define SUMMARY_DEVICE_ID_MAC_TABLE    90   //

//****************************************************************
// Structure identifiers (SID) definitions rev table
//----------------------------------------------------------------
#define SID_SORT            1               // auto sort in cluster mode
#define SID_DEDUP           1               // auto dedup in cluster mode
#define SID_CFG             1               // call returns configuration data
#define SID_SNMP_ONLY       1               // is only called by snmp module
#define SID_STATUS          0               // call returns status information
#define SID_PRIVATE         2               // call is internal and NOT included in JSON interface
#define SID_DEPRECATED     -1               // call is deprecated and NOT included in JSON interface

struct sid_info {
   int32 index;
   int32 size;
   int32 number;
   int32 sort_len;
   int32 dedup;
   int32 type;
} __attribute__ ((packed));

#define SID_REV_TABLE   {            /* SIZE                              , MAX NUM ELEMENTS           , SORT LENGTH                           , DEDUP    , TYPE          */  \
      { SID_NONE                      , 0                                 , 0                          , 0                                     , 0        , SID_PRIVATE    }, \
      { SID_ETH_STATS                 , SIZE_ETH_STATS                    , MOM_NUM_ETH_INTERFACES     , 0                                     , 0        , SID_STATUS     }, \
      { SID_RADIO_STATS               , SIZE_RADIO_STATS                  , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_STATUS     }, \
      { SID_SUMMARY                   , SIZE_CFG_REQ                      , 0                          , 0                                     , 0        , SID_PRIVATE    }, \
      { SID_STA_TABLE                 , SIZE_STA_INFO                     , MAX_NUM_STATIONS           , REQ_LEN_MAC_ADDR_STR + 1              , 0        , SID_STATUS     }, \
      { SID_NUM_STATIONS              , SIZE_NUM_STATIONS_INFO            , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_STATUS     }, \
      { SID_CHANNELS                  , SIZE_CHANNELS_INFO                , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_STATUS     }, \
      { SID_LOG                       , SIZE_SYSLOG_ENTRY                 , MAX_SIZE_LOG_FILE          , 3*SIZE_UINT32                         , 0        , SID_STATUS     }, \
      { SID_DRIVER_VERSION            , SIZE_DRIVER_FPGA_INFO             , 1                          , 0                                     , SID_DEDUP, SID_STATUS     }, \
      { SID_STA_STATS                 , SIZE_STA_STATS                    , 1                          , 0                                     , 0        , SID_STATUS     }, \
      { SID_GEOGRAPHY_INFO            , SIZE_GEOGRAPHY_INFO               , 1                          , 0                                     , SID_DEDUP, SID_STATUS     }, \
      { SID_IDS_DATABASE              , SIZE_IDS_HDR + SIZE_IDS_SSID      , 1                          , 0                                     , SID_DEDUP, SID_STATUS     }, \
      { SID_WDS_CLIENT_LINKS          , SIZE_WDS_CLIENT_LINK_INFO         , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_STATUS     }, \
      { SID_VLAN_STATS                , SIZE_VLAN_STATS                   , MAX_NUM_VLAN               , REQ_LEN_VLAN_NAME + SIZE_UINT16       , 0        , SID_STATUS     }, \
      { SID_SPANNING_TREE             , SIZE_STP_STATE                    , MAX_NUM_VLAN + 1           , REQ_LEN_VLAN_NAME + SIZE_UINT16       , SID_DEDUP, SID_STATUS     }, \
      { SID_NEIGHBOR_ARRAY_INFO       , SIZE_NEIGHBOR_ARRAY_INFO          , MAX_NUM_NEIGHBORS          , REQ_LEN_HOSTNAME_STR                  , SID_DEDUP, SID_STATUS     }, \
      { SID_ROGUE_AP_TABLE            , SIZE_ROGUE_AP_INFO                , MAX_IDS_TABLE_ENTRIES      , MAC_ADDR_LEN                          , SID_DEDUP, SID_STATUS     }, \
      { SID_WDS_STATUS                , SIZE_WDS_BIND_INFO                , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_STATUS     }, \
      { SID_WDS_HOST_LINKS            , SIZE_WDS_HOST_INFO                , MAX_NUM_WDS_HOST_LINKS     , 0                                     , 0        , SID_STATUS     }, \
      { SID_STATS_TIME_PERIOD         , SIZE_STATS_TIME_PERIOD_INFO       , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_STATUS     }, \
      { SID_AUTOCELL_RSSI_TABLE       , SIZE_AUTOCELL_RSSI_INFO           , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_STATUS     }, \
      { SID_CFG_MESSAGES              , SIZE_CFG_MESSAGE_INFO             , MAX_STR                    , 0                                     , 0        , SID_STATUS     }, \
      { SID_DHCPD_LEASE_TABLE         , SIZE_LEASE_INFO                   , MAX_NUM_STATIONS           , REQ_LEN_MAC_ADDR_STR + 1              , 0        , SID_STATUS     }, \
      { SID_FLASH_FILE_LIST           , SIZE_FLASH_FILE_LIST_INFO         , MAX_SIZE_FILE              , 0                                     , 0        , SID_PRIVATE    }, \
      { SID_SPECTRUM_ANALYZER         , SIZE_RADIO_STATS                  , MAX_SA_CHANNELS            , 0                                     , 0        , SID_STATUS     }, \
      { SID_UNASSOC_STA_TABLE         , SIZE_STA_INFO                     , MAX_NUM_STATIONS           , REQ_LEN_MAC_ADDR_STR + 1              , 0        , SID_STATUS     }, \
      { SID_PACKED_RADIO_STATS        , SIZE_RADIO_STATS                  , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_PRIVATE    }, \
      { SID_PACKED_AUTOCELL_RSSI_TABLE, SIZE_AUTOCELL_RSSI_INFO           , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_PRIVATE    }, \
      { SID_MAX_STATIONS              , SIZE_MAX_STATIONS_INFO            , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_STATUS     }, \
      { SID_ADMIN_HISTORY             , SIZE_ADMIN_HISTORY_INFO           , MAX_NUM_SESSIONS           , REQ_LEN_STR                           , 0        , SID_STATUS     }, \
      { SID_READ_FILE                 , SIZE_READ_FILE_INFO               , MAX_SIZE_FILE              , 0                                     , 0        , SID_PRIVATE    }, \
      { SID_FILE_SIZE                 , SIZE_FILE_SIZE_INFO               , 1                          , 0                                     , 0        , SID_PRIVATE    }, \
      { SID_FLASH_FILE_TABLE          , SIZE_FLASH_FILE_ENTRY             , MAX_NUM_FLASH_FILES        , REQ_LEN_HOSTNAME_STR                  , SID_DEDUP, SID_STATUS     }, \
      { SID_UBOOT_ENV_VAR_TABLE       , SIZE_UBOOT_ENV_ENTRY              , MAX_NUM_UBOOT_ENV_VARS     , REQ_LEN_ENV_VAR_NAME                  , SID_DEDUP, SID_CFG        }, \
      { SID_DBG_SETTINGS              , SIZE_DBG_SETTINGS                 , MAX_NUM_DBG_ENTRIES        , 0                                     , 0        , SID_PRIVATE    }, \
      { SID_NEIGHBOR_TUNNEL_INFO      , SIZE_NEIGHBOR_TUNNEL_INFO         , MAX_NUM_XRP_TUNNELS        , REQ_LEN_HOSTNAME_STR                  , SID_DEDUP, SID_STATUS     }, \
      { SID_ROAM_STA_TABLE            , SIZE_ROAM_STA_INFO                , MAX_NUM_STATIONS           , REQ_LEN_MAC_ADDR_STR + 1              , 0        , SID_STATUS     }, \
      { SID_NETWORK_ASSURANCE_INFO    , SIZE_NA_TEST_INFO                 , MAX_NUM_NA_TESTS           , REQ_LEN_HOSTNAME_STR                  , SID_DEDUP, SID_STATUS     }, \
      { SID_STA_TYPE_CNTS             , SIZE_STA_TYPE_CNT_INFO            , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_STATUS     }, \
      { SID_STA_TYPE_TOTAL            , SIZE_STA_TYPE_CNT_INFO            , 1                          , 0                                     , 0        , SID_STATUS     }, \
      { SID_STA_TYPE_TABLE            , SIZE_STA_TYPE_INFO                , MAX_NUM_STATIONS           , REQ_LEN_MAC_ADDR_STR + 1              , 0        , SID_STATUS     }, \
      { SID_ROGUE_AP_TYPE_CNTS        , SIZE_ROGUE_AP_CNT_INFO            , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_STATUS     }, \
      { SID_ROGUE_AP_TYPE_TABLE       , SIZE_ROGUE_AP_INFO                , MAX_IDS_TABLE_ENTRIES      , MAC_ADDR_LEN                          , 0        , SID_STATUS     }, \
      { SID_LOG_CNTS                  , SIZE_SYSLOG_COUNTS                , 1                          , 0                                     , 0        , SID_STATUS     }, \
      { SID_IDS_EVENT_LOG             , SIZE_IDS_EVENT_INFO               , MAX_IDS_EVENT_ENTRIES      , 0                                     , 0        , SID_STATUS     }, \
      { SID_IDS_STATS                 , SIZE_IDS_STATS                    , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_STATUS     }, \
      { SID_STA_ASSURANCE_INFO        , SIZE_STA_ASSURE_INFO              , MAX_NUM_STATIONS           , SIZE_UINT32                           , 0        , SID_STATUS     }, \
      { SID_DEVICE_ID_TOKENS          , SIZE_DEVICE_ID_INFO               , MAX_NUM_DEVICE_ID_TOKENS   , SIZE_DEVICE_ID_INFO                   , 0        , SID_STATUS     }, \
      { SID_RADIO_ASSURE_CNTS         , SIZE_RADIO_ASSURE_INFO            , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_STATUS     }, \
      { SID_ACTIVE_VLAN_TABLE         , SIZE_VLAN_BIT_TABLE               , 1                          , 0                                     , 0        , SID_STATUS     }, \
      { SID_QUICK_CONFIG_LIST         , SIZE_QUICK_CONFIG_ENTRY           , MAX_NUM_QUICK_CONFIGS      , REQ_LEN_HOSTNAME_STR                  , SID_DEDUP, SID_STATUS     }, \
      { SID_DPI_STATISTICS            , SIZE_DPI_INFO                     , MAX_DPI_APP_INDEX          , REQ_LEN_DPI_STR                       , 0        , SID_STATUS     }, \
      { SID_NEIGHBOR_TRAFFIC_INFO     , SIZE_NEIGHBOR_TRAFFIC_INFO        , MAX_ALL_CHANNELS           , 0                                     , 0        , SID_STATUS     }, \
      { SID_SPECTRUM_ANALYZER_TRAFFIC , SIZE_SPECTRUM_ANALYZER_TRAFFIC    , MAX_ALL_CHANNELS           , 0                                     , 0        , SID_STATUS     }, \
      { SID_UNDEF_VLAN_TABLE          , SIZE_VLAN_BIT_TABLE               , 1                          , 0                                     , 0        , SID_STATUS     }, \
      { SID_STA_LOCATION_TABLE        , SIZE_STA_LOCATION_INFO            , MAX_NUM_STATIONS           , MAC_ADDR_LEN                          , SID_DEDUP, SID_STATUS     }, \
      { SID_RAP_LOCATION_TABLE        , SIZE_STA_LOCATION_INFO            , MAX_IDS_TABLE_ENTRIES      , MAC_ADDR_LEN                          , SID_DEDUP, SID_STATUS     }, \
      { SID_NEIGHBOR_STA_TABLE        , SIZE_STA_RSSI_ENTRY               , MAX_NUM_STATIONS * MAX_IAPS, MAC_ADDR_LEN                          , SID_DEDUP, SID_STATUS     }, \
      { SID_NEIGHBOR_RAP_TABLE        , SIZE_STA_RSSI_ENTRY               , MAX_IDS_TABLE_ENTRIES      , MAC_ADDR_LEN                          , SID_DEDUP, SID_STATUS     }, \
}

#define SUMMARY_REV_TABLE   {        /* SIZE                              , MAX NUM ELEMENTS           , SORT LENGTH                           , DEDUP    , TYPE          */  \
      { SUMMARY_HEADER                , SIZE_ARRAY_ID                     , 1                          , 0                                     , 0        , SID_PRIVATE    }, \
      { SUMMARY_RADIO                 , SIZE_RADIO_CFG                    , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_CFG        }, \
      { SUMMARY_ETHERNET              , SIZE_ETH_CFG                      , MOM_NUM_ETH_INTERFACES     , 0                                     , 0        , SID_CFG        }, \
      { SUMMARY_SECURITY              , SIZE_SECURITY_CFG                 , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_SSID                  , SIZE_SSID_CFG                     , MAX_NUM_SSID               , REQ_LEN_SSID                          , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_DHCP                  , SIZE_DHCP_POOL_CFG                , MAX_NUM_DHCP_POOL          , REQ_LEN_DHCP_POOL_NAME                , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_DNS                   , SIZE_DNS_CFG                      , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_SNMP                  , SIZE_SNMP_CFG                     , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_DATE_TIME             , SIZE_DATE_TIME_CFG                , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_SYSLOG                , SIZE_SYSLOGD_CFG                  , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_RADIUS                , SIZE_RADIUS_CFG                   , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_LOCAL_RADIUS          , SIZE_RADUSER_READ_STRUCT_INFO     , MAX_NUM_RADIUS_USERS       , REQ_LEN_STR                           , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_ACL                   , REQ_LEN_MAC_ADDR_STR + 1          , MAX_SIZE_ACL               , REQ_LEN_MAC_ADDR_STR + 1              , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_ADMIN                 , SIZE_ADMIN_CFG                    , MAX_NUM_ADMIN              , REQ_LEN_STR                           , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_VERSION               , SIZE_VERSION_INFO                 , 1                          , 0                                     , 0        , SID_STATUS     }, \
      { SUMMARY_RADIO_GLB             , SIZE_IAP_GLB_SUMMARY              , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_RADIO_GLB_A           , SIZE_RADIO_GLB_11A_CFG            , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_RADIO_GLB_G           , SIZE_RADIO_GLB_11G_CFG            , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_CONSOLE               , SIZE_CONSOLE_CFG                  , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_CONTACT               , SIZE_CONTACT_CFG                  , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_FILTER                , SIZE_FILTER_CFG                   , MAX_NUM_FILTERS            , REQ_LEN_FILTER_NAME                   , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_WDS_CLIENTS           , SIZE_WDS_CLIENT_CFG               , MAX_NUM_WDS_CLIENT_LINKS   , 0                                     , 0        , SID_CFG        }, \
      { SUMMARY_IAP_DECODER           , SIZE_IAP_DECODER_RING             , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_CFG        }, \
      { SUMMARY_VLAN                  , SIZE_VLAN_CFG                     , MAX_NUM_VLAN               , REQ_LEN_VLAN_NAME + SIZE_UINT16       , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_SSID_LIMIT            , SIZE_LIMIT_CFG                    , MAX_NUM_SSID               , 0                                     , 0        , SID_DEPRECATED }, \
      { SUMMARY_IDS_DATABASE          , SIZE_IDS_SSID                     , MAX_IDS_SSID_ENTRIES       , SIZE_IDS_SSID                         , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_XRP_TARGETS           , SIZE_XRP_TARGET_CFG               , MAX_NUM_XRP_TARGETS        , SIZE_XRP_TARGET_CFG                   , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_MANAGEMENT            , SIZE_MGMT_CFG                     , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_ENV_CTRL              , SIZE_ENV_CTRL_INFO                , 1                          , 0                                     , 0        , SID_STATUS     }, \
      { SUMMARY_SSID_WPR              , SIZE_WPR_CFG                      , MAX_NUM_SSID               , 0                                     , 0        , SID_DEPRECATED }, \
      { SUMMARY_RADIUS_ACCT           , SIZE_RADIUS_ACCT_CFG              , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_STANDBY               , SIZE_STANDBY_CFG                  , 1                          , 0                                     , 0        , SID_CFG        }, \
      { SUMMARY_LOCAL_RADIUS_PW       , SIZE_RADPASSWD_READ_STRUCT_INFO   , MAX_NUM_RADIUS_USERS       , 0                                     , 0        , SID_SNMP_ONLY  }, \
      { SUMMARY_UBOOT_ENV_VARS        , SIZE_UBOOT_ENV_VAR_INFO           , MAX_SIZE_UBOOT_ENV         , 0                                     , 0        , SID_DEPRECATED }, \
      { SUMMARY_CDP                   , SIZE_CDP_CFG                      , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_CDP_LIST              , SIZE_CDP_INFO                     , MAX_NUM_CDP_ENTRIES        , REQ_LEN_STR                           , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_RADIO_GLB_N           , SIZE_RADIO_GLB_11N_CFG            , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_GROUP                 , SIZE_GROUP_CFG                    , MAX_NUM_GROUP              , REQ_LEN_GROUP                         , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_FILTER_LIST           , SIZE_FILTER_CFG                   , MAX_NUM_FILTERS            , REQ_LEN_FILTER_NAME                   , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_FILTER_LIST_RAW       , SIZE_FILTER_CFG                   , MAX_NUM_FILTERS            , 0                                     , 0        , SID_CFG        }, \
      { SUMMARY_FILTER_LIST_HEADERS   , SIZE_FILTER_LIST_HEADER_CFG       , MAX_NUM_FILTER_LISTS       , REQ_LEN_FILTER_NAME                   , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_FILTER_LIST_ALL       , SIZE_FILTER_LIST_CFG              , MAX_NUM_FILTER_LISTS       , REQ_LEN_FILTER_NAME                   , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_NETFLOW               , SIZE_NETFLOW_CFG                  , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_PACKED_RADIO          , SIZE_RADIO_CFG                    , MOM_NUM_RADIO_INTERFACES   , 0                                     , 0        , SID_PRIVATE    }, \
      { SUMMARY_ADMIN_RADIUS          , SIZE_RADIUS_CFG                   , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_SECTIONS              , SIZE_SECTION_CFG                  , MAX_NUM_SECTIONS           , REQ_LEN_STR                           , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_LOCAL_RADIUS_PW_ENC   , SIZE_RADPASSWDENC_READ_STRUCT_INFO, MAX_NUM_RADIUS_USERS       , 0                                     , 0        , SID_SNMP_ONLY  }, \
      { SUMMARY_WEP_KEYS_ENC          , SIZE_WEP_KEYS_ENC                 , 1                          , 0                                     , 0        , SID_SNMP_ONLY  }, \
      { SUMMARY_SSID_PARAMS_ENC       , SIZE_SSID_PARAMS_ENC              , MAX_NUM_SSID               , REQ_LEN_SSID                          , SID_DEDUP, SID_SNMP_ONLY  }, \
      { SUMMARY_VLAN_PARAMS_ENC       , SIZE_VLAN_PARAMS_ENC              , MAX_NUM_VLAN               , SIZE_UINT16                           , SID_DEDUP, SID_SNMP_ONLY  }, \
      { SUMMARY_CLUSTERS              , SIZE_CLUSTER_ARRAY_CFG            , MAX_NUM_CLUSTERS           , REQ_LEN_CLUSTER                       , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_CLUSTER_ARRAYS        , SIZE_CLUSTER_ARRAY_CFG            , MAX_NUM_CLUSTER_ARRAYS     , REQ_LEN_CLUSTER + REQ_LEN_HOSTNAME_STR, SID_DEDUP, SID_CFG        }, \
      { SUMMARY_EXT_MGMT              , SIZE_EXT_MGMT_CFG                 , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_RADIUS_ALL            , SIZE_RADIUS_ALL_CFG               , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_WIFI_TAG              , SIZE_WIFI_TAG_CFG                 , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_PRIVILEGE             , SIZE_PRIV_CFG                     , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_PRIV_LEVELS           , SIZE_PRIV_LEVEL_CFG               , MAX_NUM_PRIV_LEVELS + 1    , SIZE_PRIV_LEVEL_CFG                   , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_PRIV_SECTIONS         , SIZE_PRIV_SECTION_CFG             , MAX_NUM_PRIV_SECTIONS      , REQ_LEN_PRIV_NAME_STR                 , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_AUTOCHANNEL           , SIZE_AUTOCHANNEL_INFO             , 1                          , 0                                     , 0        , SID_STATUS     }, \
      { SUMMARY_LOAD_BALANCE          , SIZE_LOAD_BALANCE_CFG             , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_CLUSTER_ARRAYS_ENC    , SIZE_CLUSTER_ARRAY_CFG            , MAX_NUM_CLUSTER_ARRAYS     , REQ_LEN_CLUSTER + REQ_LEN_HOSTNAME_STR, SID_DEDUP, SID_SNMP_ONLY  }, \
      { SUMMARY_FILTER_LIST_ALL_RAW   , SIZE_FILTER_LIST_CFG              , MAX_NUM_FILTER_LISTS       , REQ_LEN_FILTER_NAME                   , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_IDS                   , SIZE_IDS_HDR                      , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_STA_ASSURANCE         , SIZE_STA_ASSURE_CFG               , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_TUNNEL                , SIZE_TUNNEL_CFG                   , MAX_NUM_TUNNELS            , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_ROAMING_ASSIST        , SIZE_ROAM_ASSIST_CFG              , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_VAP                   , SIZE_VAP_CFG                      , 0                          , 0                                     , 0        , SID_DEPRECATED }, \
      { SUMMARY_FILTER_LIST_NOSTATS   , SIZE_FILTER_CFG                   , MAX_NUM_FILTERS            , REQ_LEN_FILTER_NAME                   , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_ETHERNET_BONDS        , SIZE_BOND_CFG                     , MOM_NUM_ETH_BONDS          , 0                                     , 0        , SID_CFG        }, \
      { SUMMARY_LOCATION              , SIZE_LOCATION_CFG                 , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_GROUP_WPR_WHITELIST   , SIZE_WLIST_STR_CFG                , MAX_SIZE_WLIST             , REQ_LEN_HOSTNAME_STR                  , SID_DEDUP, SID_DEPRECATED }, \
      { SUMMARY_SSID_WPR_WHITELIST    , SIZE_WLIST_STR_CFG                , MAX_SIZE_WLIST             , REQ_LEN_HOSTNAME_STR                  , SID_DEDUP, SID_DEPRECATED }, \
      { SUMMARY_MDM                   , SIZE_MDM_CFG                      , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_OAUTH                 , SIZE_OAUTH_CFG                    , MAX_NUM_OAUTH_TOKENS       , REQ_LEN_STR                           , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_SSID_HONEYPOT_WLIST   , SIZE_HP_WLIST_STR_CFG             , MAX_SIZE_HONEYPOT_WLIST    , REQ_LEN_SSID                          , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_SSID_HONEYPOT_BCAST   , SIZE_HP_BCAST_STR_CFG             , MAX_SIZE_HONEYPOT_BCAST    , REQ_LEN_SSID                          , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_RADIO_GLB_AC          , SIZE_RADIO_GLB_11AC_CFG           , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_PROXY_FWD             , SIZE_PROXY_FWD_CFG                , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_ACTIVE_DIRECTORY      , SIZE_ACTIVE_DIRECTORY_CFG         , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_LLDP                  , SIZE_LLDP_CFG                     , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_LLDP_LIST             , SIZE_LLDP_INFO                    , MAX_NUM_LLDP_ENTRIES       , REQ_LEN_STR                           , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_PROXY_MGMT            , SIZE_PROXY_MGMT_CFG               , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_APP_LIST              , REQ_LEN_GUID_STR                  , MAX_NUM_APPS_PER_LIST      , REQ_LEN_GUID_STR                      , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_APP_LIST_HEADERS      , SIZE_APP_LIST_HEADER_CFG          , MAX_NUM_APP_LISTS          , REQ_LEN_GUID_STR                      , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_APP_LIST_ALL          , SIZE_APP_LIST_CFG                 , MAX_NUM_APP_LISTS          , REQ_LEN_GUID_STR                      , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_VLAN_POOL             , SIZE_VLAN_POOL_CFG                , MAX_NUM_VLAN_POOL          , REQ_LEN_VLAN_NAME                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_POSITION              , SIZE_POSITION_CFG                 , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_GROUP_PARAMS_ENC      , SIZE_GROUP_PARAMS_ENC             , MAX_NUM_GROUP              , REQ_LEN_GROUP                         , SID_DEDUP, SID_SNMP_ONLY  }, \
      { SUMMARY_FIXED_ADDRS           , SIZE_FIXED_ADDR_INFO              , MAX_NUM_FIXED_ADDRS        , REQ_LEN_MAC_ADDR_STR + 1              , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_BLUETOOTH             , SIZE_BLUETOOTH_CFG                , 1                          , 0                                     , SID_DEDUP, SID_CFG        }, \
      { SUMMARY_DEVICE_ID_MAC_TABLE   , SIZE_DEVICE_ID_MAC_ENTRY          , MAX_NUM_DEV_MAC_ENTRIES    , REQ_LEN_MAC_STR                       , SID_DEDUP, SID_CFG        }, \
}

//****************************************************************
// SNMP MIBII Interface defines
//----------------------------------------------------------------
#define lookup_dev(iface, tbl, num, name, flag, len) ({                              \
    int  i;                                                                          \
    for (i = 0; i < num; ++i)                                                        \
        if (tbl[i].flag && (len ? strxncmp(iface, tbl[i].name, strxlen(tbl[i].name)) \
                                :  strxcmp(iface, tbl[i].name)) == 0)                \
            break;                                                                   \
    (i < num) ? &tbl[i] : NULL;                                                      \
})
#define CMP_ALL     0
#define CMP_LEN     1

struct xap_dev_cfg {
    char *sysdev_name;
    char *usrdev_name;
};
#define XAP_BRTUN_LOOKUP_TBL    {"br0"   , "bridge"     }, {"br"    , "bridge-vlan"}, {"tun"   , "xrp-tunnel" },
#define XAP_WCLNT_LOOKUP_TBL    {"wbond0", "wds-bond-c1"}, {"wbond1", "wds-bond-c2"}, {"wbond2", "wds-bond-c3"}, {"wbond3", "wds-bond-c4"},
#define XAP_WHOST_LOOKUP_TBL    {"wbond4", "wds-bond-h1"}, {"wbond5", "wds-bond-h2"}, {"wbond6", "wds-bond-h3"}, {"wbond7", "wds-bond-h4"},

//****************************************************************
// (EID_LOGOUT_USER) Admin commands
//----------------------------------------------------------------
#define ADMIN_LOGOUT                    1
#define ADMIN_TIMEOUT                   2

//****************************************************************
// (OID_RESET_REASON) Reset codes from SCD device
//----------------------------------------------------------------
//#define RESET_POWER_ON             0x01   // Board reset
//#define RESET_EXTERNAL             0x02   // External reset asserted
//#define RESET_BROWN_OUT            0x04   // Brown out caused reset
//#define RESET_SCD_WATCHDOG         0x08   // Atmel    watchdog timer failed
//#define RESET_MPC_WATCHDOG         0x10   // Motorola watchdog timer failed

// Reset code definitions used by SNMP
#define RESET_CODE_REBOOT_REQ           0
#define RESET_CODE_POWER_ON             1
#define RESET_CODE_POWER_BROWN_OUT      2
#define RESET_CODE_SCD_RESTART          3
#define RESET_CODE_GPC                  4
#define RESET_CODE_UNAVAILABLE          5

//****************************************************************
// OID_RADIO_GLB_G_DOT11_{SLOT,PREAMBLE,PROTECT,GONLY}
//----------------------------------------------------------------
#define DOT11_SLOT_AUTO_TIME            0   // 0=long enabled (auto long/short)
#define DOT11_SLOT_SHORT_ONLY           1   //                1=short only
#define DOT11_PRE_LONG_ONLY             0   // 0=long only
#define DOT11_PRE_AUTO_LENGTH           1   //                1=short enabled (auto long/short)
#define DOT11_PROTECT_OFF               0   // 0=off
#define DOT11_PROTECT_AUTO_CTS          1   //                1=CTS enabled   (auto CTS)
#define DOT11_GONLY_OFF                 0   // 0=off
#define DOT11_GONLY_ON                  1   //                1=on

//****************************************************************
// (CMD_DELETE:OID_SSID_NAME) Options
//----------------------------------------------------------------
#define CMD_DEL_SSID_CHECKS             1   // Set request int to enable radius user checking

//---------------------------------------------------------
// Return codes
//---------------------------------------------------------
#define CMD_DEL_SSID_UNDEFINED         -1
#define CMD_DEL_SSID_RADIUS            -2   // Radius users defined for this SSID
#define CMD_DEL_SSID_LAST              -3   // Cannot delete the last SSID
#define CMD_DEL_SSID_BROADCAST         -4   // SSID is the broadcast
#define CMD_DEL_SSID_BROADCAST_RAD     -5   // SSID is the broadcast AND there are Radius users

//****************************************************************
// WEP key lengths
//----------------------------------------------------------------
#define WEP_40BIT_ASCII_KEY_LEN         5
#define WEP_40BIT_HEX_KEY_LEN          10
#define WEP_128BIT_ASCII_KEY_LEN       13
#define WEP_128BIT_HEX_KEY_LEN         26
#define NUM_WEP_KEYS                    4

//****************************************************************
// WPA Key & IE lengths
//----------------------------------------------------------------
#define WPA_KEY_LEN                    32
#define WPA_KEY_STR_LEN                (WPA_KEY_LEN*2 + 1)
#define MAX_WPA_IE_LEN                 255
#define MAX_WPA_EAPOL_KEY_LEN          99 + MAX_WPA_IE_LEN
#define MAX_AUTH_FT_IE_LEN             255
#define MAX_AUTH_RSN_IE_LEN            255
#define MAX_AUTH_MD_IE_LEN             255
#define MAX_EAPOL_FRAME_LEN           1500

//****************************************************************
//  Ethernet interface numbers
//----------------------------------------------------------------
#define GIG1_BOND                       0
#define GIG2_BOND                       1
#define GIG3_BOND                       2
#define GIG4_BOND                       3
#define GIG5_BOND                       4

//****************************************************************
// SSID Encryption
//----------------------------------------------------------------
#define ENC_NONE                        0
#define ENC_WEP                         1
#define ENC_WPA                         2
#define ENC_WPA2                        3
#define ENC_WPA_BOTH                    4

//****************************************************************
// Station encryption settings
//----------------------------------------------------------------
#define STA_CIPHER_NONE                 0
#define STA_CIPHER_RC4                  1
#define STA_CIPHER_TKIP                 2
#define STA_CIPHER_AES                  3

#define STA_KEY_MGMT_NONE               0
#define STA_KEY_MGMT_EAP                1
#define STA_KEY_MGMT_PSK                2
#define STA_KEY_MGMT_FT_IEEE8021X       3
#define STA_KEY_MGMT_FT_PSK             4

//****************************************************************
// SSID Web Page Redirect Types & Secret
//----------------------------------------------------------------
#define WPR_SSID                        0
#define WPR_GROUP                       1

#define WPR_EXTERNAL                    0
#define WPR_INTERNAL                    1
#define WPR_CLOUD                       2

#define WPR_SPLASH                      0
#define WPR_LOGIN                       1
#define WPR_NONE                        2
#define WPR_HOTEL                       3
#define WPR_MDM                         4   // internal use only ('uamscreen 4' in wprd.conf)
#define WPR_CLOUD_GAP                   5   // internal use only ('uamscreen 5' in wprd.conf)
#define WPR_CLOUD_UPSK                  6
#define WPR_LANDPAGE                    7
#define IS_WPR_LOGIN(s)                 ((s) == WPR_LOGIN || (s) == WPR_LANDPAGE || (s) == WPR_HOTEL || (s) == WPR_SPLASH)

#define WPR_CHAP                        0
#define WPR_PAP                         1
#define WPR_MS_CHAP                     2

#define WPR_NET_IP_ADDR                 "185.0.0.0"
#define WPR_NET_IP_MASK                 "255.255.0.0"
#define WPR_AUTH_IP_ADDR                "185.0.0.1"
#define WPR_INT_SECRET                  "t8ma4qci0gbfr3va"
#define WPR_TCP_PORT                    10000
#define WPR_MAX_UNAME_LEN               (64 + 1)
#define WPR_CHAP_PASSWD_LEN             17
#define WPR_CHAP_CHAL_LEN               16
#define WPR_MS_CHAP_CHAL_LEN            8
#define WPR_MS_CHAP_NT_RESP_LEN         24
#define WPR_AUTH_TIMEOUT                120
#define WPR_HTTPS_PASSTHRU_DISABLED     0
#define WPR_HTTPS_PASSTHRU_ENABLED      1

// not used in cfg anymore, included here to build mace
#define WPR_INT_URL                     "https://\\$IPADDR/cgi-bin/hotspotlogin.cgi"
#define WPR_BASE_TCP_PORT               10000
#define WPR_SPLASH_USERNAME             "guest"
#define WPR_SPLASH_PASSWORD             "guest"

#define WPR_HTTPS_PASSTHRU_OFF          0
#define WPR_HTTPS_PASSTHRU_ON           1
#define WPR_HTTPS_PASSTHRU_BLOCK        2
//****************************************************************
// Radius
//----------------------------------------------------------------
#define RAD_SERVER_REMOTE               1
#define RAD_SERVER_LOCAL                2
#define RAD_SERVER_AD                   3

#define RAD_CALLED_STA_ID_BSSID_SSID    0
#define RAD_CALLED_STA_ID_BSSID         1
#define RAD_CALLED_STA_ID_ETHERNET_MAC  2

#define RAD_STA_MAC_LOWER               0
#define RAD_STA_MAC_UPPER               1
#define RAD_STA_MAC_LOWER_HYPHEN        2
#define RAD_STA_MAC_UPPER_HYPHEN        3

#define RAD_STA_MULTI_SESSION_ID_LEN    59+1 // ascii: ap mac (6x2), station mac (6x2), 64-bit (8x2) timestamp, hyphen separators (19)
#define RAD_CLS_ATTR_COMPOSITE_LEN      512  // RADIUS CLASS ATTRIBUTE COMBINE LENGTH

//****************************************************************
// (SID_STA_TABLE) Interface selection
//----------------------------------------------------------------
#define ALL                            -1
#define BY_IFACE                       -2
#define BY_SSID                        -3
#define BY_GROUP                       -4

//****************************************************************
// (SID_ROAM_STA_TABLE)  Type selection
//----------------------------------------------------------------
#define ROAM_FROM_ARRAY                 1
#define ROAM_TO_ARRAY                   2

//****************************************************************
// SID_UNASSOC_STA_TABLE option flags
//----------------------------------------------------------------
#define UNASSOC_EXCLUDE_XIRRUS_STA      1
#define UNASSOC_PRIOR_ASSOC_STAS        2
#define UNASSOC_INCLUDE_ALL_STA         4
#define UNASSOC_SHOW_OPERATING_MODE     8
#define UNASSOC_INCLUDE_ACTIVE_ONLY    16
#define UNASSOC_RSSI_DEDUP_NO_ASSOC    32
#define UNASSOC_EXCLUDE_RANDOM_ADDR    64
#define UNASSOC_INCLUDE_RANDOM_ONLY   128
#define UNASSOC_EXCLUDE_ROGUE_APS     256

//****************************************************************
// SID_STA_TYPE_(CNTS/TABLE/TOTAL) option flags
//----------------------------------------------------------------
#define STA_TYPE_EXCLUDE_XIRRUS_STA     1
#define STA_TYPE_INCLUDE_ASSOC_STA      2
#define STA_TYPE_INCLUDE_UNASSOC_STA    4
#define STA_TYPE_SHOW_OPERATING_MODE    8
#define STA_TYPE_INCLUDE_ACTIVE_ONLY   16
#define STA_TYPE_RSSI_DEDUP_NO_ASSOC   32
#define STA_TYPE_EXCLUDE_RANDOM_ADDR   64
#define STA_TYPE_INCLUDE_RANDOM_ONLY  128
#define STA_TYPE_EXCLUDE_ROGUE_APS    256

//****************************************************************
// SID_NEIGHBOR_(STA/RAP) option flags
//----------------------------------------------------------------
#define SHARED_INCLUDE_RANDOM_ADDR      1
#define SHARED_INCLUDE_ASSOC_ONLY       2
#define SHARED_INCLUDE_UNASSOC_ONLY     4

//****************************************************************
// Radio selection
//----------------------------------------------------------------
// NOTE: these constants have different meanings for data requests
// vs. action requests
//
// for a data request, RADIO_ALL and RADIO_A both return data for all
// radios, while RADIO_11G returns data just for abg radios, regardless
// of what band they're currently programmed to operate in
//
// for an action request, RADIO_ALL operates on all radios, RADIO_11A
// operates on all a_only radios and abg radios currently in a mode,
// and RADIO_11G operates on abg radios currently in bg mode.
//
// Note: sometimes an action request in cfg gets translated to a
// data request from OOPME, and the above still holds true. fun, eh?
#define RADIO_ALL                      -1
#define RADIO_11A                      -2
#define RADIO_11G                      -3
#define RADIO_NO_WDS                   -4

//****************************************************************
// WiFi mode flags
//----------------------------------------------------------------
#define MODE_DOT11A                   0x01
#define MODE_DOT11B                   0x02
#define MODE_DOT11G                   0x04
#define MODE_DOT11N                   0x08
#define MODE_DOT11AC                  0x10
#define MODE_DOT11A_ONLY              (MODE_DOT11A                                                         )
#define MODE_DOT11B_ONLY              (              MODE_DOT11B                                           )
#define MODE_DOT11G_ONLY              (                            MODE_DOT11G                             )
#define MODE_DOT11N_ONLY              (                                          MODE_DOT11N               )
#define MODE_DOT11AC_ONLY             (                                                        MODE_DOT11AC)
#define MODE_DOT11BG                  (              MODE_DOT11B + MODE_DOT11G                             )
#define MODE_DOT11GN                  (                            MODE_DOT11G + MODE_DOT11N               )
#define MODE_DOT11AG                  (MODE_DOT11A +               MODE_DOT11G                             )
#define MODE_DOT11AN                  (MODE_DOT11A +                             MODE_DOT11N               )
#define MODE_DOT11ABG                 (MODE_DOT11A + MODE_DOT11B + MODE_DOT11G                             )
#define MODE_DOT11BGN                 (              MODE_DOT11B + MODE_DOT11G + MODE_DOT11N               )
#define MODE_DOT11ABGN                (MODE_DOT11A + MODE_DOT11B + MODE_DOT11G + MODE_DOT11N               )
#define MODE_DOT11NAC                 (                                          MODE_DOT11N + MODE_DOT11AC)
#define MODE_DOT11ANAC                (MODE_DOT11A +                             MODE_DOT11N + MODE_DOT11AC)
#define MODE_DOT11ABGNAC              (MODE_DOT11A + MODE_DOT11B + MODE_DOT11G + MODE_DOT11N + MODE_DOT11AC)

#define dot11a_enabled(n)             ((n) &  MODE_DOT11A )
#define dot11b_enabled(n)             ((n) &  MODE_DOT11B )
#define dot11g_enabled(n)             ((n) &  MODE_DOT11G )
#define dot11n_enabled(n)             ((n) & (MODE_DOT11N | MODE_DOT11AC))  // 11ac implies 11n
#define dot11bg_enabled(n)            ((n) &  MODE_DOT11BG)
#define dot11ac_enabled(n)            ((n) &  MODE_DOT11AC)

#define dot11a_only(n)                ((n) == MODE_DOT11A_ONLY)
#define dot11b_only(n)                ((n) == MODE_DOT11B_ONLY)
#define dot11g_only(n)                ((n) == MODE_DOT11G_ONLY)
#define dot11n_only(n)                ((n) == MODE_DOT11N_ONLY)
#define dot11ac_only(n)               ((n) == MODE_DOT11AC_ONLY)

//****************************************************************
// Rate counts
//----------------------------------------------------------------
#define MAX_NUM_CCK_RATES               4
#define MAX_NUM_OFDM_RATES              8
#define MAX_NUM_MCS_STREAM_RATES       10

//****************************************************************
// (SID_STATS_TIME_PERIOD) Stats type selection
//----------------------------------------------------------------
#define STATS_TYPE_ETH                  1
#define STATS_TYPE_RADIO                2
#define STATS_TYPE_VLAN                 3

//****************************************************************
// Antenna
//----------------------------------------------------------------
#define ANT_OMNI                        0
#define ANT_EXTERNAL                    0
#define ANT_INTERNAL                    1

//****************************************************************
// Media Type Masks
//----------------------------------------------------------------
#define MEDIA_B                         0x01
#define MEDIA_G                         0x02
#define MEDIA_A                         0x04
#define MEDIA_N                         0x08
#define MEDIA_AC                        0x10

#define MEDIA_AB                        (MEDIA_A + MEDIA_B                               )
#define MEDIA_AG                        (MEDIA_A           + MEDIA_G                     )
#define MEDIA_BG                        (          MEDIA_B + MEDIA_G                     )
#define MEDIA_ABG                       (MEDIA_A + MEDIA_B + MEDIA_G                     )
#define MEDIA_AN                        (MEDIA_A                     + MEDIA_N           )
#define MEDIA_BN                        (          MEDIA_B           + MEDIA_N           )
#define MEDIA_GN                        (                    MEDIA_G + MEDIA_N           )
#define MEDIA_ABN                       (MEDIA_A + MEDIA_B           + MEDIA_N           )
#define MEDIA_AGN                       (MEDIA_A +           MEDIA_G + MEDIA_N           )
#define MEDIA_BGN                       (        + MEDIA_B + MEDIA_G + MEDIA_N           )
#define MEDIA_ABGN                      (MEDIA_A + MEDIA_B + MEDIA_G + MEDIA_N           )
#define MEDIA_ABGNAC                    (MEDIA_A + MEDIA_B + MEDIA_G + MEDIA_N + MEDIA_AC)
#define MEDIA_ANAC                      (MEDIA_A                     + MEDIA_N + MEDIA_AC)
#define MEDIA_NAC                       (                              MEDIA_N + MEDIA_AC)

//****************************************************************
// TX/RX Stream Type Indexes
//----------------------------------------------------------------
#define TXRX_STREAMS_NONE              0
#define TXRX_STREAMS_1                 0
#define TXRX_STREAMS_2                 1
#define TXRX_STREAMS_3                 2
#define TXRX_STREAMS_4                 3
#define TXRX_STREAMS_MAX               4
#define TXRX_STREAMS(rates)            (((rates) & 0xff000000) ? TXRX_STREAMS_4 :   \
                                        ((rates) & 0x00ff0000) ? TXRX_STREAMS_3 :   \
                                        ((rates) & 0x0000ff00) ? TXRX_STREAMS_2 :   \
                                                                 TXRX_STREAMS_1 )

//****************************************************************
// Rogue commands
//----------------------------------------------------------------
#define ROGUE_RESET                    -1
#define ROGUE_DELETE                    0
#define ROGUE_UNKNOWN                   0
#define ROGUE_KNOWN                     1
#define ROGUE_APPROVED                  2
#define ROGUE_BLOCKED                   3
#define ROGUE_ALL_AP                    4
#define ROGUE_ACTIVE                    8

#define ROGUE_NOT_FOUND                 0
#define ROGUE_FOUND                     1
#define ROGUE_FOUND_WC                  2

#define ROGUE_STATUS_MASK            0x0f
#define ROGUE_MATCH_MASK             0xf0
#define ROGUE_BSSID                  0x10
#define ROGUE_SSID                   0x20
#define ROGUE_MANUFACTURER           0x30

#define MAX_IDS_TABLE_ENTRIES        1500
#define MAX_IDS_SSID_ENTRIES         1500
#define MAX_IDS_EVENT_ENTRIES        1500

#define IDS_OFF                         0
#define IDS_STANDARD                    1
#define IDS_ADVANCED                    2   // NetChem
#define IDS_STANDARD_AUTO_BLOCK         3

#define RF_MON_OFF                      0
#define RF_MON_DEDICATED                1
#define RF_MON_TIMESHARE                2

#define IBSS_ENABLED               0x0200
#define PRIVACY_ENABLED            0x1000

#define DOT11W_OFF                      0
#define DOT11W_CAPABLE                  1
#define DOT11W_REQUIRED                 2

#define DOT11H_OFF                      0
#define DOT11H_ON                       1

#define DOT11K_OFF                      0
#define DOT11K_ON                       1

#define DOT11R_OFF                      0
#define DOT11R_ON                       1

#define DOT11V_OFF                      0
#define DOT11V_ON                       1

//****************************************************************
// Rogue AutoBlock Encryption
//----------------------------------------------------------------
#define IDS_ENC_NONE                    0
#define IDS_ENC_WEP                     1
#define IDS_ENC_ALL                     2

//****************************************************************
// Rogue AutoBlock Type
//----------------------------------------------------------------
#define IDS_NET_TYPE_ALL                0
#define IDS_NET_TYPE_IBSS               1
#define IDS_NET_TYPE_ESS                2

//****************************************************************
// DHCP Pool
//----------------------------------------------------------------
#define DHCP_POOL_NONE                 -1

//****************************************************************
// Defaults
//----------------------------------------------------------------
#define DEF_FRAG_THRESHOLD           2346
#define DEF_RTS_THRESHOLD            2347
#define DEFAULT_SSID_NAME         "xirrus"
#define DOT11A_MIN_CHANNEL             36
#define DOT11A_DEF_CHANNEL             36
#define DOT11A_MAX_CHANNEL            165
#define DOT11G_MIN_CHANNEL              1
#define DOT11G_DEF_CHANNEL              1
#define DOT11G_MAX_CHANNEL             14

//****************************************************************
// Radio modes
//----------------------------------------------------------------
#define BAND_DOT11A                     0
#define BAND_DOT11G                     1
#define BAND_MONITOR                    2
#define BAND_MONITOR_DEDICATED          2
#define BAND_MONITOR_TIMESHARE          3
#define CHAN_MONITOR                    0
#define CHAN_MONITOR_DEDICATED          0
#define CHAN_MONITOR_TIMESHARE         -1

//****************************************************************
// Dot11n modes
//----------------------------------------------------------------
#define DOT11N_CHAN_BOND_LOWER         -1
#define DOT11N_CHAN_BOND_NONE           0
#define DOT11N_CHAN_BOND_UPPER          1

#define DOT11N_CHAN_BOND_MODE_OFF      -1
#define DOT11N_CHAN_BOND_DYNAMIC        0
#define DOT11N_CHAN_BOND_STATIC         1

//****************************************************************
// Dot11ac modes
//----------------------------------------------------------------
#define DOT11AC_CHAN_BOND_LOWER        -1
#define DOT11AC_CHAN_BOND_NONE          0
#define DOT11AC_CHAN_BOND_UPPER         1

//****************************************************************
// WDS defines
//----------------------------------------------------------------
#define WDS_ALLOW_STATIONS_DISABLED     0
#define WDS_ALLOW_STATIONS_ENABLED      1
#define MAX_NUM_WDS_CLIENT_LINKS        4   // The maximum number of WDS client links we support
#define MAX_NUM_WDS_HOST_LINKS          4   // The maximum number of WDS host links we support
#define MAX_NUM_WDS_IAPS_PER_LINK       3
#define WDS_ALL                        -1

#define wds_client(l)                   ((l <= MAX_NUM_WDS_CLIENT_LINKS) ?  l                             : 0)
#define wds_host(l)                     ((l >  MAX_NUM_WDS_CLIENT_LINKS) ? (l - MAX_NUM_WDS_CLIENT_LINKS) : 0)

//---------------------------------------------------------
// WDS index codes
//---------------------------------------------------------
#define WDS_IDX_LINK_ALL                0
#define WDS_IDX_CLNT_LINK_ALL           1
#define WDS_IDX_HOST_LINK_ALL           2

#define WDS_IDX_CLNT_LINK_BASE          2
#define WDS_IDX_CLNT_LINK_1             3
#define WDS_IDX_CLNT_LINK_2             4
#define WDS_IDX_CLNT_LINK_3             5
#define WDS_IDX_CLNT_LINK_4             6

#define WDS_IDX_HOST_LINK_BASE          6
#define WDS_IDX_HOST_LINK_1             7
#define WDS_IDX_HOST_LINK_2             8
#define WDS_IDX_HOST_LINK_3             9
#define WDS_IDX_HOST_LINK_4            10

//---------------------------------------------------------
// WDS Return codes
//---------------------------------------------------------
#define WDS_ERR_SUCCESS                 0
#define WDS_ERR_GENERAL                -1   // Something went wrong in the pipes
#define WDS_ERR_BAD_LINK               -2   // The WDS link index is out of range (1-4)
#define WDS_ERR_NO_SSID                -3   // Cannot enable WDS without SSID
#define WDS_ERR_NO_TARGET              -4   // Cannot enable WDS without Target BSSID
#define WDS_ERR_BAD_SSID               -5   // SSID is empty or unacceptable
#define WDS_ERR_BAD_TARGET             -6   // Target BSSID is empty or unacceptable
#define WDS_ERR_NO_MAX_IAPS            -7   // Max IAP count is invalid
#define WDS_ERR_NO_IAPS                -8   // No IAPs assigned to this WDS link
#define WDS_ERR_BAD_MAX_IAPS           -9   // Max IAPs < assigned IAPs
#define WDS_ERR_BAD_TARGET_OUI        -10   // Target BSSID oui does not match ours
#define WDS_ERR_BAD_TARGET_LSB        -11   // Target BSSID least significant byte invalid

//****************************************************************
// Channels
//----------------------------------------------------------------
#define MAX_BG_CHANNELS                15
#define MAX_BG_AUTO_SET                5
#define MAX_A_CHANNELS                 35
#define MAX_NUM_CHANNELS              (MAX_A_CHANNELS+MAX_BG_CHANNELS)
#define MAX_ALL_CHANNELS               256

#define AUTO_CH_CFG_PERIOD_11A         25
#define AUTO_CH_CFG_PERIOD_11G         10

//****************************************************************
// Deauth command
//----------------------------------------------------------------
#define DEAUTH_ONE_STA                  0
#define DEAUTH_ALL_STA                  1

//****************************************************************
// LED masks
//----------------------------------------------------------------
#define LED_BLINK_ON_BEACON_DONE        0x01
#define LED_BLINK_ON_DATA_TX_DONE       0x03
#define LED_BLINK_ON_MGMT_TX_DONE       0x05
#define LED_BLINK_ON_DATA_RX_DONE       0x07
#define LED_BLINK_ON_MGMT_RX            0x08
#define LED_BLINK_ON_BROADCAST_TX       0x09
#define LED_BLINK_ON_PROBE_REQ_RX       0x0a
#define LED_BLINK_HEARTBEAT_ON_ASSOC    0x0b
#define LED_BLINK_ON_ONLY_IF_ASSOC      0x0c
#define LED_BLINK_FORCE_ALL_OFF         0x0d

#define DEFAULT_LED_BLINK_MGMT_MASK     BIT(LED_BLINK_HEARTBEAT_ON_ASSOC)
#define DEFAULT_LED_BLINK_TX_MASK       BIT(LED_BLINK_ON_DATA_TX_DONE) | BIT(LED_BLINK_ON_MGMT_TX_DONE)
#define DEFAULT_LED_BLINK_RX_MASK       BIT(LED_BLINK_ON_DATA_RX_DONE) | BIT(LED_BLINK_ON_MGMT_RX)
#define DEFAULT_LED_BLINK_ON_MASK      (DEFAULT_LED_BLINK_MGMT_MASK    | DEFAULT_LED_BLINK_TX_MASK    | DEFAULT_LED_BLINK_RX_MASK)

//****************************************************************
// Maximum of Maximums (MOMs)
//----------------------------------------------------------------
#define MOM_NUM_RADIO_INTERFACES       (MOM_IAPS)               // from xirver.h
#define MOM_NUM_RADIO_MODULES          (MOM_IAP_BOARDS)         // from xirver.h

//****************************************************************
// Maximums
//----------------------------------------------------------------
#endif /* not defined _XIRDHCP_H */
#define MAX_INTERFACE_BASENAME         20
#define MAX_NUM_RADIO_INTERFACES       (MAX_IAPS)               // from xirver.h
#define MAX_NUM_RADIO_MODULES          (MAX_IAP_BOARDS)         // from xirver.h
#define MAX_NUM_STA_PER_RADIO          (MOM_ASSOC_PER_RADIO)    // from xirmac.h
// need to keep in sync: sandbox/snmp-ageng/mibs/XIRRUS-MIB.txt: globalIAPMaxStations
#define MAX_NUM_STATIONS               (MAX_ASSOC_PER_ARRAY)    // from xirmac.h
// need to keep in sync: sandbox/snmp-ageng/mibs/XIRRUS-MIB.txt: globalMaxStations, ssidLimitsStationLimit, groupStationLimit
#define MAX_NUM_ETH_BONDS              (MAX_NUM_ETH_INTERFACES)
#define MOM_NUM_ETH_BONDS              (MOM_NUM_ETH_INTERFACES)
#ifndef _XIRDHCP_H

#if     !defined(DHCP_CLIENT) && !defined(XIR_SSH_DAEMON)
#define MAXHOSTNAMELEN               (256 + 1)  // not POSIX compliant (255 + 1), but breaks cfg read/write if changed
#endif
#define MAX_SIZE_GLB_CFG_FILE     2000000
#define MAX_SIZE_GLB_CFG_EEPROM    (65536 - 8192)
#define MAX_SIZE_UBOOT_ENV          (4096 - 4)
#define MAX_SIZE_FILE              200000
#define MAX_SIZE_LOG_FILE            2000
#define MAX_SIZE_ACL                10000
#define MAX_NUM_ADMIN                  32
#define MAX_NUM_RADIUS_USERS         1000
#define AVG_SIZE_RADIUS_USER          100   // average length of user name + password  (restriction now removed)
#define MAX_SIZE_RADIUS_USERS          (MAX_NUM_RADIUS_USERS  * AVG_SIZE_RADIUS_USER)
#define MAX_NUM_SESSIONS               64
#define MAX_NUM_DHCP_POOL              16
#define MAX_NUM_XRP_TARGETS            50
#define MAX_NUM_FILTERS                50
#define MAX_NUM_CDP_ENTRIES            50
#define MAX_NUM_PRIV_LEVELS             8
#define MAX_PRIV_LEVEL                (MAX_NUM_PRIV_LEVELS - 1)
#define MAX_NUM_FILTER_LISTS          (16 + 1)
#define MAX_NUM_APP_LISTS              15
#define MAX_NUM_APPS_PER_LIST         250
#define MAX_NUM_QUICK_CONFIGS          16
#define MAX_NUM_UBOOT_ENV_VARS        256
#define MAX_NUM_FLASH_FILES          1024
#define MAX_NUM_NEIGHBORS            2500
#define MAX_NUM_SECTIONS               64
#define MAX_NUM_CLUSTERS               16
#define MAX_NUM_CLUSTER_ARRAYS         50
#define MAX_READ_STRUCT_BUFFERS        16
#define MAX_WIFI_TAG_CHANNELS           3
#define MAX_SA_CHANNELS                38
#define MAX_REKEY              1000000000
#define MAX_NUM_NA_TESTS              300
#define MAX_NUM_DBG_ENTRIES           256
#define MAX_LOGIN_ATTEMPTS              3
#define MAX_PROBE_REQ_AGE             (10 * 60 * 1000) //10 minutes
#define MAX_NUM_DEVICE_ID_TOKENS     1024
#define MAX_NUM_DEV_OUI_ENTRIES        50
#define MAX_NUM_DEV_MAC_ENTRIES      1000
#define MAX_NUM_ROAM_CONSORTIUM_OIS    50
#define MAX_NUM_DOMAINS                50
#define MAX_NUM_CELL_NETWORKS          50
#define MAX_NUM_NAI_REALMS             50
#define MAX_NUM_EAP_METHODS            5
#define MAX_NUM_AUTH_PARAMS            5
#define MAX_NUM_NETWORK_AUTH_TYPES     4
#define MAX_NUM_CONN_CAPS              50
#define MAX_NUM_OPER_NAMES             2
#define MAX_NUM_VENUE_NAMES            50
#define MAX_NUM_OAUTH_TOKENS           32
#define MAX_NUM_AUTOBLOCK_WLIST_CHANS  32
#define MAX_NUM_XRP_TUNNELS          1000
#define MAX_NUM_TUNNELS                50
#define MAX_NUM_FIXED_ADDRS           250
#define MAX_NUM_IPV6_IFACE_ADDRS        8
#define MAX_NUM_LOCATION_APS           12
#define MAX_FT_DATA_LEN               512

//****************************************************************
// Cellsize
//----------------------------------------------------------------
#define CFG_SHARP_CELL

#define CELL_MANUAL                     0
#define CELL_SMALL                      1
#define CELL_MEDIUM                     2
#define CELL_LARGE                      3
#define CELL_MAX                        4
#define CELL_AUTO                       5
#define CELL_MONITOR                    6

//****************************************************************
// Channel mode
//----------------------------------------------------------------
#define CHAN_MODE_UNLOCK                0
#define CHAN_MODE_DEFAULT               0
#define CHAN_MODE_MANUAL                1
#define CHAN_MODE_AUTO                  2
#define CHAN_MODE_RADAR                 3
#define CHAN_MODE_LOCKED                4
#define CHAN_MODE_MONITOR_DEDICATED     5
#define CHAN_MODE_MONITOR_TIMESHARE     6

//****************************************************************
// Radar timing definitions
//----------------------------------------------------------------
#define RADAR_DETECT_MINUTES           30

//****************************************************************
// IAP up pending definitions
//----------------------------------------------------------------
#define PENDING_NOTHING                 0
#define PENDING_POWER                   1
#define PENDING_CFG_CHANGE              2
#define PENDING_GEO_CHANGE              3
#define PENDING_DFS_RECOVERY            4
#define PENDING_AUTO_CHANNEL            5
#define PENDING_CHANNEL_RESET           6

//****************************************************************
// Autochannel active states
//----------------------------------------------------------------
#define AUTOCHANNEL_INACTIVE            0
#define AUTOCHANNEL_REQUEST             1
#define AUTOCHANNEL_ACQUIRED            2
#define AUTOCHANNEL_ACTIVE              3
#define AUTOCHANNEL_NEGOTIATED          4
#define AUTOCHANNEL_SCANNING            5
#define AUTOCHANNEL_SEARCHING           6
#define AUTOCHANNEL_COMPLETE            7

//****************************************************************
// Autochannel negotiation completion states
//----------------------------------------------------------------
#define AUTOCHAN_NEG_RUNNING            0
#define AUTOCHAN_NEG_WAIT_DFS           1
#define AUTOCHAN_NEG_COMPLETE           2
#define AUTOCHAN_NEG_TIMEOUT            3
#define AUTOCHAN_NEG_FAILED             4
#define MAX_AUTOCHAN_NEG_TICKS       2048

//****************************************************************
// Autochannel/negotiation action flags
//----------------------------------------------------------------
#define AUTOCHANNEL_RX_THRESHOLD     -100
#define AUTOCHANNEL_5GHZ                5
#define AUTOCHANNEL_2GHZ                2
#define AUTOCHANNEL_ALL              (5+2)

#define AUTOCHANNEL_BAND             0x07
#define AUTOCHANNEL_IFACE            0x07
#define AUTOCHANNEL_IAPS_DOWN        0x08
#define AUTOCHANNEL_DO_WDS           0x10
#define AUTOCHANNEL_DO_BOND          0x20
#define AUTOCHANNEL_AVOID_RADAR      0x40
#define AUTOCHANNEL_FULL_SCAN        0x80
#define AUTOCHANNEL_ALL_IAPS         0x100

#define AUTOCHANNEL_NO_WDS          !AUTOCHANNEL_DO_WDS
#define AUTOCHANNEL_NO_BOND         !AUTOCHANNEL_DO_BOND
#define AUTOCHANNEL_DO_RADAR        !AUTOCHANNEL_AVOID_RADAR
#define AUTOCHANNEL_NO_SCAN         !AUTOCHANNEL_FULL_SCAN
#define AUTOCHANNEL_ENB_IAPS        !AUTOCHANNEL_ALL_IAPS

//****************************************************************
// Autochannel list edit commands
//----------------------------------------------------------------
#define AUTOCHAN_LIST_CLEAR            -1
#define AUTOCHAN_LIST_ADD               0
#define AUTOCHAN_LIST_RESET             1
#define AUTOCHAN_LIST_USE_ALL           2
#define AUTOCHAN_LIST_USE_ALL_BONDABLE  3
#define AUTOCHAN_LIST_USE_ALL_NONRADAR  4

//****************************************************************
// Load Balance defaults
//----------------------------------------------------------------
#define LOAD_BAL_DFLT_DEBUG             0
#define LOAD_BAL_DFLT_MAX_ATTEMPTS      2
#define LOAD_BAL_DFLT_MIN_PROBE_REQS    0
#define LOAD_BAL_DFLT_WEIGHT_SNR        1
#define LOAD_BAL_DFLT_WEIGHT_BAND      12
#define LOAD_BAL_DFLT_WEIGHT_STREAM     9
#define LOAD_BAL_DFLT_WEIGHT_VHT        6
#define LOAD_BAL_DFLT_WEIGHT_HT         3

//****************************************************************
// WPA encryption types
//----------------------------------------------------------------
#define WPA_KEY_MGMT_EAP            BIT(0)
#define WPA_KEY_MGMT_PSK            BIT(1)
#define WPA_KEY_MGMT_FT_IEEE8021X   BIT(5)
#define WPA_KEY_MGMT_FT_PSK         BIT(6)
#define WPA_KEY_MGMT_FT_SAE         BIT(11)
#define WPA_CIPHER_TKIP             BIT(3)
#define WPA_CIPHER_CCMP             BIT(4)

#define WPA_VERSION_WPA                 1
#define WPA_VERSION_WPA2                2

#define WPA_PSK_OFF                     0
#define WPA_PSK_ON                      1
#define WPA_PSK_UPSK                    2

//****************************************************************
// U-PSK definitions
//----------------------------------------------------------------
#define UPSK_SERVER_CONN_ERROR_BLOCK    0
#define UPSK_SERVER_CONN_ERROR_ALLOW    1

#define UPSK_CACHE_MAX_SIZE             50000

//****************************************************************
// Authentication types
//----------------------------------------------------------------
#define AUTH_OPEN                       0
#define AUTH_RADIUS_MAC                 1
#define AUTH_802_1X                     2

//****************************************************************
// Radius Service-Type values
//----------------------------------------------------------------
#define RAD_SVC_LOGIN_USER              1 // Login-User
#define RAD_SVC_FRAMED_USER             2 // Framed-User
#define RAD_SVC_CALLBACK_LOGIN_USER     3 // Callback-Login-User
#define RAD_SVC_CALLBACK_FRAMED_USER    4 // Callback-Framed-User
#define RAD_SVC_OUTBOUND_USER           5 // Outbound-User
#define RAD_SVC_ADMINISTRATIVE_USER     6 // Administrative-User
#define RAD_SVC_NAS_PROMPT_USER         7 // NAS-Prompt-User
#define RAD_SVC_AUTHENTICATE_ONLY       8 // Authenticate-Only
#define RAD_SVC_CALLBACK_NAS_PROMPT     9 // Callback-NAS-Prompt
#define RAD_SVC_CALL_CHECK             10 // Call-Check
#define RAD_SVC_CALLBACK_ADMIN         11 // Callback-Administrative
#define RAD_SVC_VOICE                  12 // Voice
#define RAD_SVC_FAX                    13 // Fax
#define RAD_SVC_MODEM_RELAY            14 // Modem-Relay
#define RAD_SVC_IAPP_REGISTER          15 // IAPP-Register
#define RAD_SVC_IAPP_AP_CHECK          16 // IAPP-AP-Check
#define RAD_SVC_AUTHORIZE_ONLY         17 // Authorize-Only

//****************************************************************
// Modes for the gig ports
//----------------------------------------------------------------
#define BOND_PORT_MAP_GIG_RELATIVE      0x10000
#define BOND_MODE_LINK_BACKUP           0
#define BOND_MODE_ETH1_ONLY             1   // deprecated (here for backward compatibility)
#define BOND_MODE_ETH2_ONLY             2   // deprecated (here for backward compatibility)
#define BOND_MODE_LOAD_BALANCE          3
#define BOND_MODE_BRIDGE                4
#define BOND_MODE_802_3AD               5
#define BOND_MODE_BROADCAST             6
#define BOND_MODE_MIRROR                7  // deprecated (here for backward compatibility)
#define BOND_MODE_NOT_APPLICABLE      255
#define  ETH_BOND_NOT_APPLICABLE        0

//****************************************************************
// Serial console
//----------------------------------------------------------------
#ifndef _XIRWPRD_H                      // avoid conflict in wprd code
#define BAUDRATES                       300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200
#define DEF_BAUDRATE                    115200
#define PARITY_NONE                     0
#define PARITY_ODD                      1
#define PARITY_EVEN                     2
#endif

//****************************************************************
// show config diff files definitions
//----------------------------------------------------------------
#define DIFF_FILE_CFG                  -1
#define DIFF_STARTUP_CFG                0
#define DIFF_SAVED_CFG                  1
#define DIFF_RUNNING_CFG                2
#define DIFF_FACTORY_CFG                3
#define DIFF_INC_DEFAULTS               4
#define DIFF_STARTUP_CFG_DFLT           (DIFF_STARTUP_CFG + DIFF_INC_DEFAULTS)
#define DIFF_SAVED_CFG_DFLT             (DIFF_SAVED_CFG   + DIFF_INC_DEFAULTS)
#define DIFF_RUNNING_CFG_DFLT           (DIFF_RUNNING_CFG + DIFF_INC_DEFAULTS)
#define DIFF_FACTORY_CFG_DFLT           (DIFF_FACTORY_CFG + DIFF_INC_DEFAULTS)

//****************************************************************
// Enclosure door status
//----------------------------------------------------------------
#define DOOR_CLOSED                     0
#define DOOR_OPENED                     1

//****************************************************************
// Onboard Temperature Sensors
//----------------------------------------------------------------
#define TEMP_THRESHOLD_HI_CPU_BOARD     65
#define TEMP_THRESHOLD_HI_RAD_BOARD     65
#define TEMP_THRESHOLD_LO_CPU_BOARD     -20
#define TEMP_THRESHOLD_LO_RAD_BOARD     -20

//****************************************************************
// Port definitions
//----------------------------------------------------------------
#define UDP_PORT                        0
#define TCP_PORT                        1
#define XCF_UDP_PORT                    (('X' << 8) + 'C')      // 'XC', 0x5843, 22595
#define XRP_UDP_PORT                    (('X' << 8) + 'R')      // 'XR', 0x5852, 22610
#define XIRCON_UDP_PORT                 (('X' << 8) + 'T')      // 'XT', 0x5854, 22612
#define L3R_UDP_PORT                    15000
#define L3R_UDP_COUNT                   3000

//****************************************************************
// Xircon definitions
//----------------------------------------------------------------
#define XIRCON_DISABLED                 0
#define XIRCON_AOS_ENABLED              1
#define XIRCON_BOOT_ENABLED             2
#define XIRCON_ENABLED                  (XIRCON_AOS_ENABLED | XIRCON_BOOT_ENABLED)

//****************************************************************
// EID_SAVE_CUR_CFG return codes
//----------------------------------------------------------------
#define SCC_ERR_SIZE_ACL               -2
#define SCC_ERR_SIZE_RADIUS            -3
#define SCC_ERR_SIZE_SSID              -4
#define SCC_ERR_SIZE_FILTER            -5
#define SCC_ERR_SIZE_VLAN              -6
#define SCC_ERR_SIZE_XRP               -7
#define SCC_ERR_OPENING_CFG_FILE       -8
#define SCC_ERR_WRITE_HDR_SHORT        -9    // write returned less than expected
#define SCC_ERR_WRITE_HDR_SYS          -10   // write returned system error
#define SCC_ERR_WRITE_HDR_ZERO         -11   // write returned 0
#define SCC_ERR_WRITE_BUF_SHORT        -12   // write returned less than expected
#define SCC_ERR_WRITE_BUF_ZERO         -14   // write returned 0
#define SCC_ERR_SIZE_IDS               -15
#define SCC_ERR_SIZE_CFG               -16   // Current config exceeds size of flash

//****************************************************************
// EID_SAVE_IMAGE command codes
//----------------------------------------------------------------
#define SAVE_IMAGE_INTERNAL_DELETE       0   // Internal save delete image
#define SAVE_IMAGE_EXTERNAL_STATUS       1   // External save status
#define SAVE_IMAGE_EXTERNAL_START        2   // External save starting
#define SAVE_IMAGE_EXTERNAL_SUCCESS      3   // External save done ok
#define SAVE_IMAGE_EXTERNAL_FAIL         4   // External save done failed
#define SAVE_IMAGE_INTERNAL_EXECUTE      5   // Internal save update image
#define SAVE_IMAGE_INTERNAL_BACKUP       6   // Internal save backup image
#define SAVE_IMAGE_INTERNAL_CHECK        7   // Internal save check image
#define SAVE_IMAGE_EXTERNAL_DOWNGRADE    8   // External save image downgrade
// short hand
#define IMAGE_DELETE                     SAVE_IMAGE_INTERNAL_DELETE
#define IMAGE_STATUS                     SAVE_IMAGE_EXTERNAL_STATUS
#define IMAGE_START                      SAVE_IMAGE_EXTERNAL_START
#define IMAGE_SUCCESS                    SAVE_IMAGE_EXTERNAL_SUCCESS
#define IMAGE_FAIL                       SAVE_IMAGE_EXTERNAL_FAIL
#define IMAGE_EXECUTE                    SAVE_IMAGE_INTERNAL_EXECUTE
#define IMAGE_BACKUP                     SAVE_IMAGE_INTERNAL_BACKUP
#define IMAGE_CHECK                      SAVE_IMAGE_INTERNAL_CHECK
#define IMAGE_DOWNGRADE                  SAVE_IMAGE_EXTERNAL_DOWNGRADE

#define NUM_ETH_STATISTICS              16

//****************************************************************
// System files shared
//----------------------------------------------------------------
#define USRDEV                              "/dev/usrflash"
#define USRDEV_LOCK                         "/var/lock/flash"
#define USRDEV_TYPE                         HOMEDIR "/storage_device"
#define USRDEV_MODEL                        HOMEDIR "/model"

#define SYSDEV                              "/dev/sysflash"

#define PART_CHK                            HOMEDIR "/part_chk.sh"
#define START_FLASH                         HOMEDIR "/startflash.sh"
#define RESTART_FLASH                       HOMEDIR "/startflash.sh --restart"
#define FORMAT_FLASH                        HOMEDIR "/startflash.sh --format ext4 --reboot"
#define WIPE_FLASH                          HOMEDIR "/startflash.sh --format ext4 --reboot --wipe"
#define FULL_FORMAT_FLASH                   HOMEDIR "/startflash.sh --full"
#define FULL_WIPE_FLASH                     HOMEDIR "/startflash.sh --full --wipe"
#define DOWNGRADE_FIRMWARE                  HOMEDIR "/startflash.sh --downgrade"
#define NEW_FLASH_FLAG                      HOMEDIR "/newusbflash"

#define USRDIR                              "/mnt/flash"
#define SYSDIR                              "/mnt/sys"
#define RUNDIR                              HOMEDIR
#define BINDIR                              "/sbin"
#define USRDIR_WPR                          USRDIR "/wpr"
#define USRDIR_SSH                          USRDIR "/ssh"
#define USRDIR_SSL                          USRDIR "/ssl"
#define USRDIR_WMI                          USRDIR "/wmi"
#define USRDIR_LOCK                         "/var/lock/flash"
#define TMPDIR                              "/tmp"

#define THIS_BOOT_TIMESTAMP                 RUNDIR "/lastboot"
#define LAST_BOOT_TIMESTAMP                 RUNDIR "/lastboot.old"

#define CFG_SOCKET1_PATH                    RUNDIR  "/cfg_sock1"
#define CCFG_SOCKET1_PATH                   RUNDIR  "/ccfg_sock1"
#define XCFG_SOCKET1_PATH                   RUNDIR  "/xcfg_sock1"
#define SYSLOG_CONSOLE_PIPE                 RUNDIR  "/logconsole"
#define XIRCON_PIPE                         "/proc/xircon"

#define ERROR_EXT                           ".err"
#define OUTPUT_EXT                          ".log"
#define CONFIG_EXT                          ".conf"
#define IMAGE_EXT                           ".bin"
#define IMAGE_WILDCARD                      SW_PREFIX "-*" IMAGE_EXT

#define BOOTFILE_ACTIVE_SYMLINK             RUNDIR  "/bootfile_active"      IMAGE_EXT
#define BOOTFILE_BACKUP_SYMLINK             RUNDIR  "/bootfile_backup"      IMAGE_EXT
#define BOOTFILE_TEMP_DIR                   TMPDIR  "/image"
#define BOOTFILE_TFTP_CMD                   TMPDIR  "/bootfile_tftp_cmds"
#define BOOTFILE_TFTP_LOG                   TMPDIR  "/bootfile_tftp_logs"
#define BOOTFILE_MISSING                    TMPDIR  "/boofile_missing"

#define STGDIR                              USRDIR  "/storage"
#define TFTP_DIR                            STGDIR  "/tftp"
#define TFTP_SERVER                         HOMEDIR "/tftp-server.sh"

#define AUXNARD_DIR                         TMPDIR  "/image-extra"
#define AUXNARD_TAR                         TMPDIR  "/image-extra.tar"
#define AUXNARD_GZ                          TMPDIR  "/image-extra.tar.gz"

#define GLOBAL_ARRAY_CFG_FILE_CUR           RUNDIR  "/array_cur"            CONFIG_EXT
#define GLOBAL_ARRAY_CFG_FILE_START         RUNDIR  "/array_start"          CONFIG_EXT
#define GLOBAL_ARRAY_CFG_FILE_START_DFLT    RUNDIR  "/array_start_dflt"     CONFIG_EXT
#define GLOBAL_ARRAY_CFG_FILE_SAVED         RUNDIR  "/array_saved"          CONFIG_EXT
#define GLOBAL_ARRAY_CFG_FILE_SAVED_DFLT    RUNDIR  "/array_saved_dflt"     CONFIG_EXT
#define GLOBAL_ARRAY_CFG_FILE_FACTORY       RUNDIR  "/array_factory"        CONFIG_EXT
#define GLOBAL_ARRAY_CFG_FILE_FACTORY_DFLT  RUNDIR  "/array_factory_dflt"   CONFIG_EXT
#define GLOBAL_ARRAY_CFG_FILE_DIFF          RUNDIR  "/array_diff"           CONFIG_EXT
#define GLOBAL_ARRAY_CFG_FILE_DIFF_TEMP     RUNDIR  "/array_diff_temp"      CONFIG_EXT
#define GLOBAL_ARRAY_CFG_FILE_DIFF_SEC1     RUNDIR  "/array_diff_sec1"      CONFIG_EXT
#define GLOBAL_ARRAY_CFG_FILE_DIFF_SEC2     RUNDIR  "/array_diff_sec2"      CONFIG_EXT

#define GLOBAL_CLUST_CFG_FILE_CUR           RUNDIR  "/cluster_cur"          CONFIG_EXT
#define GLOBAL_CLUST_CFG_FILE_START         RUNDIR  "/cluster_start"        CONFIG_EXT
#define GLOBAL_CLUST_CFG_FILE_START_DFLT    RUNDIR  "/cluster_start_dflt"   CONFIG_EXT
#define GLOBAL_CLUST_CFG_FILE_SAVED         RUNDIR  "/cluster_saved"        CONFIG_EXT
#define GLOBAL_CLUST_CFG_FILE_SAVED_DFLT    RUNDIR  "/cluster_saved_dflt"   CONFIG_EXT
#define GLOBAL_CLUST_CFG_FILE_FACTORY       RUNDIR  "/cluster_factory"      CONFIG_EXT
#define GLOBAL_CLUST_CFG_FILE_FACTORY_DFLT  RUNDIR  "/cluster_factory_dflt" CONFIG_EXT
#define GLOBAL_CLUST_CFG_FILE_DIFF          RUNDIR  "/cluster_diff"         CONFIG_EXT
#define GLOBAL_CLUST_CFG_FILE_DIFF_TEMP     RUNDIR  "/cluster_diff_temp"    CONFIG_EXT
#define GLOBAL_CLUST_CFG_FILE_DIFF_SEC1     RUNDIR  "/cluster_diff_sec1"    CONFIG_EXT
#define GLOBAL_CLUST_CFG_FILE_DIFF_SEC2     RUNDIR  "/cluster_diff_sec2"    CONFIG_EXT
#define GLOBAL_CLUST_CFG_FILE_TEMP          RUNDIR  "/cluster_temp"         CONFIG_EXT
#define GLOBAL_CLUST_CFG_FILE_TEMP2         RUNDIR  "/cluster_temp2"        CONFIG_EXT

#define GLOBAL_ARRAY_CFG_PRI_FILE           "/dev/xapcfg"
#define GLOBAL_ARRAY_CFG_SEC_FILE           USRDIR  "/.config.bin"
#define GLOBAL_ARRAY_CFG_SEC_FILE_TMP       USRDIR  "/.config.tmp"
#define GLOBAL_ARRAY_CFG_COPY_FILE          RUNDIR  "/.config_copy.bin"

#define USRDIR_ARRAY_CFG_FILE_START_DFLT    USRDIR "/lastboot" CONFIG_EXT
#define USRDIR_ARRAY_CFG_FILE_SAVED_DFLT    USRDIR "/saved"    CONFIG_EXT
#define USRDIR_ARRAY_CFG_FILE_FACTORY_DFLT  USRDIR "/factory"  CONFIG_EXT

#define HISTORY_PATH                        USRDIR "/history"

#define REMOTE_BOOT_CFG_FILE                HOMEDIR "/remote_config" CONFIG_EXT
#define REMOTE_BOOT_LOG_FILE                HOMEDIR "/remote_config" OUTPUT_EXT

#define CLI_PRELOGIN_BANNER_FILE            USRDIR  "/prelogin.txt"
#define CLI_POSTLOGIN_BANNER_FILE           USRDIR  "/postlogin.txt"
#define CLI_CMD_FILE                        RUNDIR  "/cli-cmd"
#define CLI_LOG_FILE                        RUNDIR  "/cli-log"
#define CLI_TMP_FILE                        RUNDIR  "/cli-tmp"

#define END_OF_FILE_TOKEN_UC                "<EOF>"
#define END_OF_FILE_TOKEN_LC                "<eof>"

#define WMI_DIR                             "/home/wmi"
#define WMI_BASE_MODEL_FILE                 WMI_DIR "/model.txt"
#define WMI_ICONS_DIR                       WMI_DIR "/htdocs/icons"
#define WMI_UPLOADS_DIR                     WMI_DIR "/data/uploads"
#define WMI_IMPORT_CFG_FILE                 WMI_UPLOADS_DIR "/run_config.txt"
#define WMI_IMPORT_CFG_LOG_FILE             WMI_UPLOADS_DIR "/import_log.txt"
#define WMI_IMPORT_CFG_DONE_FILE            WMI_UPLOADS_DIR "/done.txt"

#define WMI_INIT_SCRIPT                     HOMEDIR "/startwmi.sh"

#define USPACE_DBM_TOOL                     "/usr/bin/uspace_dbm_tool"

#define SNMP_SYSDESC_FILE                   HOMEDIR "/sysdesc"
#define SAVE_IMAGE_DONE_FILE                HOMEDIR "/save_done"

#define ENV_CTRL_VERSION_FILE_1             HOMEDIR "/envctrl.ver"
#define ENV_CTRL_FIRMWARE_FILE_1            HOMEDIR "/envctrl.bin"
#define ENV_CTRL_VERSION_FILE_2             HOMEDIR "/envctrl2.ver"
#define ENV_CTRL_FIRMWARE_FILE_2            HOMEDIR "/envctrl2.bin"
#define ENV_CTRL_VERSION_FILE_3             HOMEDIR "/envctrl3.ver"
#define ENV_CTRL_FIRMWARE_FILE_3            HOMEDIR "/envctrl3.bin"

#define SW_UPDATE_NOTICE_FILE               USRDIR  "/sw-update"
#define DHCP_SERVER_LEASE_FILE              HOMEDIR "/dhcpd.leases"

#define FILENAMES_FILE                      HOMEDIR "/filenames.txt"
#define IMAGE_NAME_FILE                     HOMEDIR "/image_name.txt"
#define SIGNATURE_NAME_FILE                 HOMEDIR "/signature_name.txt"
#define SCP_TEMP_DIR                        TMPDIR  "/scp-temp"
#define FTP_TEMP_DIR                        TMPDIR  "/ftp-temp"
#define TFTP_TEMP_DIR                       TMPDIR  "/tftp-temp"
#define AWK_TEMP_FILE                       TMPDIR  "/awk-temp"
#define AWK_CMD_FILE                        TMPDIR  "/awk-cmd"
#define AWK_OUT_FILE                        TMPDIR  "/awk-out"
#define WDS_LOOPBACK_DIR                    HOMEDIR "/wds-loopback"

#define EBTABLES_FILE                       HOMEDIR "/ebtables.conf"
#define EBTABLES_OUTPUT                     HOMEDIR "/ebtables.log"
#define EBTABLES_VIAP_L3ROAM_FILE           HOMEDIR "/ebtables.viap-l3roam.conf"
#define EBTABLES_VIAP_L3ROAM_OUTPUT         HOMEDIR "/ebtables.viap-l3roam.log"
#define EBTABLES_VIAP_GRE_FILE              HOMEDIR "/ebtables.viap-gre.conf"
#define EBTABLES_VIAP_GRE_OUTPUT            HOMEDIR "/ebtables.viap-gre.log"
#define EBTABLES_TAP_FILE                   HOMEDIR "/ebtables.tap.conf"
#define EBTABLES_TAP_OUTPUT                 HOMEDIR "/ebtables.tap.log"
#define EBTABLES_ATOMIC_FILTER_FILE         HOMEDIR "/ebtables.filter.atomic"
#define EBTABLES_ATOMIC_BROUTE_FILE         HOMEDIR "/ebtables.broute.atomic"
#define EBTABLES_FILTER_LIST                HOMEDIR "/ebtables-filter.list"
#define EBTABLES_BROUTE_LIST                HOMEDIR "/ebtables-broute.list"
#define EBTABLES_FILTER_OUT                 HOMEDIR "/ebtables-filter.out"
#define EBTABLES_BROUTE_OUT                 HOMEDIR "/ebtables-broute.out"
#define IPTABLES_FILE                       HOMEDIR "/iptables.conf"
#define IPTABLES_OUTPUT                     HOMEDIR "/iptables.log"
#define IPTABLES_FILTER_LIST                HOMEDIR "/iptables-filter.list"
#define IPTABLES_MANGLE_LIST                HOMEDIR "/iptables-mangle.list"
#define IPTABLES_NATTBL_LIST                HOMEDIR "/iptables-nattbl.list"
#define IPTABLES_FILTER_OUT                 HOMEDIR "/iptables-filter.out"
#define IPTABLES_MANGLE_OUT                 HOMEDIR "/iptables-mangle.out"
#define IPTABLES_NATTBL_OUT                 HOMEDIR "/iptables-nattbl.out"
#define IP6TABLES_FILE                      HOMEDIR "/ip6tables.conf"
#define IP6TABLES_OUTPUT                    HOMEDIR "/ip6tables.log"
#define IP6TABLES_FILTER_LIST               HOMEDIR "/ip6tables-filter.list"
#define IP6TABLES_MANGLE_LIST               HOMEDIR "/ip6tables-mangle.list"
#define IP6TABLES_NATTBL_LIST               HOMEDIR "/ip6tables-nattbl.list"
#define IP6TABLES_FILTER_OUT                HOMEDIR "/ip6tables-filter.out"
#define IP6TABLES_MANGLE_OUT                HOMEDIR "/ip6tables-mangle.out"
#define IP6TABLES_NATTBL_OUT                HOMEDIR "/ip6tables-nattbl.out"

#define DIAGNOSTIC_FILE_IN                  HOMEDIR "/diagnostic.conf"
#define DIAGNOSTIC_FILE_OUT                 HOMEDIR "/diagnostic.log"
#define DIAGNOSTIC_FILE_ERR                 HOMEDIR "/diagnostic.err"

#define SELF_TEST_FILE_IN                   HOMEDIR "/selftest"
#define SELF_TEST_FILE_OUT                  TMPDIR  "/selftest.log"
#define SELF_TEST_FILE_ERR                  TMPDIR  "/selftest.err"

#define RESTORE_FILE_IN                     GLOBAL_ARRAY_CFG_FILE_DIFF
#define RESTORE_FILE_OUT                    TMPDIR  "/restore.log"
#define RESTORE_FILE_ERR                    TMPDIR  "/restore.err"

#define QUICK_CONFIG_DIR                    HOMEDIR "/quick"
#define QUICK_CONFIG_LIST                   QUICK_CONFIG_DIR "/list.txt"
#define QUICK_CONFIG_OUT                    QUICK_CONFIG_DIR "/config.log"
#define QUICK_CONFIG_ERR                    QUICK_CONFIG_DIR "/config.err"

#define CDP_SOCKET_PATH                     HOMEDIR "/cdp_sock"             // deprecated (here to make mace happy)
#define CDP_ETH0_SOCKET_PATH                HOMEDIR "/cdp_eth0_sock"
#define CDP_GIG1_SOCKET_PATH                HOMEDIR "/cdp_gig1_sock"
#define CDP_GIG2_SOCKET_PATH                HOMEDIR "/cdp_gig2_sock"
#define CDP_GIG3_SOCKET_PATH                HOMEDIR "/cdp_gig3_sock"
#define CDP_GIG4_SOCKET_PATH                HOMEDIR "/cdp_gig4_sock"
#define CDP_GIG5_SOCKET_PATH                HOMEDIR "/cdp_gig5_sock"
#define CDP_ETH0_SEND_OUTPUT                HOMEDIR "/cdp_eth0_send.out"
#define CDP_GIG1_SEND_OUTPUT                HOMEDIR "/cdp_gig1_send.out"
#define CDP_GIG2_SEND_OUTPUT                HOMEDIR "/cdp_gig2_send.out"
#define CDP_GIG3_SEND_OUTPUT                HOMEDIR "/cdp_gig3_send.out"
#define CDP_GIG4_SEND_OUTPUT                HOMEDIR "/cdp_gig4_send.out"
#define CDP_GIG5_SEND_OUTPUT                HOMEDIR "/cdp_gig5_send.out"

#define LLDPD_CONF                          "/etc/lldpd.conf"
#define LLDPD_FA_CONF                       HOMEDIR "/lldpd_fa.conf"
#define LLDPD_INIT_SCRIPT                   HOMEDIR "/startlldpd.sh"

#define CLI_BIN                             "/bin/xap_cli"

#define SYSLOG_MAIL_PIPE                    RUNDIR  "/logmail"
#define SYSLOG_MAIL_MSG                     RUNDIR  "/logmail-msg"
#define SYSLOG_MAIL_MSG_IN                  RUNDIR  "/logmail-msg.in"
#define SYSLOG_MAIL_MSG_ERR                 RUNDIR  "/logmail-msg.err"
#define SYSLOG_MAIL_TEST                    RUNDIR  "/logmail-test"
#define SYSLOG_MAIL_TEST_IN                 RUNDIR  "/logmail-test.in"
#define SYSLOG_MAIL_TEST_ERR                RUNDIR  "/logmail-test.err"
#define SYSLOG_MAIL_TEST_DONE               RUNDIR  "/logmail-test.done"
#define SYSLOG_MAIL_CONF                    "/usr/etc/msmtprc"

#define ETC_HOSTS_FILE                      "/etc/hosts"
#define DNS_CONF_FILE                       "/etc/resolv.conf"
#define VTUN_CONF_FILE                      "/usr/etc/vtund.conf"
#define VTUN_STAT_FILE                      "/usr/etc/vtund.stat"
#define VTUN_LOG_FILE                       "/var/log/vtund.log"

#define AUTO_BAND_LOG_FILE                  "/var/log/xap_me/autoband.txt"
#define AUTO_CHANNEL_LOG_FILE               "/var/log/xap_me/autochannel.txt"
#define MFG_LOOPBACK_LOG_FILE               "/var/log/xap_me/loopback.txt"

#define RADIUS_PING_IN                      TMPDIR "/radius_ping.in"
#define RADIUS_AUTH_IN                      TMPDIR "/radius_auth.in"

#define PAM_DIRECTORY                       "/etc/pam.d"
#define PAM_SSHCFG_FILE                     "/etc/pam.d/sshd"
#define PAM_RADIUS_FILE                     "/etc/pam_radius_auth.conf"
#define PAM_RADIUS_LIB                      "pam_radius_auth.so"

#define REBOOT_SYSLOG_FILE_COPY             HOMEDIR "/reboot.log"
#define REBOOT_SYSLOG_FILE                  USRDIR  "/reboot.log"
#define RESTART_SYSLOG_FILE_COPY            HOMEDIR "/restart.log"
#define RESTART_SYSLOG_FILE                 USRDIR  "/restart.log"
#define BOOT_SUCCESS_FILE                   USRDIR  "/lastboot"

#define HEALTH_PROCESS_LIST                 HOMEDIR "/xap_health.ini"
#define HEALTH_LOG_SCRIPT                   HOMEDIR "/health.sh"

#define DEFAULT_XMS_HOSTNAME                "Xirrus-XMS"

#define CRASH_DUMP_DEV                      "/dev/xapcrash"
#define CRASH_DUMP_FILE                     HOMEDIR "/xapcrash"

#define TARGET_DIR                          STGDIR     "/target"
#define TARGET_ASSERT_FILE                  TARGET_DIR "/ta"
#define TARGET_ASSERT_ALL_FILE              TARGET_DIR "/ta-all"

#define KMSG_DEV                            "/proc/kmsg"
#define KMSG_FILE                           HOMEDIR "/kmsg"

#define SAVE_DIFF_FILE                      HOMEDIR "/save_diff.log"

#define BLOCK_IP_SCRIPT                     "/usr/bin/block_ip.sh"
#define MGMT_REAUTH_PERIOD_FILE             "/tmp/mgmt_reauth_period"

#define IPV4_DHCPCD_INFO_FILE_PREFIX        "/var/lib/dhcpcd-"
#define IPV4_DHCPCD_INFO_FILE_SUFFIX        ".info"

#define IPV4_DHCPCD_LEASE_FILE_PREFIX       "/var/db/dhcpcd-"
#define IPV4_DHCPCD_LEASE_FILE_SUFFIX       ".lease"
#define IPV6_DHCPCD_LEASE_FILE_PREFIX       "/var/db/dhcpcd-"
#define IPV6_DHCPCD_LEASE_FILE_SUFFIX       ".lease6"

#define IPV4_DHCPCD_LOG_DIR                 "/var/log/dhcpcd4"
#define IPV6_DHCPCD_LOG_DIR                 "/var/log/dhcpcd6"

#define FIRMWARE_DIR                        "/root/modules/firmware"

#define DPI_DIR                             "/home/dpi"
#define DPI_LOG                             DPI_DIR "/dpi.log"
#define DPI_FIFO                            DPI_DIR "/fifo"
#define DPI_OUTPUT                          DPI_DIR "/output"
#define DPI_USER_APP_LIST                   DPI_DIR "/user_app_list"
#define DPI_PLUGINS                         DPI_DIR "/plugins/"
#define DPI_UPDATES_DIR                     USRDIR  "/dpi"

#define VLAN_TABLE                          HOMEDIR "/vlan_table"
#define JSON_MSG_FILE                       HOMEDIR "/location.json"

#define NETFLOW_DIR                         "/home/netflow"

#define KRB_CONFIG_FILE                     "/etc/krb5.conf"
#define SMB_DIR                             "/etc/samba"
#define SMB_CONFIG_FILE                     SMB_DIR "/smb.conf"

#define XML_TEMP_FILE                       TMPDIR "/xml-input"
#define JSON_TEMP_FILE                      TMPDIR "/json-output"

#define ACTIVATION_PID_FILE                 "/var/run/activation.pid"
#define ACTIVATION_STATE_FILE               USRDIR "/.activation"
#define ACTIVATION_INTERVAL_DEFAULT         (1)
#define ACTIVATION_SERVER_XIRRUS            "https://activate.xirrus.com"
#define ACTIVATION_SERVER_AVAYA             "https://activate.cnp.avaya.com"
#define ACTIVATION_SERVER_URL_MAX_LEN       (255 + 1)
#define ACTIVATION_VERSION_DEFAULT          "0.0.0-0000-default"
#define AUTO_DISC_SERVER_XIRRUS             "Xirrus-XMS"
#define AUTO_DISC_SERVER_AVAYA              "Avaya-WOS"



#define PROXY_MGMT_HTTP_ENV                 "/home/config/proxy_client.env"
#define PROXY_MGMT_HTTP_TMP                 "/tmp/proxy_client.env.tmp"
#define PROXY_MGMT_SOCKS_CFG                "/home/config/tsocks.conf"
#define PROXY_MGMT_SOCKS_TMP                "/tmp/tsocks.conf.tmp"

#define TEMP_CPU_FILE                       TMPDIR "/cpu_usage_output"

#define VENDOR_FILE                         HOMEDIR "/vendor"

#define ARCHLOG_DIR                         STGDIR "/archlog"

#define TEMP_ACTIVE_DIR_FILE                TMPDIR "/active_directory_out"

//****************************************************************
// Request length defines
//----------------------------------------------------------------
#define NUM_USER_AGENT_STRS             3
#define REQ_LEN_ACTIVE_DIR_STR        (20 + 1)
#define REQ_LEN_ACTIVE_VLANS_STR    (MAX_NUM_VLAN * REQ_LEN_VLAN_NUM + 1)
#define REQ_LEN_APP_LIST_DESC         (20 + 1)
#define REQ_LEN_AUTOCHAN_TIME_STR    (255 + 1)
#define REQ_LEN_BSSID_STR             (20 + 1)
#define REQ_LEN_CONN_CAP_NAME         (20 + 1)
#define REQ_LEN_COUNTRY_STR            (2 + 1)
#define REQ_LEN_DATE_TIME_STR         (20 + 1)
#define REQ_LEN_DEVICE_LIST_STR     (MAXHOSTNAMELEN)
#define REQ_LEN_DEVICE_STR            (20 + 1)
#define REQ_LEN_DOMAIN_NAME          (255 + 1)
#define REQ_LEN_DPI_STR               (19 + 1)
#define REQ_LEN_DRIVER_VERSION         48
#define REQ_LEN_ENV_VAR_NAME          (20 + 1)
#define REQ_LEN_ENV_VAR_VALUE        (500 + 1)
#define REQ_LEN_FILENAME             (255 + 1)
#define REQ_LEN_FILTER_NAME           (20 + 1)
#define REQ_LEN_GUID_STR               (9 + 1)
#define REQ_LEN_HOSTNAME_STR        (MAXHOSTNAMELEN)
#define REQ_LEN_IBEACON_UUID_STR      (50 + 1)
#define REQ_LEN_IDS_EVENT_STR        (100 + 1)
#define REQ_LEN_IFACE_NAME            (16 + 1)
#define REQ_LEN_IPV4_ADDRSTR          (15 + 1)
#define REQ_LEN_IPV6_ADDRSTR          (39 + 1)
#define REQ_LEN_IP_ADDRSTR          (REQ_LEN_IPV6_ADDRSTR)
#define REQ_LEN_LEASE_TIME            (20 + 1)
#define REQ_LEN_LONG_VERSION          (40 + 1)
#define REQ_LEN_MAC_ADDR_SHORT_STR     12
#define REQ_LEN_MAC_ADDR_STR           17
#define REQ_LEN_MAC_STR             (REQ_LEN_MAC_ADDR_STR + 1)
#define REQ_LEN_OUI_STR                (8 + 1)
#define REQ_LEN_MAP_STR               (40 + 1)
#define REQ_LEN_MFGNAME_STR         (REQ_LEN_STR - REQ_LEN_NETBIOS_STR)
#define REQ_LEN_MIN_WPA_PASSPHRASE      8
#define REQ_LEN_NAI_REALM_STR        (255 + 1)
#define REQ_LEN_NETBIOS_STR           (15 + 1)
#define REQ_LEN_NTP_KEY_STR           (20 + 1)
#define REQ_LEN_OPER_NAME            (252 + 1)
#define REQ_LEN_PORTAL_IFACE_STR      (10 * MAX_INTERFACE_BASENAME + 1)
#define REQ_LEN_PORTAL_NAME           (20 + 1)
#define REQ_LEN_PRIV_ABBR_STR         (10 + 1)
#define REQ_LEN_PRIV_NAME_STR         (20 + 1)
#define REQ_LEN_RAD_SECRET            (64 + 1)
#define REQ_LEN_RADIO_TYPE_STR        (20 + 1)
#define REQ_LEN_ROAM_CONSORTIUM_OI    (30 + 1)
#define REQ_LEN_SNMP_STR              (30 + 1)
#define REQ_LEN_SOFTWARE_VERSION      (60 + 1)
#define REQ_LEN_STP_STATE             (20 + 1)
#define REQ_LEN_STR                   (50 + 1)      // General string length
#define REQ_LEN_SYSLOG_MSG           (400 + 1)
#define REQ_LEN_TIMESTAMP             (24 + 1)
#define REQ_LEN_TUNNEL_NAME           (20 + 1)
#define REQ_LEN_URL_STR             (MAXHOSTNAMELEN + 1)
#define REQ_LEN_USER_AGENT_STR      (MAXHOSTNAMELEN)
#define REQ_LEN_USERNAME_STR        (WPR_MAX_UNAME_LEN)
#define REQ_LEN_VENUE_NAME           (252 + 1)
#define REQ_LEN_VERSION               (20 + 1)
#define REQ_LEN_VLAN_NAME             (20 + 1)
#define REQ_LEN_VLAN_NUM              ( 4 + 1)
#define REQ_LEN_WEP_KEY             (WEP_128BIT_ASCII_KEY_LEN + 1)
#define REQ_LEN_WPA_PASSPHRASE        (64 + 1)
#define REQ_LEN_WPR                   (32 + 1)
#define REQ_LEN_NAS_ID_STR          (REQ_LEN_HOSTNAME_STR)
#define REQ_LEN_CDP_ARRAY             255

//****************************************************************
// Request length defines required by DHCP
//----------------------------------------------------------------
#endif /* not defined _XIRDHCP_H */
#define REQ_LEN_SSID                  (32 + 1)
#define REQ_LEN_GROUP                 (32 + 1)
#define REQ_LEN_DHCP_POOL_NAME        (32 + 1)
#define REQ_LEN_CLUSTER               (32 + 1)
#define REQ_LEN_DHCP_CLASS            (32 + 6 + 1)  // must be large enough to hold an ssid or group, also leave room for "GROUP-" prefix
#define REQ_LEN_ETH_ADDR                6
#ifndef _XIRDHCP_H

//****************************************************************
// ACL List
//----------------------------------------------------------------
#define ACL_LIST_NO                     0
#define ACL_LIST_ALLOW                  1
#define ACL_LIST_DENY                   2
#define ACL_LIST_DENY_INC_BLK_RAPS      3

//****************************************************************
// SSID Flags
//----------------------------------------------------------------
#define SSID_FLAGS_BROADCAST            0x01
#define SSID_FLAGS_BAND_DOT11A_ONLY     0x02
#define SSID_FLAGS_BAND_DOT11BG_ONLY    0x04
#define SSID_FLAGS_BAND_BOTH            0x00
#define SSID_FLAGS_BAND_MASK            0x06

#define SSID_IAPS_ALL                   0xffff
#define SSID_IAPS_NONE                  0x0000

//****************************************************************
// Filter types
//----------------------------------------------------------------
#define FILTER_TYPE_DENY                0x00
#define FILTER_TYPE_ALLOW               0x10
#define FILTER_TYPE_SET_QOS             0x20
#define FILTER_TYPE_SET_VLAN            0x40
#define FILTER_TYPE_SET                 0x60
#define FILTER_TYPE_ALLOW_OR_SET        0x70
#define FILTER_TYPE_DISABLE             0x80
#define FILTER_TYPE_LOG                 0x08
#define FILTER_TYPE_LAYER_2             0x04
#define FILTER_TYPE_CTMARK_SET          0x04    // temporary bit to flag conntrack set-mark target
#define FILTER_TYPE_SET_DSCP            0x02    // when FILTER_TYPE_SET_QOS is 0
#define FILTER_TYPE_SET_QOS_MASK        0x03
#define FILTER_TYPE_SET_MASK            (FILTER_TYPE_SET_VLAN | FILTER_TYPE_SET_QOS | FILTER_TYPE_SET_QOS_MASK | FILTER_TYPE_SET_DSCP)
#define FILTER_TYPE_LAYER_3            !(FILTER_TYPE_LAYER_2)

#define FILTER_ADDR_ANY                 0x00
#define FILTER_ADDR_SSID                0x01
#define FILTER_ADDR_VLAN                0x02
#define FILTER_ADDR_IPV4                0x03
#define FILTER_ADDR_MAC                 0x04
#define FILTER_ADDR_IFACE               0x05
#define FILTER_ADDR_GROUP               0x06
#define FILTER_ADDR_IPV6                0x07
#define FILTER_ADDR_DEVICE              0x08
#define FILTER_ADDR_NOT                 0x80

#define FILTER_IFACE_IAP                0
#define FILTER_IFACE_WDS_CLIENT_1       1
#define FILTER_IFACE_WDS_CLIENT_2       2
#define FILTER_IFACE_WDS_CLIENT_3       3
#define FILTER_IFACE_WDS_CLIENT_4       4
#define FILTER_IFACE_WDS_ALL            5
#define FILTER_IFACE_GIG                6
#define FILTER_IFACE_WDS_HOST_1         7
#define FILTER_IFACE_WDS_HOST_2         8
#define FILTER_IFACE_WDS_HOST_3         9
#define FILTER_IFACE_WDS_HOST_4         10
#define FILTER_IFACE_TUNNEL             11

#define FILTER_WDS_HOST_CLIENT_DELTA    (FILTER_IFACE_WDS_HOST_1 - FILTER_IFACE_WDS_CLIENT_1)

#define FILTER_PROT_ANY                 0
#define FILTER_PROT_ANY_IP              255
#define FILTER_PROT_ARP                 254
#define FILTER_PROT_ARP_REQUEST         253
#define FILTER_PROT_ARP_REPLY           252
#define FILTER_PROT_ICMP                1
#define FILTER_PROT_TCP                 6
#define FILTER_PROT_UDP                 17
#define FILTER_PROT_AH                  51
#define FILTER_PROT_ICMPV6              58
#define FILTER_PROT_SCTP                132

#define FILTER_OUTPUT_COMPLETE          0
#define FILTER_OUTPUT_SUMMARY           1

#define FILTER_SRC                      0
#define FILTER_DST                      1

#define FILTER_LIMIT_NONE               0
#define FILTER_LIMIT_ALL_PPS            1
#define FILTER_LIMIT_ALL_KBPS           2
#define FILTER_LIMIT_STA_PPS            3
#define FILTER_LIMIT_STA_KBPS           4

//****************************************************************
// XRP
//----------------------------------------------------------------
#define XRP_DISABLED                    0
#define XRP_BROADCAST                   1
#define XRP_TUNNEL                      2

#define XRP_CONNECT_TARGET_ONLY         0
#define XRP_CONNECT_IN_RANGE            1
#define XRP_CONNECT_ALL                 2

#define XRP_LAYER_2_ONLY                0
#define XRP_LAYER_2_AND_3               1
#define XRP_LAYER_NONE                  2

#define XRP_TARGET_MAC                  0
#define XRP_TARGET_IP                   1
#define XRP_TARGET_HOST                 2

#define XRP_TUNNEL_CONTROL              0
#define XRP_TUNNEL_DATA_FROM            1
#define XRP_TUNNEL_DATA_TO              2

//****************************************************************
// Tunnel
//----------------------------------------------------------------
#define TUNNEL_REMOTE_PRI               0
#define TUNNEL_REMOTE_SEC               1

#define TUNNEL_TYPE_NONE                0
#define TUNNEL_TYPE_GRE                 1

#define TUNNEL_SSID_ALL                 0xffff
#define TUNNEL_SSID_NONE                0x0000

#define TUNNEL_VLAN_ALL                 0xffff
#define TUNNEL_VLAN_NONE                0x0000

//****************************************************************
// Autocell
//----------------------------------------------------------------
#define AUTOCELL_RET_RSSI_DATA          0
#define AUTOCELL_EXT_RSSI_DATA          1

#define AUTOCELL_RET_LOUDEST_DATA       2
#define AUTOCELL_EXT_LOUDEST_DATA       3

#define AUTOCELL_BY_ARRAY               0
#define AUTOCELL_BY_CHANNEL             1

//****************************************************************
// ARP filter
//----------------------------------------------------------------
#define ARP_FILTER_OFF                  0
#define ARP_FILTER_PASS_THRU            1
#define ARP_FILTER_PROXY                2

//****************************************************************
// Environmental Controller
//----------------------------------------------------------------
#define ENV_CTRL_OVER_TEMP              0
#define ENV_CTRL_UNDER_TEMP             1
#define ENV_CTRL_OVER_HUMID             2
#define ENV_CTRL_FAN_FAIL               3

//****************************************************************
// Hostap defines (belongs in hostap.h, along with struct below)
//----------------------------------------------------------------
#define HMSG_STA_AUTH                   1   // oopme   -> hostapd
#define HMSG_STA_ASSOC                  2   // oopme   -> hostapd & oopme -> wprd
#define HMSG_STA_DEAUTH                 3   // oopme   -> hostapd
#define HMSG_STA_DISASSOC               4   // oopme   -> hostapd
#define HMSG_STA_8021X_AUTH             5   // hostapd -> oopme
#define HMSG_STA_8021X_PREAUTH          6   // oopme   -> hostapd
#define HMSG_STA_8021X_PREAUTH_DEL      7   // oopme   -> hostapd
#define HMSG_STA_RADIUS_MAC_AUTH        8   // hostapd -> oopme
#define HMSG_STA_WPR_AUTH               9   // wprd    -> oopme
#define HMSG_STA_VLAN_SET               10  // hostapd -> oopme
#define HMSG_STA_FILTER_ID_SET          11  // hostapd -> oopme & wprd -> oopme
#define HMSG_STA_ASSOC_OPEN             12  // oopme   -> hostapd
#define HMSG_STA_ACCT_START             13  // oopme   -> hostapd
#define HMSG_STA_MDM                    14  // wprd    -> oopme
#define HMSG_WPA_MIC_FAILURE            15  // oopme <--> hostapd
#define HMSG_WPA_INSTALL_GTK            16  // hostapd -> oopme
#define HMSG_WPR_PASSTHRU_IPS_SET       17  // ????    -> wprd    (who sends this?)
#define HMSG_STA_CLOUD_AUTH_STATUS      18  // ????    -> ????    (deprecated?)
#define HMSG_STA_MULTI_SESSION_ID       19  // oopme <--> hostapd
#define HMSG_STA_UPSK_REQUEST           20  // hostapd -> oopme
#define HMSG_STA_UPSK_RESPONSE          21  // oopme   -> hostapd
#define HMSG_WPA_UPSK_CACHE_ADD         22  // oopme   -> hostapd
#define HMSG_WPA_UPSK_CACHE_DEL         23  // oopme   -> hostapd
#define HMSG_STA_CLEAR_CACHE            24  // oopme   -> hostapd
#define HMSG_STA_WPR_ROAM               25  // oopme   -> hostapd
#define HMSG_STA_WPR_SESSION_TIMEOUT    26  // oopme   -> hostapd
#define HMSG_STA_FT_AUTH                27  // oopme <--> hostapd
#define HMSG_STA_ASSOC_RESP_FT_IE       28  // oopme <--> hostapd
#define HMSG_FT_MSG                     29  // oopme <--> hostapd
#define HMSG_FT_INSTALL_PTK             30  // ????    -> hostapd (who sends this?)
#define HMSG_STA_SOFT_DEAUTH            31  // oopme   -> wprd
#define HMSG_WPA_INSTALL_PTK            32  // hostapd -> oopme
#define HMSG_STA_EAPOL_TX               33  // hostapd -> oopme
#define HMSG_STA_MDM_AUTH_CHECK         34  // hostapd -> oopme
#define HMSG_MAX                        35  // maximum message type value

#define AUTH_FAIL                       0
#define AUTH_SUCCESS                    1

#define WPR_AUTH_FAIL                   0
#define WPR_AUTH_SUCCESS                1
#define WPR_AUTH_LOGOUT                 2

#define GROUP_SET_NONE                  0
#define GROUP_SET_802_1X                1
#define GROUP_SET_RADIUS_MAC            2
#define GROUP_SET_WPR                   3
#define GROUP_SET_COA                   4

//****************************************************************
// Syslog definitions
//----------------------------------------------------------------
#define NUM_SYSLOG_PRI                  8
#define STA_SYSLOG_STANDARD             0
#define STA_SYSLOG_KEY_VALUE            1
#define RFC3164                         0
#define RFC3339                         1

//****************************************************************
// SNMP definitions
//----------------------------------------------------------------
#define NUM_SNMP_TRAP_HOSTS             4
#define SNMP_V3_AUTH_MD5                0
#define SNMP_V3_AUTH_SHA                1
#define SNMP_V3_PRIV_DES                0
#define SNMP_V3_PRIV_AES                1

//****************************************************************
// NTP definitions
//----------------------------------------------------------------
#define NTP_AUTH_NONE                   0
#define NTP_AUTH_MD5                    1
#define NTP_AUTH_SHA1                   2

//****************************************************************
// Radio Assurance Test Modes
//----------------------------------------------------------------
#define ASSURANCE_OFF                   0
#define ASSURANCE_ALERT_ONLY            1
#define ASSURANCE_REPAIR_WO_REBOOT      2
#define ASSURANCE_REBOOT_ALLOWED        3

//****************************************************************
// FIPS defaults
//----------------------------------------------------------------
#define FIPS_DEFAULT_WPA_PASSPHRASE     "xirrus123"
#define FIPS_DEFAULT_WPA_PSK            "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"

//****************************************************************
// Admin interface definitions
//----------------------------------------------------------------
#define ADMIN_CLI_CONSOLE               0
#define ADMIN_CLI_TELNET                1
#define ADMIN_CLI_SSH                   2
#define ADMIN_WMI_HTTPS                 3
#define ADMIN_IFACE_MASK             0x0f
#define ADMIN_FORCE_VALID            0x10

//****************************************************************
// Admin interface definitions
//----------------------------------------------------------------
#define ADMIN_CHAP                      0
#define ADMIN_PAP                       1
#define ADMIN_MS_CHAP                   2
#define ADMIN_PEAP                      3

//****************************************************************
// Device ID source definitions
//----------------------------------------------------------------
#define DEVICE_ID_NOT_DISCOVERED        0
#define DEVICE_ID_FROM_MANUFACTURER     1
#define DEVICE_ID_FROM_NETBIOS_NAME     2
#define DEVICE_ID_FROM_DHCP_HOSTNAME    3
#define DEVICE_ID_FROM_USER_AGENT_STR   4
#define DEVICE_ID_FROM_INTERNAL_RULES   5
#define DEVICE_ID_FROM_EXTERNAL_RULES   6
#define DEVICE_ID_FROM_CACHE            7
#define DEVICE_ID_FROM_USER_MAC_TABLE   8

//****************************************************************
// Device class definitions
//----------------------------------------------------------------
#define DEVICE_NONE                     0
#define DEVICE_AP                       1
#define DEVICE_APPLIANCE                2
#define DEVICE_CAR                      3
#define DEVICE_DESKTOP                  4
#define DEVICE_GAME                     5
#define DEVICE_NOTEBOOK                 6
#define DEVICE_PHONE                    7
#define DEVICE_PLAYER                   8
#define DEVICE_TABLET                   9
#define DEVICE_WATCH                   10
#define MAX_DEVICE_ENUM                11

//****************************************************************
// DPI control definitions
//----------------------------------------------------------------
#define DPI_BY_APPLICATION              0x0000
#define DPI_BY_CATEGORY                 0x0001
#define DPI_MGMT_STATS                  0x0002
#define DPI_UNASSOC_STATIONS            0x0004
#define DPI_ALL_STATIONS                0x0008
#define DPI_ALL_VLANS                   0x0010
#define DPI_SUM_VLANS                   0x0020
#define DPI_INCLUDE_ZERO                0x0040
#define DPI_ALL_SSIDS                   0x0080
#define DPI_SUM_SSIDS                   0x0100
#define DPI_VLAN                        0x0200
#define DPI_SSID                        0x0400
#define DPI_STATION                     0x0800
#define DPI_SSID_LIST                   0x1000

//****************************************************************
// Admin priv level section definitions
//----------------------------------------------------------------
#define PRIV_NONE                      -1
#define PRIV_ACL                        0
#define PRIV_ADMIN                      1
#define PRIV_BOOT_ENV                   2
#define PRIV_CDP                        3
#define PRIV_CLUSTER                    4
#define PRIV_CONSOLE                    5
#define PRIV_CONTACT                    6
#define PRIV_TIME                       7
#define PRIV_DESC                       8
#define PRIV_DHCP                       9
#define PRIV_DNS                       10
#define PRIV_ETH0                      11
#define PRIV_FILE                      12
#define PRIV_FILTER                    13
#define PRIV_GIG                       14
#define PRIV_GROUP                     15
#define PRIV_IAP                       16
#define PRIV_MGMT                      17
#define PRIV_NETFLOW                   18
#define PRIV_RADIUS                    19
#define PRIV_REBOOT                    20
#define PRIV_RESET                     21
#define PRIV_TESTS                     22
#define PRIV_SECURITY                  23
#define PRIV_SNMP                      24
#define PRIV_SSID                      25
#define PRIV_SYSLOG                    26
#define PRIV_VLAN                      27
#define PRIV_WIFI_TAG                  28
#define PRIV_STA_ASSURE                29
#define PRIV_ROAM_ASSIST               30
#define PRIV_TUNNEL                    31
#define PRIV_LOCATION                  32
#define PRIV_MDM                       33
#define PRIV_OAUTH                     34
#define PRIV_PROXY_FWD                 35
#define PRIV_LLDP                      36
#define PRIV_PROXY_MGMT                37
#define MAX_NUM_PRIV_SECTIONS          38
#define PRIV_SECTION_LOCKED            MAX_NUM_PRIV_LEVELS

//****************************************************************
// 802.11u definitions
//----------------------------------------------------------------
#define DOT11U_IPV4_NOT_AVAIL                   0
#define DOT11U_IPV4_PUBLIC                      1
#define DOT11U_IPV4_PORT_RESTRICT               2
#define DOT11U_IPV4_SINGLE_NAT                  3
#define DOT11U_IPV4_DOUBLE_NAT                  4
#define DOT11U_IPV4_PORT_RESTRICT_SNAT          5
#define DOT11U_IPV4_PORT_RESTRICT_DNAT          6
#define DOT11U_IPV4_NOT_KNOWN                   7

#define DOT11U_IPV6_NOT_AVAIL                   0
#define DOT11U_IPV6_AVAIL                       1
#define DOT11U_IPV6_NOT_KNOWN                   2

#define DOT11U_NET_AUTH_ACCEPT_TERMS            0
#define DOT11U_NET_AUTH_ONLINE_ENROLL           1
#define DOT11U_NET_AUTH_HTTP_REDIRECT           2
#define DOT11U_NET_AUTH_DNS_REDIRECT            3

#define DOT11U_AUTH_PARAM_NONE                  0
#define DOT11U_AUTH_PARAM_EXP_EAP               1
#define DOT11U_AUTH_PARAM_NON_EAP_INNER_AUTH    2
#define DOT11U_AUTH_PARAM_INNER_AUTH_EAP        3
#define DOT11U_AUTH_PARAM_EXP_INNER_EAP         4
#define DOT11U_AUTH_PARAM_CREDENTIAL            5
#define DOT11U_AUTH_PARAM_TUN_EAP_CREDENTIAL    6

//****************************************************************
// 802.11R definitions
//----------------------------------------------------------------
#define MOBILITY_DOMAIN_ID_LEN          2
#define XIRRUS_11R_R0_KEY_LIFETIME      10000
#define XIRRUS_11R_REASSOC_DEADLINE     2000
#define XIRRUS_11R_PMK_R1_PUSH          1
#define XIRRUS_11R_FT_OVER_DS           0
#define R0_RANDOM_KEY_LEN               32
#define MAX_R0_LIST_LEN                 300

//****************************************************************
// Hotspot 2.0 definitions
//----------------------------------------------------------------
#define HS20_WAN_LINK_UP                1
#define HS20_WAN_LINK_DOWN              2
#define HS20_WAN_LINK_TEST              3

#define HS20_CONN_CAP_STATUS_CLOSED     0
#define HS20_CONN_CAP_STATUS_OPEN       1
#define HS20_CONN_CAP_STATUS_UNKNOWN    2

//****************************************************************
// MDM (Mobile Device Management) definitions
//----------------------------------------------------------------
#define MDM_AUTH_NONE                   0
#define MDM_AUTH_AIRWATCH               1

#define MDM_AW_ACCESS_ERROR_BLOCK       0
#define MDM_AW_ACCESS_ERROR_ALLOW       1

#define MDM_MSG_STATUS                  1
#define MDM_MSG_DATA                    2

#define MDM_STATUS_NOT_KNOWN            0
#define MDM_STATUS_NOT_ACCESSIBLE       1
#define MDM_STATUS_NOT_ENROLLED         2
#define MDM_STATUS_NOT_COMPLIANT        3
#define MDM_STATUS_COMPLIANT            4

//****************************************************************
// SSID/Group QoS definitions
//----------------------------------------------------------------
#define DEF_QOS                         2
#define DEF_QOS_11AC                    0

//****************************************************************
// Misc. size definitions
//----------------------------------------------------------------
#define SIZE_UINT8                      sizeof(uint8)
#define SIZE_UINT16                     sizeof(uint16)
#define SIZE_UINT32                     sizeof(uint32)

#define SIZE_NUM_STATIONS_INFO          (MOM_NUM_RADIO_INTERFACES * SIZE_UINT32)
#define SIZE_STATS_TIME_PERIOD_INFO     (MOM_NUM_RADIO_INTERFACES * SIZE_UINT32)
#define SIZE_CHANNELS_INFO              (MOM_NUM_RADIO_INTERFACES * SIZE_UINT8)
#define SIZE_WDS_CLIENT_LINK_INFO       (MOM_NUM_RADIO_INTERFACES * SIZE_UINT8)

#define SIZE_CFG_MESSAGE_INFO           SIZE_UINT8
#define SIZE_FLASH_FILE_LIST_INFO       SIZE_UINT8
#define SIZE_UBOOT_ENV_VAR_INFO         SIZE_UINT8
#define SIZE_READ_FILE_INFO             SIZE_UINT8

#define SIZE_FILE_SIZE_INFO             SIZE_UINT32
#define SIZE_NEIGHBOR_TRAFFIC_INFO      SIZE_UINT32
#define SIZE_SPECTRUM_ANALYZER_TRAFFIC  SIZE_UINT32

//****************************************************************
// VLAN definitions
//----------------------------------------------------------------
#define VLAN_NO_CHANGE                  ((uint16)-1)

//****************************************************************
// Delete all definitions
//----------------------------------------------------------------
#define SET_DELETE_ALL(p)               (*(int32 *)(p) =  -1)
#define  IS_DELETE_ALL(p)               (*(int32 *)(p) == -1)

//****************************************************************
// Personal WiFi definitions
//----------------------------------------------------------------
#define PERSONAL_WIFI_OFF               0
#define PERSONAL_WIFI_ON                1
#define PERSONAL_WIFI_TIMED             2

//****************************************************************
// Structure definitions
//----------------------------------------------------------------
#define CFG_REQ_HDR_MEMBERS \
    int32  type;            \
    int32  mod;             \
    int32  obj;             \
    int32  priv;            \
    int32  session;         \
    int32  intvalue;        \
    uint64 int64value

struct cfg_req_hdr
{
    CFG_REQ_HDR_MEMBERS;
} __attribute__ ((packed));

#define SIZE_CFG_REQ_HDR        sizeof(struct cfg_req_hdr)
#define MAX_STR                 (PIPE_BUF - SIZE_CFG_REQ_HDR)   // keep sizeof cfg_req == PIPE_BUF so as to guarantee cfg_req writes to pipes are atomic

struct cfg_req
{
    CFG_REQ_HDR_MEMBERS;
    char   strvalue[MAX_STR];
} __attribute__ ((packed));

#define SIZE_CFG_REQ            sizeof(struct cfg_req)

struct clust_req
{
    char   objname[REQ_LEN_STR         ];
    char   name   [REQ_LEN_CLUSTER     ];
    char   array  [REQ_LEN_HOSTNAME_STR];
    uint32 rev;
} __attribute__ ((packed));

#define SIZE_CLUST_REQ          sizeof(struct clust_req)

struct xcfg_req
{
    CFG_REQ_HDR_MEMBERS;
    char   strvalue[MAX_STR - SIZE_CLUST_REQ];
    struct clust_req cluster;
} __attribute__ ((packed));

#define SIZE_XCFG_REQ           sizeof(struct xcfg_req)

struct cfg_struct_req
{
    int32 result;
    char  strvalue[];
} __attribute__ ((packed));

#define SIZE_CFG_STRUCT_REQ     sizeof(struct cfg_struct_req)

struct read_struct_buff
{
    struct read_struct_buff *prev;
    struct read_struct_buff *next;
    uint8  alloc;
    uint8  insert;
    uint8  diff;
    int32  length;
    int32  indent;
    void  *value;
    void  *tag;
} __attribute__ ((packed));

#define SIZE_READ_STRUCT_BUFF   sizeof(struct read_struct_buff)

struct read_struct_req
{
    int32 result;
    int32 count;
    int32 last_idx;
    int32 write_protect;
    struct read_struct_buff *first;
    struct read_struct_buff *last;
    struct read_struct_buff *last_get;
} __attribute__ ((packed));

#define SIZE_READ_STRUCT_REQ    sizeof(struct read_struct_req)

struct ssid_info
{
    u8   mac_num;
    char name[REQ_LEN_SSID];
} __attribute__ ((packed));

struct group_info
{
    u8   mac_num;
    char name[REQ_LEN_GROUP];
} __attribute__ ((packed));

struct vlan_info
{
    uint16 num;
    char   name[REQ_LEN_VLAN_NAME];
} __attribute__ ((packed));

#define SIZE_VLAN_INFO    sizeof(struct vlan_info)

struct vlan_bit_table
{
    uint8 entry[4096 / 8];
} __attribute__ ((packed));

#define SIZE_VLAN_BIT_TABLE    sizeof(struct vlan_bit_table)

struct ipv6_addr_cfg {
    char   addr[REQ_LEN_IPV6_ADDRSTR];
    uint8  mask_bits;
} __attribute__ ((packed));

#define SIZE_IPV6_ADDR_CFG    sizeof(struct ipv6_addr_cfg)

struct array_id
{
   char   hostname[REQ_LEN_HOSTNAME_STR];
   char   mac_addr[REQ_LEN_MAC_ADDR_STR + 1];
   char   ip_addr [REQ_LEN_IPV6_ADDRSTR];
   char   serial  [REQ_LEN_VERSION];
   char   model   [REQ_LEN_VERSION];
   char   brand   [REQ_LEN_VERSION];
   struct iap_counts_info  iap_cnts;
   struct iap_decoder_ring iap_info[MOM_NUM_RADIO_INTERFACES];
   struct ipv6_addr_cfg    ipv6    [MAX_NUM_IPV6_IFACE_ADDRS];
   struct vlan_info        vlan    [MAX_NUM_VLAN ];
   struct ssid_info        ssid    [MAX_NUM_SSID ];
   struct group_info       group   [MAX_NUM_GROUP];
   uint16 model_number;
   uint16 num_slots;
   uint16 num_radios_present;
   uint16 num_radio_modules;
   uint8  num_eth;
   uint8  num_vlans;
   uint8  num_ssid;
   uint8  num_group;
   uint8  save_flag;
   uint8  ipv6_support;
   uint8  factory_reset;
    int32 index;
} __attribute__ ((packed));

#define SIZE_ARRAY_ID    sizeof(struct array_id)

struct acl_cfg
{
    uint8  list_type;
    uint16 num_entries;
    char   entries[MAX_SIZE_ACL][REQ_LEN_MAC_ADDR_STR + 1];
} __attribute__ ((packed));

#define SIZE_ACL_CFG    sizeof(struct acl_cfg)

struct acl_str_cfg
{
    char  entry[REQ_LEN_MAC_ADDR_STR + 1];
} __attribute__ ((packed));

#define SIZE_ACL_STR_CFG    sizeof(struct acl_str_cfg)

struct admin_cfg
{
    char   id[REQ_LEN_STR];
    char   passwd[REQ_LEN_STR];
    char   priv_name[REQ_LEN_PRIV_NAME_STR];
    uint8  priv;
    uint32 user_id;
    uint8  enabled;
} __attribute__ ((packed));

#define SIZE_ADMIN_CFG    sizeof(struct admin_cfg)

struct contact_cfg
{
    char name      [REQ_LEN_STR];
    char email     [REQ_LEN_STR];
    char phone     [REQ_LEN_STR];
    char location  [REQ_LEN_STR];
    char snmp_group[REQ_LEN_STR];
    char hostname  [REQ_LEN_HOSTNAME_STR];  // for read struct summary
} __attribute__ ((packed));

#define SIZE_CONTACT_CFG    sizeof(struct contact_cfg)

struct dns_cfg
{
    char  hostname  [REQ_LEN_HOSTNAME_STR];
    char  domain    [REQ_LEN_HOSTNAME_STR];
    char  servers[3][REQ_LEN_IPV6_ADDRSTR];
    uint8 use_ipv4_dhcp;
    uint8 use_ipv6_dhcp;
} __attribute__ ((packed));

#define SIZE_DNS_CFG    sizeof(struct dns_cfg)

struct dhcp_server_cfg
{
    uint8  enabled;
    uint32 default_lease_time;
    uint32 max_lease_time;
    char   range_start[REQ_LEN_IPV4_ADDRSTR];
    char   range_end  [REQ_LEN_IPV4_ADDRSTR];
} __attribute__ ((packed));

#define SIZE_DHCP_CFG    sizeof(struct dhcp_server_cfg)

struct dhcp_pool_cfg
{
    char   name       [REQ_LEN_DHCP_POOL_NAME];
    char   range_start[REQ_LEN_IPV4_ADDRSTR];
    char   range_end  [REQ_LEN_IPV4_ADDRSTR];
    char   mask       [REQ_LEN_IPV4_ADDRSTR];
    char   gateway    [REQ_LEN_IPV4_ADDRSTR];
    char   domain     [REQ_LEN_HOSTNAME_STR];
    char   servers [3][REQ_LEN_IPV4_ADDRSTR];
    uint8  exists;
    uint8  enabled;
    uint8  nat;
    uint32 default_lease_time;
    uint32 max_lease_time;
} __attribute__ ((packed));

#define SIZE_DHCP_POOL_CFG    sizeof(struct dhcp_pool_cfg)

struct bond_cfg {
    char   name[8];
    char   dev_name[8];
    uint8  exists;
    uint8  mode;
    uint8  ports;
    uint8  mirror;
    char   active_vlans[REQ_LEN_ACTIVE_VLANS_STR];
} __attribute__ ((packed));

#define SIZE_BOND_CFG    sizeof(struct bond_cfg)

struct eth_cfg
{
    char   name[8];
    char   dev_name[8];
    char   env_ipaddr[8];
    uint8  exists;
    uint8  enabled;
    uint8  ipv4_dhcp;
    char   ipv4_addr   [REQ_LEN_IPV4_ADDRSTR];
    char   ipv4_mask   [REQ_LEN_IPV4_ADDRSTR];
    char   ipv4_gateway[REQ_LEN_IPV4_ADDRSTR];
    uint8  autoneg;
    uint8  duplex;
    uint16 speed;
    uint16 max_speed;
    uint16 mtu;
    uint8  mgmt;
    uint8  led;
    uint8  bond_index;
    struct bond_cfg bond;
    struct {
        uint8  link;
        uint16 speed;
        uint8  duplex;
    } status;
    struct {
        char addr[REQ_LEN_IPV4_ADDRSTR];
        char mask[REQ_LEN_IPV4_ADDRSTR];
    } static_route;
    char   mac_addr[REQ_LEN_MAC_ADDR_STR + 1];
    uint8  ipv4_mask_bits;  // ipv4
    uint8  ipv6_dhcp;
    struct ipv6_addr_cfg ipv6_static;
    struct ipv6_addr_cfg ipv6[MAX_NUM_IPV6_IFACE_ADDRS];
} __attribute__ ((packed));

#define SIZE_ETH_CFG    sizeof(struct eth_cfg)

struct eth_info
{
    char   name[8];
    uint8  exists;
    uint8  link;
    uint16 max_speed;
} __attribute__ ((packed));

#define SIZE_ETH_INFO    sizeof(struct eth_info)

struct console_cfg
{
    uint32 baud_rate;
    uint8  word_size;
    uint8  stop_bits;
    uint8  parity;
    uint32 timeout;
    uint8  mgmt;
    uint8  exists;
} __attribute__ ((packed));

#define SIZE_CONSOLE_CFG    sizeof(struct console_cfg)

struct priv_section_cfg
{
    char   name[REQ_LEN_PRIV_NAME_STR];
    char   abbr[2][REQ_LEN_PRIV_ABBR_STR];
    char   desc[REQ_LEN_STR];
    uint8  level;
    uint16 oid;
} __attribute__ ((packed));

#define SIZE_PRIV_SECTION_CFG    sizeof(struct priv_section_cfg)

struct priv_level_cfg
{
    uint8 number;
    char  name[REQ_LEN_PRIV_NAME_STR];
} __attribute__ ((packed));

#define SIZE_PRIV_LEVEL_CFG    sizeof(struct priv_level_cfg)

struct priv_cfg
{
    struct priv_section_cfg none;  // allow -1 index into section
    struct priv_section_cfg section[MAX_NUM_PRIV_SECTIONS];
    struct priv_level_cfg   level  [MAX_NUM_PRIV_LEVELS+1];
} __attribute__ ((packed));

#define SIZE_PRIV_CFG    sizeof(struct priv_cfg)

struct priv_setting_cfg
{
    char section_name[REQ_LEN_PRIV_NAME_STR];
    char level_name  [REQ_LEN_PRIV_NAME_STR];
} __attribute__ ((packed));

#define SIZE_PRIV_SETTING_CFG    sizeof(struct priv_setting_cfg)

struct coordinates
{
    float  x;
    float  y;
    float  z;
} __attribute__ ((packed));

#define SIZE_COORDINATES    sizeof(struct coordinates)

struct position_cfg
{
    struct coordinates coordinate;
    float scale;
    float angle;
    uint8 mount;
    uint8 scope;
    char  map[REQ_LEN_MAP_STR];
    struct {
        double angle    ;
        double latitude ;
        double longitude;
        double elevation;
        uint8  reference;
    } gps;
} __attribute__ ((packed));

#define position_mnt_info       position_cfg

#define SIZE_POSITION_CFG       sizeof(struct position_cfg)
#define SIZE_POSITION_MNT_INFO  sizeof(struct position_mnt_info)

struct bluetooth_cfg
{
    uint8  exists;
    uint8  ibeacon_enable;
    char   ibeacon_uuid[REQ_LEN_IBEACON_UUID_STR];
    uint16 ibeacon_major;
    uint16 ibeacon_minor;
} __attribute__ ((packed));

#define SIZE_BLUETOOTH_CFG       sizeof(struct bluetooth_cfg)

struct mgmt_cfg
{
    uint8  xircon_enabled;
    uint8  telnet_enabled;
    uint8  ssh_enabled;
    uint8  https_enabled;
    uint32 xircon_timeout;
    uint32 telnet_timeout;
    uint32 ssh_timeout;
    uint32 https_timeout;
    uint16 xircon_port;
    uint16 telnet_port;
    uint16 ssh_port;
    uint16 https_port;
    uint16 default_vlan;
    uint16 native_vlan;
    char   static_route_addr[REQ_LEN_IPV4_ADDRSTR];
    char   static_route_mask[REQ_LEN_IPV4_ADDRSTR];
    uint8  acl_mode;
    uint8  fips_enabled;
    uint8  pci_audit_enabled;
    uint8  door_exists;
    uint8  door_status;
    uint8  network_assurance;
    uint16 net_assure_period;
    uint16 reauth_period;
    uint8  max_auth_attempts;
    uint8  rdk_mode;
    uint8  stp_enabled;
    uint8  activation_enabled;  // do not save in config file
    uint32 activation_interval;
    char   activation_server[REQ_LEN_URL_STR];
    char   activation_version[REQ_LEN_VERSION];
    uint8  cloud_enabled;
    char   cloud_server[REQ_LEN_HOSTNAME_STR];
    uint16 cloud_retry;
    uint16 cloud_timeout;
    uint8  cloud_scheme;
    uint16 cloud_port;
    uint8  xms_control;
    uint8  pwifi_limit_all_sta;
    uint8  pwifi_limit_per_sta;
    char   pwifi_default_exp[REQ_LEN_DATE_TIME_STR];
    uint8  xms_e_enabled;
    char   xms_e_ip_addr [REQ_LEN_IP_ADDRSTR];
    char   xms_e_server[REQ_LEN_HOSTNAME_STR];
    uint16 xms_e_retry;
    uint16 xms_e_timeout;
    uint8  xms_e_scheme;
    uint16 xms_e_port;
    uint8  ipv6_support;
    uint8  auto_disc_enabled;
    char   auto_disc_server[REQ_LEN_HOSTNAME_STR];
    uint32 auto_disc_interval;
    uint8  auto_disc_scheme;
    uint16 auto_disc_port;
    uint8  has_status_led;
    uint8  status_led_enabled;
    uint8  has_reset_button;
    uint8  reset_button_enabled;
} __attribute__ ((packed));

#define SIZE_MGMT_CFG    sizeof(struct mgmt_cfg)

struct cloud_sys_info {
    char server[REQ_LEN_HOSTNAME_STR];
    u16  retry;
    u16  timeout;
    u8   scheme;
    u16  port;
    char deviceId[REQ_LEN_VERSION];
    char model[REQ_LEN_LONG_VERSION];
    char osVersion[REQ_LEN_SOFTWARE_VERSION];
    char bootVersion[REQ_LEN_SOFTWARE_VERSION];
    u32  uptime;
    char licenseKey[REQ_LEN_LONG_VERSION];
} __attribute__((packed));

#define SIZE_CLOUD_INFO    sizeof(struct cloud_sys_info)

struct xms_e_sys_info {
    char   server[REQ_LEN_HOSTNAME_STR];
    uint16 retry;
    uint16 timeout;
    uint8  scheme;
    uint16 port;
    char   deviceId[REQ_LEN_VERSION];
    char   model[REQ_LEN_LONG_VERSION];
    char   osVersion[REQ_LEN_SOFTWARE_VERSION];
    char   bootVersion[REQ_LEN_SOFTWARE_VERSION];
    u32    uptime;
    char   licenseKey[REQ_LEN_LONG_VERSION];
} __attribute__((packed));

#define SIZE_EASY_PASS_INFO    sizeof(struct xms_e_sys_info)

struct auto_disc_info {
    char   server[REQ_LEN_HOSTNAME_STR];
    uint32 interval;
    uint8  scheme;
    uint16 port;
    char   deviceId[REQ_LEN_VERSION];
    char   model[REQ_LEN_LONG_VERSION];
    char   osVersion[REQ_LEN_SOFTWARE_VERSION];
    char   bootVersion[REQ_LEN_SOFTWARE_VERSION];
    u32    uptime;
    char   licenseKey[REQ_LEN_LONG_VERSION];
} __attribute__((packed));

#define SIZE_AUTO_DISC_INFO    sizeof(struct auto_disc_info)

struct sta_assure_params
{
    int32 auth_failures;
    int32 assoc_time;
    int32 error_rate;
    int32 retry_rate;
    int32 data_rate;
    int32 rssi;
    int32 snr;
    int32 distance;
    int32 driver_failures;
} __attribute__ ((packed));

#define SIZE_STA_ASSURE_CFG    sizeof(struct sta_assure_cfg)

struct sta_assure_cfg
{
    uint8  enable;
    uint16 period;
    struct sta_assure_params threshold;
} __attribute__ ((packed));

#define SIZE_STA_ASSURE_CFG    sizeof(struct sta_assure_cfg)

struct roam_assist_cfg
{
    uint8  enable;
    uint16 period;
     int8  threshold;
    uint8  data_rate;
    char   devices[REQ_LEN_DEVICE_LIST_STR];
} __attribute__ ((packed));

#define SIZE_ROAM_ASSIST_CFG    sizeof(struct roam_assist_cfg)

struct ntp_server_cfg
{
    char   server[REQ_LEN_HOSTNAME_STR];
    uint8  auth_type;
    uint16 key_id;
    char   key[REQ_LEN_NTP_KEY_STR];
} __attribute__ ((packed));

#define SIZE_NTP_SERVER_CFG sizeof(struct ntp_server_cfg)

struct xtm {
    int32 sec;         /* seconds                */
    int32 min;         /* minutes                */
    int32 hour;        /* hours                  */
    int32 mday;        /* day of the month       */
    int32 mon;         /* month                  */
    int32 year;        /* years since 1900       */
    int32 wday;        /* day of the week        */
    int32 yday;        /* day in the year        */
    int32 isdst;       /* daylight saving time   */
    int32 gmtoff;
} __attribute__ ((packed));

#define BASE_YEAR      1900

struct date_time_cfg
{
    uint8  ntp_enabled;
    struct ntp_server_cfg ntp_servers[2];
     int8  off_hours;
    uint8  off_mins;
    uint8  dst_adjust;
    uint32 up_time;
    struct xtm now;
} __attribute__ ((packed));

#define SIZE_DATE_TIME_CFG  sizeof(struct date_time_cfg)

// NOTE this structure is written out in its entirety in an XRP IE
// ONLY add new members to the end of this structure for backward compaibility
struct max_stations_info
{
    uint16 last_hour;
    uint16 last_day;
    uint16 last_week;
    uint16 last_month;
    uint16 last_year;
} __attribute__ ((packed));

#define SIZE_MAX_STATIONS_INFO  sizeof(struct max_stations_info)

// NOTE this structure is written out in its entirety in an XRP IE
// ONLY add new members to the end of this structure for backward compaibility
struct sta_type_cnt_info
{
    uint16 dot11b;
    uint16 dot11g;
    uint16 dot11n_2ghz;
    uint16 sumall_2ghz;
    uint16 dot11a;
    uint16 dot11n_5ghz;
    uint16 sumall_5ghz;
    uint16 sumall_abg;
    uint16 sumall_11n;
    uint16 sumall_stas;
    uint16 dot11ac;
    uint16 sumall_11ac;
    uint16 stream_2ghz[TXRX_STREAMS_MAX];
    uint16 stream_5ghz[TXRX_STREAMS_MAX];
    uint16 sumall_strm[TXRX_STREAMS_MAX];
    uint16 su_txbf_stas;
    uint16 mu_txbf_stas;
    uint16 mu_mimo_stas;
    uint16 mu_mimo_grps;
} __attribute__ ((packed));

#define SIZE_STA_TYPE_CNT_INFO  sizeof(struct sta_type_cnt_info)

struct radio_assure_info
{
    uint64 monitor;
    uint64 beacon;
    uint64 phy;
    uint64 mac;
    uint64 system;
} __attribute__ ((packed));

#define SIZE_RADIO_ASSURE_INFO  sizeof(struct radio_assure_info)

struct radio_cfg
{
    struct iap_decoder_ring info;       // move these to top
    char   bssid[REQ_LEN_BSSID_STR];    // for cluster sorting
    uint8  enabled;
    uint8  pending;
    uint8  last_state;
    uint8  capture_enabled;
    uint8  ant;
    uint8  band;
    uint8  wifi_mode;
    uint8  cell_size;
    uint8  channel;
    uint8  channel_mode;
    uint8  channel_bond_40mhz;
    uint8  channel_bond_80mhz;
    uint8  channel_bond_160mhz;
     int8  bond_40mhz;
     int8  bond_80mhz;
     int8  bond_160mhz;
    uint8  radar_org_chan;
    uint8  radar_org_mode;
    uint8  radar_org_bond_40mhz;
    uint8  radar_org_bond_80mhz;
    uint8  radar_org_bond_160mhz;
    uint8  radar_rst_time;
     int8  txpwr;
     int8  rxthresh;
     int8  auto_txpwr;
     int8  auto_rxthresh;
    uint8  wds_link;
    uint8  wds_host_link;
    uint8  wds_client_link;
    uint8  wds_distance;
    uint8  wds_connected;
    uint16 stations;
    char   desc[REQ_LEN_STR];
    struct max_stations_info max_stations;
    struct sta_type_cnt_info sta_type_cnt;
    struct radio_assure_info iap_resets;
} __attribute__ ((packed));

#define SIZE_RADIO_CFG sizeof(struct radio_cfg)

struct radio_oopme_cfg
{
    uint8  enabled;
    uint8  ant;
    uint8  band;
    uint8  wifi_mode;
    uint8  cell_size;
    uint8  channel;
     int8  bond_40mhz;
     int8  bond_80mhz;
     int8  bond_160mhz;
    uint8  txpwr;
     int8  rxthresh;
    uint8  wds_link;
    uint8  wds_distance;
} __attribute__ ((packed));

#define SIZE_RADIO_OOPME_CFG sizeof(struct radio_oopme_cfg)

struct load_balance_cfg
{
    uint8  debug;
    uint8  max_attempts;
    uint8  min_probe_reqs;
    uint8  band_weight;
    uint8  stream_weight;
    uint8  vht_weight;
    uint8  ht_weight;
    uint8  snr_weight;
} __attribute__ ((packed));

#define SIZE_LOAD_BALANCE_CFG sizeof(struct load_balance_cfg)

struct autocell_cfg
{
    uint32 period;
    uint8  overlap;
     int8  min_tx;
     int8  max_rx;
    uint8  by_chan;
    uint8  debug;
} __attribute__ ((packed));

#define SIZE_AUTOCELL_CFG sizeof(struct autocell_cfg)

struct radio_glb_cfg
{
    uint16  beacon_int;
    uint8   beacon_dtim;
    uint8   long_retry;
    uint8   short_retry;
    char    country_code[REQ_LEN_COUNTRY_STR];
    uint8   dot11h_enable;
    uint8   dot11k_enable;
    uint8   dot11r_enable;
    uint8   dot11v_enable;
    uint8   assurance_mode;
    uint16  led_mask;
    uint8   autochan_pwron;
    char    autochan_time[REQ_LEN_AUTOCHAN_TIME_STR];
    uint16  iap_stations;
    uint8   mgmt;
    uint8   sta2sta_block;
    uint8   load_balancing;
    uint8   tx_coordination;
    uint8   rx_coordination;
    uint8   txrx_balancing;
    uint8   xrp_mode;
    uint8   xrp_peers;
    uint8   sharp_cell;
    uint8   iap_phones;
    uint8   broadcast_optimize;
    uint8   public_safety;
    uint8   xrp_layer;
    uint8   wds_allow_stations;
    uint8   wds_roam_thresh;
    uint8   wds_roam_avg_weight;
    uint8   wds_lock;
    uint8   arp_filter;
    uint8   num_slots;
    uint8   num_radio_modules;
    uint8   wfa_mode;
    uint8   rf_monitor;
    uint8   monitor_iap;
    uint16  rf_monitor_scan_time;
    uint16  rf_monitor_sta_limit;
    uint16  rf_monitor_pps_limit;
    uint16  max_stations;
    uint8   multicast_mode;
    uint8   multicast_isolation;
    uint128 multicast_exclude[MAX_NUM_MULTICAST_EXCLUDE];
    uint128 multicast_forward[MAX_NUM_MULTICAST_FORWARD];
    uint32  multicast_fwd_vlan[MAX_NUM_VLAN];
    char    mdns_filter[MAX_NUM_MDNS_FILTER][REQ_LEN_MDNS_FILTER];
    uint8   wmm_power_save;
    uint8   wmm_acm;
    uint8   cac_max_bw;
    uint8   extract_flags;
    uint16  extract_ip_dhcp_period;
    uint8   dscp_mode;
    uint8   dscp_map[MAX_NUM_DSCP_VALUES];
    uint8   ipv6_blocking;
    uint8   dot11w_mode;
    uint8   okc_enable;
    uint8   max_sta_per_iap;
    uint8   outdoor_install;
} __attribute__ ((packed));

#define SIZE_RADIO_GLB_CFG sizeof(struct radio_glb_cfg)

struct radio_glb_11a_cfg
{
    uint64 supp_rates;
    uint64 basic_rates;
    uint16 iap_stations;
    uint16 max_stations;
    uint16 frag;
    uint16 rts;
    uint8  autochan_list[MAX_A_CHANNELS];
    uint8  active_channels[MAX_NUM_CHANNELS];
    struct autocell_cfg autocell;
    uint8  retries_per_rate;
    uint8  retry_rate_downs;
     int8  prb_rsp_min_rssi;
     int8  cca_rx_thr_delta;
} __attribute__ ((packed));

#define SIZE_RADIO_GLB_11A_CFG sizeof(struct radio_glb_11a_cfg)

struct radio_glb_11g_cfg
{
    uint64 supp_rates;
    uint64 basic_rates;
    uint16 iap_stations;
    uint16 max_stations;
    uint16 frag;
    uint16 rts;
    uint8  slot;
    uint8  preamble;
    uint8  gprotect;
    uint8  gonly;
    uint8  autochan_list[MAX_BG_CHANNELS];
    uint8  active_channels[MAX_NUM_CHANNELS];
    struct autocell_cfg autocell;
    uint8  retries_per_rate;
    uint8  retry_rate_downs;
     int8  prb_rsp_min_rssi;
     int8  cca_rx_thr_delta;
} __attribute__ ((packed));

#define SIZE_RADIO_GLB_11G_CFG sizeof(struct radio_glb_11g_cfg)

struct radio_glb_11n_cfg
{
    uint64 supp_rates;
    uint64 basic_rates;
    uint8  num_rates;
    uint8  capable;
    uint8  enable;
    uint8  tx_chains;
    uint8  rx_chains;
    uint8  sgi20_capable;
    uint8  sgi_20mhz;
    uint8  sgi_40mhz;
    uint8  bond_mode_5ghz;
    uint8  bond_mode_2ghz;
    uint8  auto_bond;
    uint8  aggregate_retries;
    uint8  block_nak_retries;
} __attribute__ ((packed));

#define SIZE_RADIO_GLB_11N_CFG sizeof(struct radio_glb_11n_cfg)

struct radio_glb_11ac_cfg
{
    uint8  capable;
    uint8  enable;
    uint8  wave_2;
    uint8  sgi_80mhz;       //  80Mhz short guard interval, 0=disable, 1=enable
    uint8  sgi_160mhz;      // 160Mhz short guard interval, 0=disable, 1=enable
    uint8  max_mcs_1ss;     // Max MCS setting, single spatial stream, 0=mcs7, 1=msc8, 2=mcs9
    uint8  max_mcs_2ss;     // Max MCS setting,  dual  spatial stream, 0=mcs7, 1=msc8, 2=mcs9
    uint8  max_mcs_3ss;     // Max MCS setting, triple spatial stream, 0=mcs7, 1=msc8, 2=mcs9
    uint8  max_mcs_4ss;     // Max MCS setting,  quad  spatial stream, 0=mcs7, 1=msc8, 2=mcs9
    uint8  max_mcs_5ss;     // Max MCS setting,  quad  spatial stream, 0=mcs7, 1=msc8, 2=mcs9
    uint8  max_mcs_6ss;     // Max MCS setting,  quad  spatial stream, 0=mcs7, 1=msc8, 2=mcs9
    uint8  max_mcs_7ss;     // Max MCS setting,  quad  spatial stream, 0=mcs7, 1=msc8, 2=mcs9
    uint8  max_mcs_8ss;     // Max MCS setting,  quad  spatial stream, 0=mcs7, 1=msc8, 2=mcs9
    uint8  mu_mimo;
    uint8  tx_bf;
    uint8  auto_bond;
} __attribute__ ((packed));

#define SIZE_RADIO_GLB_11AC_CFG sizeof(struct radio_glb_11ac_cfg)

struct vlan_tunnel_cfg
{
    char   server[REQ_LEN_HOSTNAME_STR];
    char   secret[REQ_LEN_RAD_SECRET];
    uint16 port;
    uint8  state;
} __attribute__ ((packed));

// NOTE this structure is written out in its entirety to the eeprom
// ONLY add new members to the end of this structure for backward compatibility
struct vlan_cfg
{
    uint16 num;
    char   name[REQ_LEN_VLAN_NAME];
    uint8  mgmt;
    uint8  ipv4_dhcp;
    char   ipv4_addr   [REQ_LEN_IPV4_ADDRSTR];
    char   ipv4_mask   [REQ_LEN_IPV4_ADDRSTR];
    char   ipv4_gateway[REQ_LEN_IPV4_ADDRSTR];
    struct vlan_tunnel_cfg tunnel;
    uint8  dflt;
    uint8  native;
    uint8  xrp;
    char   num_str[REQ_LEN_VLAN_NUM];
    uint8  active;
    uint8  fa;
    uint8  ipv4_mask_bits;
    uint8  ipv6_dhcp;
    struct ipv6_addr_cfg ipv6_static;
    struct ipv6_addr_cfg ipv6[MAX_NUM_IPV6_IFACE_ADDRS];
} __attribute__ ((packed));

#define SIZE_VLAN_CFG sizeof(struct vlan_cfg)

struct vlan_params_enc
{
    uint16 num;
    char   tunnel_secret[2*REQ_LEN_RAD_SECRET];
} __attribute__ ((packed));

#define SIZE_VLAN_PARAMS_ENC sizeof(struct vlan_params_enc)

struct vlan_pool_cfg
{
    char   name[REQ_LEN_VLAN_NAME];
    uint16 list[MAX_NUM_VLAN];
} __attribute__ ((packed));

#define SIZE_VLAN_POOL_CFG   sizeof(struct vlan_pool_cfg)
#define SIZE_VLAN_POOL_LIST (sizeof(uint16) * MAX_NUM_VLAN)

struct tunnel_info
{
    int    brnum;
    int    viapnum[MAX_NUM_RADIO_INTERFACES];
    int    mcast_viapnum;
    int    active_remote;
    int    cur_fails;
    uint32 next_ping_time;
} __attribute__ ((packed));

#define SIZE_TUNNEL_INFO sizeof(struct tunnel_info)

// NOTE this structure is written out in its entirety to the eeprom
// ONLY add new members to the end of this structure for backward compatibility
// this structure and tunnel_cfg structure must match exactly
// up through those elements contained in tunnel_header_cfg
struct tunnel_header_cfg
{
    char   name[REQ_LEN_TUNNEL_NAME];
    uint8  enabled;
    uint8  type;
    char   old_local    [REQ_LEN_IPV4_ADDRSTR];
    char   old_remote[2][REQ_LEN_IPV4_ADDRSTR];
    uint16 ssid_mask;
    uint8  dhcp_opt;
    uint16 interval;
    uint16 failures;
    uint16 mtu;
    uint16 vlan_list[MAX_NUM_VLAN];
    char   local    [REQ_LEN_IPV6_ADDRSTR];
    char   remote[2][REQ_LEN_IPV6_ADDRSTR];
} __attribute__ ((packed));

#define SIZE_TUNNEL_HEADER_CFG sizeof(struct tunnel_header_cfg)
#define SIZE_TUNNEL_VLAN_LIST (sizeof(uint16) * MAX_NUM_VLAN)

// NOTE this structure and tunnel_header_cfg structure must match exactly
// up through those elements contained in tunnel_header_cfg
struct tunnel_cfg
{
    char   name[REQ_LEN_TUNNEL_NAME];
    uint8  enabled;
    uint8  type;
    char   old_local    [REQ_LEN_IPV4_ADDRSTR];
    char   old_remote[2][REQ_LEN_IPV4_ADDRSTR];
    uint16 ssid_mask;
    uint8  dhcp_opt;
    uint16 interval;
    uint16 failures;
    uint16 mtu;
    uint16 vlan_list[MAX_NUM_VLAN];
    char   local    [REQ_LEN_IPV6_ADDRSTR];
    char   remote[2][REQ_LEN_IPV6_ADDRSTR];
    struct tunnel_info info;
} __attribute__ ((packed));

#define SIZE_TUNNEL_CFG sizeof(struct tunnel_cfg)

struct portal_cfg
{
    char   name[REQ_LEN_PORTAL_NAME];
    uint8  enabled;
    uint16 port;
    uint8  interval;
    uint8  timeout;
    char   server      [REQ_LEN_HOSTNAME_STR];
    char   msg_template[REQ_LEN_STR];
    char   iface_int   [REQ_LEN_PORTAL_IFACE_STR];
    char   iface_ext   [REQ_LEN_PORTAL_IFACE_STR];
} __attribute__ ((packed));

#define SIZE_PORTAL_CFG    sizeof(struct portal_cfg)

struct vap_info
{
    char   name[REQ_LEN_HOSTNAME_STR];
    char   mac[REQ_LEN_MAC_ADDR_STR + 1];
    uint8  status;
    uint8  num_slots;
} __attribute__ ((packed));

#define SIZE_VAP_INFO sizeof(struct vap_info)

struct vap_cfg
{
    char   name[REQ_LEN_HOSTNAME_STR];
    char   mac[REQ_LEN_MAC_ADDR_STR + 1];
    uint8  adopt;
} __attribute__ ((packed));

#define SIZE_VAP_CFG sizeof(struct vap_cfg)

struct wds_iap_info {
    char  sw_label[6];
    uint8 channel;
    uint8 channel_bond_40mhz;
    uint8 channel_bond_80mhz;
    uint8 channel_bond_160mhz;
    uint8 link_distance;
    uint8 connected;
} __attribute__ ((packed));

#define SIZE_WDS_IAP_INFO sizeof(struct wds_iap_info)

struct wds_client_cfg
{
    uint8  index;
    uint8  enabled;
    uint8  max_iap;
    uint8  num_iaps;
    char   target      [REQ_LEN_MAC_ADDR_STR+1];
    char   iap_mac_base[REQ_LEN_MAC_ADDR_STR+1];
    char   ssid        [REQ_LEN_SSID];
    char   username    [REQ_LEN_STR];
    char   password    [REQ_LEN_RAD_SECRET];
    struct wds_iap_info iap[MAX_NUM_WDS_IAPS_PER_LINK];
} __attribute__ ((packed));

#define SIZE_WDS_CLIENT_CFG sizeof(struct wds_client_cfg)

struct wds_host_info
{
    uint8  index;
    uint8  allow_stations;
    uint8  roam_thresh;
    uint8  roam_avg_weight;
    uint8  wds_lock;
    uint8  num_iaps;
    char   source      [REQ_LEN_MAC_ADDR_STR+1];
    char   iap_mac_base[REQ_LEN_MAC_ADDR_STR+1];
    char   ssid        [REQ_LEN_SSID];
    struct wds_iap_info iap[MAX_NUM_WDS_IAPS_PER_LINK];
} __attribute__ ((packed));

#define SIZE_WDS_HOST_INFO sizeof(struct wds_host_info)

struct xrp_target_cfg_old
{
    char  base_mac_addr[REQ_LEN_MAC_ADDR_STR + 1];
} __attribute__ ((packed));

#define SIZE_XRP_TARGET_CFG_OLD sizeof(struct xrp_target_cfg_old)

// NOTE this structure is written out in its entirety to the eeprom
// ONLY add new members to the end of this structure for backward compaibility
struct xrp_target_cfg
{
    uint8 type;
    char  addr_str[REQ_LEN_HOSTNAME_STR];
} __attribute__ ((packed));

#define SIZE_XRP_TARGET_CFG sizeof(struct xrp_target_cfg)

struct xrp_interface_cfg
{
    char iface_name[REQ_LEN_IFACE_NAME];
    char ip_addr[REQ_LEN_IPV6_ADDRSTR];
    char ip_mask[REQ_LEN_IPV6_ADDRSTR];
} __attribute__ ((packed));

#define SIZE_XRP_INTERFACE_CFG sizeof(struct xrp_interface_cfg)

// NOTE this structure is written out in its entirety to the eeprom
// ONLY add new members to the end of this structure for backward compaibility
struct ids_ssid
{
    uint8 status;
    char  name[REQ_LEN_SSID];
} __attribute__ ((packed));

#define SIZE_IDS_SSID sizeof(struct ids_ssid)

struct ids_hdr
{
    uint8  enabled;
    uint16 num_ssid;
    int8   autoblock_rssi;
    uint8  autoblock_enc;
    uint8  autoblock_type;
    struct ids_attack_cfg attacks;
    uint8  autoblock_wlist[MAX_NUM_AUTOBLOCK_WLIST_CHANS+1];
} __attribute__ ((packed));

#define SIZE_IDS_HDR sizeof(struct ids_hdr)

struct ids_cfg
{
    uint8  enabled;
    uint16 num_ssid;
    int8   autoblock_rssi;
    uint8  autoblock_enc;
    uint8  autoblock_type;
    struct ids_attack_cfg attacks;
    uint8  autoblock_wlist[MAX_NUM_AUTOBLOCK_WLIST_CHANS+1];
    struct ids_ssid ssid_list[MAX_IDS_SSID_ENTRIES];
} __attribute__ ((packed));

#define SIZE_IDS_CFG sizeof(struct ids_cfg)

struct radius_server_cfg
{
    char   server [REQ_LEN_HOSTNAME_STR];
    char   ip_addr[REQ_LEN_IPV6_ADDRSTR];
    uint16 port;
    char   secret [REQ_LEN_RAD_SECRET];
} __attribute__ ((packed));

#define SIZE_RADIUS_SERVER_CFG    sizeof(struct radius_server_cfg)

struct radius_cfg
{
    uint8  enabled;
    struct radius_server_cfg pri;
    struct radius_server_cfg sec;
    uint16 timeout;
    uint16 failover_timeout;
    uint16 das_port;
    uint32 das_time_window;
    uint8  das_event_timestamp;
    uint8  called_sta_id_fmt;
    uint8  sta_mac_fmt;
    char   nas_id[REQ_LEN_HOSTNAME_STR];
    uint8  auth_type;                   // admin radius only
    uint16 handshake_timeout;			// 80211x 4-way handshake timeout  1,3 of 4.
    uint8  handshake_retries;			// 80211x 4-way handshake retires.

} __attribute__ ((packed));

#define SIZE_RADIUS_CFG    sizeof(struct radius_cfg)

struct radius_acct_cfg
{
    uint8  enabled;
    struct radius_server_cfg pri;
    struct radius_server_cfg sec;
    uint16 interval;
} __attribute__ ((packed));

#define SIZE_RADIUS_ACCT_CFG    sizeof(struct radius_acct_cfg)

struct radius_all_cfg
{
    struct radius_cfg       srvc;
    struct radius_acct_cfg  acct;
} __attribute__ ((packed));

#define SIZE_RADIUS_ALL_CFG    sizeof(struct radius_all_cfg)

struct active_directory_cfg
{
    char domain_user[REQ_LEN_ACTIVE_DIR_STR];
    char domain_passwd[2*REQ_LEN_ACTIVE_DIR_STR];
    char domain_controller[REQ_LEN_HOSTNAME_STR];
    char workgroup[REQ_LEN_HOSTNAME_STR];
    char realm[REQ_LEN_HOSTNAME_STR];
} __attribute__ ((packed));

#define SIZE_ACTIVE_DIRECTORY_CFG sizeof(struct active_directory_cfg)

struct snmpv2_cfg {
    uint8  enabled;
    char   comm_rw[REQ_LEN_STR];
    char   comm_ro[REQ_LEN_STR];
} __attribute__ ((packed));

#define SIZE_SNMPV2_CFG     sizeof(struct snmpv2_cfg)

struct snmpv3_cfg {
    uint8  enabled;
    uint8  authentication;
    uint8  privacy;
    char   rw_user_name[REQ_LEN_SNMP_STR];
    char   rw_auth_pass[REQ_LEN_SNMP_STR];
    char   rw_priv_pass[REQ_LEN_SNMP_STR];
    char   ro_user_name[REQ_LEN_SNMP_STR];
    char   ro_auth_pass[REQ_LEN_SNMP_STR];
    char   ro_priv_pass[REQ_LEN_SNMP_STR];
    char   engine_id   [REQ_LEN_STR];
} __attribute__ ((packed));

#define SIZE_SNMPV3_CFG     sizeof(struct snmpv3_cfg)

struct snmp_trap_cfg {
    uint8  auth;
    uint16 keepalive;
    char   host[NUM_SNMP_TRAP_HOSTS][REQ_LEN_HOSTNAME_STR];
    char   addr[NUM_SNMP_TRAP_HOSTS][REQ_LEN_IPV6_ADDRSTR];
    uint16 port[NUM_SNMP_TRAP_HOSTS];
} __attribute__ ((packed));

#define SIZE_SNMP_TRAP_CFG  sizeof(struct snmp_trap_cfg)

struct snmp_cfg
{
    struct snmpv2_cfg    v2;
    struct snmpv3_cfg    v3;
    struct snmp_trap_cfg trap;
} __attribute__ ((packed));

#define SIZE_SNMP_CFG    sizeof(struct snmp_cfg)

struct security_cfg
{
    uint8  wep_enabled;
    char   wep_keys[NUM_WEP_KEYS][REQ_LEN_WEP_KEY];
    uint8  wep_keys_len[NUM_WEP_KEYS];
    uint8  wep_default_key_id;
    uint8  wpa_enabled;
    uint8  wpa2_enabled;
    uint8  wpa_tkip_enabled;
    uint8  wpa_aes_enabled;
    uint8  wpa_eap_enabled;
    uint8  wpa_psk_enabled;
    uint32 wpa_upsk_timeout;
    uint8  wpa_upsk_server_error;
    char   wpa_passphrase[REQ_LEN_WPA_PASSPHRASE];
    uint32 wpa_group_rekey;
    uint8  radius_enabled;
    uint8  per_ssid_cipher;
} __attribute__ ((packed));

#define SIZE_SECURITY_CFG sizeof(struct security_cfg)

struct wep_keys_enc
{
    char keys[NUM_WEP_KEYS][2*REQ_LEN_WEP_KEY];
} __attribute__ ((packed));

#define SIZE_WEP_KEYS_ENC sizeof(struct wep_keys_enc)

struct ssid_glb_cfg
{
    int8 brcast_ssid_a;
    int8 brcast_ssid_bg;
} __attribute__ ((packed));

#define SIZE_SSID_GLB_CFG    sizeof(struct ssid_glb_cfg)

struct limit_cfg
{
    uint8  days_on;      // bit map: bit 0=Saturday,     1=Friday, ... 5=Monday, 6=Sunday
    int16  time_on;      // minutes past midnight,      -1=disabled
    int16  time_off;     // minutes past midnight,      -1=disabled
    uint32 pps;          // traffic, packets per second, 0=disabled
    uint32 sta_pps;      // traffic, packets per second, 0=disabled
    uint32 kbps;         // traffic, Kbits   per second, 0=disabled
    uint32 sta_kbps;     // traffic, Kbits   per second, 0=disabled
    int16  stations;     // station limit,               0=disabled
} __attribute__ ((packed));

#define SIZE_LIMIT_CFG    sizeof(struct limit_cfg)

struct honeypot_bcast_cfg
{
    uint16 num_ssid;
    char   ssids[MAX_SIZE_HONEYPOT_WLIST][REQ_LEN_SSID];
} __attribute__ ((packed));

#define SIZE_HONEYPOT_BCAST_CFG     sizeof(struct honeypot_bcast_cfg)

struct honeypot_wlist_cfg
{
    uint16 num_ssid;
    char   ssids[MAX_SIZE_HONEYPOT_WLIST][REQ_LEN_SSID];
} __attribute__ ((packed));

#define SIZE_HONEYPOT_WLIST_CFG     sizeof(struct honeypot_wlist_cfg)

struct hp_bcast_str_cfg {
    char ssid[REQ_LEN_SSID];
} __attribute__ ((packed));

#define SIZE_HP_BCAST_STR_CFG     sizeof(struct hp_bcast_str_cfg)

struct hp_wlist_str_cfg {
    char ssid[REQ_LEN_SSID];
} __attribute__ ((packed));

#define SIZE_HP_WLIST_STR_CFG     sizeof(struct hp_wlist_str_cfg)

struct wlist_cfg
{
    uint16 num_entries;
    char   entries[MAX_SIZE_WLIST][REQ_LEN_HOSTNAME_STR];
} __attribute__ ((packed));

#define SIZE_WLIST_CFG    sizeof(struct wlist_cfg)

struct wlist_str_cfg {
    char entry[REQ_LEN_HOSTNAME_STR];
} __attribute__ ((packed));

#define SIZE_WLIST_STR_CFG    sizeof(struct wlist_str_cfg)

struct wpr_cfg
{
    uint8  enabled;
    uint8  login;
    uint8  splash;       // deprecated (here for backward compatibility)
    uint8  timeout;
    uint8  server;
    uint8  auth_type;
    uint8  https;
    int8   custom_file;
    char   secret      [REQ_LEN_RAD_SECRET];
    char   redirect_url[REQ_LEN_URL_STR];
    char   landing_url [REQ_LEN_URL_STR];
    char   background  [REQ_LEN_STR];
    char   logo_image  [REQ_LEN_STR];
    char   header_txt  [REQ_LEN_STR];
    char   footer_txt  [REQ_LEN_STR];
    uint16 auth_timeout;
    uint8  personal_wifi;   // used by Ssids
    uint8  https_passthru;
} __attribute__ ((packed));

#define SIZE_WPR_CFG    sizeof(struct wpr_cfg)

// NOTE this structure and the one below must match exactly
// up through those elements contained in ssid_init_cfg
struct ssid_cfg
{
    char   name[REQ_LEN_SSID];
    uint8  exists;
    uint8  enabled;
    uint8  active;
    uint8  fallback_enabled;
    uint8  fallback_active;
    uint8  mac_num;
    uint16 vlan;
    uint8  qos;
    uint8  flags;
    uint16 active_iaps;
    uint8  dflt_sec;
    uint8  enc_type;
    uint8  auth_type;
    uint8  mdm_auth;
    uint8  xrp_layer;
    uint8  uses_radius;
     int8  dhcp_pool;
    uint8  dhcp_opt;
    char   dhcp_pool_name[REQ_LEN_DHCP_POOL_NAME  ];
    char   filter_list   [REQ_LEN_FILTER_NAME     ];
    char   vlan_name     [REQ_LEN_VLAN_NAME       ];
    char   vlan_pool     [REQ_LEN_VLAN_NAME       ];
    char   pwifi_sta     [REQ_LEN_MAC_ADDR_STR + 1];
    uint32 date_on;
    uint32 date_off;
    uint32 expiration;
    uint8  dot11r_enable;
    struct security_cfg sec;
    struct radius_cfg rad;
    struct radius_acct_cfg rad_acct;
    struct limit_cfg limit;
    struct wpr_cfg wpr;
    struct wlist_cfg wlist;
    struct acl_cfg acl;
} __attribute__ ((packed));

#define SIZE_SSID_CFG    sizeof(struct ssid_cfg)

// NOTE this structure and the one above must match exactly
// up through those elements contained in ssid_init_cfg
struct ssid_init_cfg
{
    char   name[REQ_LEN_SSID];
    uint8  exists;
    uint8  enabled;
    uint8  active;
    uint8  fallback_enabled;
    uint8  fallback_active;
    uint8  mac_num;
    uint16 vlan;
    uint8  qos;
    uint8  flags;
    uint16 active_iaps;
    uint8  dflt_sec;
    uint8  enc_type;
    uint8  auth_type;
    uint8  mdm_auth;
    uint8  xrp_layer;
    uint8  uses_radius;
     int8  dhcp_pool;
    uint8  dhcp_opt;
    char   dhcp_pool_name[REQ_LEN_DHCP_POOL_NAME  ];
    char   filter_list   [REQ_LEN_FILTER_NAME     ];
    char   vlan_name     [REQ_LEN_VLAN_NAME       ];
    char   vlan_pool     [REQ_LEN_VLAN_NAME       ];
    char   pwifi_sta     [REQ_LEN_MAC_ADDR_STR + 1];
    uint32 date_on;
    uint32 date_off;
    uint32 expiration;
    uint8  dot11r_enable;
} __attribute__ ((packed));

#define SIZE_SSID_INIT_CFG   sizeof(struct ssid_init_cfg)

struct ssid_params_enc
{
    char name[REQ_LEN_SSID];
    char wep_keys[NUM_WEP_KEYS][2*REQ_LEN_WEP_KEY];
    char wpa_passphrase[2*REQ_LEN_WPA_PASSPHRASE];
    char rad_pri_secret[2*REQ_LEN_RAD_SECRET];
    char rad_sec_secret[2*REQ_LEN_RAD_SECRET];
    char rad_acct_pri_secret[2*REQ_LEN_RAD_SECRET];
    char rad_acct_sec_secret[2*REQ_LEN_RAD_SECRET];
    char wpr_secret[2*REQ_LEN_RAD_SECRET];
} __attribute__ ((packed));

#define SIZE_SSID_PARAMS_ENC sizeof(struct ssid_params_enc)

struct ssid_macnum_info
{
    char   name[REQ_LEN_SSID];
    int8   mac_num;
} __attribute__ ((packed));

#define SIZE_SSID_MACNUM_INFO    sizeof(struct ssid_macnum_info)

struct group_cfg
{
    char   name[REQ_LEN_GROUP];
    char   radius_id[REQ_LEN_GROUP];
    char   device_id[REQ_LEN_DEVICE_LIST_STR];
    uint8  exists;
    uint8  enabled;
    uint8  active;
    uint8  fallback_enabled;
    uint8  fallback_active;
    uint8  mac_num;
    uint16 vlan;
    uint8  qos;
    uint8  xrp_layer;
     int8  dhcp_pool;
    char   dhcp_pool_name[REQ_LEN_DHCP_POOL_NAME];
    char   filter_list   [REQ_LEN_FILTER_NAME   ];
    char   vlan_name     [REQ_LEN_VLAN_NAME     ];
    char   vlan_pool     [REQ_LEN_VLAN_NAME     ];
    struct limit_cfg limit;
    struct wpr_cfg wpr;
    struct wlist_cfg wlist;
} __attribute__ ((packed));

#define SIZE_GROUP_CFG    sizeof(struct group_cfg)

struct group_params_enc
{
    char name[REQ_LEN_GROUP];
    char wpr_secret[2*REQ_LEN_RAD_SECRET];
} __attribute__ ((packed));

#define SIZE_GROUP_PARAMS_ENC sizeof(struct group_params_enc)

struct group_macnum_info
{
    char   name[REQ_LEN_GROUP];
    int8   mac_num;
} __attribute__ ((packed));

#define SIZE_GROUP_MACNUM_INFO    sizeof(struct group_macnum_info)

struct cluster_array_cfg
{
    uint16 num_arrays;                      // Number of arrays in cluster
    char   cluster[REQ_LEN_CLUSTER];        // Name of the parent cluster
    char   name   [REQ_LEN_HOSTNAME_STR];   // Host name or ip address of the array
    char   ip_addr[REQ_LEN_IPV6_ADDRSTR];   // IP address of the array
    char   user   [REQ_LEN_STR];            // Admin user of the array
    char   passwd [REQ_LEN_STR*2];          // Admin password of the array (x2 for hex enc)
} __attribute__ ((packed));

#define SIZE_CLUSTER_ARRAY_CFG    sizeof(struct cluster_array_cfg)

struct station_cfg
{
    uint16 activity;
    uint16 reauth;
    uint16 auth_timeout;
} __attribute__ ((packed));

#define SIZE_STATION_CFG    sizeof(struct station_cfg)

struct venue_type_strs
{
    char strs[16][30];
    char Strs[16][50];
} __attribute__ ((packed));

struct venue_name_info
{
    char lang[4];
    char name[REQ_LEN_VENUE_NAME];
} __attribute__ ((packed));

#define SIZE_VENUE_NAME_INFO    sizeof(struct venue_name_info)

struct net_auth_type_info
{
    uint8 type;
    char  url[REQ_LEN_URL_STR];
} __attribute__ ((packed));

#define SIZE_NET_AUTH_TYPE_INFO     sizeof(struct net_auth_type_info)

struct auth_param
{
    uint8  id;
    uint8  value;
    uint32 vendor_id;
    uint32 vendor_type;
} __attribute__ ((packed));

#define SIZE_AUTH_PARAM     sizeof(struct auth_param)

struct eap_method_str
{
    uint8 value;
    char  str[30];
    char  Str[30];
} __attribute__ ((packed));

struct eap_method
{
    uint8  method;
    struct auth_param auth[MAX_NUM_AUTH_PARAMS];
} __attribute__ ((packed));

#define SIZE_EAP_METHOD     sizeof(struct eap_method)

struct nai_realm_info
{
    char   realm[REQ_LEN_NAI_REALM_STR];
    struct eap_method eap[MAX_NUM_EAP_METHODS];
} __attribute__ ((packed));

#define SIZE_NAI_REALM_INFO     sizeof(struct nai_realm_info)

struct cell_net_info
{
    char mcc[4];
    char mnc[4];
} __attribute__ ((packed));

#define SIZE_CELL_NET_INFO    sizeof(struct cell_net_info)

struct dot11u_cfg
{
    uint8  interworking;
    uint8  an_opts;
    uint8  venue_group;
    uint8  venue_type;
    char   hessid[REQ_LEN_MAC_ADDR_STR + 1];
    uint8  ipv4_avail;
    uint8  ipv6_avail;
    char   rcoi[MAX_NUM_ROAM_CONSORTIUM_OIS][REQ_LEN_ROAM_CONSORTIUM_OI];
    int    num_rcoi;
    char   domain[MAX_NUM_DOMAINS][REQ_LEN_DOMAIN_NAME];
    int    num_domain;
    struct cell_net_info cell_net[MAX_NUM_CELL_NETWORKS];
    int    num_cell_net;
    struct nai_realm_info nai_realm[MAX_NUM_NAI_REALMS];
    int    num_nai_realm;
    struct net_auth_type_info net_auth_type[MAX_NUM_NETWORK_AUTH_TYPES];
    int    num_net_auth_type;
    struct venue_name_info venue_name[MAX_NUM_VENUE_NAMES];
    int    num_venue_name;
}  __attribute__ ((packed));

#define SIZE_DOT11U_CFG sizeof(struct dot11u_cfg)

struct oper_name_info
{
    char lang[4];
    char name[REQ_LEN_OPER_NAME];
}   __attribute__ ((packed));

#define SIZE_OPER_NAME_INFO    sizeof(struct oper_name_info)

struct conn_cap_info
{
    char   name[REQ_LEN_CONN_CAP_NAME];
    uint8  protocol;
    uint16 port;
    uint8  status;
}  __attribute__ ((packed));

#define SIZE_CONN_CAP_INFO    sizeof(struct conn_cap_info)

struct hs20_cfg
{
    uint8  enable;
    uint8  dgaf;
    uint8  wan_link_status;
    uint32 wan_dlink_speed;
    uint32 wan_ulink_speed;
    struct conn_cap_info conn_cap[MAX_NUM_CONN_CAPS];
    int    num_conn_cap;
    struct oper_name_info oper_name[MAX_NUM_OPER_NAMES];
    int    num_oper_name;
}  __attribute__ ((packed));

#define SIZE_HS20_CFG sizeof(struct hs20_cfg)

struct iap_glb_summary
{
    struct radio_glb_cfg radio;
    struct station_cfg   station;
    struct dot11u_cfg    dot11u;
    struct hs20_cfg      hs20;
    struct ids_hdr       ids;
} __attribute__ ((packed));

#define SIZE_IAP_GLB_SUMMARY    sizeof(struct iap_glb_summary)

struct syslogd_cfg
{
    uint8  enabled;
    uint16 file_size;
    uint8  console;
    char   server[3] [REQ_LEN_HOSTNAME_STR];
    char   email_host[REQ_LEN_HOSTNAME_STR];
    char   email_user[REQ_LEN_HOSTNAME_STR];
    char   email_pswd[REQ_LEN_RAD_SECRET  ];
    char   email_from[REQ_LEN_HOSTNAME_STR];
    char   email_rcpt[REQ_LEN_HOSTNAME_STR];
    uint16 email_port;
    uint16 server_port[3];
    uint8  level;
    uint8  console_level;
    uint8  server_level[3];
    uint8  email_level;
    uint8  sta_fmt;
    uint8  time_fmt;
    uint8  sta_url_log;
//  uint8  cloud;
//  uint8  cloud_level;
} __attribute__ ((packed));

#define SIZE_SYSLOGD_CFG    sizeof(struct syslogd_cfg)

struct standby_cfg
{
    uint8 enabled;
    char  target_mac_addr[REQ_LEN_MAC_ADDR_STR + 1];
} __attribute__ ((packed));

#define SIZE_STANDBY_CFG    sizeof(struct standby_cfg)

struct cdp_cfg
{
    uint8  enabled;
    uint16 interval;
    uint16 hold_time;
} __attribute__ ((packed));

#define SIZE_CDP_CFG    sizeof(struct cdp_cfg)

struct lldp_cfg
{
    uint8  enabled;
    uint8  request_pwr;
    uint8  waiting_pwr;
    uint16 interval;
    uint16 hold_time;
    uint8  fa;
    char   fa_key[REQ_LEN_LLDP_FA_KEY_STR];
} __attribute__ ((packed));

#define SIZE_LLDP_CFG    sizeof(struct lldp_cfg)

struct netflow_cfg
{
    uint8  enabled;
    char   host1[REQ_LEN_HOSTNAME_STR];
    uint16 port1;
} __attribute__ ((packed));

#define SIZE_NETFLOW_CFG    sizeof(struct netflow_cfg)

struct wifi_tag_cfg
{
    uint8  enabled;
    uint16 udp_port;
    uint16 channel[MAX_WIFI_TAG_CHANNELS];
    char   ip_addr[REQ_LEN_IPV6_ADDRSTR];
    char   server [REQ_LEN_IPV6_ADDRSTR];
} __attribute__ ((packed));

#define SIZE_WIFI_TAG_CFG    sizeof(struct wifi_tag_cfg)

struct location_cfg
{
    uint8  enabled;
    char   url[REQ_LEN_URL_STR];
    char   key[REQ_LEN_STR];
    uint16 period;
    uint8  per_radio_data ;
    uint8  rf_finger_print;
    uint8  include_random ;
    uint8  max_data_points;
    float  bias_base;
    float  bias_range;
    float  bias_factor;
    uint16 test_stations;
} __attribute__ ((packed));

#define SIZE_LOCATION_CFG    sizeof(struct location_cfg)

struct airwatch_cfg
{
    char   api_url[REQ_LEN_URL_STR];
    char   api_key[REQ_LEN_STR];
    char   api_username[REQ_LEN_STR];
    char   api_password[REQ_LEN_STR];
    uint16 api_timeout;
    uint16 api_poll_period;
    uint8  api_access_error;
    char   redirect_url[REQ_LEN_URL_STR];
} __attribute__ ((packed));

#define SIZE_AIRWATCH_CFG    sizeof(struct airwatch_cfg)

struct mdm_cfg
{
    struct airwatch_cfg aw;
} __attribute__ ((packed));

#define SIZE_MDM_CFG    sizeof(struct mdm_cfg)

struct eth_stats
{
    uint16 vlan;
    char   name[REQ_LEN_VLAN_NAME];
    uint8  status;
    uint8  link;
    uint8  duplex;
    uint32 speed;
    uint64 rx_bytes;
    uint64 rx_packets;
    uint64 rx_errors;
    uint64 rx_dropped;
    uint64 rx_fifo_errors;
    uint64 rx_frame_errors;
    uint64 rx_compressed;
    uint64 rx_multicast;
    uint64 tx_bytes;
    uint64 tx_packets;
    uint64 tx_errors;
    uint64 tx_dropped;
    uint64 tx_fifo_errors;
    uint64 tx_collisions;
    uint64 tx_carrier_errors;
    uint64 tx_compressed;
} __attribute__ ((packed));

#define SIZE_ETH_STATS  sizeof(struct eth_stats)

#define vlan_stats      eth_stats
#define SIZE_VLAN_STATS sizeof(struct vlan_stats)

struct stp_iface_state
{
    char  state[REQ_LEN_STP_STATE];
    uint8 root;                             // true if designated_root == designated_bridge
} __attribute__ ((packed));

struct stp_state_hdr
{
    uint16 vlan_num;
    char   vlan_name[REQ_LEN_VLAN_NAME];
    uint8  br_root;                         // true if designated_root == bridge_id
} __attribute__ ((packed));

#define SIZE_STP_STATE_HDR  sizeof(struct stp_state_hdr)

struct stp_state
{
    uint16 vlan_num;
    char   vlan_name[REQ_LEN_VLAN_NAME];
    uint8  br_root;                         // true if designated_root == bridge_id
    struct stp_iface_state gig[MAX_GIG_ETH_INTERFACES];
    struct stp_iface_state iap;
    struct stp_iface_state wds_client_link[MAX_NUM_WDS_CLIENT_LINKS];
    struct stp_iface_state wds_host_link[MAX_NUM_WDS_HOST_LINKS];
} __attribute__ ((packed));

#define SIZE_STP_STATE  sizeof(struct stp_state)

struct admin_history_info
{
    char   user[REQ_LEN_USERNAME_STR];
    char   ip_addr[REQ_LEN_IPV6_ADDRSTR];
    uint8  iface;                           // 0 = cli console, 1 = cli telnet, 2 = cli ssh, 3 = wmi https
    uint32 session_id;
    uint32 login_time;
    uint32 logout_time;
} __attribute__ ((packed));

#define SIZE_ADMIN_HISTORY_INFO    sizeof(struct admin_history_info)

struct old_position
{
    int8   rssi;
    int16  angle;
    int16  distance;
    int16  x;
    int16  y;
} __attribute__ ((packed));

#define SIZE_OLD_POSITION    sizeof(struct old_position)

struct sta_loc_ap
{
    struct MAC_ADDR addr;
    int8   index;
    int8   rssi;
    int8   bias;
    int8   bias_pt;
    int8   ssid;
    int8   group;
    int8   assoc;
    uint32 time;
} __attribute__ ((packed));

#define SIZE_STA_LOC_AP    sizeof(struct sta_loc_ap)

struct sta_location_info
{
    struct MAC_ADDR sta;
    int8   rssi;
    int8   assoc;
    int8   num;
    int8   old;
    uint32 time;
    struct coordinates coordinate;
    struct sta_loc_ap aps[MAX_NUM_LOCATION_APS];
} __attribute__ ((packed));

#define SIZE_STA_LOCATION_INFO    sizeof(struct sta_location_info)

struct station_info
{
    char   mac[REQ_LEN_MAC_ADDR_STR + 1];
    char   ipv4_addr[REQ_LEN_IPV4_ADDRSTR];
    char   ipv6_addr[REQ_LEN_IPV6_ADDRSTR];
    int8   iface;
    char   ssid[REQ_LEN_SSID];
    char   group[REQ_LEN_GROUP];
    uint16 vlan;
    uint8  qos;
    uint8  xrp_layer;
    char   user        [REQ_LEN_USERNAME_STR];  // user name for machines when we can get it
    char   name        [REQ_LEN_NETBIOS_STR ];  // netbios name for machines when we can get it
    char   manufacturer[REQ_LEN_MFGNAME_STR ];  // manufacturer name from oui lookup
    char   hostname    [REQ_LEN_HOSTNAME_STR];  // hostname from dhcp request
    char   user_agent  [NUM_USER_AGENT_STRS ][REQ_LEN_USER_AGENT_STR];  // user-agent string from http get request
    char   device_type [REQ_LEN_DEVICE_STR  ];
    char   device_class[REQ_LEN_DEVICE_STR  ];
    int8   device_source;
    int16  device_source_idx;
    int8   rssi;
    int8   snr;
    int8   silence;
    uint32 time_assoc;                          // time associated in seconds
    int8   probe_req_rssi     [MOM_NUM_RADIO_INTERFACES];
    int8   probe_req_snr      [MOM_NUM_RADIO_INTERFACES];
    int8   probe_req_silence  [MOM_NUM_RADIO_INTERFACES];
    uint32 probe_req_timestamp[MOM_NUM_RADIO_INTERFACES];
    uint32 tx_rate;
    uint8  tx_rate_10th;
    uint32 rx_rate;
    uint8  rx_rate_10th;
    uint32 stats_time_period;
    uint8  enc_type;
    uint8  cipher;
    uint8  key_mgmt;
    uint64 basic_rates;
    uint64 supp_rates;
    uint64 ht_basic_rates;
    uint64 ht_supp_rates;
    uint64 probe_ht_rates;
    uint8  band;
    uint8  channel;
    uint8  channel_bond_40mhz;
    uint8  channel_bond_80mhz;
    uint8  channel_bond_160mhz;
    uint8  media_type;
    uint8  media_streams;
    uint8  op_mode;
    uint8  op_streams;
    uint64 associations;
    uint32 last_alarm;                          // time of last sta assure alarm in seconds
    uint8  alarm_status;                        // 0 = none, 1 = > 1 day, 2 = < 1 day, 3 = active
    struct old_position position;
    struct coordinates  coordinate;
    struct simple_stats stats;
    struct pkt_type_info mgmt;
    struct pkt_type_info data;
    struct location_stats location;
} __attribute__ ((packed));

#define SIZE_STA_INFO sizeof(struct station_info)

struct sta_type_info
{
    char   mac[REQ_LEN_MAC_ADDR_STR + 1];
    uint8  media_type;
    uint8  txrx_streams;
} __attribute__ ((packed));

#define SIZE_STA_TYPE_INFO sizeof(struct sta_type_info)

struct sta_assure_info
{
    int32  rev_sorting_idx;
    int32  last_deauth_time;    // for roaming assist
    int32  last_deauth_count;   // for roaming assist
    int32  last_alarm_time;
    int32  trigger_is_active;
    int32  alarm_is_active;
    struct station_info      info;
    struct sta_assure_params trigger;
    struct sta_assure_params alarm;
    struct sta_assure_params value;
    struct sta_assure_params threshold;
    struct sta_assure_params time_stamp;
} __attribute__ ((packed));

#define SIZE_STA_ASSURE_INFO    sizeof(struct sta_assure_info)

struct lease_info
{
    char   mac_addr [REQ_LEN_MAC_ADDR_STR + 1];
    char   ipv4_addr[REQ_LEN_IPV4_ADDRSTR];
    char   hostname [REQ_LEN_STR];
    char   beg_time [REQ_LEN_LEASE_TIME];
    char   end_time [REQ_LEN_LEASE_TIME];
    char   timeleft [REQ_LEN_LEASE_TIME];
} __attribute__ ((packed));

#define SIZE_LEASE_INFO    sizeof(struct lease_info)

struct component_info {
    char serial  [REQ_LEN_VERSION];
    char part    [REQ_LEN_VERSION];
    char date    [REQ_LEN_VERSION];
    char mfg_site[REQ_LEN_VERSION];
} __attribute__ ((packed));

#define SIZE_COMPONENT_INFO sizeof(struct component_info)

// must match struct ioctl_get_version definition in xirmac.h
struct driver_fpga_info
{
    char   driver_version[REQ_LEN_DRIVER_VERSION];
    uint16 floyd_version;
    uint16 opie_version;
    uint16 andy_version;
    uint16 rincon_version;
    uint16 laguna_version;
    uint16 topanga_version;
    uint16 mugu_version;
    uint8  scd_maj_rev;
    uint8  scd_min_rev;
    uint8  scd_month;
    uint8  scd_year;
} __attribute__ ((packed));

#define SIZE_DRIVER_FPGA_INFO sizeof(struct driver_fpga_info)

struct version_info
{
    char   sw_version [REQ_LEN_SOFTWARE_VERSION];
    char   sw_name    [REQ_LEN_VERSION];
    char   license_key[REQ_LEN_LONG_VERSION];
    char   license_exp[REQ_LEN_LONG_VERSION];
    char   license_dur[REQ_LEN_LONG_VERSION];
    struct xirrus_key license;
    struct component_info system;
    struct component_info cpu;
    struct component_info iap_mod[MOM_NUM_RADIO_MODULES];
    char   eth_mac_base[REQ_LEN_VERSION];
    char   iap_mac_base[REQ_LEN_VERSION];
    char   boot_loader[REQ_LEN_SOFTWARE_VERSION];
    char   scd_version[REQ_LEN_LONG_VERSION];
    char   fpga_name_str[4][REQ_LEN_VERSION];
    char   fpga_boot_ver[4][REQ_LEN_VERSION];
    char   fpga_curr_ver[4][REQ_LEN_VERSION];
    char   xbl_name[REQ_LEN_VERSION];
    char   scd_name[REQ_LEN_VERSION];
    char   flash[REQ_LEN_VERSION];
    char   driver_version[REQ_LEN_DRIVER_VERSION];
    char   time_this_boot[REQ_LEN_LONG_VERSION];
    char   time_last_boot[REQ_LEN_LONG_VERSION];
    char   iap_mac_block [REQ_LEN_LONG_VERSION];
    char   eth_mac_block[MOM_NUM_ETH_INTERFACES][REQ_LEN_VERSION];
    char   mod_centi[5][5];
    char   mod_faren[5][5];
    char   fan_speed[5];
    char   compass_heading[4];
    char   parent_name [REQ_LEN_VERSION];
    char   parent_lower[REQ_LEN_VERSION];
    char   brand_name  [REQ_LEN_VERSION];
    char   brand_lower [REQ_LEN_VERSION];
    char   model_type  [REQ_LEN_VERSION];
    char   model_lower [REQ_LEN_VERSION];
    char   base_model  [REQ_LEN_VERSION];
    char   model  [REQ_LEN_LONG_VERSION];
    char   dpi_signatures[REQ_LEN_LONG_VERSION];
    int32  model_number;
    int32  memory;
    int32  speed;
    int32  cores;
    int32  tlbs;
    int32  num_slots;
    int32  num_radio_modules;
    int32  num_eth_interfaces;
} __attribute__ ((packed));

#define SIZE_VERSION_INFO sizeof(struct version_info)

struct geography_info
{
    uint8 active_channels   [MAX_NUM_CHANNELS];
    uint8 auto_2ghz_channels[MAX_BG_AUTO_SET ];
    uint8 auto_2ghz_3chn_set[MAX_BG_AUTO_SET ];
    uint8 auto_2ghz_4chn_set[MAX_BG_AUTO_SET ];
    uint8 auto_5ghz_channels[MAX_A_CHANNELS  ];
    uint8 radar_channels    [MAX_A_CHANNELS  ];
    uint8 default_channels  [MOM_NUM_RADIO_INTERFACES];
    uint8 power_limits      [MAX_NUM_CHANNELS];
    uint8 active_2ghz_chan  [MAX_BG_CHANNELS ];
    uint8 active_5ghz_chan  [MAX_A_CHANNELS  ];
    uint8 auto_2ghz_list    [MAX_BG_CHANNELS ];
    uint8 auto_5ghz_list    [MAX_A_CHANNELS  ];
} __attribute__ ((packed));

#define SIZE_GEOGRAPHY_INFO sizeof(struct geography_info)

struct array_counts_info
{
    uint8  iaps;
    uint8  iaps_up;
    uint8  ssids;
    uint8  ssids_up;
    uint32 time_up;
    uint16 stations;
} __attribute__ ((packed));

#define SIZE_ARRAY_COUNTS_INFO sizeof(struct array_counts_info)

struct fixed_addr_info {
    char mac_addr[REQ_LEN_MAC_ADDR_STR + 1];
    char group[REQ_LEN_GROUP];
} __attribute__ ((packed));

#define SIZE_FIXED_ADDR_INFO sizeof(struct fixed_addr_info)

// must match definition of mac_rssi_entry in xirstats.h
struct sta_rssi_entry {
    struct MAC_ADDR sta;                            // mac addr of sta
    s8  ssid ;                                      // ssid index of sta
    s8  group;                                      // user group index of sta
    s8  rssi ;                                      // rssi of sta
    u32 time ;                                      // time stamp when last heard
} __attribute__ ((packed));

#define SIZE_STA_RSSI_ENTRY sizeof(struct sta_rssi_entry)

struct station_rssi_info
{
    uint16 count;
    void  *table;           // opaque pointer (meaningful only in OOPME)
} __attribute__ ((packed));

#define SIZE_STATION_RSSI_INFO sizeof(struct station_rssi_info)

struct location_chk_info
{
    struct coordinates calculated;
    struct sta_loc_ap  aps[MAX_NUM_LOCATION_APS];
    float  location_error;
    float  angular_stddev;
} __attribute__ ((packed));

#define is_interior(angular_stddev)     ((angular_stddev) >= 1.0)
#define SIZE_LOCATION_CHK_INFO          sizeof(struct location_chk_info)

#ifdef CFG_CHK_LOCATION_BOUNDS
struct map_boundary_info
{
    struct coordinates minimum;
    struct coordinates maximum;
} __attribute__ ((packed));

#define SIZE_MAP_BOUNDARY_INFO          sizeof(struct map_boundary_info)
#endif

// pthread_mutex_t locks must be 64-bit aligned on MIPS-64bit systems due to the underlying asm code
// that's why the lock is placed after padding out the hostname, and the struct is declared aligned(8)
struct neighbor_array_info
{
    char   hostname[REQ_LEN_HOSTNAME_STR];      // first for sorting and dedupping
    char   pad[8 - (REQ_LEN_HOSTNAME_STR % 8)]; // lock must be 64-bit aligned
    pthread_mutex_t lock;                       // Neighbor lock
    int    lockOrder;
    struct MAC_ADDR  mac ;
    struct IP_ADDR   ip  ;
    struct IPV6_ADDR ipv6[MAX_NUM_IPV6_IFACE_ADDRS];
    char   mac_str [REQ_LEN_MAC_ADDR_STR + 1];
    char   location[REQ_LEN_STR];
    char   serial  [REQ_LEN_VERSION];
    char   sw_name [REQ_LEN_VERSION];
    char   xbl_name[REQ_LEN_VERSION];
    char   scd_name[REQ_LEN_VERSION];
    char   flash   [REQ_LEN_VERSION];
    char   model   [REQ_LEN_LONG_VERSION];
    char   license [REQ_LEN_LONG_VERSION];
    char   recovery[REQ_LEN_STR];
    uint16 features;
    uint32 last_beacon_rx;
    uint32 last_update;
    uint8  in_range;
     int8  rssi;
     int8  rssi_adjust;
     int8  path_loss;
     int8  bias_point;
    uint8  connected;
    uint8  xrp_mode;
    uint8  channel_list[MOM_IAPS + 1];
    uint8  ft_info[MAX_FT_DATA_LEN];
    uint32 ft_info_len;
    float  distance;
    float  attenuation;
    struct sta_rssi_entry    rssi_entry  ;
    struct array_counts_info counts      ;
    struct max_stations_info max_stations;
    struct sta_type_cnt_info sta_type_cnt;
    struct position_mnt_info position_mnt;
    struct location_chk_info location_chk;
    struct station_rssi_info station_rssi;
    struct station_rssi_info   rogue_rssi;
    struct group_macnum_info group_macnum[MAX_NUM_GROUP];
    struct  ssid_macnum_info  ssid_macnum[MAX_NUM_SSID ];
#ifdef CFG_CHK_LOCATION_BOUNDS
    struct map_boundary_info map_boundary;
#endif
} __attribute__ ((packed, aligned(8)));

#define SIZE_NEIGHBOR_ARRAY_INFO sizeof(struct neighbor_array_info)

struct neighbor_tunnel_info
{
    char   hostname[REQ_LEN_HOSTNAME_STR];
    struct IP_ADDR remote_ip;
    int32  type;
    int32  vlan;
    int32  pid;
    int32  sock;
    int32  local_port;
    int32  remote_port;
    struct IP_ADDR local_end_point;
    struct IP_ADDR remote_end_point;
    uint32 start_time;
    uint32 uptime;
} __attribute__ ((packed));

#define SIZE_NEIGHBOR_TUNNEL_INFO sizeof(struct neighbor_tunnel_info)

struct roam_sta_info
{
    char   hostname[REQ_LEN_HOSTNAME_STR];
    struct IP_ADDR neighbor_ip;
    struct MAC_ADDR  mac;
    int32  vlan;
} __attribute__ ((packed));

#define SIZE_ROAM_STA_INFO sizeof(struct roam_sta_info)

struct rogue_ap_info
{
    struct   MAC_ADDR bssid;
    struct   MAC_ADDR source;
    char     ssid[REQ_LEN_SSID];
    char     mfr[ REQ_LEN_MFGNAME_STR ];
    uint8    channel;
     int8    ht_bond;
     int8    vht_bond;
    uint8    radio;
    uint32   time_origin;
    uint32   time_recent;
    uint8    active;        // MUST follow time_recent for XML generator to work
    uint16   encryption;
    uint64   timestamp;
    uint16   capability;
    int8     rssi;
    int8     snr;
    int8     silence;
    struct   IP_ADDR  ip_addr;
    struct   MAC_ADDR base_mac ;
    int16    xrp_socket;
    int16    stations;
    int8     tx_power;
    uint64   count;
    uint8    autoblock_samples;
    uint8    autoblocked;
    uint8    type;
    uint8    net_type;
    uint8    evil_twin_status;
    struct   location_stats location;
    struct   coordinates  coordinate;
} __attribute__ ((packed));

#define SIZE_ROGUE_AP_INFO sizeof(struct rogue_ap_info)

struct rogue_ap_type_info
{
    struct   MAC_ADDR bssid;
    uint16   type;
} __attribute__ ((packed));

#define SIZE_ROGUE_AP_TYPE_INFO sizeof(struct rogue_ap_type_info)

struct rogue_ap_cnt_info
{
    uint16 known;
    uint16 unknown;
    uint16 approved;
    uint16 blocked;
    uint16 sumall_aps;
} __attribute__ ((packed));

#define SIZE_ROGUE_AP_CNT_INFO sizeof(struct rogue_ap_cnt_info)

struct ids_event_info
{
    uint16 event;
    char   event_msg[REQ_LEN_IDS_EVENT_STR];
    uint8  radio;
    uint8  channel;
    uint32 timestamp;
    uint32 period;
    uint32 cur_pkts;
    uint32 avg_pkts;
    uint32 max_pkts;
    struct MAC_ADDR mac;
    char   ssid[REQ_LEN_SSID];
} __attribute__ ((packed));

#define SIZE_IDS_EVENT_INFO sizeof(struct ids_event_info)

struct autocell_rssi_info
{
    struct MAC_ADDR  array_mac; // base iap mac address of the array for this rssi
    struct IP_ADDR   array_ip;  // ip address of the array for this rssi
    int8   rssi;                // rssi of the packet
    int8   tx_pwr;              // tx power of the packet
    int8   path_loss;           // path loss for the packet (tx_power - rssi)
    int8   min_path_loss;       // minimum path loss for the packet
    int8   max_path_loss;       // maximum path loss for the packet
    int8   timeout;             // aging timer for packets
    uint8  channel;             // channel packet was received on
} __attribute__ ((packed));

#define SIZE_AUTOCELL_RSSI_INFO sizeof(struct autocell_rssi_info)

struct autochannel_info
{
     int8  iface;
    uint8  running;
    uint8  state;
    uint8  neg_status;
    uint16 neg_time;
    uint16 num_arrays;
    uint16 search_status;
    uint8  scan_status;
    uint8  took_monitor_down;
    uint8  chan_list_a[MAX_A_CHANNELS];
    uint8  chan_list_g[MAX_BG_CHANNELS];
    struct {
        uint8 deferred;
        uint8 module;
        uint8 create_restore_pt;
        char  restore_pt_name[REQ_LEN_FILENAME];
    } save;
} __attribute__ ((packed));

#define SIZE_AUTOCHANNEL_INFO sizeof(struct autochannel_info)

struct env_ctrl_info
{
    int32 temperature;
    int32 humidity;
    int32 rpm[6];
    int8  array_on;
    int8  cool_on;
    int8  heat_on;
    char  sw_version[REQ_LEN_STR];
} __attribute__ ((packed));

#define SIZE_ENV_CTRL_INFO  sizeof(struct env_ctrl_info)
#define SIZE_ENV_CTRL_HDR  (sizeof(struct env_ctrl_info) - REQ_LEN_STR)

struct cdp_info
{
    char  hostname    [REQ_LEN_CDP_ARRAY];
    char  interface   [REQ_LEN_CDP_ARRAY];
    char  native_vlan [REQ_LEN_CDP_ARRAY];
    char  ipv4_address[REQ_LEN_CDP_ARRAY];
    char  networks    [REQ_LEN_CDP_ARRAY];
    char  model       [REQ_LEN_CDP_ARRAY];
    char  capabilities[REQ_LEN_CDP_ARRAY];
    char  software    [MAX_STR];
    int32 time_to_live;
} __attribute__ ((packed));

#define SIZE_CDP_INFO  sizeof(struct cdp_info)

struct xm_console_cfg {
    uint32 timeout;
    uint8  mgmt;
    uint8  exists;
} __attribute__ ((packed));

struct xm_iap_glb_cfg {
    uint8  mgmt;
} __attribute__ ((packed));

struct xm_license_cfg {
    char   license_key[REQ_LEN_LONG_VERSION];
    char   license_exp[REQ_LEN_LONG_VERSION];
    char   license_dur[REQ_LEN_LONG_VERSION];
    struct xirrus_key license;
} __attribute__ ((packed));

struct ext_mgmt_cfg
{
    struct mgmt_cfg       mgmt;
    struct standby_cfg    standby;
    struct xm_console_cfg console;
    struct xm_iap_glb_cfg iap_glb;
    struct xm_license_cfg info;
    int32  save_flag;
    uint8  factory_reset;
    uint8  xms_managed;
    uint8  remote_recovery;
    uint8  no_flash;
} __attribute__ ((packed));

#define SIZE_EXT_MGMT_CFG   sizeof(struct ext_mgmt_cfg)

struct cfg_diff
{
    char filename1[MAX_STR/4];
    char filename2[MAX_STR/4];
    char section  [MAX_STR/4];
} __attribute__ ((packed));

#define SIZE_CFG_DIFF sizeof(struct cfg_diff)

struct section_info
{
    char    name[REQ_LEN_STR];
    char    start_text[REQ_LEN_STR];
    char   *start_format;
    char   *end_text;
    int    *list;
    int   (*handler)(int, FILE*, int);
    struct  section_info *sub_section;
    int     system;
    int     license;
} __attribute__ ((packed));

#define SIZE_SECTION_INFO sizeof(struct section_info)

struct section_cfg
{
    char    name[REQ_LEN_STR];
} __attribute__ ((packed));

#define SIZE_SECTION_CFG sizeof(struct section_cfg)

struct user_info
{
    char id[REQ_LEN_STR];
    char passwd[REQ_LEN_STR];
} __attribute__ ((packed));

#define SIZE_USER_INFO sizeof(struct user_info)

struct login_user_info
{
    char  id[REQ_LEN_USERNAME_STR];
    char  passwd[REQ_LEN_STR];
    char  ip_addr[REQ_LEN_IPV6_ADDRSTR];
    int32 iface;
    int32 session_id;
    int32 attempts;
    int32 privnum;                          // here for backward compatibilty in cluster mode
    int32 force;                            // here for backward compatibilty in cluster mode
    char  priv_name[REQ_LEN_PRIV_NAME_STR];
} __attribute__ ((packed));

#define SIZE_LOGIN_USER_INFO sizeof(struct login_user_info)

struct raduser_info
{
    char id[REQ_LEN_STR];
    char passwd[REQ_LEN_STR];
    char ssid[REQ_LEN_SSID];
    char group[REQ_LEN_GROUP];
    char encpass[2*REQ_LEN_STR];
} __attribute__ ((packed));

#define SIZE_RADUSER_INFO sizeof(struct raduser_info)

struct raduser_read_struct_info
{
    char id[REQ_LEN_STR];
    char passwd[REQ_LEN_STR];
    char ssid[REQ_LEN_SSID];
    char group[REQ_LEN_GROUP];
} __attribute__ ((packed));

#define SIZE_RADUSER_READ_STRUCT_INFO sizeof(struct raduser_read_struct_info)

struct radpasswd_read_struct_info
{
    char passwd[REQ_LEN_STR];
} __attribute__ ((packed));

#define SIZE_RADPASSWD_READ_STRUCT_INFO sizeof(struct radpasswd_read_struct_info)

struct radpasswdenc_read_struct_info
{
    char passwdenc[2*REQ_LEN_STR];
} __attribute__ ((packed));

#define SIZE_RADPASSWDENC_READ_STRUCT_INFO sizeof(struct radpasswdenc_read_struct_info)

struct raduser
{
    char  id[REQ_LEN_STR];
    char  passwd[REQ_LEN_STR];
    uint8 ssid;
    uint8 group;
} __attribute__ ((packed));

#define SIZE_RADUSER sizeof(struct raduser)

struct raduser_var_size
{
    uint8 ssid;
    char  id[1];
    char  passwd[1];
    uint8 group;
} __attribute__ ((packed));

#define SIZE_RADUSER_VAR_SIZE sizeof(struct raduser_var_size)

struct local_radius_cfg
{
    uint8  enabled;
    struct raduser users[MAX_NUM_RADIUS_USERS];
} __attribute__ ((packed));

#define SIZE_LOCAL_RADIUS_CFG sizeof(struct local_radius_cfg)

struct flattened_filter_addr
{
    uint8  type;
    uint8  iface;
    uint8  device;
    uint16 vlan;
    int8   ssid_index;
    char   ssid_name[REQ_LEN_SSID];
    int8   group_index;
    char   group_name[REQ_LEN_GROUP];
    uint8  ipv4_mask_bits;
    char   ipv4_mask[REQ_LEN_IPV4_ADDRSTR];
    char   ipv4_base[REQ_LEN_IPV4_ADDRSTR];
    uint8  ipv6_mask_bits;
    char   ipv6_mask[REQ_LEN_IPV6_ADDRSTR];
    char   ipv6_base[REQ_LEN_IPV6_ADDRSTR];
    uint8  mac_mask_bits;
    char   mac_mask[REQ_LEN_MAC_ADDR_STR+1];
    char   mac_base[REQ_LEN_MAC_ADDR_STR+1];
} __attribute__ ((packed));

#define SIZE_FLATTENED_FILTER_ADDR sizeof(struct flattened_filter_addr)

struct filter_addr
{
    uint8 type;
    struct filter_addr_info {
        uint8  iface;
        uint8  device;
        uint16 vlan;
        struct {
            int8  index;
            char name[REQ_LEN_SSID];
        } __attribute__ ((packed)) ssid;
        struct {
            int8  index;
            char name[REQ_LEN_GROUP];
        } __attribute__ ((packed)) group;
        struct {
            uint8 mask_bits;
            char  mask[REQ_LEN_IPV4_ADDRSTR];
            char  base[REQ_LEN_IPV4_ADDRSTR];
        } __attribute__ ((packed)) ipv4;
        struct {
            uint8 mask_bits;
            char  mask[REQ_LEN_IPV6_ADDRSTR];
            char  base[REQ_LEN_IPV6_ADDRSTR];
        } __attribute__ ((packed)) ipv6;
        struct {
            uint8 mask_bits;
            char  mask[REQ_LEN_MAC_ADDR_STR+1];
            char  base[REQ_LEN_MAC_ADDR_STR+1];
        } __attribute__ ((packed)) mac;
    } __attribute__ ((packed)) addr;
} __attribute__ ((packed));

#define SIZE_FILTER_ADDR sizeof(struct filter_addr)

// NOTE this structure is written out in its entirety to the eeprom
// ONLY add new members to the end of this structure for backward compaibility
struct filter_packed_addr
{
    uint8 type;
    union {
        uint8  iface;
        uint8  device;
        uint16 vlan;
        struct {
            int8 index;
        } __attribute__ ((packed)) ssid;
        struct {
            int8 index;
        } __attribute__ ((packed)) group;
        struct {
            uint8 mask_bits;
            uint8 base[4];
        } __attribute__ ((packed)) ipv4;
        struct {
            uint8 mask_bits;
            uint8 base[6];
        } __attribute__ ((packed)) mac;
    } __attribute__ ((packed)) addr;
} __attribute__ ((packed));

#define SIZE_FILTER_PACKED_ADDR sizeof(struct filter_packed_addr)

struct filter_packed_ipv6_addr
{
    union {
        struct {
            uint8 mask_bits;
            uint8 base[16];
        } __attribute__ ((packed)) ipv6;
    } __attribute__ ((packed)) addr;
} __attribute__ ((packed));

#define SIZE_FILTER_PACKED_IPV6_ADDR sizeof(struct filter_packed_ipv6_addr)

// NOTE this structure is written out in its entirety to the eeprom
// DO NOT add new members to this structure for backward compaibility
struct filter_header
{
    char   name[REQ_LEN_FILTER_NAME];
    uint8  type;
    uint16 vlan;
    uint8  protocol;
    uint16 port;
} __attribute__ ((packed));

#define SIZE_FILTER_HEADER sizeof(struct filter_header)

// NOTE the first half of this structure is written out in its entirety to the eeprom
// ONLY add new members to this structure PAST the port for backward compaibility
struct filter_cfg
{
    char   name[REQ_LEN_FILTER_NAME];
    uint8  type;
    uint16 vlan;
    uint8  protocol;
    uint16 port;
    uint16 port_range;
    uint32 app_index;
    char   app_ident[REQ_LEN_GUID_STR];
    uint8  set_dscp;
    uint8  limit_type;
    uint32 limit_value;
    int16  time_on;      // minutes past midnight,   -1=disabled
    int16  time_off;     // minutes past midnight,   -1=disabled
    uint8  days_on;      // bit map: bit 0=Saturday,  1=Friday, ... 5=Monday, 6=Sunday
    struct filter_addr src;
    struct filter_addr dst;
    char   set_ipv4[REQ_LEN_IPV4_ADDRSTR];
    char   set_ipv6[REQ_LEN_IPV6_ADDRSTR];
    uint8  priority;
    uint64 packets;
    uint64 bytes;
} __attribute__ ((packed));

#define SIZE_FILTER_CFG sizeof(struct filter_cfg)

// NOTE this structure is written out in its entirety to the eeprom
// ONLY add new members to the end of this structure for backward compaibility
struct filter_packed_cfg
{
    char   name[REQ_LEN_FILTER_NAME];
    uint8  type;
    uint16 vlan;
    uint8  protocol;
    uint16 port;
    struct filter_packed_addr src;
    struct filter_packed_addr dst;
    uint16 port_range;
    char   app_ident[REQ_LEN_GUID_STR];
    uint8  set_dscp;
    uint8  limit_type;
    uint32 limit_value;
    int16  time_on;      // minutes past midnight,   -1=disabled
    int16  time_off;     // minutes past midnight,   -1=disabled
    uint8  days_on;      // bit map: bit 0=Saturday,  1=Friday, ... 5=Monday, 6=Sunday
    struct filter_packed_ipv6_addr ipv6_src;
    struct filter_packed_ipv6_addr ipv6_dst;
    uint8  set_ipv4[4];
    uint8  set_ipv6[16];
} __attribute__ ((packed));

#define SIZE_FILTER_PACKED_CFG sizeof(struct filter_packed_cfg)

// NOTE this structure must match the following structure
// and is used to access just the header elements
struct filter_list_header_cfg
{
    char  name[REQ_LEN_FILTER_NAME];
    uint8 exists;
    uint8 enabled;
    uint8 active;
    uint8 stateful;     // only meaningful for filter_list[0] (global filters)
    uint8 dpi;          // only meaningful for filter_list[0] (global filters)
    uint8 num;
}  __attribute__ ((packed));

#define SIZE_FILTER_LIST_HEADER_CFG sizeof(struct filter_list_header_cfg)

struct filter_list_cfg
{
    char  name[REQ_LEN_FILTER_NAME];
    uint8 exists;
    uint8 enabled;
    uint8 active;
    uint8 stateful;     // only meaningful for filter_list[0] (global filters)
    uint8 dpi;          // only meaningful for filter_list[0] (global filters)
    uint8 num;
    struct filter_cfg entry[MAX_NUM_FILTERS];
}  __attribute__ ((packed));

#define SIZE_FILTER_LIST_CFG sizeof(struct filter_list_cfg)

// NOTE this structure must match the following structure
// and is used to access just the header elements
struct app_list_header_cfg
{
    char  list_guid[REQ_LEN_GUID_STR];
    char  desc[REQ_LEN_APP_LIST_DESC];
    uint8 exists;
    uint8 num;
}  __attribute__ ((packed));

#define SIZE_APP_LIST_HEADER_CFG sizeof(struct app_list_header_cfg)

struct app_list_entry_cfg
{
    char  guid[REQ_LEN_GUID_STR];
}  __attribute__ ((packed));

#define SIZE_APP_LIST_ENTRY_CFG sizeof(struct app_list_entry_cfg)

struct app_list_cfg
{
    char  list_guid[REQ_LEN_GUID_STR];
    char  desc[REQ_LEN_APP_LIST_DESC];
    uint8 exists;
    uint8 num;
    struct app_list_entry_cfg entry[MAX_NUM_APPS_PER_LIST];
}  __attribute__ ((packed));

#define SIZE_APP_LIST_CFG sizeof(struct app_list_cfg)

struct mdm_msg
{
    uint8  type;
    uint8  mac[REQ_LEN_ETH_ADDR];
    uint8  status;
    uint32 data_size;
    char   data[1];
} __attribute__ ((packed));

#define SIZE_MDM_MSG sizeof(struct mdm_msg)

#undef CFG_SINGLE_HOSTAP_MSG

// NOTE: always use standard hostap_msg when communicating with wprd
struct hostap_msg
{
    int32  type;
    int32  module;
    uint8  mac[REQ_LEN_ETH_ADDR];                           // HMSG_STA_UPSK_RESPONSE, HMSG_WPA_UPSK_CACHE_ADD, HMSG_STA_SOFT_DEAUTH, HMSG_STA_AUTH, HMSG_STA_WPR_ROAM, HMSG_STA_CLEAR_CACHE, HMSG_STA_WPR_SESSION_TIMEOUT, HMSG_STA_8021X_PREAUTH_DEL, HMSG_STA_8021X_PREAUTH, HMSG_STA_FILTER_ID_SET, HMSG_STA_VLAN_SET, HMSG_STA_8021X_AUTH, HMSG_WPA_INSTALL_PTK, HMSG_STA_RADIUS_MAC_AUTH, HMSG_STA_WPR_AUTH, HMSG_STA_MDM
    uint16 aid;                                             // HMSG_STA_WPR_ROAM, HMSG_WPA_INSTALL_PTK, HMSG_STA_WPR_AUTH
    int32  reason;                                          // HMSG_STA_DEAUTH (aliased for wprd compatibility)
    int32  enforce_reauth_period;                           // HMSG_STA_DEAUTH (aliased for wprd compatibility)
    int32  clear_cache;                                     // HMSG_STA_DEAUTH (aliased for wprd compatibility)
    uint8  iface;                                           // HMSG_STA_WPR_ROAM, HMSG_WPA_MIC_FAILURE, HMSG_STA_FILTER_ID_SET, HMSG_STA_8021X_AUTH, HMSG_WPA_INSTALL_PTK, HMSG_WPA_INSTALL_GTK, HMSG_STA_WPR_AUTH
    char   ssid[REQ_LEN_SSID];                              // HMSG_WPA_UPSK_CACHE_DEL, HMSG_STA_UPSK_RESPONSE, HMSG_WPA_UPSK_CACHE_ADD, HMSG_STA_AUTH, HMSG_STA_WPR_ROAM, HMSG_WPA_MIC_FAILURE, HMSG_STA_RADIUS_MAC_AUTH, HMSG_STA_WPR_AUTH
    uint8  ssid_idx;                                        // HMSG_WPA_INSTALL_PTK, HMSG_WPA_INSTALL_GTK
    char   group[REQ_LEN_GROUP + 6];                        // HMSG_WPA_UPSK_CACHE_DEL, HMSG_STA_UPSK_RESPONSE, HMSG_WPA_UPSK_CACHE_ADD, HMSG_STA_WPR_ROAM (leave room for prepended "GROUP-")
    int32  cipher;                                          // HMSG_WPA_INSTALL_PTK, HMSG_WPA_INSTALL_GTK
    char   key[WPA_KEY_LEN];                                // HMSG_STA_UPSK_RESPONSE, HMSG_WPA_UPSK_CACHE_ADD, HMSG_STA_8021X_PREAUTH, HMSG_STA_8021X_AUTH, HMSG_WPA_INSTALL_PTK, HMSG_WPA_INSTALL_GTK
    uint8  key_id;                                          // HMSG_WPA_INSTALL_PTK, HMSG_WPA_INSTALL_GTK
    int32  vlan_id;                                         // HMSG_STA_WPR_ROAM, HMSG_STA_8021X_PREAUTH, HMSG_STA_VLAN_SET, HMSG_STA_8021X_AUTH, HMSG_STA_RADIUS_MAC_AUTH
    int32  auth_status;                                     // HMSG_STA_UPSK_RESPONSE, HMSG_WPA_UPSK_CACHE_ADD, HMSG_STA_WPR_ROAM, HMSG_STA_VLAN_SET, HMSG_STA_WPR_AUTH, HMSG_STA_RADIUS_MAC_AUTH
    int32  auth_type;                                       // HMSG_STA_FILTER_ID_SET
    int32  xrp_event;                                       // HMSG_STA_8021X_AUTH
    char   username[WPR_MAX_UNAME_LEN];                     // HMSG_STA_WPR_ROAM, HMSG_STA_8021X_PREAUTH, HMSG_STA_8021X_AUTH, HMSG_STA_WPR_AUTH, HMSG_STA_FILTER_ID_SET
    char   password[WPR_MAX_UNAME_LEN];                     // HMSG_STA_WPR_AUTH
    char   chap_password[WPR_CHAP_PASSWD_LEN];              // HMSG_STA_WPR_AUTH
    char   chap_challenge[WPR_CHAP_CHAL_LEN];               // HMSG_STA_WPR_AUTH
    char   filter_id[REQ_LEN_GROUP];                        // HMSG_STA_8021X_PREAUTH, HMSG_STA_FILTER_ID_SET, HMSG_STA_8021X_AUTH, HMSG_STA_WPR_AUTH, HMSG_STA_RADIUS_MAC_AUTH, HMSG_STA_WPR_AUTH
    uint32 acct_exp;                                        // HMSG_STA_WPR_ROAM, HMSG_STA_WPR_SESSION_TIMEOUT, HMSG_STA_WPR_AUTH
    uint8  iapmac[REQ_LEN_ETH_ADDR];                        // HMSG_STA_UPSK_REQUEST
    uint8  wpa_anonce[WPA_KEY_LEN];                         // HMSG_STA_UPSK_REQUEST
    uint8  wpa_eapol_key[MAX_WPA_EAPOL_KEY_LEN];            // HMSG_STA_UPSK_REQUEST, HMSG_STA_ASSOC, HMSG_STA_WPR_ROAM
    uint16 wpa_eapol_key_len;                               // HMSG_STA_UPSK_REQUEST
    uint8  ft_assoc;                                        // HMSG_WPA_INSTALL_PTK
    uint32 session_timeout;                                 // HMSG_STA_RADIUS_MAC_AUTH
    uint32 termination_action;                              // HMSG_STA_FILTER_ID_SET, HMSG_STA_VLAN_SET, HMSG_STA_RADIUS_MAC_AUTH

#ifdef CFG_SINGLE_HOSTAP_MSG
    uint8  assoc_resp_rsnie[MAX_AUTH_RSN_IE_LEN];           // HMSG_STA_ASSOC
    int32  assoc_resp_rsnie_len;                            // HMSG_STA_ASSOC
    char   wpa_ie[MAX_WPA_IE_LEN];                          // HMSG_STA_ASSOC
    int32  wpa_ie_len;                                      // HMSG_STA_ASSOC
    int32  wpa_version;                                     // HMSG_STA_ASSOC
    int32  key_mgmt;                                        // HMSG_STA_ASSOC
    int32  num_pmkid;                                       // HMSG_STA_ASSOC
    int32  pmkid_index;                                     // HMSG_STA_ASSOC

    char   multi_session_id[RAD_STA_MULTI_SESSION_ID_LEN];  // HMSG_STA_MULTI_SESSION_ID
    uint16 radius_class_attrs_len;                          // HMSG_STA_MULTI_SESSION_ID
    char   radius_class_attrs[RAD_CLS_ATTR_COMPOSITE_LEN];  // HMSG_STA_MULTI_SESSION_ID

    uint8  bssid[REQ_LEN_ETH_ADDR];                         // HMSG_STA_FT_AUTH
    uint8  auth_ft_ie[MAX_AUTH_FT_IE_LEN];                  // HMSG_STA_FT_AUTH
    int32  auth_ft_ie_len;                                  // HMSG_STA_FT_AUTH
    uint8  assoc_resp_ftie[MAX_AUTH_FT_IE_LEN];             // HMSG_STA_FT_AUTH
    int32  assoc_resp_ftie_len;                             // HMSG_STA_FT_AUTH
    int32  auth_transaction;                                // HMSG_STA_FT_AUTH

    uint8  assoc_resp_mdie[MAX_AUTH_MD_IE_LEN];             // HMSG_STA_ASSOC_RESP_FT_IE
    int32  assoc_resp_mdie_len;                             // HMSG_STA_ASSOC_RESP_FT_IE

    uint8  ft_data[MAX_FT_DATA_LEN];                        // HMSG_FT_MSG
    int32  ft_data_len;                                     // HMSG_FT_MSG

    int32  flags;                                           // HMSG_STA_EAPOL_TX
    int16  eapol_frame_len;                                 // HMSG_STA_EAPOL_TX
    uint8  eapol_frame[MAX_EAPOL_FRAME_LEN];                // HMSG_STA_EAPOL_TX
#endif

} __attribute__ ((packed));


#ifdef CFG_SINGLE_HOSTAP_MSG

#define hostap_sta_assoc_msg         hostap_msg
#define hostap_deauth_disassoc_msg   hostap_msg
#define hostap_multi_session_msg     hostap_msg
#define hostap_eapol_msg             hostap_msg
#define hostap_sta_ft_auth_msg       hostap_msg
#define hostap_sta_ft_assoc_resp_msg hostap_msg
#define hostap_ft_data_msg           hostap_msg

#else
struct hostap_sta_assoc_msg
{
    int32  type;
    int32  module;                                          // HMSG_STA_ASSOC, HMSG_STA_ASSOC_OPEN, HMSG_STA_ACCT_START
    uint8  mac[REQ_LEN_ETH_ADDR];                           // HMSG_STA_ASSOC, HMSG_STA_ASSOC_OPEN, HMSG_STA_ACCT_START
    uint16 aid;                                             // HMSG_STA_ASSOC, HMSG_STA_ASSOC_OPEN
    uint8  iface;                                           // HMSG_STA_ASSOC, HMSG_STA_ASSOC_OPEN, HMSG_STA_ACCT_START
    char   ssid[REQ_LEN_SSID];                              // HMSG_STA_ASSOC,                      HMSG_STA_ACCT_START
    char   group[REQ_LEN_GROUP + 6];                        // HMSG_STA_ASSOC                                               (leave room for prepended "GROUP-")
    char   username[WPR_MAX_UNAME_LEN];                     //                 HMSG_STA_ASSOC_OPEN, HMSG_STA_ACCT_START
    uint8  assoc_resp_rsnie[MAX_AUTH_RSN_IE_LEN];           // HMSG_STA_ASSOC
    int32  assoc_resp_rsnie_len;                            // HMSG_STA_ASSOC
    char   wpa_ie[MAX_WPA_IE_LEN];                          // HMSG_STA_ASSOC
    int32  wpa_ie_len;                                      // HMSG_STA_ASSOC
    int32  wpa_version;                                     // HMSG_STA_ASSOC
    int32  cipher;                                          // HMSG_STA_ASSOC
    int32  key_mgmt;                                        // HMSG_STA_ASSOC
    int32  num_pmkid;                                       // HMSG_STA_ASSOC
    int32  pmkid_index;                                     // HMSG_STA_ASSOC
    int32  vlan_id;                                         // HMSG_STA_ASSOC
    int32  auth_status;                                     //                                      HMSG_STA_ACCT_START
    uint32 acct_exp;                                        // HMSG_STA_ASSOC,                      HMSG_STA_ACCT_START
    uint8  wpa_eapol_key[MAX_WPA_EAPOL_KEY_LEN];            // HMSG_STA_ASSOC
    uint16 wpa_eapol_key_len;                               // HMSG_STA_ASSOC
    uint32 session_timeout;                                 // HMSG_STA_ASSOC
    uint32 termination_action;                              // HMSG_STA_ASSOC
} __attribute__ ((packed));

struct hostap_deauth_disassoc_msg
{
    int32  type;
    int32  module;                                          // HMSG_STA_DEAUTH, HMSG_STA_DISASSOC
    uint8  mac[REQ_LEN_ETH_ADDR];                           // HMSG_STA_DEAUTH, HMSG_STA_DISASSOC
    uint16 aid;                                             // HMSG_STA_WPR_ROAM, HMSG_WPA_INSTALL_PTK, HMSG_STA_WPR_AUTH
    int32  reason;                                          // HMSG_STA_DEAUTH
    int32  enforce_reauth_period;                           // HMSG_STA_DEAUTH
    int32  clear_cache;                                     // HMSG_STA_DEAUTH
} __attribute__ ((packed));

struct hostap_multi_session_msg
{
    int32  type;
    int32  module;                                          // HMSG_STA_MULTI_SESSION_ID
    uint8  mac[REQ_LEN_ETH_ADDR];                           // HMSG_STA_MULTI_SESSION_ID
    char   ssid[REQ_LEN_SSID];                              // HMSG_STA_MULTI_SESSION_ID
    int32  xrp_event;                                       // HMSG_STA_MULTI_SESSION_ID
    char   multi_session_id[RAD_STA_MULTI_SESSION_ID_LEN];  // HMSG_STA_MULTI_SESSION_ID
    uint32 acct_exp;                                        // HMSG_STA_MULTI_SESSION_ID
    uint16 radius_class_attrs_len;                          // HMSG_STA_MULTI_SESSION_ID
    char   radius_class_attrs[RAD_CLS_ATTR_COMPOSITE_LEN];  // HMSG_STA_MULTI_SESSION_ID
} __attribute__ ((packed));

struct hostap_eapol_msg
{
    int32  type;
    int32  flags;                                           // HMSG_STA_EAPOL_TX
    uint8  mac[REQ_LEN_ETH_ADDR];                           // HMSG_STA_EAPOL_TX
    uint8  iface;                                           // HMSG_STA_EAPOL_TX
    uint8  ssid_idx;                                        // HMSG_STA_EAPOL_TX
    int16  eapol_frame_len;                                 // HMSG_STA_EAPOL_TX
    uint8  eapol_frame[MAX_EAPOL_FRAME_LEN];                // HMSG_STA_EAPOL_TX
} __attribute__ ((packed));

struct hostap_sta_ft_auth_msg
{
    int32  type;
    int32  module;                                          // HMSG_STA_FT_AUTH
    uint8  mac[REQ_LEN_ETH_ADDR];                           // HMSG_STA_FT_AUTH
    uint8  bssid[REQ_LEN_ETH_ADDR];                         // HMSG_STA_FT_AUTH
    char   ssid[REQ_LEN_SSID];                              // HMSG_STA_FT_AUTH
    uint8  auth_ft_ie[MAX_AUTH_FT_IE_LEN];                  // HMSG_STA_FT_AUTH
    int32  auth_ft_ie_len;                                  // HMSG_STA_FT_AUTH
    uint8  assoc_resp_ftie[MAX_AUTH_FT_IE_LEN];             // HMSG_STA_FT_AUTH
    int32  assoc_resp_ftie_len;                             // HMSG_STA_FT_AUTH
    int32  auth_status;                                     // HMSG_STA_FT_AUTH
    int32  auth_transaction;                                // HMSG_STA_FT_AUTH
} __attribute__ ((packed));

struct hostap_sta_ft_assoc_resp_msg
{
    int32  type;
    int32  module;                                          // HMSG_STA_ASSOC_RESP_FT_IE
    uint8  mac[REQ_LEN_ETH_ADDR];                           // HMSG_STA_ASSOC_RESP_FT_IE
    uint16 aid;                                             // HMSG_STA_ASSOC_RESP_FT_IE
    uint8  bssid[REQ_LEN_ETH_ADDR];                         // HMSG_STA_ASSOC_RESP_FT_IE
    char   ssid[REQ_LEN_SSID];                              // HMSG_STA_ASSOC_RESP_FT_IE
    uint8  assoc_resp_ftie[MAX_AUTH_FT_IE_LEN];             // HMSG_STA_ASSOC_RESP_FT_IE
    int32  assoc_resp_ftie_len;                             // HMSG_STA_ASSOC_RESP_FT_IE
    uint8  assoc_resp_mdie[MAX_AUTH_MD_IE_LEN];             // HMSG_STA_ASSOC_RESP_FT_IE
    int32  assoc_resp_mdie_len;                             // HMSG_STA_ASSOC_RESP_FT_IE
    uint8  assoc_resp_rsnie[MAX_AUTH_RSN_IE_LEN];           // HMSG_STA_ASSOC_RESP_FT_IE
    int32  assoc_resp_rsnie_len;                            // HMSG_STA_ASSOC_RESP_FT_IE
} __attribute__ ((packed));


struct hostap_ft_data_msg
{
    int32  type;
    int32  module;                                          // HMSG_FT_MSG
    uint8  mac[REQ_LEN_ETH_ADDR];                           // HMSG_FT_MSG
    uint8  ft_data[MAX_FT_DATA_LEN];                        // HMSG_FT_MSG
    int32  ft_data_len;                                     // HMSG_FT_MSG
} __attribute__ ((packed));

#endif

union hostap_all_msg
{
    struct hostap_msg                   standard;
    struct hostap_sta_assoc_msg         sta_assoc;
    struct hostap_deauth_disassoc_msg   deauth_disassoc;
    struct hostap_multi_session_msg     multi_session;
    struct hostap_eapol_msg             eapol_tx;
    struct hostap_ft_data_msg           ft_buffer;
    struct hostap_sta_ft_auth_msg       sta_ft_auth;
    struct hostap_sta_ft_assoc_resp_msg sta_ft_assoc_resp;
} __attribute__ ((packed));

#define SIZE_HOSTAP_MSG                 sizeof(struct hostap_msg                  )
#define SIZE_HOSTAP_STA_ASSOC_MSG       sizeof(struct hostap_sta_assoc_msg        )
#define SIZE_HOSTAP_DEAUTH_DISASSOC_MSG sizeof(struct hostap_deauth_disassoc_msg  )
#define SIZE_HOSTAP_MULTI_SESSION_MSG   sizeof(struct hostap_multi_session_msg    )
#define SIZE_HOSTAP_EAPOL_MSG           sizeof(struct hostap_eapol_msg            )
#define SIZE_HOSTAP_FT_AUTH_MSG         sizeof(struct hostap_sta_ft_auth_msg      )
#define SIZE_HOSTAP_FT_ASSOC_RESP_MSG   sizeof(struct hostap_sta_ft_assoc_resp_msg)
#define SIZE_HOSTAP_FT_DATA_MSG         sizeof(struct hostap_ft_data_msg          )
#define SIZE_HOSTAP_ALL_MSG             sizeof(union  hostap_all_msg              )


struct uboot_env_entry
{
    char variable[REQ_LEN_ENV_VAR_NAME ];
    char value   [REQ_LEN_ENV_VAR_VALUE];
} __attribute__ ((packed));

#define SIZE_UBOOT_ENV_ENTRY sizeof(struct uboot_env_entry)

struct flash_file_entry
{
    char filename[REQ_LEN_HOSTNAME_STR];
} __attribute__ ((packed));

#define SIZE_FLASH_FILE_ENTRY sizeof(struct flash_file_entry)

struct quick_config_entry
{
    char file_and_desc[REQ_LEN_HOSTNAME_STR];
} __attribute__ ((packed));

#define SIZE_QUICK_CONFIG_ENTRY sizeof(struct quick_config_entry)

struct syslog_counts
{
    int32 num[NUM_SYSLOG_PRI];
    int32 total;
    int32 beg_time;
    int32 end_time;
    uint8 time_fmt;
} __attribute__ ((packed));

#define SIZE_SYSLOG_COUNTS sizeof(struct syslog_counts)

struct syslog_entry
{
    int64 time;                        // for cluster sorting
    int32 index;                       // for cluster sorting
    char  timestamp[REQ_LEN_TIMESTAMP];
    uint8 pri;
    char  msg[REQ_LEN_SYSLOG_MSG];
} __attribute__ ((packed));

#define SIZE_SYSLOG_ENTRY sizeof(struct syslog_entry)

struct file_write_info
{
    char filename[REQ_LEN_STR];
    char contents[MAX_STR - REQ_LEN_STR];
} __attribute__ ((packed));

struct dbg_settings
{
    char   name[REQ_LEN_STR];
    uint32 level;
    uint64 bits;
} __attribute__ ((packed));

#define SIZE_DBG_SETTINGS sizeof(struct dbg_settings)

struct na_test_info
{
    char  setting [REQ_LEN_HOSTNAME_STR];
    char  hostname[REQ_LEN_HOSTNAME_STR];
    char  ip_addr [REQ_LEN_IPV6_ADDRSTR];
    char  state   [REQ_LEN_STR];
} __attribute__ ((packed));

#define SIZE_NA_TEST_INFO sizeof(struct na_test_info)

struct capture_monitor_cfg
{
    uint8_t primary_channel;
     int8_t bond_40mhz;
     int8_t bond_80mhz;
     int8_t bond_160mhz;
     int8_t rx_threshold;
} __attribute__ ((packed));

#define SIZE_CAPTURE_MONITOR_CFG sizeof(struct capture_monitor_cfg)

struct device_id_info
{
    char name[REQ_LEN_DEVICE_STR];
} __attribute__ ((packed));

#define SIZE_DEVICE_ID_INFO sizeof(struct device_id_info)

struct device_id_mac_entry {
    char   dev_mac  [REQ_LEN_MAC_STR   ];
    char   dev_type [REQ_LEN_DEVICE_STR];
    char   dev_class[REQ_LEN_DEVICE_STR];
    uint16 dev_enum;
} __attribute__ ((packed));

#define SIZE_DEVICE_ID_MAC_ENTRY sizeof(struct device_id_mac_entry)

struct device_id_oui_entry {
    char   dev_oui  [REQ_LEN_OUI_STR    ];
    char   dev_type [REQ_LEN_DEVICE_STR ];
    char   dev_class[REQ_LEN_DEVICE_STR ];
    uint16 dev_enum;
} __attribute__ ((packed));

#define SIZE_DEVICE_ID_OUI_ENTRY sizeof(struct device_id_oui_entry)

struct dpi_info
{
    char   desc[REQ_LEN_DPI_STR ];
    char   guid[REQ_LEN_GUID_STR];
    char   help[REQ_LEN_GUID_STR + REQ_LEN_DPI_STR + 2];
    char   addr[REQ_LEN_MAC_ADDR_STR + 1];
    uint16 index;
    uint16 cat_index;
    uint16 vlan;
    uint8  prod;
    uint8  risk;
    uint32 period;
    uint32 time_stamp;
    uint64 tx_pkts;
    uint64 rx_pkts;
    uint64 tx_bytes;
    uint64 rx_bytes;
} __attribute__ ((packed));

#define SIZE_DPI_INFO sizeof(struct dpi_info)

struct oauth_entry
{
    char   token[REQ_LEN_STR];
    char   client[REQ_LEN_STR];
    char   grant[REQ_LEN_STR];
    uint32 expiration;
    char   agent[REQ_LEN_STR];
    char   scope[REQ_LEN_STR];
    char   code[REQ_LEN_STR];
    char   type[REQ_LEN_STR];
} __attribute__ ((packed));

#define SIZE_OAUTH_ENTRY sizeof(struct oauth_entry)

struct oauth_cfg
{
    uint8 max_tokens;
    uint8 num_oauth;
    struct oauth_entry  oa[MAX_NUM_OAUTH_TOKENS];
} __attribute__ ((packed));

#define SIZE_OAUTH_CFG sizeof(struct oauth_cfg)

struct proxy_fwd_cfg
{
    uint8  proxy;
    char   bluecoat_host  [REQ_LEN_HOSTNAME_STR];
    char   netboxblue_host[REQ_LEN_HOSTNAME_STR];
} __attribute__ ((packed));

#define SIZE_PROXY_FWD_CFG    sizeof(struct proxy_fwd_cfg)

struct web_proxy_msg {
    uint8  type;
    uint16 size;
    uint8  mac      [REQ_LEN_ETH_ADDR];
    char   ssid     [REQ_LEN_SSID ];
    uint8  ipv4_addr[IPV4_ADDR_LEN];
    char   group    [REQ_LEN_GROUP];
    char   user     [REQ_LEN_USERNAME_STR];
};

#define SIZE_WEB_PROXY_MSG  sizeof(struct web_proxy_msg)
#define ADD_WEB_PROXY_STN   1
#define DEL_WEB_PROXY_STN   2


//****************************************************************
// PROXY MGMT defines
//----------------------------------------------------------------

struct proxy_mgmt_entry
{
    uint8  enabled;
    char   host    [REQ_LEN_HOSTNAME_STR];
    uint16 port;
    char   username[REQ_LEN_USERNAME_STR];
    char   passwd  [REQ_LEN_STR];
    uint8  type;
}__attribute__ ((packed));

struct proxy_mgmt_socks_entry
{
    uint8  enabled;
    char   ipv4_addr[REQ_LEN_IPV4_ADDRSTR];
    uint16 port;
    char   username [REQ_LEN_USERNAME_STR];
    char   passwd   [REQ_LEN_STR];
    uint8  type;
}__attribute__ ((packed));

#define SIZE_PROXY_MGMT_ENTRY sizeof(struct proxy_mgmt_entry)

struct proxy_mgmt_subnet
{
    char   net [REQ_LEN_IPV4_ADDRSTR];
    char   mask[REQ_LEN_IPV4_ADDRSTR];
}__attribute__ ((packed));

#define SIZE_PROXY_MGMT_NET_ENTRY sizeof(proxy_mgmt_net_entry)

struct proxy_mgmt_cfg
{
    uint8 enabled;
    uint8 custom;
    struct proxy_mgmt_entry       http;
    struct proxy_mgmt_entry       https;
    struct proxy_mgmt_socks_entry socks;
    struct proxy_mgmt_subnet      net_01;
    struct proxy_mgmt_subnet      net_02;
    struct proxy_mgmt_subnet      net_03;
    struct proxy_mgmt_subnet      net_04;
    struct proxy_mgmt_subnet      net_05;
    struct proxy_mgmt_subnet      net_06;
    struct proxy_mgmt_subnet      net_07;
    struct proxy_mgmt_subnet      net_08;
    struct proxy_mgmt_subnet      net_09;
    struct proxy_mgmt_subnet      net_10;
}__attribute__ ((packed));

#define SIZE_PROXY_MGMT_CFG sizeof(struct proxy_mgmt_cfg)


//****************************************************************
// Personal WiFi defines
//----------------------------------------------------------------
struct pwifi_cfg
{
    char     ssid [REQ_LEN_WPR];
    char     psk  [REQ_LEN_WPA_PASSPHRASE];
    char     exp  [REQ_LEN_WPR];
    char     sta  [REQ_LEN_WPR];
    char     flst [REQ_LEN_WPR];
    uint8    qos;
    uint8    broadcast;
    int16    stations;     // station limit,               0=disabled
    uint32   pps;          // traffic, packets per second, 0=disabled
    uint32   sta_pps;      // traffic, packets per second, 0=disabled
    uint32   kbps;         // traffic, Kbits   per second, 0=disabled
    uint32   sta_kbps;     // traffic, Kbits   per second, 0=disabled
}__attribute__ ((packed));

#define SIZE_PWIFI_CFG sizeof(struct pwifi_cfg)


//****************************************************************
// DHCP defines (really belongs in dhcp.h)
//----------------------------------------------------------------
#endif /* not defined _XIRDHCP_H */
#define ADD_CLIENT                      1
#define DEL_CLIENT                      2

struct dhcp_msg
{
    int32 type;
    char  mac[REQ_LEN_ETH_ADDR];
    char  class_name[REQ_LEN_SSID + REQ_LEN_GROUP];
    char  old_class_name[REQ_LEN_SSID + REQ_LEN_GROUP];
} __attribute__ ((packed));

#define SIZE_DHCP_MSG sizeof(struct dhcp_msg)

//****************************************************************
// dot11r FT defines common to hostapd and OOPME
//----------------------------------------------------------------
typedef struct
{
    uint8     msgId;
    uint16    msgDataLen;
    uint8     mdid[2];
    uint8     srcMac[MAC_ADDR_LEN];
    uint8     dstMac[MAC_ADDR_LEN];
} __attribute__ ((packed)) ft_msg_hdr_t;

#define SIZE_FT_MSG_HDR sizeof(ft_msg_hdr_t)

enum FT_MSG_ID
{
    FT_MSG_ID_R0R1_KH_INFO    = 1,
    FT_MSG_ID_PMK_PUSH        = 2,
    FT_MSG_ID_PMK_PULL_REQ    = 3,
    FT_MSG_ID_PMK_PULL_RESP   = 4,
    FT_MSG_ID_ACT_FRAME_REQ   = 5,
    FT_MSG_ID_ACT_FRAME_RESP  = 6,
};
#endif // _XIRCFG_H

