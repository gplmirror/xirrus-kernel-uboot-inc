//******************************************************************************
/** @file deprecated.h
 *
 * Deprecated cfg request defines
 *
 * <I>Copyright (C) 2005 Xirrus. All Rights Reserved</I>
 * 
 * @author  Eric K. Henderson
 * @date    11/7/2005
 * 
 * This file contains a mapping from legacy constants to the new names found in
 * xircfg.h, xiriap.h, and xiroid.h.
 *
 * PLEASE DO NOT ADD ANYTHING TO THIS FILE UNLESS IT IS NEEDED FOR LEGACY
 * SUPPORT.
 *
 * New OIDs should go in xiroid.h using the new naming conventions.
 * New WMI/CLI interface structures and defines belong in xircfg.h.
 * New iap/radio defines belong in xiriap.h
 **/
//------------------------------------------------------------------------------
#ifndef _DEPRECATED_H
#define _DEPRECATED_H

#include "xlatoid.h"    // translation of new oids to old oids (everything shared with mace)
#include "cfgdefs.h"    // definitions unique to the legacy config module

#endif // _DEPRECATED_H
