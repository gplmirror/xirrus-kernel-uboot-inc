#ifndef _SKIN_H
#define _SKIN_H

#ifdef __cplusplus
#define BEGIN_C_DECLS extern "C" {
#define END_C_DECLS }
#else
#define BEGIN_C_DECLS
#define END_C_DECLS
#endif

typedef void *SKIN;

BEGIN_C_DECLS
/* Well if this was C++ these would be constructor and destructor */

extern SKIN *skin_init( char *skinfile );
/* return null means some error, file does not exist, could not malloc memory, etc. */

/* Default file for constructor if NULL skinfile give */
#define DEFAULT_GLOBAL_SKIN_FILE "/home/config/global.skin"

extern int skin_free( SKIN *skin );
/* return 0 OK, non-zero some error, skin is null, or not a skin handle */

/* Which would make these methods, accessors, etc */

extern int skin_get_string( SKIN *skin, char *my_name, char **var );
/* non-zero means success var has an allocated string in it, do not forget to free it
   zero means error of some sort, nothing was allocated to var */

extern int skin_get_file( SKIN *skin, char *my_name, SKIN *var );
/* again non-zero means var has an allocated skin handle in it, do not forget to free it
   zero means error of some sort, nothing allocated to var. */

extern int skin_get_int( SKIN *skin, char *my_name, int *var );
extern int skin_get_uint( SKIN *skin, char *my_name, unsigned int *var );
/* return semantics are that either way there's nothing allocated in var which
   you have to free, its just your supplied variable got overwritten (non-zero return)
   or not (zero return). In the error (zero) case your variable will
   have its original value so either populate it with default value
   before the call or check the error return */

END_C_DECLS

#endif // _SKIN_H
