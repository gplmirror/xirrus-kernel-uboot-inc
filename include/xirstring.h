//******************************************************************************
/** @file xirstring.h
 *
 * Safe String Macros
 *
 * <I>Copyright (C) 2017 Xirrus. All Rights Reserved</I>
 *
 * @author  Dirk Gates
 * @date    5/11/2017
 *
 **/
//------------------------------------------------------------------------------
#ifndef _XIRSTRING_H
#define _XIRSTRING_H

//******************************************************************************
/**
 * safe string functions -- tolerate null pointer inputs
 */
//------------------------------------------------------------------------------
#define strxcmp(     s1, s2)    ({ const char *_s1 = (const char *)(s1); const char *_s2 = (const char *)(s2); (!_s1 && !_s2) ? 0 : !_s1 ? -1 : !_s2 ? 1 : strcmp    ( _s1, _s2); })
#define strxcasecmp( s1, s2)    ({ const char *_s1 = (const char *)(s1); const char *_s2 = (const char *)(s2); (!_s1 && !_s2) ? 0 : !_s1 ? -1 : !_s2 ? 1 : strcasecmp( _s1, _s2); })

#define strxncmp(    s1, s2, n) ({ const char *_s1 = (const char *)(s1); const char *_s2 = (const char *)(s2); (!_s1 && !_s2) ? 0 : !_s1 ? -1 : !_s2 ? 1 : strncmp   ( _s1, _s2, n); })
#define strxncasecmp(s1, s2, n) ({ const char *_s1 = (const char *)(s1); const char *_s2 = (const char *)(s2); (!_s1 && !_s2) ? 0 : !_s1 ? -1 : !_s2 ? 1 : strncasecmp(_s1, _s2, n); })

#define strxcat(     s1, s2)    ({ char *_s1 = (char *)(s1); const char *_s2 = (const char *)(s2); !_s1 ? NULL : !_s2 ? _s1 : strcat (_s1, _s2   ); })
#define strxncat(    s1, s2, n) ({ char *_s1 = (char *)(s1); const char *_s2 = (const char *)(s2); !_s1 ? NULL : !_s2 ? _s1 : strncat(_s1, _s2, n); })

#define strxstr(     s1, s2)    ({ char *_s1 = (char *)(s1); const char *_s2 = (const char *)(s2); (!_s1 || !_s2) ? NULL : strstr    (_s1, _s2); })
#define strxcasestr( s1, s2)    ({ char *_s1 = (char *)(s1); const char *_s2 = (const char *)(s2); (!_s1 || !_s2) ? NULL : strcasestr(_s1, _s2); })

#define strxchr(     s1, c2)    ({ char *_s1 = (char *)(s1); !_s1 ? NULL : strchr (_s1, c2); })
#define strxrchr(    s1, c2)    ({ char *_s1 = (char *)(s1); !_s1 ? NULL : strrchr(_s1, c2); })

#define strxlen(     s1    )    ({ char *_s1 = (char *)(s1); !_s1 ? 0    : strlen (_s1   ); })
#define strxnlen(    s1, n )    ({ char *_s1 = (char *)(s1); !_s1 ? 0    : strnlen(_s1, n); })

//******************************************************************************
/**
 * safe strncpy -- force null termination, tolerate null inputs
 */
//------------------------------------------------------------------------------
#define strxncpy(d, s, l)       ({ char *_d = (char *)(d); const char *_s = (const char *)(s); int _l = l; if (_d && _s) strncpy(_d, _s, _l); if (_d) _d[_l ? _l-1 : 0] = 0; _d; })
#define strxcpy( d, s)          strxncpy( d, s, sizeof(d) )

#endif //_XIRSTRING_H

