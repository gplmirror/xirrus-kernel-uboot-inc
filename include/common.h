//******************************************************************************
/** @file common.h
 *
 * Global types and macros available to all classes
 *
 * <I>Copyright (C) 2004 Xirrus. All Rights Reserved</I>
 *
 * @author  Eric K. Henderson
 * @date    4/7/2004
 *
 **/
//------------------------------------------------------------------------------
#ifndef _COMMON_H
#define _COMMON_H

#ifdef _WINDOWS
#include "stdafx.h"
#endif
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <net/ethernet.h>
#include <netinet/in.h>
#include "xtypes.h"
#include "xirmac.h"
#include "xirstring.h"

//****************************************************************
// Some necessary motherhood
//----------------------------------------------------------------
#if defined(UNUSED)
  /* nothing */
#elif defined(__GNUC__)
#  define UNUSED(x)             UNUSED_ ## x __attribute__((__unused__))
#  define UNUSED_FUNCTION(x)    __attribute__((__unused__)) UNUSED_ ## x
#else
#  define UNUSED(x)             UNUSED_ ## x
#  define UNUSED_FUNCTION(x)    UNUSED_ ## x
#endif

#define countof(x)              (sizeof(x) / sizeof((x)[0]))
#define range_check(i, limit)   ({ int idx = i; 0 <= idx && idx < (int)(limit); })

//****************************************************************
// Endian fun and games for u128s, u64s, u32s, and u16s
//----------------------------------------------------------------
#if __BYTE_ORDER == __BIG_ENDIAN
#define ntoh128(x)              (x)
#define ntoh64( x)              (x)
#define ntoh32( x)              (x)
#define ntoh16( x)              (x)
#define hton128(x)              (x)
#define hton64( x)              (x)
#define hton32( x)              (x)
#define hton16( x)              (x)
#else
#include <byteswap.h>
#ifndef bswap_128
#define bswap_128(x)            ({ uint128_t _x=x; u64 *p=(u64 *)&_x; ((uint128_t)bswap_64(p[1]) << 64) | (uint128_t)bswap_64(p[0]); })
#endif
#define ntoh128(x)              bswap_128(x)
#define ntoh64( x)              bswap_64( x)
#define ntoh32( x)              bswap_32( x)
#define ntoh16( x)              bswap_16( x)
#define hton128(x)              bswap_128(x)
#define hton64( x)              bswap_64( x)
#define hton32( x)              bswap_32( x)
#define hton16( x)              bswap_16( x)
#endif

//******************************************************************************
/**
 * Abstracts a MAC address as defined by IEEE
 */
//------------------------------------------------------------------------------
#define NULL_MAC_ADDR         (MAC_ADDR){{0,0,0,0,0,0}}
#define MAC_ADDR_LEN          6
#define MAC_ADDR_STRLEN       ((2*MAC_ADDR_LEN) + 5 + 1)  //  5 colons and 1 NULL terminator
#define MAC_ADDR_SHORT        ((2*MAC_ADDR_LEN) + 0 + 1)  // no colons and 1 NULL terminator

// for compatibility with c code (when the rest of the methods confuse the compiler on things like offsetof)
struct mac_addr
{
   BYTE addr[MAC_ADDR_LEN];
} __attribute__ ((packed));

struct MAC_ADDR
{
   BYTE addr[MAC_ADDR_LEN];

#ifdef __cplusplus

   char *getAscii     (char sAddr[]) const { return getString    (sAddr, addr, 1); };
   char *getAscii2    (char sAddr[]) const { return getString    (sAddr, addr, 0); };
   char *getString    (char sAddr[]) const { return getString    (sAddr, addr, 1); };
   char *getString2   (char sAddr[]) const { return getString    (sAddr, addr, 0); };
   char *getStringDash(char sAddr[]) const { return getStringDash(sAddr, addr   ); };

   void  setString(const char sAscii[]) { setAscii(sAscii); } // or use operator '='
   void  setAscii (const char sAscii[])
   {
      for (int i = 0, j = 0; i < ETHER_ADDR_LEN; i++)
      {
         int iHexDigit;

         if (sAscii[j] == '\0')
         {
            addr[i] = 0;
            continue;
         }

         iHexDigit = toupper(sAscii[ j++ ]) - '0';
         if (iHexDigit > 9)
            iHexDigit -= 'A' - ('9' + 1);
         addr[i] = (BYTE)(iHexDigit << 4);

         iHexDigit = toupper(sAscii[ j++ ]) - '0';
         if (iHexDigit > 9)
            iHexDigit -= 'A' - ('9' + 1);
         addr[i] = (BYTE)(addr[i] | (iHexDigit));

         if (sAscii[j] == ':')
            j++;
      }
   };

   static char *getString(char sAddr[], const BYTE addr[MAC_ADDR_LEN], int colon = 1)
   {
      if (colon)    snprintf(sAddr, MAC_ADDR_STRLEN, "%02x:%02x:%02x:%02x:%02x:%02x", addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);
      else          snprintf(sAddr, MAC_ADDR_SHORT , "%02x%02x%02x%02x%02x%02x"     , addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);

      return sAddr;
   };

   // need caps to match the mac format used in wpr
   static char *getStringDash(char sAddr[], const BYTE addr[MAC_ADDR_LEN])
   {
      snprintf(sAddr, MAC_ADDR_STRLEN, "%02X-%02X-%02X-%02X-%02X-%02X", addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);
      return sAddr;
   };

   static char *getMask(char sMask[], int iMaskBits)
   {
      uint64 macmask = (0xffffffffffffULL << (48 - iMaskBits)) & 0xffffffffffffULL;
      snprintf(sMask, MAC_ADDR_STRLEN, "%02x:%02x:%02x:%02x:%02x:%02x",
               (int)(macmask >> 40ULL) & 0xff, (int)(macmask >> 32ULL) & 0xff, (int)(macmask >> 24ULL) & 0xff,
               (int)(macmask >> 16ULL) & 0xff, (int)(macmask >>  8ULL) & 0xff, (int)(macmask >>  0ULL) & 0xff);
      return sMask;
   }

   //---------------------------------------------------------
   // unsigned long long representation with upper two bytes 0
   //---------------------------------------------------------
   uint64 ull() const { return ( ((uint64)(((const WORD16 *)&(addr[0]))[0]) << 32) | (uint64)((const WORD32 *)&(addr[2]))[0] ); };
   uint64 u64() const { return ( ((uint64)(((const WORD16 *)&(addr[0]))[0]) << 32) | (uint64)((const WORD32 *)&(addr[2]))[0] ); };
   uint32 oui() const { return ( ((uint32)(((const WORD16 *)&(addr[0]))[0]) << 16) |                            addr[2]      ); };

   bool operator==(const MAC_ADDR& mac) const
   {
      if ( ((const WORD16 *)&(addr[4]))[0] != ((const WORD16 *)&(mac.addr[4]))[0] ) return false;
      if ( ((const WORD32 *)  addr    )[0] != ((const WORD32 *)  mac.addr    )[0] ) return false;
      return true;
   }

   bool operator!=(const MAC_ADDR& mac) const { return !(*this == mac); }

   bool operator<(const MAC_ADDR& mac) const
   {
      if ( ((const WORD32 *)  addr    )[0] < ((const WORD32 *)  mac.addr    )[0] ) return true;
      if ( ((const WORD16 *)&(addr[4]))[0] < ((const WORD16 *)&(mac.addr[4]))[0] ) return true;
      return false;
   }

   bool operator>(const MAC_ADDR& mac) const
   {
      if ( ((const WORD32 *)  addr    )[0] > ((const WORD32 *)  mac.addr    )[0] ) return true;
      if ( ((const WORD16 *)&(addr[4]))[0] > ((const WORD16 *)&(mac.addr[4]))[0] ) return true;
      return false;
   }

   MAC_ADDR const &operator=(MAC_ADDR const &mac)
   {
      ((WORD32 *)  addr    )[0] = ((const WORD32 *)  mac.addr    )[0];
      ((WORD16 *)&(addr[4]))[0] = ((const WORD16 *)&(mac.addr[4]))[0];
      return *this;
   }

   MAC_ADDR const &operator=(uint64 const uiAddr)
   {
      ((WORD16 *)&(addr[0]))[0] = (const WORD16)(uiAddr >> 32);
      ((WORD32 *)&(addr[2]))[0] = (const WORD32) uiAddr;
      return *this;
   }

   MAC_ADDR const &operator=(BYTE const mac_addr[MAC_ADDR_LEN])
   {
      ((WORD32 *)  addr    )[0] = ((const WORD32 *)  mac_addr    )[0];
      ((WORD16 *)&(addr[4]))[0] = ((const WORD16 *)&(mac_addr[4]))[0];
      return *this;
   }

   //---------------------------------------------------------
   // This method determines if this address is within uiOffset
   // of the given base address.
   //---------------------------------------------------------
   bool isFromBase( const MAC_ADDR& maBase, uint uiOffset ) const
   {
      //---------------------------------------------------------
      // First check the OUI
      //---------------------------------------------------------
      if ( ( *(const uint32 *)(addr       ) & 0xffffff00 ) !=
           ( *(const uint32 *)(maBase.addr) & 0xffffff00 ) )
         return false;
      //---------------------------------------------------------
      // Now subtract this from the base, but use unsigned numbers
      // so we get absolute values.
      //---------------------------------------------------------
      if ( ( ( *(const uint32 *)(&(addr       )[2]) & 0x00ffffff ) -
             ( *(const uint32 *)(&(maBase.addr)[2]) & 0x00ffffff ) ) < uiOffset )
         return true;

      return false;
   }

   //---------------------------------------------------------
   // This method returns a hardware radio index  from a given
   // mac address (assuming it's one of ours)
   //---------------------------------------------------------
   int getHwRadio(int num_slots, const MAC_ADDR& maBase) const
   {
       return (get_radio_from_mac(num_slots, addr, maBase.addr));
   }

   //---------------------------------------------------------
   // This method returns an ssid index from a given mac address
   // (assuming it's one of ours)
   //---------------------------------------------------------
   int  getSsidMac()   const { return (addr[5] & 0xf); }
   bool isMulticast()  const { return (addr[0] & 0x1); }
   bool isBroadcast()  const { return (u64() == 0xffffffffffffULL); }
   bool isAvaya()      const { return (is_avaya_macaddr      (addr)); }
   bool isXirrus()     const { return (is_xirrus_macaddr     (addr)); }
   bool isXirrusOnly() const { return (is_xirrus_only_macaddr(addr)); }

#endif

} __attribute__ ((packed));

//******************************************************************************
/**
 * Abstracts an IPv4 address as defined by IEEE
 *
 * Note: This structure may be used in communication with straight C modules
 *       and must compile to 4 bytes - i.e. do not use virtual functions
 *       or other data members. Use the CIpAddr for more advanced features
 */
//------------------------------------------------------------------------------
#define NULL_IPV4_ADDR    (IPV4_ADDR){}
#define IPV4_ADDR_LEN     4
#define IPV4_ADDR_STRLEN  ((3*IPV4_ADDR_LEN) + 3 + 1) // 3 dots and 1 NULL terminator

// for compatibility with c code (when the rest of the methods confuse the compiler on things like offsetof)
struct ipv4_addr
{
   BYTE addr[IPV4_ADDR_LEN];
} __attribute__ ((packed));

struct IPV4_ADDR
{
   BYTE addr[IPV4_ADDR_LEN];

#ifdef __cplusplus

   //---------------------------------------------------------
   // type assignment / tests (for compatibility)
   //---------------------------------------------------------
   void setIPv4(WORD32 ip) { *(WORD32 *)addr =   hton32  (ip); };
   void setIPv4(BYTE  *ip) { *(WORD32 *)addr = *(WORD32 *)ip ; };
   void setIPv6(BYTE  *ip) { }; // ignore
   void setIPv4()          { }; // ignore
   void setIPv6()          { }; // ignore
   bool  isIPv4()    const { return true   ; };
   bool  isIPv6()    const { return false  ; };
   bool  isLLA ()    const { return false  ; };
   int       af()    const { return AF_INET; };

   //---------------------------------------------------------
   // default test/set
   //---------------------------------------------------------
   bool  isDefault(bool bAvaya = false) const { return (*(WORD32 *)addr == hton32(bAvaya ? AVAYA_DEF_IPV4_U32 : XIRRUS_DEF_IPV4_U32)); };
   void setDefault(bool bAvaya = false)       {         *(WORD32 *)addr =  hton32(bAvaya ? AVAYA_DEF_IPV4_U32 : XIRRUS_DEF_IPV4_U32) ; };

   //---------------------------------------------------------
   // assignment operator
   //---------------------------------------------------------
   IPV4_ADDR& operator =(int i) { *((WORD32 *)addr) = hton32(i); return *this; };

   //---------------------------------------------------------
   // comparisons
   //---------------------------------------------------------
   bool operator ==(const IPV4_ADDR& ipv4) const { return (       *((const WORD32 *)addr)  ==        *((const WORD32 *)ipv4.addr)) ; };
   bool operator !=(const IPV4_ADDR& ipv4) const { return (       *((const WORD32 *)addr)  !=        *((const WORD32 *)ipv4.addr)) ; };
   bool operator < (const IPV4_ADDR& ipv4) const { return (ntoh32(*((const WORD32 *)addr)) <  ntoh32(*((const WORD32 *)ipv4.addr))); };
   bool operator > (const IPV4_ADDR& ipv4) const { return (ntoh32(*((const WORD32 *)addr)) >  ntoh32(*((const WORD32 *)ipv4.addr))); };
   bool operator <=(const IPV4_ADDR& ipv4) const { return (ntoh32(*((const WORD32 *)addr)) <= ntoh32(*((const WORD32 *)ipv4.addr))); };
   bool operator >=(const IPV4_ADDR& ipv4) const { return (ntoh32(*((const WORD32 *)addr)) >= ntoh32(*((const WORD32 *)ipv4.addr))); };

   //---------------------------------------------------------
   // unsigned representation
   //---------------------------------------------------------
   uint32 u32() const { return ( (uint32) ntoh32(*((const WORD32 *)addr)) ); };

   //---------------------------------------------------------
   // string methods
   //---------------------------------------------------------
   char *getString(      char sAscii[] ) const {         inet_ntop(AF_INET, addr, sAscii, IPV4_ADDR_STRLEN); return sAscii; }
   bool  setString(const char sAscii[] )       { return (inet_pton(AF_INET, sAscii, addr) == 1);                            }   // or use operator '='

   //---------------------------------------------------------
   // socket helpers (for compatibility)
   //---------------------------------------------------------
   struct sockaddr *sockaddr(struct sockaddr_in  *sin , int port) const { sin->sin_family = AF_INET ; sin->sin_port = hton16(port); memcpy(&sin->sin_addr.s_addr, addr, IPV4_ADDR_LEN); return ((struct sockaddr *)sin); };
   struct sockaddr *sockaddr(struct sockaddr_in6 *sin6, int port) const { return (sockaddr((struct sockaddr_in *)sin6, port)); };
   socklen_t        addrlen()                                     const { return (sizeof(struct sockaddr_in)); };

#endif // _cplusplus

} __attribute__ ((packed));

//******************************************************************************
/**
 * Abstracts an IPv6 address as defined by IEEE
 *
 * Note: This structure may be used in communication with straight C modules
 *       and must compile to 16 bytes - i.e. do not use virtual functions
 *       or other data members. Use the CIpv6Addr for more advanced features
 */
//------------------------------------------------------------------------------
#define NULL_IPV6_ADDR    (IPV6_ADDR){}
#define IPV6_ADDR_LEN     16
#define IPV6_ADDR_STRLEN  ((2*IPV6_ADDR_LEN) + 7 + 1) // 7 colons and 1 NULL terminator

// for compatibility with c code (when the rest of the methods confuse the compiler on things like offsetof)
struct ipv6_addr
{
   BYTE addr[IPV6_ADDR_LEN];
} __attribute__ ((packed));

struct IPV6_ADDR
{
   BYTE addr[IPV6_ADDR_LEN];

#ifdef __cplusplus

   //---------------------------------------------------------
   // type assignment / tests (for compatibility)
   //---------------------------------------------------------
   void setIPv6(BYTE  *ip) { *(WORD128 *)addr = *(WORD128 *)ip; };
   void setIPv4(WORD32 ip) { }; // ignore
   void setIPv4(BYTE  *ip) { }; // ignore
   void setIPv4()          { }; // ignore
   void setIPv6()          { }; // ignore
   bool  isIPv4()    const { return false; };
   bool  isIPv6()    const { return true ; };
   bool  isLLA ()    const { return (*(const WORD16 *)addr == hton16(0xfe80)); };
   int       af()    const { return AF_INET6; };

   //---------------------------------------------------------
   // default test/set
   //---------------------------------------------------------
   bool  isDefault(bool bAvaya = false) const { return (*(WORD128 *)addr == hton32(bAvaya ? AVAYA_DEF_IPV4_U32 : XIRRUS_DEF_IPV4_U32)); };
   void setDefault(bool bAvaya = false)       {         *(WORD128 *)addr =  hton32(bAvaya ? AVAYA_DEF_IPV4_U32 : XIRRUS_DEF_IPV4_U32) ; };

   //---------------------------------------------------------
   // assignment operator
   //---------------------------------------------------------
   IPV6_ADDR& operator =(int128 i) { *((WORD128 *)addr) = hton128(i); return *this; };

   //---------------------------------------------------------
   // comparisons
   //---------------------------------------------------------
   bool operator ==(const IPV6_ADDR& ipv6) const { return (        *((const WORD128 *)addr)  ==         *((const WORD128 *)ipv6.addr)) ; };
   bool operator !=(const IPV6_ADDR& ipv6) const { return (        *((const WORD128 *)addr)  !=         *((const WORD128 *)ipv6.addr)) ; };
   bool operator < (const IPV6_ADDR& ipv6) const { return (ntoh128(*((const WORD128 *)addr)) <  ntoh128(*((const WORD128 *)ipv6.addr))); };
   bool operator > (const IPV6_ADDR& ipv6) const { return (ntoh128(*((const WORD128 *)addr)) >  ntoh128(*((const WORD128 *)ipv6.addr))); };
   bool operator <=(const IPV6_ADDR& ipv6) const { return (ntoh128(*((const WORD128 *)addr)) <= ntoh128(*((const WORD128 *)ipv6.addr))); };
   bool operator >=(const IPV6_ADDR& ipv6) const { return (ntoh128(*((const WORD128 *)addr)) >= ntoh128(*((const WORD128 *)ipv6.addr))); };

   //---------------------------------------------------------
   // unsigned representation
   //---------------------------------------------------------
   uint128 u128() const { return ( (uint128) ntoh128(*((const WORD128 *)addr)) ); };

   //---------------------------------------------------------
   // string methods
   //---------------------------------------------------------
   char *getString(      char sAscii[] ) const { if (u128()) inet_ntop(AF_INET6, addr, sAscii, IPV6_ADDR_STRLEN); else *sAscii='\0'; return sAscii; }
   bool  setString(const char sAscii[] )       { return     (inet_pton(AF_INET6, sAscii, addr) == 1);                                               }   // or use operator '='

   //---------------------------------------------------------
   // socket helpers (for compatibility)
   //---------------------------------------------------------
   struct sockaddr *sockaddr(struct sockaddr_in  *sin , int port) const { sin ->sin_family  = AF_UNSPEC; return (NULL); };
   struct sockaddr *sockaddr(struct sockaddr_in6 *sin6, int port) const { sin6->sin6_family = AF_INET6 ; sin6->sin6_port = hton16(port); memcpy(sin6->sin6_addr.s6_addr, addr, IPV6_ADDR_LEN); return ((struct sockaddr *)sin6); };
   socklen_t        addrlen()                                     const { return (sizeof(struct sockaddr_in6)); };

#endif // _cplusplus

} __attribute__ ((packed));

//******************************************************************************
/**
 * Abstracts an IP address, either IPv4 or IPv6, as defined by IEEE
 *
 * Note: This structure may be used in communication with straight C modules
 *       and must compile to 20 bytes - i.e. do not use virtual functions
 *       or other data members.
 */
//------------------------------------------------------------------------------
#define NULL_IP_ADDR      (IP_ADDR){0}
#define IP_ADDR_LEN       IPV6_ADDR_LEN     // max of IPv4 & IPv6
#define IP_ADDR_STRLEN    IPV6_ADDR_STRLEN  // max of IPv4 & IPv6

// for compatibility with c code (when the rest of the methods confuse the compiler on things like offsetof)
struct ip_addr
{
   union {
       struct ipv4_addr ipv4;
       struct ipv6_addr ipv6;
       BYTE   addr[IPV6_ADDR_LEN];      // for backward compatibility
   };
   u8 pad[3];
   u8 family;
} __attribute__ ((packed));

struct IP_ADDR
{
    union {
        struct IPV4_ADDR ipv4;
        struct IPV6_ADDR ipv6;
        BYTE   addr[IPV6_ADDR_LEN];     // for backward compatibility
    };
    u8 pad[3];
    u8 family;

#ifdef __cplusplus

   //---------------------------------------------------------
   // type assignment / tests
   //---------------------------------------------------------
   void zeroPad()          {                        pad[0] = 0; pad[1] = 0; pad[2] = 0; };
   void setIPv4(WORD32 ip) { ipv4 =             ip; family  =  AF_INET  ; zeroPad();    };
   void setIPv4(BYTE  *ip) { ipv4 = *(WORD32  *)ip; family  =  AF_INET  ; zeroPad();    };
   void setIPv6(BYTE  *ip) { ipv6 = *(WORD128 *)ip; family  =  AF_INET6 ; zeroPad();    };
   void setIPv4()          {                        family  =  AF_INET  ; zeroPad();    };
   void setIPv6()          {                        family  =  AF_INET6 ; zeroPad();    };
   bool  isIPv4()    const {                return (family  != AF_INET6);               };
   bool  isIPv6()    const {                return (family  == AF_INET6);               };
   bool  isLLA ()    const {                return (isIPv6() ? ipv6.isLLA() : false  ); };
   int       af()    const {                return (isIPv6() ? AF_INET6     : AF_INET); };

   //---------------------------------------------------------
   // default test/set
   //---------------------------------------------------------
   bool  isDefault(bool bAvaya = false) const { return (isIPv6() ? ipv6. isDefault(bAvaya) : ipv4. isDefault(bAvaya)); };
   void setDefault(bool bAvaya = false)       {         isIPv6() ? ipv6.setDefault(bAvaya) : ipv4.setDefault(bAvaya) ; };

   //---------------------------------------------------------
   // assignment operators
   //---------------------------------------------------------
   IP_ADDR& operator =(int    i)            { ipv4 =  i; setIPv4(); return *this; };
   IP_ADDR& operator =(int128 i)            { ipv6 =  i; setIPv6(); return *this; };
   IP_ADDR& operator =(const IPV4_ADDR& ip) { ipv4 = ip; setIPv4(); return *this; };
   IP_ADDR& operator =(const IPV6_ADDR& ip) { ipv6 = ip; setIPv6(); return *this; };

   //---------------------------------------------------------
   // comparisons
   //---------------------------------------------------------
   bool operator ==(const IPV4_ADDR& ip) const { return (isIPv4() ? (ipv4 == ip) : false); };
   bool operator !=(const IPV4_ADDR& ip) const { return (isIPv4() ? (ipv4 != ip) : false); };
   bool operator < (const IPV4_ADDR& ip) const { return (isIPv4() ? (ipv4 <  ip) : false); };
   bool operator > (const IPV4_ADDR& ip) const { return (isIPv4() ? (ipv4 >  ip) : false); };
   bool operator <=(const IPV4_ADDR& ip) const { return (isIPv4() ? (ipv4 <= ip) : false); };
   bool operator >=(const IPV4_ADDR& ip) const { return (isIPv4() ? (ipv4 >= ip) : false); };

   bool operator ==(const IPV6_ADDR& ip) const { return (isIPv6() ? (ipv6 == ip) : false); };
   bool operator !=(const IPV6_ADDR& ip) const { return (isIPv6() ? (ipv6 != ip) : false); };
   bool operator < (const IPV6_ADDR& ip) const { return (isIPv6() ? (ipv6 <  ip) : false); };
   bool operator > (const IPV6_ADDR& ip) const { return (isIPv6() ? (ipv6 >  ip) : false); };
   bool operator <=(const IPV6_ADDR& ip) const { return (isIPv6() ? (ipv6 <= ip) : false); };
   bool operator >=(const IPV6_ADDR& ip) const { return (isIPv6() ? (ipv6 >= ip) : false); };

   bool operator ==(const IP_ADDR&   ip) const { return ((isIPv6() && ip.isIPv6()) ? (ipv6 == ip.ipv6) : (ipv4 == ip.ipv4)); };
   bool operator !=(const IP_ADDR&   ip) const { return ((isIPv6() && ip.isIPv6()) ? (ipv6 != ip.ipv6) : (ipv4 != ip.ipv4)); };
   bool operator < (const IP_ADDR&   ip) const { return ((isIPv6() && ip.isIPv6()) ? (ipv6 <  ip.ipv6) : (ipv4 <  ip.ipv4)); };
   bool operator > (const IP_ADDR&   ip) const { return ((isIPv6() && ip.isIPv6()) ? (ipv6 >  ip.ipv6) : (ipv4 >  ip.ipv4)); };
   bool operator <=(const IP_ADDR&   ip) const { return ((isIPv6() && ip.isIPv6()) ? (ipv6 <= ip.ipv6) : (ipv4 <= ip.ipv4)); };
   bool operator >=(const IP_ADDR&   ip) const { return ((isIPv6() && ip.isIPv6()) ? (ipv6 >= ip.ipv6) : (ipv4 >= ip.ipv4)); };

   //---------------------------------------------------------
   // unsigned representation
   //---------------------------------------------------------
   uint32   u32() const { return (isIPv6() ? ipv6.u128() : ipv4.u32()); };
   uint128 u128() const { return (isIPv6() ? ipv6.u128() : ipv4.u32()); };

   //---------------------------------------------------------
   // string methods
   //---------------------------------------------------------
   char *getString(      char sAscii[] ) const { if (isIPv6()) return (ipv6.getString(sAscii));           else return (ipv4.getString(sAscii));           };
   void  setString(const char sAscii[] )       { if                   (ipv4.setString(sAscii)) setIPv4(); else if     (ipv6.setString(sAscii)) setIPv6(); };   // or use operator '='

   //---------------------------------------------------------
   // socket helpers
   //---------------------------------------------------------
   struct sockaddr *sockaddr(struct sockaddr_in  *sin , int port) const { return (isIPv6() ? ipv6.sockaddr(sin , port) : ipv4.sockaddr(sin , port)); };
   struct sockaddr *sockaddr(struct sockaddr_in6 *sin6, int port) const { return (isIPv6() ? ipv6.sockaddr(sin6, port) : ipv4.sockaddr(sin6, port)); };
   socklen_t        addrlen()                                     const { return (isIPv6() ? ipv6.addrlen()            : ipv4.addrlen());            };

#endif // _cplusplus

} __attribute__ ((packed));


//******************************************************************************
/**
 * An optimized inline operation to copy a MAC_ADDR
 */
//------------------------------------------------------------------------------
#define BYTE_ADDR_COPY(_dest, _src)                            \
{                                                              \
   ((WORD32 *)  (_dest)    )[0] = ((WORD32 *)  (_src)    )[0]; \
   ((WORD16 *)&((_dest)[4]))[0] = ((WORD16 *)&((_src)[4]))[0]; \
}

#define htows(A)  ((((uint16)(A) & 0xff00) >> 8) | \
                   (((uint16)(A) & 0x00ff) << 8))

#define htowl(A)  ((((uint32)(A) & 0xff000000) >> 24) | \
                   (((uint32)(A) & 0x00ff0000) >>  8) | \
                   (((uint32)(A) & 0x0000ff00) <<  8) | \
                   (((uint32)(A) & 0x000000ff) << 24))

#define htowll(A) ((((uint64)(A) & 0xff00000000000000ULL) >> 56) | \
                   (((uint64)(A) & 0x00ff000000000000ULL) >> 40) | \
                   (((uint64)(A) & 0x0000ff0000000000ULL) >> 24) | \
                   (((uint64)(A) & 0x000000ff00000000ULL) >>  8) | \
                   (((uint64)(A) & 0x00000000ff000000ULL) <<  8) | \
                   (((uint64)(A) & 0x0000000000ff0000ULL) << 24) | \
                   (((uint64)(A) & 0x000000000000ff00ULL) << 40) | \
                   (((uint64)(A) & 0x00000000000000ffULL) << 56))

#define wtohs(A)  htows(A)
#define wtohl(A)  htowl(A)

#if !defined(BIT)
    #define BITS_PER_LONG 32
    #define BIT(x)      (1UL<<((x)%BITS_PER_LONG))
#endif

//******************************************************************************
/**
 * Min/Max macros
 */
//------------------------------------------------------------------------------
#if !defined(MIN) && !defined(MAX)
#define MIN(x,y)  ({ typeof(x) _x = (x), _y = (y); (_x < _y) ? _x : _y; })
#define MAX(x,y)  ({ typeof(x) _x = (x), _y = (y); (_x > _y) ? _x : _y; })
#endif

#endif // _COMMON_H
