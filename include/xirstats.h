//******************************************************************************
/** @file xirstats.h
 *
 * Statistics structures shared among all levels of software
 *
 * <I>Copyright (C) 2006 Xirrus. All Rights Reserved</I>
 *
 **/
//------------------------------------------------------------------------------
#ifndef _XIRSTATS_H
#define _XIRSTATS_H

#ifndef _XIRHOSTAPD_H
#include "xtypes.h"
#endif /* not defined _XIRHOSTAPD_H */
#include "xirver.h"

#define NUM_MCS_RATES   320     // 10 mcs-rates/stream x 4 streams x 4 channel widths (20, 40, 80, 160) x 2 guard intervals (short, long) = 320

//* simple_cnts strcuture
/**
 * @brief abridged counters struct
 */
struct simple_cnts {
    u64 packets;
    u64 bytes;
    u64 retries;
    u64 errors;
} __attribute__ ((packed));

struct simple_stats {
    struct simple_cnts rx;
    struct simple_cnts tx;
} __attribute__ ((packed));

#define SIZE_SIMPLE_STATS sizeof(struct simple_stats)

// NOTE: MAX_DPI_APP_INDEX must match the definition in nf_conntrack.h in the kernel

#define MAX_STA_DPI_CATEGORY    16
#define MAX_STA_DPI_PROTOCOL    32
#define MAX_DPI_CAT_INDEX       16
#define MAX_DPI_APP_INDEX       3000        // must keep MAX_DPI_APP_INDEX * MAX_DPI_APP_NAME (20) < 64KB to pass names in ioctl
#define MAX_DPI_APP_NAME        20
#define MAX_DPI_TYPE_INDEX      2
#define MAX_DPI_PROD_INDEX     (5 + 1)      // valid values = 1-5, +1 to allow zero relative
#define MAX_DPI_RISK_INDEX     (5 + 1)      // valid values = 1-5, +1 to allow zero relative
#define DPI_TYPE_MGT            1
#define DPI_TYPE_STA            0

struct dpi_stats_info {
    u32  index;
    u32  time_stamp;
    u64  tx_pkts;
    u64  rx_pkts;
    u64  tx_bytes;
    u64  rx_bytes;
} __attribute__ ((packed));

#define SIZE_DPI_STATS_INFO     sizeof(struct dpi_stats_info)

struct vlan_dpi_stats {
    struct {
        struct dpi_stats_info category[MAX_DPI_CAT_INDEX];
        struct dpi_stats_info protocol[MAX_DPI_APP_INDEX];
    } type[MAX_DPI_TYPE_INDEX];
} __attribute__ ((packed));

#define SIZE_VLAN_DPI_STATS      sizeof(struct vlan_dpi_stats)

struct sta_dpi_stats {
    struct dpi_stats_info category[MAX_STA_DPI_CATEGORY];
    struct dpi_stats_info protocol[MAX_STA_DPI_PROTOCOL];
} __attribute__ ((packed));

#define SIZE_STA_DPI_STATS      sizeof(struct sta_dpi_stats)

struct sta_stats {
    u64    period;
    u64    radio;
    u64    vlan;
    u64    associations;
    struct simple_stats          total;                 // for returning stats by station
    struct simple_stats   cck_rates[4];
    struct simple_stats  ofdm_rates[8];
    struct simple_stats   mcs_rates[NUM_MCS_RATES];
    struct sta_dpi_stats           dpi;
} __attribute__ ((packed));

#define SIZE_STA_STATS sizeof(struct sta_stats)

//* location tracking structures and macros
struct location_info {
    s64 first;
    s64 last;
    s64 max;
    s64 min;
    struct {
        s64 values;
        s64 squares;
        s64 cubes;
    } sum;
} __attribute__ ((packed));

#define SIZE_LOCATION_INFO       sizeof(struct location_info)

struct location_stats {
    s64 frames;
    s64 frequency;
    s64 time_first;
    s64 time_last;
    s64 bssid;
    struct location_info rcv_interval;
    struct location_info sig_strength;
} __attribute__ ((packed));

#define SIZE_LOCATION_STATS      sizeof(struct location_stats)

static inline void update_location_info(struct location_info *info, s64 now, s64 value) {
    info->last = now;
    if (info->first ==  0) info->first = now;
    if (value > info->max) info->max = value;
    if (value < info->min) info->min = value;
    info->sum.values  += value;
    info->sum.squares += value * value;
    info->sum.cubes   += value * value * value;
}

static inline void update_location_rcv_interval(struct location_stats *stats) {
    s64 interval = stats->rcv_interval.last ? stats->time_last - stats->rcv_interval.last : 0;
    if (interval > 24*60*60) {  // if greater than 24 hours, clear stats
        interval = 0;
        memset(&stats->rcv_interval, 0, sizeof(stats->rcv_interval));
        memset(&stats->sig_strength, 0, sizeof(stats->sig_strength));
    }
    if (stats->rcv_interval.max == 0)
        stats->rcv_interval.min = 1000;
    update_location_info(&stats->rcv_interval, stats->time_last, interval);
}

static inline void update_location_sig_strength(struct location_stats *stats, int rssi) {
    if (stats->sig_strength.min == 0)
        stats->sig_strength.max = -1000;
    update_location_info(&stats->sig_strength, (s64)rssi, (s64)rssi);
}


//* info_cnts structure
/**
 * @brief info (as opposed to error) stats structure
 */
struct info_cnts {
    u64 bytes;                                      //total bytes received / transmitted
    u64 unicasts;                                   //total data packets received / transmitted
    u64 multicasts;                                 //multicast packets received / transmitted
    u64 broadcasts;                                 //broadcast packets received / transmitted
    u64 management;                                 //total data packets received / transmitted
    u64 beacons;                                    //total beacons received / transmitted
    u64 fragments;                                  //total fragments received / transmitted
    u64 rts_count;                                  //count rts's transmitted/received
    u64 cts_count;                                  //count cts's transmitted/received
} __attribute__ ((packed));

struct rx_errs {
    u64 total;                                      //number of packets w/ any kind of errors
    u64 dropped;                                    //no space in linux buffers
    u64 crc;                                        //number of rx packets w/ crc errors
    u64 fragmentation;                              //fragmentation re-assembly errors
    u64 encryption;                                 //number of rx's that could not be decrypted
    u64 unassociated;                               //rx from unassociated sta
    u64 duplicates;                                 //number of duplicate rx's received
    u64 overruns;                                   //receiver ring buff overflow
} __attribute__ ((packed));

struct rx_retries {
    u64 total;                                      //count rx's that have retried bit set / or tx retries
} __attribute__ ((packed));

struct tx_errs {
    u64 total;                                      //number of packets w/ any kind of errors
    u64 dropped;                                    //no space in our tx queue
    u64 unassociated;                               //tx to unassociated station
    u64 ack_failures;                               //number of tx's that failed (due to no ack)
    u64 rts_failures;                               //number of tx's aborted because no cts received
    u64 soft_resets;                                //number soft resets
    u64 hard_resets;                                //number hard resets
} __attribute__ ((packed));

struct tx_retries {
    u64 total;                                      //count rx's that have retried bit set / or tx retries
    u64 multiple;                                   //packets that had just multiple retries, then succeeded or failed
    u64 single;                                     //packets that had just one retry, then succeeded
    u64 rts;                                        //count rts retries (not incremented if first rts is successful)
} __attribute__ ((packed));

struct rx_cnts {
    struct info_cnts  info;
    struct rx_errs    errs;
    struct rx_retries retries;
} __attribute__ ((packed));

struct tx_cnts {
    struct info_cnts  info;
    struct tx_errs    errs;
    struct tx_retries retries;
} __attribute__ ((packed));

struct ratio {
    u64 sum;                                        //sum   of measurements
    u64 cnt;                                        //count of measurements
} __attribute__ ((packed));

struct carrier_stats {
    struct ratio tx_rx;                             //tx & rx activity (in 4us increments) (mbusy)
    struct ratio noise;                             //energy detect    (in 4us increments) (xbusy & !mbusy)
} __attribute__ ((packed));

struct packet_stats {
    struct ratio unicast;                            //sum of unicast    packet values
    struct ratio multicast;                          //sum of multicast  packet values
    struct ratio broadcast;                          //sum of broadcast  packet values
    struct ratio management;                         //sum of management packet values
    struct ratio beacon;                             //sum of beacon     packet values
} __attribute__ ((packed));

struct signal_stats {
    struct ratio tx_rate;                            //sum of tx packet rate's
    struct ratio rx_rate;                            //sum of rx packet rate's
    struct ratio rate;                               //sum of tx & rx packet rate's
    struct ratio rssi;                               //sum of rx packet rssi's
    struct ratio silence;                            //sum of rx packet silence measurements
    struct ratio snr;                                //sum of rx packet snr measurements
} __attribute__ ((packed));

struct info_stats {
    u64  dsp_index;                                  //display index (used for sorting)
    char sw_label[8];                                //software name of radio
    u64  enabled;                                    //radio enable
    u64  band;                                       //band radio is on
    u64  channel;                                    //channel radio is on
    u64  channel_bond_40mhz;                         //40MHz bonded channel radio is on
    u64  channel_bond_80mhz;                         //80MHz bonded channel radio is on
    u64  channel_bond_160mhz;                        //160MHz bonded channel radio is on
    u64  min_rate;                                   //minimum data rate for radio
    u64  time;                                       //time in ms
    u64  last_nfcal;                                 //time since last nf_cal in ms
    u64  rogue_blocked;                              //count of remaining passes to block rogues
} __attribute__ ((packed));

#define SIZE_INFO_STATS sizeof(struct info_stats)

//* all_stats strcuture
/**
 * @brief stats structure for the apme
 */
struct iap_stats {
    struct rx_cnts rx;
    struct tx_cnts tx;
    struct simple_cnts             total;
    struct simple_stats          unicast;
    struct simple_stats     cck_rates[4];
    struct simple_stats    ofdm_rates[8];
    struct simple_stats     mcs_rates[NUM_MCS_RATES];
} __attribute__ ((packed));

#define SIZE_IAP_STATS sizeof(struct iap_stats)

struct ratio_stats {
    struct carrier_stats    carrier_busy;
    struct signal_stats   signal_quality;
    struct packet_stats   rssi_by_packet;
    struct packet_stats    snr_by_packet;
} __attribute__ ((packed));

#define SIZE_RATIO_STATS sizeof(struct ratio_stats)

struct radio_stats {                                //must be iap_stats, followed by ratio stats, followed by info stats
    struct rx_cnts rx;
    struct tx_cnts tx;
    struct simple_cnts             total;
    struct simple_stats          unicast;
    struct simple_stats     cck_rates[4];
    struct simple_stats    ofdm_rates[8];
    struct simple_stats     mcs_rates[NUM_MCS_RATES];
    struct carrier_stats    carrier_busy;
    struct signal_stats   signal_quality;
    struct packet_stats   rssi_by_packet;
    struct packet_stats    snr_by_packet;
    struct info_stats               info;           //must be last in structure
} __attribute__ ((packed));

#define SIZE_RADIO_STATS sizeof(struct radio_stats)


#define IDS_STATS_TIME_SLOT                 10
#define IDS_STATS_NUM_TIME_SLOTS            360
#define IDS_STATS_NUM_MGMT_PKT_TYPES        16

#define IDS_STATS_ASSOC_OFFSET              0
#define IDS_STATS_REASSOC_OFFSET            2
#define IDS_STATS_PROBE_REQ_OFFSET          4
#define IDS_STATS_PROBE_RESP_OFFSET         5
#define IDS_STATS_BEACON_OFFSET             8
#define IDS_STATS_DISASSOC_OFFSET           10
#define IDS_STATS_AUTH_OFFSET               11
#define IDS_STATS_DEAUTH_OFFSET             12

#define IDS_STATS_SPOOFED_BEACON_OFFSET     (IDS_STATS_NUM_MGMT_PKT_TYPES + IDS_STATS_BEACON_OFFSET)
#define IDS_STATS_SPOOFED_DISASSOC_OFFSET   (IDS_STATS_NUM_MGMT_PKT_TYPES + IDS_STATS_DISASSOC_OFFSET)
#define IDS_STATS_SPOOFED_DEAUTH_OFFSET     (IDS_STATS_NUM_MGMT_PKT_TYPES + IDS_STATS_DEAUTH_OFFSET)

#define IDS_STATS_EAP_OFFSET                (2*IDS_STATS_NUM_MGMT_PKT_TYPES)
#define IDS_STATS_NULL_PROBE_RESP_OFFSET    (2*IDS_STATS_NUM_MGMT_PKT_TYPES + 1)
#define IDS_STATS_MIC_ERROR_OFFSET          (2*IDS_STATS_NUM_MGMT_PKT_TYPES + 2)
#define IDS_STATS_SEQ_NUM_ANOMALY_OFFSET    (2*IDS_STATS_NUM_MGMT_PKT_TYPES + 3)

struct ids_pkt_stats {
    u8 idx_wrap;
    u16 cur_idx;
    u32 cur_count;
    u16 cur_count_slots;
    u32 cur_avg;
    u32 cur_max;
    u32 packets[IDS_STATS_NUM_TIME_SLOTS];
} __attribute__ ((packed));

struct ids_sta_stats {
    u8 addr[6];
    struct ids_pkt_stats seq_num_anomalies;
} __attribute__ ((packed));

struct ids_stats {
    struct ids_pkt_stats mgmt_pkts[IDS_STATS_NUM_MGMT_PKT_TYPES];
    struct ids_pkt_stats spoofed_mgmt_pkts[IDS_STATS_NUM_MGMT_PKT_TYPES];
    struct ids_pkt_stats eap_pkts;
    struct ids_pkt_stats null_probe_resps;
    struct ids_pkt_stats mic_errs;
    struct ids_pkt_stats seq_num_anomalies;
} __attribute__ ((packed));

#define SIZE_IDS_STATS sizeof(struct ids_stats)

struct pkt_type_info
{
    s8  rssi;
    s8  snr;
    s8  silence;
    u32 tx_rate;
    u8  tx_rate_10th;
    u32 rx_rate;
    u8  rx_rate_10th;
} __attribute__ ((packed));

#define SIZE_PKT_TYPE_INFO      sizeof(struct pkt_type_info)

struct mac_sta_info {                               //for get_sta_info cmd
    u8   addr[6];                                    // mac addr of sta
    u16  aid;                                        // aid == 0 means we have no such sta in our table
    u32  secs_since_activity;                        // seconds since last packet transmitted to, or received from this sta
    u32  secs_since_stats;                           // seconds since we began collecting stats for this guy
    u32  ip_addr;
    u128 ip6_addr;
    s8   radio;
    u8   rssi;
    u8   snr;
    u8   silence;
    u8   band;
    u8   probe_req_rssi     [MAX_IAPS];
    u8   probe_req_snr      [MAX_IAPS];
    u8   probe_req_silence  [MAX_IAPS];
    u32  probe_req_timestamp[MAX_IAPS];
    u32  tx_rate;
    u8   tx_rate_10th;
    u32  rx_rate;
    u8   rx_rate_10th;
    u8   ofdm_capable;
    u8   ht_capable;
    u8   vht_capable;
    u64  probe_ht_rates;
    u64  associations;
    struct simple_stats stats;                       // for returning stats by station
    struct pkt_type_info mgmt;
    struct pkt_type_info data;
    struct location_stats location;
} __attribute__ ((packed));

// must match definition of sta_rssi_entry in xircfg.h
struct mac_rssi_entry {
    u8  addr[6];                                    // mac addr of sta
    s8  ssid ;                                      // user group index of sta
    s8  group;                                      // user group index of sta
    s8  rssi ;                                      // rssi of sta
    u32 time ;                                      // time stamp when last heard
} __attribute__ ((packed));

static inline void set_stats_totals(struct radio_stats *stats)
{
    stats->total.packets = stats->rx.info.unicasts + stats->rx.info.multicasts + stats->rx.info.broadcasts + stats->rx.info.beacons + stats->rx.errs.total +
                           stats->tx.info.unicasts + stats->tx.info.multicasts + stats->tx.info.broadcasts + stats->tx.info.beacons + stats->tx.errs.total;
    stats->total.bytes   = stats->rx.info.bytes    + stats->tx.info.bytes;
    stats->total.retries = stats->rx.retries.total + stats->tx.retries.total;
    stats->total.errors  = stats->rx.errs.total    + stats->tx.errs.total;
}

static inline void set_11n_carrier_busy(struct radio_stats *stats)
{
    u64 stats_total_bad_pkts = stats->total.errors;

    stats->carrier_busy.tx_rx.sum = stats->signal_quality.rate.sum ? (stats->signal_quality.rate.cnt * stats->total.bytes   * 8) / stats->signal_quality.rate.sum : 0;
    stats->carrier_busy.noise.sum = stats->total.packets           ? (stats->carrier_busy.tx_rx.sum  * stats_total_bad_pkts * 8) / stats->total.packets           : 0;

    stats->carrier_busy.tx_rx.cnt = stats->carrier_busy.tx_rx.sum  ?  stats->info.time * 1000ULL : 0;
    stats->carrier_busy.noise.cnt = stats->carrier_busy.noise.sum  ?  stats->info.time * 1000ULL : 0;

    if (stats->carrier_busy.tx_rx.sum > stats->carrier_busy.tx_rx.cnt) stats->carrier_busy.tx_rx.sum = stats->carrier_busy.tx_rx.cnt;
    if (stats->carrier_busy.noise.sum > stats->carrier_busy.noise.cnt) stats->carrier_busy.noise.sum = stats->carrier_busy.noise.cnt;

    if (stats->carrier_busy.tx_rx.sum +
        stats->carrier_busy.noise.sum > stats->carrier_busy.tx_rx.cnt) stats->carrier_busy.noise.sum = stats->carrier_busy.tx_rx.cnt -
                                                                                                       stats->carrier_busy.tx_rx.sum;
}

#define AVG_SIGNAL_WEIGHT     50
static inline int avg_signal_w(int old_val, int new_val, int weight)
{
    return ((old_val && new_val) ? (((old_val)*weight + (new_val)*(100 - weight) + ((old_val > 0) ? 50 : -50))/100) : (old_val) ? (old_val) : (new_val));
}
static inline int avg_signal(int old_val, int new_val)
{
  return (avg_signal_w(old_val, new_val, AVG_SIGNAL_WEIGHT));
}
#endif //_XIRSTATS_H



