//******************************************************************************
/** @file xtypes.h
 *
 * <I>Copyright (C) 2006 Xirrus. All Rights Reserved</I>
 *
 **/
//------------------------------------------------------------------------------
#ifndef _XTYPES_H
#define _XTYPES_H

#if !defined(__KERNEL__)

#include <stdint.h>

//****************************************************************
// arch dependent definitions for use in printf and scanf
//----------------------------------------------------------------
#if   __WORDSIZE == 64
#define CC32 ""
#define CC64 "l"
#elif __WORDSIZE == 32
#define CC32 ""
#define CC64 "ll"
#else
#define CC32 "l"
#define CC64 "ll"
#endif

#if !defined(COMMON_H) && !defined(_XIRDHCP_H)   // dodge hostapd & dhcp redefines
    typedef uint8_t     u8     ;
    typedef uint16_t    u16    ;
    typedef uint32_t    u32    ;
#endif

#if !defined(COMMON_H)                           // dodge hostapd redefines
    typedef uint64_t    u64    ;

    typedef int8_t      s8     ;
    typedef int16_t     s16    ;
    typedef int32_t     s32    ;
    typedef int64_t     s64    ;
#endif

    typedef int8_t      int8   ;
    typedef int16_t     int16  ;
    typedef int32_t     int32  ;
    typedef int64_t     int64  ;

    typedef uint8_t     uint8  ;
    typedef uint16_t    uint16 ;
    typedef uint32_t    uint32 ;
    typedef uint64_t    uint64 ;

    typedef uint8_t     uchar  ;
    typedef uint8_t     BYTE   ;
    typedef uint8_t     WORD8  ;
    typedef uint16_t    WORD16 ;
    typedef uint32_t    WORD32 ;
    typedef uint64_t    WORD64 ;

#endif  //!defined(__KERNEL__)

    typedef           __int128   int128_t;
    typedef unsigned  __int128  uint128_t;

    typedef uint128_t   u128   ;
    typedef uint128_t   uint128;
    typedef uint128_t   WORD128;

    typedef int128_t    s128   ;
    typedef int128_t    int128 ;

#endif  //_XTYPES_H

