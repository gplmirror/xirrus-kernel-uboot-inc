//******************************************************************************
/** @file xir_nlmark.h
 *
 * macros for the setting and reading xirrus defined skb->nlmark (net link mark) fields
 *
 * <I>Copyright (C) 2012 Xirrus. All Rights Reserved</I>
 **/
//------------------------------------------------------------------------------
#ifndef _XIR_NLMARK_H
#define _XIR_NLMARK_H

// NOTE: DPI_MARK_WORKING  must match the definition in nf_conntrack.h in the kernel
// NOTE: DPI_MARK_COMPLETE must match the definition in nf_conntrack.h in the kernel

#include <bitfield.h>
#include <xtypes.h>

#define        DPI_MARK_WORKING              bit_const_64(31)
#define        DPI_MARK_COMPLETE             bit_const_64(30)
#define        DPI_MARK_RX_STA               bit_const_64(29)
#define        DPI_MARK_RX_MGT               bit_const_64(28)
#define        DPI_MARK_MGT                  bit_const_64(27)
#define        DPI_MARK_WDS                  bit_const_64(26)

#define        DPI_MARK_PROGRESS             (DPI_MARK_COMPLETE | DPI_MARK_WORKING)
#define        DPI_MARK_RX_FLAG              (DPI_MARK_RX_STA   | DPI_MARK_RX_MGT )

#define        DPI_MARK_VLAN_IDX_SHIFT       20
#define        DPI_MARK_PROT_IDX_SHIFT       20
#define        DPI_MARK_APP_LIST_SHIFT       16
#define        DPI_MARK_CATEGORY_SHIFT       12
#define        DPI_MARK_PROTOCOL_SHIFT        0

#define        DPI_MARK_VLAN_IDX_MASK        field_mask_64(25, 20)
#define        DPI_MARK_PROT_IDX_MASK        field_mask_64(25, 20)
#define        DPI_MARK_APP_LIST_MASK        field_mask_64(19, 16)
#define        DPI_MARK_CATEGORY_MASK        field_mask_64(15, 12)
#define        DPI_MARK_PROTOCOL_MASK        field_mask_64(11,  0)

#define    GET_DPI_MARK_VLAN_IDX(m)          get_field_64( 25, 20, (u64)(m))
#define    GET_DPI_MARK_PROT_IDX(m)          get_field_64( 25, 20, (u64)(m))
#define    GET_DPI_MARK_APP_LIST(m)          get_field_64( 19, 16, (u64)(m))
#define    GET_DPI_MARK_CATEGORY(m)          get_field_64( 15, 12, (u64)(m))
#define    GET_DPI_MARK_PROTOCOL(m)          get_field_64( 11,  0, (u64)(m))

#define    SET_DPI_MARK_VLAN_IDX(m)          set_field_64( 25, 20, (u64)(m))
#define    SET_DPI_MARK_PROT_IDX(m)          set_field_64( 25, 20, (u64)(m))
#define    SET_DPI_MARK_APP_LIST(m)          set_field_64( 19, 16, (u64)(m))
#define    SET_DPI_MARK_CATEGORY(m)          set_field_64( 15, 12, (u64)(m))
#define    SET_DPI_MARK_PROTOCOL(m)          set_field_64( 11,  0, (u64)(m))

#define ASSIGN_DPI_MARK_VLAN_IDX(m, s)       (m = ((m) & ~(DPI_MARK_VLAN_IDX_MASK)) | SET_DPI_MARK_VLAN_IDX(s))
#define ASSIGN_DPI_MARK_PROT_IDX(m, s)       (m = ((m) & ~(DPI_MARK_PROT_IDX_MASK)) | SET_DPI_MARK_PROT_IDX(s))
#define ASSIGN_DPI_MARK_APP_LIST(m, s)       (m = ((m) & ~(DPI_MARK_APP_LIST_MASK)) | SET_DPI_MARK_APP_LIST(s))
#define ASSIGN_DPI_MARK_CATEGORY(m, s)       (m = ((m) & ~(DPI_MARK_CATEGORY_MASK)) | SET_DPI_MARK_CATEGORY(s))
#define ASSIGN_DPI_MARK_PROTOCOL(m, s)       (m = ((m) & ~(DPI_MARK_PROTOCOL_MASK)) | SET_DPI_MARK_PROTOCOL(s))

#define  MATCH_DPI_MARK_VLAN_IDX_MASK        DPI_MARK_VLAN_IDX_MASK
#define  MATCH_DPI_MARK_PROT_IDX_MASK        DPI_MARK_PROT_IDX_MASK
#define  MATCH_DPI_MARK_APP_LIST_MASK        DPI_MARK_APP_LIST_MASK
#define  MATCH_DPI_MARK_CATEGORY_MASK        DPI_MARK_CATEGORY_MASK
#define  MATCH_DPI_MARK_PROTOCOL_MASK        DPI_MARK_PROTOCOL_MASK

#define  MATCH_DPI_MARK_VLAN_IDX(m)          SET_DPI_MARK_VLAN_IDX(m)
#define  MATCH_DPI_MARK_PROT_IDX(m)          SET_DPI_MARK_PROT_IDX(m)
#define  MATCH_DPI_MARK_APP_LIST(m)          SET_DPI_MARK_APP_LIST(m)
#define  MATCH_DPI_MARK_CATEGORY(m)          SET_DPI_MARK_CATEGORY(m)
#define  MATCH_DPI_MARK_PROTOCOL(m)          SET_DPI_MARK_PROTOCOL(m)

#define  DPI_VALID_CT_PROTOCOL_MASK          (DPI_MARK_COMPLETE | DPI_MARK_MGT | DPI_MARK_PROTOCOL_MASK | DPI_MARK_CATEGORY_MASK | DPI_MARK_APP_LIST_MASK)

//------------------------------------------------------------------------------
// dpi protocol stack definitions
//------------------------------------------------------------------------------
#define    MAX_DPI_MARK_PROTOCOL_DEPTH         3

#define        DPI_MARK_PROTOCOL0_SHIFT        0
#define        DPI_MARK_PROTOCOL1_SHIFT       32
#define        DPI_MARK_PROTOCOL2_SHIFT       44

#define        DPI_MARK_PROTOCOL0_MASK       field_mask_64(11,   0)
#define        DPI_MARK_PROTOCOL1_MASK       field_mask_64(43,  32)
#define        DPI_MARK_PROTOCOL2_MASK       field_mask_64(55,  44)

#define    GET_DPI_MARK_PROTOCOL0(m)         get_field_64( 11,   0, (u64)(m))
#define    GET_DPI_MARK_PROTOCOL1(m)         get_field_64( 43,  32, (u64)(m))
#define    GET_DPI_MARK_PROTOCOL2(m)         get_field_64( 55,  44, (u64)(m))

#define    SET_DPI_MARK_PROTOCOL0(m)         set_field_64( 11,   0, (u64)(m))
#define    SET_DPI_MARK_PROTOCOL1(m)         set_field_64( 43,  32, (u64)(m))
#define    SET_DPI_MARK_PROTOCOL2(m)         set_field_64( 55,  44, (u64)(m))

#define    DPI_MARK_PROTOCOL_MASK_IDX(idx) ({                \
    u64 result;                                              \
    switch (idx) {                                           \
        case  0: result = DPI_MARK_PROTOCOL0_MASK;   break;  \
        case  1: result = DPI_MARK_PROTOCOL1_MASK;   break;  \
        case  2: result = DPI_MARK_PROTOCOL2_MASK;   break;  \
        default: result = 0;                         break;  \
    }                                                        \
    result;                                                  \
})

#define    GET_DPI_MARK_PROTOCOL_IDX(m, idx) ({              \
    u64 result;                                              \
    switch (idx) {                                           \
        case  0: result = GET_DPI_MARK_PROTOCOL0(m); break;  \
        case  1: result = GET_DPI_MARK_PROTOCOL1(m); break;  \
        case  2: result = GET_DPI_MARK_PROTOCOL2(m); break;  \
        default: result = 0;                         break;  \
    }                                                        \
    result;                                                  \
})

#define    SET_DPI_MARK_PROTOCOL_IDX(m, idx) ({              \
    u64 result;                                              \
    switch (idx) {                                           \
        case  0: result = SET_DPI_MARK_PROTOCOL0(m); break;  \
        case  1: result = SET_DPI_MARK_PROTOCOL1(m); break;  \
        case  2: result = SET_DPI_MARK_PROTOCOL2(m); break;  \
        default: result = 0;                         break;  \
    }                                                        \
    result;                                                  \
})

//------------------------------------------------------------------------------
// conntrack mark definitions (for stateful mangle table)
//------------------------------------------------------------------------------
#define        CTMARK_DROPPED                bit_const(0)
#define        CTMARK_ACCEPTED               bit_const(1)
#define        CTMARK_CLASSIFIED             bit_const(2)
#define        CTMARK_DEV_CHANGE             bit_const(3)
#define        CTMARK_DEVICE_SRC_MASK        field_mask( 7, 4)
#define        CTMARK_DEVICE_DST_MASK        field_mask(11, 8)

#define    GET_CTMARK_DEVICE_SRC(m)          get_field( 7, 4, m)
#define    GET_CTMARK_DEVICE_DST(m)          get_field(11, 8, m)

#define    SET_CTMARK_DEVICE_SRC(m)          set_field( 7, 4, m)
#define    SET_CTMARK_DEVICE_DST(m)          set_field(11, 8, m)

#define ASSIGN_CTMARK_DEVICE_SRC(m, s)       (atomic32_and(m, ~(CTMARK_DEVICE_SRC_MASK)), atomic32_or(m, CTMARK_DEV_CHANGE | SET_CTMARK_DEVICE_SRC(s)))
#define ASSIGN_CTMARK_DEVICE_DST(m, s)       (atomic32_and(m, ~(CTMARK_DEVICE_DST_MASK)), atomic32_or(m, CTMARK_DEV_CHANGE | SET_CTMARK_DEVICE_DST(s)))

#endif // _XIR_NLMARK_H

