//******************************************************************************
/** @file xiriap.h
 *
 * IAP (radio) constants
 *
 * <I>Copyright (C) 2005 Xirrus. All Rights Reserved</I>
 *
 **/
//------------------------------------------------------------------------------
#ifndef _XIRIAP_H
#define _XIRIAP_H

#include "xirver.h"
#include "common.h"
#include "ratemask.h"

//****************************************************************
// IAP band and antenna definitions
//----------------------------------------------------------------
#define IAP_BAND_BG         2
#define IAP_BAND_A          5
#define IAP_BAND_ABG        (IAP_BAND_A + IAP_BAND_BG)

#define IAP_11N             0
#define IAP_11AC            1
#define IAP_11AC_WAVE2      2

#define IAP_1x1             1
#define IAP_2x2             2
#define IAP_2x3             2
#define IAP_3x3             3
#define IAP_4x4             4

#define ANT_NONE           -1
#define ANT_EXTERNAL        0
#define ANT_INT_DIR         1
#define ANT_INT_OMNI        2

#define ANT_SEC             0           // secondary antenna, either external or internal omni
#define ANT_PRI             1           // primary antenna, always internal directional

#define MAX_NUM_ANT         2

#define MAX_TRANSMIT_POWER  20          // used for building .11h beacon IEs

//****************************************************************
// XR Array IAP board type table
//----------------------------------------------------------------
struct iap_board_type
{
    char part_number[18 + 1];
    u8   wifi_specification;
    u8   num_spatial_streams;
    u8   bands_supported;
    u8   antenna_type;
    u8   sgi20_capable;
    u8   ac_wave_number;
} __attribute__ ((packed));

#define SIZE_IAP_BOARD_TYPE sizeof(struct iap_board_type)

                                /* part number   wifi spec       streams  bands         antenna       sgi20  ac wave      Notes                                                                               */
#define XR_IAP_INFO_TABLE       {"100-0007-103", IAP_11AC_WAVE2, IAP_4x4, IAP_BAND_ABG, ANT_EXTERNAL, 1,     2      }, /* ODM   , PCIE 4x4 Broadcom  2.4/5GHz BCM43465 Radio Card, PCIe , Skyworks, LiteOn    */ \
                                {"100-0007-104", IAP_11AC_WAVE2, IAP_4x4, IAP_BAND_A  , ANT_EXTERNAL, 1,     2      }, /* ODM   , PCIE 4x4 Broadcom      5GHz BCM43465 Radio Card, PCIe , Skyworks, LiteOn    */ \
                                {"100-0119-001", IAP_11N       , IAP_3x3, IAP_BAND_ABG, ANT_INT_DIR , 1,     0      }, /* Xirrus, PCIE 3x3 Osprey    2.4/5GHz   AR9390 Radio Card                             */ \
                                {"100-0119-002", IAP_11N       , IAP_3x3, IAP_BAND_ABG, ANT_INT_DIR , 1,     0      }, /* Xirrus, PCIE 3x3 Osprey    2.4/5GHz   AR9390 Radio Card                             */ \
                                {"100-0119-003", IAP_11N       , IAP_2x2, IAP_BAND_ABG, ANT_INT_DIR , 1,     0      }, /* Xirrus, PCIE 2x2 Osprey    2.4/5GHz   AR9392 Radio Card                             */ \
                                {"100-0119-004", IAP_11N       , IAP_3x3, IAP_BAND_ABG, ANT_EXTERNAL, 1,     0      }, /* Xirrus, PCIE 3x3 Osprey    2.4/5GHz   AR9390 Radio Card, hardened                   */ \
                                {"100-0119-005", IAP_11N       , IAP_2x2, IAP_BAND_ABG, ANT_EXTERNAL, 1,     0      }, /* Xirrus, PCIE 2x2 Osprey    2.4/5GHz   AR9392 Radio Card, hardened                   */ \
                                {"100-0119-006", IAP_11N       , IAP_3x3, IAP_BAND_ABG, ANT_EXTERNAL, 1,     0      }, /* Xirrus, PCIE 3x3 Osprey    2.4/5GHz   AR9390 Radio Card, hardened                   */ \
                                {"100-0119-007", IAP_11N       , IAP_2x2, IAP_BAND_ABG, ANT_EXTERNAL, 1,     0      }, /* Xirrus, PCIE 2x2 Osprey    2.4/5GHz   AR9392 Radio Card, hardened                   */ \
                                {"100-0119-008", IAP_11N       , IAP_2x2, IAP_BAND_ABG, ANT_INT_DIR , 1,     0      }, /* Xirrus, PCIE 2x2 Osprey    2.4/5GHz   AR9392 Radio Card                             */ \
                                {"100-0119-009", IAP_11N       , IAP_3x3, IAP_BAND_ABG, ANT_INT_DIR , 1,     0      }, /* Xirrus, PCIE 3x3 Osprey    2.4/5GHz   AR9390 Radio Card                             */ \
                                {"100-0119-010", IAP_11N       , IAP_2x2, IAP_BAND_ABG, ANT_EXTERNAL, 1,     0      }, /* Xirrus, PCIE 2x2 Osprey    2.4/5GHz   AR9392 Radio Card, hardened                   */ \
                                {"100-0125-001", IAP_11N       , IAP_2x2, IAP_BAND_ABG, ANT_INT_DIR , 0,     0      }, /* Xirrus, MPCI 2x2 Merlin    2.4/5GHz   AR9280 Radio Card, development only           */ \
                                {"100-0125-003", IAP_11N       , IAP_2x2, IAP_BAND_ABG, ANT_INT_DIR , 0,     0      }, /* Xirrus, MPCI 2x2 Merlin    2.4/5GHz   AR9280 Radio Card, development only           */ \
                                {"100-0125-005", IAP_11N       , IAP_2x2, IAP_BAND_ABG, ANT_INT_DIR , 0,     0      }, /* Xirrus, MPCI 2x2 Merlin    2.4/5GHz   AR9280 Radio Card, development only           */ \
                                {"100-0155-001", IAP_11N       , IAP_3x3, IAP_BAND_A  , ANT_INT_OMNI, 0,     0      }, /* ODM   , MPCI 2X2 Merlin        5GHZ   AR9220 Radio Card, early                      */ \
                                {"100-0156-001", IAP_11N       , IAP_2x2, IAP_BAND_ABG, ANT_INT_OMNI, 0,     0      }, /* ODM   , MPCI 2X2 Merlin    2.4/5GHZ   AR9220 Radio Card, early                      */ \
                                {"100-0161-001", IAP_11AC      , IAP_3x3, IAP_BAND_ABG, ANT_INT_DIR , 1,     1      }, /* Xirrus, PCIE 3x3 Peregrine 2.4/5GHz  QCA9890 Radio Card                             */ \
                                {"100-0161-002", IAP_11AC      , IAP_2x2, IAP_BAND_ABG, ANT_INT_DIR , 1,     1      }, /* Xirrus, PCIE 2x2 Peregrine 2.4/5GHz  QCA9892 Radio Card                             */ \
                                {"100-0161-003", IAP_11AC      , IAP_3x3, IAP_BAND_ABG, ANT_EXTERNAL, 1,     1      }, /* Xirrus, PCIE 3x3 Peregrine 2.4/5GHz  QCA9890 Radio Card, SMA connector              */ \
                                {"100-0161-004", IAP_11AC      , IAP_2x2, IAP_BAND_ABG, ANT_EXTERNAL, 1,     1      }, /* Xirrus, PCIE 2x2 Peregrine 2.4/5GHz  QCA9892 Radio Card, SMA connector              */ \
                                {"100-0165-001", IAP_11AC      , IAP_3x3, IAP_BAND_ABG, ANT_INT_DIR , 1,     1      }, /* Xirrus, PCIE 3x3 Peregrine 2.4/5GHz  QCA9890 Radio Card, FEM development            */ \
                                {"100-0166-001", IAP_11AC      , IAP_3x3, IAP_BAND_ABG, ANT_INT_OMNI, 1,     1      }, /* Xirrus, PCIE 3x3 Peregrine 2.4/5GHz  QCA9880 Radio Card, XR630                      */ \
                                {"100-0166-002", IAP_11AC      , IAP_2x2, IAP_BAND_ABG, ANT_INT_OMNI, 1,     1      }, /* Xirrus, PCIE 2x2 Peregrine 2.4/5GHz  QCA9882 Radio Card, XR620,  XH2                */ \
                                {"100-0166-011", IAP_11AC      , IAP_3x3, IAP_BAND_ABG, ANT_INT_OMNI, 1,     1      }, /* Xirrus, PCIE 3x3 Peregrine 2.4/5GHz  QCA9890 Radio Card, XR630,  FIPS               */ \
                                {"100-0166-012", IAP_11AC      , IAP_2x2, IAP_BAND_ABG, ANT_INT_OMNI, 1,     1      }, /* Xirrus, PCIE 2x2 Peregrine 2.4/5GHz  QCA9892 Radio Card, XR620,  FIPS               */ \
                                {"100-0175-001", IAP_11AC      , IAP_3x3, IAP_BAND_ABG, ANT_INT_OMNI, 1,     1      }, /* Xirrus, PCIE 3x3 Peregrine 2.4/5GHz  QCA9880 Radio Card, XR630,  SiGe FEM           */ \
                                {"100-0175-002", IAP_11AC      , IAP_2x2, IAP_BAND_ABG, ANT_INT_OMNI, 1,     1      }, /* Xirrus, PCIE 2x2 Peregrine 2.4/5GHz  QCA9882 Radio Card, XR620,  SiGe FEM           */ \
                                {"100-0178-001", IAP_11AC      , IAP_3x3, IAP_BAND_ABG, ANT_INT_DIR , 1,     1      }, /* Xirrus, PCIE 3x3 Peregrine 2.4/5GHz  QCA9880 Radio Card, XD4, Hitachi FEM           */ \
                                {"100-0178-002", IAP_11AC      , IAP_2x2, IAP_BAND_ABG, ANT_INT_DIR , 1,     1      }, /* Xirrus, PCIE 2x2 Peregrine 2.4/5GHz  QCA9882 Radio Card, XD4, Hitachi FEM           */ \
                                {"100-0182-001", IAP_11AC_WAVE2, IAP_4x4, IAP_BAND_ABG, ANT_INT_OMNI, 1,     2      }, /* Xirrus, PCIE 4x4 Broadcom  2.4/5GHz BCM43465 Radio Card, PCIe , Skyworks, LiteOn    */ \
                                {"100-0186-001", IAP_11AC_WAVE2, IAP_4x4, IAP_BAND_ABG, ANT_INT_DIR , 1,     2      }, /* Xirrus, PCIE 4x4 Broadcom  2.4/5GHz BCM43465 Radio Card, Wedge, Skyworks, Benchmark */ \
                                {"100-0190-001", IAP_11AC_WAVE2, IAP_4x4, IAP_BAND_ABG, ANT_INT_OMNI, 1,     2      }, /* Xirrus, PCIE 4x4 Broadcom  2.4/5GHz BCM43465 Radio Card, PCIe , Skyworks, DNI       */ \
                                {"100-0193-003", IAP_11AC      , IAP_3x3, IAP_BAND_ABG, ANT_INT_OMNI, 1,     1      }, /* Xirrus, PCIE 3x3 Broadcom  2.4/5GHz BCM43460 Radio Card, PCIe , Skyworks, LiteOn    */ \
                                {"100-0195-001", IAP_11AC_WAVE2, IAP_3x3, IAP_BAND_A  , ANT_INT_OMNI, 1,     2      }, /* Xirrus, PCIE 3x3 Broadcom      5GHz BCM43525 Radio Card, PCIe , Skyworks, LiteOn    */ \
                                {"100-0196-001", IAP_11AC_WAVE2, IAP_4x4, IAP_BAND_A  , ANT_EXTERNAL, 1,     2      }, /* Xirrus, PCIE 4x4 Broadcom      5GHz BCM43465 Radio Card, PCIe , Skyworks, LiteOn    */ \
                                {"100-0200-001", IAP_11AC      , IAP_3x3, IAP_BAND_ABG, ANT_INT_OMNI, 1,     1      }, /* Xirrus, PCIE 3x3 Broadcom  2.4/5GHz BCM43460 Radio Card, PCIe , Skyworks, LiteOn    */ \
                                {"100-0201-001", IAP_11AC_WAVE2, IAP_3x3, IAP_BAND_A  , ANT_INT_OMNI, 1,     2      }, /* Xirrus, PCIE 3x3 Broadcom      5GHz BCM43525 Radio Card, PCIe , Skyworks, LiteOn    */ \
                                {"425-0001-001", IAP_11N       , IAP_3x3, IAP_BAND_ABG, ANT_INT_DIR , 1,     0      }, /* ODM   , PCIE 3X3 Osprey    2.4/5GHZ   AR9380 Radio Card                             */ \
                                {"425-0002-001", IAP_11N       , IAP_2x2, IAP_BAND_ABG, ANT_INT_DIR , 1,     0      }, /* ODM   , PCIE 2X2 Osprey    2.4/5GHZ   AR9380 Radio Card                             */ \
                                {"425-0003-001", IAP_11N       , IAP_2x2, IAP_BAND_ABG, ANT_INT_OMNI, 0,     0      }, /* ODM   , MPCI 2X2 Merlin    2.4/5GHZ   AR9220 Radio Card, DNI                        */ \
                                {"425-0004-001", IAP_11N       , IAP_2x2, IAP_BAND_A  , ANT_INT_OMNI, 0,     0      }, /* ODM   , MPCI 2X2 Merlin        5GHZ   AR9220 Radio Card, DNI                        */

//****************************************************************
// IAP radio type and decoder table counts
//----------------------------------------------------------------
struct iap_counts
{
    u8 radios;                       // actual number of radios
    u8 entries;                      // entries in iap decoder table
    u8 offset;                       // offset to start of entries in table
} __attribute__ ((packed));

struct iap_counts_info
{
    struct iap_counts a_only;
    struct iap_counts a;
    struct iap_counts abg;
    struct iap_counts all;
    u8 max_sta_per_iap;
} __attribute__ ((packed));

#define SIZE_IAP_COUNTS_INFO sizeof(struct iap_counts_info)

struct iap_decoder_ring
{
    u8   dsp_index;         // for cluster sorting
    char     sw_label[6];
    char     hw_label[6];
    char alt_sw_label[6];
    char alt_hw_label[6];
    char wifi_dev_str[8];
    char wdsl_dev_str[8];
    char iap_type[24];
    char alt_type[24];
    u8   wifi_modes;
    u8   core_num;
    u8   chan_idx;
    u8   sw_index;
    u8   hw_index;
    u8   exists;            // iap slot is available
    u8   present;           // iap slot is populated
    u8   internal;          // iap slot is internal
    u8   bands_supported;
    u8   monitor_capable;
    u8   sgi20_capable;
    u8   ac_wave_2;
    u16  max_rate;
    u64  mcs_mask;
    s16  antenna_angle;
    u8   num_antennas;
    s8   antenna_types[MAX_NUM_ANT];
    u8   antenna_bands[MAX_NUM_ANT];
} __attribute__ ((packed));

#define SIZE_IAP_DECODER_RING sizeof(struct iap_decoder_ring)

//****************************************************************
// Spatial Stream Masks
//----------------------------------------------------------------
#define INDEX_SPATIAL_STREAM_1           0
#define INDEX_SPATIAL_STREAM_2           1
#define INDEX_SPATIAL_STREAM_3           2
#define INDEX_SPATIAL_STREAM_4           3
#define INDEX_SPATIAL_STREAM_5           4
#define INDEX_SPATIAL_STREAM_6           5
#define INDEX_SPATIAL_STREAM_7           6
#define INDEX_SPATIAL_STREAM_8           7

#define SHIFT_SPATIAL_STREAM_1           0
#define SHIFT_SPATIAL_STREAM_2           8
#define SHIFT_SPATIAL_STREAM_3          16
#define SHIFT_SPATIAL_STREAM_4          24
#define SHIFT_SPATIAL_STREAM_5          32
#define SHIFT_SPATIAL_STREAM_6          40
#define SHIFT_SPATIAL_STREAM_7          48
#define SHIFT_SPATIAL_STREAM_8          56

#define MASK_SPATIAL_STREAM_1           (0xffULL << SHIFT_SPATIAL_STREAM_1)
#define MASK_SPATIAL_STREAM_2           (0xffULL << SHIFT_SPATIAL_STREAM_2)
#define MASK_SPATIAL_STREAM_3           (0xffULL << SHIFT_SPATIAL_STREAM_3)
#define MASK_SPATIAL_STREAM_4           (0xffULL << SHIFT_SPATIAL_STREAM_4)
#define MASK_SPATIAL_STREAM_5           (0xffULL << SHIFT_SPATIAL_STREAM_5)
#define MASK_SPATIAL_STREAM_6           (0xffULL << SHIFT_SPATIAL_STREAM_6)
#define MASK_SPATIAL_STREAM_7           (0xffULL << SHIFT_SPATIAL_STREAM_7)
#define MASK_SPATIAL_STREAM_8           (0xffULL << SHIFT_SPATIAL_STREAM_8)

#define MASK_1_SPATIAL_STREAM           (MASK_SPATIAL_STREAM_1                         )
#define MASK_2_SPATIAL_STREAMS          (MASK_SPATIAL_STREAM_2 | MASK_1_SPATIAL_STREAM )
#define MASK_3_SPATIAL_STREAMS          (MASK_SPATIAL_STREAM_3 | MASK_2_SPATIAL_STREAMS)
#define MASK_4_SPATIAL_STREAMS          (MASK_SPATIAL_STREAM_4 | MASK_3_SPATIAL_STREAMS)
#define MASK_5_SPATIAL_STREAMS          (MASK_SPATIAL_STREAM_5 | MASK_4_SPATIAL_STREAMS)
#define MASK_6_SPATIAL_STREAMS          (MASK_SPATIAL_STREAM_6 | MASK_5_SPATIAL_STREAMS)
#define MASK_7_SPATIAL_STREAMS          (MASK_SPATIAL_STREAM_7 | MASK_6_SPATIAL_STREAMS)
#define MASK_8_SPATIAL_STREAMS          (MASK_SPATIAL_STREAM_8 | MASK_7_SPATIAL_STREAMS)

//****************************************************************
// IAP radio h/w numbering definitions
//----------------------------------------------------------------
#define RAD_R0               0          // radio hardware indices
#define RAD_R1               1
#define RAD_R2               2
#define RAD_R3               3
#define RAD_R4               4
#define RAD_R5               5
#define RAD_R6               6
#define RAD_R7               7
#define RAD_R8               8
#define RAD_R9               9
#define RAD_R10             10
#define RAD_R11             11
#define RAD_R12             12
#define RAD_R13             13
#define RAD_R14             14
#define RAD_R15             15

#define IAP_ABG1             0          // iap abg software indices
#define IAP_ABG2             1
#define IAP_ABG3             2
#define IAP_ABG4             3
#define IAP_ABG5             4
#define IAP_ABG6             5
#define IAP_ABG7             6
#define IAP_ABG8             7
#define IAP_ABG9             8
#define IAP_ABG10            9
#define IAP_ABG11           10
#define IAP_ABG12           11
#define IAP_ABG13           12
#define IAP_ABG14           13
#define IAP_ABG15           14
#define IAP_ABG16           15

#define IAP_A1               0          // iap a-only software indices
#define IAP_A2               1
#define IAP_A3               2
#define IAP_A4               3
#define IAP_A5               4
#define IAP_A6               5
#define IAP_A7               6
#define IAP_A8               7
#define IAP_A9               8
#define IAP_A10              9
#define IAP_A11             10
#define IAP_A12             11
#define IAP_A13             12
#define IAP_A14             13
#define IAP_A15             14
#define IAP_A16             15

#define DSP_ABG1             0          // display/cluster abg sorting indices
#define DSP_ABG2             1
#define DSP_ABG3             2
#define DSP_ABG4             3
#define DSP_ABG5             4
#define DSP_ABG6             5
#define DSP_ABG7             6
#define DSP_ABG8             7
#define DSP_ABG9             8
#define DSP_ABG10            9
#define DSP_ABG11           10
#define DSP_ABG12           11
#define DSP_ABG13           12
#define DSP_ABG14           13
#define DSP_ABG15           14
#define DSP_ABG16           15

#define DSP_A1               0          // display/cluster a-only sorting indices
#define DSP_A2               1
#define DSP_A3               2
#define DSP_A4               3
#define DSP_A5               4
#define DSP_A6               5
#define DSP_A7               6
#define DSP_A8               7
#define DSP_A9               8
#define DSP_A10              9
#define DSP_A11             10
#define DSP_A12             11
#define DSP_A13             12
#define DSP_A14             13
#define DSP_A15             14
#define DSP_A16             15

#define MONITOR_STR         "Monitor IAP"

//-------------------------------------------------------------------------------
// IAP configuration counts (radios, entries, offset)
//
//                             a-only           a            abg           all
//                           ----------    ----------    ----------    ----------
#define IAP16_COUNTS        { 0,  0,  0}, {16, 16,  0}, {16, 16,  0}, {16, 16,  0},    // 16-slot xr6000 array (all 2.4/5Ghz)
#define IAP12_COUNTS
#define IAP_8_COUNTS        { 0,  0,  0}, { 8,  8,  0}, { 8,  8,  0}, { 8,  8,  0},    //  8-slot xr4000 array (all 2.4/5Ghz)
#define IAP_4_COUNTS        { 0,  0,  0}, { 4,  4,  0}, { 4,  4,  0}, { 4,  4,  0},    //  4-slot xr2000 array (all 2.4/5Ghz)
#define IAP4X_COUNTS        { 3,  3,  1}, { 4,  4,  0}, { 1,  1,  0}, { 4,  4,  0},    //  4-slot xa4    array (one 2.4, three 5Ghz)
#define IAP_2_COUNTS        { 0,  0,  0}, { 2,  2,  0}, { 2,  2,  0}, { 2,  2,  0},    //  2-slot xr1000 array (all 2.4/5Ghz)
#define IAP_1_COUNTS        { 0,  0,  0}, { 1,  1,  0}, { 1,  1,  0}, { 1,  1,  0},    //  1-slot xr100  array (all 2.4/5Ghz)
#define IAP_XR500_COUNTS    { 1,  1,  1}, { 2,  2,  0}, { 1,  1,  0}, { 2,  2,  0},    //  2-slot xr500  array (one 2.4/5GHz, one 5GHz only)
#define IAP_2x5GHZ_COUNTS   { 2,  2,  0}, { 2,  2,  0}, { 0,  0,  0}, { 2,  2,  0},    //  2-slot xd2    array (two 5GHz only)

//****************************************************************
// IAP configuration definitions
//----------------------------------------------------------------
#define IAP_NONE            {0,         "",      "",    "",     "",      "",       "",       "",              "",              0,              0, 0, 0,         0,       0, 0, 0,            0, 0, 0, 0, 0,   0                     ,   0, 0, {           0,            0}, {           0,           0 } }

#define IAP16_ABG1          {DSP_ABG1 , "iap1" , "r0" , "a12" , "an12" , "wifi8" , "wdsl0" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 2, 0, IAP_ABG1 , RAD_R0 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,   0, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG2          {DSP_ABG2 , "iap2" , "r1" , "a1"  , "an1"  , "wifi0" , "wdsl1" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 0, 0, IAP_ABG2 , RAD_R1 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  23, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG3          {DSP_ABG3 , "iap3" , "r2" , "abg1", "abgn1", "wifi10", "wdsl2" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 4, 0, IAP_ABG3 , RAD_R2 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  45, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG4          {DSP_ABG4 , "iap4" , "r3" , "a2"  , "an2"  , "wifi1" , "wdsl3" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 1, 0, IAP_ABG4 , RAD_R3 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  67, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG5          {DSP_ABG5 , "iap5" , "r4" , "a3"  , "an3"  , "wifi12", "wdsl4" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 0, 0, IAP_ABG5 , RAD_R4 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  90, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG6          {DSP_ABG6 , "iap6" , "r5" , "a4"  , "an4"  , "wifi2" , "wdsl5" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 2, 0, IAP_ABG6 , RAD_R5 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 113, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG7          {DSP_ABG7 , "iap7" , "r6" , "abg2", "abgn2", "wifi14", "wdsl6" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 2, 0, IAP_ABG7 , RAD_R6 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 135, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG8          {DSP_ABG8 , "iap8" , "r7" , "a5"  , "an5"  , "wifi3" , "wdsl7" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 3, 0, IAP_ABG8 , RAD_R7 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 157, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG9          {DSP_ABG9 , "iap9" , "r8" , "a6"  , "an6"  , "wifi9" , "wdsl8" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 3, 0, IAP_ABG9 , RAD_R8 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 180, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG10         {DSP_ABG10, "iap10", "r9" , "a7"  , "an7"  , "wifi4" , "wdsl9" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 4, 0, IAP_ABG10, RAD_R9 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 203, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG11         {DSP_ABG11, "iap11", "r10", "abg3", "abgn3", "wifi11", "wdsl10", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 5, 0, IAP_ABG11, RAD_R10, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 225, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG12         {DSP_ABG12, "iap12", "r11", "a8"  , "an8"  , "wifi5" , "wdsl11", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 5, 0, IAP_ABG12, RAD_R11, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 247, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG13         {DSP_ABG13, "iap13", "r12", "a9"  , "an9"  , "wifi13", "wdsl12", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 1, 0, IAP_ABG13, RAD_R12, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 270, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG14         {DSP_ABG14, "iap14", "r13", "a10" , "an10" , "wifi6" , "wdsl13", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 0, 0, IAP_ABG14, RAD_R13, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 293, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG15         {DSP_ABG15, "iap15", "r14", "abg4", "abgn4", "wifi15", "wdsl14", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 3, 0, IAP_ABG15, RAD_R14, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 315, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16_ABG16         {DSP_ABG16, "iap16", "r15", "a11" , "an11" , "wifi7" , "wdsl15", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 1, 0, IAP_ABG16, RAD_R15, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 337, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }

#define IAP16C_ABG1         {DSP_ABG1 , "iap1" , "r0" , "a12" , "an12" , "wifi8" , "wdsl0" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 2, 0, IAP_ABG1 , RAD_R0 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,   0, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG2         {DSP_ABG2 , "iap2" , "r1" , "a1"  , "an1"  , "wifi1" , "wdsl1" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 0, 0, IAP_ABG2 , RAD_R1 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  23, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG3         {DSP_ABG3 , "iap3" , "r2" , "abg1", "abgn1", "wifi10", "wdsl2" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 4, 0, IAP_ABG3 , RAD_R2 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  45, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG4         {DSP_ABG4 , "iap4" , "r3" , "a2"  , "an2"  , "wifi0" , "wdsl3" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 1, 0, IAP_ABG4 , RAD_R3 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  67, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG5         {DSP_ABG5 , "iap5" , "r4" , "a3"  , "an3"  , "wifi12", "wdsl4" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 0, 0, IAP_ABG5 , RAD_R4 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  90, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG6         {DSP_ABG6 , "iap6" , "r5" , "a4"  , "an4"  , "wifi5" , "wdsl5" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 2, 0, IAP_ABG6 , RAD_R5 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 113, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG7         {DSP_ABG7 , "iap7" , "r6" , "abg2", "abgn2", "wifi14", "wdsl6" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 2, 0, IAP_ABG7 , RAD_R6 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 135, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG8         {DSP_ABG8 , "iap8" , "r7" , "a5"  , "an5"  , "wifi2" , "wdsl7" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 3, 0, IAP_ABG8 , RAD_R7 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 157, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG9         {DSP_ABG9 , "iap9" , "r8" , "a6"  , "an6"  , "wifi9" , "wdsl8" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 3, 0, IAP_ABG9 , RAD_R8 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 180, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG10        {DSP_ABG10, "iap10", "r9" , "a7"  , "an7"  , "wifi6" , "wdsl9" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 4, 0, IAP_ABG10, RAD_R9 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 203, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG11        {DSP_ABG11, "iap11", "r10", "abg3", "abgn3", "wifi11", "wdsl10", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 5, 0, IAP_ABG11, RAD_R10, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 225, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG12        {DSP_ABG12, "iap12", "r11", "a8"  , "an8"  , "wifi3" , "wdsl11", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 5, 0, IAP_ABG12, RAD_R11, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 247, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG13        {DSP_ABG13, "iap13", "r12", "a9"  , "an9"  , "wifi13", "wdsl12", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 1, 0, IAP_ABG13, RAD_R12, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 270, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG14        {DSP_ABG14, "iap14", "r13", "a10" , "an10" , "wifi7" , "wdsl13", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 0, 0, IAP_ABG14, RAD_R13, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 293, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG15        {DSP_ABG15, "iap15", "r14", "abg4", "abgn4", "wifi15", "wdsl14", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 3, 0, IAP_ABG15, RAD_R14, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 315, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP16C_ABG16        {DSP_ABG16, "iap16", "r15", "a11" , "an11" , "wifi4" , "wdsl15", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 1, 0, IAP_ABG16, RAD_R15, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 337, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }

#define IAP_8_ABG1          {DSP_ABG1 , "iap1" , "r0" , "a4"  , "an4"  , "wifi1" , "wdsl0" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 1, 0, IAP_ABG1 , RAD_R0 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,   0, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP_8_ABG2          {DSP_ABG2 , "iap2" , "r2" , "abg1", "abgn1", "wifi3" , "wdsl2" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 3, 0, IAP_ABG2 , RAD_R2 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  45, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP_8_ABG3          {DSP_ABG3 , "iap3" , "r4" , "a1"  , "an1"  , "wifi4" , "wdsl4" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 0, 0, IAP_ABG3 , RAD_R4 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  90, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP_8_ABG4          {DSP_ABG4 , "iap4" , "r6" , "abg2", "abgn2", "wifi2" , "wdsl6" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 2, 0, IAP_ABG4 , RAD_R6 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 135, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP_8_ABG5          {DSP_ABG5 , "iap5" , "r8" , "a2"  , "an2"  , "wifi0" , "wdsl8" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 0, 0, IAP_ABG5 , RAD_R8 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 180, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP_8_ABG6          {DSP_ABG6 , "iap6" , "r10", "abg3", "abgn3", "wifi7" , "wdsl10", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 3, 0, IAP_ABG6 , RAD_R10, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 225, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP_8_ABG7          {DSP_ABG7 , "iap7" , "r12", "a3"  , "an3"  , "wifi5" , "wdsl12", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 1, 0, IAP_ABG7 , RAD_R12, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 270, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP_8_ABG8          {DSP_ABG8 , "iap8" , "r14", "abg4", "abgn4", "wifi6" , "wdsl14", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 2, 0, IAP_ABG8 , RAD_R14, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 315, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }

#define IAP_4_ABG1          {DSP_ABG1 , "iap1" , "r2" , "abg1", "abgn1", "wifi1" , "wdsl2" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 1, 0, IAP_ABG1 , RAD_R2 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  45, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP_4_ABG2          {DSP_ABG2 , "iap2" , "r6" , "abg2", "abgn2", "wifi0" , "wdsl6" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 0, 0, IAP_ABG2 , RAD_R6 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 135, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP_4_ABG3          {DSP_ABG3 , "iap3" , "r10", "abg3", "abgn3", "wifi2" , "wdsl10", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 1, 0, IAP_ABG3 , RAD_R10, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 225, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP_4_ABG4          {DSP_ABG4 , "iap4" , "r14", "abg4", "abgn4", "wifi3" , "wdsl14", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 0, 0, IAP_ABG4 , RAD_R14, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 315, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }

#define IAP_4_A1            {DSP_ABG1 , "iap1" , "r2" , "a1"  , "an1"  , "wifi1" , "wdsl2" , ".11an     2x2", ".11an     3x3", MODE_DOT11AN  , 1, 0, IAP_A1   , RAD_R2 , 1, 1, 1, IAP_BAND_A  , 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  45, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_A  , IAP_BAND_A  } }
#define IAP_4_A2            {DSP_ABG2 , "iap2" , "r6" , "a2"  , "an2"  , "wifi0" , "wdsl6" , ".11an     2x2", ".11an     3x3", MODE_DOT11AN  , 0, 0, IAP_A2   , RAD_R6 , 1, 1, 1, IAP_BAND_A  , 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 135, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_A  , IAP_BAND_A  } }
#define IAP_4_A3            {DSP_ABG3 , "iap3" , "r10", "a3"  , "an3"  , "wifi2" , "wdsl10", ".11an     2x2", ".11an     3x3", MODE_DOT11AN  , 1, 0, IAP_A3   , RAD_R10, 1, 1, 1, IAP_BAND_A  , 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 225, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_A  , IAP_BAND_A  } }
#define IAP_4_A4            {DSP_ABG4 , "iap4" , "r14", "a4"  , "an4"  , "wifi3" , "wdsl14", ".11an     2x2", ".11an     3x3", MODE_DOT11AN  , 0, 0, IAP_A4   , RAD_R14, 1, 1, 1, IAP_BAND_A  , 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 315, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_A  , IAP_BAND_A  } }

#define IAP_2_ABG1          {DSP_ABG1 , "iap1" , "r4" , "abg1", "abgn1", "wifi0" , "wdsl4" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 0, 0, IAP_ABG1 , RAD_R4 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  90, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }
#define IAP_2_ABG2          {DSP_ABG2 , "iap2" , "r12", "abg2", "abgn2", "wifi1" , "wdsl12", ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 1, 0, IAP_ABG2 , RAD_R12, 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS, 270, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }

#define IAP_1_ABG1          {DSP_ABG1 , "iap1" , "r4" , "abg1", "abgn1", "wifi0" , "wdsl4" , ".11abgn   2x2", ".11abgn   3x3", MODE_DOT11ABGN, 0, 0, IAP_ABG1 , RAD_R4 , 1, 1, 1, IAP_BAND_ABG, 1, 1, 0, 300, MASK_2_SPATIAL_STREAMS,  90, 2, {ANT_INT_OMNI, ANT_INT_DIR }, {IAP_BAND_ABG, IAP_BAND_ABG} }

#define IAP12_ALL_RADIOS
#define IAP16C_ALL_RADIOS   IAP16C_ABG1,IAP16C_ABG2,IAP16C_ABG3,IAP16C_ABG4,IAP16C_ABG5,IAP16C_ABG6,IAP16C_ABG7,IAP16C_ABG8,IAP16C_ABG9,IAP16C_ABG10,IAP16C_ABG11,IAP16C_ABG12,IAP16C_ABG13,IAP16C_ABG14,IAP16C_ABG15,IAP16C_ABG16
#define IAP16_ALL_RADIOS    IAP16_ABG1, IAP16_ABG2, IAP16_ABG3, IAP16_ABG4, IAP16_ABG5, IAP16_ABG6, IAP16_ABG7, IAP16_ABG8, IAP16_ABG9, IAP16_ABG10, IAP16_ABG11, IAP16_ABG12, IAP16_ABG13, IAP16_ABG14, IAP16_ABG15, IAP16_ABG16
#define IAP_8_ALL_RADIOS    IAP_8_ABG1, IAP_8_ABG2, IAP_8_ABG3, IAP_8_ABG4, IAP_8_ABG5, IAP_8_ABG6, IAP_8_ABG7, IAP_8_ABG8, IAP_NONE  , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE
#define IAP_4_ALL_RADIOS    IAP_4_ABG1, IAP_4_ABG2, IAP_4_ABG3, IAP_4_ABG4, IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE
#define IAP4X_ALL_RADIOS    IAP_4_ABG1, IAP_4_A2  , IAP_4_A3  , IAP_4_A4  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE
#define IAP_2_ALL_RADIOS    IAP_2_ABG1, IAP_2_ABG2, IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE
#define IAP_1_ALL_RADIOS    IAP_1_ABG1, IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE  , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE   , IAP_NONE

//****************************************************************
// Channel frequency definitions
//----------------------------------------------------------------
#define chan_is_2ghz(n)         ( (n) && (n) <= 14 )
#define chan_is_5ghz(n)         ( (n) && (n) >= 36 )
#define chan_to_freq_2ghz(n)    (((n) <  14 ? 2407 : 2414) + 5*(n))                                     // convert chan to freq, 2.4GHz band
#define chan_to_freq_5ghz(n)    (((n) < 175 ? 5000 : 4000) + 5*(n))                                     // convert chan to freq, 5  GHz band
#define chan_to_freq(n)         ((int)(chan_is_2ghz(n) ? chan_to_freq_2ghz(n) : chan_to_freq_5ghz(n)))  // convert chan to freq.
#define chan_compare(n1, n2)    (chan_to_freq(n1) - chan_to_freq(n2))

//****************************************************************
// Geography channel definitions
//----------------------------------------------------------------
#define US_2GHZ_BAND              1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11
#define ETSI_2GHZ_BAND            1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11, 12, 13
#define JAPAN_2GHZ_BAND           1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11, 12, 13, 14
#define AUNZ_2GHZ_BAND          ETSI_2GHZ_BAND

#define JAPAN_OLD_5GHZ_BAND      34,  38,  42,  46
#define JAPAN_4PT9_GHZ_BAND     184, 188, 192, 196
#define JAPAN_LOW_5GHZ_BAND     208, 212, 216
#define JAPAN_5GHZ_BAND       //JAPAN_4PT9_GHZ_BAND, JAPAN_LOW_5GHZ_BAND // disabled per Steve S. 9/2006

#define LOWER_5GHZ_BAND          36,  40,  44,  48
#define MIDDLE_5GHZ_BAND         52,  56,  60,  64
#define TW_MID_5GHZ_BAND              56,  60,  64
#define UPPER_5GHZ_BAND         149, 153, 157, 161, 165
#define ETSI_5GHZ_BAND          100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140
#define ETSI_5EXT_BAND          100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144  // ETSI extended, adds new channel for 802.11ac
#define ETSI_RSTR_BAND          100, 104, 108, 112, 116,                132, 136, 140       // ETSI restricted, eliminates channels due to new radar regs
#define ETSI_USRX_BAND          100, 104, 108, 112,                          136, 140, 144  // US outdoor restricted, eliminates channels due to weather radar
#define ETSI_USEX_BAND          100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144  // US indoor extended list, adds new channel for 802.11ac (added back 120, 124, 128 now that we passed new DFS rules)
#define KOREAN_5GHZ_BAND        100, 104, 108, 112, 116, 120, 124, 128
#define RUSSIA_ETSI_BAND                                                132, 136, 140, 144
#define RUSSIA_UPPER_BAND       149, 153, 157, 161, 165
#define INDONESIA_5GHZ_BAND     149, 153, 157, 161
#define SAFETY_4PT9_GHZ_BAND    191, 195

// trim autochannel lists such that we only have bondable pairs in the list for 11n
#define LOWER_5GHZ_AUTO         LOWER_5GHZ_BAND
#define MIDDLE_5GHZ_AUTO        MIDDLE_5GHZ_BAND
#define TW_MID_5GHZ_AUTO        TW_MID_5GHZ_BAND
#define UPPER_5GHZ_AUTO         149, 153, 157, 161
#define ETSI_RSTR_AUTO          100, 104, 108, 112,                     132, 136
#define ETSI_USRX_AUTO          100, 104, 108, 112                              , 140, 144
#define ETSI_USEX_AUTO          100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144  // added back 116, 120, 124, 128 now that we passed new DFS rules
#define ETSI_5GHZ_AUTO          100, 104, 108, 112, 116, 120, 124, 128, 132, 136
#define ETSI_5EXT_AUTO          100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144
#define RUSSIA_ETSI_AUTO                                                132, 136, 140, 144
#define RUSSIA_UPPER_AUTO       149, 153, 157, 161

#define ETSI_EURO_BAND          ETSI_5GHZ_BAND
#define ETSI_EURO_AUTO          ETSI_5GHZ_AUTO
#define ETSI_EURO_PWR           ETSI_5GHZ_PWR

#define US_2GHZ_AUTO_3CH        {1, 6, 11, 0}
#define EURO_2GHZ_AUTO_3CH      {1, 6, 11, 0}

#define US_2GHZ_AUTO_4CH        {1, 4,  8, 11, 0}
#define EURO_2GHZ_AUTO_4CH      {1, 5,  9, 13, 0}

#define LCD_2GHZ_AUTO           US_2GHZ_AUTO_3CH,    US_2GHZ_AUTO_3CH,    US_2GHZ_AUTO_4CH
#define US_2GHZ_AUTO            US_2GHZ_AUTO_3CH,    US_2GHZ_AUTO_3CH,    US_2GHZ_AUTO_4CH
#define EURO_2GHZ_AUTO          EURO_2GHZ_AUTO_3CH,  EURO_2GHZ_AUTO_3CH,  EURO_2GHZ_AUTO_4CH
#define JAPAN_2GHZ_AUTO         EURO_2GHZ_AUTO
#define AUNZ_2GHZ_AUTO          EURO_2GHZ_AUTO
#define BRAZIL_2GHZ_AUTO        EURO_2GHZ_AUTO
#define SINGAPORE_2GHZ_AUTO     EURO_2GHZ_AUTO
#define ISRAEL_2GHZ_AUTO        EURO_2GHZ_AUTO
#define MALAY_2GHZ_AUTO         EURO_2GHZ_AUTO
#define KOREAN_2GHZ_AUTO        EURO_2GHZ_AUTO
#define CHINA_2GHZ_AUTO         EURO_2GHZ_AUTO
#define INDONESIA_2GHZ_AUTO     EURO_2GHZ_AUTO
#define RUSSIA_2GHZ_AUTO        EURO_2GHZ_AUTO
#define ARMENIA_2GHZ_AUTO       EURO_2GHZ_AUTO
#define QATAR_2GHZ_AUTO         EURO_2GHZ_AUTO
#define TAIWAN_2GHZ_AUTO        US_2GHZ_AUTO
#define GHANA_2GHZ_AUTO         EURO_2GHZ_AUTO
#define NIGERIA_2GHZ_AUTO       EURO_2GHZ_AUTO

#define LCD_5GHZ_AUTO           {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,                                       0}
#define US_5GHZ_AUTO            {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,                      UPPER_5GHZ_AUTO, 0}
#define USX_5GHZ_AUTO           {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,  ETSI_USEX_AUTO,     UPPER_5GHZ_AUTO, 0}
#define USO_5GHZ_AUTO           {                  MIDDLE_5GHZ_AUTO,  ETSI_USEX_AUTO,     UPPER_5GHZ_AUTO, 0}
#define USR_5GHZ_AUTO           {                  MIDDLE_5GHZ_AUTO,  ETSI_USRX_AUTO,     UPPER_5GHZ_AUTO, 0}
#define USNR_5GHZ_AUTO          {LOWER_5GHZ_AUTO,                                         UPPER_5GHZ_AUTO, 0}
#define EURO_5GHZ_AUTO          {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,  ETSI_EURO_AUTO,                      0}
#define JAPAN_5GHZ_AUTO         {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,  ETSI_5GHZ_AUTO,                      0}
#define AUNZ_5GHZ_AUTO          {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,                      UPPER_5GHZ_AUTO, 0}
#define BRAZIL_5GHZ_AUTO        {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,  ETSI_5EXT_AUTO,     UPPER_5GHZ_AUTO, 0}
#define SINGAPORE_5GHZ_AUTO     {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,  ETSI_5EXT_AUTO,     UPPER_5GHZ_AUTO, 0}
#define ISRAEL_5GHZ_AUTO        {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,                                       0}
#define MALAY_5GHZ_AUTO         {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,                      UPPER_5GHZ_AUTO, 0}
#define KOREAN_5GHZ_AUTO        {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,  KOREAN_5GHZ_BAND,   UPPER_5GHZ_AUTO, 0}
#define CHINA_5GHZ_AUTO         {                                                         UPPER_5GHZ_AUTO, 0}
#define CHINA_5GHZ_AUTO_EXT     {LOWER_5GHZ_AUTO,                                         UPPER_5GHZ_AUTO, 0}
#define INDONESIA_5GHZ_AUTO     {                                                         UPPER_5GHZ_AUTO, 0}
#define RUSSIA_5GHZ_AUTO        {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,  RUSSIA_ETSI_AUTO, RUSSIA_UPPER_AUTO, 0}
#define ARMENIA_5GHZ_AUTO       {LOWER_5GHZ_AUTO,                                                          0}
#define QATAR_5GHZ_AUTO         {                                                         UPPER_5GHZ_AUTO, 0}
#define TAIWAN_5GHZ_AUTO        {                  TW_MID_5GHZ_AUTO,  ETSI_RSTR_AUTO,     UPPER_5GHZ_AUTO, 0}
#define GHANA_5GHZ_AUTO         {LOWER_5GHZ_AUTO,  MIDDLE_5GHZ_AUTO,  ETSI_5GHZ_AUTO,     UPPER_5GHZ_AUTO, 0}
#define NIGERIA_5GHZ_AUTO       {                  MIDDLE_5GHZ_AUTO,  ETSI_5GHZ_AUTO,     UPPER_5GHZ_AUTO, 0}

#define LCD_RADAR               {                                                                          0}
#define US_RADAR                {                  MIDDLE_5GHZ_BAND,                                       0}
#define USX_RADAR               {                  MIDDLE_5GHZ_BAND,  ETSI_USEX_BAND,                      0}
#define USO_RADAR               {                  MIDDLE_5GHZ_BAND,  ETSI_USEX_BAND,                      0}
#define USR_RADAR               {                  MIDDLE_5GHZ_BAND,  ETSI_USRX_BAND,                      0}
#define USNR_RADAR              {                                                                          0}
#define EURO_RADAR              {                  MIDDLE_5GHZ_BAND,  ETSI_5GHZ_BAND,                      0}
#define JAPAN_RADAR             {                  MIDDLE_5GHZ_BAND,  ETSI_5GHZ_BAND,                      0}
#define AUNZ_RADAR              {                  MIDDLE_5GHZ_BAND,  ETSI_RSTR_BAND,                      0}
#define BRAZIL_RADAR            {                  MIDDLE_5GHZ_BAND,  ETSI_5EXT_BAND,                      0}
#define SINGAPORE_RADAR         {                  MIDDLE_5GHZ_BAND,  ETSI_5EXT_BAND,                      0}
#define ISRAEL_RADAR            {                  MIDDLE_5GHZ_BAND,                                       0}
#define MALAY_RADAR             {                  MIDDLE_5GHZ_BAND,                                       0}
#define KOREAN_RADAR            {                  MIDDLE_5GHZ_BAND,  KOREAN_5GHZ_BAND,                    0}
#define RUSSIA_RADAR            {                                                                          0}
#define ARMENIA_RADAR           {                                                                          0}
#define QATAR_RADAR             {                                                                          0}
#define TAIWAN_RADAR            {                  TW_MID_5GHZ_BAND,  ETSI_RSTR_BAND,                      0}
#define GHANA_RADAR             {                  MIDDLE_5GHZ_BAND,  ETSI_5GHZ_BAND,                      0}
#define NIGERIA_RADAR           {                  MIDDLE_5GHZ_BAND,  ETSI_5GHZ_BAND,                      0}

#define LCD_CHANNELS            {US_2GHZ_BAND,                        LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,                                         0}

#define US_CHANNELS             {US_2GHZ_BAND,                        LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,                        UPPER_5GHZ_BAND, 0}
#define USX_CHANNELS            {US_2GHZ_BAND,                        LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,  ETSI_USEX_BAND,       UPPER_5GHZ_BAND, 0}
#define USO_CHANNELS            {US_2GHZ_BAND,                                          MIDDLE_5GHZ_BAND,  ETSI_USEX_BAND,       UPPER_5GHZ_BAND, 0}
#define USR_CHANNELS            {US_2GHZ_BAND,                                          MIDDLE_5GHZ_BAND,  ETSI_USRX_BAND,       UPPER_5GHZ_BAND, 0}
#define USNR_CHANNELS           {US_2GHZ_BAND,                        LOWER_5GHZ_BAND,                                           UPPER_5GHZ_BAND, 0}

#define US_PS_CHANNELS          {US_2GHZ_BAND,  SAFETY_4PT9_GHZ_BAND, LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,                        UPPER_5GHZ_BAND, 0}
#define USX_PS_CHANNELS         {US_2GHZ_BAND,  SAFETY_4PT9_GHZ_BAND, LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,  ETSI_USEX_BAND,       UPPER_5GHZ_BAND, 0}
#define USO_PS_CHANNELS         {US_2GHZ_BAND,  SAFETY_4PT9_GHZ_BAND,                   MIDDLE_5GHZ_BAND,  ETSI_USEX_BAND,       UPPER_5GHZ_BAND, 0}
#define USR_PS_CHANNELS         {US_2GHZ_BAND,  SAFETY_4PT9_GHZ_BAND,                   MIDDLE_5GHZ_BAND,  ETSI_USRX_BAND,       UPPER_5GHZ_BAND, 0}
#define USNR_PS_CHANNELS        {US_2GHZ_BAND,  SAFETY_4PT9_GHZ_BAND, LOWER_5GHZ_BAND,                                           UPPER_5GHZ_BAND, 0}

#define EURO_CHANNELS           {ETSI_2GHZ_BAND,                      LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,  ETSI_EURO_BAND,                        0}
#define AUNZ_CHANNELS           {ETSI_2GHZ_BAND,                      LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,  ETSI_RSTR_BAND,       UPPER_5GHZ_BAND, 0}
#define JAPAN_CHANNELS          {JAPAN_2GHZ_BAND,                     LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,  ETSI_5GHZ_BAND,                        0}
#define BRAZIL_CHANNELS         {ETSI_2GHZ_BAND,                      LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,  ETSI_5EXT_BAND,       UPPER_5GHZ_BAND, 0}
#define SINGAPORE_CHANNELS      {ETSI_2GHZ_BAND,                      LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,  ETSI_5EXT_BAND,       UPPER_5GHZ_BAND, 0}
#define ISRAEL_CHANNELS         {ETSI_2GHZ_BAND,                      LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,                                         0}
#define MALAY_CHANNELS          {ETSI_2GHZ_BAND,                      LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,                        UPPER_5GHZ_BAND, 0}
#define KOREAN_CHANNELS         {ETSI_2GHZ_BAND,                      LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,  KOREAN_5GHZ_BAND,     UPPER_5GHZ_BAND, 0}
#define CHINA_CHANNELS          {ETSI_2GHZ_BAND,                      LOWER_5GHZ_BAND,                                           UPPER_5GHZ_BAND, 0} //
#define INDONESIA_CHANNELS      {ETSI_2GHZ_BAND,                                                                             INDONESIA_5GHZ_BAND, 0}
#define RUSSIA_CHANNELS         {ETSI_2GHZ_BAND,                      LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,  RUSSIA_ETSI_BAND,   RUSSIA_UPPER_BAND, 0}
#define ARMENIA_CHANNELS        {ETSI_2GHZ_BAND,                      LOWER_5GHZ_BAND,                                                            0}
#define QATAR_CHANNELS          {ETSI_2GHZ_BAND,                                                                                 UPPER_5GHZ_BAND, 0}
#define TAIWAN_CHANNELS         {US_2GHZ_BAND,                                          TW_MID_5GHZ_BAND,  ETSI_RSTR_BAND,       UPPER_5GHZ_BAND, 0}
#define GHANA_CHANNELS          {ETSI_2GHZ_BAND,                      LOWER_5GHZ_BAND,  MIDDLE_5GHZ_BAND,  ETSI_5GHZ_BAND,       UPPER_5GHZ_BAND, 0}
#define NIGERIA_CHANNELS        {ETSI_2GHZ_BAND,                                        MIDDLE_5GHZ_BAND,  ETSI_5GHZ_BAND,       UPPER_5GHZ_BAND, 0}

#define CHANNEL_MAP16(abg1, abg2, abg3, abg4, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12)    {abg2, a1, a2, a3, abg1, a4, a5, a6, abg4, a7, a8, a9, abg3, a10, a11, a12}
#define CHANNEL_MAP12(abg1, abg2, abg3, abg4, a1, a2, a3, a4, a5, a6, a7, a8)                       {abg2, a1,     a2, abg1, a3,     a4, abg4, a5,     a6, abg3,  a7,       a8}
#define CHANNEL_MAP_8(abg1, abg2, abg3, abg4, a1, a2, a3, a4)                                       {abg2,     a1,     abg1,     a2,     abg4,     a3,     abg3,       a4,    }
#define CHANNEL_MAP_4(abg1, abg2, abg3, abg4)                                                       {abg2,             abg1,             abg4,             abg3               }
#define CHANNEL_MAP_2(abg1, abg2)                                                                   {abg2,             abg1                                                   }
#define CHANNEL_MAP_1(abg1)                                                                         {                  abg1                                                   }

//----------------------------------------------------------------
// Channel map: 1-11, 36-64
//----------------------------------------------------------------
#define LCD_DEF_16_CHANS        CHANNEL_MAP16(  1,   0,  11,   6,  36,  36,  56,  44,  44,  64,  52,  52,  40,  60,  60,  48)
#define LCD_DEF_12_CHANS        CHANNEL_MAP12(  1,   0,  11,   6,  36,       56,  44,       64,  52,       40,  60,       48)
#define LCD_DEF_8_CHANS         CHANNEL_MAP_8(  1,   0,  11,   6,  36,            44,            52,            60          )
#define LCD_DEF_4_CHANS         CHANNEL_MAP_4( 52,   0,  60,   6)
#define LCD_DEF_2_CHANS         CHANNEL_MAP_2(           60,   6)
#define LCD_DEF_1_CHAN          CHANNEL_MAP_1(           60     )

#define ISRAEL_DEF_16_CHANS     CHANNEL_MAP16(  1,   0,  11,   6,  36,  36,  56,  44,  44,  64,  52,  52,  40,  60,  60,  48)
#define ISRAEL_DEF_12_CHANS     CHANNEL_MAP12(  1,   0,  11,   6,  36,       56,  44,       64,  52,       40,  60,       48)
#define ISRAEL_DEF_8_CHANS      CHANNEL_MAP_8(  1,   0,  11,   6,  36,            44,            52,            60          )
#define ISRAEL_DEF_4_CHANS      CHANNEL_MAP_4( 52,   0,  60,   6)
#define ISRAEL_DEF_2_CHANS      CHANNEL_MAP_2(           60,   6)
#define ISRAEL_DEF_1_CHAN       CHANNEL_MAP_1(           60     )

//----------------------------------------------------------------
// Channel map: 1-11, 36-64, 100-116, 132-140, 149-165
//----------------------------------------------------------------
#define BRAZIL_DEF_16_CHANS     CHANNEL_MAP16(  1,   0,  11,   6,  36, 116, 149,  44, 132, 100,  52, 140, 157,  60, 165, 108)
#define BRAZIL_DEF_12_CHANS     CHANNEL_MAP12(  1,   0,  11,   6,  36,      149,  44,      100,  52,      157,  60,      108)
#define BRAZIL_DEF_8_CHANS      CHANNEL_MAP_8(  1,   0,  11, 149,  36,            44,            52,            60          )
#define BRAZIL_DEF_4_CHANS      CHANNEL_MAP_4( 52,   0,  60,   6)
#define BRAZIL_DEF_2_CHANS      CHANNEL_MAP_2(           60,   6)
#define BRAZIL_DEF_1_CHAN       CHANNEL_MAP_1(           60     )

#define SINGAPORE_DEF_16_CHANS  CHANNEL_MAP16(  1,   0,  11,   6,  36, 116, 149,  44, 132, 100,  52, 140, 157,  60, 165, 108)
#define SINGAPORE_DEF_12_CHANS  CHANNEL_MAP12(  1,   0,  11,   6,  36,      149,  44,      100,  52,      157,  60,      108)
#define SINGAPORE_DEF_8_CHANS   CHANNEL_MAP_8(  1,   0,  11, 149,  36,            44,            52,            60          )
#define SINGAPORE_DEF_4_CHANS   CHANNEL_MAP_4( 52,   0,  60,   6)
#define SINGAPORE_DEF_2_CHANS   CHANNEL_MAP_2(           60,   6)
#define SINGAPORE_DEF_1_CHAN    CHANNEL_MAP_1(           60     )

#define GHANA_DEF_16_CHANS      CHANNEL_MAP16(  1,   0,  11,   6,  36, 116, 149,  44, 132, 100,  52, 140, 157,  60, 165, 108)
#define GHANA_DEF_12_CHANS      CHANNEL_MAP12(  1,   0,  11,   6,  36,      149,  44,      100,  52,      157,  60,      108)
#define GHANA_DEF_8_CHANS       CHANNEL_MAP_8(  1,   0,  11, 149,  36,            44,            52,            60          )
#define GHANA_DEF_4_CHANS       CHANNEL_MAP_4( 52,   0,  60,   6)
#define GHANA_DEF_2_CHANS       CHANNEL_MAP_2(           60,   6)
#define GHANA_DEF_1_CHAN        CHANNEL_MAP_1(           60     )

#define US_DEF_16_CHANS         CHANNEL_MAP16(  1,   0,  11,   6,  36, 116, 149,  44, 132, 100,  52, 140, 157,  60, 165, 108)
#define US_DEF_12_CHANS         CHANNEL_MAP12(  1,   0,  11,   6,  36,      149,  44,      100,  52,      157,  60,      108)
#define US_DEF_8_CHANS          CHANNEL_MAP_8(  1,   0,  11, 149,  36,            44,            52,            60          )
#define US_DEF_4_CHANS          CHANNEL_MAP_4( 52,   0,  60,   6)
#define US_DEF_2_CHANS          CHANNEL_MAP_2(           60,   6)
#define US_DEF_1_CHAN           CHANNEL_MAP_1(           60     )

//----------------------------------------------------------------
// Channel map: 1-11, 52-64, 100-112, 132-136, 149-165
//----------------------------------------------------------------
#define USO_DEF_16_CHANS        CHANNEL_MAP16(  1,   0,  11,   6,  52, 100, 153,  60, 104, 161, 149, 108,  56, 157, 112,  64)
#define USO_DEF_12_CHANS        CHANNEL_MAP12(  1,   0,  11,   6,  52,      153,  60,      161, 149,       56, 157,       64)
#define USO_DEF_8_CHANS         CHANNEL_MAP_8(  1,   0,  11, 100,  52,            60,           149,           157          )
#define USO_DEF_4_CHANS         CHANNEL_MAP_4(149,   0, 157,   6)
#define USO_DEF_2_CHANS         CHANNEL_MAP_2(          157,   6)
#define USO_DEF_1_CHAN          CHANNEL_MAP_1(          157     )

#define USR_DEF_16_CHANS        CHANNEL_MAP16(  1,   0,  11,   6,  52, 100, 153,  60, 104, 161, 149, 108,  56, 157, 112,  64)
#define USR_DEF_12_CHANS        CHANNEL_MAP12(  1,   0,  11,   6,  52,      153,  60,      161, 149,       56, 157,       64)
#define USR_DEF_8_CHANS         CHANNEL_MAP_8(  1,   0,  11, 100,  52,            60,           149,           157          )
#define USR_DEF_4_CHANS         CHANNEL_MAP_4(149,   0, 157,   6)
#define USR_DEF_2_CHANS         CHANNEL_MAP_2(          157,   6)
#define USR_DEF_1_CHAN          CHANNEL_MAP_1(          157     )

#define NIGERIA_DEF_16_CHANS    CHANNEL_MAP16(  1,   0,  11,   6,  52, 100, 153,  60, 104, 161, 149, 108,  56, 157, 112,  64)
#define NIGERIA_DEF_12_CHANS    CHANNEL_MAP12(  1,   0,  11,   6,  52,      153,  60,      161, 149,       56, 157,       64)
#define NIGERIA_DEF_8_CHANS     CHANNEL_MAP_8(  1,   0,  11, 100,  52,            60,           149,           157          )
#define NIGERIA_DEF_4_CHANS     CHANNEL_MAP_4(149,   0, 157,   6)
#define NIGERIA_DEF_2_CHANS     CHANNEL_MAP_2(          157,   6)
#define NIGERIA_DEF_1_CHAN      CHANNEL_MAP_1(          157     )

//----------------------------------------------------------------
// Channel map: 1-11, 36-48, 149-165
//----------------------------------------------------------------
#define USNR_DEF_16_CHANS       CHANNEL_MAP16(  1,   0,  11,   6,  36,  36, 153,  44,  44, 161, 149, 149,  40, 157, 157,  48)
#define USNR_DEF_12_CHANS       CHANNEL_MAP12(  1,   0,  11,   6,  36,      153,  44,      161, 149,       40, 157,       48)
#define USNR_DEF_8_CHANS        CHANNEL_MAP_8(  1,   0,  11, 165,  36,            44,           149,           157          )
#define USNR_DEF_4_CHANS        CHANNEL_MAP_4(149,   0, 157,   6)
#define USNR_DEF_2_CHANS        CHANNEL_MAP_2(          157,   6)
#define USNR_DEF_1_CHAN         CHANNEL_MAP_1(          157     )

//----------------------------------------------------------------
// Channel map: 1-11, 36-64, 100-116, 132-140
//----------------------------------------------------------------
#define EURO_DEF_16_CHANS       CHANNEL_MAP16(  1,   0,  11,   6,  36,  48, 100,  44,  56, 108,  52,  64, 116,  60,  40, 132)
#define EURO_DEF_12_CHANS       CHANNEL_MAP12(  1,   0,  11,   6,  36,      100,  44,      108,  52,      116,  60,      132)
#define EURO_DEF_8_CHANS        CHANNEL_MAP_8(  1,   0,  11, 100,  36,            44,            52,            60          )
#define EURO_DEF_4_CHANS        CHANNEL_MAP_4( 52,   0,  60,   6)
#define EURO_DEF_2_CHANS        CHANNEL_MAP_2(           60,   6)
#define EURO_DEF_1_CHAN         CHANNEL_MAP_1(           60     )

#define JAPAN_DEF_16_CHANS      CHANNEL_MAP16(  1,   0,  11,   6,  36,  48, 100,  44,  56, 108,  52,  64, 116,  60,  40, 132)
#define JAPAN_DEF_12_CHANS      CHANNEL_MAP12(  1,   0,  11,   6,  36,      100,  44,      108,  52,      116,  60,      132)
#define JAPAN_DEF_8_CHANS       CHANNEL_MAP_8(  1,   0,  11, 100,  36,            44,            52,            60          )
#define JAPAN_DEF_4_CHANS       CHANNEL_MAP_4( 52,   0,  60,   6)
#define JAPAN_DEF_2_CHANS       CHANNEL_MAP_2(           60,   6)
#define JAPAN_DEF_1_CHAN        CHANNEL_MAP_1(           60     )

//----------------------------------------------------------------
// Channel map: 1-11, 36-64, 149-165
//----------------------------------------------------------------
#define AUNZ_DEF_16_CHANS       CHANNEL_MAP16(  1,   0,  11,   6,  36,  48, 149,  44,  56, 157,  52,  64, 153,  60,  40, 161)
#define AUNZ_DEF_12_CHANS       CHANNEL_MAP12(  1,   0,  11,   6,  36,      149,  44,      157,  52,      153,  60,      161)
#define AUNZ_DEF_8_CHANS        CHANNEL_MAP_8(  1,   0,  11, 149,  36,            44,            52,            60          )
#define AUNZ_DEF_4_CHANS        CHANNEL_MAP_4( 52,   0,  60,   6)
#define AUNZ_DEF_2_CHANS        CHANNEL_MAP_2(           60,   6)
#define AUNZ_DEF_1_CHAN         CHANNEL_MAP_1(           60     )

#define MALAY_DEF_16_CHANS      CHANNEL_MAP16(  1,   0,  11,   6,  36,  48, 149,  44,  56, 157,  52,  64, 153,  60,  40, 161)
#define MALAY_DEF_12_CHANS      CHANNEL_MAP12(  1,   0,  11,   6,  36,      149,  44,      157,  52,      153,  60,      161)
#define MALAY_DEF_8_CHANS       CHANNEL_MAP_8(  1,   0,  11, 149,  36,            44,            52,            60          )
#define MALAY_DEF_4_CHANS       CHANNEL_MAP_4( 52,   0,  60,   6)
#define MALAY_DEF_2_CHANS       CHANNEL_MAP_2(           60,   6)
#define MALAY_DEF_1_CHAN        CHANNEL_MAP_1(           60     )

//----------------------------------------------------------------
// Channel map: 1-11, 36-64, 100-128, 149-165
//----------------------------------------------------------------
#define KOREAN_DEF_16_CHANS     CHANNEL_MAP16(  1,   0,  11,   6,  36, 116, 149,  44, 124, 100,  52, 128, 157,  60, 165, 108)
#define KOREAN_DEF_12_CHANS     CHANNEL_MAP12(  1,   0,  11,   6,  36,      149,  44,      100,  52,      157,  60,      108)
#define KOREAN_DEF_8_CHANS      CHANNEL_MAP_8(  1,   0,  11, 149,  36,            44,            52,            60          )
#define KOREAN_DEF_4_CHANS      CHANNEL_MAP_4( 52,   0,  60,   6)
#define KOREAN_DEF_2_CHANS      CHANNEL_MAP_2(           60,   6)
#define KOREAN_DEF_1_CHAN       CHANNEL_MAP_1(           60     )

//----------------------------------------------------------------
// Channel map: 1-11, 149-165
//----------------------------------------------------------------
#define CHINA_DEF_16_CHANS      CHANNEL_MAP16(  1,   0,  11,   6, 161, 161, 161, 157, 157, 157, 153, 153, 153, 149, 149, 149)
#define CHINA_DEF_12_CHANS      CHANNEL_MAP12(  1,   0,  11,   6, 161,      161, 157,      157, 153,      153, 149,      149)
#define CHINA_DEF_8_CHANS       CHANNEL_MAP_8(  1,   0,  11, 165, 161,           157,           153,           149          )
#define CHINA_DEF_4_CHANS       CHANNEL_MAP_4(149,   0, 157,   6)
#define CHINA_DEF_2_CHANS       CHANNEL_MAP_2(          157,   6)
#define CHINA_DEF_1_CHAN        CHANNEL_MAP_1(          157     )

#define QATAR_DEF_16_CHANS      CHANNEL_MAP16(  1,   0,  11,   6, 161, 161, 161, 157, 157, 157, 153, 153, 153, 149, 149, 149)
#define QATAR_DEF_12_CHANS      CHANNEL_MAP12(  1,   0,  11,   6, 161,      161, 157,      157, 153,      153, 149,      149)
#define QATAR_DEF_8_CHANS       CHANNEL_MAP_8(  1,   0,  11, 165, 161,           157,           153,           149          )
#define QATAR_DEF_4_CHANS       CHANNEL_MAP_4(149,   0, 157,   6)
#define QATAR_DEF_2_CHANS       CHANNEL_MAP_2(          157,   6)
#define QATAR_DEF_1_CHAN        CHANNEL_MAP_1(          157     )

//----------------------------------------------------------------
// Channel map: 1-11, 149-161
//----------------------------------------------------------------
#define INDONESIA_DEF_16_CHANS  CHANNEL_MAP16(  1,   0,  11,   6, 161, 161, 161, 157, 157, 157, 153, 153, 153, 149, 149, 149)
#define INDONESIA_DEF_12_CHANS  CHANNEL_MAP12(  1,   0,  11,   6, 161,      161, 157,      157, 153,      153, 149,      149)
#define INDONESIA_DEF_8_CHANS   CHANNEL_MAP_8(  1,   0,  11, 165, 161,           157,           153,           149          )
#define INDONESIA_DEF_4_CHANS   CHANNEL_MAP_4(149,   0, 157,   6)
#define INDONESIA_DEF_2_CHANS   CHANNEL_MAP_2(          157,   6)
#define INDONESIA_DEF_1_CHAN    CHANNEL_MAP_1(          157     )

//----------------------------------------------------------------
// Channel map: 1-11, 36-64, 132-140, 149-161
//----------------------------------------------------------------
#define RUSSIA_DEF_16_CHANS     CHANNEL_MAP16(  1,   0,  11,   6,  36,  48, 132,  44,  56, 136,  52,  64, 140,  60,  40, 149)
#define RUSSIA_DEF_12_CHANS     CHANNEL_MAP12(  1,   0,  11,   6,  36,      132,  44,      136,  52,      140,  60,       40)
#define RUSSIA_DEF_8_CHANS      CHANNEL_MAP_8(  1,   0,  11, 132,  36,            44,            52,            60          )
#define RUSSIA_DEF_4_CHANS      CHANNEL_MAP_4( 52,   0,  60,   6)
#define RUSSIA_DEF_2_CHANS      CHANNEL_MAP_2(           60,   6)
#define RUSSIA_DEF_1_CHAN       CHANNEL_MAP_1(           60     )

//----------------------------------------------------------------
// Channel map: 1-11, 36-48
//----------------------------------------------------------------
#define ARMENIA_DEF_16_CHANS    CHANNEL_MAP16(  1,   0,  11,   6,  48,  48,  48,  44,  44,  44,  40,  40,  40,  36,  36,  36)
#define ARMENIA_DEF_12_CHANS    CHANNEL_MAP12(  1,   0,  11,   6,  48,       48,  44,       44,  40,       40,  36,       36)
#define ARMENIA_DEF_8_CHANS     CHANNEL_MAP_8(  1,   0,  11,   6,  48,            44,            40,            36          )
#define ARMENIA_DEF_4_CHANS     CHANNEL_MAP_4( 36,   0,  44,   6)
#define ARMENIA_DEF_2_CHANS     CHANNEL_MAP_2(           44,   6)
#define ARMENIA_DEF_1_CHAN      CHANNEL_MAP_1(           44     )

//----------------------------------------------------------------
// Channel map: 1-11, 56-64, 100-112, 132-136, 149-165
//----------------------------------------------------------------
#define TAIWAN_DEF_16_CHANS     CHANNEL_MAP16(  1,   0,  11,   6,  60, 100, 153,  56, 104, 161, 149, 108,  64, 157, 112, 165)
#define TAIWAN_DEF_12_CHANS     CHANNEL_MAP12(  1,   0,  11,   6,  60,      153,  56,      161, 149,       64, 157,      165)
#define TAIWAN_DEF_8_CHANS      CHANNEL_MAP_8(  1,   0,  11, 100,  60,            56,           149,           157          )
#define TAIWAN_DEF_4_CHANS      CHANNEL_MAP_4(149,   0, 157,   6)
#define TAIWAN_DEF_2_CHANS      CHANNEL_MAP_2(          157,   6)
#define TAIWAN_DEF_1_CHAN       CHANNEL_MAP_1(          157     )

//****************************************************************
// Geography power definitions
//----------------------------------------------------------------
#define US_2GHZ_PWR              20,  20,  20,  20,  20,  20,  20,  20,  20,  20,  20
#define ETSI_2GHZ_PWR            20,  20,  20,  20,  20,  20,  20,  20,  20,  20,  20, 20, 20
#define JAPAN_2GHZ_PWR           20,  20,  20,  20,  20,  20,  20,  20,  20,  20,  20, 20, 20, 20

#define LOWER_5GHZ_US_PWR        17,  17,  17,  17
#define LOWER_5GHZ_ETSI_PWR      23,  23,  23,  23
#define LOWER_5GHZ_JAPAN_PWR     22,  22,  22,  22
#define MIDDLE_5GHZ_PWR          23,  23,  23,  23
#define TW_MID_5GHZ_PWR          23,  23,  23
#define MIDDLE_5GHZ_JAPAN_PWR    22,  22,  22,  22
#define UPPER_5GHZ_PWR           29,  29,  29,  29,  29
#define KOREAN_5GHZ_PWR          23,  23,  23,  23,  23,  23,  23,  23
#define RUSSIA_5GHZ_PWR          23,  23,  23,  23
#define RUSSIA_UPPER_PWR         29,  29,  29,  29,  29
#define INDONESIA_5GHZ_PWR       29,  29,  29,  29
#define ETSI_5GHZ_USEX_PWR       23,  23,  23,  23,  23,  23,  23,  23,  23,  23,  23,  23
#define ETSI_5GHZ_RSTR_PWR       23,  23,  23,  23,  23,  23,  23,  23
#define ETSI_5GHZ_USRX_PWR       23,  23,  23,  23,  23,  23,  23
#define ETSI_RSTR_PWR            30,  30,  30,  30,  30,  30,  30,  30
#define ETSI_5GHZ_PWR            30,  30,  30,  30,  30,  30,  30,  30,  30,  30,  30
#define ETSI_5EXT_PWR            30,  30,  30,  30,  30,  30,  30,  30,  30,  30,  30,  30
#define ETSI_5GHZ_JAPAN_PWR      22,  22,  22,  22,  22,  22,  22,  22,  22,  22,  22
#define SAFETY_4PT9_GHZ_PWR      30,  30

#define LCD_PWR                {US_2GHZ_PWR,                         LOWER_5GHZ_US_PWR,    MIDDLE_5GHZ_PWR,                                                0}

#define US_PWR                 {US_2GHZ_PWR,                         LOWER_5GHZ_US_PWR,    MIDDLE_5GHZ_PWR,                                UPPER_5GHZ_PWR, 0}
#define USX_PWR                {US_2GHZ_PWR,                         LOWER_5GHZ_US_PWR,    MIDDLE_5GHZ_PWR,       ETSI_5GHZ_USEX_PWR,      UPPER_5GHZ_PWR, 0}
#define USO_PWR                {US_2GHZ_PWR,                                               MIDDLE_5GHZ_PWR,       ETSI_5GHZ_USEX_PWR,      UPPER_5GHZ_PWR, 0}
#define USR_PWR                {US_2GHZ_PWR,                                               MIDDLE_5GHZ_PWR,       ETSI_5GHZ_USRX_PWR,      UPPER_5GHZ_PWR, 0}
#define USNR_PWR               {US_2GHZ_PWR,                         LOWER_5GHZ_US_PWR,                                                    UPPER_5GHZ_PWR, 0}

#define US_PS_PWR              {US_2GHZ_PWR,    SAFETY_4PT9_GHZ_PWR, LOWER_5GHZ_US_PWR,    MIDDLE_5GHZ_PWR,                                UPPER_5GHZ_PWR, 0}
#define USX_PS_PWR             {US_2GHZ_PWR,    SAFETY_4PT9_GHZ_PWR, LOWER_5GHZ_US_PWR,    MIDDLE_5GHZ_PWR,       ETSI_5GHZ_USEX_PWR,      UPPER_5GHZ_PWR, 0}
#define USO_PS_PWR             {US_2GHZ_PWR,    SAFETY_4PT9_GHZ_PWR,                       MIDDLE_5GHZ_PWR,       ETSI_5GHZ_USEX_PWR,      UPPER_5GHZ_PWR, 0}
#define USR_PS_PWR             {US_2GHZ_PWR,    SAFETY_4PT9_GHZ_PWR,                       MIDDLE_5GHZ_PWR,       ETSI_5GHZ_USRX_PWR,      UPPER_5GHZ_PWR, 0}
#define USNR_PS_PWR            {US_2GHZ_PWR,    SAFETY_4PT9_GHZ_PWR, LOWER_5GHZ_US_PWR,                                                    UPPER_5GHZ_PWR, 0}

#define EURO_PWR               {ETSI_2GHZ_PWR,                       LOWER_5GHZ_ETSI_PWR,  MIDDLE_5GHZ_PWR,       ETSI_EURO_PWR,                           0}
#define JAPAN_PWR              {JAPAN_2GHZ_PWR,                      LOWER_5GHZ_JAPAN_PWR, MIDDLE_5GHZ_JAPAN_PWR, ETSI_5GHZ_JAPAN_PWR,                     0}
#define AUNZ_PWR               {ETSI_2GHZ_PWR,                       LOWER_5GHZ_US_PWR,    MIDDLE_5GHZ_PWR,       ETSI_RSTR_PWR,           UPPER_5GHZ_PWR, 0}
#define BRAZIL_PWR             {ETSI_2GHZ_PWR,                       LOWER_5GHZ_ETSI_PWR,  MIDDLE_5GHZ_PWR,       ETSI_5EXT_PWR,           UPPER_5GHZ_PWR, 0}
#define SINGAPORE_PWR          {ETSI_2GHZ_PWR,                       LOWER_5GHZ_ETSI_PWR,  MIDDLE_5GHZ_PWR,       ETSI_5EXT_PWR,           UPPER_5GHZ_PWR, 0}
#define ISRAEL_PWR             {ETSI_2GHZ_PWR,                       LOWER_5GHZ_ETSI_PWR,  MIDDLE_5GHZ_PWR,                                                0}
#define MALAY_PWR              {ETSI_2GHZ_PWR,                       LOWER_5GHZ_ETSI_PWR,  MIDDLE_5GHZ_PWR,                                UPPER_5GHZ_PWR, 0}
#define KOREAN_PWR             {ETSI_2GHZ_PWR,                       LOWER_5GHZ_ETSI_PWR,  MIDDLE_5GHZ_PWR,       KOREAN_5GHZ_PWR,         UPPER_5GHZ_PWR, 0}
#define CHINA_PWR              {ETSI_2GHZ_PWR,                                                                                             UPPER_5GHZ_PWR, 0}
#define INDONESIA_PWR          {ETSI_2GHZ_PWR,                                                                                         INDONESIA_5GHZ_PWR, 0}
#define RUSSIA_PWR             {ETSI_2GHZ_PWR,                       LOWER_5GHZ_ETSI_PWR,  MIDDLE_5GHZ_PWR,       RUSSIA_5GHZ_PWR,       RUSSIA_UPPER_PWR, 0}
#define ARMENIA_PWR            {ETSI_2GHZ_PWR,                       LOWER_5GHZ_ETSI_PWR,                                                                  0}
#define QATAR_PWR              {ETSI_2GHZ_PWR,                                                                                             UPPER_5GHZ_PWR, 0}
#define TAIWAN_PWR             {US_2GHZ_PWR,                                               TW_MID_5GHZ_PWR,       ETSI_5GHZ_RSTR_PWR,      UPPER_5GHZ_PWR, 0}
#define GHANA_PWR              {ETSI_2GHZ_PWR,                       LOWER_5GHZ_ETSI_PWR,  MIDDLE_5GHZ_PWR,       ETSI_5GHZ_PWR,           UPPER_5GHZ_PWR, 0}
#define NIGERIA_PWR            {ETSI_2GHZ_PWR,                                             MIDDLE_5GHZ_PWR,       ETSI_5GHZ_PWR,           UPPER_5GHZ_PWR, 0}

//****************************************************************
// Global Regulatory Class Definitions (from 802.11-2012 Annex E)
//----------------------------------------------------------------
#define   ETSI_2GHZ_REG_CLASS        81
#define  LOWER_5GHZ_REG_CLASS       115
#define MIDDLE_5GHZ_REG_CLASS       118
#define   ETSI_5GHZ_REG_CLASS       121
#define  UPPER_5GHZ_REG_CLASS       125

#define REGULATORY_CLASS(chan)     ((  1 <= (chan) && (chan) <=  13) ?   ETSI_2GHZ_REG_CLASS :  \
                                    ( 36 <= (chan) && (chan) <=  48) ?  LOWER_5GHZ_REG_CLASS :  \
                                    ( 52 <= (chan) && (chan) <=  64) ? MIDDLE_5GHZ_REG_CLASS :  \
                                    (100 <= (chan) && (chan) <= 144) ?   ETSI_5GHZ_REG_CLASS :  \
                                    (149 <= (chan) && (chan) <= 165) ?  UPPER_5GHZ_REG_CLASS : 0)

//****************************************************************
// Cellsize
//----------------------------------------------------------------
#define CELL_MANUAL                     0
#define CELL_SMALL                      1
#define CELL_MEDIUM                     2
#define CELL_LARGE                      3
#define CELL_MAX                        4
#define CELL_AUTO                       5
#define CELL_MONITOR                    6

#define CELL_MONITOR_TX_POWER          20
#define CELL_MAX_TX_POWER              20
#define CELL_LARGE_TX_POWER            19
#define CELL_MEDIUM_TX_POWER           12
#define CELL_SMALL_TX_POWER             5
#define CELL_MIN_TX_POWER             -10

#define CELL_MONITOR_RX_THRESHOLD     -95
#define CELL_MAX_RX_THRESHOLD         -90
#define CELL_LARGE_RX_THRESHOLD       -89
#define CELL_MEDIUM_RX_THRESHOLD      -86
#define CELL_SMALL_RX_THRESHOLD       -82
#define CELL_MIN_RX_THRESHOLD         -50

#define CELL_OLD_LARGE_RX_THRESHOLD   -87
#define CELL_OLD_MEDIUM_RX_THRESHOLD  -81
#define CELL_OLD_SMALL_RX_THRESHOLD   -75

#define CELL_DEFAULT                   CELL_MAX
#define CELL_DEFAULT_TX_POWER          CELL_MAX_TX_POWER
#define CELL_DEFAULT_RX_THRESHOLD      CELL_MAX_RX_THRESHOLD

#define AUTOCELL_MAX_TX_POWER          20
#define AUTOCELL_DFT_TX_POWER          10
#define AUTOCELL_MIN_TX_POWER           1
#define AUTOCELL_DFT_RX_THRESHOLD     -80
#define AUTOCELL_MIN_BIAS             -90
#define AUTOCELL_MAX_BIAS             -60
#define AUTOCELL_DFT_OVERLAP           50
#define AUTOCELL_TXRX_RATIO           1/2
#define AUTOCELL_TXRX_OFFSET         (AUTOCELL_MIN_BIAS + AUTOCELL_MAX_TX_POWER * AUTOCELL_TXRX_RATIO)

//****************************************************************
// 802.11ac Definitions
//----------------------------------------------------------------
#define MCS0                            0x01
#define MCS1                            0x02
#define MCS2                            0x04
#define MCS3                            0x08
#define MCS4                            0x10
#define MCS5                            0x20
#define MCS6                            0x40
#define MCS7                            0x80

#define MAX_MCS7                        0
#define MAX_MCS8                        1
#define MAX_MCS9                        2
#define MAX_MCS_UNSUPPORTED             3

#define BOND_MODE_40MHZ( pri_chan)     (((pri_chan)       &  4) ? 1 : -1)
#define BOND_MODE_80MHZ( pri_chan)    ((((pri_chan) +  4) &  8) ? 1 : -1)
#define BOND_MODE_160MHZ(pri_chan)    ((((pri_chan) + 12) & 16) ? 1 : -1)

#define BOND_CHAN_40MHZ( pri_chan)      ((pri_chan) + 4* BOND_MODE_40MHZ(pri_chan))
#define BOND_CHAN_80MHZ( pri_chan)      ((pri_chan) - 4*(BOND_MODE_40MHZ(pri_chan) < 0) + 8* BOND_MODE_80MHZ(pri_chan))
#define BOND_CHAN_160MHZ(pri_chan)      ((pri_chan) - 4*(BOND_MODE_40MHZ(pri_chan) < 0) - 8*(BOND_MODE_80MHZ(pri_chan) < 0) + 16*BOND_MODE_160MHZ(pri_chan))

//****************************************************************
// Helper macros
//----------------------------------------------------------------
#define right_band(rng, rad)        (((rng) != RADIO_11A || (rad)->band == BAND_DOT11A) && \
                                     ((rng) != RADIO_11G || (rad)->band == BAND_DOT11G))

#define radio_match(rng, iap, rad)  ((iap)->present && ((iap)->bands_supported != IAP_BAND_ABG ||  right_band(rng, rad)))

#define is_cell_size(size, rad)     ((rad)->txpwr == size ## _TX_POWER && (rad)->rxthresh == size ## _RX_THRESHOLD)

#define skip_radio(rng, iap, rad)   !radio_match(rng, iap, rad)
#define wrong_band(rng, rad)        !right_band(rng, rad)

// loop_on_matching_radios
//
// helper macro to loop over a set of radios (used to process return data from OOPME to cfg or cfg to cli
//
// parameters
// i        : index starting at 0 of radios checked (use to index into return data)
// iface    : iface to match (RADIO_ALL, RADIO_11A, RADIO_11G, or a specific iap number
// iap_list : pointer to iap_decoder_ring structure for each radio matched (use iap_list->sw_index to get iap index)
// radio    : pointer to radio table information (if not available as a global)
//
// assumes the existance of globals iap_cnts & iap_info
//
// executes the code following the macro for each iap that matches the iface passed.
// for a specific iface the code is executed once with i=0 & iap_list pointing to the specific iap
// for iface=RADIO_ALL the code is executed up to MAX_NUM_RADIOS times with i incrementing from 0 & iap_list pointing to each iap
// for iface=RADIO_11A the code is executed up to MAX_NUM_RADIOS times, but only for those radios operating in a mode.
//   i is incremented for each radio checked and iap_list points to each iap in a mode when the code executes.
// for iface=RADIO_11G the code is eceuted up to MAX_NUM_AGB_RADIOS times, but only for those radios operating in bg mode.
//   i is incremented for each radio checked and iap_list points to each iap in bg mode when the code executes.
//
// note: see the definition of RADIO_ALL, RADIO_11A, RADIO_11G in xircfg.h to understand this silliness.
//
// new fangled version that always loops over the abg radios first ... go ahead, try to figure it out ... good luck ...

#define loop_on_matching_radios(i, iface, iap_list, radio_list)                             \
    int _cnt;                                                                               \
    int _delta;                                                                             \
    iap_list = &iap_info[iap_cnts.abg.offset];                                              \
    switch (iface) {                                                                        \
        case RADIO_ALL: _cnt = iap_cnts.all.entries; i = iap_cnts.abg.offset; break;        \
        case RADIO_11A: _cnt = iap_cnts.a.entries  ; i = iap_cnts.abg.offset; break;        \
        case RADIO_11G: _cnt = iap_cnts.abg.entries; i = 0;                   break;        \
        default:        _cnt = 1;                    i = 0;                                 \
                        iap_list = &iap_info[iface];                          break;        \
    }                                                                                       \
    if (radio_list == NULL) radio_list = &radio[iap_list->sw_index];                        \
    else                    radio_list += i;                                                \
    for ( ; _cnt--; _delta = (i == iap_cnts.all.entries-1) ? 1-iap_cnts.all.entries : 1,    \
                    i += _delta, iap_list += _delta, radio_list += _delta)                  \
        if (radio_match(iface, iap_list, radio_list))


// this version initializes the radio_list pointer before looping (handy in the cli)
//
#define loop_on_matching_radios_init(i, iface, iap_list, radio_list, radio)                 \
    radio_list = radio;                                                                     \
    loop_on_matching_radios(i, iface, iap_list, radio_list)

#endif //_XIRIAP_H
