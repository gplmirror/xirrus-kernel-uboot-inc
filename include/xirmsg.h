//******************************************************************************
/** @file xirmsg.h
 *
 * config module message definitions
 *
 * <I>Copyright (C) 2005 Xirrus. All Rights Reserved</I>
 *
 **/
//------------------------------------------------------------------------------
#ifndef _XIRMSG_H
#define _XIRMSG_H

#include "common.h"
#include "xiriap.h"

//****************************************************************
// Config module message definitions
//   gather up all internal debug syslogs and return to caller
//----------------------------------------------------------------
#define cfg_msg_rst()   memset(cfg_msg_buff, (cfg_msg_len = 0), sizeof(cfg_msg_buff))

#define set_cfg_msg(fmt, args...)      ({                                                                                                           \
    if (cfg_msg_len < sizeof(cfg_msg_buff)-1) cfg_msg_len += snprintf(cfg_msg_buff+cfg_msg_len, sizeof(cfg_msg_buff)-1-cfg_msg_len, fmt, ##args);   \
    if (cfg_msg_len > sizeof(cfg_msg_buff)-1) cfg_msg_len  =                                    sizeof(cfg_msg_buff)-1                          ;   \
    if (cfg_msg_len < sizeof(cfg_msg_buff)-1) cfg_msg_len += snprintf(cfg_msg_buff+cfg_msg_len, sizeof(cfg_msg_buff)-1-cfg_msg_len, "\n"       );   \
    if (cfg_msg_len > sizeof(cfg_msg_buff)-1) cfg_msg_len  =                                    sizeof(cfg_msg_buff)-1                          ;   \
})

#define cfg_msg_load(req)       ({                                                                                                                  \
    if ((req) && (req)->intvalue >= 0 && ((req)->mod == MOD_CLI || (req)->mod == MOD_WMI || (req)->mod == MOD_CLOUD))                               \
        strxncpy((req)->strvalue, (cfg_msg_len < MAX_STR-1) ? cfg_msg_buff : "more", MAX_STR);                                                      \
})

#define cfg_syslog(req, level, fmt, args...)    ({                                                                                                  \
    if (    !(req) || (req)->mod != MOD_BOOT) {                                                                                                     \
        if (!(req) || (req)->mod == MOD_INTERNAL || (level) == LOG_DEBUG)  set_cfg_msg(fmt, ##args);                                                \
        syslog(level, fmt, ##args);                                                                                                                 \
    }                                                                                                                                               \
})

#define cfg_msg_syslog(level, fmt, args...)     ({                                                                                                  \
    set_cfg_msg(  fmt, ##args);                                                                                                                     \
    syslog(level, fmt, ##args);                                                                                                                     \
})

#define cli_syslog(req, level, fmt, args...)    ({                                                                                                  \
    if ((req) && (req)->mod == MOD_CLI) cfg_syslog(req, level, fmt, ##args);                                                                        \
    else                                    syslog(     level, fmt, ##args);                                                                        \
})

#define cfg_syslog_obj(req, level, obj)    ({                                                                                                       \
    char buff[1000] = "";                                                                                                                           \
    if (create_obj_syslog(obj, buff) != 0)                                                                                                          \
        cfg_syslog(req, level, buff);                                                                                                               \
})

#define cli_syslog_obj(req, level, obj)    ({                                                                                                       \
    char buff[1000] = "";                                                                                                                           \
    if (create_obj_syslog(obj, buff) != 0)                                                                                                          \
        cli_syslog(req, level, buff);                                                                                                               \
})

#define SIZE_CFG_MSG_BUFF       10000



//      Message                              Message
//      Identifier                           Explanation
//      ------------------------------       -----------------------------------------------------------------------------
//
#define MSG_DHCP_POOL_DISABLED               "Note: New DHCP Pool is currently disabled. Enable after configuration. "
#define MSG_DHCP_WPR_OVERLAP_EDIT            "Note: DHCP pool NAT subnet overlaps another DHCP Pool or VLAN subnet. Correct before enabling pools. "

#define MSG_IAP_DUP_OVERLAP_CHAN_CHG         "Note: IAP %s channel changed to avoid duplicate or overlapping setting. "
#define MSG_IAP_DUP_OVERLAP_CHAN_DIS         "Note: IAP %s disabled to avoid duplicate or overlapping channel setting. "
#define MSG_IAP_ABG_ANGLE_CHAN_DIS           "Note: IAP %s disabled to avoid closer than 90 deg spacing for 2.4GHz IAPs (including the RF monitor). "

#define MSG_IAP_GLB_XRP_LAYER_2_ONLY         "Note: Fast roaming broadcast mode only works at layer 2. "
#define MSG_IAP_GLB_XRP_MODE_TUNNEL          "Note: Fast roaming layer 2-and-3 requires tunneling. "
#define MSG_IAP_GLB_PUB_SAFETY_BAND          "Note: The 4.9 GHz Public Safety Band requires a license and may be used only by qualified public safety operators. "
#define MSG_IAP_GLB_WAVE_2_CHAIN_LIMIT       "Note: Power constraints allow only 3 TX chains when operating all 4 radios in the 5GHz band.  Setting TX chains to 3. "
#define MSG_IAP_GLB_CONVERT_ISOLATE_GRP_CLR  "Note: Multicast isolation by user group requires multicast to unicast conversion.  Clearing multicast isolation by user group. "
#define MSG_IAP_GLB_ISOLATE_GRP_CONVERT_SET  "Note: Multicast isolation by user group requires multicast to unicast conversion.  Enabling multicast to unicast conversion. "
#define MSG_IAP_GLB_ISOLATE_GRP_EXCLUDE_WARN "Note: Multicast addresses excluded from unicast conversion will not be isolated by user group. " "\n" \
                                             "      Delete any address from the multicast exclude list that is to be isolated by user group. "

#define MSG_IAP_AUTOCHAN_BAD_LIST            "Note: Invalid channel (%d) ignored. "
#define MSG_IAP_AUTOCHAN_DUP_LIST            "Note: Overlapping channel (%d) ignored. "
#define MSG_IAP_INT_DIR_ANTENNA              "Note: IAP %s internal directional antenna is 2.4GHz only. "
#define MSG_IAP_MONITOR_NO_CELLSIZE          "Note: Cannot change cellsize "       "on " MONITOR_STR " while RF monitor is enabled. "
#define MSG_IAP_MONITOR_NO_ANTENNA           "Note: Cannot change antenna "        "on " MONITOR_STR " while RF monitor is enabled. "
#define MSG_IAP_MONITOR_NO_RX_THRESHOLD      "Note: Cannot change RX threshold "   "on " MONITOR_STR " while RF monitor is enabled. "
#define MSG_IAP_MONITOR_NO_TX_POWER          "Note: Cannot change TX power "       "on " MONITOR_STR " while RF monitor is enabled. "
#define MSG_IAP_MONITOR_NO_CHANNEL_BOND      "Note: Cannot change channel bonding ""on " MONITOR_STR " while RF monitor is enabled. "
#define MSG_IAP_MONITOR_NO_CHANNEL_MODE      "Note: Cannot change channel lock "   "on " MONITOR_STR " while RF monitor is enabled. "
#define MSG_IAP_MONITOR_NO_WIFI_MODE         "Note: Cannot change WiFi mode "      "on " MONITOR_STR " while RF monitor is enabled. "
#define MSG_IAP_MONITOR_NO_TIMESHARE         "Note: Timeshare monitor mode is not available on this model."

#define MSG_IAP_MONITOR_AUTOBLOCK_CHK        ""                                                                                                                      "\n" \
                                             "CAUTION: Selecting and engaging Auto Block may result in many APs being blocked.  User caution in configuring and "    "\n" \
                                             "operating any form of Auto Block is highly recommended, as auto-blocking may be subject to significant statutory and " "\n" \
                                             "U.S. Federal Communications Commission (FCC) regulatory controls, restrictions, enforcement actions and penalties. "   "\n" \
                                             ""                                                                                                                      "\n" \
                                             "User is solely responsible for making sure that all uses of any auto-blocking feature(s) of this product are fully "   "\n" \
                                             "compliant with all applicable statutes, regulations, FCC enforcement actions & rules, etc. regarding Wi-Fi blocking. " "\n" \
                                             "See for example FCC Enforcement Advisory No. 2015-01 dated January 27, 2015.  "                                        "\n" \
                                             ""                                                                                                                      "\n" \
                                             "All uses of any auto-blocking feature(s) in this product are solely at User's discretion and individual choice. "      "\n" \
                                             "User assumes all liability and responsibility for all such uses.  %s assumes no liability or responsibility "          "\n" \
                                             "for any discretionary decision by User to configure, engage and to use any auto-blocking feature(s) of this product. " "\n" \
                                             ""                                                                                                                      "\n"

#define MSG_IAP_EXEC_DUP_CHAN                "Interface IAP %s state remaining down. Duplicate or overlapping channel assigned (%d) "
#define MSG_IAP_EXEC_RADAR_CHAN              "Interface IAP %s state remaining down. Radar detected on channel assigned (%d)  "
#define MSG_IAP_EXEC_ANGLE_CHAN              "Interface IAP %s state remaining down. 2.4GHz IAPs (including the RF monitor) must be spaced at least 90 deg apart. "

#define MSG_IAP_MIMO_REQ_TXBF                "Note: Multi-User-MIMO requires Tx-Beam-Forming. "
#define MSG_IAP_11R_ENABLE_11K               "Note: 802.11k support is recommended when using 802.11r, and will be enabled for your convenience. "

#define MSG_IDS_RF_MONITOR_ENB               "Note: RF monitor is disabled. Enable RF monitor for full intrusion detection support. "

#define MSG_LOCATION_NO_EXT_ANTENNA          "Note: Access Points with external antennas will not particpate in distributed location calculations. "

#define MSG_RF_MONITOR_ENB_STANDBY           "Note: Standby mode "                      "requires the RF monitor to be enabled. Enabling RF monitor. "
#define MSG_RF_MONITOR_ENB_AUTOCELL          "Note: Non-channel specific autocell mode ""requires the RF monitor to be enabled. Enabling RF monitor. "
#define MSG_RF_MONITOR_ENB_ASSURANCE         "Note: Radio assurance "                   "requires the RF monitor to be enabled. Enabling RF monitor. "

#define MSG_RF_MONITOR_DED_STANDBY           "Note: Standby mode "                      "requires the RF monitor to be enabled in dedicated mode. Enabling dedicated mode. "
#define MSG_RF_MONITOR_DED_ASSURANCE         "Note: Radio assurance "                   "requires the RF monitor to be enabled in dedicated mode. Enabling dedicated mode. "

#define MSG_RF_MONITOR_DIS_STANDBY           "Note: Standby mode "                      "requires the RF monitor to be enabled. Disabling Standby mode. "
#define MSG_RF_MONITOR_DIS_AUTOCELL          "Note: Non-channel specific autocell mode ""requires the RF monitor to be enabled. Disabling Autocell mode. "
#define MSG_RF_MONITOR_DIS_ASSURANCE         "Note: Radio assurance "                   "requires the RF monitor to be enabled. Disabling Radio assurance. "

#define MSG_NET_ASSURANCE_ENB_FALLBACK       "Note: Fallback requires Network Assurance to be enabled. Enabling Network Assurance. "
#define MSG_NET_ASSURANCE_DIS_FALLBACK       "Note: Fallback requires Network Assurance to be enabled. Disabling Fallback on %s %s. "

#define MSG_LLDP_ENB_FA                      "Note: Fabric Attach requires LLDP to be enabled. Enabling LLDP."
#define MSG_VLAN_ENB_FA                      "Note: VLAN Fabric Attach requires global Fabric Attach to be enabled. Enabling global Fabric Attach."

#define MSG_SSID_DHCP_WPR_OFF                "Note: Web Page Redirect requires an enabled DHCP Pool. "
#define MSG_SSID_DHCP_POOL_WPR_OFF           "Note: Web Page Redirect requires DHCP Pool to be enabled. SSID %s is using this DHCP Pool. "
#define MSG_SSID_DISABLED                    "Note: New SSID is created disabled. Enable after configuration. "
#define MSG_SSID_WPR_ENB_BAD_SUBNET          "Note: DHCP Pool subnet is not unique. "    "Web Page Redirect and/or NAT requires a unique subnet. "
#define MSG_SSID_WPR_ENB_BAD_VLAN            "Note: SSID VLAN number is not unique. "    "Web Page Redirect requires a unique SSID VLAN number. "
#define MSG_SSID_WPR_ENB_BAD_NATIVE          "Note: SSID VLAN is the native VLAN. "      "Web Page Redirect requires a non-native SSID VLAN number. "
#define MSG_SSID_WPR_ENB_DHCP_POOL_OFF       "Note: DHCP Pool for SSID is disabled. "    "Web Page Redirect requires a DHCP Pool to be enabled. "
#define MSG_SSID_WPR_ENB_DHCP_SHARED         "Note: DHCP Pool for SSID is shared.   "    "Web Page Redirect requires a unique DHCP Pool. "
#define MSG_SSID_WPR_ENB_NO_DHCP_POOL        "Note: No DHCP Pool selected for SSID. "    "Web Page Redirect requires a DHCP Pool. "
#define MSG_SSID_WPR_ENB_NO_DHCP_NAT         "Note: DHCP Pool NAT for SSID is disabled. ""Web Page Redirect requires NAT to be enabled. "
#define MSG_SSID_WPR_ENB_NO_DHCP_DNS         "Note: DHCP Pool DNS for SSID is not set. " "Web Page Redirect requires a valid DNS to be set. "
#define MSG_SSID_WPR_ENB_DHCP_DNS_FALLBACK   "Note: DHCP Pool DNS for SSID is not set. " "Web Page Redirect DNS will default to the global DNS. "
#define MSG_SSID_WPR_ENB_DHCP_DEFAULTS       "Note: DHCP Pool range and gateway set to defaults for Web Page Redirect. "

#define MSG_RADIUS_ADD_BAD_SSID_ENC          "Note: SSID %s security settings currently don't support RADIUS. Correct for proper operation. "

#define MSG_VLAN_WRITE_DFLT_RTE_NO_ADDR      "Note: VLAN DHCP IP address not yet set. Default route will be set when DHCP completes. "
#define MSG_VLAN_WRITE_DFLT_RTE_NO_GW        "Note: VLAN DHCP gateway not yet set. Default route will be set when DHCP completes. "
#define MSG_VLAN_WRITE_DFLT_RTE_NO_MGMT      "Note: VLAN Management not set. Setting default route VLAN management to enabled. "
#define MSG_VLAN_WRITE_NATIVE_NO_WPR         "Note: VLAN is assigned to an SSID using Web Page Redirect. Web Page Redirect requires a non-native SSID VLAN number. "
#define MSG_VLAN_WRITE_NATIVE_IP_CONFLICT    "Note: IP address conflict between gig1/2 and VLAN %s (%d).  Must change either gig1/2 IP address or VLAN IP address. "

#define MSG_GROUP_DHCP_WPR_OFF               "Note: Web Page Redirect requires an enabled DHCP Pool. "
#define MSG_GROUP_DHCP_POOL_WPR_OFF          "Note: Web Page Redirect requires DHCP Pool to be enabled. User Group %s is using this DHCP Pool. "
#define MSG_GROUP_DISABLED                   "Note: New User Group is created disabled. Enable after configuration. "
#define MSG_GROUP_WPR_ENB_BAD_SUBNET         "Note: DHCP Pool subnet is not unique. "          "Web Page Redirect and/or NAT requires a unique subnet. "
#define MSG_GROUP_WPR_ENB_BAD_VLAN           "Note: User Group VLAN number is not unique. "    "Web Page Redirect requires a unique User Group VLAN number. "
#define MSG_GROUP_WPR_ENB_BAD_NATIVE         "Note: User Group VLAN is the native VLAN. "      "Web Page Redirect requires a non-native User Group VLAN number. "
#define MSG_GROUP_WPR_ENB_DHCP_POOL_OFF      "Note: DHCP Pool for User Group is disabled. "    "Web Page Redirect requires a DHCP Pool to be enabled. "
#define MSG_GROUP_WPR_ENB_DHCP_SHARED        "Note: DHCP Pool for User Group is shared.   "    "Web Page Redirect requires a unique DHCP Pool. "
#define MSG_GROUP_WPR_ENB_NO_DHCP_POOL       "Note: No DHCP Pool selected for User Group. "    "Web Page Redirect requires a DHCP Pool. "
#define MSG_GROUP_WPR_ENB_NO_DHCP_NAT        "Note: DHCP Pool NAT for User Group is disabled. ""Web Page Redirect requires NAT to be enabled. "
#define MSG_GROUP_WPR_ENB_NO_DHCP_DNS        "Note: DHCP Pool DNS for User Group is not set. " "Web Page Redirect requires a valid DNS to be set. "
#define MSG_GROUP_WPR_ENB_DHCP_DNS_FALLBACK  "Note: DHCP Pool DNS for User Group is not set. " "Web Page Redirect DNS will default to the global DNS. "
#define MSG_GROUP_WPR_ENB_DHCP_DEFAULTS      "Note: DHCP Pool range and gateway set to defaults for Web Page Redirect. "

#define MSG_ETH_WRITE_BAD_ADDR               "Note: Current IP address is invalid. IP mask will be set when a valid IP address is entered. "
#define MSG_ETH_WRITE_BAD_MASK               "Note: Current IP mask is invalid. IP address will be set when a valid IP mask is entered. "
#define MSG_ETH_WRITE_ETH0_GATEWAY_IGNORED   "Note: Interface gig1/2 gateway will be used as default route. Delete gig1/2 gateway to use eth0 gateway as default route. "
#define MSG_ETH_WRITE_ETH0_GATEWAY_USED      "Note: Interface eth0 gateway will be used as default route. "

#define MSG_FILTER_SET_FORCES_ALLOW          "Note: Setting VLAN, QoS, DSCP, IP, or traffic limits require a filter to pass traffic. Filter type changed to allow from default of deny. "
#define MSG_FILTER_MUST_BE_LAYER_2           "Note: Source or destination MAC address, or ARP protocol, requires a layer 2 filter. Filter layer changed to 2. "
#define MSG_FILTER_DPI_MUST_BE_ENABLED       "Note: Application filters require that Application Control be enabled. Use 'application-control' command to enable. "

#define MSG_WDS_SSIDS_MUST_MATCH             "Note: SSID must be defined and have identical authentication and encryption settings on the target system. "
#define MSG_ADMIN_BAD_RADIUS_CFG             "Note: Admin RADIUS server no longer fully configured. Disabling admin RADIUS authentication. "

#define MSG_MULTICAST_BAD_LIST               "Note: Invalid multicast address (%s) ignored. "
#define MSG_MULTICAST_BAD_VLAN               "Note: Undefined VLAN (%d) ignored. "

#define MSG_DNS_WRITE_WMI_HOSTNAME_CHG       "Note: Hostname changed. Web interface must be reset before SSL certificate will be updated.\n\n"      \
                                             "Either reboot the system (System Tools menu), or close all web sessions and use the "                  \
                                             "CLI to stop and restart the web server (config, management, https off/on)."

#define MSG_INTERFACE_DISABLED               "Note: New %s is created disabled. Enable after configuration. "

#define MSG_RECOVERY_IMAGE_UNAVAILABLE       "Note: The recovery image selected (%s) is currently not available on this network.\n"   \
                                             "The image must be running on a system on this network for this unit to boot correctly."

#define WARN_CLUSTER_CONNECT                 "Warning: Cluster connection to "    "%s failed. "
#define WARN_CLUSTER_LOGIN                   "Warning: Cluster login to "         "%s failed. "
#define WARN_CLUSTER_SEND                    "Warning: Cluster send to "          "%s failed. "
#define WARN_CLUSTER_RECEIVE                 "Warning: Cluster receive from "     "%s failed. "
#define WARN_CLUSTER_MISMATCH                "Warning: Cluster version check on " "%s failed. "

#endif //_XIRMSG_H
