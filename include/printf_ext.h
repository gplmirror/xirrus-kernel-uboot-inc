/* printf_ext.h
 *
 * Extends glibc printf() format strings with new conversion characters
 *
 * P - string, center by padding with spaces on both sides (specify width)
 *
 * M - mac address, provide pointer to 6-byte buffer
 *     default is lower-case, use ' for upper case
 *     width accepted, - accepted, use # to center
 */

#ifndef PRINTF_EXT_H
#define PRINTF_EXT_H

// call this before using new conversion characters
int init_printf_ext(void);

#endif
