//******************************************************************************
/** @file xirsupp.h
 *
 * Structures and constants for the wpa_supplicant request interface
 *
 * <I>Copyright (C) 2006 Xirrus. All Rights Reserved</I>
 *
 * This file contains constants and structures that are used between the
 * configuration module and the wpa_supplicant client
 *
 *
 **/
//------------------------------------------------------------------------------
#ifndef _XIRSUPP_H
#define _XIRSUPP_H

#include "xtypes.h"

//****************************************************************
// File pipe names
//----------------------------------------------------------------
#define XWS_PIPE_EVENT                 HOMEDIR"/xws-event"
#define XWS_PIPE_READ                  HOMEDIR"/xws-read"
#define XWS_PIPE_WRITE                 HOMEDIR"/xws-write"

//****************************************************************
// Commands
//----------------------------------------------------------------
#define XWS_CMD_READ                    0   // read an OID
#define XWS_CMD_WRITE                   1   // write an OID
#define XWS_CMD_EXECUTE                 2   // perform some function

//****************************************************************
// Status
//----------------------------------------------------------------
#define XWS_STAT_SUCCESS                0
#define XWS_STAT_NOT_FOUND              1
#define XWS_STAT_MAC_ERROR              2

//****************************************************************
// Obj IDs
//----------------------------------------------------------------
//#define XWS_OID_AUTH                    0   // Authentication type
//#define XWS_OID_BSSID                   1   // get/set BSSID
//#define XWS_OID_SSID                    2   // get/set SSID

//****************************************************************
// Execute IDs
//----------------------------------------------------------------
#define XWS_EID_DISASSOCIATE            0
#define XWS_EID_DEAUTHENTICATE          1
#define XWS_EID_SETKEY                  2
#define XWS_EID_TKIP_COUNTERMEASURES    3
#define XWS_EID_DROP_UNENCRYPTED        4

//****************************************************************
// XWS_OID_AUTH
//----------------------------------------------------------------
#define XWS_AUTH_WPA_VERSION            0
#define XWS_AUTH_CIPHER_PAIRWISE1       1
#define XWS_AUTH_CIPHER_GROUP           2
#define XWS_AUTH_KEY_MGMT               3
#define XWS_AUTH_TKIP_COUNTERMEASURES   4
#define XWS_AUTH_DROP_UNENCRYPTED       5
#define XWS_AUTH_80211_AUTH_ALG         6
#define XWS_AUTH_WPA_ENABLED            7
#define XWS_AUTH_RX_UNENCRYPTED_EAPOL   8
#define XWS_AUTH_ROAMING_CONTROL        9
#define XWS_AUTH_PRIVACY_INVOKED        10

//****************************************************************
// Event IDs
//----------------------------------------------------------------
#define XWS_EVT_ASSOC                   1   // Client has associated
#define XWS_EVT_DISASSOC                2   // Received disassoc packet
#define XWS_EVT_DEAUTH                  3   // Received deauth packet

//****************************************************************
// Encryption algorithms - these must match IW_ENCODEs in wpa_supp
//----------------------------------------------------------------
#define XWS_SETKEY_ALG_NONE             0
#define XWS_SETKEY_ALG_WEP              1
#define XWS_SETKEY_ALG_TKIP             2
#define XWS_SETKEY_ALG_CCMP             3
//****************************************************************
// Maximums
//----------------------------------------------------------------
#define XWS_MAX_SIZE_EVT                1024
#define XWS_MAX_KEY                     32
#define XWS_MAX_SSID                    32

#define XWS_LEN_BSSID                   6

//****************************************************************
// Structure definitions
//----------------------------------------------------------------
struct xws_hdr
{
    int id        ; // ID of the object or event under study
    int iface     ; // Xirrus interface (IAP)
    int ssid      ; // SSID mac num
    int len       ; // Length of the data
    int kv_logs   ; // flag whether to use key/value pair syslogs
    union
    {
       int cmd    ; // Incoming to ME
       int status ; // Outgoing from ME
    }             ;

} __attribute__ ((packed));

struct xws_eid_setkey
{
   int alg;
   int group;              // is this a group key (1) or pairwise (0)
   int key_id;
   int key_len;
   u8  key[XWS_MAX_KEY];
};

struct xws_evt_assoc
{
   u8    bssid[XWS_LEN_BSSID];
   int   ssid_len;
   char  ssid[XWS_MAX_SSID];
   u8    ie_req_len;
   u8    ie_resp_len;
   // Variable data follows in the form
   u8    ie_req[ 0 ];
   // u8  ie_resp[ ie_resp_len ];
};

#endif // _XIRSUPP_H

