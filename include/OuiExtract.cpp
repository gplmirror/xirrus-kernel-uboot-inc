#include <stdio.h>
#include <ctype.h>
#include <string.h>


const char *ignore_tokens[] = {
    "a",
    "ab",
    "ag",
    "and",
    "aps",
    "as",
    "asa",
    "automation",
    "b",
    "bcs",
    "beijing",
    "bhd",
    "broadband",
    "bv",
    "bvba",
    "cc",
    "chs",
    "cm",
    "co",
    "cokg",
    "coltd",
    "com",
    "comm",
    "communication",
    "communications",
    "company",
    "computer",
    "computers",
    "control",
    "controls",
    "coop",
    "corp",
    "corporate",
    "corporation",
    "cpe",
    "danmark",
    "data",
    "dd",
    "de",
    "design",
    "device",
    "devices",
    "digital",
    "div",
    "ecs",
    "elec",
    "electric",
    "electro",
    "electro-mechanics",
    "electronic",
    "electronics",
    "elektronik",
    "emv",
    "engineering",
    "equipment",
    "europe",
    "ev",
    "fit",
    "gmbh",
    "gmbhco",
    "gr",
    "group",
    "hf",
    "hk",
    "hn",
    "id",
    "inc",
    "incorporated",
    "ind",
    "indco",
    "indcoltd",
    "industrial",
    "industries",
    "information",
    "inst",
    "int",
    "international",
    "intl",
    "kft",
    "kg",
    "kk",
    "ku",
    "l",
    "lab",
    "lda",
    "limited",
    "llc",
    "lp",
    "ltd",
    "ltda",
    "ltdco",
    "manufacturing",
    "manufactuaring",
    "mbh",
    "mechanics",
    "mfg",
    "mfgco",
    "mfgcorp",
    "microsystems",
    "mobile",
    "mobility",
    "multimedia",
    "n",
    "network",
    "networks",
    "nts",
    "nv",
    "of",
    "ohg",
    "owned",
    "oy",
    "p",
    "plc",
    "precision",
    "products",
    "pt",
    "pte",
    "pteltd",
    "pty",
    "pvt",
    "research",
    "s",
    "sa",
    "sbu",
    "sdn",
    "se",
    "semiconductor",
    "sg",
    "shenzhen",
    "sl",
    "solutions",
    "spa",
    "srl",
    "sro",
    "subsidiary",
    "sys",
    "system",
    "systems",
    "tech",
    "technologies",
    "technologieswholly",
    "technology",
    "telecom",
    "thailand",
    "the",
    "tokyo",
    "uk",
    "usa",
    "v",
    "wireless",
};

char *_strlwr(char *pString)
{
   char *p = pString;

   while (*p)
   {
      *p = tolower(*p);
      p++;
   }
   return pString;
}


int getline(char *s, int n)
{
        int c, i = 0;

        while ((c = getchar()) != EOF && c != '\n' && i++ < n-1)
                *s++ = (char)c;
        *s = '\0';
        return ((c == EOF) ? -1 : i);
}

void strip_special_chars(char *s)
{
        char *t = s;

        while (*s) {
                if (('a' <= *s && *s <= 'z') ||
                        ('A' <= *s && *s <= 'Z') ||
                        ('0' <= *s && *s <= '9') || *s == ' ' || *s == '-')
                        *t++ = *s++;
                else {
                        if (*s != '.' && *s != '&')
                                *t++ = ' ';
                        ++s;
                }
        }
        *t = '\0';
}

int lookup_token(char *s)
{
        unsigned i;
        int cnt = 0;

        for (i = 0; i < sizeof(ignore_tokens)/sizeof(char *); i++) {
                if (strcasecmp(s, ignore_tokens[i]) == 0)
                        ++cnt;
        }
        return (cnt);
}


char *cleanup_token(char *token)
{
        char *sub_tokens[5];
        char *s;
        int n = 0;
        int i;

        sub_tokens[n++] = token;
        for (s = token; *s; s++) {
                if (*s == '-') {
                        *s = '\0';
                        sub_tokens[n++] = ++s;
                }
        }
        for (i = 0; i < n; i++) {
                if (strlen(sub_tokens[i]) > 4) {
                        _strlwr(sub_tokens[i]);
                        sub_tokens[i][0] = toupper(sub_tokens[i][0]);
                }
        }
        for (i = 1; i < n; i++) {
                *--sub_tokens[i] = '-';
        }
        return(token);
}

int main(int argc, char *argv[])
{
        static char tokens[10][50];
        char buffer[100], temp[10];
        int  mac_addr[3];
        int  n, i, j, cnt = 0;

        while (getline(buffer, sizeof(buffer)) >= 0) {
                if (sscanf(buffer, "%x-%x-%x (%s", &mac_addr[0], &mac_addr[1], &mac_addr[2], temp) == 4 && strcmp(temp, "hex)") == 0) {
                        printf("    { {0x%02x, 0x%02x, 0x%02x},  \"", mac_addr[0], mac_addr[1], mac_addr[2]);
                        strip_special_chars(&buffer[18]);
                        n = sscanf(&buffer[18], "%s %s %s %s %s %s %s %s %s %s",
                                tokens[0], tokens[1], tokens[2], tokens[3], tokens[4],
                                tokens[5], tokens[6], tokens[7], tokens[8], tokens[9]);
                        // Special case for Honeywell (sigh)
                        if (strstr(buffer, "Hand Held Products Inc")) {
                            printf("%s", "Honeywell Hand Held");
                        }
                        else {
                            for (i = j = 0; i < n; i++) {
                                    if (strlen(tokens[i]) > 1 && lookup_token(tokens[i]) == 0) {
                                            if (j++)
                                                    printf(" ");
                                            printf("%s", cleanup_token(tokens[i]));
                                    }
                            }
                            if (!j) {
                                    for (i = j = 0; i < n; i++) {
                                            if (strlen(tokens[i]) > 3) {
                                                    if (j++)
                                                            printf(" ");
                                                    printf("%s", cleanup_token(tokens[i]));
                                            }
                                    }
                            }
                        }
                        printf("\" },\n");
                        cnt++;
                }
        }
}

