#ifndef _XIRLLDP_H
#define _XIRLLDP_H

#include "xirmac.h"
#include "xirstring.h"

#define LLDP_FA_TYPE_STRS               { "", "", "FA Server", "FA Proxy", "FA Server No Auth", "FA Proxy No Auth", "FA Client WAP", "" }

#define MAX_NUM_LLDP_ENTRIES            50

#define LLDP_CFG_REQ_FA_CFG             0
#define LLDP_CFG_REQ_IP_ADDR            1
#define LLDP_CFG_REQ_HOSTNAME           2

#define LLDP_FA_ELEMENT_DISCOVERED      1
#define LLDP_FA_ELEMENT_AGED_OUT        2
#define LLDP_FA_MGMT_VLAN_MISMATCH      3

#define REQ_LEN_LLDP_STR                (50 + 1)
#define REQ_LEN_LLDP_IFACE_STR          (15 + 1)
#define REQ_LEN_LLDP_IP_ADDR_STR        (15 + 1)
#define REQ_LEN_LLDP_MAC_ADDR_STR       (17 + 1)
#define REQ_LEN_LLDP_FA_KEY_STR         (32 + 1)
#define REQ_LEN_LLDP_SW_STR             (100 + 1)
#define REQ_LEN_LLDP_HOSTNAME_STR       (256 + 1)

struct lldp_info
{
    char hostname    [REQ_LEN_LLDP_STR];
    char interface   [REQ_LEN_LLDP_IFACE_STR];
    char ip_address  [REQ_LEN_LLDP_IP_ADDR_STR];
    char mac_address [REQ_LEN_LLDP_MAC_ADDR_STR];
    char model       [REQ_LEN_LLDP_STR];
    char capabilities[REQ_LEN_LLDP_STR];
    char software    [REQ_LEN_LLDP_SW_STR];
    u_int16_t vlan;
    u_int8_t  fa_type;
    u_int32_t last_update;
} __attribute__ ((packed));

#define SIZE_LLDP_INFO  sizeof(struct lldp_info)

struct lldp_trap
{
    char interface  [REQ_LEN_LLDP_IFACE_STR];
    char ip_address [REQ_LEN_LLDP_IP_ADDR_STR];
    char mac_address[REQ_LEN_LLDP_MAC_ADDR_STR];
    u_int8_t  fa_type;
    u_int16_t mgmt_vlan;
    u_int16_t dflt_vlan;
} __attribute__ ((packed));

#define SIZE_LLDP_TRAP  sizeof(struct lldp_trap)

struct lldp_fa_cfg
{
    u_int8_t  fa;
    char      fa_key[REQ_LEN_LLDP_FA_KEY_STR];
    u_int8_t  eth_aggr;
    u_int16_t mgmt_vlan;
    u_int16_t num_vlans;
    u_int16_t vlan[MAX_NUM_VLAN];
    u_int16_t pwr_req;
    u_int8_t  num_radios_down;
    char      hostname[REQ_LEN_LLDP_HOSTNAME_STR];
    char      sys_descr[REQ_LEN_LLDP_SW_STR];
    char      sys_oid[REQ_LEN_LLDP_STR];
} __attribute__ ((packed));

#define SIZE_LLDP_FA_CFG  sizeof(struct lldp_fa_cfg)

#endif // _XIRLLDP_H
