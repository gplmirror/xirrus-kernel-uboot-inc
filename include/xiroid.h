//******************************************************************************
/** @file xiroid.h
 *
 * Xirrus Object Identifiers
 *
 * <I>Copyright (C) 2005 Xirrus. All Rights Reserved</I>
 *
 * @author  Eric K. Henderson
 * @date    11/6/2005
 *
 * Raw Xirrus OIDs
 *
 * An OID is a numerical value used to index an operation or object.
 * An OBJ (object) is a configuration property or entity.
 **/
//------------------------------------------------------------------------------
#ifndef _XIROID_H
#define _XIROID_H
#include "xirver.h"

//****************************************************************
// Helper Macros
//
// OID_IS_OBJ()   returns true if _x_ is in range of OIDs
// OID_OBJ()      returns the normalized OID of _x_
// IDX_OBJ()      returns the object index of _x_
// OFFSET_OBJ()   returns the offset of the object with index _x_
//----------------------------------------------------------------
#define OID_IS_OBJ( _x_, _obj_, _start_ )   ((_start_) <= (_x_) && (_x_) <= ((_start_) + ((NUM_##_obj_##_OIDS) * (NUM_##_obj_##_OBJS)) - 1))
#define OID_OBJ(    _x_, _obj_, _start_ )   ((_start_) + ((_x_) - (_start_)) % (NUM_##_obj_##_OIDS))
#define IDX_OBJ(    _x_, _obj_, _start_ )   (((_x_) - (_start_)) / (NUM_##_obj_##_OIDS))
#define OFFSET_OBJ( _x_, _obj_, _start_ )   ((_x_) * (NUM_##_obj_##_OIDS))

#define OBJ_ENCRYPT_FLAG                    0x8000  // or in to OID to rc4 encrypt the object on storage

//****************************************************************
// Configuration object identifiers
//
// These are used with read/write and add/delete commands
//----------------------------------------------------------------
#define OID_ACL_LIST                        1       // Access Control List

//---------------------------------------------------------
// Admin users [32x3] : (2-4) - (95-97)
//---------------------------------------------------------
#define NUM_ADMIN_OIDS                      3
#define NUM_ADMIN_OBJS                      32

#define OFFSET_ADMIN( _x_ )                 OFFSET_OBJ( _x_, ADMIN, OID_ADMIN_USER(0))
#define OID_IS_ADMIN( _x_ )                 OID_IS_OBJ( _x_, ADMIN, OID_ADMIN_USER(0))
#define OID_ADMIN(    _x_ )                 OID_OBJ(    _x_, ADMIN, OID_ADMIN_USER(0))
#define IDX_ADMIN(    _x_ )                 IDX_OBJ(    _x_, ADMIN, OID_ADMIN_USER(0))

#define OID_ADMIN_USER( _idx_ )             (2 + OFFSET_ADMIN(_idx_))
#define OID_ADMIN_PASS( _idx_ )             (3 + OFFSET_ADMIN(_idx_))
#define OID_ADMIN_PRIV( _idx_ )             (4 + OFFSET_ADMIN(_idx_))

//#define OID_ADMIN_NUM                     98      // Deprecated

//---------------------------------------------------------
// Description
//---------------------------------------------------------
#define OID_DESC_LOCATION                   99      // Location name
#define OID_SNMP_GROUP_NAME                 100     // Group name (snmp only)
#define OID_MGMT_FIPS_ENABLED               101     // FIPS switch

//---------------------------------------------------------
// Beacon
//---------------------------------------------------------
#define OID_IAP_GLB_BROADCAST_OPTIMIZE      102     // enable / disable broadcast rate optimization

//---------------------------------------------------------
// Contact info
//---------------------------------------------------------
#define OID_CONTACT_EMAIL                   103
#define OID_CONTACT_NAME                    104
#define OID_CONTACT_PHONE                   105

//---------------------------------------------------------
// DHCP
//---------------------------------------------------------
#define NUM_DHCP_OIDS                       1
#define NUM_DHCP_OBJS                       5

#define OID_IS_DHCP( _x_ )                  OID_IS_OBJ( _x_, DHCP, OID_DHCP_LEASE_DEFAULT )
#define IDX_DHCP(    _x_ )                  IDX_OBJ(    _x_, DHCP, OID_DHCP_LEASE_DEFAULT )

#define OID_DHCP_LEASE_DEFAULT              106
#define OID_DHCP_ENABLED                    107
#define OID_DHCP_LEASE_MAX                  108
#define OID_DHCP_RANGE_END                  109
#define OID_DHCP_RANGE_START                110

//---------------------------------------------------------
// DNS (see also OID_DNS_USE_DHCP)
//---------------------------------------------------------
#define NUM_DNS_OIDS                        1
#define NUM_DNS_OBJS                        5

#define OID_IS_DNS( _x_ )                   OID_IS_OBJ( _x_, DNS, OID_DNS_DOMAIN)
#define IDX_DNS(    _x_ )                   IDX_OBJ(    _x_, DNS, OID_DNS_DOMAIN)

#define OID_DNS_DOMAIN                      111
#define OID_DNS_HOSTNAME                    112
#define OID_DNS_SERVER1                     113
#define OID_DNS_SERVER2                     114
#define OID_DNS_SERVER3                     115

//---------------------------------------------------------
// Original Ethernet objects [3x10]: (116-125) - (136-145)
//---------------------------------------------------------
#define NUM_ORG_ETH_OIDS                    10
#define NUM_ORG_ETH_OBJS                    3

#define OFFSET_ORG_ETH( _x_ )               OFFSET_OBJ( _x_, ORG_ETH, OID_ORG_ETH_ALLOW_MGMT(0))
#define OID_IS_ORG_ETH( _x_ )               OID_IS_OBJ( _x_, ORG_ETH, OID_ORG_ETH_ALLOW_MGMT(0))
#define OID_ORG_ETH(    _x_ )               OID_OBJ(    _x_, ORG_ETH, OID_ORG_ETH_ALLOW_MGMT(0))
#define IDX_ORG_ETH(    _x_ )               IDX_OBJ(    _x_, ORG_ETH, OID_ORG_ETH_ALLOW_MGMT(0))

#define OID_ORG_ETH_ALLOW_MGMT(  _idx_)     (116 + OFFSET_ORG_ETH(_idx_))
#define OID_ORG_ETH_AUTONEG(     _idx_)     (117 + OFFSET_ORG_ETH(_idx_))
#define OID_ORG_ETH_IPV4_DHCP(   _idx_)     (118 + OFFSET_ORG_ETH(_idx_))
#define OID_ORG_ETH_DUPLEX(      _idx_)     (119 + OFFSET_ORG_ETH(_idx_))
#define OID_ORG_ETH_ENABLED(     _idx_)     (120 + OFFSET_ORG_ETH(_idx_))
#define OID_ORG_ETH_IPV4_GATEWAY(_idx_)     (121 + OFFSET_ORG_ETH(_idx_))
#define OID_ORG_ETH_IPV4_ADDR(   _idx_)     (122 + OFFSET_ORG_ETH(_idx_))
#define OID_ORG_ETH_IPV4_MASK(   _idx_)     (123 + OFFSET_ORG_ETH(_idx_))
#define OID_ORG_ETH_MTU(         _idx_)     (124 + OFFSET_ORG_ETH(_idx_))
#define OID_ORG_ETH_SPEED(       _idx_)     (125 + OFFSET_ORG_ETH(_idx_))

//---------------------------------------------------------
// Serial port console
//---------------------------------------------------------
#define NUM_CON_OIDS                        1
#define NUM_CON_OBJS                        5

#define OID_IS_CONSOLE( _x_ )               OID_IS_OBJ( _x_, CON, OID_CON_BAUD_RATE)
#define IDX_CONSOLE(    _x_ )               IDX_OBJ(    _x_, CON, OID_CON_BAUD_RATE)

#define OID_CON_BAUD_RATE                   146
#define OID_CON_PARITY                      147
#define OID_CON_STOP_BITS                   148
#define OID_CON_TIMEOUT                     149
#define OID_CON_WORD_SIZE                   150

//---------------------------------------------------------
// Network Time Protocol
//---------------------------------------------------------
#define OID_NTP_ENABLED                     151     // Network Time Protocol Enabled
#define OID_NTP_SERVER1                     152
#define OID_NTP_SERVER2                     153

//---------------------------------------------------------
// IAP properties [16x20]: (154-173) - (454-473)
//---------------------------------------------------------
#define NUM_IAP_OIDS                        20
#define NUM_IAP_OBJS                        16

#define OFFSET_IAP( _x_ )                   OFFSET_OBJ( _x_, IAP, OID_IAP_START(0) )
#define OID_IS_IAP( _x_ )                   OID_IS_OBJ( _x_, IAP, OID_IAP_START(0) )
#define OID_IAP(    _x_ )                   OID_OBJ(    _x_, IAP, OID_IAP_START(0) )
#define IDX_IAP(    _x_ )                   IDX_OBJ(    _x_, IAP, OID_IAP_START(0) )

#define OID_IAP_START(        _idx_)        (154 + OFFSET_IAP(_idx_)) // Unused (spare)
#define OID_IAP_ANTENNA(      _idx_)        (155 + OFFSET_IAP(_idx_))
#define OID_IAP_BOND_40MHZ(   _idx_)        (156 + OFFSET_IAP(_idx_))
#define OID_IAP_BAND(         _idx_)        (157 + OFFSET_IAP(_idx_))
#define OID_IAP_CELL_SIZE(    _idx_)        (158 + OFFSET_IAP(_idx_))
#define OID_IAP_CHANNEL(      _idx_)        (159 + OFFSET_IAP(_idx_))
#define OID_IAP_DESC(         _idx_)        (160 + OFFSET_IAP(_idx_))
#define OID_IAP_CHANNEL_MODE( _idx_)        (161 + OFFSET_IAP(_idx_))
#define OID_IAP_WIFI_MODE(    _idx_)        (162 + OFFSET_IAP(_idx_))
#define OID_IAP_BOND_80MHZ(   _idx_)        (163 + OFFSET_IAP(_idx_))
#define OID_IAP_ENABLED(      _idx_)        (164 + OFFSET_IAP(_idx_))
#define OID_IAP_BOND_160MHZ(  _idx_)        (165 + OFFSET_IAP(_idx_))
#define OID_IAP_WDS_LINK(     _idx_)        (167 + OFFSET_IAP(_idx_))
#define OID_IAP_WDS_DISTANCE( _idx_)        (168 + OFFSET_IAP(_idx_))
#define OID_IAP_RX_THRESHOLD( _idx_)        (169 + OFFSET_IAP(_idx_))
#define OID_IAP_TX_POWER(     _idx_)        (172 + OFFSET_IAP(_idx_))
#define OID_IAP_END(          _idx_)        (173 + OFFSET_IAP(_idx_)) // Unused (spare)

#define OID_IAP_SPARE_1(      _idx_)        (166 + OFFSET_IAP(_idx_))
#define OID_IAP_SPARE_2(      _idx_)        (170 + OFFSET_IAP(_idx_))
#define OID_IAP_SPARE_3(      _idx_)        (171 + OFFSET_IAP(_idx_))

//---------------------------------------------------------
// Radius server
//---------------------------------------------------------
#define NUM_RADIUS_BASIC_OIDS               8
#define NUM_RADIUS_BASIC_OBJS               1

#define OID_IS_RADIUS_BASIC( _x_ )          OID_IS_OBJ( _x_, RADIUS_BASIC, OID_RADIUS_ENABLED )
#define OID_RADIUS_BASIC(    _x_ )          OID_OBJ(    _x_, RADIUS_BASIC, OID_RADIUS_ENABLED )
#define IDX_RADIUS_BASIC(    _x_ )          IDX_OBJ(    _x_, RADIUS_BASIC, OID_RADIUS_ENABLED )

#define OID_RADIUS_ENABLED                  474
#define OID_RADIUS_PRI_PORT                 475
#define OID_RADIUS_PRI_SECRET               476
#define OID_RADIUS_PRI_SERVER               477
#define OID_RADIUS_SEC_PORT                 478
#define OID_RADIUS_SEC_SECRET               479
#define OID_RADIUS_SEC_SERVER               480
#define OID_RADIUS_TIMEOUT                  481

//---------------------------------------------------------
// SNMP
//---------------------------------------------------------
#define OID_SNMP_V2_COMMUNITY_RW            482
#define OID_SNMP_V2_ENABLED                 483
#define OID_SNMP_TRAP_AUTH                  484     // Trap authentication failures?
#define OID_SNMP_TRAP_HOST_1                485     // Host to send traps to
#define OID_SNMP_TRAP_PORT_1                486     // Trap port number

//---------------------------------------------------------
// SSID [16x5]: (487-491) - (562-566)
//---------------------------------------------------------
#define NUM_SSID_BASIC_OIDS                 5
#define NUM_SSID_BASIC_OBJS                 16

#define OFFSET_SSID_BASIC( _x_ )            OFFSET_OBJ( _x_, SSID_BASIC, OID_SSID_FLAGS(0) )
#define OID_IS_SSID_BASIC( _x_ )            OID_IS_OBJ( _x_, SSID_BASIC, OID_SSID_FLAGS(0) )
#define OID_SSID_BASIC(    _x_ )            OID_OBJ(    _x_, SSID_BASIC, OID_SSID_FLAGS(0) )
#define IDX_SSID_BASIC(    _x_ )            IDX_OBJ(    _x_, SSID_BASIC, OID_SSID_FLAGS(0) )

#define OID_SSID_FLAGS(    _idx_)           (487 + OFFSET_SSID_BASIC(_idx_))
#define OID_SSID_ENC_TYPE( _idx_)           (488 + OFFSET_SSID_BASIC(_idx_))
#define OID_SSID_NAME(     _idx_)           (489 + OFFSET_SSID_BASIC(_idx_))
#define OID_SSID_QOS_LEVEL(_idx_)           (490 + OFFSET_SSID_BASIC(_idx_))
#define OID_SSID_VLAN(     _idx_)           (491 + OFFSET_SSID_BASIC(_idx_))

#define OID_SSID_NUM                        567     // Deprecated (here for backward compatibility)

//---------------------------------------------------------
// Station Activity
//---------------------------------------------------------
#define OID_STA_TIMEOUT                     568
#define OID_STA_REAUTH_PERIOD               569

//---------------------------------------------------------
// Syslog
//---------------------------------------------------------
#define OID_SYSLOG_CONSOLE                  570     // Display syslog messages on the console
#define OID_SYSLOG_ENABLED                  571
#define OID_SYSLOG_FILE_SIZE                572
#define OID_SYSLOG_LEVEL                    573
#define OID_SYSLOG_SERVER                   574     // IP of syslog sink

//---------------------------------------------------------
// Default security info
//---------------------------------------------------------
#define OID_SEC_WEP_ENABLED                 575
#define OID_SEC_WEP_KEY1                    576
#define OID_SEC_WEP_KEY2                    577
#define OID_SEC_WEP_KEY3                    578
#define OID_SEC_WEP_KEY4                    579
#define OID_SEC_WEP_KEY1_LEN                580
#define OID_SEC_WEP_KEY2_LEN                581
#define OID_SEC_WEP_KEY3_LEN                582
#define OID_SEC_WEP_KEY4_LEN                583
#define OID_SEC_WEP_KEY_ID_DEF              584     // ID of the default WEP Key (1 - 4)
#define OID_SEC_WPA_ENABLED                 585
#define OID_SEC_WPA_TKIP_ON                 586
#define OID_SEC_WPA_AES_ON                  587
#define OID_SEC_WPA_EAP_ON                  588
#define OID_SEC_WPA_PSK_ON                  589
#define OID_SEC_WPA_PASSPHRASE              590
#define OID_SEC_WPA_GROUP_REKEY             591

#define OID_IS_WEP_KEY( _x_ )               ((_x_) == OID_SEC_WEP_KEY1 || \
                                             (_x_) == OID_SEC_WEP_KEY2 || \
                                             (_x_) == OID_SEC_WEP_KEY3 || \
                                             (_x_) == OID_SEC_WEP_KEY4    )

//---------------------------------------------------------
// Global Radio properties
//---------------------------------------------------------
#define OID_IDS_ENABLE                      592     // 0 = Off, 1 = On,  2 = Advanced (NetChem)
#define OID_SYSLOG_SERVER_SEC               593     // IP of secondary syslog sink
#define OID_IAP_GLB_XRP_LAYER               594
#define OID_IAP_GLB_SHARP_CELL              595
#define OID_IAP_GLB_XRP_PEERS               596
#define OID_IDS_DATABASE                    597     // add or delete ssids to the rogue database
#define OID_IAP_GLB_COUNTRY_CODE            598
#define OID_IAP_GLB_XRP_MODE                599
#define OID_IAP_GLB_BEACON_INTERVAL         600
#define OID_IAP_GLB_BEACON_DTIM             601
#define OID_IAP_GLB_RETRY_LONG              602
#define OID_IAP_GLB_RETRY_SHORT             603
#define OID_IAP_GLB_DOT11_H_ENABLE          604
#define OID_IAP_GLB_ASSURANCE_MODE          605     // 0 = off, 1 = alert only, 2 = alert & repair, 3 = alert, repair, & reboot if needed
#define OID_IAP_GLB_A_RATES_BASIC           606
#define OID_IAP_GLB_A_RATES_SUPP            607
#define OID_IAP_GLB_A_FRAG_THRESHOLD        608
#define OID_IAP_GLB_A_RTS_THRESHOLD         609
#define OID_IAP_GLB_G_RATES_BASIC           610
#define OID_IAP_GLB_G_RATES_SUPP            611
#define OID_IAP_GLB_G_FRAG_THRESHOLD        612
#define OID_IAP_GLB_G_RTS_THRESHOLD         613
#define OID_IAP_GLB_G_DOT11_SLOT            614     // 0=long enabled (auto long/short) 1=short only
#define OID_IAP_GLB_G_DOT11_PREAMBLE        615     // 0=long only                      1=short enabled (auto long/short)
#define OID_IAP_GLB_LED_BLINK_MASK          616
//#define OID_SSID_BROADCAST                617     // Deprecated
#define OID_MGMT_TELNET_ENABLED             618
#define OID_MGMT_SSH_ENABLED                619
#define OID_TIME_OFFSET_HOURS               620
#define OID_TIME_OFFSET_MINS                621
#define OID_IAP_GLB_G_DOT11_PROTECT         622     // 0=off  1=CTS enabled (auto CTS)   2=RTS/CTS enabled (auto RTS)
#define OID_IAP_GLB_G_DOT11_GONLY           623     // 0=off  1=on
#define OID_IAP_GLB_AUTOCHANNEL_MODE        624
#define OID_IAP_GLB_AUTOCHANNEL_TIME        625
//#define OID_IAP_GLB_OLD_IAP_STATIONS      626     // Deprecated
#define OID_IAP_GLB_MGMT_ENABLED            627
#define OID_TIME_DST_ADJUST                 628     // Adjust for daylight savings
#define OID_IAP_GLB_STA2STA_BLOCK           629     // block or forward intra-Array STA to STA traffic
#define OID_SEC_WPA2_ENABLED                630
#define OID_ETH_MODE                        631     // 0=link backup, 1=eth1 only, 2=eth2 only, 3=load balance, 4=bridge, 5=802.3ad, 6=broadcast, 7=mirror
#define OID_IAP_GLB_LOAD_BALANCING          632     // 0=off, 1=on, 2=aggressive
#define OID_IAP_GLB_TX_COORDINATION         633     // 0=off, 1=on, 2=cts
#define OID_IAP_GLB_RX_COORDINATION         634     // 0=off
#define OID_IAP_GLB_TXRX_BALANCING          635     // 0=off
#define OID_MGMT_HTTPS_ENABLED              636
#define OID_MGMT_CON_ENABLED                637
#define OID_TIMEOUT_TELNET                  638
#define OID_TIMEOUT_SSH                     639
#define OID_TIMEOUT_HTTPS                   640

//---------------------------------------------------------
// Security info by SSID [16x17]: (641-657) - (895-912)
//---------------------------------------------------------
#define NUM_SSID_SECURITY_OIDS              17
#define NUM_SSID_SECURITY_OBJS              NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_SECURITY( _x_ )         OFFSET_OBJ( _x_, SSID_SECURITY, OID_SSID_SEC_WEP_ENABLED(0) )
#define OID_IS_SSID_SECURITY( _x_ )         OID_IS_OBJ( _x_, SSID_SECURITY, OID_SSID_SEC_WEP_ENABLED(0) )
#define OID_SSID_SECURITY(    _x_ )         OID_OBJ(    _x_, SSID_SECURITY, OID_SSID_SEC_WEP_ENABLED(0) )
#define IDX_SSID_SECURITY(    _x_ )         IDX_OBJ(    _x_, SSID_SECURITY, OID_SSID_SEC_WEP_ENABLED(0) )

#define OID_SSID_SEC_WEP_ENABLED(    _idx_) (641 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WEP_KEY1(       _idx_) (642 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WEP_KEY2(       _idx_) (643 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WEP_KEY3(       _idx_) (644 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WEP_KEY4(       _idx_) (645 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WEP_KEY1_LEN(   _idx_) (646 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WEP_KEY2_LEN(   _idx_) (647 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WEP_KEY3_LEN(   _idx_) (648 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WEP_KEY4_LEN(   _idx_) (649 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WEP_KEY_ID_DEF( _idx_) (650 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WPA_ENABLED(    _idx_) (651 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WPA_TKIP_ON(    _idx_) (652 + OFFSET_SSID_SECURITY(_idx_)) // not used if OID_PER_SSID_CIPHER_ENABLE is 0
#define OID_SSID_SEC_WPA_AES_ON(     _idx_) (653 + OFFSET_SSID_SECURITY(_idx_)) // not used if OID_PER_SSID_CIPHER_ENABLE is 0
#define OID_SSID_SEC_WPA_EAP_ON(     _idx_) (654 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WPA_PSK_ON(     _idx_) (655 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WPA_PASSPHRASE( _idx_) (656 + OFFSET_SSID_SECURITY(_idx_))
#define OID_SSID_SEC_WPA_GROUP_REKEY(_idx_) (657 + OFFSET_SSID_SECURITY(_idx_)) // not used, global only

//---------------------------------------------------------
// Radius info by SSID [16x8]: (913-920) - (1033-1040)
//---------------------------------------------------------
#define NUM_SSID_RADIUS_OIDS                8
#define NUM_SSID_RADIUS_OBJS                NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_RADIUS( _x_ )           OFFSET_OBJ( _x_, SSID_RADIUS, OID_SSID_RAD_ENABLED(0) )
#define OID_IS_SSID_RADIUS( _x_ )           OID_IS_OBJ( _x_, SSID_RADIUS, OID_SSID_RAD_ENABLED(0) )
#define OID_SSID_RADIUS(    _x_ )           OID_OBJ(    _x_, SSID_RADIUS, OID_SSID_RAD_ENABLED(0) )
#define IDX_SSID_RADIUS(    _x_ )           IDX_OBJ(    _x_, SSID_RADIUS, OID_SSID_RAD_ENABLED(0) )

#define OID_SSID_RAD_ENABLED(   _idx_)      (913 + OFFSET_SSID_RADIUS(_idx_))
#define OID_SSID_RAD_PRI_PORT(  _idx_)      (914 + OFFSET_SSID_RADIUS(_idx_))
#define OID_SSID_RAD_PRI_SECRET(_idx_)      (915 + OFFSET_SSID_RADIUS(_idx_))
#define OID_SSID_RAD_PRI_SERVER(_idx_)      (916 + OFFSET_SSID_RADIUS(_idx_))
#define OID_SSID_RAD_SEC_PORT(  _idx_)      (917 + OFFSET_SSID_RADIUS(_idx_))
#define OID_SSID_RAD_SEC_SECRET(_idx_)      (918 + OFFSET_SSID_RADIUS(_idx_))
#define OID_SSID_RAD_SEC_SERVER(_idx_)      (919 + OFFSET_SSID_RADIUS(_idx_))
#define OID_SSID_RAD_TIMEOUT(   _idx_)      (920 + OFFSET_SSID_RADIUS(_idx_))

//---------------------------------------------------------
// SSID Default Security [16x1]: 1041 - 1056
//---------------------------------------------------------
#define NUM_SSID_SEC_DFLT_OIDS              1
#define NUM_SSID_SEC_DFLT_OBJS              NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_SEC_DFLT( _x_ )         OFFSET_OBJ( _x_, SSID_SEC_DFLT, OID_SSID_SEC_DEF(0) )
#define OID_IS_SSID_SEC_DFLT( _x_ )         OID_IS_OBJ( _x_, SSID_SEC_DFLT, OID_SSID_SEC_DEF(0) )
#define OID_SSID_SEC_DFLT(    _x_ )         OID_OBJ(    _x_, SSID_SEC_DFLT, OID_SSID_SEC_DEF(0) )
#define IDX_SSID_SEC_DFLT(    _x_ )         IDX_OBJ(    _x_, SSID_SEC_DFLT, OID_SSID_SEC_DEF(0) )

#define OID_SSID_SEC_DEF(_idx_)             (1041 + OFFSET_SSID_SEC_DFLT(_idx_))

//---------------------------------------------------------
// SSID MAC Number [16x1]: 1057 - 1072
//---------------------------------------------------------
#define NUM_SSID_MAC_OIDS                   1
#define NUM_SSID_MAC_OBJS                   NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_MAC( _x_ )              OFFSET_OBJ( _x_, SSID_MAC, OID_SSID_MAC_NUM(0) )
#define OID_IS_SSID_MAC( _x_ )              OID_IS_OBJ( _x_, SSID_MAC, OID_SSID_MAC_NUM(0) )
#define OID_SSID_MAC(    _x_ )              OID_OBJ(    _x_, SSID_MAC, OID_SSID_MAC_NUM(0) )
#define IDX_SSID_MAC(    _x_ )              IDX_OBJ(    _x_, SSID_MAC, OID_SSID_MAC_NUM(0) )

#define OID_SSID_MAC_NUM(_idx_)             (1057 + OFFSET_SSID_MAC(_idx_)) // also used to format 'exit' on output

//---------------------------------------------------------
// Multi DHCP Pool info [16x11]: (1073-1083) - (1238-1248)
//---------------------------------------------------------
#define NUM_DHCP_POOL_OIDS                  11
#define NUM_DHCP_POOL_OBJS                  NUM_SSID_BASIC_OBJS

#define OFFSET_DHCP_POOL( _x_ )             OFFSET_OBJ( _x_, DHCP_POOL, OID_DHCP_POOL_ENABLED(0) )
#define OID_IS_DHCP_POOL( _x_ )             OID_IS_OBJ( _x_, DHCP_POOL, OID_DHCP_POOL_ENABLED(0) )
#define OID_DHCP_POOL(    _x_ )             OID_OBJ(    _x_, DHCP_POOL, OID_DHCP_POOL_ENABLED(0) )
#define IDX_DHCP_POOL(    _x_ )             IDX_OBJ(    _x_, DHCP_POOL, OID_DHCP_POOL_ENABLED(0) )

#define OID_DHCP_POOL_ENABLED(      _idx)   (1073 + OFFSET_DHCP_POOL(_idx))
#define OID_DHCP_POOL_LEASE_DEFAULT(_idx)   (1074 + OFFSET_DHCP_POOL(_idx))
#define OID_DHCP_POOL_LEASE_MAX(    _idx)   (1075 + OFFSET_DHCP_POOL(_idx))
#define OID_DHCP_POOL_RANGE_END(    _idx)   (1076 + OFFSET_DHCP_POOL(_idx))
#define OID_DHCP_POOL_RANGE_START(  _idx)   (1077 + OFFSET_DHCP_POOL(_idx))
#define OID_DHCP_POOL_GATEWAY(      _idx)   (1078 + OFFSET_DHCP_POOL(_idx))
#define OID_DHCP_POOL_MASK(         _idx)   (1079 + OFFSET_DHCP_POOL(_idx))
#define OID_DHCP_POOL_DNS_DOMAIN(   _idx)   (1080 + OFFSET_DHCP_POOL(_idx))
#define OID_DHCP_POOL_DNS_SERVER1(  _idx)   (1081 + OFFSET_DHCP_POOL(_idx))
#define OID_DHCP_POOL_DNS_SERVER2(  _idx)   (1082 + OFFSET_DHCP_POOL(_idx))
#define OID_DHCP_POOL_DNS_SERVER3(  _idx)   (1083 + OFFSET_DHCP_POOL(_idx))

//---------------------------------------------------------
// SSID DHCP info [16x1]: 1249 - 1264
//---------------------------------------------------------
#define NUM_SSID_DHCP_OIDS                  1
#define NUM_SSID_DHCP_OBJS                  NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_DHCP( _x_ )             OFFSET_OBJ( _x_, SSID_DHCP, OID_SSID_DHCP_POOL(0) )
#define OID_IS_SSID_DHCP( _x_ )             OID_IS_OBJ( _x_, SSID_DHCP, OID_SSID_DHCP_POOL(0) )
#define OID_SSID_DHCP(    _x_ )             OID_OBJ(    _x_, SSID_DHCP, OID_SSID_DHCP_POOL(0) )
#define IDX_SSID_DHCP(    _x_ )             IDX_OBJ(    _x_, SSID_DHCP, OID_SSID_DHCP_POOL(0) )

#define OID_SSID_DHCP_POOL(_idx_)           (1249 + OFFSET_SSID_DHCP(_idx_) )

//---------------------------------------------------------
// DHCP pool names [16x1 + 1]: 1265 - 1280, 1281
//---------------------------------------------------------
#define NUM_DHCP_NAME_OIDS                  1
#define NUM_DHCP_NAME_OBJS                  NUM_DHCP_POOL_OBJS

#define OFFSET_DHCP_NAME( _x_ )             OFFSET_OBJ( _x_, DHCP_NAME, OID_DHCP_POOL_NAME(0) )
#define OID_IS_DHCP_NAME( _x_ )             OID_IS_OBJ( _x_, DHCP_NAME, OID_DHCP_POOL_NAME(0) )
#define OID_DHCP_NAME(    _x_ )             OID_OBJ(    _x_, DHCP_NAME, OID_DHCP_POOL_NAME(0) )
#define IDX_DHCP_NAME(    _x_ )             IDX_OBJ(    _x_, DHCP_NAME, OID_DHCP_POOL_NAME(0) )

#define OID_DHCP_POOL_NAME(_idx_)           (1265 + OFFSET_DHCP_NAME(_idx_))

#define OID_DHCP_POOL_NUM                   1281

//---------------------------------------------------------
// WDS Link Info [4x8]: (1282-1289) - (1306-1313)
//---------------------------------------------------------
#define NUM_WDS_LINK_OIDS                   8
#define NUM_WDS_LINK_OBJS                   4

#define OFFSET_WDS_LINK( _x_ )              OFFSET_OBJ( _x_, WDS_LINK, OID_WDS_LINK_ENABLED(0) )
#define OID_IS_WDS_LINK( _x_ )              OID_IS_OBJ( _x_, WDS_LINK, OID_WDS_LINK_ENABLED(0) )
#define OID_WDS_LINK(    _x_ )              OID_OBJ(    _x_, WDS_LINK, OID_WDS_LINK_ENABLED(0) )
#define IDX_WDS_LINK(    _x_ )              IDX_OBJ(    _x_, WDS_LINK, OID_WDS_LINK_ENABLED(0) )

#define OID_WDS_LINK_ENABLED( _idx_)        (1282 + OFFSET_WDS_LINK(_idx_))
#define OID_WDS_LINK_MAX_IAP( _idx_)        (1283 + OFFSET_WDS_LINK(_idx_))
#define OID_WDS_LINK_TARGET(  _idx_)        (1284 + OFFSET_WDS_LINK(_idx_))
#define OID_WDS_LINK_SSID(    _idx_)        (1285 + OFFSET_WDS_LINK(_idx_))
#define OID_WDS_LINK_USERNAME(_idx_)        (1286 + OFFSET_WDS_LINK(_idx_))
#define OID_WDS_LINK_PASSWORD(_idx_)        (1287 + OFFSET_WDS_LINK(_idx_))
#define OID_WDS_LINK_IAP_LIST(_idx_)        (1288 + OFFSET_WDS_LINK(_idx_)) //used for display purposes
#define OID_WDS_LINK_SPARE(   _idx_)        (1289 + OFFSET_WDS_LINK(_idx_))

//---------------------------------------------------------
// DHCP pool nat flags [16x1]: 1314 - 1329
//---------------------------------------------------------
#define NUM_DHCP_NAT_OIDS                   1
#define NUM_DHCP_NAT_OBJS                   NUM_DHCP_POOL_OBJS

#define OFFSET_DHCP_NAT( _x_ )              OFFSET_OBJ( _x_, DHCP_NAT, OID_DHCP_POOL_NAT(0) )
#define OID_IS_DHCP_NAT( _x_ )              OID_IS_OBJ( _x_, DHCP_NAT, OID_DHCP_POOL_NAT(0) )
#define OID_DHCP_NAT(    _x_ )              OID_OBJ(    _x_, DHCP_NAT, OID_DHCP_POOL_NAT(0) )
#define IDX_DHCP_NAT(    _x_ )              IDX_OBJ(    _x_, DHCP_NAT, OID_DHCP_POOL_NAT(0) )

#define OID_DHCP_POOL_NAT(_idx_)            (1314 + OFFSET_DHCP_NAT(_idx_))

//---------------------------------------------------------
// SSID Limits [16x8]: (1330-1337) - (1450-1457)
//---------------------------------------------------------
#define NUM_SSID_LIMIT_OIDS                 8
#define NUM_SSID_LIMIT_OBJS                 NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_LIMIT( _x_ )            OFFSET_OBJ( _x_, SSID_LIMIT, OID_SSID_ENABLED(0) )
#define OID_IS_SSID_LIMIT( _x_ )            OID_IS_OBJ( _x_, SSID_LIMIT, OID_SSID_ENABLED(0) )
#define OID_SSID_LIMIT(    _x_ )            OID_OBJ(    _x_, SSID_LIMIT, OID_SSID_ENABLED(0) )
#define IDX_SSID_LIMIT(    _x_ )            IDX_OBJ(    _x_, SSID_LIMIT, OID_SSID_ENABLED(0) )

#define OID_SSID_ENABLED(      _idx_)       (1330 + OFFSET_SSID_LIMIT(_idx_))
#define OID_SSID_DAYS_ON(      _idx_)       (1331 + OFFSET_SSID_LIMIT(_idx_))
#define OID_SSID_TIME_ON(      _idx_)       (1332 + OFFSET_SSID_LIMIT(_idx_))
#define OID_SSID_TIME_OFF(     _idx_)       (1333 + OFFSET_SSID_LIMIT(_idx_))
#define OID_SSID_PPS_LIMIT(    _idx_)       (1334 + OFFSET_SSID_LIMIT(_idx_))
#define OID_SSID_PPS_PER_STA(  _idx_)       (1335 + OFFSET_SSID_LIMIT(_idx_))
#define OID_SSID_STATION_LIMIT(_idx_)       (1336 + OFFSET_SSID_LIMIT(_idx_))
#define OID_SSID_LIMIT_SPARE(  _idx_)       (1337 + OFFSET_SSID_LIMIT(_idx_))

//---------------------------------------------------------
// VLAN Default Management
//---------------------------------------------------------
#define OID_VLAN_DEFAULT                    1458

//---------------------------------------------------------
// Standby configuration
//---------------------------------------------------------
#define OID_STANDBY_ENABLED                 1459
#define OID_STANDBY_TARGET                  1460

//---------------------------------------------------------
// Global Radio properties
//---------------------------------------------------------
#define OID_IAP_GLB_MAX_IAP_PHONES          1461
#define OID_IAP_GLB_PUBLIC_SAFETY           1462

//---------------------------------------------------------
// New SNMP properties
//---------------------------------------------------------
#define OID_SNMP_V2_COMMUNITY_RO            1463

//---------------------------------------------------------
// New Autocell properties
//---------------------------------------------------------
#define OID_IAP_GLB_AUTO_CELL_PERIOD        1464    // 0 = off
#define OID_IAP_GLB_AUTO_CELL_OVERLAP       1465

//---------------------------------------------------------
// SSID Web Page Redirect [16x8]: (1466-1473) - (1586-1593)
//---------------------------------------------------------
#define NUM_SSID_WPR_OIDS                   8
#define NUM_SSID_WPR_OBJS                   NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_WPR( _x_ )              OFFSET_OBJ( _x_, SSID_WPR, OID_SSID_WPR_ENABLED(0) )
#define OID_IS_SSID_WPR( _x_ )              OID_IS_OBJ( _x_, SSID_WPR, OID_SSID_WPR_ENABLED(0) )
#define OID_SSID_WPR(    _x_ )              OID_OBJ(    _x_, SSID_WPR, OID_SSID_WPR_ENABLED(0) )
#define IDX_SSID_WPR(    _x_ )              IDX_OBJ(    _x_, SSID_WPR, OID_SSID_WPR_ENABLED(0) )

#define OID_SSID_WPR_ENABLED(     _idx_)    (1466 + OFFSET_SSID_WPR(_idx_))
#define OID_SSID_WPR_REDIRECT_URL(_idx_)    (1467 + OFFSET_SSID_WPR(_idx_))
#define OID_SSID_WPR_SECRET(      _idx_)    (1468 + OFFSET_SSID_WPR(_idx_))
#define OID_SSID_WPR_HTTPS(       _idx_)    (1469 + OFFSET_SSID_WPR(_idx_))
#define OID_SSID_WPR_AUTH_TYPE(   _idx_)    (1470 + OFFSET_SSID_WPR(_idx_))
#define OID_SSID_WPR_SERVER(      _idx_)    (1471 + OFFSET_SSID_WPR(_idx_))
#define OID_SSID_WPR_SCREEN(      _idx_)    (1472 + OFFSET_SSID_WPR(_idx_))
#define OID_SSID_WPR_TIMEOUT(     _idx_)    (1473 + OFFSET_SSID_WPR(_idx_))

//---------------------------------------------------------
// SSID Web Page Redirect Landing Page [16x1]: (1594-1609)
//---------------------------------------------------------
#define NUM_SSID_WPR_LANDING_OIDS           1
#define NUM_SSID_WPR_LANDING_OBJS           NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_WPR_LANDING( _x_ )      OFFSET_OBJ( _x_, SSID_WPR_LANDING, OID_SSID_WPR_LANDING_URL(0) )
#define OID_IS_SSID_WPR_LANDING( _x_ )      OID_IS_OBJ( _x_, SSID_WPR_LANDING, OID_SSID_WPR_LANDING_URL(0) )
#define OID_SSID_WPR_LANDING(    _x_ )      OID_OBJ(    _x_, SSID_WPR_LANDING, OID_SSID_WPR_LANDING_URL(0) )
#define IDX_SSID_WPR_LANDING(    _x_ )      IDX_OBJ(    _x_, SSID_WPR_LANDING, OID_SSID_WPR_LANDING_URL(0) )

#define OID_SSID_WPR_LANDING_URL(_idx_)     (1594 + OFFSET_SSID_WPR_LANDING(_idx_))

//---------------------------------------------------------
// Native VLAN
//---------------------------------------------------------
#define OID_VLAN_NATIVE                     1610
#define OID_IS_GLB_VLAN_CTRL(_x_)           ((_x_) == OID_VLAN_NATIVE || \
                                             (_x_) == OID_VLAN_DEFAULT   )

//---------------------------------------------------------
// eth0 static route [1x2]: (1747-1748)
//---------------------------------------------------------
#define NUM_ETH_ROUTE_OIDS                  2
#define NUM_ETH_ROUTE_OBJS                  1

#define OID_IS_ETH_ROUTE( _x_ )             OID_IS_OBJ( _x_, ETH_ROUTE, OID_ETH_ROUTE_ADDR )
#define IDX_ETH_ROUTE(    _x_ )             IDX_OBJ(    _x_, ETH_ROUTE, OID_ETH_ROUTE_ADDR )

#define OID_ETH_ROUTE_ADDR                  1611
#define OID_ETH_ROUTE_MASK                  1612

//---------------------------------------------------------
// Global Radius accounting info [1x8]: (1613-1620)
//---------------------------------------------------------
#define NUM_RADIUS_ACCT_OIDS                8
#define NUM_RADIUS_ACCT_OBJS                1

#define OID_IS_RADIUS_ACCT( _x_ )           OID_IS_OBJ( _x_, RADIUS_ACCT, OID_RADIUS_ACCT_ENABLED )
#define IDX_RADIUS_ACCT(    _x_ )           IDX_OBJ(    _x_, RADIUS_ACCT, OID_RADIUS_ACCT_ENABLED )

#define OID_RADIUS_ACCT_ENABLED             1613
#define OID_RADIUS_ACCT_PRI_PORT            1614
#define OID_RADIUS_ACCT_PRI_SECRET          1615
#define OID_RADIUS_ACCT_PRI_SERVER          1616
#define OID_RADIUS_ACCT_SEC_PORT            1617
#define OID_RADIUS_ACCT_SEC_SECRET          1618
#define OID_RADIUS_ACCT_SEC_SERVER          1619
#define OID_RADIUS_ACCT_INTERVAL            1620

//---------------------------------------------------------
// Radius accounting info by SSID [16x8]: (1621-1628) - (1741-1748)
//---------------------------------------------------------
#define NUM_SSID_RADIUS_ACCT_OIDS           8
#define NUM_SSID_RADIUS_ACCT_OBJS           NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_RADIUS_ACCT( _x_ )      OFFSET_OBJ( _x_, SSID_RADIUS_ACCT, OID_SSID_RAD_ACCT_ENABLED(0) )
#define OID_IS_SSID_RADIUS_ACCT( _x_ )      OID_IS_OBJ( _x_, SSID_RADIUS_ACCT, OID_SSID_RAD_ACCT_ENABLED(0) )
#define OID_SSID_RADIUS_ACCT(    _x_ )      OID_OBJ(    _x_, SSID_RADIUS_ACCT, OID_SSID_RAD_ACCT_ENABLED(0) )
#define IDX_SSID_RADIUS_ACCT(    _x_ )      IDX_OBJ(    _x_, SSID_RADIUS_ACCT, OID_SSID_RAD_ACCT_ENABLED(0) )

#define OID_SSID_RAD_ACCT_ENABLED(   _idx_) (1621 + OFFSET_SSID_RADIUS_ACCT(_idx_))
#define OID_SSID_RAD_ACCT_PRI_PORT(  _idx_) (1622 + OFFSET_SSID_RADIUS_ACCT(_idx_))
#define OID_SSID_RAD_ACCT_PRI_SECRET(_idx_) (1623 + OFFSET_SSID_RADIUS_ACCT(_idx_))
#define OID_SSID_RAD_ACCT_PRI_SERVER(_idx_) (1624 + OFFSET_SSID_RADIUS_ACCT(_idx_))
#define OID_SSID_RAD_ACCT_SEC_PORT(  _idx_) (1625 + OFFSET_SSID_RADIUS_ACCT(_idx_))
#define OID_SSID_RAD_ACCT_SEC_SECRET(_idx_) (1626 + OFFSET_SSID_RADIUS_ACCT(_idx_))
#define OID_SSID_RAD_ACCT_SEC_SERVER(_idx_) (1627 + OFFSET_SSID_RADIUS_ACCT(_idx_))
#define OID_SSID_RAD_ACCT_INTERVAL(  _idx_) (1628 + OFFSET_SSID_RADIUS_ACCT(_idx_))

//---------------------------------------------------------
// SSID Authentication info [16x1]: 1749 - 1764
//---------------------------------------------------------
#define NUM_SSID_AUTH_OIDS                  1
#define NUM_SSID_AUTH_OBJS                  NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_AUTH( _x_ )             OFFSET_OBJ( _x_, SSID_AUTH, OID_SSID_AUTH_TYPE(0) )
#define OID_IS_SSID_AUTH( _x_ )             OID_IS_OBJ( _x_, SSID_AUTH, OID_SSID_AUTH_TYPE(0) )
#define OID_SSID_AUTH(    _x_ )             OID_OBJ(    _x_, SSID_AUTH, OID_SSID_AUTH_TYPE(0) )
#define IDX_SSID_AUTH(    _x_ )             IDX_OBJ(    _x_, SSID_AUTH, OID_SSID_AUTH_TYPE(0) )

#define OID_SSID_AUTH_TYPE(_idx_)           (1749 + OFFSET_SSID_AUTH(_idx_) )

//---------------------------------------------------------
// Syslog Level info
//---------------------------------------------------------
#define OID_SYSLOG_CONSOLE_LEVEL            1765    // Syslog level for console
#define OID_SYSLOG_SERVER_PRI_LEVEL         1766    // Syslog level for primary server
#define OID_SYSLOG_SERVER_SEC_LEVEL         1767    // Syslog level for secondary server

//---------------------------------------------------------
// SSID Extended info [16x8]: (1768-1775) - (1888-1895)
//---------------------------------------------------------
#define NUM_SSID_EXT_OIDS                   8
#define NUM_SSID_EXT_OBJS                   NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_EXT( _x_ )              OFFSET_OBJ( _x_, SSID_EXT, OID_SSID_XRP_LAYER(0) )
#define OID_IS_SSID_EXT( _x_ )              OID_IS_OBJ( _x_, SSID_EXT, OID_SSID_XRP_LAYER(0) )
#define OID_SSID_EXT(    _x_ )              OID_OBJ(    _x_, SSID_EXT, OID_SSID_XRP_LAYER(0) )
#define IDX_SSID_EXT(    _x_ )              IDX_OBJ(    _x_, SSID_EXT, OID_SSID_XRP_LAYER(0) )

#define OID_SSID_XRP_LAYER(   _idx_)        (1768 + OFFSET_SSID_EXT(_idx_))
#define OID_SSID_FILTER_LIST( _idx_)        (1769 + OFFSET_SSID_EXT(_idx_))
#define OID_SSID_ACTIVE_IAPS( _idx_)        (1770 + OFFSET_SSID_EXT(_idx_))
#define OID_SSID_ACL_LIST(    _idx_)        (1771 + OFFSET_SSID_EXT(_idx_))
#define OID_SSID_ACL_ENTRY(   _idx_)        (1772 + OFFSET_SSID_EXT(_idx_))
#define OID_SSID_FALLBACK(    _idx_)        (1773 + OFFSET_SSID_EXT(_idx_))
#define OID_SSID_KBPS_LIMIT(  _idx_)        (1774 + OFFSET_SSID_EXT(_idx_))
#define OID_SSID_KBPS_PER_STA(_idx_)        (1775 + OFFSET_SSID_EXT(_idx_))

#define OID_IS_SSID_ACL_ENTRY(_x_)          (OID_IS_SSID_EXT(_x_) && OID_SSID_EXT(_x_) == OID_SSID_ACL_ENTRY(0))

//---------------------------------------------------------
// More Radius info
//---------------------------------------------------------
#define OID_RADIUS_NAS_ID                   1896

//---------------------------------------------------------
// More SNMP settings
//---------------------------------------------------------
#define OID_SNMP_TRAP_HOST_2                1897
#define OID_SNMP_TRAP_HOST_3                1898
#define OID_SNMP_TRAP_HOST_4                1899
#define OID_SNMP_TRAP_PORT_2                1900
#define OID_SNMP_TRAP_PORT_3                1901
#define OID_SNMP_TRAP_PORT_4                1902

//---------------------------------------------------------
// More Global Properties
//---------------------------------------------------------
#define OID_WDS_ALLOW_STATIONS              1903

//---------------------------------------------------------
// Syslog Email info
//---------------------------------------------------------
#define OID_SYSLOG_EMAIL_HOST               1904
#define OID_SYSLOG_EMAIL_FROM               1905
#define OID_SYSLOG_EMAIL_RCPT               1906
#define OID_SYSLOG_EMAIL_LEVEL              1907

//---------------------------------------------------------
// More Autocell properties
//---------------------------------------------------------
#define OID_IAP_GLB_AUTO_CELL_MIN_TX        1908    // 0 = off
#define OID_IS_GLB_AUTO_CELL(_x_)           ((_x_) == OID_IAP_GLB_AUTO_CELL_PERIOD  || \
                                             (_x_) == OID_IAP_GLB_AUTO_CELL_OVERLAP || \
                                             (_x_) == OID_IAP_GLB_AUTO_CELL_MIN_TX  || \
                                             (_x_) == OID_IAP_GLB_AUTO_CELL_MAX_RX  || \
                                             (_x_) == OID_IAP_GLB_AUTO_CELL_BY_CHAN    )

//---------------------------------------------------------
// Syslog Email authentication info
//---------------------------------------------------------
#define OID_SYSLOG_EMAIL_USER               1909
#define OID_SYSLOG_EMAIL_PASSWORD           1910

//---------------------------------------------------------
// 802.11n configuration info
//---------------------------------------------------------
#define OID_IAP_GLB_MCS_RATES_BASIC         1911    // bit 0 = mcs0 .... bit 63 = msc63
#define OID_IAP_GLB_MCS_RATES_SUPP          1912    // bit 0 = mcs0 .... bit 63 = msc63
#define OID_IAP_GLB_N_AGGREGATE_RETRIES     1913    // number of times to retry aggregates
#define OID_IAP_GLB_N_BLOCK_NAK_RETRIES     1914    // number of times to retry packets nack'd in an aggregate
#define OID_IAP_GLB_N_ENABLE                1915    // 0=off,  1=on
#define OID_IAP_GLB_N_TX_CHAINS             1916    // 1 - 3
#define OID_IAP_GLB_N_RX_CHAINS             1917    // 1 - 3
#define OID_IAP_GLB_N_SGI_40MHZ             1918    // 0=long,    1=short
#define OID_IAP_GLB_N_BOND_MODE_5GHZ        1919    // 0=dynamic  1=static
#define OID_IAP_GLB_N_BOND_MODE_2GHZ        1920    // 0=dynamic  1=static
#define OID_IAP_GLB_N_AUTO_BOND             1921    // 0=off      1=on (automatically bond any channel that can be bonded)
#define OID_IAP_GLB_N_SGI_20MHZ             1922    // 0=long,    1=short
#define OID_IAP_GLB_N_SPARE_1               1923    // just in case ....
#define OID_IAP_GLB_N_SPARE_2               1924
#define OID_IAP_GLB_N_SPARE_3               1925

//---------------------------------------------------------
// More misc. global IAP configuration info
//---------------------------------------------------------
#define OID_IAP_GLB_OKC_ENABLE              1926

//---------------------------------------------------------
// Station info extraction settings
//---------------------------------------------------------
#define OID_IAP_GLB_EXTRACT_IP_DHCP_PERIOD  1927
#define OID_IAP_GLB_EXTRACT_FLAGS           1928

//---------------------------------------------------------
// Syslog Email port info
//---------------------------------------------------------
#define OID_SYSLOG_EMAIL_PORT               1929

//---------------------------------------------------------
// More misc. global IAP configuration info
//---------------------------------------------------------
#define OID_IAP_GLB_MAX_ALL_STATIONS        1930
#define OID_IAP_GLB_RF_MONITOR_ENABLE       1931
#define OID_IAP_GLB_WFA_MODE                1932
#define OID_IAP_GLB_ARP_FILTER              1933

//---------------------------------------------------------
// Autochannel extended configuration info
//---------------------------------------------------------
#define OID_IAP_GLB_A_AUTOCHANNEL_LIST      1934
#define OID_IAP_GLB_G_AUTOCHANNEL_LIST      1935
#define OID_IAP_GLB_AUTOCHANNEL_SPARE       1936

//---------------------------------------------------------
// CDP configuration info
//---------------------------------------------------------
#define OID_CDP_ENABLE                      1937
#define OID_CDP_INTERVAL                    1938
#define OID_CDP_HOLD_TIME                   1939

//---------------------------------------------------------
// User Group properties [16x25]: (1940-1964) - (2315-2339)
//---------------------------------------------------------
#define NUM_USER_GROUP_OIDS                 25
#define NUM_USER_GROUP_OBJS                 16

#define OFFSET_USER_GROUP( _x_ )            OFFSET_OBJ( _x_, USER_GROUP, OID_GROUP_ENABLED(0) )
#define OID_IS_USER_GROUP( _x_ )            OID_IS_OBJ( _x_, USER_GROUP, OID_GROUP_ENABLED(0) )
#define OID_USER_GROUP(    _x_ )            OID_OBJ(    _x_, USER_GROUP, OID_GROUP_ENABLED(0) )
#define IDX_USER_GROUP(    _x_ )            IDX_OBJ(    _x_, USER_GROUP, OID_GROUP_ENABLED(0) )

#define OID_GROUP_ENABLED(         _idx_)   (1940 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_NAME(            _idx_)   (1941 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_RADIUS_ID(       _idx_)   (1942 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_QOS_LEVEL(       _idx_)   (1943 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_VLAN(            _idx_)   (1944 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_DHCP_POOL(       _idx_)   (1945 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_FILTER_LIST(     _idx_)   (1946 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_DAYS_ON(         _idx_)   (1947 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_TIME_ON(         _idx_)   (1948 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_TIME_OFF(        _idx_)   (1949 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_PPS_LIMIT(       _idx_)   (1950 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_PPS_PER_STA(     _idx_)   (1951 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_STATION_LIMIT(   _idx_)   (1952 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_WPR_ENABLED(     _idx_)   (1953 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_WPR_LANDING_URL( _idx_)   (1954 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_WPR_SPARE(       _idx_)   (1955 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_WPR_SPLASH(      _idx_)   (1956 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_WPR_TIMEOUT(     _idx_)   (1957 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_XRP_LAYER(       _idx_)   (1958 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_MAC_NUM(         _idx_)   (1959 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_FALLBACK(        _idx_)   (1960 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_DEVICE_ID(       _idx_)   (1961 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_KBPS_LIMIT(      _idx_)   (1962 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_KBPS_PER_STA(    _idx_)   (1963 + OFFSET_USER_GROUP(_idx_))
#define OID_GROUP_WPR_WLIST_DN(    _idx_)   (1964 + OFFSET_USER_GROUP(_idx_))

#define OID_IS_GROUP_WPR_WLIST(_x_)         (OID_IS_USER_GROUP(_x_) && OID_USER_GROUP(_x_) == OID_GROUP_WPR_WLIST_DN(0))

#define OID_GROUP_NUM                       2340

//---------------------------------------------------------
// Filter List info [16x4]: (2341-2344) - (2401-2404)
//---------------------------------------------------------
#define NUM_FILTER_LIST_OIDS                4
#define NUM_FILTER_LIST_OBJS                16

#define OFFSET_FILTER_LIST( _x_ )           OFFSET_OBJ( _x_, FILTER_LIST, OID_FILTER_LIST_NAME(0) )
#define OID_IS_FILTER_LIST( _x_ )           OID_IS_OBJ( _x_, FILTER_LIST, OID_FILTER_LIST_NAME(0) )
#define OID_FILTER_LIST(    _x_ )           OID_OBJ(    _x_, FILTER_LIST, OID_FILTER_LIST_NAME(0) )
#define IDX_FILTER_LIST(    _x_ )           IDX_OBJ(    _x_, FILTER_LIST, OID_FILTER_LIST_NAME(0) )

#define OID_FILTER_LIST_NAME(   _idx_)      (2341 + OFFSET_FILTER_LIST(_idx_))
#define OID_FILTER_LIST_ENABLED(_idx_)      (2342 + OFFSET_FILTER_LIST(_idx_))
#define OID_FILTER_LIST_SPARE_1(_idx_)      (2343 + OFFSET_FILTER_LIST(_idx_))
#define OID_FILTER_LIST_SPARE_2(_idx_)      (2344 + OFFSET_FILTER_LIST(_idx_))

#define OID_FILTER_LIST_NUM                 2405

//---------------------------------------------------------
// Global Filter objects
//---------------------------------------------------------
#define OID_GLB_FILTER_LIST_ENABLED         2406
#define OID_GLB_FILTER_LIST_STATEFUL        2407
#define OID_GLB_FILTER_LIST_DPI_ENABLED     2408

//---------------------------------------------------------
// Management port objects
//---------------------------------------------------------
#define OID_MGMT_TELNET_PORT                2409
#define OID_MGMT_SSH_PORT                   2410
#define OID_MGMT_HTTPS_PORT                 2411

//---------------------------------------------------------
// Rogue Auto-block objects
//---------------------------------------------------------
#define OID_IDS_AUTOBLOCK_RSSI              2412
#define OID_IDS_AUTOBLOCK_ENC               2413

//---------------------------------------------------------
// Ethernet extended objects [5x6]: (2414-2418) - (2439-2443)
//---------------------------------------------------------
#define NUM_ETH_EXT_OIDS                    5
#define NUM_ETH_EXT_OBJS                    6

#define  OFFSET_ETH_EXT(       _x_ )        OFFSET_OBJ( _x_, ETH_EXT, OID_ETH_LED(0))
#define _OID_IS_ETH_EXT(       _x_ )        OID_IS_OBJ( _x_, ETH_EXT, OID_ETH_LED(0))
#define  OID_ETH_EXT(          _x_ )        OID_OBJ(    _x_, ETH_EXT, OID_ETH_LED(0))
#define  IDX_ETH_EXT(          _x_ )        IDX_OBJ(    _x_, ETH_EXT, OID_ETH_LED(0))

#define  OFFSET_BOND(          _x_ )        OFFSET_OBJ( _x_, ETH_EXT, OID_ETH_LED(0))
#define _OID_IS_BOND(          _x_ )        OID_IS_OBJ( _x_, ETH_EXT, OID_ETH_LED(0))
#define  OID_BOND(             _x_ )        OID_OBJ(    _x_, ETH_EXT, OID_ETH_LED(0))
#define  IDX_BOND(             _x_ )        IDX_OBJ(    _x_, ETH_EXT, OID_ETH_LED(0))

#define OID_ETH_LED(          _idx_)        (2414 + OFFSET_ETH_EXT(_idx_))
#define OID_BOND_MODE(        _idx_)        (2415 + OFFSET_ETH_EXT(_idx_))
#define OID_BOND_ACT_VLAN(    _idx_)        (2416 + OFFSET_ETH_EXT(_idx_))
#define OID_BOND_MIRROR(      _idx_)        (2417 + OFFSET_ETH_EXT(_idx_))
#define OID_ETH_BOND_INDEX(   _idx_)        (2418 + OFFSET_ETH_EXT(_idx_))

#define OID_IS_ETH_LED(        _x_ )        (_OID_IS_ETH_EXT(_x_) &&  OID_ETH_EXT(_x_) == OID_ETH_LED(       0))
#define OID_IS_ETH_BOND_INDEX( _x_ )        (_OID_IS_ETH_EXT(_x_) &&  OID_ETH_EXT(_x_) == OID_ETH_BOND_INDEX(0))
#define OID_IS_ETH_EXT(        _x_ )        (_OID_IS_ETH_EXT(_x_) && (OID_ETH_EXT(_x_) == OID_ETH_LED(       0)  \
                                                                  ||  OID_ETH_EXT(_x_) == OID_ETH_BOND_INDEX(0)))

#define OID_IS_BOND_MODE(      _x_ )        (_OID_IS_BOND(_x_)    &&  OID_BOND(_x_)    == OID_BOND_MODE(     0))
#define OID_IS_BOND_ACT_VLAN(  _x_ )        (_OID_IS_BOND(_x_)    &&  OID_BOND(_x_)    == OID_BOND_ACT_VLAN( 0))
#define OID_IS_BOND_MIRROR(    _x_ )        (_OID_IS_BOND(_x_)    &&  OID_BOND(_x_)    == OID_BOND_MIRROR(   0))
#define OID_IS_BOND(           _x_ )        (_OID_IS_BOND(_x_)    && (OID_BOND(_x_)    == OID_BOND_MODE(     0)  \
                                                                  ||  OID_BOND(_x_)    == OID_BOND_ACT_VLAN( 0)  \
                                                                  ||  OID_BOND(_x_)    == OID_BOND_MIRROR(   0)))
//---------------------------------------------------------
// NetFlow [1x4]: (2444 - 2447)
//---------------------------------------------------------
#define OID_NETFLOW_ENABLE                  2444    // 0=off,  1=on
#define OID_NETFLOW_HOST1                   2445    // NetFlow collector. We may eventually support multiple collectors.
#define OID_NETFLOW_PORT1                   2446    // Port to send NetFlow data to

//---------------------------------------------------------
// Another misc Management
//---------------------------------------------------------
#define OID_MGMT_MAX_AUTH_ATTEMPTS          2447

//---------------------------------------------------------
// More NTP settings
//---------------------------------------------------------
#define OID_NTP_SERVER1_AUTH_TYPE           2448
#define OID_NTP_SERVER1_KEY_ID              2449
#define OID_NTP_SERVER1_KEY                 2450
#define OID_NTP_SERVER2_AUTH_TYPE           2451
#define OID_NTP_SERVER2_KEY_ID              2452
#define OID_NTP_SERVER2_KEY                 2453

//---------------------------------------------------------
// Global IAP multicast mode
//---------------------------------------------------------
#define OID_IAP_GLB_MULTICAST_MODE          2454

//---------------------------------------------------------
// Even more Autocell properties
//---------------------------------------------------------
#define OID_IAP_GLB_A_AUTO_CELL_PERIOD      2455
#define OID_IAP_GLB_G_AUTO_CELL_PERIOD      2456
#define OID_IAP_GLB_A_AUTO_CELL_OVERLAP     2457
#define OID_IAP_GLB_G_AUTO_CELL_OVERLAP     2458
#define OID_IAP_GLB_A_AUTO_CELL_MIN_TX      2459
#define OID_IAP_GLB_G_AUTO_CELL_MIN_TX      2460

//---------------------------------------------------------
// Tertiary Syslog Server
//---------------------------------------------------------
#define OID_SYSLOG_SERVER_3RD               2461    // IP address of tertiary syslog server
#define OID_SYSLOG_SERVER_3RD_LEVEL         2462    // Syslog level for tertiary syslog server

//---------------------------------------------------------
// Admin Radius Server [1x8]: (2463 - 2470)
//---------------------------------------------------------
#define NUM_ADMIN_RADIUS_OIDS               8
#define NUM_ADMIN_RADIUS_OBJS               1

#define OID_IS_ADMIN_RADIUS( _x_ )          OID_IS_OBJ( _x_, ADMIN_RADIUS, OID_ADMIN_RADIUS_ENABLED )
#define OID_ADMIN_RADIUS(    _x_ )          OID_OBJ(    _x_, ADMIN_RADIUS, OID_ADMIN_RADIUS_ENABLED )
#define IDX_ADMIN_RADIUS(    _x_ )          IDX_OBJ(    _x_, ADMIN_RADIUS, OID_ADMIN_RADIUS_ENABLED )

#define OID_ADMIN_RADIUS_ENABLED            2463
#define OID_ADMIN_RADIUS_PRI_PORT           2464
#define OID_ADMIN_RADIUS_PRI_SECRET         2465
#define OID_ADMIN_RADIUS_PRI_SERVER         2466
#define OID_ADMIN_RADIUS_SEC_PORT           2467
#define OID_ADMIN_RADIUS_SEC_SECRET         2468
#define OID_ADMIN_RADIUS_SEC_SERVER         2469
#define OID_ADMIN_RADIUS_TIMEOUT            2470

//---------------------------------------------------------
// SNMPv3 settings
//---------------------------------------------------------
#define OID_SNMP_V3_ENABLED                 2471
#define OID_SNMP_V3_AUTH_TYPE               2472
#define OID_SNMP_V3_PRIV_TYPE               2473
#define OID_SNMP_V3_RW_USER_NAME            2474
#define OID_SNMP_V3_RW_AUTH_PASS            2475
#define OID_SNMP_V3_RW_PRIV_PASS            2476
#define OID_SNMP_V3_RO_USER_NAME            2477
#define OID_SNMP_V3_RO_AUTH_PASS            2478
#define OID_SNMP_V3_RO_PRIV_PASS            2479
#define OID_SNMP_V3_ENGINE_ID               2480
#define OID_SNMP_V3_SPARE_0                 2481
#define OID_SNMP_V3_SPARE_1                 2482
#define OID_SNMP_V3_SPARE_2                 2483
#define OID_SNMP_V3_SPARE_3                 2484

#define OID_IS_SNMP_IGNORE(_x_)             ((_x_) == OID_SNMP_V3_ENGINE_ID)

//---------------------------------------------------------
// More SNMP trap settings
//---------------------------------------------------------
#define OID_SNMP_TRAP_KEEPALIVE             2485
#define OID_SNMP_TRAP_SPARE_0               2486
#define OID_SNMP_TRAP_SPARE_1               2487

//---------------------------------------------------------
// Misc. Management (and friends) settings
//---------------------------------------------------------
#define OID_MGMT_REAUTH_PERIOD              2488
#define OID_MGMT_PCI_AUDIT_ENABLED          2489
#define OID_ADMIN_RADIUS_AUTH_TYPE          2490
#define OID_PER_SSID_CIPHER_ENABLE          2491
#define OID_DNS_USE_DHCP                    2492

//---------------------------------------------------------
// wifi_tag settings
//---------------------------------------------------------
#define OID_WIFI_TAG_ENABLED                2493
#define OID_WIFI_TAG_UDP_PORT               2494
#define OID_WIFI_TAG_CHANNEL_1              2495
#define OID_WIFI_TAG_CHANNEL_2              2496
#define OID_WIFI_TAG_CHANNEL_3              2497
#define OID_WIFI_TAG_SERVER                 2498

//---------------------------------------------------------
// global IAP DSCP settings
//---------------------------------------------------------
#define OID_IAP_GLB_DSCP_MODE               2499
#define OID_IAP_GLB_DSCP_MAP                2500

//---------------------------------------------------------
// Privilege level section settings [1x38]: (2501 - 2538)
//---------------------------------------------------------
#define NUM_PRIV_SECTION_OIDS               38
#define NUM_PRIV_SECTION_OBJS               1

#define OID_IS_PRIV_SECTION( _x_ )          OID_IS_OBJ( _x_, PRIV_SECTION, OID_PRIV_DESCRIPTION )
#define OID_PRIV_SECTION(    _x_ )          OID_OBJ(    _x_, PRIV_SECTION, OID_PRIV_DESCRIPTION )
#define IDX_PRIV_SECTION(    _x_ )          IDX_OBJ(    _x_, PRIV_SECTION, OID_PRIV_DESCRIPTION )

#define OID_PRIV_DESCRIPTION                2501
#define OID_PRIV_CONTACT_INFO               2502
#define OID_PRIV_FILE                       2503
#define OID_PRIV_BOOT_ENV                   2504
#define OID_PRIV_ADMIN                      2505
#define OID_PRIV_CONSOLE                    2506
#define OID_PRIV_IAP                        2507
#define OID_PRIV_ETH0                       2508
#define OID_PRIV_GIG                        2509
#define OID_PRIV_STA_ASSURE                 2510
#define OID_PRIV_VLAN                       2511
#define OID_PRIV_DNS                        2512
#define OID_PRIV_MANAGEMENT                 2513
#define OID_PRIV_CDP                        2514
#define OID_PRIV_NETFLOW                    2515
#define OID_PRIV_SYSLOG                     2516
#define OID_PRIV_DATE_TIME                  2517
#define OID_PRIV_SNMP                       2518
#define OID_PRIV_DHCP_SERVER                2519
#define OID_PRIV_SECURITY                   2520
#define OID_PRIV_SSID                       2521
#define OID_PRIV_GROUP                      2522
#define OID_PRIV_RADIUS_SERVER              2523
#define OID_PRIV_ACL                        2524
#define OID_PRIV_FILTER                     2525
#define OID_PRIV_CLUSTER                    2526
#define OID_PRIV_WIFI_TAG                   2527
#define OID_PRIV_RUN_TESTS                  2528
#define OID_PRIV_REBOOT                     2529
#define OID_PRIV_RESET                      2530
#define OID_PRIV_ROAM_ASSIST                2531
#define OID_PRIV_TUNNEL                     2532
#define OID_PRIV_LOCATION                   2533
#define OID_PRIV_MDM                        2534
#define OID_PRIV_OAUTH                      2535
#define OID_PRIV_PROXY_FWD                  2536
#define OID_PRIV_LLDP                       2537
#define OID_PRIV_PROXY_MGMT                 2538

//---------------------------------------------------------
// Privilege level names [8x1]: 2539 - 2546
//---------------------------------------------------------
#define NUM_PRIV_NAME_OIDS                  1
#define NUM_PRIV_NAME_OBJS                  8

#define OFFSET_PRIV_NAME( _x_ )             OFFSET_OBJ( _x_, PRIV_NAME, OID_PRIV_LEVEL_NAME(0) )
#define OID_IS_PRIV_NAME( _x_ )             OID_IS_OBJ( _x_, PRIV_NAME, OID_PRIV_LEVEL_NAME(0) )
#define OID_PRIV_NAME(    _x_ )             OID_OBJ(    _x_, PRIV_NAME, OID_PRIV_LEVEL_NAME(0) )
#define IDX_PRIV_NAME(    _x_ )             IDX_OBJ(    _x_, PRIV_NAME, OID_PRIV_LEVEL_NAME(0) )

#define OID_PRIV_LEVEL_NAME(_idx_)          (2539 + OFFSET_PRIV_NAME(_idx_) )

//---------------------------------------------------------
// SSID Web Page Redirect Customization [16x8]: (2547-2554) - (2667-2674)
//---------------------------------------------------------
#define NUM_SSID_WPR_CUSTOM_OIDS            8
#define NUM_SSID_WPR_CUSTOM_OBJS            NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_WPR_CUSTOM( _x_ )       OFFSET_OBJ( _x_, SSID_WPR_CUSTOM, OID_SSID_WPR_BACKGROUND(0) )
#define OID_IS_SSID_WPR_CUSTOM( _x_ )       OID_IS_OBJ( _x_, SSID_WPR_CUSTOM, OID_SSID_WPR_BACKGROUND(0) )
#define OID_SSID_WPR_CUSTOM(    _x_ )       OID_OBJ(    _x_, SSID_WPR_CUSTOM, OID_SSID_WPR_BACKGROUND(0) )
#define IDX_SSID_WPR_CUSTOM(    _x_ )       IDX_OBJ(    _x_, SSID_WPR_CUSTOM, OID_SSID_WPR_BACKGROUND(0) )

#define OID_SSID_WPR_BACKGROUND( _idx_)     (2547 + OFFSET_SSID_WPR_CUSTOM(_idx_))
#define OID_SSID_WPR_LOGO_IMAGE( _idx_)     (2548 + OFFSET_SSID_WPR_CUSTOM(_idx_))
#define OID_SSID_WPR_HEADER_TEXT(_idx_)     (2549 + OFFSET_SSID_WPR_CUSTOM(_idx_))
#define OID_SSID_WPR_FOOTER_TEXT(_idx_)     (2550 + OFFSET_SSID_WPR_CUSTOM(_idx_))
#define OID_SSID_WPR_CUSTOM_1(   _idx_)     (2551 + OFFSET_SSID_WPR_CUSTOM(_idx_))
#define OID_SSID_WPR_CUSTOM_2(   _idx_)     (2552 + OFFSET_SSID_WPR_CUSTOM(_idx_))
#define OID_SSID_WPR_CUSTOM_3(   _idx_)     (2553 + OFFSET_SSID_WPR_CUSTOM(_idx_))
#define OID_SSID_WPR_CUSTOM_4(   _idx_)     (2554 + OFFSET_SSID_WPR_CUSTOM(_idx_))

//---------------------------------------------------------
// User Group Web Page Redirect Customization [16x8]: (2675-2682) - (2795-2802)
//---------------------------------------------------------
#define NUM_USER_GROUP_WPR_CUSTOM_OIDS      8
#define NUM_USER_GROUP_WPR_CUSTOM_OBJS      NUM_USER_GROUP_OBJS

#define OFFSET_USER_GROUP_WPR_CUSTOM( _x_ ) OFFSET_OBJ( _x_, USER_GROUP_WPR_CUSTOM, OID_GROUP_WPR_BACKGROUND(0) )
#define OID_IS_USER_GROUP_WPR_CUSTOM( _x_ ) OID_IS_OBJ( _x_, USER_GROUP_WPR_CUSTOM, OID_GROUP_WPR_BACKGROUND(0) )
#define OID_USER_GROUP_WPR_CUSTOM(    _x_ ) OID_OBJ(    _x_, USER_GROUP_WPR_CUSTOM, OID_GROUP_WPR_BACKGROUND(0) )
#define IDX_USER_GROUP_WPR_CUSTOM(    _x_ ) IDX_OBJ(    _x_, USER_GROUP_WPR_CUSTOM, OID_GROUP_WPR_BACKGROUND(0) )

#define OID_GROUP_WPR_BACKGROUND( _idx_)    (2675 + OFFSET_USER_GROUP_WPR_CUSTOM(_idx_))
#define OID_GROUP_WPR_LOGO_IMAGE( _idx_)    (2676 + OFFSET_USER_GROUP_WPR_CUSTOM(_idx_))
#define OID_GROUP_WPR_HEADER_TEXT(_idx_)    (2677 + OFFSET_USER_GROUP_WPR_CUSTOM(_idx_))
#define OID_GROUP_WPR_FOOTER_TEXT(_idx_)    (2678 + OFFSET_USER_GROUP_WPR_CUSTOM(_idx_))
#define OID_GROUP_WPR_CUSTOM_1(   _idx_)    (2679 + OFFSET_USER_GROUP_WPR_CUSTOM(_idx_))
#define OID_GROUP_WPR_CUSTOM_2(   _idx_)    (2680 + OFFSET_USER_GROUP_WPR_CUSTOM(_idx_))
#define OID_GROUP_WPR_CUSTOM_3(   _idx_)    (2681 + OFFSET_USER_GROUP_WPR_CUSTOM(_idx_))
#define OID_GROUP_WPR_CUSTOM_4(   _idx_)    (2682 + OFFSET_USER_GROUP_WPR_CUSTOM(_idx_))

//---------------------------------------------------------
// VLAN settings [16x3]: (2803-2805) - (2848-2850) (for network assurance)
// ---------------------------------------------------------
#define NUM_VLAN1_OIDS                      3
#define NUM_VLAN1_OBJS                      16

#define OFFSET_VLAN1( _x_ )                 OFFSET_OBJ( _x_, VLAN1, OID_VLAN1_NAME(0) )
#define OID_IS_VLAN1( _x_ )                 OID_IS_OBJ( _x_, VLAN1, OID_VLAN1_NAME(0) )
#define OID_VLAN1(    _x_ )                 OID_OBJ(    _x_, VLAN1, OID_VLAN1_NAME(0) )
#define IDX_VLAN1(    _x_ )                 IDX_OBJ(    _x_, VLAN1, OID_VLAN1_NAME(0) )

#define OID_VLAN1_NAME(         _idx_)      (2803 + OFFSET_VLAN1(_idx_))
#define OID_VLAN1_GATEWAY(      _idx_)      (2804 + OFFSET_VLAN1(_idx_))
#define OID_VLAN1_TUNNEL_SERVER(_idx_)      (2805 + OFFSET_VLAN1(_idx_))

//---------------------------------------------------------
// Network Assurance settings [1x5]: (2851-2855)
// ---------------------------------------------------------
#define NUM_NET_ASSURANCE_OIDS              5
#define NUM_NET_ASSURANCE_OBJS              1

#define OID_IS_NET_ASSURANCE( _x_ )         OID_IS_OBJ( _x_, NET_ASSURANCE, OID_NET_ASSURANCE_ENABLE )
#define OID_NET_ASSURANCE(    _x_ )         OID_OBJ(    _x_, NET_ASSURANCE, OID_NET_ASSURANCE_ENABLE )
#define IDX_NET_ASSURANCE(    _x_ )         IDX_OBJ(    _x_, NET_ASSURANCE, OID_NET_ASSURANCE_ENABLE )

#define OID_NET_ASSURANCE_ENABLE            2851
#define OID_NET_ASSURANCE_PERIOD            2852
#define OID_NET_ASSURANCE_SPARE_1           2853
#define OID_NET_ASSURANCE_SPARE_2           2854
#define OID_NET_ASSURANCE_SPARE_3           2855

//---------------------------------------------------------
// Station Assurance settings [1x12]: (2856-2867)
// ---------------------------------------------------------
#define NUM_STA_ASSURANCE_OIDS              12
#define NUM_STA_ASSURANCE_OBJS              1

#define OID_IS_STA_ASSURANCE( _x_ )         OID_IS_OBJ( _x_, STA_ASSURANCE, OID_STA_ASSURANCE_ENABLE )
#define OID_STA_ASSURANCE(    _x_ )         OID_OBJ(    _x_, STA_ASSURANCE, OID_STA_ASSURANCE_ENABLE )
#define IDX_STA_ASSURANCE(    _x_ )         IDX_OBJ(    _x_, STA_ASSURANCE, OID_STA_ASSURANCE_ENABLE )

#define OID_STA_ASSURANCE_ENABLE            2856
#define OID_STA_ASSURANCE_PERIOD            2857
#define OID_STA_ASSURANCE_ASSOC_TIME        2858
#define OID_STA_ASSURANCE_ERROR_RATE        2859
#define OID_STA_ASSURANCE_RETRY_RATE        2860
#define OID_STA_ASSURANCE_DATA_RATE         2861
#define OID_STA_ASSURANCE_RSSI              2862
#define OID_STA_ASSURANCE_SNR               2863
#define OID_STA_ASSURANCE_DISTANCE          2864
#define OID_STA_ASSURANCE_AUTH_FAILURES     2865
#define OID_STA_ASSURANCE_SPARE_1           2866

//---------------------------------------------------------
// More misc. global IAP configuration info
//---------------------------------------------------------
#define OID_IAP_GLB_WMM_ACM_CAC_MAX_BW      2867
#define OID_IAP_GLB_WMM_POWER_SAVE          2868

//---------------------------------------------------------
// Roaming Assist  settings [1x6]: (2869-2874)
// ---------------------------------------------------------
#define NUM_ROAMING_ASSIST_OIDS             6
#define NUM_ROAMING_ASSIST_OBJS             1

#define OID_IS_ROAMING_ASSIST( _x_ )        OID_IS_OBJ( _x_, ROAMING_ASSIST, OID_ROAMING_ASSIST_ENABLE )
#define OID_ROAMING_ASSIST(    _x_ )        OID_OBJ(    _x_, ROAMING_ASSIST, OID_ROAMING_ASSIST_ENABLE )
#define IDX_ROAMING_ASSIST(    _x_ )        IDX_OBJ(    _x_, ROAMING_ASSIST, OID_ROAMING_ASSIST_ENABLE )

#define OID_ROAMING_ASSIST_ENABLE           2869
#define OID_ROAMING_ASSIST_PERIOD           2870
#define OID_ROAMING_ASSIST_THRESHOLD        2871
#define OID_ROAMING_ASSIST_DATA_RATE        2872
#define OID_ROAMING_ASSIST_DEVICES          2873
#define OID_ROAMING_ASSIST_SPARE            2874

//---------------------------------------------------------
// More Rogue Auto-block objects
//---------------------------------------------------------
#define OID_IDS_AUTOBLOCK_TYPE              2875
#define OID_IDS_AUTOBLOCK_SPARE_1           2876
#define OID_IDS_AUTOBLOCK_SPARE_2           2877
#define OID_IDS_AUTOBLOCK_SPARE_3           2878
#define OID_IDS_AUTOBLOCK_SPARE_4           2879

//---------------------------------------------------------
// New Ethernet objects [4x10]: (2880 - 2889) - (2910 - 2919)
//---------------------------------------------------------
#define NUM_NEW_ETH_OIDS                    10
#define NUM_NEW_ETH_OBJS                    4

#define OFFSET_NEW_ETH( _x_ )               OFFSET_OBJ( _x_, NEW_ETH, OID_NEW_ETH_ALLOW_MGMT(0))
#define OID_IS_NEW_ETH( _x_ )               OID_IS_OBJ( _x_, NEW_ETH, OID_NEW_ETH_ALLOW_MGMT(0))
#define OID_NEW_ETH(    _x_ )               OID_OBJ(    _x_, NEW_ETH, OID_NEW_ETH_ALLOW_MGMT(0))
#define IDX_NEW_ETH(    _x_ )               IDX_OBJ(    _x_, NEW_ETH, OID_NEW_ETH_ALLOW_MGMT(0))

#define OID_NEW_ETH_ALLOW_MGMT(  _idx_)     (2880 + OFFSET_NEW_ETH(_idx_))
#define OID_NEW_ETH_AUTONEG(     _idx_)     (2881 + OFFSET_NEW_ETH(_idx_))
#define OID_NEW_ETH_IPV4_DHCP(   _idx_)     (2882 + OFFSET_NEW_ETH(_idx_))
#define OID_NEW_ETH_DUPLEX(      _idx_)     (2883 + OFFSET_NEW_ETH(_idx_))
#define OID_NEW_ETH_ENABLED(     _idx_)     (2884 + OFFSET_NEW_ETH(_idx_))
#define OID_NEW_ETH_IPV4_GATEWAY(_idx_)     (2885 + OFFSET_NEW_ETH(_idx_))
#define OID_NEW_ETH_IPV4_ADDR(   _idx_)     (2886 + OFFSET_NEW_ETH(_idx_))
#define OID_NEW_ETH_IPV4_MASK(   _idx_)     (2887 + OFFSET_NEW_ETH(_idx_))
#define OID_NEW_ETH_MTU(         _idx_)     (2888 + OFFSET_NEW_ETH(_idx_))
#define OID_NEW_ETH_SPEED(       _idx_)     (2889 + OFFSET_NEW_ETH(_idx_))

//---------------------------------------------------------
// Universal Ethernet objects
//---------------------------------------------------------
#define NUM_ETH_OIDS                        10
#define NUM_ETH_OBJS                        7

#define OID_IS_ETH( _x_ )                   (OID_IS_ORG_ETH( _x_ ) || OID_IS_NEW_ETH( _x_ ))
#define OID_ETH(    _x_ )                   (OID_IS_ORG_ETH( _x_ ) ?  OID_ORG_ETH( _x_ ) : OID_NEW_ETH( _x_ ) - OID_NEW_ETH_ALLOW_MGMT(0) + OID_ORG_ETH_ALLOW_MGMT(0))
#define IDX_ETH(    _x_ )                   (OID_IS_ORG_ETH( _x_ ) ?  IDX_ORG_ETH( _x_ ) : IDX_NEW_ETH( _x_ ) + NUM_ORG_ETH_OBJS)

#define OID_ETH_ALLOW_MGMT(  _idx_)         (((_idx_) < NUM_ORG_ETH_OBJS) ? OID_ORG_ETH_ALLOW_MGMT(  _idx_) : OID_NEW_ETH_ALLOW_MGMT(  _idx_ - NUM_ORG_ETH_OBJS))
#define OID_ETH_AUTONEG(     _idx_)         (((_idx_) < NUM_ORG_ETH_OBJS) ? OID_ORG_ETH_AUTONEG(     _idx_) : OID_NEW_ETH_AUTONEG(     _idx_ - NUM_ORG_ETH_OBJS))
#define OID_ETH_IPV4_DHCP(   _idx_)         (((_idx_) < NUM_ORG_ETH_OBJS) ? OID_ORG_ETH_IPV4_DHCP(   _idx_) : OID_NEW_ETH_IPV4_DHCP(   _idx_ - NUM_ORG_ETH_OBJS))
#define OID_ETH_DUPLEX(      _idx_)         (((_idx_) < NUM_ORG_ETH_OBJS) ? OID_ORG_ETH_DUPLEX(      _idx_) : OID_NEW_ETH_DUPLEX(      _idx_ - NUM_ORG_ETH_OBJS))
#define OID_ETH_ENABLED(     _idx_)         (((_idx_) < NUM_ORG_ETH_OBJS) ? OID_ORG_ETH_ENABLED(     _idx_) : OID_NEW_ETH_ENABLED(     _idx_ - NUM_ORG_ETH_OBJS))
#define OID_ETH_IPV4_GATEWAY(_idx_)         (((_idx_) < NUM_ORG_ETH_OBJS) ? OID_ORG_ETH_IPV4_GATEWAY(_idx_) : OID_NEW_ETH_IPV4_GATEWAY(_idx_ - NUM_ORG_ETH_OBJS))
#define OID_ETH_IPV4_ADDR(   _idx_)         (((_idx_) < NUM_ORG_ETH_OBJS) ? OID_ORG_ETH_IPV4_ADDR(   _idx_) : OID_NEW_ETH_IPV4_ADDR(   _idx_ - NUM_ORG_ETH_OBJS))
#define OID_ETH_IPV4_MASK(   _idx_)         (((_idx_) < NUM_ORG_ETH_OBJS) ? OID_ORG_ETH_IPV4_MASK(   _idx_) : OID_NEW_ETH_IPV4_MASK(   _idx_ - NUM_ORG_ETH_OBJS))
#define OID_ETH_MTU(         _idx_)         (((_idx_) < NUM_ORG_ETH_OBJS) ? OID_ORG_ETH_MTU(         _idx_) : OID_NEW_ETH_MTU(         _idx_ - NUM_ORG_ETH_OBJS))
#define OID_ETH_SPEED(       _idx_)         (((_idx_) < NUM_ORG_ETH_OBJS) ? OID_ORG_ETH_SPEED(       _idx_) : OID_NEW_ETH_SPEED(       _idx_ - NUM_ORG_ETH_OBJS))

//---------------------------------------------------------
// DEPRECATED: External IAP (XAP) properties [4x20]: (2920-2939) - (2980-2999)
//---------------------------------------------------------

//---------------------------------------------------------
// Cluster properties  [1x10]: (3000-3009)
//---------------------------------------------------------
#define NUM_CLUST_OIDS                      10
#define NUM_CLUST_OBJS                      1

#define OFFSET_CLUSTER( _x_ )               OFFSET_OBJ( _x_, CLUST, OID_CLUST_ENABLED )
#define OID_IS_CLUSTER( _x_ )               OID_IS_OBJ( _x_, CLUST, OID_CLUST_ENABLED )
#define OID_CLUSTER(    _x_ )               OID_OBJ(    _x_, CLUST, OID_CLUST_ENABLED )
#define IDX_CLUSTER(    _x_ )               IDX_OBJ(    _x_, CLUST, OID_CLUST_ENABLED )

#define OID_CLUST_ENABLED                   3000    // Enable/disable a cluster
#define OID_CLUST_NAME                      3001    // Add/del a cluster
#define OID_CLUST_ARRAY                     3002    // Enlist/delist an array in a cluster
#define OID_CLUST_SPARE_1                   3003
#define OID_CLUST_SPARE_2                   3004
#define OID_CLUST_SPARE_3                   3005
#define OID_CLUST_SPARE_4                   3006
#define OID_CLUST_SPARE_5                   3007
#define OID_CLUST_SPARE_6                   3008
#define OID_CLUST_SPARE_7                   3009

//--------------------------------------------------------------
// IDS settings (DoS attacks) [20x5]: (3010-3014) - (3105-3109)
// -------------------------------------------------------------
#define NUM_IDS_DOS_ATTACK_OIDS             5
#define NUM_IDS_DOS_ATTACK_OBJS             20

#define OFFSET_IDS_DOS_ATTACK( _x_ )        OFFSET_OBJ( _x_, IDS_DOS_ATTACK, OID_IDS_DOS_ATTACK_MODE(0) )
#define OID_IS_IDS_DOS_ATTACK( _x_ )        OID_IS_OBJ( _x_, IDS_DOS_ATTACK, OID_IDS_DOS_ATTACK_MODE(0) )
#define OID_IDS_DOS_ATTACK(    _x_ )        OID_OBJ(    _x_, IDS_DOS_ATTACK, OID_IDS_DOS_ATTACK_MODE(0) )
#define IDX_IDS_DOS_ATTACK(    _x_ )        IDX_OBJ(    _x_, IDS_DOS_ATTACK, OID_IDS_DOS_ATTACK_MODE(0) )

#define OID_IDS_DOS_ATTACK_MODE(     _idx_) (3010 + OFFSET_IDS_DOS_ATTACK(_idx_))
#define OID_IDS_DOS_ATTACK_THRESHOLD(_idx_) (3011 + OFFSET_IDS_DOS_ATTACK(_idx_))
#define OID_IDS_DOS_ATTACK_PERIOD(   _idx_) (3012 + OFFSET_IDS_DOS_ATTACK(_idx_))
#define OID_IDS_DOS_ATTACK_SPARE_1(  _idx_) (3013 + OFFSET_IDS_DOS_ATTACK(_idx_))
#define OID_IDS_DOS_ATTACK_SPARE_2(  _idx_) (3014 + OFFSET_IDS_DOS_ATTACK(_idx_))

//--------------------------------------------------------------
// IDS settings (Misc attacks) [1x40]: (3110-3149)
// -------------------------------------------------------------
#define NUM_IDS_ATTACK_OIDS                 40
#define NUM_IDS_ATTACK_OBJS                 1

#define OID_IS_IDS_ATTACK( _x_ )            OID_IS_OBJ( _x_, IDS_ATTACK, OID_IDS_ATTACK_DUR_ENABLE )
#define OID_IDS_ATTACK(    _x_ )            OID_OBJ(    _x_, IDS_ATTACK, OID_IDS_ATTACK_DUR_ENABLE )
#define IDX_IDS_ATTACK(    _x_ )            IDX_OBJ(    _x_, IDS_ATTACK, OID_IDS_ATTACK_DUR_ENABLE )

#define OID_IDS_ATTACK_DUR_ENABLE           3110
#define OID_IDS_ATTACK_DUR_THRESHOLD        3111
#define OID_IDS_ATTACK_DUR_PERIOD           3112
#define OID_IDS_ATTACK_DUR_NAV              3113
#define OID_IDS_ATTACK_DUR_SPARE_1          3114
#define OID_IDS_ATTACK_DUR_SPARE_2          3115
#define OID_IDS_ATTACK_SEQ_ANOMALY_MODE     3116
#define OID_IDS_ATTACK_SEQ_ANOMALY_GAP      3117
#define OID_IDS_ATTACK_SEQ_ANOMALY_SPARE_2  3118
#define OID_IDS_ATTACK_SEQ_ANOMALY_SPARE_3  3119
#define OID_IDS_ATTACK_SEQ_ANOMALY_SPARE_4  3120
#define OID_IDS_ATTACK_STA_IMP_ENABLE       3121
#define OID_IDS_ATTACK_STA_IMP_THRESHOLD    3122
#define OID_IDS_ATTACK_STA_IMP_PERIOD       3123
#define OID_IDS_ATTACK_STA_IMP_SPARE_1      3124
#define OID_IDS_ATTACK_STA_IMP_SPARE_2      3125
#define OID_IDS_ATTACK_STA_IMP_SPARE_3      3126
#define OID_IDS_ATTACK_STA_IMP_SPARE_4      3127
#define OID_IDS_ATTACK_EVIL_TWIN_ENABLE     3128
#define OID_IDS_ATTACK_EVIL_TWIN_SPARE_1    3129
#define OID_IDS_ATTACK_EVIL_TWIN_SPARE_2    3130
#define OID_IDS_ATTACK_EVIL_TWIN_SPARE_3    3131
#define OID_IDS_ATTACK_EVIL_TWIN_SPARE_4    3132
#define OID_IDS_ATTACK_SPARE_1              3133
#define OID_IDS_ATTACK_SPARE_2              3134
#define OID_IDS_ATTACK_SPARE_3              3135
#define OID_IDS_ATTACK_SPARE_4              3136
#define OID_IDS_ATTACK_SPARE_5              3137
#define OID_IDS_ATTACK_SPARE_6              3138
#define OID_IDS_ATTACK_SPARE_7              3139
#define OID_IDS_ATTACK_SPARE_8              3140
#define OID_IDS_ATTACK_SPARE_9              3141
#define OID_IDS_ATTACK_SPARE_10             3142
#define OID_IDS_ATTACK_SPARE_11             3143
#define OID_IDS_ATTACK_SPARE_12             3144
#define OID_IDS_ATTACK_SPARE_13             3145
#define OID_IDS_ATTACK_SPARE_14             3146
#define OID_IDS_ATTACK_SPARE_15             3147
#define OID_IDS_ATTACK_SPARE_16             3148
#define OID_IDS_ATTACK_SPARE_17             3149

//---------------------------------------------------------
// Extended VLAN settings [16x3]: (3150-3152) - (3195-3197) (for network assurance)
// ---------------------------------------------------------
#define NUM_VLAN2_OIDS                      3
#define NUM_VLAN2_OBJS                      16

#define OFFSET_VLAN2( _x_ )                 OFFSET_OBJ( _x_, VLAN2, OID_VLAN2_NAME(0) )
#define OID_IS_VLAN2( _x_ )                 OID_IS_OBJ( _x_, VLAN2, OID_VLAN2_NAME(0) )
#define OID_VLAN2(    _x_ )                 OID_OBJ(    _x_, VLAN2, OID_VLAN2_NAME(0) )
#define IDX_VLAN2(    _x_ )                 IDX_OBJ(    _x_, VLAN2, OID_VLAN2_NAME(0) )

#define OID_VLAN2_NAME(         _idx_)      (3150 + OFFSET_VLAN2(_idx_))
#define OID_VLAN2_GATEWAY(      _idx_)      (3151 + OFFSET_VLAN2(_idx_))
#define OID_VLAN2_TUNNEL_SERVER(_idx_)      (3152 + OFFSET_VLAN2(_idx_))

//---------------------------------------------------------
// More WDS settings
//---------------------------------------------------------
//#define OID_WDS_STP_ENABLE                3198    // Deprecated
#define OID_WDS_ROAM_THRESHOLD              3199
#define OID_WDS_ROAM_AVG_WEIGHT             3200
#define OID_WDS_LOCK                        3201
#define OID_WDS_SPARE_1                     3202
#define OID_WDS_SPARE_2                     3203
#define OID_WDS_SPARE_3                     3204

//---------------------------------------------------------
// DEPRECATED: Virtual IAP (VAP) properties [12x20]: (3205-3224) - (3425-3444)
//---------------------------------------------------------

//---------------------------------------------------------
// More misc. global IAP configuration info
//---------------------------------------------------------
#define OID_IAP_GLB_WMM_ACM                 3445
#define OID_IAP_GLB_MAX_IAP_STATIONS        3446
#define OID_IAP_GLB_RFM_TS_SCAN_TIME        3447
#define OID_IAP_GLB_RFM_TS_STA_LIMIT        3448
#define OID_IAP_GLB_RFM_TS_PPS_LIMIT        3449
#define OID_IAP_GLB_IPV6_BLOCKING           3450
#define OID_IAP_GLB_A_MAX_ALL_STATIONS      3451
#define OID_IAP_GLB_G_MAX_ALL_STATIONS      3452
#define OID_IAP_GLB_A_MAX_IAP_STATIONS      3453
#define OID_IAP_GLB_G_MAX_IAP_STATIONS      3454
#define OID_IAP_GLB_DOT11_K_ENABLE          3455
#define OID_IAP_GLB_DOT11_W_MODE            3456
#define OID_IAP_GLB_A_AUTO_CELL_MAX_RX      3457
#define OID_IAP_GLB_G_AUTO_CELL_MAX_RX      3458
#define OID_IAP_GLB_AUTO_CELL_MAX_RX        3459

//---------------------------------------------------------
// More misc. manangement configuration info
//---------------------------------------------------------
#define OID_MGMT_XIRCON_ENABLED             3460
#define OID_MGMT_XIRCON_PORT                3461
#define OID_TIMEOUT_XIRCON                  3462
#define OID_MGMT_STP_ENABLED                3463
#define OID_MGMT_XMS_CONTROL                3464

//---------------------------------------------------------
// More station settings
//---------------------------------------------------------
#define OID_STA_AUTH_TIMEOUT                3465
#define OID_STA_SPARE_1                     3466
#define OID_STA_SPARE_2                     3467
#define OID_STA_SPARE_3                     3468
#define OID_STA_SPARE_4                     3469

//---------------------------------------------------------
// More syslog server info
//---------------------------------------------------------
#define OID_SYSLOG_SERVER_PRI_PORT          3470
#define OID_SYSLOG_SERVER_SEC_PORT          3471
#define OID_SYSLOG_SERVER_3RD_PORT          3472
#define OID_SYSLOG_STATION_FORMAT           3473
#define OID_SYSLOG_TIME_FORMAT              3474
#define OID_SYSLOG_STATION_URL_LOG          3475
#define OID_SYSLOG_SPARE_1                  3476
#define OID_SYSLOG_SPARE_2                  3477
#define OID_SYSLOG_SPARE_3                  3478
#define OID_SYSLOG_SPARE_4                  3479

//---------------------------------------------------------
// More Radius info
//---------------------------------------------------------
#define NUM_RADIUS_EXT_OIDS                 10
#define NUM_RADIUS_EXT_OBJS                 1

#define OID_IS_RADIUS_EXT( _x_ )            OID_IS_OBJ( _x_, RADIUS_EXT, OID_RADIUS_DAS_PORT )
#define OID_RADIUS_EXT(    _x_ )            OID_OBJ(    _x_, RADIUS_EXT, OID_RADIUS_DAS_PORT )
#define IDX_RADIUS_EXT(    _x_ )            IDX_OBJ(    _x_, RADIUS_EXT, OID_RADIUS_DAS_PORT )

#define OID_RADIUS_DAS_PORT                 3480
#define OID_RADIUS_DAS_TIME_WINDOW          3481
#define OID_RADIUS_DAS_EVENT_TIMESTAMP      3482
#define OID_RADIUS_CALLED_STA_ID_FORMAT     3483
#define OID_RADIUS_STA_MAC_FORMAT           3484
#define OID_RADIUS_FAILOVER_TIMEOUT         3485
#define OID_RADIUS_SPARE_1                  3486
#define OID_RADIUS_SPARE_2                  3487
#define OID_RADIUS_SPARE_3                  3488
#define OID_RADIUS_SPARE_4                  3489

//---------------------------------------------------------
// 802.11u settings
//---------------------------------------------------------
#define OID_11U_INTERWORKING                3490
#define OID_11U_ACCESS_NETWORK_OPTS         3491
#define OID_11U_VENUE_GROUP                 3492
#define OID_11U_VENUE_TYPE                  3493
#define OID_11U_HESSID                      3494
#define OID_11U_IPV4_AVAIL                  3495
#define OID_11U_IPV6_AVAIL                  3496
#define OID_11U_SPARE_1                     3497
#define OID_11U_SPARE_2                     3498
#define OID_11U_SPARE_3                     3499
#define OID_11U_SPARE_4                     3500
#define OID_11U_SPARE_5                     3501

//---------------------------------------------------------
// 802.11r settings
//---------------------------------------------------------
#define OID_IAP_GLB_DOT11_R_ENABLE          3502
#define OID_IAP_GLB_DOT11_R_SPARE_1         3503

//---------------------------------------------------------
// 802.11v settings
//---------------------------------------------------------
#define OID_IAP_GLB_DOT11_V_ENABLE          3504
#define OID_IAP_GLB_DOT11_V_SPARE_1         3505

//---------------------------------------------------------
// More misc. global IAP configuration info
//---------------------------------------------------------
#define OID_IAP_GLB_OUTDOOR_INSTALL         3506
#define OID_IAP_GLB_SPARE_1                 3507
#define OID_IAP_GLB_SPARE_2                 3508
#define OID_IAP_GLB_SPARE_3                 3509

//---------------------------------------------------------
// Hotspot 2.0 settings
//---------------------------------------------------------
#define OID_HS20_ENABLE                     3510
#define OID_HS20_DGAF                       3511
#define OID_HS20_WAN_LINK_STATUS            3512
#define OID_HS20_WAN_DOWNLINK_SPEED         3513
#define OID_HS20_WAN_UPLINK_SPEED           3514

//---------------------------------------------------------
// GPS position settings
//---------------------------------------------------------
#define OID_POSITION_GPS_ANGLE              3515
#define OID_POSITION_GPS_LATITUDE           3516
#define OID_POSITION_GPS_LONGITUDE          3517
#define OID_POSITION_GPS_ELEVATION          3518
#define OID_POSITION_GPS_REFERENCE          3519

#define OID_IS_POSITION_GPS(_x_)            ((_x_) == OID_POSITION_GPS_ANGLE || (_x_) == OID_POSITION_GPS_LATITUDE || (_x_) == OID_POSITION_GPS_LONGITUDE || (_x_) == OID_POSITION_GPS_ELEVATION)

//---------------------------------------------------------
// Position settings
//---------------------------------------------------------
#define OID_POSITION_X                      3520
#define OID_POSITION_Y                      3521
#define OID_POSITION_Z                      3522
#define OID_POSITION_SCALE                  3523
#define OID_POSITION_ANGLE                  3524
#define OID_POSITION_MOUNT                  3525
#define OID_POSITION_SCOPE                  3526
#define OID_POSITION_MAP                    3527

//---------------------------------------------------------
// Location server settings (for XPS & Euclid)
//---------------------------------------------------------
#define OID_LOCATION_MAX_DATA_POINTS        3528
#define OID_LOCATION_BIAS_BASE              3529
#define OID_LOCATION_ENABLE                 3530
#define OID_LOCATION_URL                    3531
#define OID_LOCATION_KEY                    3532
#define OID_LOCATION_PERIOD                 3533
#define OID_LOCATION_PER_RADIO_DATA         3534
#define OID_LOCATION_RF_FINGER_PRINT        3535
#define OID_LOCATION_BIAS_RANGE             3536
#define OID_LOCATION_BIAS_FACTOR            3537

//---------------------------------------------------------
// SSID Extended info [16x10]: (3538-3547) - (3688-3697)
//---------------------------------------------------------
#define NUM_SSID_EXT2_OIDS                  10
#define NUM_SSID_EXT2_OBJS                  NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_EXT2( _x_ )             OFFSET_OBJ( _x_, SSID_EXT2, OID_SSID_WPR_WLIST_DN(0) )
#define OID_IS_SSID_EXT2( _x_ )             OID_IS_OBJ( _x_, SSID_EXT2, OID_SSID_WPR_WLIST_DN(0) )
#define OID_SSID_EXT2(    _x_ )             OID_OBJ(    _x_, SSID_EXT2, OID_SSID_WPR_WLIST_DN(0) )
#define IDX_SSID_EXT2(    _x_ )             IDX_OBJ(    _x_, SSID_EXT2, OID_SSID_WPR_WLIST_DN(0) )

#define OID_SSID_WPR_WLIST_DN(      _idx_)  (3538 + OFFSET_SSID_EXT2(_idx_))
#define OID_SSID_MDM_AUTH(          _idx_)  (3539 + OFFSET_SSID_EXT2(_idx_))
#define OID_SSID_DHCP_OPTION(       _idx_)  (3540 + OFFSET_SSID_EXT2(_idx_))
#define OID_SSID_WPR_AUTH_TIMEOUT(  _idx_)  (3541 + OFFSET_SSID_EXT2(_idx_))
#define OID_SSID_WPR_PERSONAL_WIFI( _idx_)  (3542 + OFFSET_SSID_EXT2(_idx_))
#define OID_SSID_EXPIRATION(        _idx_)  (3543 + OFFSET_SSID_EXT2(_idx_))
#define OID_SSID_PERSONAL_WIFI_STA( _idx_)  (3544 + OFFSET_SSID_EXT2(_idx_))
#define OID_SSID_VLAN_POOL(         _idx_)  (3545 + OFFSET_SSID_EXT2(_idx_))
#define OID_SSID_UPSK_CACHE_TIMEOUT(_idx_)  (3546 + OFFSET_SSID_EXT2(_idx_))
#define OID_SSID_UPSK_SERVER_ERROR(_idx_ )  (3547 + OFFSET_SSID_EXT2(_idx_))

#define OID_IS_SSID_WPR_WLIST(  _x_ )       (OID_IS_SSID_EXT2(_x_) && OID_SSID_EXT2(_x_) == OID_SSID_WPR_WLIST_DN(0))
#define OID_IS_SSID_EXPIRATION( _x_ )       (OID_IS_SSID_EXT2(_x_) && OID_SSID_EXT2(_x_) == OID_SSID_EXPIRATION(  0))

//---------------------------------------------------------
// Cloud Mgmt settings
//---------------------------------------------------------
#define OID_CLOUD_ENABLED                   3698
#define OID_CLOUD_SERVER                    3699
#define OID_CLOUD_RETRY                     3700
#define OID_CLOUD_TIMEOUT                   3701
#define OID_CLOUD_SCHEME                    3702
#define OID_CLOUD_PORT                      3703
#define OID_CLOUD_SPARE1                    3704
#define OID_CLOUD_SPARE2                    3705

//---------------------------------------------------------
// MDM (Mobile Device Management) settings (AirWatch)
//---------------------------------------------------------
#define OID_MDM_AW_API_URL                  3706
#define OID_MDM_AW_API_KEY                  3707
#define OID_MDM_AW_API_USERNAME             3708
#define OID_MDM_AW_API_PASSWORD             3709
#define OID_MDM_AW_API_TIMEOUT              3710
#define OID_MDM_AW_API_POLL_PERIOD          3711
#define OID_MDM_AW_API_ACCESS_ERROR         3712
#define OID_MDM_AW_REDIRECT_URL             3713
#define OID_MDM_SPARE_1                     3714
#define OID_MDM_SPARE_2                     3715
#define OID_MDM_SPARE_3                     3716
#define OID_MDM_SPARE_4                     3717
#define OID_MDM_SPARE_5                     3718
#define OID_MDM_SPARE_6                     3719
#define OID_MDM_SPARE_7                     3720
#define OID_MDM_SPARE_8                     3721

//---------------------------------------------------------
// High Density Global IAP configuration info
//---------------------------------------------------------
#define OID_IAP_GLB_A_RETRIES_PER_RATE      3722
#define OID_IAP_GLB_G_RETRIES_PER_RATE      3723
#define OID_IAP_GLB_A_RETRY_RATE_DOWNS      3724
#define OID_IAP_GLB_G_RETRY_RATE_DOWNS      3725
#define OID_IAP_GLB_A_PRB_RSP_MIN_RSSI      3726
#define OID_IAP_GLB_G_PRB_RSP_MIN_RSSI      3727
#define OID_IAP_GLB_A_CCA_RX_THR_DELTA      3728
#define OID_IAP_GLB_G_CCA_RX_THR_DELTA      3729

//---------------------------------------------------------
// More location server settings (for XPS & Euclid)
//---------------------------------------------------------
#define OID_LOCATION_TEST_STATIONS          3730
#define OID_LOCATION_INCLUDE_RANDOM         3731
#define OID_LOCATION_SPARE_1                3732
#define OID_LOCATION_SPARE_2                3733

//---------------------------------------------------------
// More misc. manangement configuration info
//---------------------------------------------------------
#define OID_MGMT_SPARE_1                    3734
#define OID_MGMT_SPARE_2                    3735
#define OID_MGMT_SPARE_3                    3736
#define OID_MGMT_SPARE_4                    3737
#define OID_MGMT_SPARE_5                    3738
#define OID_MGMT_SPARE_6                    3739
#define OID_MGMT_RESET_BUTTON               3740
#define OID_MGMT_IPV6_SUPPORT               3741
#define OID_MGMT_PWIFI_DFLT_EXP             3742
#define OID_MGMT_PWIFI_ALL_STA              3743
#define OID_MGMT_PWIFI_PER_STA              3744
#define OID_MGMT_STATUS_LED                 3745

//---------------------------------------------------------
// Activation
//---------------------------------------------------------
#define NUM_ACTIVATION_OIDS                 2
#define OID_ACTIVATION_ENABLED              3746
#define OID_ACTIVATION_INTERVAL             3747

//---------------------------------------------------------
// 802.11ac configuration info
//---------------------------------------------------------
#define OID_IAP_GLB_AC_ENABLE               3748    // 0=off,  1=on
#define OID_IAP_GLB_AC_SGI_80MHZ            3749    // 80Mhz short guard interval, 0=disable, 1=enable
#define OID_IAP_GLB_AC_MAX_MCS_1SS          3750    // Max MCS setting, single spatial stream, 0=mcs7, 1=msc8, 2=mcs9
#define OID_IAP_GLB_AC_MAX_MCS_2SS          3751    // Max MCS setting,  dual  spatial stream, 0=mcs7, 1=msc8, 2=mcs9
#define OID_IAP_GLB_AC_MAX_MCS_3SS          3752    // Max MCS setting, triple spatial stream, 0=mcs7, 1=msc8, 2=mcs9
#define OID_IAP_GLB_AC_MAX_MCS_4SS          3753    // Max MCS setting,  quad  spatial stream, 0=mcs7, 1=msc8, 2=mcs9
#define OID_IAP_GLB_AC_SGI_160MHZ           3754    // 160Mhz short guard interval, 0=disable, 1=enable
#define OID_IAP_GLB_AC_MULTI_USER_MIMO      3755
#define OID_IAP_GLB_AC_TX_BEAM_FORMING      3756
#define OID_IAP_GLB_AC_AUTO_BOND            3757
#define OID_IAP_GLB_AC_SPARE_1              3758
#define OID_IAP_GLB_AC_SPARE_2              3759

//---------------------------------------------------------
// Global IAP multicast isolation mode
//---------------------------------------------------------
#define OID_IAP_GLB_MULTICAST_ISOLATION     3760

//---------------------------------------------------------
// Even more Autocell properties
//---------------------------------------------------------
#define OID_IAP_GLB_A_AUTO_CELL_BY_CHAN     3761
#define OID_IAP_GLB_G_AUTO_CELL_BY_CHAN     3762
#define OID_IAP_GLB_AUTO_CELL_BY_CHAN       3763

//---------------------------------------------------------
// More Extended VLAN settings [32x3]: (3764-3766) - (3857-3859) (for network assurance)
// ---------------------------------------------------------
#define NUM_VLAN3_OIDS                      3
#define NUM_VLAN3_OBJS                      32

#define OFFSET_VLAN3( _x_ )                 OFFSET_OBJ( _x_, VLAN3, OID_VLAN3_NAME(0) )
#define OID_IS_VLAN3( _x_ )                 OID_IS_OBJ( _x_, VLAN3, OID_VLAN3_NAME(0) )
#define OID_VLAN3(    _x_ )                 OID_OBJ(    _x_, VLAN3, OID_VLAN3_NAME(0) )
#define IDX_VLAN3(    _x_ )                 IDX_OBJ(    _x_, VLAN3, OID_VLAN3_NAME(0) )

#define OID_VLAN3_NAME(         _idx_)      (3764 + OFFSET_VLAN3(_idx_))
#define OID_VLAN3_GATEWAY(      _idx_)      (3765 + OFFSET_VLAN3(_idx_))
#define OID_VLAN3_TUNNEL_SERVER(_idx_)      (3766 + OFFSET_VLAN3(_idx_))

//---------------------------------------------------------
// Proxy Forward
//---------------------------------------------------------
#define OID_PROXY_FWD_PROXY_TYPE            3860
#define OID_PROXY_FWD_BLUECOAT_HOST         3861
#define OID_PROXY_FWD_NETBOXBLUE_HOST       3862
#define OID_PROXY_FWD_SPARE_1               3863
#define OID_PROXY_FWD_SPARE_2               3864
#define OID_PROXY_FWD_SPARE_3               3865

//---------------------------------------------------------
// More 802.11ac configuration info
//---------------------------------------------------------
#define OID_IAP_GLB_AC_MAX_MCS_5SS          3866    // Max MCS setting, single spatial stream, 0=mcs7, 1=msc8, 2=mcs9
#define OID_IAP_GLB_AC_MAX_MCS_6SS          3867    // Max MCS setting,  dual  spatial stream, 0=mcs7, 1=msc8, 2=mcs9
#define OID_IAP_GLB_AC_MAX_MCS_7SS          3868    // Max MCS setting, triple spatial stream, 0=mcs7, 1=msc8, 2=mcs9
#define OID_IAP_GLB_AC_MAX_MCS_8SS          3869    // Max MCS setting,  quad  spatial stream, 0=mcs7, 1=msc8, 2=mcs9

//---------------------------------------------------------
// Active Directory
//---------------------------------------------------------
#define OID_ACTIVE_DIRECTORY_USER           3870
#define OID_ACTIVE_DIRECTORY_PASS           3871
#define OID_ACTIVE_DIRECTORY_DC             3872
#define OID_ACTIVE_DIRECTORY_WG             3873
#define OID_ACTIVE_DIRECTORY_REALM          3874
#define OID_ACTIVE_DIRECTORY_SPARE_1        3875    // Growth for things like attribute mapping
#define OID_ACTIVE_DIRECTORY_SPARE_2        3876
#define OID_ACTIVE_DIRECTORY_SPARE_3        3877
#define OID_ACTIVE_DIRECTORY_SPARE_4        3878
#define OID_ACTIVE_DIRECTORY_SPARE_5        3879

//---------------------------------------------------------
// LLDP configuration info
//---------------------------------------------------------
#define OID_LLDP_ENABLE                     3880
#define OID_LLDP_INTERVAL                   3881
#define OID_LLDP_HOLD_TIME                  3882
#define OID_LLDP_REQUEST_PWR                3883
#define OID_LLDP_FABRIC_ATTACH              3884
#define OID_LLDP_FABRIC_ATTACH_MSG_AUTH_KEY 3885
#define OID_LLDP_SPARE_1                    3886
#define OID_LLDP_SPARE_2                    3887
#define OID_LLDP_SPARE_3                    3888
#define OID_LLDP_SPARE_4                    3889

//---------------------------------------------------------
// PROXY MGMT configuration info   (3890 - 3929)
//---------------------------------------------------------
#define OID_PROXY_MGMT_ENABLE               3890
#define OID_PROXY_MGMT_CUSTOM               3891

#define OID_PROXY_MGMT_HTTP_ENABLE          3892
#define OID_PROXY_MGMT_HTTP_HOST            3893
#define OID_PROXY_MGMT_HTTP_PORT            3894
#define OID_PROXY_MGMT_HTTP_USER            3895
#define OID_PROXY_MGMT_HTTP_PASS            3896
#define OID_PROXY_MGMT_HTTP_TYPE            3897

#define OID_PROXY_MGMT_HTTPS_ENABLE         3898
#define OID_PROXY_MGMT_HTTPS_HOST           3899
#define OID_PROXY_MGMT_HTTPS_PORT           3900
#define OID_PROXY_MGMT_HTTPS_USER           3901
#define OID_PROXY_MGMT_HTTPS_PASS           3902
#define OID_PROXY_MGMT_HTTPS_TYPE           3903

#define OID_PROXY_MGMT_SOCKS_ENABLE         3904
#define OID_PROXY_MGMT_SOCKS_IP             3905
#define OID_PROXY_MGMT_SOCKS_PORT           3906
#define OID_PROXY_MGMT_SOCKS_USER           3907
#define OID_PROXY_MGMT_SOCKS_PASS           3908
#define OID_PROXY_MGMT_SOCKS_TYPE           3909

#define OID_PROXY_MGMT_SUBNET_01            3910
#define OID_PROXY_MGMT_MASK_01              3911
#define OID_PROXY_MGMT_SUBNET_02            3912
#define OID_PROXY_MGMT_MASK_02              3913
#define OID_PROXY_MGMT_SUBNET_03            3914
#define OID_PROXY_MGMT_MASK_03              3915
#define OID_PROXY_MGMT_SUBNET_04            3916
#define OID_PROXY_MGMT_MASK_04              3917
#define OID_PROXY_MGMT_SUBNET_05            3918
#define OID_PROXY_MGMT_MASK_05              3919
#define OID_PROXY_MGMT_SUBNET_06            3920
#define OID_PROXY_MGMT_MASK_06              3921
#define OID_PROXY_MGMT_SUBNET_07            3922
#define OID_PROXY_MGMT_MASK_07              3923
#define OID_PROXY_MGMT_SUBNET_08            3924
#define OID_PROXY_MGMT_MASK_08              3925
#define OID_PROXY_MGMT_SUBNET_09            3926
#define OID_PROXY_MGMT_MASK_09              3927
#define OID_PROXY_MGMT_SUBNET_10            3928
#define OID_PROXY_MGMT_MASK_10              3929

//---------------------------------------------------------
// App List info [16x4]: (3930-3903) - (3990-3993)
//---------------------------------------------------------
#define NUM_APP_LIST_OIDS                   4
#define NUM_APP_LIST_OBJS                   16

#define OFFSET_APP_LIST( _x_ )              OFFSET_OBJ( _x_, APP_LIST, OID_APP_LIST_GUID(0) )
#define OID_IS_APP_LIST( _x_ )              OID_IS_OBJ( _x_, APP_LIST, OID_APP_LIST_GUID(0) )
#define OID_APP_LIST(    _x_ )              OID_OBJ(    _x_, APP_LIST, OID_APP_LIST_GUID(0) )
#define IDX_APP_LIST(    _x_ )              IDX_OBJ(    _x_, APP_LIST, OID_APP_LIST_GUID(0) )

#define OID_APP_LIST_GUID(   _idx_)         (3930 + OFFSET_APP_LIST(_idx_))
#define OID_APP_LIST_DESC(   _idx_)         (3931 + OFFSET_APP_LIST(_idx_))
#define OID_APP_LIST_SPARE_1(_idx_)         (3932 + OFFSET_APP_LIST(_idx_))
#define OID_APP_LIST_SPARE_2(_idx_)         (3933 + OFFSET_APP_LIST(_idx_))

#define OID_APP_LIST_NUM                    3994

//---------------------------------------------------------
// Activation Extension (3995 - 3999)
//---------------------------------------------------------
#define NUM_ACTIVATION_EXT_OIDS             5

#define OID_ACTIVATION_SERVER               3995
#define OID_ACTIVATION_VERSION              3996
#define OID_ACTIVATION_SPARE1               3997
#define OID_ACTIVATION_SPARE2               3998
#define OID_ACTIVATION_SPARE3               3999

//---------------------------------------------------------
// Proxy Mgmt Extension (4000 - 4009)
//---------------------------------------------------------
#define OID_PROXY_MGMT_SPARE_1              4000
#define OID_PROXY_MGMT_SPARE_2              4001
#define OID_PROXY_MGMT_SPARE_3              4002
#define OID_PROXY_MGMT_SPARE_4              4003
#define OID_PROXY_MGMT_SPARE_5              4004
#define OID_PROXY_MGMT_SPARE_6              4005
#define OID_PROXY_MGMT_SPARE_7              4006
#define OID_PROXY_MGMT_SPARE_8              4007
#define OID_PROXY_MGMT_SPARE_9              4008
#define OID_PROXY_MGMT_SPARE_10             4009

//---------------------------------------------------------
// VLAN Pools [16x8]: (4010-4017) - (4130-4137)
// ---------------------------------------------------------
#define NUM_VLAN_POOL_OIDS                  8
#define NUM_VLAN_POOL_OBJS                  16

#define OFFSET_VLAN_POOL( _x_ )             OFFSET_OBJ( _x_, VLAN_POOL, OID_VLAN_POOL_NAME(0) )
#define OID_IS_VLAN_POOL( _x_ )             OID_IS_OBJ( _x_, VLAN_POOL, OID_VLAN_POOL_NAME(0) )
#define OID_VLAN_POOL(    _x_ )             OID_OBJ(    _x_, VLAN_POOL, OID_VLAN_POOL_NAME(0) )
#define IDX_VLAN_POOL(    _x_ )             IDX_OBJ(    _x_, VLAN_POOL, OID_VLAN_POOL_NAME(0) )

#define OID_VLAN_POOL_NAME(   _idx_)        (4010 + OFFSET_VLAN_POOL(_idx_))
#define OID_VLAN_POOL_LIST(   _idx_)        (4011 + OFFSET_VLAN_POOL(_idx_))    //                                              (strvalue = u16 [MAX_NUM_VLAN + 1]
#define OID_VLAN_POOL_SPARE_1(_idx_)        (4012 + OFFSET_VLAN_POOL(_idx_))
#define OID_VLAN_POOL_SPARE_2(_idx_)        (4013 + OFFSET_VLAN_POOL(_idx_))
#define OID_VLAN_POOL_SPARE_3(_idx_)        (4014 + OFFSET_VLAN_POOL(_idx_))
#define OID_VLAN_POOL_SPARE_4(_idx_)        (4015 + OFFSET_VLAN_POOL(_idx_))
#define OID_VLAN_POOL_SPARE_5(_idx_)        (4016 + OFFSET_VLAN_POOL(_idx_))
#define OID_VLAN_POOL_SPARE_6(_idx_)        (4017 + OFFSET_VLAN_POOL(_idx_))

#define OID_IS_VLAN_POOL_LIST(_x_)          (OID_IS_VLAN_POOL(_x_) && OID_VLAN_POOL(_x_) == OID_VLAN_POOL_LIST(0))

//---------------------------------------------------------
// User Group Extension [16x8]: (4138-4145) - (4258-4265)
//---------------------------------------------------------
#define NUM_USER_GROUP_EXT_OIDS             8
#define NUM_USER_GROUP_EXT_OBJS             NUM_USER_GROUP_OBJS

#define OFFSET_USER_GROUP_EXT( _x_ )        OFFSET_OBJ( _x_, USER_GROUP_EXT, OID_GROUP_VLAN_POOL(0) )
#define OID_IS_USER_GROUP_EXT( _x_ )        OID_IS_OBJ( _x_, USER_GROUP_EXT, OID_GROUP_VLAN_POOL(0) )
#define OID_USER_GROUP_EXT(    _x_ )        OID_OBJ(    _x_, USER_GROUP_EXT, OID_GROUP_VLAN_POOL(0) )
#define IDX_USER_GROUP_EXT(    _x_ )        IDX_OBJ(    _x_, USER_GROUP_EXT, OID_GROUP_VLAN_POOL(0) )

#define OID_GROUP_VLAN_POOL(       _idx_)   (4138 + OFFSET_USER_GROUP_EXT(_idx_))
#define OID_GROUP_WPR_SCREEN(      _idx_)   (4139 + OFFSET_USER_GROUP_EXT(_idx_))
#define OID_GROUP_WPR_SERVER(      _idx_)   (4140 + OFFSET_USER_GROUP_EXT(_idx_))
#define OID_GROUP_WPR_REDIRECT_URL(_idx_)   (4141 + OFFSET_USER_GROUP_EXT(_idx_))
#define OID_GROUP_WPR_SECRET(      _idx_)   (4142 + OFFSET_USER_GROUP_EXT(_idx_))
#define OID_GROUP_WPR_HTTPS(       _idx_)   (4143 + OFFSET_USER_GROUP_EXT(_idx_))
#define OID_GROUP_WPR_AUTH_TYPE(   _idx_)   (4144 + OFFSET_USER_GROUP_EXT(_idx_))
#define OID_GROUP_WPR_AUTH_TIMEOUT(_idx_)   (4145 + OFFSET_USER_GROUP_EXT(_idx_))

//---------------------------------------------------------
// SSID Extended info [16x10]: (4266-4275) - (4416-4425)
//---------------------------------------------------------
#define NUM_SSID_EXT3_OIDS                  10
#define NUM_SSID_EXT3_OBJS                  NUM_SSID_BASIC_OBJS

#define OFFSET_SSID_EXT3( _x_ )             OFFSET_OBJ( _x_, SSID_EXT3, OID_SSID_DATE_ON(0) )
#define OID_IS_SSID_EXT3( _x_ )             OID_IS_OBJ( _x_, SSID_EXT3, OID_SSID_DATE_ON(0) )
#define OID_SSID_EXT3(    _x_ )             OID_OBJ(    _x_, SSID_EXT3, OID_SSID_DATE_ON(0) )
#define IDX_SSID_EXT3(    _x_ )             IDX_OBJ(    _x_, SSID_EXT3, OID_SSID_DATE_ON(0) )

#define OID_SSID_DATE_ON(             _i_)  (4266 + OFFSET_SSID_EXT3(_i_))
#define OID_SSID_DATE_OFF(            _i_)  (4267 + OFFSET_SSID_EXT3(_i_))
#define OID_SSID_RAD_FAILOVER_TIMEOUT(_i_)  (4268 + OFFSET_SSID_EXT3(_i_))
#define OID_SSID_WPR_HTTPS_PASSTHRU(  _i_)  (4269 + OFFSET_SSID_EXT3(_i_))
#define OID_SSID_DOT11_R_ENABLE(      _i_)  (4270 + OFFSET_SSID_EXT3(_i_))
#define OID_SSID_80211X_4WAY_TIMEOUT( _i_)  (4271 + OFFSET_SSID_EXT3(_i_))
#define OID_SSID_80211X_4WAY_RETRIES( _i_)  (4272 + OFFSET_SSID_EXT3(_i_))
#define OID_SSID_SPARE_3(             _i_)  (4273 + OFFSET_SSID_EXT3(_i_))
#define OID_SSID_SPARE_4(             _i_)  (4274 + OFFSET_SSID_EXT3(_i_))
#define OID_SSID_SPARE_5(             _i_)  (4275 + OFFSET_SSID_EXT3(_i_))

#define OID_IS_SSID_DATE_ON(  _x_ )         (OID_IS_SSID_EXT3(_x_) && OID_SSID_EXT3(_x_) == OID_SSID_DATE_ON(           0))
#define OID_IS_SSID_DATE_OFF( _x_ )         (OID_IS_SSID_EXT3(_x_) && OID_SSID_EXT3(_x_) == OID_SSID_DATE_OFF(          0))
#define OID_IS_SSID_WPR_HTTPS_PASSTHRU(_x_) (OID_IS_SSID_EXT3(_x_) && OID_SSID_EXT3(_x_) == OID_SSID_WPR_HTTPS_PASSTHRU(0))

//---------------------------------------------------------
// IPv6 Ethernet objects [6x10]: (4426-4435) - (4476-4485)
//---------------------------------------------------------
#define NUM_ETH_IPV6_OIDS                   10
#define NUM_ETH_IPV6_OBJS                   6

#define OFFSET_ETH_IPV6( _x_ )              OFFSET_OBJ( _x_, ETH_IPV6, OID_ETH_IPV6_DHCP(0))
#define OID_IS_ETH_IPV6( _x_ )              OID_IS_OBJ( _x_, ETH_IPV6, OID_ETH_IPV6_DHCP(0))
#define OID_ETH_IPV6(    _x_ )              OID_OBJ(    _x_, ETH_IPV6, OID_ETH_IPV6_DHCP(0))
#define IDX_ETH_IPV6(    _x_ )              IDX_OBJ(    _x_, ETH_IPV6, OID_ETH_IPV6_DHCP(0))

#define OID_ETH_IPV6_DHCP(   _idx_)         (4426 + OFFSET_ETH_IPV6(_idx_))
#define OID_ETH_IPV6_ADDR(   _idx_)         (4427 + OFFSET_ETH_IPV6(_idx_))
#define OID_ETH_IPV6_MASK(   _idx_)         (4428 + OFFSET_ETH_IPV6(_idx_))
#define OID_ETH_IPV6_SPARE_1(_idx_)         (4429 + OFFSET_ETH_IPV6(_idx_))
#define OID_ETH_IPV6_SPARE_2(_idx_)         (4430 + OFFSET_ETH_IPV6(_idx_))
#define OID_ETH_IPV6_SPARE_3(_idx_)         (4431 + OFFSET_ETH_IPV6(_idx_))
#define OID_ETH_IPV6_SPARE_4(_idx_)         (4432 + OFFSET_ETH_IPV6(_idx_))
#define OID_ETH_IPV6_SPARE_5(_idx_)         (4433 + OFFSET_ETH_IPV6(_idx_))
#define OID_ETH_IPV6_SPARE_6(_idx_)         (4434 + OFFSET_ETH_IPV6(_idx_))
#define OID_ETH_IPV6_SPARE_7(_idx_)         (4435 + OFFSET_ETH_IPV6(_idx_))

//---------------------------------------------------------
// EasyPass XMS-E Mgmt settings
//---------------------------------------------------------
#define OID_EASY_PASS                       4486
#define OID_EASY_PASS_SERVER                4487
#define OID_EASY_PASS_RETRY                 4488
#define OID_EASY_PASS_TIMEOUT               4489
#define OID_EASY_PASS_SCHEME                4490
#define OID_EASY_PASS_PORT                  4491
#define OID_EASY_PASS_SPARE1                4492
#define OID_EASY_PASS_SPARE2                4493

//---------------------------------------------------------
// Bluetooth settings
//---------------------------------------------------------
#define OID_BLUETOOTH_IBEACON_ENABLE        4494
#define OID_BLUETOOTH_IBEACON_UUID          4495
#define OID_BLUETOOTH_IBEACON_MAJOR         4496
#define OID_BLUETOOTH_IBEACON_MINOR         4497
#define OID_BLUETOOTH_SPARE1                4498
#define OID_BLUETOOTH_SPARE2                4499
#define OID_BLUETOOTH_SPARE3                4500
#define OID_BLUETOOTH_SPARE4                4501
#define OID_BLUETOOTH_SPARE5                4502
#define OID_BLUETOOTH_SPARE6                4503
#define OID_BLUETOOTH_SPARE7                4504
#define OID_BLUETOOTH_SPARE8                4505
#define OID_BLUETOOTH_SPARE9                4506
#define OID_BLUETOOTH_SPARE10               4507
#define OID_BLUETOOTH_SPARE11               4508
#define OID_BLUETOOTH_SPARE12               4509

//---------------------------------------------------------
// Auto-Discovery Mgmt settings
//---------------------------------------------------------
#define OID_AUTO_DISC                       4510
#define OID_AUTO_DISC_SERVER                4511
#define OID_AUTO_DISC_INTERVAL              4512
#define OID_AUTO_DISC_SCHEME                4513
#define OID_AUTO_DISC_PORT                  4514
#define OID_AUTO_DISC_SPARE1                4515
#define OID_AUTO_DISC_SPARE2                4516
#define OID_AUTO_DISC_SPARE3                4517

//---------------------------------------------------------
// Number of Objects
//---------------------------------------------------------
#define NUM_OBJS                            4518    // The max num of cfg objects needed

//---------------------------------------------------------
// Universal SSID properties
//---------------------------------------------------------
#define OID_IS_SSID(_idx_)  (OID_IS_SSID_AUTH       (_idx_) || \
                             OID_IS_SSID_BASIC      (_idx_) || \
                             OID_IS_SSID_DHCP       (_idx_) || \
                             OID_IS_SSID_EXT        (_idx_) || \
                             OID_IS_SSID_EXT2       (_idx_) || \
                             OID_IS_SSID_EXT3       (_idx_) || \
                             OID_IS_SSID_LIMIT      (_idx_) || \
                             OID_IS_SSID_MAC        (_idx_) || \
                             OID_IS_SSID_RADIUS     (_idx_) || \
                             OID_IS_SSID_RADIUS_ACCT(_idx_) || \
                             OID_IS_SSID_SECURITY   (_idx_) || \
                             OID_IS_SSID_SEC_DFLT   (_idx_) || \
                             OID_IS_SSID_WPR        (_idx_) || \
                             OID_IS_SSID_WPR_CUSTOM (_idx_) || \
                             OID_IS_SSID_WPR_LANDING(_idx_))

#define OID_SSID(_idx_)     (OID_IS_SSID_AUTH       (_idx_) ? OID_SSID_AUTH       (_idx_) :  \
                             OID_IS_SSID_BASIC      (_idx_) ? OID_SSID_BASIC      (_idx_) :  \
                             OID_IS_SSID_DHCP       (_idx_) ? OID_SSID_DHCP       (_idx_) :  \
                             OID_IS_SSID_EXT        (_idx_) ? OID_SSID_EXT        (_idx_) :  \
                             OID_IS_SSID_EXT2       (_idx_) ? OID_SSID_EXT2       (_idx_) :  \
                             OID_IS_SSID_EXT3       (_idx_) ? OID_SSID_EXT3       (_idx_) :  \
                             OID_IS_SSID_LIMIT      (_idx_) ? OID_SSID_LIMIT      (_idx_) :  \
                             OID_IS_SSID_MAC        (_idx_) ? OID_SSID_MAC        (_idx_) :  \
                             OID_IS_SSID_RADIUS     (_idx_) ? OID_SSID_RADIUS     (_idx_) :  \
                             OID_IS_SSID_RADIUS_ACCT(_idx_) ? OID_SSID_RADIUS_ACCT(_idx_) :  \
                             OID_IS_SSID_SECURITY   (_idx_) ? OID_SSID_SECURITY   (_idx_) :  \
                             OID_IS_SSID_SEC_DFLT   (_idx_) ? OID_SSID_SEC_DFLT   (_idx_) :  \
                             OID_IS_SSID_WPR        (_idx_) ? OID_SSID_WPR        (_idx_) :  \
                             OID_IS_SSID_WPR_CUSTOM (_idx_) ? OID_SSID_WPR_CUSTOM (_idx_) :  \
                             OID_IS_SSID_WPR_LANDING(_idx_) ? OID_SSID_WPR_LANDING(_idx_) : OID_SSID_NAME(0))

#define IDX_SSID(_idx_)     (OID_IS_SSID_AUTH       (_idx_) ? IDX_SSID_AUTH       (_idx_) :  \
                             OID_IS_SSID_BASIC      (_idx_) ? IDX_SSID_BASIC      (_idx_) :  \
                             OID_IS_SSID_DHCP       (_idx_) ? IDX_SSID_DHCP       (_idx_) :  \
                             OID_IS_SSID_EXT        (_idx_) ? IDX_SSID_EXT        (_idx_) :  \
                             OID_IS_SSID_EXT2       (_idx_) ? IDX_SSID_EXT2       (_idx_) :  \
                             OID_IS_SSID_EXT3       (_idx_) ? IDX_SSID_EXT3       (_idx_) :  \
                             OID_IS_SSID_LIMIT      (_idx_) ? IDX_SSID_LIMIT      (_idx_) :  \
                             OID_IS_SSID_MAC        (_idx_) ? IDX_SSID_MAC        (_idx_) :  \
                             OID_IS_SSID_RADIUS     (_idx_) ? IDX_SSID_RADIUS     (_idx_) :  \
                             OID_IS_SSID_RADIUS_ACCT(_idx_) ? IDX_SSID_RADIUS_ACCT(_idx_) :  \
                             OID_IS_SSID_SECURITY   (_idx_) ? IDX_SSID_SECURITY   (_idx_) :  \
                             OID_IS_SSID_SEC_DFLT   (_idx_) ? IDX_SSID_SEC_DFLT   (_idx_) :  \
                             OID_IS_SSID_WPR        (_idx_) ? IDX_SSID_WPR        (_idx_) :  \
                             OID_IS_SSID_WPR_CUSTOM (_idx_) ? IDX_SSID_WPR_CUSTOM (_idx_) :  \
                             OID_IS_SSID_WPR_LANDING(_idx_) ? IDX_SSID_WPR_LANDING(_idx_) : 0)

//---------------------------------------------------------
// Universal User Group properties
//---------------------------------------------------------
#define OID_IS_GROUP(_idx_) (OID_IS_USER_GROUP           (_idx_) || \
                             OID_IS_USER_GROUP_EXT       (_idx_) || \
                             OID_IS_USER_GROUP_WPR_CUSTOM(_idx_))

#define OID_GROUP(_idx_)    (OID_IS_USER_GROUP           (_idx_) ? OID_USER_GROUP           (_idx_) :  \
                             OID_IS_USER_GROUP_EXT       (_idx_) ? OID_USER_GROUP_EXT       (_idx_) :  \
                             OID_IS_USER_GROUP_WPR_CUSTOM(_idx_) ? OID_USER_GROUP_WPR_CUSTOM(_idx_) : OID_GROUP_NAME(0))

#define IDX_GROUP(_idx_)    (OID_IS_USER_GROUP           (_idx_) ? IDX_USER_GROUP           (_idx_) :  \
                             OID_IS_USER_GROUP_EXT       (_idx_) ? IDX_USER_GROUP_EXT       (_idx_) :  \
                             OID_IS_USER_GROUP_WPR_CUSTOM(_idx_) ? IDX_USER_GROUP_WPR_CUSTOM(_idx_) : 0)

//---------------------------------------------------------
// Universal WDS properties
//---------------------------------------------------------
#define OID_IS_WDS(_idx_)   (OID_IS_WDS_LINK(_idx_) || (_idx_) == OID_WDS_ALLOW_STATIONS || (_idx_) == OID_WDS_ROAM_AVG_WEIGHT || (_idx_) == OID_WDS_ROAM_THRESHOLD)
#define OID_WDS(_idx_)      (OID_IS_WDS_LINK(_idx_) ? OID_WDS_LINK(_idx_) : 0)
#define IDX_WDS(_idx_)      (OID_IS_WDS_LINK(_idx_) ? IDX_WDS_LINK(_idx_) : 0)

//---------------------------------------------------------
// Universal VLAN properties (for network assurance)
//---------------------------------------------------------
#define NUM_VLAN_OIDS                       3
#define NUM_VLAN_OBJS                       (NUM_VLAN1_OBJS + NUM_VLAN2_OBJS + NUM_VLAN3_OBJS)

#define OID_IS_VLAN( _x_ )                  (OID_IS_VLAN1( _x_ )                   || OID_IS_VLAN2( _x_ )                                                        || OID_IS_VLAN3( _x_ ))
#define OID_VLAN(    _x_ )                  (OID_IS_VLAN1( _x_ ) ? OID_VLAN1( _x_ ) : OID_IS_VLAN2( _x_ ) ? OID_VLAN2( _x_ ) - OID_VLAN2_NAME(0) + OID_VLAN1_NAME(0) : OID_VLAN3( _x_ ) - OID_VLAN3_NAME(0) + OID_VLAN1_NAME(0) )
#define IDX_VLAN(    _x_ )                  (OID_IS_VLAN1( _x_ ) ? IDX_VLAN1( _x_ ) : OID_IS_VLAN2( _x_ ) ? IDX_VLAN2( _x_ ) + NUM_VLAN1_OBJS                        : IDX_VLAN2( _x_ ) + NUM_VLAN1_OBJS    + NUM_VLAN2_OBJS    )

#define OID_VLAN_OBJECT(TYPE,  _idx_)       (((_idx_) < NUM_VLAN1_OBJS                 ) ? OID_VLAN1_##TYPE( _idx_                                   ) : \
                                             ((_idx_) < NUM_VLAN1_OBJS + NUM_VLAN2_OBJS) ? OID_VLAN2_##TYPE((_idx_) - NUM_VLAN1_OBJS                 ) : \
                                                                                           OID_VLAN3_##TYPE((_idx_) - NUM_VLAN1_OBJS - NUM_VLAN2_OBJS)   )
#define OID_VLAN_NAME(         _idx_)       OID_VLAN_OBJECT(NAME         , _idx_)
#define OID_VLAN_GATEWAY(      _idx_)       OID_VLAN_OBJECT(GATEWAY      , _idx_)
#define OID_VLAN_TUNNEL_SERVER(_idx_)       OID_VLAN_OBJECT(TUNNEL_SERVER, _idx_)

//---------------------------------------------------------
// Universal Radio properties (note: XAPs & VAPs not used)
//---------------------------------------------------------
#define NUM_RADIO_OIDS                      20
#define NUM_RADIO_OBJS                      NUM_IAP_OBJS

#define OID_IS_RADIO( _x_ )                 OID_IS_IAP( _x_ )
#define OID_RADIO(    _x_ )                 OID_IAP(    _x_ )
#define IDX_RADIO(    _x_ )                 IDX_IAP(    _x_ )

#define OID_RADIO_START(        _idx_)      OID_IAP_START(        _idx_) // Unused (spare)
#define OID_RADIO_ANTENNA(      _idx_)      OID_IAP_ANTENNA(      _idx_)
#define OID_RADIO_BOND_40MHZ(   _idx_)      OID_IAP_BOND_40MHZ(   _idx_)
#define OID_RADIO_BAND(         _idx_)      OID_IAP_BAND(         _idx_)
#define OID_RADIO_CELL_SIZE(    _idx_)      OID_IAP_CELL_SIZE(    _idx_)
#define OID_RADIO_CHANNEL(      _idx_)      OID_IAP_CHANNEL(      _idx_)
#define OID_RADIO_DESC(         _idx_)      OID_IAP_DESC(         _idx_)
#define OID_RADIO_CHANNEL_MODE( _idx_)      OID_IAP_CHANNEL_MODE( _idx_)
#define OID_RADIO_WIFI_MODE(    _idx_)      OID_IAP_WIFI_MODE(    _idx_)
#define OID_RADIO_BOND_80MHZ(   _idx_)      OID_IAP_BOND_80MHZ(   _idx_)
#define OID_RADIO_ENABLED(      _idx_)      OID_IAP_ENABLED(      _idx_)
#define OID_RADIO_BOND_160MHZ(  _idx_)      OID_IAP_BOND_160MHZ(  _idx_)
#define OID_RADIO_WDS_LINK(     _idx_)      OID_IAP_WDS_LINK(     _idx_)
#define OID_RADIO_WDS_DISTANCE( _idx_)      OID_IAP_WDS_DISTANCE( _idx_)
#define OID_RADIO_RX_THRESHOLD( _idx_)      OID_IAP_RX_THRESHOLD( _idx_)
#define OID_RADIO_TX_POWER(     _idx_)      OID_IAP_TX_POWER(     _idx_)
#define OID_RADIO_END(          _idx_)      OID_IAP_END(          _idx_) // Unused (spare)

#define OID_RADIO_SPARE_1(      _idx_)      OID_IAP_SPARE_1(      _idx_)
#define OID_RADIO_SPARE_2(      _idx_)      OID_IAP_SPARE_2(      _idx_)
#define OID_RADIO_SPARE_3(      _idx_)      OID_IAP_SPARE_3(      _idx_)

//****************************************************************
// Non-table OIDs
//----------------------------------------------------------------
#define OID_OLD_ACL_ENTRY                   800     // Deprecated obj id for acl_entry   (stored) (for compatibility)
#define OID_OLD_RADIUS_USER                 802     // Deprecated obj id for radius_user (stored) (for compatibility)

#define EXT_OBJECTS_BASE                    10000
#define OID_ACL_ENTRY                       10000
#define OID_ACL_SIZE                        10001
#define OID_RADIUS_USER                     10002   //                                                                          (strvlaue = struct raduser_info)
#define OID_RADIUS_USER_NUM                 10003
#define OID_SAVE_FLAG                       10004
#define OID_AUTO_CH_STATUS                  10005
#define OID_NUM_LOGS                        10006
#define OID_NUM_RADIOS                      10007   // The number of populated radios                                           (strvalue = struct iap_counts_info)
#define OID_RESET_REASON                    10008   // reason reported by atmel via driver
#define OID_RADAR_TEST_MODE                 10009
#define OID_RADIUS_USER_VAR_SIZE            10010   // Variable length radius user entry
#define OID_RADIUS_USER_INDEX               10011   // Find radius user index
#define OID_ADMIN_ENTRY_INDEX               10012   // Find admin entry index
#define OID_FILTER_ENTRY                    10013   // Protocol/port filter entry                                               (strvalue = struct filter_cfg)
#define OID_VLAN_ENTRY                      10014   //                                                                          (strvalue = struct vlan_cfg)
#define OID_VLAN_SET_IP                     10015   // Set IP addr, mask, & gateway only (deprecated, here for backward compatibility)
#define OID_NFS_FLAG                        10016   // return nfs flag (1=nfs)
#define OID_RADAR_DETECTED                  10017   // sent from oopme to config on radar detect
#define OID_RADAR_CHAN_SWITCH               10018   // sent from config to oopme to announce channel switch
//#define OID_ROGUE_SPARE                   10019   // unused (was add or delete ssids to the rogue database, now deprecated)
#define OID_XRP_TARGET_ENTRY_OLD            10020   // add or del xrp tunnel partners (deprecated, here for backward compatibility)
#define OID_SET_GEOGRAPHY                   10021   // set new geography information
#define OID_GET_IAP_BSSID                   10022   // get bssid string for a given iap
#define OID_SOFTWARE_IMAGE_STATUS           10023   // get status of software image
#define OID_GET_PID_STATUS                  10024   // get status of a child process
#define OID_XRP_TARGET_ENTRY                10025   // new add or del xrp tunnel partners                                       (strvalue = struct xrp_target_cfg)
#define OID_XRP_INTERFACE                   10026   // interface info used by XRP
#define OID_WDS_LOOPBACK_MODE               10027   // enable/disable wds loopback mode
#define OID_IAP_GLB_N_CAPABILITIES          10028   // read only: bit 0=802.11n capable (other bits tbd)
#define OID_FILTER_LIST_ENTRY               10029   // Protocol/port filter entry                                               (strvalue = struct filter_cfg)
#define OID_CFG_READY                       10030   // config is up and ready
#define OID_OOPME_RUNNING                   10031   // OOPME is up and fully running
#define OID_AUTO_CH_NEGOTIATION             10032   // read & write autochannel negotiation state (request/active)
#define OID_MFG_LOOPBACK_TEST               10033   // execute mfg loopback (grafted into oopme from u-boot)
#define OID_REMOTE_IMAGE_NAME               10034   // remote tftp boot image name
#define OID_REMOTE_IMAGE_SERVER             10035   // remote tftp boot image server
#define OID_REMOTE_CONFIG_NAME              10036   // remote tftp boot config name
#define OID_LICENSE_KEY                     10037   // software license key string
#define OID_CLUSTER_REV                     10038   // cluster xircfg revision                                                  (strvalue = struct version_info)
#define OID_NEW_ACL_ENTRY                   10039   // string based acl, can contain wildcards
#define OID_PRIV_NAMED_SECTION              10040   // set priv level for named section
#define OID_SET_RADAR_TEST_TIME             10041   // set radar timeout for testing
#define OID_VLAN_SET_IPV4_ADDR              10042   // Set VLAN IPv4 addr (used by new dhcpcd)
#define OID_VLAN_SET_IPV4_MASK              10043   // Set VLAN IPv4 mask (used by new dhcpcd)
#define OID_VLAN_SET_IPV4_GATEWAY           10044   // Set VLAN IPv4 gateway (used by new dhcpcd)
#define OID_LOAD_BALANCE_DEBUG              10045   // Set load balancing debug mode
#define OID_LOAD_BALANCE_MAX_ATTEMPTS       10046   // Set load balancing max assoc attempts
#define OID_LOAD_BALANCE_MIN_PROBE_REQS     10047   // Set load balancing min probe requests
#define OID_LOAD_BALANCE_WEIGHT_SNR         10048   // Set load balancing snr weight factor
#define OID_LOAD_BALANCE_WEIGHT_BAND        10049   // Set load balancing band weight factor
#define OID_LOAD_BALANCE_WEIGHT_HT          10050   // Set load balancing ht station weight factor
#define OID_DBG_LEVEL                       10051   // Set debug level of a class
#define OID_DBG_GROUP_SET                   10052   // Set debug group of a class
#define OID_DBG_GROUP_CLR                   10053   // Clear debug group of a class
#define OID_DBG_BITS_SET                    10054   // Set debug bits of a class
#define OID_DBG_BITS_CLR                    10055   // Clear debug bits of a class
#define OID_DBG_CONSOLE                     10056   // Set console output on/off for a class
#define OID_UPDATE_NEIGHBOR_IP              10057   // Update neighbor ip address (sent to xcfg from oopme)
#define OID_MGMT_RDK_MODE                   10058   // Set RDK mode
#define OID_IAP_GLB_RF_MONITOR_IAP          10059   // Set the monitor radio iap index
#define OID_NUM_ETH_INTERFACES              10060   // Get number of ethernet interfaces                                        (strvalue = struct eth_cfg [MOM_NUM_ETH_INTERFACES])
#define OID_PACKET_CAPTURE_MODE             10061   // take a radio into or out of packet capture mode
#define OID_BOND_PORT_MAP                   10062   // Set port map for a bond
#define OID_VAP_ENTRY                       10063   // Set port map for a bond
#define OID_MULTICAST_EXCLUDE_OLD           10064   // Add or del excluded multicast ipv4 addresses
#define OID_MULTICAST_FORWARD_OLD           10065   // Add or del forwarded multicast ipv4 addresses
#define OID_MULTICAST_FWD_VLAN              10066   // Add or del forwarded multicast vlan
#define OID_MDNS_FILTER                     10067   // Add or del mdns filter service
#define OID_11U_ROAM_CONSORTIUM             10068   // Add or del 802.11u roaming consortium OI
#define OID_11U_DOMAIN                      10069   // Add or del 802.11u domain name
#define OID_11U_CELL_NETWORK                10070   // Add or del 802.11u cellular network info                                 (strvalue = struct cell_net_info)
#define OID_11U_NAI_REALM                   10071   // Add or del 802.11u NAI realm                                             (strvalue = struct nai_realm_info)
#define OID_11U_NETWORK_AUTH_TYPE           10072   // Add or del 802.11u network authentication type info                      (strvalue = struct net_auth_type_info)
#define OID_HS20_CONN_CAP                   10073   // Add or del hotspot 2.0 connection capability info                        (strvalue = struct conn_cap_info)
#define OID_HS20_OPER_NAME                  10074   // Add or del hotspot 2.0 operator friendly name                            (strvalue = struct oper_name_info)
#define OID_11U_VENUE_NAME                  10075   // Add or del 802.11u venue name                                            (strvalue = struct venue_name_info)
#define OID_ACTIVE_VLAN_CLEARED             10076   // Return true if active VLAN table has been cleared
#define OID_DPI_APP_DESCRIPTION             10077   // Return detailed application description
#define OID_SSID_TUNNEL                     10078   // Set SSID tunnel (update tunnel SSID mask)
#define OID_GROUP_ENTRY                     10079   // Find User Group entry by name or RADIUS ID                               (strvalue = struct group_cfg)
#define OID_OAUTH_ENTRY                     10080   // Find OAUTH table entry                                                   (strvalue = struct oauth_entry)
#define OID_IAP_GLB_AC_CAPABILITIES         10081   // read only: bit 0=802.11ac capable (other bits tbd)
#define OID_IDS_AUTOBLOCK_WLIST             10082   // Rogue AP channel whitelist
#define OID_SSID_HONEYPOT_WLIST             10083   // Modify SSID honeypot whitelist
#define OID_DPI_APP_COUNT                   10084   // Return number of DPI applications and categories
#define OID_LOAD_BALANCE_WEIGHT_VHT         10085   // Set load balancing vht station weight factor
#define OID_LOAD_BALANCE_WEIGHT_STREAM      10086   // Set load balancing stream count station weight factor
#define OID_SSID_HONEYPOT_BCAST             10087   // Add or del honeypot broadcast ssids
#define OID_CLUSTER_RUNNING                 10088   // cluster running flag
#define OID_APP_LIST_ENTRY                  10089   // Application list entry
#define OID_LLDP_FA_CFG                     10090   // Get LLDP FA settings                                                     (strvalue = struct lldp_fa_cfg)
#define OID_PERSONAL_WIFI_SETUP             10091   // Set up Personal WiFi
#define OID_VLAN_POOL_ENTRY                 10092   // Add or del VLAN pool                                                     (strvalue = struct vlan_pool_cfg)
#define OID_SET_AUTOCELL_MIN_RSSI           10093   // Set autocell min rssi value
#define OID_SET_AUTOCELL_MAX_RSSI           10094   // Set autocell max rssi value
#define OID_IAP_GLB_ASSURANCE_DEBUG         10095   // Set radio assurance debug flag
#define OID_SSID_WRITE_ENTRY                10096   // Write entire ssid in one go
#define OID_MULTICAST_FIXED_ADDR            10097   // Add or del a multicast fixed address entry
#define OID_VLAN_SET_IPV6                   10098   // Set VLAN IPv6 addr (used by new IPv6 dhcpcd)
#define OID_MULTICAST_EXCLUDE               10099   // Add or del excluded multicast ipv4 or ipv6 addresses
#define OID_MULTICAST_FORWARD               10100   // Add or del forwarded multicast ipv4 or ipv6 addresses
#define OID_REMOTE_RECOVERY_ENABLE          10101   // Enable/disable/set remote image recovery using xrp beacons
#define OID_REMOTE_RECOVERY_IMAGE           10102   // Enable/disable/set remote image recovery using xrp beacons
#define OID_REMOTE_RECOVERY_IMAGE_CHECK     10103   // Verify is image is being advertised for remote recovery
#define OID_DEVICE_ID_USER_OUI_TABLE        10104   // Device ID User OUI Table entry (old)
#define OID_DEVICE_ID_USER_MAC_TABLE        10105   // Device ID User MAC Table entry
#define OID_IAP_GLB_AUTO_CELL_DEBUG         10106   // Set autocell debug mode
#define OID_HOSTAPD_MSG_CONTROL             10107   // Enable/disable hostapd communications

//---------------------------------------------------------
// Plumbing / Shared XSP Non-Table OIDs
//---------------------------------------------------------
#define OID_TUNNEL_ENTRY                    10120   // Tunnel entry                                                             (strvalue = struct tunnel_cfg)

//---------------------------------------------------------
// 802.11u & Hotspot 2.0 OIDS
//---------------------------------------------------------
#define OID_IS_11U( _x_ )                   ((_x_) == OID_11U_INTERWORKING        || \
                                             (_x_) == OID_11U_ACCESS_NETWORK_OPTS || \
                                             (_x_) == OID_11U_VENUE_GROUP         || \
                                             (_x_) == OID_11U_VENUE_TYPE          || \
                                             (_x_) == OID_11U_HESSID              || \
                                             (_x_) == OID_11U_IPV4_AVAIL          || \
                                             (_x_) == OID_11U_IPV6_AVAIL          || \
                                             (_x_) == OID_11U_ROAM_CONSORTIUM     || \
                                             (_x_) == OID_11U_DOMAIN              || \
                                             (_x_) == OID_11U_CELL_NETWORK        || \
                                             (_x_) == OID_11U_NAI_REALM           || \
                                             (_x_) == OID_11U_NETWORK_AUTH_TYPE   || \
                                             (_x_) == OID_11U_VENUE_NAME             )

#define OID_IS_HS20( _x_ )                  ((_x_) == OID_HS20_ENABLE             || \
                                             (_x_) == OID_HS20_DGAF               || \
                                             (_x_) == OID_HS20_WAN_LINK_STATUS    || \
                                             (_x_) == OID_HS20_WAN_DOWNLINK_SPEED || \
                                             (_x_) == OID_HS20_WAN_UPLINK_SPEED   || \
                                             (_x_) == OID_HS20_CONN_CAP           || \
                                             (_x_) == OID_HS20_OPER_NAME             )

//---------------------------------------------------------
// VLAN Entry [1x1]: (10014)
//---------------------------------------------------------
#define NUM_VLAN_CONFIG_OIDS                1
#define NUM_VLAN_CONFIG_OBJS                1

#define OFFSET_VLAN_CONFIG( _x_ )           OFFSET_OBJ(  0 , VLAN_CONFIG, OID_VLAN_ENTRY )                  // don't index this oid
#define OID_IS_VLAN_CONFIG( _x_ )           OID_IS_OBJ( _x_, VLAN_CONFIG, OID_VLAN_ENTRY )
#define OID_VLAN_CONFIG(    _x_ )           OID_OBJ(    _x_, VLAN_CONFIG, OID_VLAN_ENTRY )
#define IDX_VLAN_CONFIG(    _x_ )           IDX_OBJ(    _x_, VLAN_CONFIG, OID_VLAN_ENTRY )

//---------------------------------------------------------
// Filter List Entry [1x1]: (10029)
//---------------------------------------------------------
#define NUM_FILTER_LIST_MEMBER_OIDS         1
#define NUM_FILTER_LIST_MEMBER_OBJS         1

#define OFFSET_FILTER_LIST_MEMBER( _x_ )    OFFSET_OBJ(  0 , FILTER_LIST_MEMBER, OID_FILTER_LIST_ENTRY )    // don't index this oid
#define OID_IS_FILTER_LIST_MEMBER( _x_ )    OID_IS_OBJ( _x_, FILTER_LIST_MEMBER, OID_FILTER_LIST_ENTRY )
#define OID_FILTER_LIST_MEMBER(    _x_ )    OID_OBJ(    _x_, FILTER_LIST_MEMBER, OID_FILTER_LIST_ENTRY )
#define IDX_FILTER_LIST_MEMBER(    _x_ )    IDX_OBJ(    _x_, FILTER_LIST_MEMBER, OID_FILTER_LIST_ENTRY )

//---------------------------------------------------------
// Filter List Move [1x2]: (10029)
//---------------------------------------------------------
#define NUM_FILTER_LIST_MOVE_OIDS           2
#define NUM_FILTER_LIST_MOVE_OBJS           1

#define OFFSET_FILTER_LIST_MOVE( _x_ )      OFFSET_OBJ(  0 , FILTER_LIST_MOVE, EID_MOVE_FILTER_LIST_UP )    // don't index this oid
#define EID_IS_FILTER_LIST_MOVE( _x_ )      OID_IS_OBJ( _x_, FILTER_LIST_MOVE, EID_MOVE_FILTER_LIST_UP )
#define EID_FILTER_LIST_MOVE(    _x_ )      OID_OBJ(    _x_, FILTER_LIST_MOVE, EID_MOVE_FILTER_LIST_UP )
#define IDX_FILTER_LIST_MOVE(    _x_ )      IDX_OBJ(    _x_, FILTER_LIST_MOVE, EID_MOVE_FILTER_LIST_UP )

//---------------------------------------------------------
// Remote Image Entry [1x3]: (10034 - 10036)
//---------------------------------------------------------
#define NUM_REMOTE_IMAGE_OIDS               3
#define NUM_REMOTE_IMAGE_OBJS               1

#define OFFSET_REMOTE_IMAGE( _x_ )          OFFSET_OBJ(  0 , REMOTE_IMAGE, OID_REMOTE_IMAGE_NAME )          // don't index this oid
#define OID_IS_REMOTE_IMAGE( _x_ )          OID_IS_OBJ( _x_, REMOTE_IMAGE, OID_REMOTE_IMAGE_NAME )
#define OID_REMOTE_IMAGE(    _x_ )          OID_OBJ(    _x_, REMOTE_IMAGE, OID_REMOTE_IMAGE_NAME )
#define IDX_REMOTE_IMAGE(    _x_ )          IDX_OBJ(    _x_, REMOTE_IMAGE, OID_REMOTE_IMAGE_NAME )

//---------------------------------------------------------
// VLAN Set IPv4 Entry [1x3]: (10042 - 10044)
//---------------------------------------------------------
#define NUM_VLAN_SET_IPV4_OIDS              3
#define NUM_VLAN_SET_IPV4_OBJS              1

#define OFFSET_VLAN_SET_IPV4( _x_ )         OFFSET_OBJ(  0 , VLAN_SET_IPV4 , OID_VLAN_SET_IPV4_ADDR )         // don't index this oid
#define OID_IS_VLAN_SET_IPV4( _x_ )         OID_IS_OBJ( _x_, VLAN_SET_IPV4 , OID_VLAN_SET_IPV4_ADDR )
#define OID_VLAN_SET_IPV4(    _x_ )         OID_OBJ(    _x_, VLAN_SET_IPV4 , OID_VLAN_SET_IPV4_ADDR )
#define IDX_VLAN_SET_IPV4(    _x_ )         IDX_OBJ(    _x_, VLAN_SET_IPV4 , OID_VLAN_SET_IPV4_ADDR )

//---------------------------------------------------------
// Load Balance Entry [1x6]: (10045 - 10050)
//---------------------------------------------------------
#define NUM_LOAD_BALANCE_OIDS               6
#define NUM_LOAD_BALANCE_OBJS               1

#define OFFSET_LOAD_BALANCE( _x_ )          OFFSET_OBJ(  0 , LOAD_BALANCE, OID_LOAD_BALANCE_DEBUG )         // don't index this oid
#define OID_IS_LOAD_BALANCE( _x_ )          OID_IS_OBJ( _x_, LOAD_BALANCE, OID_LOAD_BALANCE_DEBUG )
#define OID_LOAD_BALANCE(    _x_ )          OID_OBJ(    _x_, LOAD_BALANCE, OID_LOAD_BALANCE_DEBUG )
#define IDX_LOAD_BALANCE(    _x_ )          IDX_OBJ(    _x_, LOAD_BALANCE, OID_LOAD_BALANCE_DEBUG )

//---------------------------------------------------------
// DBG Entry [1x6]: (10051 - 10056)
//---------------------------------------------------------
#define NUM_DBG_OIDS                        6
#define NUM_DBG_OBJS                        1

#define OFFSET_DBG( _x_ )                   OFFSET_OBJ(  0 , DBG, OID_DBG_LEVEL )                           // don't index this oid
#define OID_IS_DBG( _x_ )                   OID_IS_OBJ( _x_, DBG, OID_DBG_LEVEL )
#define OID_DBG(    _x_ )                   OID_OBJ(    _x_, DBG, OID_DBG_LEVEL )
#define IDX_DBG(    _x_ )                   IDX_OBJ(    _x_, DBG, OID_DBG_LEVEL )

//---------------------------------------------------------
// App List Entry [1x1]: (10089)
//---------------------------------------------------------
#define NUM_APP_LIST_MEMBER_OIDS            1
#define NUM_APP_LIST_MEMBER_OBJS            1

#define OFFSET_APP_LIST_MEMBER( _x_ )       OFFSET_OBJ(  0 , APP_LIST_MEMBER, OID_APP_LIST_ENTRY )          // don't index this oid
#define OID_IS_APP_LIST_MEMBER( _x_ )       OID_IS_OBJ( _x_, APP_LIST_MEMBER, OID_APP_LIST_ENTRY )
#define OID_APP_LIST_MEMBER(    _x_ )       OID_OBJ(    _x_, APP_LIST_MEMBER, OID_APP_LIST_ENTRY )
#define IDX_APP_LIST_MEMBER(    _x_ )       IDX_OBJ(    _x_, APP_LIST_MEMBER, OID_APP_LIST_ENTRY )

//---------------------------------------------------------
// Multicast Fixed Addr [1x1]: (10097)
//---------------------------------------------------------
#define NUM_FIXED_ADDR_OIDS                 1
#define NUM_FIXED_ADDR_OBJS                 1

#define OFFSET_FIXED_ADDR( _x_ )            OFFSET_OBJ(  0 , FIXED_ADDR, OID_MULTICAST_FIXED_ADDR )         // don't index this oid
#define OID_IS_FIXED_ADDR( _x_ )            OID_IS_OBJ( _x_, FIXED_ADDR, OID_MULTICAST_FIXED_ADDR )
#define OID_FIXED_ADDR(    _x_ )            OID_OBJ(    _x_, FIXED_ADDR, OID_MULTICAST_FIXED_ADDR )
#define IDX_FIXED_ADDR(    _x_ )            IDX_OBJ(    _x_, FIXED_ADDR, OID_MULTICAST_FIXED_ADDR )

#endif //_XIROID_H
