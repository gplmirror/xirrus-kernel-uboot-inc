//* xirendian.h - byte order definitions for xirrus
/**
 * @file xirendian.h
 * @brief byte order definitions for xirrus
 * @remarks create QCA-required endian defines from system defines.
 *
 * NOTE: This file is now shared between many projects - including the driver and apps. As such,
 * be careful about what you add in here or some of them may no longer compile. In particular, try to
 * keep include files to a minimum. The only declarations here should be those that pertain to the
 * IOCTL interface between the driver and userland apps. (All the declarations should be here BTW
 * otherwise we end up duplicating declarations upstairs).
 */
#ifndef XIRENDIAN_H
#define XIRENDIAN_H

#ifndef DARWIN
#include <asm/byteorder.h>
#endif

#if   defined(__BYTE_ORDER) ? __BYTE_ORDER == __BIG_ENDIAN    : defined(__BIG_ENDIAN)
# define       _BIG_ENDIAN    __BIG_ENDIAN
# define       _LITTLE_ENDIAN   1234
# define       _BYTE_ORDER    __BIG_ENDIAN
#elif defined(__BYTE_ORDER) ? __BYTE_ORDER == __LITTLE_ENDIAN : defined(__LITTLE_ENDIAN)
# define       _BIG_ENDIAN      4321
# define       _LITTLE_ENDIAN __LITTLE_ENDIAN
# define       _BYTE_ORDER    __LITTLE_ENDIAN
#else
# error      "__BYTE_ORDER not defined"
#endif

#endif //XIRENDIAN_H
