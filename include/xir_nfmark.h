//******************************************************************************
/** @file xir_nfmark.h
 *
 * macros for the setting and reading xirrus defined skb->nfmark (net filter mark) fields
 *
 * <I>Copyright (C) 2007 Xirrus. All Rights Reserved</I>
 **/
//------------------------------------------------------------------------------
#ifndef _XIR_NFMARK_H
#define _XIR_NFMARK_H

// NOTE: FILTER_MARK_DPI_REINJECTED_FLAG must match NFMARK_DPI_REINJECTED definition in nf_conntrack.h in the kernel

#include <bitfield.h>

//****************************************************************
// net filter mark (nfmark) field definitions
//----------------------------------------------------------------
#define        FILTER_MARK_SSID_SRC_FLAG        bit_const(31)
#define        FILTER_MARK_SSID_DST_FLAG        bit_const(30)
#define        FILTER_MARK_GROUP_SRC_FLAG       bit_const(29)
#define        FILTER_MARK_GROUP_DST_FLAG       bit_const(28)
#define        FILTER_MARK_DEVICE_SRC_FLAG      bit_const(27)
#define        FILTER_MARK_DEVICE_DST_FLAG      bit_const(26)

#define        FILTER_MARK_SET_VLAN_IDX_FLAG    bit_const(27)
#define        FILTER_MARK_SET_QOS_FLAG         bit_const(26)
#define        FILTER_MARK_DEVICE_FLAG          bit_const(25)
#define        FILTER_MARK_DPI_REINJECTED_FLAG  bit_const(24)

#define        FILTER_MARK_SSID_SRC_MASK        field_mask( 3,  0)
#define        FILTER_MARK_SSID_DST_MASK        field_mask( 7,  4)
#define        FILTER_MARK_GROUP_SRC_MASK       field_mask(11,  8)
#define        FILTER_MARK_GROUP_DST_MASK       field_mask(15, 12)
#define        FILTER_MARK_DEVICE_SRC_MASK      field_mask(19, 16)
#define        FILTER_MARK_DEVICE_DST_MASK      field_mask(23, 20)
#define        FILTER_MARK_SET_VLAN_IDX_MASK    field_mask(21, 16)
#define        FILTER_MARK_SET_QOS_MASK         field_mask(23, 22)

#define    GET_FILTER_MARK_SSID_SRC(  m)        get_field(  3,  0, m)
#define    GET_FILTER_MARK_SSID_DST(  m)        get_field(  7,  4, m)
#define    GET_FILTER_MARK_GROUP_SRC( m)        get_field( 11,  8, m)
#define    GET_FILTER_MARK_GROUP_DST( m)        get_field( 15, 12, m)
#define    GET_FILTER_MARK_DEVICE_SRC(m)        get_field( 19, 16, m)
#define    GET_FILTER_MARK_DEVICE_DST(m)        get_field( 23, 20, m)
#define    GET_FILTER_MARK_VLAN_IDX(  m)        get_field( 21, 16, m)
#define    GET_FILTER_MARK_QOS(       m)        get_field( 23, 22, m)

#define    SET_FILTER_MARK_SSID_SRC(  s)        set_field(  3,  0, s)
#define    SET_FILTER_MARK_SSID_DST(  s)        set_field(  7,  4, s)
#define    SET_FILTER_MARK_GROUP_SRC( s)        set_field( 11,  8, s)
#define    SET_FILTER_MARK_GROUP_DST( s)        set_field( 15, 12, s)
#define    SET_FILTER_MARK_DEVICE_SRC(s)        set_field( 19, 16, s)
#define    SET_FILTER_MARK_DEVICE_DST(s)        set_field( 23, 20, s)
#define    SET_FILTER_MARK_VLAN_IDX(  v)        set_field( 21, 16, v)
#define    SET_FILTER_MARK_QOS(       q)        set_field( 23, 22, q)

#define ASSIGN_FILTER_MARK_SSID_SRC(  m, s)     (m = ((m) & ~(FILTER_MARK_SSID_SRC_MASK                               )) | FILTER_MARK_SSID_SRC_FLAG                                | SET_FILTER_MARK_SSID_SRC(  s))
#define ASSIGN_FILTER_MARK_SSID_DST(  m, s)     (m = ((m) & ~(FILTER_MARK_SSID_DST_MASK                               )) | FILTER_MARK_SSID_DST_FLAG                                | SET_FILTER_MARK_SSID_DST(  s))
#define ASSIGN_FILTER_MARK_GROUP_SRC( m, s)     (m = ((m) & ~(FILTER_MARK_GROUP_SRC_MASK                              )) | FILTER_MARK_GROUP_SRC_FLAG                               | SET_FILTER_MARK_GROUP_SRC( s))
#define ASSIGN_FILTER_MARK_GROUP_DST( m, s)     (m = ((m) & ~(FILTER_MARK_GROUP_DST_MASK                              )) | FILTER_MARK_GROUP_DST_FLAG                               | SET_FILTER_MARK_GROUP_DST( s))
#define ASSIGN_FILTER_MARK_DEVICE_SRC(m, s)     (m = ((m) & ~(FILTER_MARK_DEVICE_SRC_MASK   |  FILTER_MARK_DEVICE_FLAG)) | FILTER_MARK_DEVICE_SRC_FLAG   |  FILTER_MARK_DEVICE_FLAG | SET_FILTER_MARK_DEVICE_SRC(s))
#define ASSIGN_FILTER_MARK_DEVICE_DST(m, s)     (m = ((m) & ~(FILTER_MARK_DEVICE_DST_MASK   |  FILTER_MARK_DEVICE_FLAG)) | FILTER_MARK_DEVICE_DST_FLAG   |  FILTER_MARK_DEVICE_FLAG | SET_FILTER_MARK_DEVICE_DST(s))
#define ASSIGN_FILTER_MARK_VLAN_IDX(  m, v)     (m = ((m) & ~(FILTER_MARK_SET_VLAN_IDX_MASK |  FILTER_MARK_DEVICE_FLAG)) | FILTER_MARK_SET_VLAN_IDX_FLAG | !FILTER_MARK_DEVICE_FLAG | SET_FILTER_MARK_VLAN_IDX(  v))
#define ASSIGN_FILTER_MARK_QOS(       m, q)     (m = ((m) & ~(FILTER_MARK_SET_QOS_MASK      |  FILTER_MARK_DEVICE_FLAG)) | FILTER_MARK_SET_QOS_FLAG      | !FILTER_MARK_DEVICE_FLAG | SET_FILTER_MARK_QOS(       q))

#define  MATCH_FILTER_MARK_SSID_SRC_MASK                     (FILTER_MARK_SSID_SRC_FLAG     |                              FILTER_MARK_SSID_SRC_MASK     )
#define  MATCH_FILTER_MARK_SSID_DST_MASK                     (FILTER_MARK_SSID_DST_FLAG     |                              FILTER_MARK_SSID_DST_MASK     )
#define  MATCH_FILTER_MARK_GROUP_SRC_MASK                    (FILTER_MARK_GROUP_SRC_FLAG    |                              FILTER_MARK_GROUP_SRC_MASK    )
#define  MATCH_FILTER_MARK_GROUP_DST_MASK                    (FILTER_MARK_GROUP_DST_FLAG    |                              FILTER_MARK_GROUP_DST_MASK    )
#define  MATCH_FILTER_MARK_DEVICE_SRC_MASK                   (FILTER_MARK_DEVICE_SRC_FLAG   |  FILTER_MARK_DEVICE_FLAG   | FILTER_MARK_DEVICE_SRC_MASK   )
#define  MATCH_FILTER_MARK_DEVICE_DST_MASK                   (FILTER_MARK_DEVICE_DST_FLAG   |  FILTER_MARK_DEVICE_FLAG   | FILTER_MARK_DEVICE_DST_MASK   )
#define TARGET_FILTER_MARK_SET_VLAN_IDX_MASK                 (FILTER_MARK_SET_VLAN_IDX_FLAG | !FILTER_MARK_DEVICE_FLAG   | FILTER_MARK_SET_VLAN_IDX_MASK )
#define TARGET_FILTER_MARK_SET_QOS_MASK                      (FILTER_MARK_SET_QOS_FLAG      | !FILTER_MARK_DEVICE_FLAG   | FILTER_MARK_SET_QOS_MASK      )

#define  MATCH_FILTER_MARK_SSID_SRC(  s)                     (FILTER_MARK_SSID_SRC_FLAG     |                              SET_FILTER_MARK_SSID_SRC(  s) )
#define  MATCH_FILTER_MARK_SSID_DST(  s)                     (FILTER_MARK_SSID_DST_FLAG     |                              SET_FILTER_MARK_SSID_DST(  s) )
#define  MATCH_FILTER_MARK_GROUP_SRC( s)                     (FILTER_MARK_GROUP_SRC_FLAG    |                              SET_FILTER_MARK_GROUP_SRC( s) )
#define  MATCH_FILTER_MARK_GROUP_DST( s)                     (FILTER_MARK_GROUP_DST_FLAG    |                              SET_FILTER_MARK_GROUP_DST( s) )
#define  MATCH_FILTER_MARK_DEVICE_SRC(s)                     (FILTER_MARK_DEVICE_SRC_FLAG   |  FILTER_MARK_DEVICE_FLAG   | SET_FILTER_MARK_DEVICE_SRC(s) )
#define  MATCH_FILTER_MARK_DEVICE_DST(s)                     (FILTER_MARK_DEVICE_DST_FLAG   |  FILTER_MARK_DEVICE_FLAG   | SET_FILTER_MARK_DEVICE_DST(s) )
#define TARGET_FILTER_MARK_VLAN_IDX(  v)                     (FILTER_MARK_SET_VLAN_IDX_FLAG | !FILTER_MARK_DEVICE_FLAG   | SET_FILTER_MARK_VLAN_IDX(  v) )
#define TARGET_FILTER_MARK_QOS(       q)                     (FILTER_MARK_SET_QOS_FLAG      | !FILTER_MARK_DEVICE_FLAG   | SET_FILTER_MARK_QOS(       q) )

#endif // _XIR_NFMARK_H


