
#include <linux/module.h>
#include <linux/xirrus/xapcrash.h>

// global bit to alert anyone interested that we've captured a crash this boot
// mainly used by the octeon watchdog to skip yipping and quickly reboot
int xirrus_system_crashed = 0;

// for xapcrash, primary crash capturing function
int (*xirrus_crash_capture_fcn)(const char* label, const char* details, struct pt_regs* regs) = 0;

// method of capturing kmsgs to the crash buffer
int (*xirrus_crash_console_fcn)(const char *text, size_t length) = 0;

// additional xapcrash support, warning system
int (*xirrus_crash_warning_fcn)(unsigned int warning_type, unsigned int warning_detail) = 0;

EXPORT_SYMBOL(xirrus_system_crashed);
EXPORT_SYMBOL(xirrus_crash_capture_fcn);
EXPORT_SYMBOL(xirrus_crash_console_fcn);
EXPORT_SYMBOL(xirrus_crash_warning_fcn);