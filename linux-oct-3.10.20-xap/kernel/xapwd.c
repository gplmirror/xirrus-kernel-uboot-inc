
#include <linux/module.h>
#include <linux/xirrus/xapwd.h>

unsigned int  wd_need_pet[WD_MAX_CORES]     = { 0 };
unsigned long last_wd_pet_jif[WD_MAX_CORES] = { 0 };
unsigned long watchdog_lock[WD_MAX_CORES]   = { 0 };

EXPORT_SYMBOL(wd_need_pet);
EXPORT_SYMBOL(last_wd_pet_jif);
EXPORT_SYMBOL(watchdog_lock);

unsigned long (*xirrus_wd_check_fcn)(int core) = 0;
EXPORT_SYMBOL(xirrus_wd_check_fcn);
