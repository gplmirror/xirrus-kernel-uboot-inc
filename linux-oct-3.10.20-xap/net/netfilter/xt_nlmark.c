/*
 *	xt_nlmark - Netfilter module to match NLMARK value
 *
 *	(C) 1999-2001 Marc Boucher <marc@mbsi.ca>
 *	Copyright © CC Computer Consultants GmbH, 2007 - 2008
 *	Jan Engelhardt <jengelh@medozas.de>
 *
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License version 2 as
 *	published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/skbuff.h>

#include <linux/netfilter/xt_nlmark.h>
#include <linux/netfilter/x_tables.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Marc Boucher <marc@mbsi.ca>");
MODULE_DESCRIPTION("Xtables: packet nlmark match");
MODULE_ALIAS("ipt_nlmark");
MODULE_ALIAS("ip6t_nlmark");

static bool
nlmark_mt(const struct sk_buff *skb, struct xt_action_param *par)
{
	const struct xt_nlmark_mtinfo1 *info = par->matchinfo;

	return ((skb->nlmark & info->mask) == info->mark) ^ info->invert;
}

static struct xt_match nlmark_mt_reg __read_mostly = {
	.name           = "nlmark",
	.revision       = 1,
	.family         = NFPROTO_UNSPEC,
	.match          = nlmark_mt,
	.matchsize      = sizeof(struct xt_nlmark_mtinfo1),
	.me             = THIS_MODULE,
};

static int __init nlmark_mt_init(void)
{
	return xt_register_match(&nlmark_mt_reg);
}

static void __exit nlmark_mt_exit(void)
{
	xt_unregister_match(&nlmark_mt_reg);
}

module_init(nlmark_mt_init);
module_exit(nlmark_mt_exit);
