#ifndef _NF_QUEUE_H
#define _NF_QUEUE_H
#include <net/netfilter/nf_conntrack.h>

#ifdef  CONFIG_XIRRUS_PARANOIA
#define EXTRA_NFQUEUE_CHECKS
#endif

/* Each queued (to userspace) skbuff has one of these. */
struct nf_queue_entry {
	struct list_head	list;
	struct sk_buff		*skb;
	unsigned int		id;

	struct nf_hook_ops	*elem;
	u_int8_t		pf;
	u16			size; /* sizeof(entry) + saved route keys */
	unsigned int		hook;
	struct net_device	*indev;
	struct net_device	*outdev;
	int			(*okfn)(struct sk_buff *);

#ifdef CONFIG_XIRRUS
        struct nf_conn          *nfct;
#endif
#ifdef  EXTRA_NFQUEUE_CHECKS
	struct net_device	*dev;
	struct nf_bridge_info   *bridge;
        int                     queue;
        u64                     nlmark;
#endif
	/* extra space to store route keys */
};

#ifdef  EXTRA_NFQUEUE_CHECKS
extern char (*nfct_app_names)[][MAX_DPI_APP_NAME];

#define dev(d)                  ((d) ? (d)->name : "none")
#define ptr(p)                  ((unsigned long)(p) & 0xfffffffff)
#define app(c)                  (((c) && (((struct nf_conn *)(c))->protocol & NLMARK_DPI_PROTOCOL_MASK) && nfct_app_names) ? (*nfct_app_names)[(((struct nf_conn *)(c))->protocol & NLMARK_DPI_PROTOCOL_MASK) % MAX_DPI_APP_INDEX] : "none")

#define cnt(e)                  (((e)->nfct) ? atomic_read(&((e)->nfct->ct_general.use)) : 0)
#define nf_dump_entry(t, c, e)  printk(KERN_INFO "%-8.8s : %-20.20s entry=%09lx, id=%-6d, queue=%d, skb=0x%09lx, nfct=0x%09lx, cnt=%d, app=%-6.6s, elem=0x%09lx, pf=%d, hook=%d, dev=%-6.6s, indev=%-6.6s, outdev=%-6.6s, bridge=0x%09lx, nlmark=0x%016lx, okfn=0x%09lx\n", \
                                                  t,       c,       ptr(e),      e->id,   e->queue, ptr(e->skb), ptr(e->nfct), cnt(e),app(e->nfct),ptr(e->elem), e->pf, e->hook, dev(e->dev),dev(e->indev),dev(e->outdev),ptr(e->bridge), ptr(e->nlmark) , ptr(e->okfn));
#else
#define cnt(e)                  0
#define nf_dump_entry(t, c, e)
#endif

#define nf_queue_entry_reroute(x) ((void *)x + sizeof(struct nf_queue_entry))

/* Packet queuing */
struct nf_queue_handler {
	int			(*outfn)(struct nf_queue_entry *entry,
					 unsigned int queuenum);
};

void nf_register_queue_handler(const struct nf_queue_handler *qh);
void nf_unregister_queue_handler(void);
extern void nf_reinject(struct nf_queue_entry *entry, unsigned int verdict);

bool nf_queue_entry_get_refs(struct nf_queue_entry *entry);
void nf_queue_entry_release_refs(struct nf_queue_entry *entry);

#if defined(CONFIG_IMQ) || defined(CONFIG_IMQ_MODULE)
extern void nf_register_queue_imq_handler(const struct nf_queue_handler *qh);
extern void nf_unregister_queue_imq_handler(void);
#endif

#endif /* _NF_QUEUE_H */
