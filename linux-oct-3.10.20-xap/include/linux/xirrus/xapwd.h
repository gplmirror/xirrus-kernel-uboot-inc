/*
 * Copyright (C) 2011 Xirrus Inc., All rights reserved.
 *
 * File:    xap_wd.h - Xirrus AP: watchdog include file
 *
 */
#ifndef _LINUX_XAPWD_H
#define _LINUX_XAPWD_H

#define XIRRUS_WD_PATHNAME     "/dev/xapwd"

/* exported vars */
#define WD_MAX_CORES 6
extern unsigned int  wd_need_pet[WD_MAX_CORES];
extern unsigned long last_wd_pet_jif[WD_MAX_CORES];
extern unsigned long watchdog_lock[WD_MAX_CORES];

extern unsigned long (*xirrus_wd_check_fcn)(int core); 

#endif /* __XAPWD_H__ */
