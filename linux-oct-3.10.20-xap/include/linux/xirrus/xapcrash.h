 
#ifndef _LINUX_XAPCRASH_H
#define _LINUX_XAPCRASH_H

// These definitions need to stay in sync with the logic in xapcrash.c (the module's file, not the kernel's)
// Defined here so they're accessible to various other kernel modules (currently EDAC and Watchdog).
#define XAPCRASH_WATCHDOG_YIP           0
#define XAPCRASH_CORRECTED_RAM_ERROR    1
#define XAPCRASH_UNCORRECTED_RAM_ERROR  2
#define XAPCRASH_UNKNOWN_RAM_ERROR      3

// global bit to alert anyone interested that we've captured a crash this boot
// mainly used by the octeon watchdog to skip yipping and quickly reboot
extern int xirrus_system_crashed;

// for xapcrash, primary crash capturing function
extern int (*xirrus_crash_capture_fcn)(const char* label, const char* details, struct pt_regs* regs);

// method of capturing kmsgs to the crash buffer
extern int (*xirrus_crash_console_fcn)(const char *text, size_t length);

// additional xapcrash support, warning system
extern int (*xirrus_crash_warning_fcn)(unsigned int warning_type, unsigned int warning_detail);

#endif