//******************************************************************************
// bitfield macros
//------------------------------------------------------------------------------
#ifndef _BITFIELD_OPS
#define _BITFIELD_OPS

#define bit_const(b)            (1 << (b))

#define get_field(b1,b2,n)      (((n) >> (b2)) & ((1 << ((b1)-(b2)+1))-1))
#define set_field(b1,b2,n)      (((n) & ((1 << ((b1)-(b2)+1))-1)) << (b2))

#define field_mask(b1,b2)       (set_field(b1,b2,-1))
#define update_field(b1,b2,m,n) (set_field(b1,b2, n) | (m & ~field_mask(b1,b2)))

#define get_nibble(w,n)         (((w) >> (n)*4) & 0xf)

#endif

