#ifndef __OCTEON_XARRAY_SHARED_H__
#define __OCTEON_XARRAY_SHARED_H__

//##################
//##################
//### spd values ###
//##################
//##################

#define OCTEON_XARRAY_XR600_CN60XX_SPD_VALUES\
   0x92,\
   0x11,\
   0x0B,\
   0x03,\
   0x04,  /* 0x03 for 512MB DDR, 0x04 for 1GB DDR */ \
   0x19,  /* 0x11 for 512MB DDR, 0x19 for 1GB DDR */ \
   0x02,\
   0x02,\
   0x02,\
   0x11,\
   0x01,\
   0x08,\
   0x0A,\
   0x00,\
   0xFC,\
   0x00,\
   0x6E,\
   0x78,\
   0x6E,\
   0x3C,\
   0x6E,\
   0x11,\
   0x18,\
   0x86,\
   0x00,\
   0x05,\
   0x3C,\
   0x3C,\
   0x01,\
   0x40,\
   0x83,\
   0x05,\
   0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
   0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0F,0x01,0x02,0x00

#define OCTEON_XARRAY_XR700_CN70XX_SPD_VALUES \
   0x92, /*00 --> 256-byte spd*/ \
   0x11, /*01 (not accessed)*/ \
   0x0b, /*02 (not accessed)*/ \
   0x06, /*03 --> Mini-UDIMM*/ \
   0x04, /*04 --> 4Gbit chips*/ \
   0x19, /*05 --> 15 rows x 10 columns*/ \
   0x02, /*06 --> 1.35V*/ \
   0x02, /*07 --> 1 rank with x16 chips*/ \
   0x04, /*08 --> 32-bit module width*/ \
   0x11, /*09 (not accessed)*/ \
   0x01, /*10 --> medium timebase dividend*/ \
   0x08, /*11 --> medium timebase divisor*/ \
   0x0c, /*12 --> DDR3 667MHz*/ \
   0x00, /*13 (not accessed)*/ \
   0x7e, /*14 #--> CAS latency  4-11*/ \
   0x00, /*15 --> CAS latency 12-18*/ \
   0x69, /*16 --> taa-min*/ \
   0x78, /*17 --> twr-min*/ \
   0x69, /*18 --> trcd-min*/ \
   0x30, /*19 --> trrd-min*/ \
   0x69, /*20 --> trp-min*/ \
   0x11, /*21 --> tras+trc upper nibble*/ \
   0x20, /*22 --> tras-min*/ \
   0x89, /*23 --> trc-min*/ \
   0x20, /*24 --> trfc-min lsb*/ \
   0x08, /*25 --> trfc-min msb*/ \
   0x3c, /*26 --> twtr-min*/ \
   0x3c, /*27 --> trtp-min*/ \
   0x00, /*28 --> tfaw-min msb*/ \
   0xf0, /*29 --> tfaw-min lsb*/ \
   0x83, /*30 (not accessed)*/ \
   0x05, /*31 (not accessed)*/ \
   0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
   0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
   0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
   0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
   0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
   0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xc5,0xb9,

#define OCTEON_XARRAY_XR1K_CN52XX_SPD_VALUES\
   0x80,\
   0x08,\
   0x08,\
   0x0e,\
   0x0a,\
   0x60,\
   0x20,\
/* 0x48, module data width */ \
   0x00,\
   0x05,\
   0x25,\
   0x40,\
   0x00,\
/* 0x06, ecc */ \
   0x82,\
   0x08,\
   0x08,\
   0x00,\
   0x0c,\
   0x08,\
   0x70,\
   0x01,\
   0x00,\
/* 0x01, dimm type */ \
   0x04,\
   0x07,\
   0x30,\
   0x45,\
   0x3d,\
   0x50,\
   0x3c,\
   0x28,\
   0x3c,\
   0x2d,\
   0x01,\
   0x17,0x25,0x05,0x12,0x3c,0x1e,0x1e,0x00,0x06,0x3c,0x7f,0x80,0x14,0x1e,0x0f,0x00,\
   0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x12,0x12

#define OCTEON_XARRAY_XR2K_CN61XX_SPD_VALUES\
   0x92,\
   0x11,\
   0x0B,\
   0x03,\
   0x04,\
   0x19,\
   0x02,\
   0x02,\
   0x02,\
   0x11,\
   0x01,\
   0x08,\
   0x0A,\
   0x00,\
   0xFC,\
   0x00,\
   0x6E,\
   0x78,\
   0x6E,\
   0x3C,\
   0x6E,\
   0x11,\
   0x18,\
   0x86,\
   0x00,\
   0x05,\
   0x3C,\
   0x3C,\
   0x01,\
   0x40,\
   0x83,\
   0x05,\
   0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
   0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0F,0x01,0x02,0x00

//####################
//####################
//### board delays ###
//####################
//####################

/*
** CN52xx Registered configuration
*/
#define OCTEON_XARRAY_CN52XX_DDR_BOARD_DELAY            4900
#define OCTEON_XARRAY_CN52XX_LMC_DELAY_CLK              3
#define OCTEON_XARRAY_CN52XX_LMC_DELAY_CMD              0
#define OCTEON_XARRAY_CN52XX_LMC_DELAY_DQ               2

/*
** CN52xx Unbuffered configuration
*/
#define OCTEON_XARRAY_CN50XX_UNB_DDR_BOARD_DELAY        2625
#define OCTEON_XARRAY_CN50XX_UNB_LMC_DELAY_CLK          3
#define OCTEON_XARRAY_CN50XX_UNB_LMC_DELAY_CMD          0
#define OCTEON_XARRAY_CN50XX_UNB_LMC_DELAY_DQ           3

#define OCTEON_XARRAY_CN52XX_UNB_DDR_BOARD_DELAY        4000
#define OCTEON_XARRAY_CN52XX_UNB_LMC_DELAY_CLK          6
#define OCTEON_XARRAY_CN52XX_UNB_LMC_DELAY_CMD          0
#define OCTEON_XARRAY_CN52XX_UNB_LMC_DELAY_DQ           6

#define OCTEON_XARRAY_XR1K_CN52XX_UNB_DDR_BOARD_DELAY   4000
#define OCTEON_XARRAY_XR1K_CN52XX_UNB_LMC_DELAY_CLK     6
#define OCTEON_XARRAY_XR1K_CN52XX_UNB_LMC_DELAY_CMD     0
#define OCTEON_XARRAY_XR1K_CN52XX_UNB_LMC_DELAY_DQ      6

#define OCTEON_XARRAY_XR2K_CN52XX_UNB_DDR_BOARD_DELAY   4000
#define OCTEON_XARRAY_XR2K_CN52XX_UNB_LMC_DELAY_CLK     4
#define OCTEON_XARRAY_XR2K_CN52XX_UNB_LMC_DELAY_CMD     0
#define OCTEON_XARRAY_XR2K_CN52XX_UNB_LMC_DELAY_DQ      6

//#############
//#############
//### XR500 ###
//#############
//#############

#define OCTEON_XARRAY_CN50XX_DDR_CONFIGURATION                          \
{                                                                       \
    .dimm_config_table   = { OCTEON_XARRAY_CN50XX_DRAM_SOCKET_CONFIGURATION, \
                             DIMM_CONFIG_TERMINATOR },                  \
    .unbuffered = {                                                     \
        .ddr_board_delay =   OCTEON_XARRAY_CN50XX_UNB_DDR_BOARD_DELAY,  \
        .lmc_delay_clk   =   OCTEON_XARRAY_CN50XX_UNB_LMC_DELAY_CLK,    \
        .lmc_delay_cmd   =   OCTEON_XARRAY_CN50XX_UNB_LMC_DELAY_CMD,    \
        .lmc_delay_dq    =   OCTEON_XARRAY_CN50XX_UNB_LMC_DELAY_DQ,     \
        },                                                              \
    .registered = {                                                     \
        .ddr_board_delay =   OCTEON_XARRAY_CN52XX_DDR_BOARD_DELAY,      \
        .lmc_delay_clk   =   OCTEON_XARRAY_CN52XX_LMC_DELAY_CLK,        \
        .lmc_delay_cmd   =   OCTEON_XARRAY_CN52XX_LMC_DELAY_CMD,        \
        .lmc_delay_dq    =   OCTEON_XARRAY_CN52XX_LMC_DELAY_DQ,         \
        },                                                              \
    .odt_1rank_config    = { OCTEON_XARRAY_CN50XX_DRAM_ODT_1RANK_CONFIGURATION }, \
    .odt_2rank_config    = { OCTEON_XARRAY_CN50XX_DRAM_ODT_2RANK_CONFIGURATION }, \
}

#define OCTEON_XARRAY_CN50XX_DRAM_SOCKET_CONFIGURATION \
    {{0, 0}, {octeon_xarray_xr1k_cn52xx_spd_values, NULL}}

/* Note: CN50XX RODT_ENA 0 = disabled, 1 = Weak Read ODT, 2 = Strong Read ODT */
#define OCTEON_XARRAY_CN50XX_DRAM_ODT_1RANK_CONFIGURATION \
    /* DIMMS   RODT_ENA LMC_ODT_CTL  reserved       QS_DIC RODT_CTL DIC */ \
    /* =====   ======== ============ ============== ====== ======== === */ \
    /*   1 */ {   2,    0x00000001,  {.u64=0x0000},    1,   0x0000,  0  }, \
    /*   2 */ {   2,    0x00010001,  {.u64=0x0000},    2,   0x0000,  0  }, \
    /*   3 */ {   2,    0x01040104,  {.u64=0x0000},    2,   0x0000,  0  }, \
    /*   4 */ {   2,    0x01040104,  {.u64=0x0000},    2,   0x0000,  0  }

/* Note: CN50XX RODT_ENA 0 = disabled, 1 = Weak Read ODT, 2 = Strong Read ODT */
#define OCTEON_XARRAY_CN50XX_DRAM_ODT_2RANK_CONFIGURATION \
    /* DIMMS   RODT_ENA LMC_ODT_CTL  reserved       QS_DIC RODT_CTL DIC */ \
    /* =====   ======== ============ ============== ====== ======== === */ \
    /*   1 */ {   2,    0x00000011,  {.u64=0x0000},    1,   0x0000,  0  }, \
    /*   2 */ {   2,    0x00110011,  {.u64=0x0000},    2,   0x0000,  0  }, \
    /*   3 */ {   2,    0x11441144,  {.u64=0x0000},    3,   0x0000,  0  }, \
    /*   4 */ {   2,    0x11441144,  {.u64=0x0000},    3,   0x0000,  0  }

//#############
//#############
//### XR600 ###
//#############
//#############

#define OCTEON_XARRAY_XR600_CN60XX_DDR_CONFIGURATION                \
/* Interface 0 */                                                   \
{                                                                   \
    .custom_lmc_config = {                                          \
        .min_rtt_nom_idx    = 2,                                    \
        .max_rtt_nom_idx    = 5,                                    \
        .min_rodt_ctl       = 1,                                    \
        .max_rodt_ctl       = 4,                                    \
        .dqx_ctl            = 4,                                    \
        .ck_ctl             = 4,                                    \
        .cmd_ctl            = 4,                                    \
        .min_cas_latency    = 7,                                    \
        .offset_en          = 0,                                    \
        .offset_udimm       = 2,                                    \
        .offset_rdimm       = 1,                                    \
        .ddr_rtt_nom_auto   = 0,                                    \
        .ddr_rodt_ctl_auto  = 0,                                    \
        .rlevel_compute     = 1,                                    \
        .ddr2t_udimm 	    = 1,                                    \
        .ddr2t_rdimm 	    = 1,                                    \
        .maximum_adjacent_rlevel_delay_increment = 2,               \
        .fprch2		    = 1,                                    \
        .parity		    = 0,                                    \
        .mode32b	    = 1                                     \
    },                                                              \
    .dimm_config_table = {                                          \
        OCTEON_XARRAY_XR600_CN60XX_DRAM_SOCKET_CONFIGURATION,       \
        DIMM_CONFIG_TERMINATOR,                                     \
    },                                                              \
    .unbuffered = {                                                 \
        .ddr_board_delay = 0,                                       \
        .lmc_delay_clk   = 0,                                       \
        .lmc_delay_cmd   = 0,                                       \
        .lmc_delay_dq    = 0,                                       \
    },                                                              \
    .registered = {                                                 \
        .ddr_board_delay = 0,                                       \
        .lmc_delay_clk   = 0,                                       \
        .lmc_delay_cmd   = 0,                                       \
        .lmc_delay_dq    = 0,                                       \
    },                                                              \
    .odt_1rank_config = {                                           \
        OCTEON_XARRAY_CN63XX_DRAM_ODT_1RANK_CONFIGURATION,          \
    }                                                               \
}

#define OCTEON_XARRAY_XR600_CN60XX_DRAM_SOCKET_CONFIGURATION \
    {{0, 0}, {octeon_xarray_xr600_cn60xx_spd_values, NULL}}

//#############
//#############
//### XR700 ###
//#############
//#############

#define OCTEON_XARRAY_XR700_CN70XX_DDR_CONFIGURATION {			\
	/* Interface 0 */						\
	.custom_lmc_config = {						\
		.min_rtt_nom_idx	= 1,				\
		.max_rtt_nom_idx	= 5,				\
		.min_rodt_ctl		= 1,				\
		.max_rodt_ctl		= 5,				\
		.dqx_ctl		= 4,				\
		.ck_ctl			= 4,				\
		.cmd_ctl		= 4,				\
		.ctl_ctl		= 4,				\
		.min_cas_latency	= 0,				\
		.offset_en 		= 1,				\
		.offset_udimm		= 2,				\
		.offset_rdimm		= 2,				\
		.ddr_rtt_nom_auto	= 0,				\
		.ddr_rodt_ctl_auto	= 0,				\
		.rlevel_compute 	= 0,				\
		.ddr2t_udimm 		= 1,				\
		.ddr2t_rdimm 		= 1,				\
                .maximum_adjacent_rlevel_delay_increment = 2,           \
		.fprch2			= 2,				\
		.parity			= 0,				\
		.mode32b		= 1,				\
                .dll_write_offset	= {0, 0, 0, 0, 0, 0, 0, 0, 0},  \
                .dll_read_offset 	= {0, 0, 0, 0, 0, 0, 0, 0, 0},  \
	},								\
        .dimm_config_table = {						\
		OCTEON_XARRAY_XR700_CN70XX_DRAM_SOCKET_CONFIGURATION,	\
		DIMM_CONFIG_TERMINATOR,					\
	},								\
	.unbuffered = {							\
		.ddr_board_delay = 0,					\
		.lmc_delay_clk = 0,					\
		.lmc_delay_cmd = 0,					\
		.lmc_delay_dq = 0,					\
	},								\
	.registered = {							\
		.ddr_board_delay = 0,					\
		.lmc_delay_clk = 0,					\
		.lmc_delay_cmd = 0,					\
		.lmc_delay_dq = 0,					\
	},								\
	.odt_1rank_config = {                        			\
		OCTEON_XARRAY_CN70XX_DRAM_ODT_1RANK_CONFIGURATION,      \
	},                                                              \
}

#define OCTEON_XARRAY_XR700_CN70XX_DRAM_SOCKET_CONFIGURATION \
	{{0x0, 0x0}, {octeon_xarray_xr700_cn70xx_spd_values, NULL}}

#define OCTEON_XARRAY_CN70XX_DRAM_ODT_1RANK_CONFIGURATION \
	/* DIMMS   reserved  WODT_MASK                LMCX_MODEREG_PARAMS1                   RODT_CTL    RODT_MASK    reserved */  \
	/* =====   ======== ============== ==========================================        ========= ============== ======== */  \
	/*   1 */ {   0,    0x00000001ULL, OCTEON_XARRAY_CN70XX_MODEREG_PARAMS1_1RANK_1SLOT,     3,     0x00000000ULL,  0  }	   \

#define OCTEON_XARRAY_CN70XX_MODEREG_PARAMS1_1RANK_1SLOT \
{							\
	.cn63xx = {					\
		.pasr_00	= 0,			\
		.asr_00		= 0,			\
		.srt_00		= 0,			\
		.rtt_wr_00	= 0,			\
		.dic_00		= 0,			\
		.rtt_nom_00	= rttnom_40ohm,		\
		.pasr_01	= 0,			\
		.asr_01		= 0,			\
		.srt_01		= 0,			\
		.rtt_wr_01	= 0,			\
		.dic_01		= 0,			\
		.rtt_nom_01	= 0,			\
		.pasr_10	= 0,			\
		.asr_10		= 0,			\
		.srt_10		= 0,			\
		.rtt_wr_10	= 0,			\
		.dic_10		= 0,			\
		.rtt_nom_10	= 0,			\
		.pasr_11	= 0,			\
		.asr_11		= 0,			\
		.srt_11		= 0,			\
		.rtt_wr_11	= 0,			\
		.dic_11		= 0,			\
		.rtt_nom_11	= 0			\
	}						\
}

//##############
//##############
//### XR1000 ###
//##############
//##############

#define OCTEON_XARRAY_XR1K_CN52XX_DDR_CONFIGURATION                     \
{                                                                       \
    .dimm_config_table   = { OCTEON_XARRAY_XR1K_CN52XX_DRAM_SOCKET_CONFIGURATION, \
                             DIMM_CONFIG_TERMINATOR },                  \
    .unbuffered = {                                                     \
        .ddr_board_delay =   OCTEON_XARRAY_XR1K_CN52XX_UNB_DDR_BOARD_DELAY, \
        .lmc_delay_clk   =   OCTEON_XARRAY_XR1K_CN52XX_UNB_LMC_DELAY_CLK,   \
        .lmc_delay_cmd   =   OCTEON_XARRAY_XR1K_CN52XX_UNB_LMC_DELAY_CMD,   \
        .lmc_delay_dq    =   OCTEON_XARRAY_XR1K_CN52XX_UNB_LMC_DELAY_DQ,    \
        },                                                              \
    .registered = {                                                     \
        .ddr_board_delay =   OCTEON_XARRAY_CN52XX_DDR_BOARD_DELAY,      \
        .lmc_delay_clk   =   OCTEON_XARRAY_CN52XX_LMC_DELAY_CLK,        \
        .lmc_delay_cmd   =   OCTEON_XARRAY_CN52XX_LMC_DELAY_CMD,        \
        .lmc_delay_dq    =   OCTEON_XARRAY_CN52XX_LMC_DELAY_DQ,         \
        },                                                              \
    .odt_1rank_config    = { OCTEON_XARRAY_CN52XX_DRAM_ODT_1RANK_CONFIGURATION }, \
    .odt_2rank_config    = { OCTEON_XARRAY_CN52XX_DRAM_ODT_2RANK_CONFIGURATION }, \
}

#define OCTEON_XARRAY_XR1K_CN52XX_DRAM_SOCKET_CONFIGURATION \
    {{0, 0}, {octeon_xarray_xr1k_cn52xx_spd_values, NULL}}

//##############
//##############
//### XR2000 ###
//##############
//##############

#define OCTEON_XARRAY_XR2K_CN52XX_DDR_CONFIGURATION                     \
{                                                                       \
    .dimm_config_table   = { OCTEON_XARRAY_XR2K_DRAM_SOCKET_CONFIGURATION,   \
                             DIMM_CONFIG_TERMINATOR },                  \
    .unbuffered = {                                                     \
        .ddr_board_delay =   OCTEON_XARRAY_XR2K_CN52XX_UNB_DDR_BOARD_DELAY,  \
        .lmc_delay_clk   =   OCTEON_XARRAY_XR2K_CN52XX_UNB_LMC_DELAY_CLK,    \
        .lmc_delay_cmd   =   OCTEON_XARRAY_XR2K_CN52XX_UNB_LMC_DELAY_CMD,    \
        .lmc_delay_dq    =   OCTEON_XARRAY_XR2K_CN52XX_UNB_LMC_DELAY_DQ,     \
        },                                                              \
    .registered = {                                                     \
        .ddr_board_delay =   OCTEON_XARRAY_CN52XX_DDR_BOARD_DELAY,      \
        .lmc_delay_clk   =   OCTEON_XARRAY_CN52XX_LMC_DELAY_CLK,        \
        .lmc_delay_cmd   =   OCTEON_XARRAY_CN52XX_LMC_DELAY_CMD,        \
        .lmc_delay_dq    =   OCTEON_XARRAY_CN52XX_LMC_DELAY_DQ,         \
        },                                                              \
    .odt_1rank_config    = { OCTEON_XARRAY_CN52XX_DRAM_ODT_1RANK_CONFIGURATION },     \
    .odt_2rank_config    = { OCTEON_XARRAY_CN52XX_DRAM_ODT_2RANK_CONFIGURATION },     \
}

#define OCTEON_XARRAY_XR2K_DRAM_SOCKET_CONFIGURATION \
    {{0x51, 0}, {NULL, NULL}}  // spd address = 0x51

//##############
//##############
//### XR2100 ###
//##############
//##############

#define OCTEON_XARRAY_XR2K_CN61XX_DDR_CONFIGURATION                 \
/* Interface 0 */                                                   \
{                                                                   \
    .custom_lmc_config = {                                          \
        .min_rtt_nom_idx    = 2,                                    \
        .max_rtt_nom_idx    = 5,                                    \
        .min_rodt_ctl       = 1,                                    \
        .max_rodt_ctl       = 4,                                    \
        .dqx_ctl            = 4,                                    \
        .ck_ctl             = 4,                                    \
        .cmd_ctl            = 4,                                    \
        .min_cas_latency    = 7,                                    \
        .offset_en          = 0,                                    \
        .offset_udimm       = 2,                                    \
        .offset_rdimm       = 1,                                    \
        .ddr_rtt_nom_auto   = 0,                                    \
        .ddr_rodt_ctl_auto  = 0,                                    \
        .rlevel_compute     = 1,                                    \
        .ddr2t_udimm 	    = 1,                                    \
        .ddr2t_rdimm 	    = 1,                                    \
        .maximum_adjacent_rlevel_delay_increment = 2,               \
        .fprch2		    = 1,                                    \
        .parity		    = 0,                                    \
        .mode32b	    = 1                                     \
    },                                                              \
    .dimm_config_table = {                                          \
        OCTEON_XARRAY_XR2K_CN61XX_DRAM_SOCKET_CONFIGURATION,        \
        DIMM_CONFIG_TERMINATOR,                                     \
    },                                                              \
    .unbuffered = {                                                 \
        .ddr_board_delay = 0,                                       \
        .lmc_delay_clk   = 0,                                       \
        .lmc_delay_cmd   = 0,                                       \
        .lmc_delay_dq    = 0,                                       \
    },                                                              \
    .registered = {                                                 \
        .ddr_board_delay = 0,                                       \
        .lmc_delay_clk   = 0,                                       \
        .lmc_delay_cmd   = 0,                                       \
        .lmc_delay_dq    = 0,                                       \
    },                                                              \
    .odt_1rank_config = {                                           \
        OCTEON_XARRAY_CN63XX_DRAM_ODT_1RANK_CONFIGURATION,          \
    }                                                               \
}

#define OCTEON_XARRAY_XR2K_CN61XX_DRAM_SOCKET_CONFIGURATION \
    {{0, 0}, {octeon_xarray_xr2k_cn61xx_spd_values, NULL}}


//##############
//##############
//### XR4000 ###
//##############
//##############

#define OCTEON_XARRAY_CN52XX_DDR_CONFIGURATION                          \
{                                                                       \
    .dimm_config_table   = { OCTEON_XARRAY_DRAM_SOCKET_CONFIGURATION,   \
                             DIMM_CONFIG_TERMINATOR },                  \
    .unbuffered = {                                                     \
        .ddr_board_delay =   OCTEON_XARRAY_CN52XX_UNB_DDR_BOARD_DELAY,  \
        .lmc_delay_clk   =   OCTEON_XARRAY_CN52XX_UNB_LMC_DELAY_CLK,    \
        .lmc_delay_cmd   =   OCTEON_XARRAY_CN52XX_UNB_LMC_DELAY_CMD,    \
        .lmc_delay_dq    =   OCTEON_XARRAY_CN52XX_UNB_LMC_DELAY_DQ,     \
        },                                                              \
    .registered = {                                                     \
        .ddr_board_delay =   OCTEON_XARRAY_CN52XX_DDR_BOARD_DELAY,      \
        .lmc_delay_clk   =   OCTEON_XARRAY_CN52XX_LMC_DELAY_CLK,        \
        .lmc_delay_cmd   =   OCTEON_XARRAY_CN52XX_LMC_DELAY_CMD,        \
        .lmc_delay_dq    =   OCTEON_XARRAY_CN52XX_LMC_DELAY_DQ,         \
        },                                                              \
    .odt_1rank_config    = { OCTEON_XARRAY_CN52XX_DRAM_ODT_1RANK_CONFIGURATION },     \
    .odt_2rank_config    = { OCTEON_XARRAY_CN52XX_DRAM_ODT_2RANK_CONFIGURATION },     \
}

#define OCTEON_XARRAY_DRAM_SOCKET_CONFIGURATION \
    {{0x55, 0}, {NULL, NULL}}  // spd address = 0x55

/* Note: CN52XX RODT_ENA 0 = disabled, 1 = Weak Read ODT, 2 = Strong Read ODT */
#define OCTEON_XARRAY_CN52XX_DRAM_ODT_1RANK_CONFIGURATION \
    /* DIMMS   RODT_ENA  WODT_CTL0     WODT_CTL1        QS_DIC RODT_CTL DIC */ \
    /* =====   ======== ============ ================== ====== ======== === */ \
    /*   1 */ {   2,     0x00000100, {.u64=0x00000000},    3,   0x0001,  0  },  \
    /*   2 */ {   2,     0x01000400, {.u64=0x00000000},    3,   0x0104,  0  },  \
    /*   3 */ {   2,     0x01000400, {.u64=0x00000400},    3,   0x0104,  0  },  \
    /*   4 */ {   2,     0x01000400, {.u64=0x04000400},    3,   0x0104,  0  }

/* Note: CN52XX RODT_ENA 0 = disabled, 1 = Weak Read ODT, 2 = Strong Read ODT */
#define OCTEON_XARRAY_CN52XX_DRAM_ODT_2RANK_CONFIGURATION \
    /* DIMMS   RODT_ENA  WODT_CTL0     WODT_CTL1        QS_DIC RODT_CTL DIC */ \
    /* =====   ======== ============ ================== ====== ======== === */ \
    /*   1 */ {   2,     0x00000101, {.u64=0x00000000},    3,   0x0011,  0  },  \
    /*   2 */ {   2,     0x01010404, {.u64=0x00000000},    3,   0x1144,  0  },  \
    /*   3 */ {   2,     0x01010404, {.u64=0x00000404},    3,   0x1144,  0  },  \
    /*   4 */ {   2,     0x01010404, {.u64=0x04040404},    3,   0x1144,  0  }

//##############
//##############
//### XR6000 ###
//##############
//##############

#define OCTEON_XARRAY_CN63XX_DDR_CONFIGURATION                      \
/* Interface 0 */                                                   \
{                                                                   \
    .custom_lmc_config = {                                          \
        .min_rtt_nom_idx    = 2,                                    \
        .max_rtt_nom_idx    = 5,                                    \
        .min_rodt_ctl       = 1,                                    \
        .max_rodt_ctl       = 4,                                    \
        .dqx_ctl            = 4,                                    \
        .ck_ctl             = 4,                                    \
        .cmd_ctl            = 4,                                    \
        .min_cas_latency    = 7,                                    \
        .offset_en          = 0,                                    \
        .offset_udimm       = 2,                                    \
        .offset_rdimm       = 1,                                    \
        .ddr_rtt_nom_auto   = 0,                                    \
        .ddr_rodt_ctl_auto  = 0,                                    \
        .rlevel_compute     = 1,                                    \
        .ddr2t_udimm 	    = 1,                                    \
        .ddr2t_rdimm 	    = 1,                                    \
        .maximum_adjacent_rlevel_delay_increment = 2,               \
        .fprch2		    = 1,                                    \
        .parity		    = 1,                                    \
        .mode32b	    = 0                                     \
    },                                                              \
    .dimm_config_table = {                                          \
        OCTEON_XARRAY_DRAM_SOCKET_CONFIGURATION,                    \
        DIMM_CONFIG_TERMINATOR,                                     \
    },                                                              \
    .unbuffered = {                                                 \
        .ddr_board_delay = 0,                                       \
        .lmc_delay_clk   = 0,                                       \
        .lmc_delay_cmd   = 0,                                       \
        .lmc_delay_dq    = 0,                                       \
    },                                                              \
    .registered = {                                                 \
        .ddr_board_delay = 0,                                       \
        .lmc_delay_clk   = 0,                                       \
        .lmc_delay_cmd   = 0,                                       \
        .lmc_delay_dq    = 0,                                       \
    },                                                              \
    .odt_1rank_config = {                                           \
        OCTEON_XARRAY_CN63XX_DRAM_ODT_1RANK_CONFIGURATION,          \
    },                                                              \
    .odt_2rank_config = {                                           \
        OCTEON_XARRAY_CN63XX_DRAM_ODT_2RANK_CONFIGURATION,          \
    }                                                               \
}

#define OCTEON_XARRAY_CN63XX_DRAM_ODT_1RANK_CONFIGURATION \
    /* DIMMS   reserved  WODT_MASK                LMCX_MODEREG_PARAMS1                   RODT_CTL    RODT_MASK    reserved */ \
    /* =====   ======== ============== ==========================================        ========= ============== ======== */ \
    /*   1 */ {   0,    0x00000001ULL, OCTEON_XARRAY_CN63XX_MODEREG_PARAMS1_1RANK_1SLOT,     3,     0x00000000ULL,  0  },     \
    /*   2 */ {   0,    0x00050005ULL, OCTEON_XARRAY_CN63XX_MODEREG_PARAMS1_1RANK_2SLOT,     3,     0x00010004ULL,  0  }

#define OCTEON_XARRAY_CN63XX_DRAM_ODT_2RANK_CONFIGURATION \
    /* DIMMS   reserved  WODT_MASK                LMCX_MODEREG_PARAMS1                   RODT_CTL    RODT_MASK    reserved */ \
    /* =====   ======== ============== ==========================================        ========= ============== ======== */ \
    /*   1 */ {   0,    0x00000101ULL, OCTEON_XARRAY_CN63XX_MODEREG_PARAMS1_2RANK_1SLOT,     3,     0x00000000ULL,    0  },   \
    /*   2 */ {   0,    0x09090606ULL, OCTEON_XARRAY_CN63XX_MODEREG_PARAMS1_2RANK_2SLOT,     3,     0x01010404ULL,    0  }

/* LMC0_MODEREG_PARAMS1 */
#define OCTEON_XARRAY_CN63XX_MODEREG_PARAMS1_1RANK_1SLOT \
    { .cn63xx = { .pasr_00      = 0,                    \
                  .asr_00       = 0,                    \
                  .srt_00       = 0,                    \
                  .rtt_wr_00    = 0,                    \
                  .dic_00       = 0,                    \
                  .rtt_nom_00   = rttnom_40ohm,         \
                  .pasr_01      = 0,                    \
                  .asr_01       = 0,                    \
                  .srt_01       = 0,                    \
                  .rtt_wr_01    = 0,                    \
                  .dic_01       = 0,                    \
                  .rtt_nom_01   = 0,                    \
                  .pasr_10      = 0,                    \
                  .asr_10       = 0,                    \
                  .srt_10       = 0,                    \
                  .rtt_wr_10    = 0,                    \
                  .dic_10       = 0,                    \
                  .rtt_nom_10   = 0,                    \
                  .pasr_11      = 0,                    \
                  .asr_11       = 0,                    \
                  .srt_11       = 0,                    \
                  .rtt_wr_11    = 0,                    \
                  .dic_11       = 0,                    \
                  .rtt_nom_11   = 0,                    \
        }                                               \
    }

#define OCTEON_XARRAY_CN63XX_MODEREG_PARAMS1_1RANK_2SLOT \
    { .cn63xx = { .pasr_00      = 0,                    \
                  .asr_00       = 0,                    \
                  .srt_00       = 0,                    \
                  .rtt_wr_00    = rttwr_120ohm,         \
                  .dic_00       = 0,                    \
                  .rtt_nom_00   = rttnom_20ohm,         \
                  .pasr_01      = 0,                    \
                  .asr_01       = 0,                    \
                  .srt_01       = 0,                    \
                  .rtt_wr_01    = 0,                    \
                  .dic_01       = 0,                    \
                  .rtt_nom_01   = 0,                    \
                  .pasr_10      = 0,                    \
                  .asr_10       = 0,                    \
                  .srt_10       = 0,                    \
                  .rtt_wr_10    = rttwr_120ohm,         \
                  .dic_10       = 0,                    \
                  .rtt_nom_10   = rttnom_20ohm,         \
                  .pasr_11      = 0,                    \
                  .asr_11       = 0,                    \
                  .srt_11       = 0,                    \
                  .rtt_wr_11    = 0,                    \
                  .dic_11       = 0,                    \
                  .rtt_nom_11   = 0                     \
        }                                               \
    }

#define OCTEON_XARRAY_CN63XX_MODEREG_PARAMS1_2RANK_1SLOT \
    { .cn63xx = { .pasr_00      = 0,                    \
                  .asr_00       = 0,                    \
                  .srt_00       = 0,                    \
                  .rtt_wr_00    = 0,                    \
                  .dic_00       = 0,                    \
                  .rtt_nom_00   = rttnom_40ohm,         \
                  .pasr_01      = 0,                    \
                  .asr_01       = 0,                    \
                  .srt_01       = 0,                    \
                  .rtt_wr_01    = 0,                    \
                  .dic_01       = 0,                    \
                  .rtt_nom_01   = 0,                    \
                  .pasr_10      = 0,                    \
                  .asr_10       = 0,                    \
                  .srt_10       = 0,                    \
                  .rtt_wr_10    = 0,                    \
                  .dic_10       = 0,                    \
                  .rtt_nom_10   = 0,                    \
                  .pasr_11      = 0,                    \
                  .asr_11       = 0,                    \
                  .srt_11       = 0,                    \
                  .rtt_wr_11    = 0,                    \
                  .dic_11       = 0,                    \
                  .rtt_nom_11   = 0,                    \
        }                                               \
    }

#define OCTEON_XARRAY_CN63XX_MODEREG_PARAMS1_2RANK_2SLOT \
    { .cn63xx = { .pasr_00      = 0,                    \
                  .asr_00       = 0,                    \
                  .srt_00       = 0,                    \
                  .rtt_wr_00    = 0,                    \
                  .dic_00       = 0,                    \
                  .rtt_nom_00   = rttnom_30ohm,         \
                  .pasr_01      = 0,                    \
                  .asr_01       = 0,                    \
                  .srt_01       = 0,                    \
                  .rtt_wr_01    = 0,                    \
                  .dic_01       = 0,                    \
                  .rtt_nom_01   = rttnom_120ohm,        \
                  .pasr_10      = 0,                    \
                  .asr_10       = 0,                    \
                  .srt_10       = 0,                    \
                  .rtt_wr_10    = 0,                    \
                  .dic_10       = 0,                    \
                  .rtt_nom_10   = rttnom_30ohm,         \
                  .pasr_11      = 0,                    \
                  .asr_11       = 0,                    \
                  .srt_11       = 0,                    \
                  .rtt_wr_11    = 0,                    \
                  .dic_11       = 0,                    \
                  .rtt_nom_11   = rttnom_120ohm,        \
        }                                               \
    }

#endif   /* __OCTEON_XARRAY_SHARED_H__ */
