#ifndef XIRRUS_OTIS_H
#define XIRRUS_OTIS_H

#define SCD_SERIAL_LEN    16    /* length of serial number string               */
#define SCD_LICENSE_LEN   24    /* length of license key string                 */

/********************************************************************************
 *  System Configuration Device (aka Otis) Data Structure                       *
 ********************************************************************************/
typedef struct {
        u8 cnt;                 /* LED timer count                              */
        u8 msg;                 /* LED error message number (BCD)               */
        u8 on;                  /* LED current on  time     (ms * 10)           */
        u8 off;                 /* LED current off time     (ms * 10)           */
        u8 on_rst;              /* LED on  time post reset  (ms * 10)           */
        u8 off_rst;             /* LED off time post reset  (ms * 10)           */
} scd_led_t;

typedef struct {
        u8 cent;                /* manufacturing date, century                  */
        u8 year;                /* manufacturing date, year                     */
        u8 mon;                 /* manufacturing date, month                    */
        u8 mday;                /* manufacturing date, month day                */
} scd_date_t;

typedef struct {
        u8 hour;                /* manufacturing date, hours                    */
        u8 min;                 /* manufacturing date, minutes                  */
        u8 sec;                 /* manufacturing date, seconds                  */
} scd_time_t;

typedef struct {
        scd_date_t date;        /* manufacturing date                           */
        scd_time_t time;        /* manufacturing time                           */
        u8 location;            /* manufacturing location                       */
} scd_mfg_info_t;

typedef struct {
        u16 type;               /* board type number                            */
        u8 version;             /* board version number                         */
        u8 revision;            /* board revision number                        */
} scd_board_id_t;

typedef struct {
        u16 x_data_min;         /* digital compass x-axis minimum               */
        u16 x_data_max;         /* digital compass x-axis maximum               */
        u16 y_data_min;         /* digital compass y-axis minimum               */
        u16 y_data_max;         /* digital compass y-axis maximum               */
} scd_cmps_cal_t;

typedef struct {
        u8 stat_lo;             /* status register,  low  byte                  */
        u8 stat_hi;             /* status register,  high byte                  */
        u8 ctrl_lo;             /* control register, low  byte                  */
        u8 ctrl_hi;             /* control register, high byte                  */

#ifdef __mips__
        u8 proc_io_speed;       /* processor i/o speed                          */
        u8 fan_setting;         /* fan speed setting (0-100%)                   */
        u8 spare_0;             /* spare                                        */
        u8 spare_1;             /* spare                                        */
#else
        u8 syst_pll;            /* system pll ratio, current value              */
        u8 core_pll;            /* core   pll ratio, current value              */
        u8 syst_fallback;       /* system pll ratio, fallback on watchdog reset */
        u8 core_fallback;       /* core   pll ratio, fallback on watchdog reset */
#endif
        u16 wd_timer_cnt;       /* watchdog counter current value               */
        u16 wd_reset_cnt;       /* watchdog counter reset default value         */

        u8 proc_speed;          /* processor speed (MHz * 10)                   */
        u8 reset_time;          /* processor reset time (ms * 10)               */

        scd_led_t led[2];       /* LED (green & red) information                */

#ifdef __mips__
        u16 fan_speed;          /* current fan speed (rpm)                      */
#else
        u16 thermal_cnt;        /* processor thermal feedback count             */
#endif
        u8 maj_rev;             /* software major revision level                */
        u8 min_rev;             /* software minor revision level                */
        u8 sw_date;             /* software date code, (month << 4)+(year-2000) */

        u8 chksum;              /* register file checksum                       */

        u32 ser_num;            /* serial number                                */
        scd_board_id_t board;   /* hardware information                         */
        scd_mfg_info_t mfg;     /* manufacturing time, date, & location         */

        u8 eth_mac_count;       /* ethernet mac address, count                  */
        u8 eth_mac_base[6];     /* ethernet mac address, base                   */
        u8 country_code_0;      /* country code first byte                      */

        u8 rad_mac_count;       /* radio mac address, count                     */
        u8 rad_mac_base[6];     /* radio mac address, base                      */
        u8 country_code_1;      /* country code second byte                     */

                                /* ------ extended register file -------------- */
        union {
          u8 ext_reg_start;     /* extended register file starting byte         */
          u8 ext_chksum;        /* extended register file checksum              */
        };
        u8 ext_revision;        /* extended register file revision              */
        u8 osc_cal;             /* oscillator calibration byte                  */
        u8 mpc_nostart;         /* times 8540 didn't start                      */

        scd_board_id_t system;  /* system information                           */
        scd_mfg_info_t assy;    /* system mfg time, date, & location            */
        u8 sys_serial [SCD_SERIAL_LEN ]; /* system serial number string         */

        u8 sys_license[SCD_LICENSE_LEN]; /* software license key                */

        scd_cmps_cal_t cmps;    /* compass calibration data                     */
} sys_cfg_dev_t;


/********************************************************************************
 *  System Configuration Device (aka Otis) Data Offsets                         *
 ********************************************************************************/
#define SCD_STAT_LO       0     /* status register,  low  byte                  */
#define SCD_STAT_HI       1     /* status register,  high byte                  */
#define SCD_CTRL_LO       2     /* control register, low  byte                  */
#define SCD_CTRL_HI       3     /* control register, high byte                  */

#ifdef __mips__
#define SCD_PROC_IO_SPEED 4     /* processor i/o speed                          */
#define SCD_FAN_SETTING   5     /* fan speed setting (0-100%)                   */
#define SCD_SPARE_0       6     /* spare                                        */
#define SCD_SPARE_1       7     /* spare                                        */
#else
#define SCD_SYST_PLL      4     /* system pll ratio, current value              */
#define SCD_CORE_PLL      5     /* core   pll ratio, current value              */
#define SCD_SYST_FBK      6     /* system pll ratio, fallback on watchdog reset */
#define SCD_CORE_FBK      7     /* core   pll ratio, fallback on watchdog reset */
#endif

#define SCD_WD_TMR_CNT    8     /* watchdog counter current value               */
#define SCD_WD_TMR_RST   10     /* watchdog counter reset default value         */

#define SCD_PROC_SPEED   12     /* processor speed (MHz * 10)                   */
#define SCD_RESET_TIME   13     /* processor reset time (ms * 10)               */

#define SCD_LED1_CNT     14     /* LED1 (green) timer count                     */
#define SCD_LED1_MSG     15     /* LED1 (green) error message number (BCD)      */
#define SCD_LED1_ON      16     /* LED1 (green) current on  time     (ms * 10)  */
#define SCD_LED1_OFF     17     /* LED1 (green) current off time     (ms * 10)  */
#define SCD_LED1_ON_RST  18     /* LED1 (green) on  time post reset  (ms * 10)  */
#define SCD_LED1_OFF_RST 19     /* LED1 (green) off time post reset  (ms * 10)  */

#define SCD_LED2_CNT     20     /* LED2 (amber) timer count                     */
#define SCD_LED2_MSG     21     /* LED2 (amber) error message number (BCD)      */
#define SCD_LED2_ON      22     /* LED2 (amber) current on  time     (ms * 10)  */
#define SCD_LED2_OFF     23     /* LED2 (amber) current off time     (ms * 10)  */
#define SCD_LED2_ON_RST  24     /* LED2 (amber) on  time post reset  (ms * 10)  */
#define SCD_LED2_OFF_RST 25     /* LED2 (amber) off time post reset  (ms * 10)  */

#define SCD_NON_WP_CNT   26     /* size of writeable register file              */

#ifdef __mips__
#define SCD_FAN_SPEED    26     /* current fan speed (rpm)                      */
#else
#define SCD_THERMAL_CNT  26     /* processor thermal feedback count             */
#endif

#define SCD_MAJ_REV      28     /* software major revision level                */
#define SCD_MIN_REV      29     /* software minor revision level                */
#define SCD_SW_DATE      30     /* software date code, (month << 4)+(year-2000) */

#define SCD_CHKSUM       31     /* register file checksum                       */

#define SCD_SER_NUM      32     /* serial number                                */

#define SCD_BOARD_ID     36     /* hardware revision                            */
#define SCD_BOARD_TYPE   36     /* hardware revision                            */
#define SCD_BOARD_VER    38     /* hardware revision                            */
#define SCD_BOARD_REV    39     /* hardware revision                            */

#define SCD_MAN_DATE     40     /* manufacturing date                           */
#define SCD_MAN_CENT     40     /* manufacturing date, century                  */
#define SCD_MAN_YEAR     41     /* manufacturing date, year                     */
#define SCD_MAN_MON      42     /* manufacturing date, month                    */
#define SCD_MAN_MDAY     43     /* manufacturing date, month day                */

#define SCD_MAN_TIME     44     /* manufacturing time                           */
#define SCD_MAN_HOUR     44     /* manufacturing time, hours                    */
#define SCD_MAN_MIN      45     /* manufacturing time, minutes                  */
#define SCD_MAN_SEC      46     /* manufacturing time, seconds                  */

#define SCD_LOCATION     47     /* manufacturing location                       */

#define SCD_ETH_NUM      48     /* ethernet mac address, count                  */
#define SCD_ETH_MAC      49     /* ethernet mac address, base                   */
#define SCD_CNTRY_0      55     /* country code, first byte                     */

#define SCD_RAD_NUM      56     /* radio mac address, count                     */
#define SCD_RAD_MAC      57     /* radio mac address, base                      */
#define SCD_CNTRY_1      63     /* country code, second byte                    */

                                /* --------- extended register file ----------- */
#define SCD_EXT_REG_SET  64     /* end of register file                         */
#define SCD_EXT_CHKSUM   64     /* extended register file chksum                */
#define SCD_EXT_REV      65     /* extended register file chksum                */
#define SCD_OSC_CAL      66     /* oscillator calibration byte                  */
#define SCD_MPC_NOSTART  67     /* mpc no start count                           */

#define SCD_SYSTEM_ID    68     /* system hardware revision                     */
#define SCD_SYSTEM_TYPE  68     /* system hardware revision                     */
#define SCD_SYSTEM_VER   70     /* system hardware revision                     */
#define SCD_SYSTEM_REV   71     /* system hardware revision                     */

#define SCD_SYS_DATE     72     /* system manufacturing date                    */
#define SCD_SYS_CENT     72     /* system manufacturing date, century           */
#define SCD_SYS_YEAR     73     /* system manufacturing date, year              */
#define SCD_SYS_MON      74     /* system manufacturing date, month             */
#define SCD_SYS_MDAY     75     /* system manufacturing date, month day         */

#define SCD_SYS_TIME     76     /* system manufacturing time                    */
#define SCD_SYS_HOUR     76     /* system manufacturing time, hours             */
#define SCD_SYS_MIN      77     /* system manufacturing time, minutes           */
#define SCD_SYS_SEC      78     /* system manufacturing time, seconds           */
#define SCD_SYS_LOCATION 79     /* system manufacturing location                */

#define SCD_SYS_SERIAL   80     /* system serial number                         */
#define SCD_SYS_LICENSE  96     /* software license key                         */

#define SCD_XMIN        120     /* compass x-axis minimum                       */
#define SCD_XMAX        122     /* compass x-axis maximum                       */
#define SCD_YMIN        124     /* compass y-axis mimimum                       */
#define SCD_YMAX        126     /* compass y-axis maximum                       */

#define SCD_STD_REG_CNT  64     /* end of register file                         */
#define SCD_EXT_REG_CNT  64     /* end of register file                         */
#define SCD_REG_CNT     128     /* end of register file                         */

/********************************************************************************
 *  System Configuration Device (aka Otis) Status & Control Bit Definitions     *
 ********************************************************************************/

/* -------------- Status Register, Low Byte, Bit Definitions ------------------ */
#define SRL_PON_RESET   0x01    /* Board reset                                  */
#define SRL_EXT_RESET   0x02    /* External reset asserted                      */
#define SRL_BRN_RESET   0x04    /* Brown out caused reset                       */
#define SRL_AWD_RESET   0x08    /* Atmel    watchdog timer failed               */
#define SRL_MWD_RESET   0x10    /* Motorola watchdog timer failed               */
#define SRL_EEP_FAIL    0x20    /* EEPROM chksum failed, EEPROM reloaded        */
#define SRL_IRQ_REQ     0x80    /* IRQ output bit                               */

/* -------------- Status Register, High Byte, Bit Definitions ----------------- */
#define SRH_FPGA_INIT   0x01    /* FPGA init bit                                */
#define SRH_FPGA_DONE   0x02    /* FPGA done bit                                */

/* -------------- Control Register, Low Byte, Bit Definitions ----------------- */
#define CRL_RESET       0x01    /* Reset Board                                  */
#define CRL_LED1_OVR    0x02    /* LED1 Override                                */
#define CRL_LED2_OVR    0x04    /* LED2 Override                                */
#define CRL_AWD_DIS     0x08    /* Atmel    watchdog disable (sampled at reset) */
#define CRL_MWD_DIS     0x10    /* Motorola watchdog disable                    */
#define CRL_EEP_WR      0x20    /* EEPROM write (set to write, clear when done) */
#define CRL_AUTO_PLL    0x40    /* auto configure CCB & core pll's              */
#define CRL_IRQ_ENB     0x80    /* IRQ enable                                   */

/* -------------- Control Register, High Byte, Bit Definitions  --------------- */
#define CRH_FPGA_PROG   0x01    /* FPGA program bit                             */
#define CRH_FPGA_RST    0x02    /* FPGA reset bit                               */
#define CRH_FANSPEED_0  0x04    /* Fan speed control [0]                        */
#define CRH_FANSPEED_1  0x08    /* Fan speed control [1]                        */
#define CRH_FLASH_WP    0x10    /* Boot flash write protect     (0 = enable)    */
#define CRH_ENV_WP      0x20    /* Environment write protect    (0 = enable)    */
#define CRH_CF_WP       0x40    /* Compact flash write protect  (0 = enable)    */
#define CRH_RADIO_WP    0x80    /* Radio EEPROM write protect   (0 = enable)    */
#define CRH_DATA_PATH   0x80    /* Data path select (XR arrays) (0 = plx   )    */


/********************************************************************************
 *  System Configuration Device (aka Otis) Misc Definitions                     *
 ********************************************************************************/
#define SCD_WR_TIMEOUT           4000   /* Timeout for writing SCD eeprom       */
#define SCD_PRGM_FLASH           0xec   /* program flash command                */
#define SCD_PRGM_FLASH_DONE      0xad   /* program flash done command           */
#define SCD_GRN                  0      /* green LED index                      */
#define SCD_RED                  1      /* red LED index                        */
#define SCD_MAX_CODE_SIZE        0x2000 /* 8K byte max code space               */
#define SCD_MIN_CODE_SIZE        0x0e00 /* 3.5K byte min code space             */
#define SCD_GEN3_MAX_CODE_SIZE   0x3000 /* 12K byte max code space              */
#define SCD_GEN3_MIN_CODE_SIZE   0x0600 /* 1.5K byte min code space             */
#define SCD_XR1K_MAX_CODE_SIZE   0x1000 /* 4K byte max code space               */
#define SCD_XR1K_MIN_CODE_SIZE   0x0800 /* 2K byte min code space               */

#define SCD_IDENT_STR            "Xirrus 8540 SCD Program"
#define SCD_IDENT_52XX_STR       "Xirrus 52xx SCD Program"
#define SCD_IDENT_63XX_STR       "Xirrus 63xx SCD Program"
#define SCD_IDENT_68XX_STR       "Xirrus 68xx SCD Program"
#define SCD_IDENT_XR1K_STR       "Xirrus XR1K SCD Program"
#define SCD_BOOT_STR             "Xirrus Mega8 Bootloader"

#ifdef  CFG_NON_MIXED_REV_FIELD

#define SCD_BOARD_MAJ_REV(n)    ((((n) & 0xf0) >> 4) + 'A')
#define SCD_BOARD_MIN_REV(n)      ((n) & 0x0f)
#define SCD_BOARD_SET_REV(s)    ((((((s)[0] | 0x20) - 'a') & 0xf) << 4) | \
                                (simple_strtoul((s)+1, NULL, 10) & 0xf))
#else

#define SCD_BOARD_MAJ_REV(n)    ((((n) & 0x70) >> 4) + (((n) & 0x80) ? 'A' : '0'))
#define SCD_BOARD_MIN_REV(n)    (  (n) & 0x0f)

#define SCD_BOARD_SET_REV(s)    ((((((s)[0] > '9') ? ((((s)[0] | 0x20) - 'a') | 0x8) : \
                                    ((s)[0] - '0')) & 0xf) << 4) |                     \
                                    (simple_strtoul((s)+1, NULL, 10) & 0xf))
#endif

#define SCD_HAS_MAJ_REV(n)      (((n) & 0xf0) != 0xf0)
#define SCD_HAS_MIN_REV(n)      (((n) & 0x0f) != 0x0f && ((n) & 0x0f) != 0x00)

#define SCD_MAX_MAJ_REV         5
#define SCD_MAX_SW_YEAR         12

/********************************************************************************
 *  System Configuration Device (aka Otis) Image Lookup Routine                 *
 ********************************************************************************/
#undef CFG_DEBUG_SCD_FIND_IMAGE
#ifdef CFG_DEBUG_SCD_FIND_IMAGE
 #ifdef __KERNEL__
  #define scd_debug(fmt, args...)   printk(KERN_DEBUG "%s(%d):" fmt, __FUNCTION__, __LINE__, ##args)
 #else
  #define scd_debug(fmt, args...)   printf("\n" fmt, ##args)
 #endif
#else
 #define scd_debug(fmt, args...)
#endif

#define scd_word(i)                 (__le16_to_cpu(*(u16 *)(i)))
#define scd_char(i)                               (*(u8  *)(i))
#define scd_find(i, str)                   strncmp((char *)(i), str, strlen(str))

static inline int find_scd_image(char *ident_str, u8 **addr, int *cnt)
{
    u8 *last = *addr + *cnt;
    u8 *start_sig, *end_sig;
    u8 *start    , *end;
    u8 *i;
                                                                            scd_debug("addr  = %p, last = %p, cnt = %d", *addr, last, *cnt);
    for (start = end = *addr; start < last && end < last; start = end) {    scd_debug("start = %p", start);
        // look for starting signature
        for (i = start; i < last && scd_word(i) != 0x1234; i += 2)
            ;
        start_sig = i;                                                      scd_debug("starting signature = %p, value = 0x%hx", start_sig, scd_word(start_sig));

        // bail if we didn't find it
        if (start_sig >= last)
            break;

        // verify ending signature, bail on mismatch
        end_sig = start + 2 * scd_word(start_sig + 2);                      scd_debug("ending signature   = %p, value = 0x%hx", end_sig, scd_word(end_sig));
        if (scd_word(end_sig) != 0x5678)
            break;

        // calculate ending address, include trailing 0xff's
        for (i = end_sig + 2; i < last && scd_char(i) == 0xff; ++i)
            ;
        end = i;                                                            scd_debug("end = %p", end);

        // look for correct signon
        for (i = start; i < end; i++) {
            if (scd_find(i, ident_str) == 0)
                break;
        }
        if (i < end) {                                                      scd_debug("signon match, addr = %p, cnt = %ld", start, end - start);
            *addr = start;
            *cnt  = end - start;
            return 1;
        }                                                                   scd_debug("signon mismatch");
    }                                                                       scd_debug("done");
    return 0;
}
#endif  //XIRRUS_OTIS_H
