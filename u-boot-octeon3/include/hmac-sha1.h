
#ifndef _HMAC_SHA1_H
#define _HMAC_SHA1_H

#define SHA_DIGEST_LENGTH 20

typedef struct
{
    unsigned long total[2];     /*!< number of bytes processed  */
    unsigned long state[5];     /*!< intermediate digest state  */
    unsigned char buffer[64];   /*!< data block being processed */
}
sha1_context;

void sha1_hmac( unsigned const char *key, int keylen,
                unsigned const char *input, int ilen,
                unsigned char output[SHA_DIGEST_LENGTH] );

#endif // _HMAC_SHA1_H
