/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * This file provides a meta-command to ease the task of updating
 * the boot file
 */
#include <asm/gpio.h>
#include <common.h>
#include <command.h>
#include <spartan3.h>
#include <otis.h>
#include <image.h>
#include <rtc.h>
#include <asm/arch/octeon_boot.h>
#include <u-boot/md5.h>

#define CONFIG_MIN_UBOOT_XR4000_TIMESTAMP   2012,  1, 20, 0, 0, 0    // minimum u-boot timestamp for xr4k   (version 6051+)
#define CONFIG_MIN_UBOOT_XR1000_TIMESTAMP   2012,  3, 10, 0, 0, 0    // minimum u-boot timestamp for xr1k   (version 6062+)
#define CONFIG_MIN_UBOOT_XR2000_TIMESTAMP   2012,  3, 27, 0, 0, 0    // minimum u-boot timestamp for xr2k   (version 6070+)
#define CONFIG_MIN_UBOOT_XR6000_TIMESTAMP   2012,  3, 27, 0, 0, 0    // minimum u-boot timestamp for xr6k   (version 6070+)
#define CONFIG_MIN_UBOOT_XR500_TIMESTAMP    2012, 11, 27, 0, 0, 0    // minimum u-boot timestamp for xr500  (version 6132+)
#define CONFIG_MIN_UBOOT_XR600_TIMESTAMP    2013,  9,  6, 0, 0, 0    // minimum u-boot timestamp for xr600  (version 6149+)
#define CONFIG_MIN_UBOOT_XR2100_TIMESTAMP   2013,  5, 28, 0, 0, 0    // minimum u-boot timestamp for xr2kcr (version 6142+)

void bitswap(ulong addr, int size, int length);
int cert_check(uchar *addr);

extern int  flash_copy(ulong addr, ulong dest, int size);
extern int  flash_sect_erase (ulong addr_first, ulong addr_last);
extern int  flash_sect_protect (int p, ulong addr_first, ulong addr_last);
extern int  find_our_scd_image(uchar **addr, int *cnt);
extern int  do_scd_update (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]);
extern int  do_reset(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]);
extern char *env_name_spec;
extern int  avr_present (void);

extern u8 xr1k_flash_workaround;
static int  xr1k_flash_erase(void);
static void xr1k_flash_copy(ulong data_addr);
static void xr1k_flash_write(u16 *src_addr, u16 *dest_addr);

uint bitswap_data(uint data, int bits)
{
	uint swap_data = 0;

	while (bits--) {
		swap_data |= (data & 1);
		if (bits) {
			swap_data <<= 1;
			data      >>= 1;
		}
	}
	return (swap_data);
}

void bitswap(ulong addr, int size, int length)
{
	int i;

	for (i = 0; i < length; i += size, addr += size) {
		switch (size) {
		case 4 : *((uint   *)addr) = (uint)   bitswap_data(*((uint   *)addr), 32);	break;
		case 2 : *((ushort *)addr) = (ushort) bitswap_data(*((ushort *)addr), 16);	break;
		default: *((u_char *)addr) = (u_char) bitswap_data(*((u_char *)addr),  8);	break;
		}
	}
}

static char *base_name (char *name)
{
    register char *s = name;

    if (!name || !*name)
        return ("");

    while (*s++)
            ;

    while (--s >= name) {
        if (*s == '/' || *s == '\\')
            return (++s);
    }
    return(name);
}

int do_update (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        DECLARE_GLOBAL_DATA_PTR;
        char command_str1[100];
        char command_str2[100];
        char command_str3[100];
        char *s, *get_name;
        char *filename;
        ulong filesize;
        ulong load_addr = simple_strtoul (getenv("loadaddr"), NULL, 16);
        ulong data;
        ulong data_addr;
        ulong checksum;
        int   i, match;
        int   use_usb = 0;
        int   use_mem = 0;
        int   scd_update = 0;
        int   len;
        image_header_t header;
        image_header_t *hdr = &header;

        if (!load_addr)
             load_addr = CONFIG_SYS_LOAD_ADDR;

        if (argc <= 1) {
	    return CMD_RET_USAGE;
        }
        else if (strncmp(argv[1], "server", 6) == 0) {
            if (argc < 3) {
                printf("### Error: Server IP address not specified ###\n");
                return 1;
            }
            sprintf(command_str3, "env set serverip %s", argv[2]);
            if (run_command(command_str3, 0) != 0)
                return 1;
            argc -= 2;
            argv += 2;
        }
        else if (strcmp(argv[1], "usb") == 0) {
            use_usb = 1;
            --argc;
            ++argv;
        }
        else if (strcmp(argv[1], "mem") == 0) {
            use_mem = 1;
            --argc;
            ++argv;
        }
        putc('\n');

        for (i = 1; i < argc; i++) {
                match = 0;

                /************************************************************************
                 * Avalon                                                               *
                 ************************************************************************/
                if (strncmp(base_name(argv[i]), "avalon", 6) == 0 || strcmp(argv[i], "all") == 0 || strcmp(argv[i], "fpga") == 0) {
                        ++match;

                        if ((gd->arch.board_desc.rev_major != BD_XR4000) && (gd->arch.board_desc.rev_major != BD_XR6000)) {
                                printf ("FPGA update not supported on this device\n");
                                return 1;
                        }

                        get_name = (strncmp(base_name(argv[i]), "avalon", 6) == 0 && strcmp(argv[i], "avalon") != 0) ?
                                                      argv[i]  : (s = getenv ("image_avalon")) ? s : "avalon.bin";

                        sprintf(command_str1, "%s %x %s", use_usb ? "load" : "tftp", (uint)load_addr+8, get_name);
                        if (!use_mem && run_command(command_str1, 0) != 0)
                                return 1;

                        if (*(ulong *)(load_addr+8+SPARTAN6_SIGNATURE_OFFSET ) != SPARTAN6_SIGNATURE) {
                                printf("### ERROR: Invalid avalon image ###\n");
                                printf("--> use 8-byte offset on load address; e.g., 20000008 instead of 20000000\n\n");
                                return 1;
                        }

                        filename = (s = getenv("filename")) ? s : get_name;
                        filesize = simple_strtoul(getenv("filesize"), NULL, 16);

                        sprintf(command_str2, "env set image_avalon %s", filename);
                        if (run_command(command_str2, 0) != 0)
                                return 1;

                        bitswap(load_addr+8, 1, filesize);
                        *(ulong *)(load_addr) = filesize;
                        *(ulong *)(load_addr+4) = crc32(0, (unsigned char *)load_addr+8, filesize);

                        if (flash_sect_protect(  0, CONFIG_AVALON_IMAGE, CONFIG_AVALON_IMAGE_END) != 0) return 1;
                        if (flash_sect_erase(       CONFIG_AVALON_IMAGE, CONFIG_AVALON_IMAGE_END) != 0) return 1;
                        if (flash_copy(  load_addr, CONFIG_AVALON_IMAGE, filesize + 8        ) != 0) return 1;
                        if (flash_sect_protect(  1, CONFIG_AVALON_IMAGE, CONFIG_AVALON_IMAGE_END) != 0) return 1;
                }
#if 0 //FIX THIS
                /************************************************************************
                 * U-Boot-Test                                                          *
                 ************************************************************************/
                if (strncmp(base_name(argv[i]), "u-boot-test", 11) == 0) {
                        ++match;
                        get_name = (strncmp(base_name(argv[i]), "u-boot-test", 11) == 0 && strcmp(argv[i], "u-boot-test") != 0) ?
                                                      argv[i]  : (s = getenv("image_uboot_test"))  ?  s  : "u-boot-test.bin";

                        sprintf(command_str1, "%s %x %s", use_usb ? "load" : "tftp", load_addr, get_name);

                        if (!use_mem) {
                                memset((char *)load_addr, 0, sizeof(image_header_t) + CONFIG_FLASH_TEST_PROTECT_LEN);
                                if (run_command(command_str1, 0) != 0)
                                        return 1;
                        }
                        filename = (s = getenv("filename")) ? s : get_name;
                        filesize = simple_strtoul(getenv("filesize"), NULL, 16);

                        if (filesize > CONFIG_FLASH_TEST_PROTECT_LEN) {
                                printf("### u-boot-test image size exceeds flash allocation ###\n\n");
                                return 1;
                        }

                        memmove (&header, (char *)load_addr, sizeof(image_header_t));
                        if (ntohl(hdr->ih_magic) != IH_MAGIC && ntohl(hdr->ih_magic) != IH_ALT1_MAGIC) {
                                printf("### Invalid signature ###\n\n");
                                return 1;
                        }

                        data = (ulong)&header;
                        len  = sizeof(image_header_t);
                        checksum = ntohl(hdr->ih_hcrc);
                        hdr->ih_hcrc = 0;
                        if (crc32 (0, (uchar *)data, len) != checksum) {
                                printf("### Bad header checksum ###\n\n");
                                return 1;
                        }

                        data = load_addr + sizeof(image_header_t);
                        len  = ntohl(hdr->ih_size);
                        checksum = ntohl(hdr->ih_dcrc);
                        if (crc32 (0, (uchar *)data, len) != checksum) {
                                printf("### Bad image checksum ###\n\n");
                                return 1;
                        }
                        data_addr = data;

                        sprintf(command_str2, "env set image_uboot_test %s", filename);
                        if (run_command(command_str2, 0) != 0)
                                return 1;

                        if (flash_sect_protect(0, CONFIG_FLASH_TEST, CONFIG_FLASH_TEST+CONFIG_FLASH_TEST_PROTECT_LEN-1) != 0) return 1;
                        if (flash_sect_erase  (   CONFIG_FLASH_TEST, CONFIG_FLASH_TEST+CONFIG_FLASH_TEST_PROTECT_LEN-1) != 0) return 1;
                        if (flash_copy(data_addr, CONFIG_FLASH_TEST,                CONFIG_FLASH_TEST_PROTECT_LEN  ) != 0) return 1;
                        if (flash_sect_protect(1, CONFIG_FLASH_TEST, CONFIG_FLASH_TEST+CONFIG_FLASH_TEST_PROTECT_LEN-1) != 0) return 1;

                        printf("\n");
                        sprintf(command_str3, "go 0x%08llx", 0x1fc00000 + CONFIG_FLASH_TEST - CONFIG_SYS_FLASH_BASE);
                        run_command(command_str3, 0);
                        return (0);
                }
#endif
                /************************************************************************
                 * U-Boot                                                               *
                 ************************************************************************/
                if (strncmp(base_name(argv[i]), "u-boot", 6) == 0 || strncmp(argv[i], "all", 3) == 0) {
                        ++match;
                        get_name = (strncmp(base_name(argv[i]), "u-boot", 6) == 0 && strcmp(argv[i], "u-boot") != 0) ?
                                                      argv[i]  : (s = getenv ("image_uboot")) ?  s : "u-boot.bin";

                        sprintf(command_str1, "%s %x %s", use_usb ? "load" : "tftp", (uint)load_addr, get_name);

                        if (!use_mem) {
                                memset((char *)load_addr, 0, sizeof(image_header_t) + CONFIG_FLASH_PROTECT_LEN);
                                if (run_command(command_str1, 0) != 0)
                                        return 1;
                        }
                        filename = (s = getenv("filename")) ? s : get_name;
                        filesize = simple_strtoul(getenv("filesize"), NULL, 16);

                        memmove (&header, (char *)load_addr, sizeof(image_header_t));
                        if (((gd->arch.board_desc.rev_major == BD_XR700   ||
                              gd->arch.board_desc.rev_major == BD_XD2_230 ||
                              gd->arch.board_desc.rev_major == BD_XD2_240 ||
                              gd->arch.board_desc.rev_major == BD_XD3_230 ||
                              gd->arch.board_desc.rev_major == BD_XD4_130 ||
                              gd->arch.board_desc.rev_major == BD_XD4_240 ||
                              gd->arch.board_desc.rev_major == BD_XA4_240) &&
                             ntohl(hdr->ih_magic) != IH_ALT2_MAGIC) ||
                            ((gd->arch.board_desc.rev_major != BD_XR700   &&
                              gd->arch.board_desc.rev_major != BD_XD2_230 &&
                              gd->arch.board_desc.rev_major != BD_XD2_240 &&
                              gd->arch.board_desc.rev_major != BD_XD3_230 &&
                              gd->arch.board_desc.rev_major != BD_XD4_130 &&
                              gd->arch.board_desc.rev_major != BD_XD4_240 &&
                              gd->arch.board_desc.rev_major != BD_XA4_240) &&
                             ntohl(hdr->ih_magic) != IH_ALT1_MAGIC && ntohl(hdr->ih_magic) != IH_ALT2_MAGIC)) {
                                printf("### Invalid signature ###\n\n");
                                return 1;
                        }

                        data = (ulong)&header;
                        len  = sizeof(image_header_t);
                        checksum = ntohl(hdr->ih_hcrc);
                        hdr->ih_hcrc = 0;
                        if (crc32 (0, (uchar *)data, len) != checksum) {
                                printf ("### Bad header checksum ###\n\n");
                                return 1;
                        }

                        if (strlen((char *)hdr->ih_name) != 15) {
                                printf ("### Invalid u-boot name length in image header ###\n\n");
                                return 1;
                        }

                        // if using u-boot-7xxx, do this version check to make sure we don't brick any systems
                        if (strncmp((char *)hdr->ih_name,"u-boot-7",8) == 0) {
                                if ((gd->arch.board_desc.rev_major == BD_XD2_240 && strncmp((char *)hdr->ih_name, gpio_get_value(19) ?
                                                                                                                 "u-boot-7154"       :     // New XD2-240
                                                                                                                 "u-boot-7047",11) < 0) || // Old XD2-240
                                    (gd->arch.board_desc.rev_major == BD_XD2_230 && strncmp((char *)hdr->ih_name,"u-boot-7146",11) < 0) ||
                                    (gd->arch.board_desc.rev_major == BD_XD3_230 && strncmp((char *)hdr->ih_name,"u-boot-7133",11) < 0) ||
                                    (gd->arch.board_desc.rev_major == BD_XD4_130 && strncmp((char *)hdr->ih_name,"u-boot-7031",11) < 0) ||
                                    (gd->arch.board_desc.rev_major == BD_XD4_240 && strncmp((char *)hdr->ih_name,"u-boot-7054",11) < 0) ||
                                    (gd->arch.board_desc.rev_major == BD_XA4_240 && strncmp((char *)hdr->ih_name,"u-boot-7133",11) < 0) ||
                                    (gd->arch.board_desc.rev_major == BD_XR700   && strncmp((char *)hdr->ih_name,"u-boot-7015",11) < 0)) {
                                   printf ("### Bad image version (requires later version) ###\n\n");
                                   return 1;
                                }
                        }
                        // if using u-boot-6xxx, do this version check to make sure we don't brick any systems
                        else if (strncmp((char *)hdr->ih_name,"u-boot-6",8) == 0) {
                                if ((gd->arch.board_desc.rev_major == BD_XR500  && hdr->ih_time < mktime(CONFIG_MIN_UBOOT_XR500_TIMESTAMP )) ||
                                    (gd->arch.board_desc.rev_major == BD_XR600  && hdr->ih_time < mktime(CONFIG_MIN_UBOOT_XR600_TIMESTAMP )) ||
                                    (gd->arch.board_desc.rev_major == BD_XR1000 && hdr->ih_time < mktime(CONFIG_MIN_UBOOT_XR1000_TIMESTAMP)) ||
                                    (gd->arch.board_desc.rev_major == BD_XR2000 && hdr->ih_time < mktime(CONFIG_MIN_UBOOT_XR2000_TIMESTAMP)) ||
                                    (gd->arch.board_desc.rev_major == BD_XR2100 && hdr->ih_time < mktime(CONFIG_MIN_UBOOT_XR2100_TIMESTAMP)) ||
                                    (gd->arch.board_desc.rev_major == BD_XR4000 && hdr->ih_time < mktime(CONFIG_MIN_UBOOT_XR4000_TIMESTAMP)) ||
                                    (gd->arch.board_desc.rev_major == BD_XR6000 && hdr->ih_time < mktime(CONFIG_MIN_UBOOT_XR6000_TIMESTAMP))) {
                                   printf ("### Bad image version (requires later version) ###\n\n");
                                   return 1;
                                }
                        }
                        else {
                                printf ("### Invalid u-boot name in image header ###\n\n");
                                return 1;
                        }

                        data = load_addr + sizeof(image_header_t);
                        len  = ntohl(hdr->ih_size);
                        checksum = ntohl(hdr->ih_dcrc);
                        if (crc32 (0, (uchar *)data, len) != checksum) {
                                printf ("### Bad image checksum ###\n\n");
                                return 1;
                        }
                        data_addr = data;

                        sprintf(command_str2, "env set image_uboot %s", filename);
                        if (run_command(command_str2, 0) != 0)
                                return 1;

                        put_cmd_label(env_name_spec, "Saving"); puts("Environment ");
                        saveenv();      // save here -- once we flash, some strings will be hosed
                        printf("\n");

                        /* add crc when downgrading from u-boot-7xxx to u-boot-61xx */
                        if (len < CONFIG_FLASH_PROTECT_OLD) *(ulong *)(data_addr+CONFIG_MONITOR_OLD_OFFSET) = crc32(0, (unsigned char *)data_addr, CONFIG_MONITOR_OLD_OFFSET);

			if (xr1k_flash_workaround) {
			        if (xr1k_flash_erase() == 0) {
				        xr1k_flash_copy(data_addr);
			        }
			}
			else {
                                if (flash_sect_protect(0, CONFIG_SYS_FLASH_BASE, CONFIG_SYS_FLASH_BASE+CONFIG_FLASH_PROTECT_LEN-1)  != 0) return 1;
                                if (flash_sect_erase  (   CONFIG_SYS_FLASH_BASE, CONFIG_SYS_FLASH_BASE+CONFIG_FLASH_PROTECT_LEN-1)  != 0) return 1;
                                if (flash_copy(data_addr, CONFIG_SYS_FLASH_BASE, CONFIG_FLASH_PROTECT_LEN)                   != 0) return 1;
                                if (flash_sect_protect(1, CONFIG_SYS_FLASH_BASE, CONFIG_SYS_FLASH_BASE+CONFIG_FLASH_PROTECT_LEN-1)  != 0) return 1;
			}
                        do_reset(NULL,0,0,NULL);    // reset immediately -- all bets are off with the new code flashed
                }

                /************************************************************************
                 * SCD                                                                  *
                 ************************************************************************/
                if (strncmp(base_name(argv[i]), "scd", 3) == 0) {
                        ++match;
                        if (!avr_present()) {
                                printf ("SCD update not supported on this device\n");
                                return 1;
                        }

                        get_name = strncmp(base_name(argv[i]), "scd", 3) == 0 && strcmp(argv[i], "scd") != 0 ?
                                                     argv[i]  : (s = getenv ("image_scd")) ? s : "scd.bin";

                        sprintf(command_str1, "%s %x %s", use_usb ? "load" : "tftp", (uint)load_addr, get_name);
                        if (!use_mem && run_command(command_str1, 0) != 0)
                                return 1;

                        filename = (s = getenv("filename")) ? s : get_name;
                        filesize = simple_strtoul(getenv("filesize"), NULL, 16);

                        if (filesize < SCD_GEN3_MIN_CODE_SIZE || filesize > 2*SCD_GEN3_MAX_CODE_SIZE) {
                                printf("### ERROR: Invalid SCD image size ###\n");
                                return 1;
                        }
                        uchar *addr = (uchar *)load_addr;
                        int    cnt  = filesize;

                        if (!find_our_scd_image(&addr, &cnt)) {
                                printf("### ERROR: Invalid SCD image ###\n");
                                return 1;
                        }
                        sprintf(command_str2, "env set image_scd %s", filename);
                        if (run_command(command_str2, 0) != 0)
                                return 1;

                        scd_update = 1;
                        break;  // bail now, update the environment and then update the scd (will reset the board)
                }

                /************************************************************************
                 * Certificates                                                         *
                 ************************************************************************/
                if (strncmp(base_name(argv[i]), "cert", 4) == 0) {
                        int   retval;
                        uchar *addr;

                        ++match;

                        get_name = (strncmp(argv[i], "cert", 4) == 0 && argv[i+1]) ?  argv[++i] : "certificates.xcb";

                        sprintf(command_str1, "%s %x %s", use_usb ? "load" : "tftp", (uint)load_addr, get_name);
                        if (!use_mem && run_command(command_str1, 0) != 0)
                                return 1;

                        addr = (uchar *)load_addr;
                        retval = cert_check(addr);

                        if (retval == 0) {
                                if (flash_sect_protect(0, CONFIG_CERT_FLASH_ADDR, CONFIG_CERT_FLASH_ADDR+CONFIG_CERT_FLASH_SIZE-1) != 0) return 1;
                                if (flash_sect_erase  (   CONFIG_CERT_FLASH_ADDR, CONFIG_CERT_FLASH_ADDR+CONFIG_CERT_FLASH_SIZE-1) != 0) return 1;
                                if (flash_copy(load_addr, CONFIG_CERT_FLASH_ADDR, CONFIG_CERT_FLASH_SIZE)                          != 0) return 1;
                                if (flash_sect_protect(1, CONFIG_CERT_FLASH_ADDR, CONFIG_CERT_FLASH_ADDR+CONFIG_CERT_FLASH_SIZE-1) != 0) return 1;
                                put_cmdmsg_label("Certs");
                                printf("Loaded Successfully\n");
                        }
                        else {
                                put_cmdmsg_label("Certs");
                                if (retval == -1) {
                                        printf("### ERROR: Could not find size of md5sum protected region ###\n");
                                }
                                else if (retval == -2) {
                                        printf("### ERROR: Could not find expected md5sum value ###\n");
                                }
                                else if (retval == -3) {
                                        printf("### ERROR: md5sum mismatch ###\n");
                                }
                                else {
                                        printf("### ERROR: unexpected error ###\n");
                                }
                                return 1;
                        }

                        addr = (uchar *)load_addr;
                        memset(addr, 0, CONFIG_CERT_FLASH_SIZE);
                        memcpy(addr, (void *)CONFIG_CERT_FLASH_ADDR, CONFIG_CERT_FLASH_SIZE);
                        retval = cert_check(addr);

                        put_cmdmsg_label("Certs");
                        if (retval == 0) {
                                printf("Verified Successfully\n");
                                return 0;
                        }
                        else {
                                if (retval == -1) {
                                        printf("### ERROR: Could not find size of md5sum protected region ###\n");
                                }
                                else if (retval == -2) {
                                        printf("### ERROR: Could not find expected md5sum value ###\n");
                                }
                                else if (retval == -3) {
                                        printf("### ERROR: md5sum mismatch ###\n");
                                }
                                else {
                                        printf("### ERROR: unexpected error ###\n");
                                }
                                return 1;
                        }
                }

                if (!match) {
                        printf("### ERROR: Unrecognized filename '%s' ### \n", argv[i]);
                        return 1;
                }
        }
#ifdef CONFIG_INIT_ENV_AFTER_UPDATE            // warning! -- this fails when flashing a new u-boot beacause some strings aren't relocated from flash
                                            //             and get screwed up when the new version is flashed -- besides, we now do this at boot
        put_cmd_label(env_name_spec, "Init");   puts("Environment\n");
        initialize_environment();
#endif
        put_cmd_label(env_name_spec, "Saving"); puts("Environment ");
        saveenv();
        printf("\n");
        if (scd_update)
            do_scd_update(NULL, 0, 0, NULL);   // will reset the board
        do_reset(NULL, 0, 0, NULL);

        return 0;
}

int do_remove (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        DECLARE_GLOBAL_DATA_PTR;
        char command_str[100];
        int  i, match;

        if (argc == 1 || argc > 5) {
                printf ("Usage:\n%s\n", cmdtp->usage);
                return 1;
        }

        putc('\n');

        if ((gd->arch.board_desc.rev_major != BD_XR4000) && (gd->arch.board_desc.rev_major != BD_XR6000)) {
                printf ("FPGA removal not supported on this device\n");
                return 1;
        }

        for (i = 1; i < argc; i++) {
                match = 0;

                /************************************************************************
                 * Avalon                                                              *
                 ************************************************************************/
                if (strncmp(base_name(argv[i]), "avalon", 7) == 0 || strcmp(argv[i], "all") == 0 || strcmp(argv[i], "fpga") == 0) {
                        ++match;
                        sprintf(command_str, "env clr image_avalon");
                        if (run_command(command_str, 0) != 0)
                                return 1;

                        if (flash_sect_protect(0, CONFIG_AVALON_IMAGE, CONFIG_AVALON_IMAGE_END) != 0) return 1;
                        if (flash_sect_erase  (   CONFIG_AVALON_IMAGE, CONFIG_AVALON_IMAGE_END) != 0) return 1;
                        if (flash_sect_protect(1, CONFIG_AVALON_IMAGE, CONFIG_AVALON_IMAGE_END) != 0) return 1;
                }

                if (!match) {
                        printf("### ERROR: Unrecognized name '%s' ### \n", argv[i]);
                        return 1;
                }
        }
#ifdef CONFIG_INIT_ENV_AFTER_UPDATE            // warning! -- this fails when flashing a new u-boot beacause some strings aren't relocated from flash
                                            //             and get screwed up when the new version is flashed -- besides, we now do this at boot
        put_cmd_label(env_name_spec, "Init");   puts("Environment\n");
        initialize_environment();
#endif
        put_cmd_label(env_name_spec, "Saving"); puts("Environment ");
        saveenv();
        printf("\n");

        return 0;
}

static int xr1k_flash_erase(void)
{
        char spinner[] = "-\\|/-\\|/-.";
        u32  status  = 0;
        int  timeout = 0;

        put_cmd_label("Flash", "Erase");

        *((u16 *)0x1f500054) = 0xaa;  // put in read mode 1 of 3
        udelay(1);
        *((u16 *)0x1f4002aa) = 0x55;  // put in read mode 2 of 3
        udelay(1);
        *((u16 *)0x1f400000) = 0xf0;  // put in read mode 3 of 3
        udelay(1);

        *((u16 *)0x1f500554) = 0xaa;  // chip erase sequence (make sure flash is in read mode before executing this sequence)
        udelay(1);
        *((u16 *)0x1f4002aa) = 0x55;
        udelay(1);
        *((u16 *)0x1f500554) = 0x80;
        udelay(1);
	*((u16 *)0x1f500554) = 0xaa;
        udelay(1);
	*((u16 *)0x1f4002aa) = 0x55;
        udelay(1);
	*((u16 *)0x1f500554) = 0x10;
        udelay(1);

        // 120 seconds is worst case erase time
        for (timeout = 0; timeout < 1250 && status != 0xffffffff; timeout++) {
		udelay (100000);
	        status = *((u32 *)0x1f400000);  // will read all F's when erase completes
                if (timeout % 2 == 0) {
                        int idx = (timeout/2) % 10;
                        if (idx) putc('\b');
                        putc(spinner[idx]);
                }
	}
	if (timeout == 0) {
	        puts(" ### Error: erase timeout ###\n");
		return 1;
        }
	else {
	        puts("\b. done\n");
	        return 0;
        }
}

static void xr1k_flash_copy(ulong data_addr)
{
        char spinner[] = "-\\|/.";
        u16 *src_addr;
        u16 *dst_addr;
        int i, size;

        put_cmd_label("Flash", "Writing");

        src_addr = (u16 *)data_addr;
        dst_addr = (u16 *)CONFIG_SYS_FLASH_BASE;
        size = CONFIG_FLASH_PROTECT_LEN/2;
	for (i = 0; i < size; i++) {
                xr1k_flash_write (src_addr++, dst_addr++);
                if (i % 10000 == 0) {
                        int idx = (i/10000) % 5;
                        if (idx) putc('\b');
                        putc(spinner[idx]);
                }
	}
        puts("\b. done\n");
}

static void xr1k_flash_write(u16 *src_addr, u16 *dst_addr)
{
        *((u16 *)0x1f500554) = 0xaa;  // program sequence
        udelay(1);
        *((u16 *)0x1f4002aa) = 0x55;
        udelay(1);
        *((u16 *)0x1f500554) = 0xa0;
        udelay(1);
	*dst_addr = *src_addr;
        udelay(22);  // typical program time is 13usec
}

int cert_check(uchar *addr)
{
        char  buffer[10];
        char  *s;
        int   i, j, size, loop_max;
        uchar md5sum_calc[16];
        uchar md5sum_expect[16];

        /* find the size of the md5sum protected region */
        s = &buffer[0];
        loop_max = sizeof(buffer) - 1;
        for (i = 0; i < loop_max && *addr != '\n'; i++, *s++ = *addr++);

        if (i == loop_max) {
                return -1;
        }

        addr++; // move past '\n'
        *s = '\0';
        size = simple_strtoul(buffer, NULL, 10);

        /* extracted the expected md5sum */
        for (i = 0; i < 16; i++) {
                for (j = 0, s = &buffer[0]; j < 2; j++, *s++ = *addr++);
                *s = '\0';
                md5sum_expect[i] = (uchar)simple_strtoul(buffer, NULL, 16);
        }

        /* find the start of the md5sum protected region */
        loop_max = 64;
        for (i = 0; i < loop_max && *addr++ != '\n'; i++);

        if (i == loop_max) {
                return -2;
        }

        /* calculate the md5sum of the protected region */
        md5_wd(addr, size, md5sum_calc, CHUNKSZ_MD5);

        /* verify the expected vs. calculated md5sum */
        for (i = 0; i < 16; i++) {
                if (md5sum_expect[i] != md5sum_calc[i]) {
                        return -3;
                }
        }
        return 0;
}

/**************************************************/

U_BOOT_CMD(
        update,    5,    0,    do_update,
        "update the boot FLASH",
        "[server a.b.c.d] [usb] [mem] [ u-boot ] [scd] [ avalon ] [ fpga ] [ all ]\n"
        "    - tftp file(s) to board, or load files from usb/mmc, and copy to boot flash"
);

U_BOOT_CMD(
        remove,    5,    0,    do_remove,
        "remove fpga images from the boot FLASH",
        "[ avalon ] [ fpga ] [ all ]\n"
        "    - remove fpga images(s) from boot flash"
);
