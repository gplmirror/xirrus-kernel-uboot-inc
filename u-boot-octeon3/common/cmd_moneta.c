//==============================================================================
// Moneta: Memory Order-and-Normalcy Evaluation-and-Test Agent
//------------------------------------------------------------------------------
// Author: J. Alan Jones, Xirrus Inc, 12/20/2014
//------------------------------------------------------------------------------
//
// Possible Future Development
//  - INCLUDE AN ACTUAL INITIALIZATION TEST
//  - Test for linear access optimization.
//  - Provide performance analysis.
//  - Compare drives to internal catalog of known parts.
//  - Replace 'rescan' function with a custom device reset.
//  - More detailed error reporting.
//  - Improve duration reporting to provide overall progress and data rate.
//
//==============================================================================
// Required Headers
//==============================================================================
#include <stdint.h>
#include <stdbool.h>

#include <common.h>
#include <command.h>
#include <usb.h>
#include <mmc.h>
#include <part.h>
#include <malloc.h> // This breaks the global data pointer if placed above "common.h"
//==============================================================================
// Required Global Variables
//==============================================================================
// Global U-boot data
DECLARE_GLOBAL_DATA_PTR;         // Defined in global_data.h

// Reserved image space
// While normally set aside for loading the AOS image, this is moneta's primary
// memory space. Generally 160 MiB on XR-520's and 256 MiB on all others.
extern ulong load_reserved_addr; // Defined in board_octeon.c
extern ulong load_reserved_size; // Defined in board_octeon.c
//==============================================================================
// Local Macros
//==============================================================================
// Macros to easily convert raw byte counts to other unit prefixes
#define in_KiB(X) ((uint64_t)(X) / (uint64_t)(1024))
#define in_MiB(X) ((uint64_t)(X) / (uint64_t)(1024 * 1024))
#define in_GiB(X) ((uint64_t)(X) / (uint64_t)(1024 * 1024 * 1024))

#define fault(X) \
    do { if (cfg.try_continue) { print_fault(X); } else { return(X); } } while (0)

#define printv(...) \
    do { if (cfg.be_verbose) printf(__VA_ARGS__); } while (0)

//==============================================================================
// Local Constants, Enumerations, and Definitions
//==============================================================================
// Device indices
// Hard-coded values corresponding to the index of the devices, as defined by
// their device drivers.  Better than trying to scan for the device each time.
static const uint32_t USB_DEV_INDEX = 1;
static const uint32_t MMC_DEV_INDEX = 0;
static const uint32_t BLK_DEV_INDEX = 0;

// Pattern definitions
// The pattern set used in flash memory and ram tests are defined here.
// Pattern set can be expanded or reduced by updating the variables here.
static const uint32_t PATTERN_COUNT = 4;
static const uint32_t PATTERN_BYTES = (4*4);
static const uint32_t PATTERNS[4]   = { 0x00000000, 0xFFFFFFFF,
                                        0x55555555, 0xAAAAAAAA };

// Expected minimum partition sizes, in sectors.
static const uint64_t PART_MIN_SIZE =    0x20000000; // Min 512 MiB expected
static const uint64_t PART_MAX_SIZE = 0x10000000000; // Max 1 TiB, this is a sane limit as of 12/30/14

// General rules of life
static const uint32_t SECTOR_SIZE = 512;
static const uint32_t MBR_SIZE    = 512;
static const uint32_t SB_SIZE     = 2048;

static const uint32_t DEFAULT_FLASH_TEST_SIZE     = 384;
static const uint32_t DEFAULT_FLASH_LOOP_SIZE_USB =   8;
static const uint32_t DEFAULT_FLASH_LOOP_SIZE_MMC =  64;

static const uint32_t MAX_STIM_BLOCKS = 8192;
//static const uint32_t MAX_STIM_BLOCKS = 1;

static uint32_t rng_seed_32;
static uint64_t rng_seed_64;

// Device interface identifiers.
typedef enum {
    DEV_INT_UNKNOWN  = 0x0000,
    DEV_INT_USB      = 0x0010,
    DEV_INT_USB_CVMX = 0x0020,
    DEV_INT_USB_EHCI = 0x0030,
    DEV_INT_EMMC     = 0x0100
} moneta_dev_int;

// Fault identifiers.
typedef enum {

    M_SYSTEM       = 0x00FF,
    M_NO_DEV       = 0x0001,
    M_BAD_USB_DESC = 0x0002,
    M_MMC_INIT     = 0x0003,
    M_NO_BLK_DESC  = 0x0004,

    FT_NONE        = 0x0100, // Flash Test Faults
    FT_SOME        = 0x0101,
    FT_CANCELED    = 0x0102,
    FT_READ_FAIL   = 0x0103,
    FT_READ_DIFF   = 0x0104,
    FT_WRITE_FAIL  = 0x0105,

    RT_NONE        = 0x0200,
    RT_SOME        = 0x0201,
    RT_ZEROING     = 0x0202,
    RT_PATTERN     = 0x0203,
    RT_DIRTY       = 0x0204,

    PT_NONE        = 0x0300,
    PT_SOME        = 0x0301,
    PT_BLOCK_SIZE  = 0x0302,
    PT_READ_FAIL   = 0x0303,
    PT_BAD_MBR     = 0x0304,
    PT_BAD_TYPE    = 0x0305,
    PT_LARGE       = 0x0306,
    PT_SMALL       = 0x0307,
    PT_INVALID     = 0x0308,
    PT_BAD_SIG     = 0x0309,
    PT_CAP_DIFF    = 0x030A,
    PT_EXISTS      = 0x030B,

    IT_NONE        = 0x0400,
    IT_SOME        = 0x0401,
    IT_USB_INIT    = 0x0402,
    IT_MMC_INIT    = 0x0403,
    IT_USB_STOR    = 0x0404,
    IT_USB_NO_DEV  = 0x0405,
    IT_MMC_NO_DEV  = 0x0406,
    IT_BLK_NO_DEV  = 0x0407,

    B_NONE         = 0x0500, // Blast Faults
    B_MEMORY       = 0x0501,
    B_WRITE        = 0x0502,
    B_RESCAN       = 0x0503,
    B_NO_DEV       = 0x0504,
    B_CANCEL       = 0x0505,

    E_NONE         = 0x0600, // EFU Faults
    E_MEMORY       = 0x0601,
    E_WRITE        = 0x0602,
    E_RESCAN       = 0x0603,
    E_NO_DEV       = 0x0604,
    E_CANCEL       = 0x0605,
    E_BLK_SIZE     = 0x0606,

    ST_NONE        = 0x0700,
    ST_SOME        = 0x0701,
    ST_CANCELED    = 0x0702,
    ST_READ_FAIL   = 0x0703

} moneta_fault;

// Partition Information Structure
typedef struct {
    bool     part_valid; // Taken from the back of the MBR
    uint8_t  part_status;
    uint8_t  part_type;

    uint32_t lba_first;

    uint16_t first_cyl;
    uint8_t  first_hd;
    uint8_t  first_sect;

    uint16_t final_cyl;
    uint8_t  final_hd;
    uint8_t  final_sect;

    uint32_t sectors;

    uint32_t unit_size;  // Taken from the Superblock or FAT16 Boot Record
    uint32_t unit_count;
} moneta_part;

// Partition Table Structure
typedef struct {
    bool        mbr_valid;    // Taken from the Master Boot Record
    uint32_t    bs_sum;
    uint32_t    disk_sig;
    moneta_part parts[4];
} moneta_part_table;

// Configuration structure
typedef struct {
    bool try_continue;          // Continue after errors are found.
    bool be_verbose;            // Be chatty
    int  ram_test_runs;         // Enable / disable ram test
    int  init_test_runs;        // Number of times to run initialization test
    int  stim_duration;         // How long to run the stimulation test
    bool part_test;             // Enable / disable the partition test
    bool show_info;             // Enable / disable showing drive information
    bool show_help;             // Enable / disable showing the help message
    int  flash_test_runs;       // Number of times to run the flash test
    int  flash_test_loop_size; // Number of MiB to test per-loop.
    int  flash_test_portion;    // Portion of memory to test in percent
    int  flash_test_size;       // Size of memory to test in MiB
    bool jitter;                // Include jitter reads
    bool read_only;             // Use read-only mode
    bool blast;                 // Enable and disable the engineering tools.
    bool efu;                   // Enable and disable the engineering tools.

    moneta_dev_int     interface; // Interface to use/try for the test
    struct usb_device* usb_dev;   // Potential device pointers
    struct mmc*        mmc_dev;   // Potential device pointers
    block_dev_desc_t*  blk_dev;   // Both USB and MMC should result in a blk_dev
    moneta_part_table  part_tbl;

    moneta_fault last_fault;  // Last fault reported by print_fault function
    int          fault_count; // Number of times print_fault has been called by the current test
} moneta_config;

static moneta_config cfg; // Global configuration

//==============================================================================
//    Function Prototypes
//==============================================================================
// Printing functions
static int  print_help (void);
static void print_cfg  (void);
static void print_usb  (struct usb_device* usb_dev);
static void print_mmc  (struct mmc*        mmc_dev);
static void print_blk  (block_dev_desc_t*  block_dev);
static void print_prt  (moneta_part_table* part);
static void print_fault(moneta_fault       fault);

// Helper functions
static int            build_cfg(int argc, char* const argv[]);
static bool           is_usb(void);
static bool           is_mmc(void);
static int            rescan(void);
static uint32_t       rand32(void);
static moneta_dev_int get_dev_int(void);
static char*          get_dev_int_str(moneta_dev_int interface);

// Test functions
static moneta_fault ram_test(void);
static moneta_fault init_test(void);
static moneta_fault part_test(void);
static moneta_fault flash_test(void);
static moneta_fault stim_test(void);

// Other functions
static moneta_fault blast(void);
static moneta_fault efu(void);

// Primary functions
static int do_moneta(cmd_tbl_t* cmdtp, int flag, int argc, char* const argv[]);
// U_BOOT_CMD()

//==============================================================================
//  Printing Functions
//==============================================================================
// print_help
// -----------------------------------------------------------------------------
// There's no help for you here.
static int print_help() {
    serial_rstmore(0);
    printf("\n");
    printf(" NAME                                                                           \n");
    printf("     MONETA - the Memory Order-and-Normalcy Evaluation-and-Test Agent           \n");
    printf("\n");
    printf(" SYNOPSIS                                                                       \n");
    printf("     moneta [test] [options]                                                    \n");
    printf("     moneta [utility] [options]                                                 \n");
    printf("\n");
    printf(" DESCRIPTION                                                                    \n");
    printf("         MONETA is a set of utilities for the test and evaluation of array's    \n");
    printf("     flash memory devices.                                                      \n");
    printf("         MONETA primarily tests the hardware aspect of the device but can also  \n");
    printf("     check the condition of the partition table to determine if the formatting  \n");
    printf("     is at least superficially correct.                                         \n");
    printf("         MONETA includes a set of three pre-configured tests, with varying      \n");
    printf("     duration and accuracy.  For 99%% of circumstances, 'moneta check' should be\n");
    printf("     sufficient.  However, MONETA also includes access to its core utilities,   \n");
    printf("     providing some ability to define custom tests.                             \n");
    printf("         Unless the read-only flag is set, MONETA performs 'potentially-        \n");
    printf("     'destructive testing.  No damage will occur so long at the program is run  \n"); serial_rstmore(0);
    printf("     to completion, or allowed an orderly shutdown after the 'Ctrl-C' signal is \n");
    printf("     sent.  Uncontrolled shutdown (removing power) may result in data loss, as  \n");
    printf("     MONETA must temporarily store the drive's contents to RAM while write-     \n");
    printf("     testing is underway.                                                       \n");
    printf("         While it cannot distinguish every fault, MONETA will make a best effort\n");
    printf("     to advise on the device's condition.  Often times, the stress of the       \n");
    printf("     testing itself is enough to finish-off failing devices, greatly simplifying\n");
    printf("     diagnosis.                                                                 \n");
    printf("        MONETA also include a pair of engineering tools, blast and efu.  These  \n");
    printf("     tools exist only for development purposes and should be utilized with care.\n");
    printf("     They can be valuable in recovering units that are unable to boot properly, \n");
    printf("     but are capable of retrieving AOS images over the network.                 \n");
    printf("\n");
    printf(" PRE-CONFIGURED TESTS                                                           \n");
    printf("     quick - Fast test of the flash memory device.  Lower chance of detecting   \n");
    printf("             faulty devices, but can provide a quick affirmation of most        \n");
    printf("             failures.                                                          \n");
    printf("\n");
    printf("     check - General test of the flash memory device.  Good balance of speed and\n");
    printf("             detection reliability.                                             \n"); serial_rstmore(0);
    printf("\n");
    printf("     full  - Thorough test of the flash memory device.  Slow, but has an        \n");
    printf("             excellent chance of detecting problem devices.  Also functions as a\n");
    printf("             burn-in test for some drives that need a few cycles before becoming\n");
    printf("             reliable.                                                          \n");
    printf("\n");
    printf(" DISCRETE UTILITIES                                                             \n");
    printf("     flash - Test the flash memory device for faults.                           \n");
    printf("\n");
    printf("     init  - Test the ability to reliably initialize the flash memory device.   \n");
    printf("             (functional, but additional development needed)                    \n");
    printf("\n");
    printf("     part  - Check the condition of the partitions on the flash memory device.  \n");
    printf("\n");
    printf("     ram   - Perform a spot-check of system RAM.                                \n");
    printf("\n");
    printf("     info  - Print information about the device.                                \n");
    printf("\n");
    printf("     help  - Print this information.                                            \n");
    printf("\n");                                                                                 serial_rstmore(0);
    printf("     stim  - Stimulates the drive with rapid, random-accesses.  Quickly triggers\n");
    printf("             otherwise rare bus conditions.  Useful for studying driver and     \n");
    printf("             firmware robustness.                                               \n");
    printf("\n");
    printf("     blast - Erase partition table from flash memory device.                    \n");
    printf("             (development use only)                                             \n");
    printf("\n");
    printf("     efu   - Create an emergency FAT16 partition on the flash memory device.    \n");
    printf("             (development use only)                                             \n");
    printf("\n");
    printf(" OPTIONS                                                                        \n");
    printf("     -v, -verbose                                                               \n");
    printf("          Be verbose about what the application is doing.                       \n");
    printf("          (affects: all)                                                        \n");
    printf("\n");
    printf("     -t, -try_continue                                                          \n");
    printf("          Try to continue testing after the first fault is detected.            \n");
    printf("          (affects: quick, check, full, flash, init, ram)                       \n");
    printf("\n");
    printf("     -c, -count                                                                 \n"); serial_rstmore(0);
    printf("          Set the number of times to run the test.                              \n");
    printf("          (affects: flash, ram, init)                                           \n");
    printf("\n");
    printf("     -b, -block_size                                                            \n");
    printf("          Set the number of MiB to check in each iteration.  Larger values      \n");
    printf("          speed-up testing, but decrease application responsiveness.            \n");
    printf("          (affects: quick, check, full, flash)                                  \n");
    printf("\n");
    printf("     -s, -size                                                                  \n");
    printf("          Size of flash memory to test in MiB.  Cannot be used with '-p' option.\n");
    printf("          (affects: flash)                                                      \n");
    printf("\n");
    printf("     -p, -portion                                                               \n");
    printf("          Percentage of flash memory to test.  Cannot be used with '-s' option. \n");
    printf("          (affects: flash)                                                      \n");
    printf("\n");
    printf("     -r, -read_only                                                             \n");
    printf("          Perform read-only testing on the flash memory device.  Not as         \n");
    printf("          effective as a read-write test, but safe to interrupt.                \n");
    printf("          (affects: quick, check, full, flash)                                  \n"); serial_rstmore(0);
    printf("\n");
    printf("     -j, -jitter                                                                \n");
    printf("          Include additional reads between operations to 'jitter' the device's  \n");
    printf("          open-blocks.  May help reveal additional problem devices.             \n");
    printf("          (affects: quick, check, flash)                                        \n");
    printf("\n");
    printf("     -d, -duration                                                              \n");
    printf("          Causes the stimulation test to run for the specified number of        \n");
    printf("          seconds.  If a negative duration is provided, the test will run       \n");
    printf("          continuously until an error is detected or the user cancels the test  \n");
    printf("          via Ctrl-C.                                                           \n");
    printf("          (affects: stim)                                                       \n");
    printf("\n");
    printf("     -i, -interface                                                             \n");
    printf("          Manually define device interface to test.  Overrides auto-detection.  \n");
    printf("          Accepts the following options: USB, MMC, CVMX, EHCI                   \n");
    printf("          (affects: all)                                                        \n");
    printf("\n");
    printf(" WHY 'MONETA'                                                                   \n");
    printf("     No one was going to remember how to spell 'Mnemosyne'.                     \n"); serial_rstmore(0);
    printf("\n");
    return 0;
}

// print_cfg
// -----------------------------------------------------------------------------
//
static void print_cfg(void) {
    serial_rstmore(0);
    printf("Moneta Configuration\n");
    printf("------------------------------\n");
    printf("Interface:  %s\n",     get_dev_int_str(cfg.interface));
    printf("Continue:   %s\n",    (cfg.try_continue ? "true" : "FALSE"));
    printf("Verbose:    %s\n",    (cfg.be_verbose   ? "true" : "FALSE"));
    printf("Show Info:  %s\n",    (cfg.show_info    ? "true" : "FALSE"));
    printf("Jitter:     %s\n",    (cfg.jitter       ? "true" : "FALSE"));
    printf("Read-only:  %s\n",    (cfg.read_only    ? "true" : "FALSE"));
    printf("Blast:      %s\n",    (cfg.blast        ? "true" : "FALSE"));
    printf("EFU:        %s\n",    (cfg.efu          ? "true" : "FALSE"));
    printf("Part Test:  %s\n",    (cfg.part_test    ? "true" : "FALSE"));
    printf("RAM Test:   %d\n",     cfg.ram_test_runs);
    printf("Init Test:  %d\n",     cfg.init_test_runs);
    printf("Stim Test:  %d\n",     cfg.stim_duration);
    printf("Flash Test: %d\n",     cfg.flash_test_runs);
    printf("Loop Size:  %d MiB\n", cfg.flash_test_loop_size);
    printf("FT Size:    %d MiB\n", cfg.flash_test_size);
    printf("FT Prop:    %d%%\n",   cfg.flash_test_portion);
    printf("\n");
    serial_rstmore(0);
}

// print_usb
// -----------------------------------------------------------------------------
// Print the usb device structure without prompting for a key-press.
static void print_usb(struct usb_device* usb_dev) {
    char description_buffer[512];
    if (usb_dev->config.desc.iConfiguration != 0) {
        usb_string(usb_dev, usb_dev->config.desc.iConfiguration, description_buffer, sizeof(description_buffer));
    }
    else sprintf(description_buffer, "(null)");

    char manufacturer_buffer[512];
    sprintf(manufacturer_buffer, "%s", ((usb_dev->mf[0] == '\0') ? "(null)" : usb_dev->mf));

    serial_rstmore(0);
    printf("    USB Device Descriptor\n");
    printf("----------------------------------------\n");
    printf("Vendor ID:       0x%04X\n", usb_dev->descriptor.idVendor);
    printf("Product ID:      0x%04X\n", usb_dev->descriptor.idProduct);
    printf("Device Rev:      %d.%d\n", (usb_dev->descriptor.bcdDevice>>8) & 0xff, usb_dev->descriptor.bcdDevice & 0xff);
    printf("Manufacturer:    %s\n",     manufacturer_buffer);
    printf("Product:         %s\n",     usb_dev->prod);
    printf("Serial:          %s\n",     usb_dev->serial);

    if (cfg.be_verbose) {
        printf("USB Revision:    %d.%d\n", (usb_dev->descriptor.bcdUSB>>8) & 0xff, usb_dev->descriptor.bcdUSB & 0xff);
        printf("Device Class:    0x%X\n",   usb_dev->descriptor.bDeviceClass);
        printf("Max Packet Size: %d\n",     usb_dev->descriptor.bMaxPacketSize0);
        printf("Configurations:  %d\n",     usb_dev->descriptor.bNumConfigurations);
        printf("Config Number:   %d\n",     usb_dev->configno);
        printf("Interfaces:      %d\n",     usb_dev->config.desc.bNumInterfaces);
        printf("Max Power:       %d mA\n",  usb_dev->config.desc.bMaxPower*2);
        printf("Description:     %s\n",     description_buffer);
    }
    printf("\n");
    serial_rstmore(0);
}

// print_mmc
// -----------------------------------------------------------------------------
// Print the mmc device structure without prompting for a key-press.
static void print_mmc(struct mmc* mmc_dev) {
    uint32_t serial_number = (((mmc_dev->cid[2] >>  8) & 0xff) << 24) |
                             (((mmc_dev->cid[2]      ) & 0xff) << 16) |
                             (((mmc_dev->cid[3] >> 24) & 0xff) <<  8) |
                              ((mmc_dev->cid[3] >> 16) & 0xff);
    serial_rstmore(0);
    printf("    MMC Device Descriptor\n");
    printf("----------------------------------------\n");
    printf("Name:             %c%c%c%c%c%c\n", (mmc_dev->cid[0]      ) & 0xff,
                                               (mmc_dev->cid[1] >> 24) & 0xff,
                                               (mmc_dev->cid[1] >> 16) & 0xff,
                                               (mmc_dev->cid[1] >>  8) & 0xff,
                                               (mmc_dev->cid[1]      ) & 0xff,
                                               (mmc_dev->cid[2] >> 24) & 0xff);
    printf("Revision:         %d.%d\n",        (mmc_dev->cid[2] >> 20) & 0x0f,
                                               (mmc_dev->cid[2] >> 16) & 0x0f);
    printf("Serial:           0x%X\n",         serial_number);
    printf("Manufacturer ID:  0x%X\n",         (mmc_dev->cid[0] >> 24) & 0xff);
    printf("OEM ID:           0x%X\n",         (mmc_dev->cid[0] >>  8) & 0xff);
    printf("Manufacture Date: %d/%d\n",        (mmc_dev->cid[3] >> 12) & 0x0f,
                                               (mmc_dev->cid[3] >>  8) & 0x0f);
    printf("Capacity:         %llu MiB\n",     in_MiB(mmc_dev->capacity));

    if (cfg.be_verbose) {
        printf("Bus Width:        %d-bit\n",       mmc_dev->bus_width);
        printf("Transfer Speed:   %d\n",           mmc_dev->tran_speed);
        printf("Read Block Size:  %d bytes\n",     mmc_dev->read_bl_len);
        printf("Write Block Size: %d bytes\n",     mmc_dev->write_bl_len);
    }
    printf("\n");
    serial_rstmore(0);
}

// print_blk
// -----------------------------------------------------------------------------
// Print the block device descriptor without prompting for a key-press.
static void print_blk(block_dev_desc_t* block_dev) {
    serial_rstmore(0);
    printf("    Block Device Descriptor\n");
    printf("----------------------------------------\n");
    printf("Vendor:     %s\n",        block_dev->vendor);
    printf("Product:    %s\n",        block_dev->product);
    printf("Revision:   %s\n",        block_dev->revision);
    printf("Blocks:     %llu\n",      block_dev->lba);
    printf("Block Size: %lu bytes\n", block_dev->blksz);
    printf("Capacity:   %llu MiB\n", in_MiB(block_dev->lba * block_dev->blksz));
    printf("\n");
    serial_rstmore(0);
}

// print_prt
//------------------------------------------------------------------------------
//
static void print_prt(moneta_part_table* part_tbl) {

    serial_rstmore(0);
    printf("\n");
    if (!part_tbl->mbr_valid) {
        printf("Master Boot Record is invalid.");
        return;
    }
    printf("    Partition Table\n");
    printf("----------------------------------------\n");
    printf("Bootstrap Sum:  0x%X\n", part_tbl->bs_sum);
    printf("Disk Signature: 0x%X\n", part_tbl->disk_sig);

    int i;
    for (i = 0; i < 4; i++) {
        moneta_part* part = &(part_tbl->parts[i]);

        serial_rstmore(0);
        if (part->part_valid) {
            printf("\n");
            printf("--- Partition %d Details ---\n", i+1);
            printf("Status:           0x%X (%s)\n", part->part_status, (part->part_status == 0x80) ? "active" : (part->part_status == 0x00) ? "inactive" : "invalid");
            printf("Type:             0x%X (%s)\n", part->part_type,   (part->part_valid  == 0x06) ? "fat16"  : (part->part_valid  == 0x83) ? "linux"    : "unknown");
            printf("CHS First Sector: %u / %u / %u\n", part->first_cyl, part->first_hd, part->first_sect);
            printf("CHS Final Sector: %u / %u / %u\n", part->final_cyl, part->final_hd, part->final_sect);
            printf("LBA First Sector: %u\n", part->lba_first);
            printf("Total Sectors:    %u (%llu MiB)\n", part->sectors, in_MiB(512 * part->sectors));
            printf("FS Unit Size:     %u\n", part->unit_size);
            printf("FS Unit Count:    %u\n", part->unit_count);
            printf("FS Capacity:      %llu MiB\n", in_MiB(part->unit_count * part->unit_size));
        }
        else {
            printv("\n");
            printv("--- No Entry for Partition %d ---\n", i+1);
        }
    }
    printf("\n");
    serial_rstmore(0);
    return;
}

// print_fault
// -----------------------------------------------------------------------------
// Print the message associated with the fault code without prompting for a
// key-press.
static void print_fault(moneta_fault fault) {
    // Update the global variables
    cfg.fault_count++;
    cfg.last_fault = fault;
    printf("\n");
    serial_rstmore(0);
    switch(fault) {

        case M_SYSTEM:       printf("SYSTEM FAULT DETECTED: Error in executing code.\n");                       break;
        case M_NO_DEV:       printf("DEVICE FAULT DETECTED: Unable to locate device on selected interface.\n"); break;
        case M_BAD_USB_DESC: printf("DEVICE FAULT DETECTED: A bad USB device descriptor was returned.\n");      break;
        case M_MMC_INIT:     printf("DEVICE FAULT DETECTED: eMMC device failed to initialize.\n");               break;
        case M_NO_BLK_DESC:  printf("DEVICE FAULT DETECTED: Unable to retrieve block device descriptor.\n");    break;

        case FT_NONE:       printf("Flash memory test completed successfully.\n");                                 break;
        case FT_SOME:       printf("Flash memory test complete with some faults.\n");                              break;
        case FT_CANCELED:   printf("Flash memory test canceled by user.\n");                                       break;
        case FT_READ_FAIL:  printf("DEVICE FAULT DETECTED: A read failed to get expected data volume.\n");         break;
        case FT_READ_DIFF:  printf("DEVICE FAULT DETECTED: Reads to the same address returned varying values.\n"); break;
        case FT_WRITE_FAIL: printf("DEVIVE FAULT DETECTED: A write failed to set expected data volume.\n");        break;

        case RT_NONE:    printf("RAM spot-check completed successfully.\n");             break;
        case RT_SOME:    printf("RAM spot-check complete with some faults.\n");          break;
        case RT_ZEROING: printf("RAM FAULT DETECTED: Test region not zeroed\n");         break;
        case RT_PATTERN: printf("RAM FAULT DETECTED: Pattern mismatch\n");               break;
        case RT_DIRTY:   printf("RAM FAULT DETECTED: Dirty fault (re-read mismatch)\n"); break;

        case PT_NONE:       printf("Partition test completed successfully.\n");                            break;
        case PT_SOME:       printf("Partition test completed with some faults\n");                         break;
        case PT_BLOCK_SIZE: printf("DEVICE FAULT DETECTED: Returned and invalid block size.\n");           break;
        case PT_READ_FAIL:  printf("DEVICE FAULT DETECTED: A read failed to get expected data volume.\n"); break;
        case PT_BAD_MBR:    printf("FORMAT FAULT DETECTED: Master Boot Record signature is missing.\n");   break;
        case PT_BAD_TYPE:   printf("FORMAT FAULT DETECTED: Partition is of the wrong type.\n");            break;
        case PT_LARGE:      printf("FORMAT FAULT DETECTED: Partition is too large.\n");                    break;
        case PT_SMALL:      printf("FORMAT FAULT DETECTED: Partition is too small.\n");                    break;
        case PT_INVALID:    printf("FORMAT FAULT DETECTED: Partition is missing.\n");                      break;
        case PT_BAD_SIG:    printf("FORMAT FAULT DETECTED: Bad ext4 signature.\n");                        break;
        case PT_CAP_DIFF:   printf("FORMAT FAULT DETECTED: MBR and SB report different capacities.\n");    break;
        case PT_EXISTS:     printf("FORMAT FAULT DETECTED: Extra partition exists.\n");                    break;

        case IT_NONE:       printf("Device initialization test completed successfully.\n");               break;
        case IT_SOME:       printf("Device initialization test compelted with some faults;\n");           break;
        case IT_USB_INIT:   printf("DEVICE FAULT DETECTED: Failed to reinitialize USB device.\n");        break;
        case IT_MMC_INIT:   printf("DEVICE FAULT DETECTED: Failed to reinitialize eMMC device.\n");       break;
        case IT_USB_STOR:   printf("DEVICE FAULT DETECTED: USB device failed to present block device\n"); break;
        case IT_USB_NO_DEV: printf("DEVICE FAULT DETECTED: Unable to retrieve USB device descriptor.");   break;
        case IT_MMC_NO_DEV: printf("DEVICE FAULT DETECTED: Unable to retrieve eMMC device descriptor.");  break;
        case IT_BLK_NO_DEV: printf("DEVICE FAULT DETECTED: Unable to retrieve block device descriptor."); break;

        case B_NONE:   printf("Blast completed successfully.\n");                            break;
        case B_MEMORY: printf("SYSTEM FAULT DETECTED: Unable to allocate system memory.\n"); break;
        case B_WRITE:  printf("DEVICE FAULT DETECTED: Unable to write data to device.\n");   break;
        case B_RESCAN: printf("DEVICE FAULT DETECTED: Error rescanning device.\n");          break;
        case B_NO_DEV: printf("DEVICE FAULT DETECTED: No block device found.\n");            break;
        case B_CANCEL: printf("Partition table erasing canceled.\n");                        break;

        case E_NONE:     printf("EFU completed successfully.\n");                              break;
        case E_MEMORY:   printf("SYSTEM FAULT DETECTED: Unable to allocate system memory.\n"); break;
        case E_WRITE:    printf("DEVICE FAULT DETECTED: Unable to write data to device.\n");   break;
        case E_RESCAN:   printf("DEVICE FAULT DETECTED: Error rescanning device.\n");          break;
        case E_NO_DEV:   printf("DEVICE FAULT DETECTED: No block device found.\n");            break;
        case E_CANCEL:   printf("Emergency reformat canceled.\n");                             break;
        case E_BLK_SIZE: printf("DEVICE FAULT DETECTED: Returned and invalid block size.\n");  break;

        case ST_NONE:      printf("Stimulation test completed successfully.\n");                          break;
        case ST_SOME:      printf("Stimulation test completed with some faults.\n");                      break;
        case ST_CANCELED:  printf("Stimulation test canceled by user.\n");                                break;
        case ST_READ_FAIL: printf("DEVICE FAULT DETECTED: A read failed to get expected data volume.\n"); break;

        default: printf("FAULT DETECTED: Unrecognized fault code.\n"); break;
    }
    serial_rstmore(0);
}

//==============================================================================
//  Helper Functions
//==============================================================================

// is_usb
//------------------------------------------------------------------------------
// short way to sort between the various interfaces
static bool is_usb(void) { return (cfg.interface & 0x00F0); }

// is_mmc
//------------------------------------------------------------------------------
static bool is_mmc(void) { return (cfg.interface & 0x0F00); }
// short way to sort between the various interfaces

// get_dev_int
//------------------------------------------------------------------------------
// Use the Xirrus model number to determine cd which interface we should be
// checking.
// We could determine this in several other ways, but this works fine for now.
static moneta_dev_int get_dev_int(void) {
    switch (gd->arch.board_desc.rev_major) {
        case BD_XD2_230:
        case BD_XD2_240:
        case BD_XD3_230:
        case BD_XD4_130:
        case BD_XD4_240:
        case BD_XA4_240:
        case BD_XR600:
        case BD_XR700:  return DEV_INT_EMMC;

        case BD_XR2100:
        case BD_XR6000: return DEV_INT_USB_EHCI;

        case BD_XR500:
        case BD_XR1000:
        case BD_XR2000:
        case BD_XR4000: return DEV_INT_USB_CVMX;

        default:        return DEV_INT_UNKNOWN;
    }
}

// get_dev_int_str
//------------------------------------------------------------------------------
// Convert interface enumeration to a string.  Could have used macros...
static char* get_dev_int_str(moneta_dev_int interface) {
    static char* unk_str  = "Unknown";
    static char* usb_str  = "USB";
    static char* cvmx_str = "USB via CVMX";
    static char* ehci_str = "USB via EHCI";
    static char* mmc_str  = "eMMC";

    switch (interface) {
        case DEV_INT_USB:      return usb_str;
        case DEV_INT_USB_CVMX: return cvmx_str;
        case DEV_INT_USB_EHCI: return ehci_str;
        case DEV_INT_EMMC:     return mmc_str;
        case DEV_INT_UNKNOWN:
        default:               return unk_str;
    }
}

// build_cfg
//------------------------------------------------------------------------------
// sort out the contents of the command line into a usable configuration
// Returns zero if the configuration was valid and built successfully.
// Returns one if the constructed configuration is invalid.
static int build_cfg(int argc, char* const argv[]) {

    if (argc < 2) return 1;

    // Zeroize the configuration
    memset(&cfg, 0, sizeof(moneta_config));

    // Configure the general defaults
    cfg.interface             = get_dev_int();
    cfg.flash_test_loop_size  = 0;
    cfg.flash_test_size       = DEFAULT_FLASH_TEST_SIZE;

    // Determine which test or utility we're running and set the defaults
    if      (strncmp(argv[1], "quick", 5) == 0) {
        cfg.show_info             = true;
        cfg.init_test_runs        = 5;
        cfg.flash_test_runs       = 1;
    }
    else if (strncmp(argv[1], "check", 5) == 0) {
        cfg.show_info             = true;
        cfg.init_test_runs        = 10;
        cfg.flash_test_runs       = 2;
    }
    else if (strncmp(argv[1], "full" , 4) == 0) {
        cfg.show_info             = true;
        cfg.ram_test_runs         = 1;
        cfg.init_test_runs        = 15;
        cfg.flash_test_runs       = 1;
        cfg.flash_test_portion    = 100;
    }
    else if (strncmp(argv[1], "flash", 5) == 0) { cfg.flash_test_runs = 1;    }
    else if (strncmp(argv[1], "init" , 4) == 0) { cfg.init_test_runs  = 10;   }
    else if (strncmp(argv[1], "part" , 4) == 0) { cfg.part_test       = true; }
    else if (strncmp(argv[1], "ram"  , 3) == 0) { cfg.ram_test_runs   = 1;    }
    else if (strncmp(argv[1], "stim" , 4) == 0) { cfg.stim_duration   = 15;   }
    else if (strncmp(argv[1], "info" , 4) == 0) { cfg.show_info       = true; }
    else if (strncmp(argv[1], "blast", 5) == 0) { cfg.blast           = true; }
    else if (strncmp(argv[1], "efu",   3) == 0) { cfg.efu             = true; }
    else if (strncmp(argv[1], "help" , 4) == 0) { cfg.show_help       = true; }
    else {
        printf("Unrecognized test or utility: %s.\n", argv[1]);
        return 1;
    }

    // Loop through any remaining options

    if (argc > 2) {
        int i;
        for(i = 2; i < argc; i++) {
            if      (strncmp(argv[i], "-v", 2) == 0) { cfg.be_verbose   = true; }
            else if (strncmp(argv[i], "-t", 2) == 0) { cfg.try_continue = true; }
            else if (strncmp(argv[i], "-r", 2) == 0) { cfg.read_only    = true; }
            else if (strncmp(argv[i], "-j", 2) == 0) { cfg.jitter       = true; }
            else if (strncmp(argv[i], "-c", 2) == 0) {
                if (++i >= argc) {
                    printf("Missing count value.\n");
                    return 1;
                }
                else {
                     uint32_t count = simple_strtoul(argv[i], NULL, 10);
                    if (count <= 0) {
                        printf("Invalid count value.\n");
                        return 1;
                    }
                    else { // The tests we're running will have a non-zero value in the run count already.
                        if      (strncmp(argv[1], "flash", 5) == 0) cfg.flash_test_runs = count;
                        else if (strncmp(argv[1], "ram"  , 3) == 0) cfg.ram_test_runs   = count;
                        else if (strncmp(argv[1], "init" , 4) == 0) cfg.init_test_runs  = count;
                        else {
                            printf("Count option not valid with %s.\n", argv[1]);
                            return 1;
                        }
                    }
                }
            } // end -count
            else if (strncmp(argv[i], "-b", 2) == 0) {
                if (++i >= argc) {
                    printf("Missing block-size value.\n");
                    return 1;
                }
                else {
                     uint32_t block_size = simple_strtoul(argv[i], NULL, 10);
                    if (block_size <= 0) {
                        printf("Invalid block-size value.\n");
                        return 1;
                    }
                    else { cfg.flash_test_loop_size = block_size; }
                }
            } // end -block_size
            else if (strncmp(argv[i], "-s", 2) == 0) {
                if (++i >= argc) {
                    printf("Missing size value.\n");
                    return 1;
                }
                else {
                    uint32_t size = simple_strtoul(argv[i], NULL, 10);
                    if (size <= 0) {
                        printf("Invalid size value.\n");
                        return 1;
                    }
                    else { cfg.flash_test_size = size; }
                }
            } // end -size
            else if (strncmp(argv[i], "-p", 2) == 0) {
                if (++i >= argc) {
                    printf("Missing portion value.\n");
                    return 1;
                }
                else {
                    uint32_t portion = simple_strtoul(argv[i], NULL, 10);
                    if ((portion <= 0) || (portion > 100)) {
                        printf("Invalid portion value: %d\n", portion);
                        return 1;
                    }
                    else { cfg.flash_test_portion = portion; }
                }
            } // end -portion
            else if (strncmp(argv[i], "-d", 2) == 0) {
                if (++i >= argc) {
                    printf("Missing duration value.\n");
                    return 1;
                }
                else {
                    int32_t duration = simple_strtol(argv[i], NULL, 10);
                    if (duration == 0) { // Reserved value to disable the test
                        printf("Invalid duration value: %d\n", duration);
                        return 1;
                    }
                    else { cfg.stim_duration = duration; }
                }
            } // end -duration
            else if (strncmp(argv[i], "-i", 2) == 0) {
                if (++i >= argc) {
                    printf("Missing interface string.\n");
                    return 1;
                }
                else {
                  if      (strncmp(argv[i], "usb" , 3) == 0 || strncmp(argv[i], "USB" , 3) == 0) { cfg.interface = DEV_INT_USB;      }
                  else if (strncmp(argv[i], "cvmx", 4) == 0 || strncmp(argv[i], "CVMX", 4) == 0) { cfg.interface = DEV_INT_USB_CVMX; }
                  else if (strncmp(argv[i], "ehci", 4) == 0 || strncmp(argv[i], "EHCI", 4) == 0) { cfg.interface = DEV_INT_USB_EHCI; }
                  else if (strncmp(argv[i], "mmc" , 3) == 0 || strncmp(argv[i], "MMC" , 3) == 0) { cfg.interface = DEV_INT_EMMC;     }
                  else if (strncmp(argv[i], "emmc", 4) == 0 || strncmp(argv[i], "EMMC", 4) == 0) { cfg.interface = DEV_INT_EMMC;     }
                  else {
                      printf("Unrecognized interface string: %s\n", argv[i]);
                      return 1;
                  }
                }
            } // end -interface
            else {
                printf("Unrecognized option: %s.\n", argv[i]);
                return 1;
            } // else no string matches
        }
    } // End if argc > 2

    if (cfg.interface == DEV_INT_UNKNOWN) {
       printf("Unrecognized array model: %d.\n", gd->arch.board_desc.rev_major);
       return 1;
    }

    if (cfg.flash_test_loop_size == 0) {
        if (is_usb()) cfg.flash_test_loop_size = DEFAULT_FLASH_LOOP_SIZE_USB;
        if (is_mmc()) cfg.flash_test_loop_size = DEFAULT_FLASH_LOOP_SIZE_MMC;
    }

    return 0;
}

// rescan
//------------------------------------------------------------------------------
//
static int rescan(void) {
    if (is_usb()) {
        usb_stop();
        if (usb_init() < 0) return 1;
        if (usb_stor_scan(0) < 0) return 1;

        cfg.usb_dev = usb_get_dev_index(USB_DEV_INDEX);
        if (cfg.usb_dev == 0) return 1;

        cfg.blk_dev = usb_stor_get_dev(BLK_DEV_INDEX);
        if (cfg.blk_dev == 0) return 1;

        return 0;
    }
    else if (is_mmc()) {
        cfg.mmc_dev = find_mmc_device(MMC_DEV_INDEX);
        if (cfg.mmc_dev == 0) return 1;

        cfg.mmc_dev->has_init = 0;
        if (mmc_init(cfg.mmc_dev) < 0) return 1;

        cfg.blk_dev = mmc_get_dev(BLK_DEV_INDEX);
        if (cfg.blk_dev == 0) return 1;

        return 0;
    }
    return 0;
}

// rand32())
//------------------------------------------------------------------------------
// Pseudo-random number generation.  Used to generate the random data sizes and
// addresses used by the jitter and stim routines.
// Not really random, but close enough to fool the firmware in most flash
// drives: all their optimizations break down.
// They tend to cry...  And throw bus errors.
unsigned int rand32(void) {
    rng_seed_32 ^= (rng_seed_32 << 13);
    rng_seed_32 ^= (rng_seed_32 >> 17);
    rng_seed_32 ^= (rng_seed_32 << 5);
    return rng_seed_32;
}

unsigned int rand64(void) {
    rng_seed_64 ^= (rng_seed_64 <<  3);
    rng_seed_64 ^= (rng_seed_64 >> 29);
    rng_seed_64 ^= (rng_seed_64 <<  7);
    rng_seed_64 ^= (rng_seed_64 >> 23);
    rng_seed_64 ^= (rng_seed_64 << 13);
    rng_seed_64 ^= (rng_seed_64 >> 17);
    rng_seed_64 ^= (rng_seed_64 <<  5);
    return rng_seed_64;
}

//==============================================================================
// Test Functions
//==============================================================================

// ram_test
//------------------------------------------------------------------------------
// Not a powerful RAM test, but has the ability to check for the worst errors
// in the region of memory reserved for our boot image.  A few XR-520's have
// been seen that refuse to boot from memory, due to corrupted RAM.
// This test *should* flush those units out.
static moneta_fault ram_test(void) {

    uint8_t*  test_region_bytes = (uint8_t*) load_reserved_addr;
    uint32_t* test_region_words = (uint32_t*)load_reserved_addr;
    uint64_t  test_size_bytes   = load_reserved_size;
    uint64_t  test_size_words   = load_reserved_size / 4;
    uint32_t  percent           = test_size_words / 100;
    uint32_t  i                 = 0;
    uint32_t  rc                = 0;

    cfg.fault_count = 0;

    serial_rstmore(0);
    printf("\nPerforming a spot-check of system RAM.\n\n");

    printv("Test Address: 0x%X\n", (uint32_t)test_region_bytes);
    printv("Test Region:  %llu MiB\n", in_MiB(test_size_bytes));
    if((cfg.ram_test_runs > 1) || cfg.be_verbose) {
        printf("Iterations:   %d\n", cfg.ram_test_runs);
    }
    printv("\n");

    for (rc=0; rc < cfg.ram_test_runs; rc++) {
        serial_rstmore(0);
        if((cfg.ram_test_runs > 1) || cfg.be_verbose) {
            printf("Starting iteration: %d of %d\n", rc + 1, cfg.ram_test_runs);
        }
         //---------------------------------------------------------------------
        printv("Zeroing test region...        ");
        memset(test_region_bytes, 0x00, test_size_bytes);
        printv("Complete.\n");
        //----------------------------------------------------------------------
        printf("Verifying region is zeroed... ###%%");
       for (i = 0; i < test_size_words; i++) {
             if (i % percent == 0) printf("\b\b\b\b%3d%%", i / percent);
            if (test_region_words[i] != 0) {
                printf("\n");
                fault(RT_ZEROING);
                printf("\nVerifying region is zeroed... %3d%%", i / percent);
              }
        }
        printf("\b\b\b\bComplete.\n");
        //----------------------------------------------------------------------
        printf("Writing test patterns...      ###%%");
        for (i = 0; i < test_size_words; i++) {
            if (i % percent == 0) printf("\b\b\b\b%3d%%", i / percent);
            test_region_words[i] = PATTERNS[i % PATTERN_COUNT];
        }
        printf("\b\b\b\bComplete.\n");
        //----------------------------------------------------------------------
        printf("Comparing test pattern...     ###%%");
        for (i = 0; i < test_size_words; i++) {
            if (i % percent == 0) printf("\b\b\b\b%3d%%", i / percent);
            if (test_region_words[i] != PATTERNS[i % PATTERN_COUNT]) {
                printf("\n");
                fault(RT_PATTERN);
                printf("\nComparing test pattern...     %3d%%", i / percent);
            }
        }
        printf("\b\b\b\bComplete.\n");
        //----------------------------------------------------------------------
        printf("Checking for dirty-faults...  ###%%");
        for (i = 0; i < test_size_words; i++) {
            if (i % percent == 0) printf("\b\b\b\b%3d%%", i / percent);
            if (test_region_words[i] != PATTERNS[i % PATTERN_COUNT]) {
                printf("\n");
                fault(RT_DIRTY);
                printf("\nChecking for dirty-faults...  %3d%%", i / percent);
            }
        }
        printf("\b\b\b\bComplete.\n");
        //----------------------------------------------------------------------
        printf("\n");
    }

    return (cfg.fault_count == 0) ? RT_NONE : RT_SOME;
}

// part_test
//------------------------------------------------------------------------------
//
static moneta_fault part_test(void) {

    uint32_t i, p;

    uint64_t buffer_size = load_reserved_size;           // Bytes available in reserved memory
    uint8_t* buffer      = (uint8_t*)load_reserved_addr; // Byte access to reserved memory

    cfg.fault_count = 0;

    printf("Checking the MBR and Super Blocks...\n");

    // Quick obvious checks
    if (cfg.blk_dev == 0) return M_SYSTEM;
    if ((cfg.blk_dev->blksz == 0) || (cfg.blk_dev->blksz > buffer_size)) return PT_BLOCK_SIZE;

    // Try to read the MBR, return the failure if we can't
    uint32_t first_block = 0;
    uint32_t block_count = MBR_SIZE / cfg.blk_dev->blksz;
    printv("MBR, read block count %u\n", block_count);
    if (cfg.blk_dev->block_read(BLK_DEV_INDEX, first_block, block_count, (ulong*)buffer) != block_count) return PT_READ_FAIL;

    // Check for the MBR signature
    printv("MBR signature: 0x%X%X\n", buffer[510], buffer[511]);
    if ((buffer[510] != 0x55) || (buffer[511] != 0xAA)) fault(PT_BAD_MBR);

    cfg.part_tbl.mbr_valid = true;
    cfg.part_tbl.disk_sig  = (buffer[440] << 24) | (buffer[441] << 16) | (buffer[442] << 8) | (buffer[443]);
    printv("Disk Signature: %u\n", cfg.part_tbl.disk_sig);

    for(i = 0; i < 440; i++) cfg.part_tbl.bs_sum += buffer[i];
    printv("Bootstrap Sum:  %u\n", cfg.part_tbl.bs_sum);

    // Check the four partition entries in the MBR, ignore any that are zeroed
    for(p = 0; p < 4; p++) {
        uint8_t* partition = &(buffer[446 + (p * 16)]);

        uint32_t byte_total = 0;
        for (i = 0; i < 16; i++) byte_total += partition[i];
        if (byte_total == 0) continue;

        cfg.part_tbl.parts[p].part_valid  = true;
        cfg.part_tbl.parts[p].part_status = partition[0];
        cfg.part_tbl.parts[p].first_hd    = partition[1];
        cfg.part_tbl.parts[p].first_sect  = partition[2] & 0x3F;
        cfg.part_tbl.parts[p].first_cyl   = ((partition[2] & 0xC0) << 2) | partition[3];
        cfg.part_tbl.parts[p].part_type   = partition[4];
        cfg.part_tbl.parts[p].final_hd    = partition[5];
        cfg.part_tbl.parts[p].final_sect  = partition[6] & 0x3F;
        cfg.part_tbl.parts[p].final_cyl   = ((partition[6] & 0xC0) << 2) |  partition[7];
        cfg.part_tbl.parts[p].lba_first   = (partition[11] << 24) | (partition[10] << 16) | (partition[ 9] << 8) | partition[ 8];
        cfg.part_tbl.parts[p].sectors     = (partition[15] << 24) | (partition[14] << 16) | (partition[13] << 8) | partition[12];
    }

    // Now check each partition individually, based on the expected unique characteristics.

    // Partition 1 and 2
    // Chained error messages via else-if prevents unneeded outputs
    for(i = 0; i < 2; i++) {
        moneta_part* part = &(cfg.part_tbl.parts[i]);
        if      (!part->part_valid                            ) fault(PT_INVALID);
        else if ( part->part_type  != 0x83                    ) fault(PT_BAD_TYPE);
        else if ((part->sectors * SECTOR_SIZE) < PART_MIN_SIZE) fault(PT_SMALL);
        else if ((part->sectors * SECTOR_SIZE) > PART_MAX_SIZE) fault(PT_LARGE);
        else {
            // Try to extract the superblock information.
            first_block = (part->lba_first * SECTOR_SIZE) / cfg.blk_dev->blksz;
            block_count = SB_SIZE / cfg.blk_dev->blksz;
            printv("SB, read block count %u\n", block_count);
            if      (cfg.blk_dev->block_read(BLK_DEV_INDEX, first_block, block_count, (ulong*)buffer) != block_count) fault(PT_READ_FAIL);
            else if ((buffer[0x438] != 0x53) || (buffer[0x439] != 0xEF)) fault(PT_BAD_SIG);
            else {
                // Calculate and compare capacities
                part->unit_count =               (buffer[0x407] << 24) | (buffer[0x406] << 16) | (buffer[0x405] << 8) | (buffer[0x404]);
                part->unit_size  = 0x1 << (10 + ((buffer[0x421] << 24) | (buffer[0x420] << 16) | (buffer[0x419] << 8) | (buffer[0x418])));
                uint64_t sb_capacity     = part->unit_count * part->unit_size;
                uint64_t mbr_capacity    = part->sectors * 512;
                printv("MBR Capacity: %llu, SB Capacity: %llu\n", mbr_capacity, sb_capacity);
                if (sb_capacity != mbr_capacity) fault(PT_CAP_DIFF);
            }
        }
    }

    // Partitions 3 and 4: Shouldn't Even Exist
    for(i = 2; i < 4; i++) {
        if (cfg.part_tbl.parts[i].part_valid) fault(PT_EXISTS);
    }

    print_prt(&cfg.part_tbl);

    return (cfg.fault_count == 0) ? PT_NONE : PT_SOME;
}

// init_test
//------------------------------------------------------------------------------
//
static moneta_fault init_test(void) {                                            // TODO TODO TODO

    uint32_t rc;

    cfg.fault_count = 0;
    serial_rstmore(0);

    printf("Performing an initialization test of the flash memory device.\n");
    printf("Total Iterations: %d\n", cfg.init_test_runs);
    printf("\n");

    for (rc=0; rc < cfg.init_test_runs; rc++) {
        serial_rstmore(0);
        printf("Reinitialize: %d of %d\n", rc + 1, cfg.init_test_runs);

        if (is_usb()) {
            printv("Stopping USB controller... \n");
            usb_stop();
            printv("\bDone.\n");

            printv("Reinitializing USB controller... \n");
            if (usb_init() < 0) fault(IT_USB_INIT);
            printv("\bDone.\n");

            printv("Checking for USB device descriptor... \n");
            cfg.usb_dev = usb_get_dev_index(USB_DEV_INDEX);
            if (cfg.usb_dev == 0) fault(IT_USB_NO_DEV);
            printv("\bDone.\n");

            printv("Checking for block device descriptor... \n");
            cfg.blk_dev = usb_stor_get_dev(BLK_DEV_INDEX);
            if (cfg.blk_dev == 0) fault(IT_BLK_NO_DEV);
            printv("\bDone.\n");
        }
        else if (is_mmc()) {
            printv("Rescanning for eMMC device... \n");
            cfg.mmc_dev = find_mmc_device(MMC_DEV_INDEX);
            if (cfg.mmc_dev == 0) fault(IT_MMC_NO_DEV);
            printv("\bDone.\n");

            printv("Reinitializing eMMC device... \n");
            cfg.mmc_dev->has_init = 0;
            if (mmc_init(cfg.mmc_dev) < 0) fault(IT_MMC_INIT);
            printv("\bDone.\n");

            printv("Checking for block device descriptor... \n");
            cfg.blk_dev = mmc_get_dev(BLK_DEV_INDEX);
            if (cfg.blk_dev == 0) fault(IT_BLK_NO_DEV);
            printv("\bDone.\n");
        }
    } // end for each iteration

    return (cfg.fault_count == 0) ? IT_NONE : IT_SOME;
}

// flash_test
//------------------------------------------------------------------------------
//
static moneta_fault flash_test(void) {                                           // TODO TODO TODO

    uint32_t start_time   = get_timer(0); // Timing variables

    uint32_t i, j, rc; // Generic iterators

    uint64_t  ram_bytes = load_reserved_size;           // Bytes available in reserved memory
    uint8_t*  ram       = (uint8_t*)load_reserved_addr; // Byte access to reserved memory

    uint64_t dev_bytes   =  cfg.blk_dev->lba * cfg.blk_dev->blksz;
    //uint64_t dev_words   =  dev_bytes / 4;
    uint64_t dev_blocks  =  cfg.blk_dev->lba;

    uint64_t test_bytes  = (cfg.flash_test_portion == 0) ?
                           (cfg.flash_test_size * 1024 * 1024) :
                          ((cfg.flash_test_portion * dev_bytes) / 100);
    //uint64_t test_words  = test_bytes / 4;
    uint64_t test_blocks = test_bytes / cfg.blk_dev->blksz;

    uint64_t loop_bytes  = cfg.flash_test_loop_size * 1024 * 1024;
    uint64_t loop_words  = loop_bytes / 4;
    uint64_t loop_blocks = loop_bytes / cfg.blk_dev->blksz;

    uint64_t block_index;
    uint64_t blocks_remaining;
    uint64_t blocks_to_request;

    if (test_bytes > dev_bytes) {
        printv("Test size set to %llu MiB due to device capacity.\n", in_MiB(dev_bytes));
        test_bytes = dev_bytes;
    }

    if (loop_bytes > (ram_bytes / 4)) {
        printv("Test block size set to %llu MiB due to RAM limitations.\n", in_MiB(ram_bytes / 4));
        loop_bytes = (ram_bytes / 4);
    }

    uint32_t*  working_bank = (uint32_t*)&(ram[0*loop_bytes]);
    uint32_t*  storage_bank = (uint32_t*)&(ram[1*loop_bytes]);
    uint32_t*  pattern_bank = (uint32_t*)&(ram[2*loop_bytes]);
    uint32_t*  garbage_bank = (uint32_t*)&(ram[3*loop_bytes]);

    serial_rstmore(0);
    printf("\n");
    printf("Preparing to test the flash memory device...\n");
    printv("Ram Address:     %p\n"      , ram);
    printv("Ram Capacity:    %llu MiB\n", in_MiB(ram_bytes));
    printf("Device Capacity: %4llu MiB\n", in_MiB(dev_bytes));
    printf("Test Size:       %4llu MiB\n", in_MiB(test_bytes));
    printf("Test Loop Size:  %4llu MiB\n", in_MiB(loop_bytes));
    printf("Run Count:       %4d\n"      , cfg.flash_test_runs);
    printf("\n");

    cfg.fault_count = 0;

    if (cfg.read_only != true) {
        printv("Generating random pattern...\n");
        for (i = 0; i < loop_words; i++) { pattern_bank[i] = rand32(); }
    }
    else printf("Read-only set, skipping read-write tests.\n");

    if (cfg.jitter == true) printf("Jitter enabled\n");

    printv("Test preparation completed in %lu ms.\n", get_timer(start_time));
    printv("\n");
    //--------------------------------------------------------------------------

    // Read-only testing section
    // This section is separate from the section below in order to chase out a
    // a few drives that would otherwise appear to be fine to the write test,
    // due to bad-blocks being weeded out by the writes.
    printf("Beginning read-read testing of flash memory.\n"
           "Press Ctrl+C to end testing early.\n"
           "\n");

    // Outer iterator for each full run.
    for (rc = 0; rc < cfg.flash_test_runs; rc++) {

        if (ctrlc()) return FT_CANCELED;

        printf("Run: %2d of %d  |  Complete:   0%% /", rc+1, cfg.flash_test_runs);

        // Inner loop walks through all of the data
        block_index = 0;
        blocks_remaining = test_blocks;
        while (blocks_remaining > 0) {

            if (ctrlc()) {
                printf("\b\b  \n");
                return FT_CANCELED;
            }

            blocks_to_request = (loop_blocks < blocks_remaining) ? (loop_blocks) : (blocks_remaining);

            // Initial Read
            if (cfg.blk_dev->block_read(BLK_DEV_INDEX, block_index, blocks_to_request, storage_bank) != blocks_to_request) fault(FT_READ_FAIL);

            // Throw in a few jitter reads to open and close the access blocks
            if (cfg.jitter == true) {
                for(j = 0; j < 8; j++) {
                    if (cfg.blk_dev->block_read(BLK_DEV_INDEX, (rand64() % dev_blocks), 1, garbage_bank) != 1) fault(FT_READ_FAIL);
                }
            }

            printf("\b\b\b\b\b\b%3llu%% \\", (100 * (block_index + (blocks_to_request / 2))) / test_blocks);

            // Second Read
            if (cfg.blk_dev->block_read(BLK_DEV_INDEX, block_index, blocks_to_request, working_bank) != blocks_to_request) fault(FT_READ_FAIL);

            // Compare the Blocks
            if (memcmp(working_bank, storage_bank, blocks_to_request * cfg.blk_dev->blksz) != 0) fault(FT_READ_DIFF);

            printf("\b\b\b\b\b\b%3llu%% /", 100 * (block_index + blocks_to_request) / test_blocks);

            block_index      += blocks_to_request;
            blocks_remaining -= blocks_to_request;

        } // end while there are blocks left
        printf("\b\b  \n");
    } // End for each run

    //--------------------------------------------------------------------------
    if (cfg.read_only == true) return FT_NONE;
    //--------------------------------------------------------------------------

    // Read-write testing section
    printf("\n"
           "Beginning write-read-restore testing of flash memory.\n"
           "DO NOT power-off the unit until the test is complete.\n"
           "Press Ctrl+C to safely end testing early.\n"
           "\n");

    // Outer iterator for each full run.
    for (rc = 0; rc < cfg.flash_test_runs; rc++) {

        if (ctrlc()) return FT_CANCELED;

        printf("Run: %2d of %d  |  Complete:   0%% |", rc+1, cfg.flash_test_runs);
        // Inner loop walks through all of the data
        block_index = 0;
        blocks_remaining = test_blocks;
        while (blocks_remaining > 0) {

            if (ctrlc()) {
                printf("\b\b  \n");
                return FT_CANCELED;
            }

            blocks_to_request = (loop_blocks < blocks_remaining) ? (loop_blocks) : (blocks_remaining);

            // Backup the data to RAM
            if (cfg.blk_dev->block_read(BLK_DEV_INDEX, block_index, blocks_to_request, storage_bank)  != blocks_to_request) fault(FT_READ_FAIL);

            printf("\b\b\b\b\b\b%3llu%% /", (100 * (block_index + (blocks_to_request / 4))) / test_blocks);

            // Write the pattern to the drive
            if (cfg.blk_dev->block_write(BLK_DEV_INDEX, block_index, blocks_to_request, pattern_bank) != blocks_to_request) fault(FT_WRITE_FAIL);

            printf("\b\b\b\b\b\b%3llu%% -", (100 * (block_index + (blocks_to_request / 2))) / test_blocks);

            // Throw in a few jitter reads to open and close the access blocks
            if (cfg.jitter == true) {
                for(j = 0; j < 8; j++) {
                    if (cfg.blk_dev->block_read(BLK_DEV_INDEX, (rand64() % dev_blocks), 1, garbage_bank) != 1) fault(FT_READ_FAIL);
                }
            }

            // Read the pattern
            if (cfg.blk_dev->block_read(BLK_DEV_INDEX, block_index, blocks_to_request, working_bank)  != blocks_to_request) fault(FT_READ_FAIL);

            // Compare
            if (memcmp(pattern_bank, working_bank, blocks_to_request * cfg.blk_dev->blksz) != 0) fault(FT_READ_DIFF);

            printf("\b\b\b\b\b\b%3llu%% \\", (100 * (block_index + (3*blocks_to_request / 4))) / test_blocks);

            // Write the old data back to the drive
            if (cfg.blk_dev->block_write(BLK_DEV_INDEX, block_index, blocks_to_request, storage_bank) != blocks_to_request) fault(FT_WRITE_FAIL);

            // Re-jitter
            if (cfg.jitter == true) {
                for(j = 0; j < 8; j++) {
                    if (cfg.blk_dev->block_read(BLK_DEV_INDEX, (rand64() % dev_blocks), 1, garbage_bank) != 1) fault(FT_READ_FAIL);
                }
            }

            printf("\b\b\b\b\b\b%3llu%% |", 100 * (block_index + blocks_to_request) / test_blocks);

            // Read old data again
            if (cfg.blk_dev->block_read(BLK_DEV_INDEX, block_index, blocks_to_request, working_bank) != blocks_to_request) fault(FT_READ_FAIL);

            // compare
            if (memcmp(storage_bank, working_bank, blocks_to_request * cfg.blk_dev->blksz) != 0) fault(FT_READ_DIFF);

            block_index      += blocks_to_request;
            blocks_remaining -= blocks_to_request;

        } // end while there are blocks left
        printf("\b\b  \n");
    } // End for each run

    return (cfg.fault_count == 0) ? FT_NONE : FT_SOME;
}

// stim_test
//------------------------------------------------------------------------------
//
static moneta_fault stim_test(void) {                                           // TODO TODO TODO

    serial_rstmore(0);
    printf("\n");
    if (cfg.stim_duration < 0) printf("Running continuous stim test... Press Ctrl-C to end.\n");
    else                       printf("Running stim test for %d seconds... Press Ctrl-C to end early.\n", cfg.stim_duration);
    printf("\n");

    cfg.fault_count = 0;

    uint64_t read_count   = 0;
    uint64_t block_count  = 0;
    uint64_t block_index  = 0;
    uint64_t total_blocks = 0;
    uint64_t result       = 0;
    uint64_t start_time   = get_timer(0);

    while ((get_timer(start_time) < (1000 * cfg.stim_duration)) || (cfg.stim_duration == -1)) {

        if (ctrlc()) return ST_CANCELED;

        block_count = (rand64() % MAX_STIM_BLOCKS) + 1;
        block_index =  rand64() %  (cfg.blk_dev->lba - block_count);

        if (((read_count % 25) == 0) && cfg.be_verbose) {
            serial_rstmore(0);
            printf("Read Count: %5llu  |  Data Volume: %5llu MiB\n", read_count, in_MiB(total_blocks * cfg.blk_dev->blksz));
        }

        result = cfg.blk_dev->block_read(BLK_DEV_INDEX, block_index, block_count, (void*)load_reserved_addr);

        if (result != block_count) {
            printv("Error reading %llu blocks from index 0x%llX\n", block_count, block_index);
            fault(ST_READ_FAIL);
        }
        total_blocks += block_count;
        read_count++;
    }

    printv("Completed %llu reads, totaling %llu MiB.", read_count, in_MiB(total_blocks * cfg.blk_dev->blksz));

    return (cfg.fault_count == 0) ? ST_NONE : ST_SOME;
}

//==============================================================================
// Other Functions
//==============================================================================

// blast
//------------------------------------------------------------------------------
// Erase the partition table.  Provides a back-way of recovering post 7.3 units.
// This is because 7.3 should recognize the erased table, and reformat the
// device before booting to the command line.
static moneta_fault blast(void) {

    if (cfg.blk_dev == NULL) return B_NO_DEV;

    printf("\n");
    printf("This will annhilate the partition table of the storage device.\n");
    printf("The disk will need to be reformatted to be usable again.\n");
    printf("All data on the device will be lost.\n");
    printf("There is no undo option.\n");
    printf("Do you wish to continue (y/n)?\n");

    char character = fgetc(stdin);
    if ((character != 'y') && (character != 'Y')) return B_CANCEL;

    printf("\nErasing partition table... ");

    void* partition_table = malloc(MBR_SIZE);
    if (partition_table != NULL) memset(partition_table, 0x00, MBR_SIZE);
    else {
      printf("FAILED\n");
      return B_MEMORY;
    }

    int blocks_written = cfg.blk_dev->block_write(BLK_DEV_INDEX, 0, 1, partition_table);
    free(partition_table);

    if (blocks_written != 1) {
        printf("FAILED\n");
        return B_WRITE;
    }

    printf("Complete.\nRebooting...\n");
    do_reset(NULL, 0, 0, NULL);

    return B_NONE;
}

// efu
//------------------------------------------------------------------------------
//
static moneta_fault efu(void) {

    if (cfg.blk_dev == NULL)     return E_NO_DEV;
    if (cfg.blk_dev->blksz <= 0) return E_BLK_SIZE;

    // These are all the bytes we need to create the partition and file system.
    static const uint8_t partition_bytes[16] = {0x00, 0x00, 0x02, 0x00, 0x06, 0x3D, 0xFE, 0xF3,   0x01, 0x00, 0x00, 0x00, 0xCF, 0x5B, 0x3B, 0x00};
    static const uint8_t boot_signature  [2] = {0x55, 0xAA};
    static const uint8_t fat_boot_record[64] = {0xEB, 0x3C, 0x90, 0x65, 0x66, 0x75, 0x00, 0x00,   0x00, 0x00, 0x00, 0x00, 0x02, 0x40, 0x01, 0x00,
                                                0x01, 0x00, 0x04, 0x00, 0x00, 0xF8, 0x00, 0x01,   0x3E, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x00, 0x00,
                                                0xCF, 0x5B, 0x3B, 0x00, 0x00, 0x00, 0x29, 0x6C,   0x33, 0x33, 0x74, 0x58, 0x69, 0x72, 0x72, 0x75,
                                                0x73, 0x20, 0x20, 0x20, 0x20, 0x20, 0x46, 0x41,   0x54, 0x31, 0x36, 0x20, 0x20, 0x20, 0x00, 0x00};
    static const uint8_t first_clusters  [4] = {0xF8, 0xFF, 0xFF, 0xFF};
    static const uint8_t volume_file    [32] = {0x58, 0x69, 0x72, 0x72, 0x75, 0x73, 0x20, 0x20,   0x20, 0x20, 0x20, 0x08, 0x00, 0x00, 0x6B, 0x53,
                                                0x41, 0x45, 0x41, 0x45, 0x00, 0x00, 0x6B, 0x53,   0x41, 0x45, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    // All offsets are absolute in bytes
    static const int    part_offset =    446; // Location of the first partition description of Master Boot Record
    static const int mbr_sig_offset =    510; // Boot record signature
    static const int     fbr_offset =    512; // FAT Boot Record beginning
    static const int fbr_sig_offset =   1022; // Boot record signature
    static const int cluster_offset =   1024; // Location of first two clusters (need to be marked)
    static const int    file_offset = 132096; // Location of first file (volume file needs to be written)

    // Sector size is assumed to be 512
    //static const int sectors_to_write =    259; // 1 MBR + 1 FAT-BR + 256 FAT + 1 for Volume File
    static const int image_size       = 132608; // 132608 = 512 * sectors_to_write;

    int blocks_to_write = image_size / cfg.blk_dev->blksz;       // Start the value at the number of complete blocks
    if (image_size % cfg.blk_dev->blksz != 0) blocks_to_write++; // Add an extra full block if we have a remainder.

    int memory_size = blocks_to_write * cfg.blk_dev->blksz;

    printf("\n");
    printf("This will reformat the device with an emergency FAT16 file-system.\n");
    printf("All data on the device will be lost.\n");
    printf("There is no undo option.\n");
    printf("Do you wish to continue (y/n)?\n");

    int character = fgetc(stdin);
    if ((character != 'y') && (character != 'Y')) return E_CANCEL;

    printf("\nPerforming emergency format of the device...\n");
    printv("Block size: %lu;  Blocks to write: %d;  Allocating: %lluKiB\n", cfg.blk_dev->blksz, blocks_to_write, in_KiB(memory_size));

    // Allocate the memory, ensure it is zeroed so the FAT is clean
    uint8_t* emergency_image = malloc(memory_size);
    if (emergency_image != NULL) memset(emergency_image, 0, memory_size);
    else return E_MEMORY;

    printv("Building image...\n");
    memcpy((void*)(&(emergency_image[part_offset])),    (void*)partition_bytes, 16 );
    memcpy((void*)(&(emergency_image[mbr_sig_offset])), (void*)boot_signature,   2 );
    memcpy((void*)(&(emergency_image[fbr_offset])),     (void*)fat_boot_record, 64 );
    memcpy((void*)(&(emergency_image[fbr_sig_offset])), (void*)boot_signature,   2 );
    memcpy((void*)(&(emergency_image[cluster_offset])), (void*)first_clusters,   4 );
    memcpy((void*)(&(emergency_image[file_offset])),    (void*)volume_file,     32 );

    printv("Writing image to disk...\n");
    int blocks_written = cfg.blk_dev->block_write(BLK_DEV_INDEX, 0, blocks_to_write, (ulong*)emergency_image);
    free(emergency_image);

    if (blocks_written != blocks_to_write) return E_WRITE;

    printf("Complete.\nRebooting...\n");
    do_reset(NULL, 0, 0, NULL);

    return E_NONE;
}

//==============================================================================
// Primary Functions
//==============================================================================

// do_moneta : primary moneta function and code-path
//------------------------------------------------------------------------------
//
static int do_moneta(cmd_tbl_t* cmdtp, int flag, int argc, char* const argv[]) {

    uint32_t start_time   = get_timer(0); // Timing variables

    if (build_cfg(argc, argv)) return CMD_RET_USAGE;
    if (cfg.show_help)         return print_help();

    serial_rstmore(0);
    printf("\n\nMONETA: Memory Order-and-Normalcy, Evaluation-and-Test Agent\n");

    printv("Detecting device... ");
    if (is_usb()) {
        cfg.usb_dev = usb_get_dev_index(USB_DEV_INDEX);
        if (cfg.usb_dev == 0) {
            printv("\n");
            print_fault(M_NO_DEV);
            return 0; }
        if (cfg.usb_dev->descriptor.bDescriptorType != USB_DT_DEVICE) {
            printv("\n");
            fault(M_BAD_USB_DESC);
        }
    }
    else if (is_mmc()) {
        cfg.mmc_dev = find_mmc_device(MMC_DEV_INDEX);
        if (cfg.mmc_dev == NULL) {
            print_fault(M_NO_DEV);
            return 0;
        }
        if (mmc_init(cfg.mmc_dev) != 0) {
            fault(M_MMC_INIT);
        }
    }
    else {
        print_fault(M_SYSTEM);
        return 0;
    }
    printv("Complete.\n");

    printv("Getting block device information... ");
    if      (is_usb()) cfg.blk_dev = usb_stor_get_dev(BLK_DEV_INDEX);
    else if (is_mmc()) cfg.blk_dev = mmc_get_dev(BLK_DEV_INDEX);
    else {
        print_fault(M_SYSTEM);
        return 0;
    }
    if (cfg.blk_dev == NULL) {
        print_fault(M_NO_BLK_DESC);
        return 0;
    }
    printv("Complete.\n");

    printf("\n");

    rng_seed_32 = get_timer(0);
    rng_seed_64 = get_timer(0);

    //if (cfg.be_verbose) print_cfg();

    if (cfg.efu) {
        print_fault(efu());
        return 0;
    }
    if (cfg.blast) {
        print_fault(blast());
        return 0;
    }
    if (cfg.part_test) {
        print_fault(part_test());
        return 0;
    }

    if (cfg.show_info == true) {
        if (is_usb()) print_usb(cfg.usb_dev);
        if (is_mmc()) print_mmc(cfg.mmc_dev);
        print_blk(cfg.blk_dev);
    }

    if (cfg.ram_test_runs)   print_fault(ram_test());
    if (cfg.init_test_runs)  print_fault(init_test());
    if (cfg.flash_test_runs) print_fault(flash_test());
    if (cfg.stim_duration)   print_fault(stim_test());

    printf("MONETA completed in %u seconds.\n", (uint32_t)(get_timer(start_time) / 1000));

    return 0;
}

// U_BOOT_CMD
//------------------------------------------------------------------------------
// Make moneta accessible to u-boot, provide the help information
U_BOOT_CMD(moneta, 16, 1, do_moneta,
    "memory order-and-normalcy evaluation-and-test agent",
           "help  - print detailed usage information\n"
    "\n"
    "moneta quick - perform a quick check of the flash memory device\n"
    "moneta check - perform a general check of the flash memory device\n"
    "moneta full  - perform a thorough check of the flash memory device\n"
    "\n"
    "moneta flash - check the flash memory device using custom settings\n"
    "moneta info  - print information about the flash memory device\n"
    "moneta init  - perform an initialization test of the flash memory device\n"
    "moneta part  - check the partitions on the flash memory device\n"
    "moneta ram   - perform a spot-check of system RAM\n"
    "\n"
    "moneta blast - erase the partition table of the flash memory device\n"
    "moneta efu   - format the flash memory device with an emergency FAT16 partition\n"
);
//==============================================================================
// End of File
//==============================================================================
