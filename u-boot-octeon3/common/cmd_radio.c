/*
 * Radio Functions
 *
 */

#include <common.h>
#include <command.h>
#include <radio_info.h>
#include <otis.h>
#include <rtc.h>
#include <i2c.h>
#include <linux/ctype.h>
#include <watchdog.h>
#include <asm/arch/cvmx-pcie.h>
#include <malloc.h>

#define EEPROM_SUCCESS  0
#define	endtick(seconds) (get_ticks() + (uint64_t)(seconds) * get_tbclk())

void qca_data_check (int i);
int  qca_data_process(u8 *qca_rd_ptr);
int  radio_eeprom_read (int bd, int addr, char *buf, int len);

extern int scd_write (unsigned long offset, uchar *buf, unsigned long len);
extern uchar scd_reg_read (unsigned long reg);
extern void scd_reg_write (unsigned long reg, uchar val);
extern int radio_get_leds(void);
extern void radio_set_leds(int mask);
extern void radio_spin_leds(int spin_state);
extern int reset_button_active (void);
extern char *mon_strs[16];
extern uint pcie_port_with_radios;
extern uint radio_pcie_bar0_array[20];
extern unsigned short radio_pcie_device_id_array[20];
extern int check_sum(uchar *, int);
extern uint16_t byte_swap16(uint16_t data);
extern uint32_t byte_swap32(uint32_t data);
extern int xir_miiphy_read (unsigned char addr, unsigned char reg, unsigned short * value);
extern int xir_miiphy_write (unsigned char addr, unsigned char reg, unsigned short value);

#define is_qca_radio(x)  ( x == AR9220_DEVICE_ID_DEFAULT || \
                           x == AR9220_DEVICE_ID         || \
                           x == AR9280_DEVICE_ID_DEFAULT || \
                           x == AR9280_DEVICE_ID         || \
                           x == AR9390_DEVICE_ID_DEFAULT || \
                           x == AR9390_DEVICE_ID         || \
                           x == AR9890_DEVICE_ID_DEFAULT )

#define is_bcm_radio(x)  ( x == BCM43525_DEVICE_ID_DEFAULT || \
                           x == BCM43460_DEVICE_ID_DEFAULT || \
                           x == BCM43525_DEVICE_ID         || \
                           x == BCM43460_DEVICE_ID         || \
                           x == BCM43465sb_DEVICE_ID       || \
                           x == BCM43465db_DEVICE_ID       )

int radio_devid_check (int bd, int msg) {

        if (is_qca_radio(radio_pcie_device_id_array[bd]))
                return QCA;
        else if (is_bcm_radio(radio_pcie_device_id_array[bd]))
                return BCM;
        else
                if (msg)
                        printf ("## radio board %d has unrecognized PCI device id of 0x%04x ##\n", bd, radio_pcie_device_id_array[bd]);
                return 0;

}

static int first_eeprom_status_check = 0xffff;  /* done bit does not get set on AR9890 until first i2c access completes */

int radio_eeprom_status (int bd, unsigned short pcie_device_id, uint64_t pcie_addr_base, int *reg_ptr)
{
        int  to = 100;
        uint busy, done;
        uint eeprom_status_addr;
        uint eeprom_status_data_busy;
        uint eeprom_status_data_done;

        if (is_bcm_radio(pcie_device_id)) {
                eeprom_status_addr = BCM43465_EEPROM_CTRL;
                eeprom_status_data_busy = BCM43465_EEPROM_BUSY;

                do {
                        if (!to--) {
                                printf ("## radio eeprom busy bit timeout. ##\n");
                                return (-1);
                        }
                        udelay (100);
                        *reg_ptr = byte_swap32 (cvmx_read64_uint32(pcie_addr_base + eeprom_status_addr));
                        busy = *reg_ptr & eeprom_status_data_busy;
                } while (busy);
        }
        else if (pcie_device_id == AR9890_DEVICE_ID_DEFAULT) {
                eeprom_status_addr = AR9890_SI_CS;
                eeprom_status_data_done = AR9890_SI_CS_DONE_INT;

                do {
                        if (!to--) {
                                printf ("## radio eeprom done bit timeout. ##\n");
                                return (-1);
                        }
                        udelay (100);
                        *reg_ptr = cvmx_read64_uint32(pcie_addr_base + eeprom_status_addr);
                        if (*reg_ptr & AR9890_SI_CS_DONE_ERR) {
                                printf ("## radio eeprom access error. ##\n");
                                return (-1);
                        }
                        done = *reg_ptr & eeprom_status_data_done;
                } while (!done && !(first_eeprom_status_check & (1<<bd)));
                first_eeprom_status_check &= ~(1<<bd);
        }
        else {
                if ((pcie_device_id == AR9390_DEVICE_ID_DEFAULT) || (pcie_device_id == AR9390_DEVICE_ID)) {
                        eeprom_status_addr = AR9390_EEPROM_STATUS_DATA;
                        eeprom_status_data_busy = AR9390_EEPROM_STATUS_DATA_BUSY;
                }
                else {
                        eeprom_status_addr = AR92XX_EEPROM_STATUS_DATA;
                        eeprom_status_data_busy = AR92XX_EEPROM_STATUS_DATA_BUSY;
                }

                do {
                        if (!to--) {
                                printf ("## radio eeprom busy bit timeout. ##\n");
                                return (-1);
                        }
                        udelay (100);
                        *reg_ptr = byte_swap32 (cvmx_read64_uint32(pcie_addr_base + eeprom_status_addr));
                        busy = *reg_ptr & eeprom_status_data_busy;
                } while (busy);
        }

        return (0);
}

int radio_eeprom_write_protect (unsigned short pcie_device_id, uint64_t pcie_addr_base, int mode)
{
        if (is_bcm_radio(pcie_device_id)) {
                if (mode)
                        cvmx_write64_uint32((pcie_addr_base + BCM43465_EEPROM_CTRL), byte_swap32(BCM43465_EEPROM_START | BCM43465_EEPROM_WRDIS));
                else
                        cvmx_write64_uint32((pcie_addr_base + BCM43465_EEPROM_CTRL), byte_swap32(BCM43465_EEPROM_START | BCM43465_EEPROM_WREN));
        }
        if (pcie_device_id == AR9890_DEVICE_ID_DEFAULT) {
                if (mode) {
                        cvmx_write64_uint32((pcie_addr_base + AR9890_WLAN_GPIO_OUT_W1TS), 0x02000000);  /* protect */
                }
                else {
                        cvmx_write64_uint32((pcie_addr_base + AR9890_WLAN_GPIO_OUT_W1TC), 0x02000000);  /* unprotect */
                }
        }
        else if ((pcie_device_id == AR9390_DEVICE_ID_DEFAULT) || (pcie_device_id == AR9390_DEVICE_ID)) {
                if (mode) {
                        cvmx_write64_uint32((pcie_addr_base + AR9390_GPIO_OE_OUT      ), 0x00000000);  /* protect */
                }
                else {
                        cvmx_write64_uint32((pcie_addr_base + AR9390_GPIO_OUTPUT_MUX2 ), 0x1f000000);  /* unprotect */
                        cvmx_write64_uint32((pcie_addr_base + AR9390_GPIO_OUTPUT      ), 0xbfffffff);
                        cvmx_write64_uint32((pcie_addr_base + AR9390_GPIO_OE_OUT      ), 0x00300000);
                }
        }
        else {
                if (mode) {
                        cvmx_write64_uint32((pcie_addr_base + AR92XX_GPIO_OE_OUT      ), 0x00000000);  /* protect */
                }
                else {
                        cvmx_write64_uint32((pcie_addr_base + AR92XX_GPIO_INPUT_EN_VAL), 0x00000200);  /* unprotect */
                        cvmx_write64_uint32((pcie_addr_base + AR92XX_GPIO_OUTPUT_MUX1 ), 0x00000000);
                        cvmx_write64_uint32((pcie_addr_base + AR92XX_GPIO_OUTPUT_MUX2 ), 0x00000000);
                        cvmx_write64_uint32((pcie_addr_base + AR92XX_GPIO_IN_OUT      ), 0x00000000);
                        cvmx_write64_uint32((pcie_addr_base + AR92XX_GPIO_OE_OUT      ), 0xc0000000);
                }
        }
        return (0);
}

#ifdef USE_BCM_EEPROM_STRUCT
static const u8 crc8_table[256] = {
    0x00, 0xF7, 0xB9, 0x4E, 0x25, 0xD2, 0x9C, 0x6B,
    0x4A, 0xBD, 0xF3, 0x04, 0x6F, 0x98, 0xD6, 0x21,
    0x94, 0x63, 0x2D, 0xDA, 0xB1, 0x46, 0x08, 0xFF,
    0xDE, 0x29, 0x67, 0x90, 0xFB, 0x0C, 0x42, 0xB5,
    0x7F, 0x88, 0xC6, 0x31, 0x5A, 0xAD, 0xE3, 0x14,
    0x35, 0xC2, 0x8C, 0x7B, 0x10, 0xE7, 0xA9, 0x5E,
    0xEB, 0x1C, 0x52, 0xA5, 0xCE, 0x39, 0x77, 0x80,
    0xA1, 0x56, 0x18, 0xEF, 0x84, 0x73, 0x3D, 0xCA,
    0xFE, 0x09, 0x47, 0xB0, 0xDB, 0x2C, 0x62, 0x95,
    0xB4, 0x43, 0x0D, 0xFA, 0x91, 0x66, 0x28, 0xDF,
    0x6A, 0x9D, 0xD3, 0x24, 0x4F, 0xB8, 0xF6, 0x01,
    0x20, 0xD7, 0x99, 0x6E, 0x05, 0xF2, 0xBC, 0x4B,
    0x81, 0x76, 0x38, 0xCF, 0xA4, 0x53, 0x1D, 0xEA,
    0xCB, 0x3C, 0x72, 0x85, 0xEE, 0x19, 0x57, 0xA0,
    0x15, 0xE2, 0xAC, 0x5B, 0x30, 0xC7, 0x89, 0x7E,
    0x5F, 0xA8, 0xE6, 0x11, 0x7A, 0x8D, 0xC3, 0x34,
    0xAB, 0x5C, 0x12, 0xE5, 0x8E, 0x79, 0x37, 0xC0,
    0xE1, 0x16, 0x58, 0xAF, 0xC4, 0x33, 0x7D, 0x8A,
    0x3F, 0xC8, 0x86, 0x71, 0x1A, 0xED, 0xA3, 0x54,
    0x75, 0x82, 0xCC, 0x3B, 0x50, 0xA7, 0xE9, 0x1E,
    0xD4, 0x23, 0x6D, 0x9A, 0xF1, 0x06, 0x48, 0xBF,
    0x9E, 0x69, 0x27, 0xD0, 0xBB, 0x4C, 0x02, 0xF5,
    0x40, 0xB7, 0xF9, 0x0E, 0x65, 0x92, 0xDC, 0x2B,
    0x0A, 0xFD, 0xB3, 0x44, 0x2F, 0xD8, 0x96, 0x61,
    0x55, 0xA2, 0xEC, 0x1B, 0x70, 0x87, 0xC9, 0x3E,
    0x1F, 0xE8, 0xA6, 0x51, 0x3A, 0xCD, 0x83, 0x74,
    0xC1, 0x36, 0x78, 0x8F, 0xE4, 0x13, 0x5D, 0xAA,
    0x8B, 0x7C, 0x32, 0xC5, 0xAE, 0x59, 0x17, 0xE0,
    0x2A, 0xDD, 0x93, 0x64, 0x0F, 0xF8, 0xB6, 0x41,
    0x60, 0x97, 0xD9, 0x2E, 0x45, 0xB2, 0xFC, 0x0B,
    0xBE, 0x49, 0x07, 0xF0, 0x9B, 0x6C, 0x22, 0xD5,
    0xF4, 0x03, 0x4D, 0xBA, 0xD1, 0x26, 0x68, 0x9F
};

u8 crc8_calc (u8 *pdata, uint nbytes, u8 crc)
{
        while (nbytes-- > 0)
		crc = crc8_table[(crc ^ *pdata++) & 0xff];
	return crc;
}

#define swap16_buf(buf, len) ({ \
	u16 *_buf = (u16 *)(buf); \
	uint _wds = (len) / 2; \
	while (_wds--) { \
		*_buf = byte_swap16(*_buf); \
		_buf++; \
	} \
})

#define ltoh16_buf(buf, i) swap16_buf((u16 *)(buf), (i))
#define htol16_buf(buf, i) swap16_buf((u16 *)(buf), (i))

#define SROM13_WORDS     590
#define CRC8_INIT_VALUE  0xff
#define CRC8_GOOD_VALUE  0x9f
#endif

int radio_eeprom_write (int bd, int addr, char *buf, int len)
{
        DECLARE_GLOBAL_DATA_PTR;
        uint64_t pcie_addr_base;
        int reg_data, temp_data, retval;
        uint cur_addr32;
        int first_pass = 1;
        unsigned short pcie_device_id;
        int pcie_port;
#ifdef USE_BCM_EEPROM_STRUCT
        u16 *orig_buf;
        u16 *new_buf;
        u8 crc8;
        int nwords;
        int i;
#endif
        pcie_device_id = radio_pcie_device_id_array[bd];
        pcie_port = (gd->arch.board_desc.rev_major == BD_XD2_230 ||
                     gd->arch.board_desc.rev_major == BD_XD2_240 ||
                     gd->arch.board_desc.rev_major == BD_XD3_230 ||
                     gd->arch.board_desc.rev_major == BD_XR600   ||
                     gd->arch.board_desc.rev_major == BD_XR700   ||
                     gd->arch.board_desc.rev_major == BD_XR1000) ? bd : pcie_port_with_radios;
        pcie_addr_base = (cvmx_pcie_get_mem_base_address(pcie_port) | (1ull << 63)) + radio_pcie_bar0_array[bd];

     /* make sure eeprom is not busy when entering routine */
        if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &reg_data) < 0)
                return (-1);

     /* turn off radio eeprom write protection */
        radio_eeprom_write_protect (pcie_device_id, pcie_addr_base, 0);

     /* for BCM43465 */
     /* must read full broadcom structure, then do read-modify write to properly update crc of structure */
        if (is_bcm_radio(pcie_device_id)) {

#ifdef USE_BCM_EEPROM_STRUCT
                nwords = SROM13_WORDS;
	        orig_buf = (u16 *)malloc(nwords * 2);
	        new_buf  = (u16 *)malloc(nwords * 2);

             /* read all data from eeprom */
                radio_eeprom_read (bd, 0, (char *)orig_buf, nwords * 2);
                memcpy(new_buf, orig_buf, nwords * 2);

             /* check that eeprom has already been programmed */
                if (new_buf[0] == 0xffff) {
                        printf ("## eeprom first requires initial programming. ##\n");
                        retval = -1;
                        goto write_protect_eeprom;
                }

             /* check crc prior to changes */
		htol16_buf(new_buf, nwords * 2);
		if (crc8_calc((u8 *)new_buf, (nwords * 2), CRC8_INIT_VALUE) != CRC8_GOOD_VALUE) {
                        printf ("## eeprom crc check failed (1). ##\n");
                        retval = -1;
                        goto write_protect_eeprom;
                }
		ltoh16_buf(new_buf, nwords * 2);

                memcpy(((u8 *)new_buf + addr), buf, len);

             /* calculate new crc */
		htol16_buf(new_buf, nwords * 2);
		crc8 = ~crc8_calc((u8 *)new_buf, (nwords * 2) - 1, CRC8_INIT_VALUE);
                new_buf[nwords - 1] = (new_buf[nwords - 1] & 0xff00) | crc8;
		ltoh16_buf(new_buf, nwords * 2);

             /* check crc after changes to make sure things were done properly */
		htol16_buf(new_buf, nwords * 2);
		if (crc8_calc((u8 *)new_buf, (nwords * 2), CRC8_INIT_VALUE) != CRC8_GOOD_VALUE) {
                        printf ("## eeprom crc check failed (2). ##\n");
                        retval = -1;
                        goto write_protect_eeprom;
                }
		ltoh16_buf(new_buf, nwords * 2);

             /* make sure eeprom is ready */
                if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &reg_data) < 0)
                        return (-1);

             /* commit all changes to the eeprom (including valid crc8) */
		for (i = 0; i < nwords; i++) {
			if (orig_buf[i] != new_buf[i]) {

                                printf("i=%d new_buf=%04x orig_buf=%04x\n", i, new_buf[i], orig_buf[i]); // NEED TO REPLACE WITH TBD DELAY

		     	     /* program address and data */
                        	cvmx_write64_uint32((pcie_addr_base + BCM43465_EEPROM_ADDR), byte_swap32(i<<1));
                        	cvmx_write64_uint32((pcie_addr_base + BCM43465_EEPROM_DATA), byte_swap32(new_buf[i]));

		     	     /* start write access */
                        	cvmx_write64_uint32((pcie_addr_base + BCM43465_EEPROM_CTRL), byte_swap32(BCM43465_EEPROM_START | BCM43465_EEPROM_WRITE));

                        	if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &reg_data) < 0)
                                	return (-1);
                        }
                }

                if (orig_buf)
                        free(orig_buf);

                if (new_buf)
                        free(new_buf);

                mdelay(100); // required for rs command

#else //!USE_BCM_EEPROM_STRUCT

                while (len > 0) {

                        if (addr > ((BCM_EEPROM_MAX_BYTE_OFFSET + 2)>>1)) {
                                printf ("## exceeded radio eeprom maximum offset. ##\n");
                                retval = -1;
                                goto write_protect_eeprom;
                        }

                        if ((first_pass && (addr & 0x1)) || (len == 1)) {  /* first/last 16-bit word has 1 valid byte - do read-modify-write */
                                mdelay(10); /* needed for read to work reliably */
                                radio_eeprom_read (bd, (addr & 0xfffffffe), (char *)&reg_data, 2); //cvmx_read64_uint32(pcie_addr_base + AR_EEPROM_BASE + cur_addr32);
                                //printf("\naddr=0x%08x; reg_data=0x%08x\n", addr & 0xfffffffe, reg_data);
                                reg_data >>= 16; /* shift down 2 bytes */
                                if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &temp_data) < 0) {
                                        retval = -1;
                                        goto write_protect_eeprom;
                                }
                        }

                        if (first_pass && (addr & 0x1)) {  /* first 16-bit word has 1 valid byte - do read-modify-write */
                                temp_data = (*buf & 0x000000ff) | (reg_data & 0x0000ff00);
                                buf++;
                                len--;
                        }
                        else if (len == 1) {               /* last 16-bit word has 1 valid byte - do read-modify-write */
                                temp_data = (*buf<<8 & 0x0000ff00) | (reg_data & 0x000000ff);
                                buf++;
                                len--;
                        }
                        else {                             /* middle 16-bit words have 2 valid bytes */
                                temp_data = *(uint16_t *)buf;
                                buf += 2;
                                len -= 2;
                        }
                        //printf("\naddr=0x%08x; temp_data=0x%08x\n", addr & 0xfffffffe, temp_data);
                        mdelay(5);

		     /* program address and data */
                        cvmx_write64_uint32((pcie_addr_base + BCM43465_EEPROM_ADDR), byte_swap32(addr & 0xfffffffe));
                        cvmx_write64_uint32((pcie_addr_base + BCM43465_EEPROM_DATA), byte_swap32(temp_data));

		     /* start write access */
                       	cvmx_write64_uint32((pcie_addr_base + BCM43465_EEPROM_CTRL), byte_swap32(BCM43465_EEPROM_START | BCM43465_EEPROM_WRITE));

                     /* when busy signal is deasserted, the write has completed */
                        if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &temp_data) < 0) {
                                retval = -1;
                                goto write_protect_eeprom;
                        }
                        addr += 2;
                        first_pass = 0;
                }
                mdelay(100); // required for rs command
#endif
	}
     /* for AR98XX */
        else if (pcie_device_id == AR9890_DEVICE_ID_DEFAULT) {
                while (len > 0) {

                        if (addr > ((AR_EEPROM_MAX_BYTE_OFFSET + 2)>>1)) {
                                printf ("## exceeded radio eeprom maximum offset. ##\n");
                                retval = -1;
                                goto write_protect_eeprom;
                        }

		     /* send 4 bytes of i2c data --> "i2c_addr + write_bit", eeprom_addr[15:8], eeprom_addr[7:0], data_byte */
                        cvmx_write64_uint32((pcie_addr_base + AR9890_TX_DATA0), (0xa0<<24) | ((addr & 0xffff)<<8) | (*buf & 0xff));
                        cvmx_write64_uint32((pcie_addr_base + AR9890_SI_CS), 0x04010000);

                        if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &reg_data) < 0) {
                                retval = -1;
                                goto write_protect_eeprom;
                        }

                udelay (4000);  /* need delay here to avoid i2c error */

                buf++;
                addr++;
                len--;
                }
        }
     /* for AR92XX and AR93XX */
        else {
                cur_addr32 = 2*addr & 0xfffffffc;

                while (len > 0) {

                        if ((cur_addr32) > AR_EEPROM_MAX_BYTE_OFFSET) {
                                printf ("## exceeded radio eeprom maximum offset. ##\n");
                                retval = -1;
                                goto write_protect_eeprom;
                        }

                        if ((first_pass && (addr & 0x1)) || (len == 1)) {  /* first/last 16-bit word has 1 valid byte - do read-modify-write */
                                cvmx_read64_uint32(pcie_addr_base + AR_EEPROM_BASE + cur_addr32);
                                if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &reg_data) < 0) {
                                        retval = -1;
                                        goto write_protect_eeprom;
                                }
                        }
                        if (first_pass && (addr & 0x1)) {  /* first 16-bit word has 1 valid byte - do read-modify-write */
                                temp_data = (*buf & 0x000000ff) | (reg_data & 0x0000ff00);
                                buf++;
                                len--;
                        }
                        else if (len == 1) {               /* last 16-bit word has 1 valid byte - do read-modify-write */
                                temp_data = (*buf<<8 & 0x0000ff00) | (reg_data & 0x000000ff);
                                buf++;
                                len--;
                        }
                        else {                             /* middle 16-bit words have 2 valid bytes */
                                temp_data = *(uint16_t *)buf;
                                buf += 2;
                                len -= 2;
                        }

                     /* write memory-mapped eeprom location in atheros device */
                        cvmx_write64_uint32((pcie_addr_base + AR_EEPROM_BASE + cur_addr32), byte_swap32 (temp_data));

                     /* when busy signal is deasserted, the write has completed */
                        if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &reg_data) < 0) {
                                retval = -1;
                                goto write_protect_eeprom;
                        }
                        cur_addr32 += 4;
                        first_pass = 0;
                }
        }

        retval = 0;

 write_protect_eeprom :
     /* turn on radio eeprom write protection */
        radio_eeprom_write_protect (pcie_device_id, pcie_addr_base, 1);
        return (retval);
}

int radio_eeprom_read (int bd, int addr, char *buf, int len)
{
        DECLARE_GLOBAL_DATA_PTR;
        uint64_t pcie_addr_base;
        int reg_data;
        uint cur_addr32;
        int first_pass = 1;
        unsigned short pcie_device_id;
        int pcie_port;

        pcie_device_id = radio_pcie_device_id_array[bd];
        pcie_port = (gd->arch.board_desc.rev_major == BD_XD2_230 ||
                     gd->arch.board_desc.rev_major == BD_XD2_240 ||
                     gd->arch.board_desc.rev_major == BD_XD3_230 ||
                     gd->arch.board_desc.rev_major == BD_XR600   ||
                     gd->arch.board_desc.rev_major == BD_XR700   ||
                     gd->arch.board_desc.rev_major == BD_XR1000) ? bd : pcie_port_with_radios;
        pcie_addr_base = (cvmx_pcie_get_mem_base_address(pcie_port) | (1ull << 63)) + radio_pcie_bar0_array[bd];

     /* make sure eeprom is not busy when entering routine */
        if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &reg_data) < 0)
                return (-1);

     /* for BCM43465 */
        if (is_bcm_radio(pcie_device_id)) {
                while (len > 0) {

		        if (addr > ((BCM_EEPROM_MAX_BYTE_OFFSET + 2)>>1)) {
                                printf ("## exceeded radio eeprom maximum offset. ##\n");
                                        return (-1);
                        }
		     /* program address */
                        cvmx_write64_uint32((pcie_addr_base + BCM43465_EEPROM_ADDR), byte_swap32(addr));

		     /* start read access */
                        cvmx_write64_uint32((pcie_addr_base + BCM43465_EEPROM_CTRL), byte_swap32(BCM43465_EEPROM_START | BCM43465_EEPROM_READ));

                        if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &reg_data) < 0)
                                return (-1);

                        reg_data = byte_swap32(cvmx_read64_uint32(pcie_addr_base + BCM43465_EEPROM_DATA));

                        if (first_pass && (addr & 0x1)) {  /* first 16-bit word has 1 valid byte */
                                *buf = reg_data;
                                buf++;
                                len--;
                        }
                        else if (len == 1) {               /* last 16-bit word has 1 valid byte */
                                *buf = reg_data>>8;
                                buf++;
                                len--;
                        }
                        else {                             /* middle 16-bit words have 2 valid bytes */
                                *(uint16_t*)buf = reg_data;
                                buf += 2;
                                len -= 2;
                        }
                        addr += 2;
                        first_pass = 0;
                }
        }
     /* for AR98XX */
        else if (pcie_device_id == AR9890_DEVICE_ID_DEFAULT) {
                while (len > 0) {

		        if (addr > ((AR_EEPROM_MAX_BYTE_OFFSET + 2)>>1)) {
                                printf ("## exceeded radio eeprom maximum offset. ##\n");
                                        return (-1);
                        }
		     /* send 3 bytes of i2c data --> "i2c_addr + write_bit", eeprom_addr[15:8], eeprom_addr[7:0] */
                        cvmx_write64_uint32((pcie_addr_base + AR9890_TX_DATA0), (0xa0<<24) | ((addr & 0xffff)<<8));
                        cvmx_write64_uint32((pcie_addr_base + AR9890_SI_CS), 0x03010000);

                        if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &reg_data) < 0)
                                return (-1);

		     /* send 1 byte of i2c data --> "i2c_addr + read_bit"; receive 1 byte of i2c data */
                        cvmx_write64_uint32((pcie_addr_base + AR9890_TX_DATA0), (0xa1<<24));
                        cvmx_write64_uint32((pcie_addr_base + AR9890_SI_CS), 0x10010000);

                        if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &reg_data) < 0)
                                return (-1);

                        reg_data = cvmx_read64_uint32(pcie_addr_base + AR9890_RX_DATA0);
                        *buf = reg_data>>24;

                buf++;
                addr++;
                len--;
                }
        }
     /* for AR92XX and AR93XX */
        else {
                cur_addr32 = 2*addr & 0xfffffffc;

                while (len > 0) {

                        if ((cur_addr32) > AR_EEPROM_MAX_BYTE_OFFSET) {
                                printf ("## exceeded radio eeprom maximum offset. ##\n");
                                        return (-1);
                        }

                     /* read memory-mapped eeprom location in atheros device.  the actual read data is found in eeprom status register */
                        cvmx_read64_uint32(pcie_addr_base + AR_EEPROM_BASE + cur_addr32);

                     /* when busy signal is deasserted, read data is valid */
                        if (radio_eeprom_status (bd, pcie_device_id, pcie_addr_base, &reg_data) < 0)
                                return (-1);

                        if (first_pass && (addr & 0x1)) {  /* first 16-bit word has 1 valid byte */
                                *buf = reg_data;
                                buf++;
                                len--;
                        }
                        else if (len == 1) {               /* last 16-bit word has 1 valid byte */
                                *buf = reg_data>>8;
                                buf++;
                                len--;
                        }
                        else {                             /* middle 16-bit words have 2 valid bytes */
                                *(uint16_t*)buf = reg_data;
                                buf += 2;
                                len -= 2;
                        }
                        cur_addr32 += 4;
                        first_pass = 0;
                }
        }
        return (0);
}

int red_last_bd;
int red_last_offset;
int red_last_len = 0x20;

int do_radio_eeprom_display (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        uint16_t buf;
        int bd, offset, len;

	/* We use the last specified parameters, unless new ones are entered. */
	bd = red_last_bd;
	offset = red_last_offset;
	len = red_last_len;

	if (argc < 3) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	if ((flag & CMD_FLAG_REPEAT) == 0) {
		/* Address is specified since argc > 1
		*/
		bd = simple_strtoul(argv[1], NULL, 16);
                if (bd > 19) {
                        printf ("Board # must be between 0 and 19 (decimal).\n");
                        return (1);
                }

		offset = simple_strtoul(argv[2], NULL, 16);

		/* If another parameter, it is the length to display.
		 * Length is the number of objects, not number of bytes.
		 */
		if (argc > 3)
			len = simple_strtoul(argv[3], NULL, 16);
	}

	red_last_bd = bd;
	red_last_len = len;

        if (!radio_pcie_bar0_array[bd]) {  /* check to see if radio has a non-zero BAR entry - i.e., if radio was enumerated */
                printf ("## radio board %d not detected during PCIe enumeration. ##\n", bd);
                return (-1);
        }

        if (!radio_devid_check(bd,1))
                return (-1);

	/* Print the lines */
	while (len-- > 0) {
                if (radio_eeprom_read (bd, 2*offset, (char *)&buf, 2) < 0)
                              break;
                printf ("%04x %04x\n", offset, buf);
                offset++;
	}

        putc ('\n');
	red_last_offset = offset;

	return (0);
}

#define CFG_RADIO_EEPROM_TEST
#ifdef CFG_RADIO_EEPROM_TEST
int do_radio_eeprom_write ( cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        uint16_t buf;
        int bd, offset;

        if ((argc != 4)) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	bd = simple_strtoul(argv[1], NULL, 16);
        offset = simple_strtoull(argv[2], NULL, 16);
        buf = simple_strtoull(argv[3], NULL, 16);

        if (!radio_pcie_bar0_array[bd]) {  /* check to see if radio has a non-zero BAR entry - i.e., if radio was enumerated */
                printf ("## radio board %d not detected during PCIe enumeration. ##\n", bd);
                return (-1);
        }

        if (!radio_devid_check(bd,1))
                return (-1);

        radio_eeprom_write (bd, 2*offset, (char *)&buf, 2);

    return(0);
}

int do_radio_eeprom_write_test ( cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        char buf[16] = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f};
        int bd, addr, len;

        if ((argc != 4)) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	bd = simple_strtoul(argv[1], NULL, 16);
        addr = simple_strtoull(argv[2], NULL, 16);
        len = simple_strtoull(argv[3], NULL, 16);

        if (!radio_pcie_bar0_array[bd]) {  /* check to see if radio has a non-zero BAR entry - i.e., if radio was enumerated */
                printf ("## radio board %d not detected during PCIe enumeration. ##\n", bd);
                return (-1);
        }

        if (!radio_devid_check(bd,1))
                return (-1);

        radio_eeprom_write (bd, addr, buf, len);

        return(0);
}
#endif

/************************************************************************************************************************
 * General Radio Information Display Routines                                                                           *
 ************************************************************************************************************************/

/* Radio Information
 *
 * Syntax:
 *      ri
 */

int do_radio_ri ( cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        radio_mfg_data_t radio_data;
        ext_radio_mfg_data_t ext_radio_data;
        int i, num_boards = 0;
        int ext_radio_data_valid;
        int vendor;

        for (i = 0; i < MAX_NUM_RADIOS; i++) {  /* radios with BAR entry of zero will be skipped - i.e., radio was not enumerated */
                if (radio_pcie_bar0_array[i])
                        radio_devid_check(i,1);  /* validate PCI device ID */
        }

        for (i = 0; i < MAX_NUM_RADIOS; i++) {  /* radios with BAR entry of zero will be skipped - i.e., radio was not enumerated */
                if (radio_pcie_bar0_array[i] && (vendor = radio_devid_check(i,0)) &&
                    radio_eeprom_read (i, (vendor == QCA) ? AR_EEPROM_MFG_BYTE_OFFSET : BCM_EEPROM_MFG_BYTE_OFFSET, (char *)&radio_data, sizeof(radio_data)) == EEPROM_SUCCESS) {

                        if ((radio_eeprom_read (i, (vendor == QCA) ? AR_EEPROM_EXT_MFG_BYTE_OFFSET : BCM_EEPROM_EXT_MFG_BYTE_OFFSET, (char *)&ext_radio_data, sizeof(ext_radio_data)) == EEPROM_SUCCESS) &&
                            (check_sum((uchar *)&ext_radio_data, sizeof(ext_radio_data)) == 0))
                                ext_radio_data_valid = 1;
                        else
                                ext_radio_data_valid = 0;

                        ++num_boards;
                        printf("\n");
                        printf("Radio Board %d Manufacturing Data:\n", i);
                        printf("=================================\n");

                        if (check_sum((uchar *)&radio_data, sizeof(radio_data)) != 0) {
                                if (vendor == QCA)
                                        qca_data_check(i);
                                else
                                        printf("  ## Invalid checksum ##\n");
                                continue;
                        }

                        if (ext_radio_data_valid && radio_data.hw.type != 0xffff && radio_data.hw.version != 0xff && radio_data.hw.revision != 0xff) {
                                if (ext_radio_data.design != 0xffff) {
                                       printf("Board ID, Ser Num: %03u-%04u-%03u.%c", byte_swap16(ext_radio_data.design), byte_swap16(radio_data.hw.type),
                                              radio_data.hw.version, RAD_BOARD_MAJ_REV(radio_data.hw.revision));
                                       printf(RAD_BOARD_MIN_REV(radio_data.hw.revision) ? "%-2d" : "  ", RAD_BOARD_MIN_REV(radio_data.hw.revision));
                                }
                                else {
                                       printf("Board ID, Ser Num: ----%04u-%03u.%c", byte_swap16(radio_data.hw.type),
                                              radio_data.hw.version, RAD_BOARD_MAJ_REV(radio_data.hw.revision));
                                       printf(RAD_BOARD_MIN_REV(radio_data.hw.revision) ? "%-2d" : "  ", RAD_BOARD_MIN_REV(radio_data.hw.revision));
                                }
                        }
                        else if (radio_data.hw.type != 0xffff && radio_data.hw.version != 0xff && radio_data.hw.revision != 0xff) {
                                printf("Board ID, Ser Num: %04u-%03u.%c", byte_swap16(radio_data.hw.type),
                                       radio_data.hw.version, RAD_BOARD_MAJ_REV(radio_data.hw.revision));
                                printf(RAD_BOARD_MIN_REV(radio_data.hw.revision) ? "%-2d" : "  ", RAD_BOARD_MIN_REV(radio_data.hw.revision));
                        }
                        else
                                printf("Board ID, Ser Num: -----.---.---");
                        if (radio_data.ser_num != 0xffffffff)
                                printf("  %010u\n", byte_swap32(radio_data.ser_num));
                        else
                                printf("  ----------\n");
                        if (radio_data.mfg.date.cent != 0xff && radio_data.mfg.date.year != 0xff && radio_data.mfg.date.mon != 0xff && radio_data.mfg.date.mday != 0xff)
                                printf("Manufacture Date : %2d%02d-%s-%02d",
                                       radio_data.mfg.date.cent, radio_data.mfg.date.year, mon_strs[radio_data.mfg.date.mon & 0xf], radio_data.mfg.date.mday);
                        else
                                printf("Manufacture Date : ----/---/--");
                        if (radio_data.mfg.time.hour != 0xff && radio_data.mfg.time.min != 0xff)
                                printf(" %2d:%02d\n", radio_data.mfg.time.hour, radio_data.mfg.time.min);
                        else
                                printf(" --:--\n");
                        if (radio_data.mfg.location != 0xff)
                                printf("Manufacture Site : %3d\n", radio_data.mfg.location);
                        else
                                printf("Manufacture Site : ---\n");

                        if (ext_radio_data_valid) {
                                if (ext_radio_data.rack == 0xff) {
                                        printf("Board Calibrated : ---\n");
                                        printf("Calibration Rack : ---\n");
                                        printf("Validation Result: ---\n");
                                }
                                else if (!ext_radio_data.flags.s.calibrated) {
                                        printf("Board Calibrated : No\n");
                                }
                                else {
                                        printf("Board Calibrated : Yes\n");
                                        printf("Calibration Rack : %3d\n", ext_radio_data.rack);
                                        printf("Validation Result: %s\n" , ext_radio_data.flags.s.test_result ? "Pass" : "Fail");
                                }
                        }
                }
        }
        if (num_boards == 0)
                printf("No radio boards found\n");
        printf("\n");
        return(0);
}

/* the XR620, XR630, & XD4 have the radio board manufacturing data stored in the customer data area of the QCA EEPROM structure in ASCII format */
void qca_data_check (int i) {
        qca_radio_data_t qca_rd;
        DECLARE_GLOBAL_DATA_PTR;

        if (((gd->arch.board_desc.rev_major == BD_XD4_130) ||
             (gd->arch.board_desc.rev_major == BD_XR600)   ||
             (gd->arch.board_desc.rev_major == BD_XR700))  &&
            radio_eeprom_read (i, QCA_RADIO_DATA_BYTE_OFFSET, (char *)&qca_rd, sizeof(qca_rd)) == EEPROM_SUCCESS) {
                if (qca_data_process((u8 *)&qca_rd) == 0) {
                        if (qca_rd.rev_letter == 0)
                                printf("Board ID, Ser Num: 01%u%u-%03u.%u",
                                       qca_rd.part_num[0], qca_rd.part_num[1], qca_rd.version_num, qca_rd.rev_num);
                        else
                                printf("Board ID, Ser Num: 01%u%u-%03u.%X%u",
                                       qca_rd.part_num[0], qca_rd.part_num[1], qca_rd.version_num, qca_rd.rev_letter, qca_rd.rev_num);
                        printf("   000%u%u%u%u%u%u%u\n",
                               qca_rd.ser_num[0], qca_rd.ser_num[1], qca_rd.ser_num[2], qca_rd.ser_num[3], qca_rd.ser_num[4], qca_rd.ser_num[5], qca_rd.ser_num[6]);
                        printf("Manufacture Date : 20%u%u-%s-%u%u %u%u:00\n",
                               qca_rd.year[0], qca_rd.year[1], mon_strs[qca_rd.month[1] + (qca_rd.month[0] ? 10 : 0)], qca_rd.day[0], qca_rd.day[1], qca_rd.hour[0], qca_rd.hour[1]);
                }
                else
                        printf("  ## Invalid Radio Manufacture Data ##\n");
        }
        else
                printf("  ## Invalid checksum ##\n");
}

/* convert from ASCII to HEX and verify the data appears to be valid */
int qca_data_process(u8 *qca_rd_ptr) {
        int i;
        int all_0s;
        int all_fs;
        int invalid;

        all_0s = 1;
        all_fs = 1;
        invalid = 0;
        for (i=0; i<sizeof(qca_radio_data_t); i++) {
                all_0s &= (*qca_rd_ptr == 0);
                all_fs &= (*qca_rd_ptr == 0xff);
                *qca_rd_ptr = ascii_to_hex(*qca_rd_ptr); // convert from ascii to hex
                if (i==0 && *qca_rd_ptr > 9)  invalid = 1; // year[0]
                if (i==1 && *qca_rd_ptr > 9)  invalid = 1; // year[1]
                if (i==2 && *qca_rd_ptr > 1)  invalid = 1; // month[0]
                if (i==3 && *qca_rd_ptr > 9)  invalid = 1; // month[1]
                if (i==4 && *qca_rd_ptr > 3)  invalid = 1; // day[0]
                if (i==5 && *qca_rd_ptr > 9)  invalid = 1; // day[1]
                if (i==6 && *qca_rd_ptr > 2)  invalid = 1; // hour[0]
                if (i==7 && *qca_rd_ptr > 9)  invalid = 1; // hour[1]
                qca_rd_ptr++;
        }
        return (all_0s || all_fs || invalid);
}

/************************************************************************************************************************
 * Radio Board manufacturing Data Setup Routines                                                                        *
 ************************************************************************************************************************/

static struct base_def_t {
        char *name;
        int   addr,   size;
} *base_ptr, base_defs[] = {
        {"board",      16, 1},  {"bd",  16, 1},
        {"bdid",        4, 4},
        {"clear",      18, 1},  {"clr", 18, 1},
        {"hwid",        4, 4},
        {"id",          4, 4},
        {"manufacture", 8, 1},  {"manuf",8, 1}, {"mfg",  8, 1},
        {"save",       17, 1},
        {"serial",      0, 4},  {"ser#", 0, 4},
        {NULL,          0, 0}
};

static struct offset_def_t {
        char *name;
        int   offset;
} *offset_ptr, offset_defs[] = {
        {"date",     0},
        {"hardware", 0},        {"hw",    0},
        {"id",       0},
        {"location", 6},        {"loc",   6},
        {"now",     -1},
        {"number",   0},        {"num",   0},
        {"radio",    0},        {"rad",   0},
        {"site",     6},
        {"time",     4},
        {NULL,       0}
};

static struct reg_def_t {
        char *reg_name;
        int   divisor;
        int   base;
} reg_defs[] = {
        {"serial number",        1, 10},        {"serial number",        1, 10},
        {"serial number",        1, 10},        {"serial number",        1, 10},
        {"board identification", 1, 10},        {"board type",           1, 10},
        {"board version",        1, 10},        {"board revision",       1, 10},
        {"manufacture date",     1, 10},        {"manufacture year",     1, 10},
        {"manufacture month",    1, 10},        {"manufacture day",      1, 10},
        {"manufacture time",     1, 10},        {"manufacture minutes",  1, 10},
        {"manufacture location", 1, 10},        {"register checksum",    1, 16},
        {NULL,                   0,  0}
};

static struct ext_base_def_t {
        char *name;
        int   addr, size, bit_mask, bit_num;
} *ext_base_ptr, ext_base_defs[] = {
        {"design",     0, 2, 0, 0},
        {"rack",       2, 1, 0, 0},
        {"val-bit",    3, 1, 1, 1},
        {"cal-bit",    3, 1, 1, 0},
        {"save-ext",  19, 1, 0, 0},
        {"clear-ext", 20, 1, 0, 0}, {"clr-ext", 20, 1, 0, 0},
        {NULL,         0, 0, 0, 0}
};

static struct reg_def_t ext_reg_defs[] = {
        {"board design id ",     1, 10},        {"board design id",      1, 10},
        {"rack number",          1, 10},        {"calibration flags",    1, 16},
        {NULL,                   0,  0}
};

int do_radio_rs (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        int radio_bd  = -1;
        int base_addr = 0;
        int reg_size  = 0;
        int offset    = 0;
        int reg_num   = 0;
        unsigned value= 0;
        char *value_ptr;
        char *cur_argv;
        char verify[8];
        struct rtc_time rtc;
        mfg_info_t mfg;
        mfg_board_id_t board;
        radio_mfg_data_t radio_data;
        ext_radio_mfg_data_t ext_radio_data;
        char *s;
        int year;
        int ext_reg_data = 0;
        int reg_bit_mask = 0;
        int reg_bit_num = 0;
        int vendor = -1;
        int mfg_byte_offset = 0;
        int ext_mfg_byte_offset = 0;
        int muxed_mfg_byte_offset;
        int index = 0;
        int ext_reg_index = 0;
        char flags;
        //int i;

        while (--argc) {
                cur_argv = *++argv;
                if (strcmp(cur_argv, "help") == 0) {
                        printf("Usage: rs [basename offset [ ... offset] value] ...");

                        printf("\n\n   - basename: ");
                        base_ptr = base_defs;
                        while (base_ptr->name) {
                                printf("%-12s ", base_ptr->name);
                                if ((++base_ptr - base_defs) % 5 == 0 && base_ptr->name)
                                        printf("\n               ");
                        }

                        printf("\n\n   - offset  : ");
                        offset_ptr = offset_defs;
                        while (offset_ptr->name) {
                                printf("%-12s ", offset_ptr->name);
                                if ((++offset_ptr - offset_defs) % 5 == 0 && offset_ptr->name)
                                        printf("\n               ");
                        }

                        printf("\n\n   extended data area");
                        printf("\n\n   - basename: ");
                        ext_base_ptr = ext_base_defs;
                        while (ext_base_ptr->name) {
                                printf("%-12s ", ext_base_ptr->name);
                                if ((++ext_base_ptr - ext_base_defs) % 5 == 0 && ext_base_ptr->name)
                                        printf("\n               ");
                        }
                        printf("\n");
                        return 0;
                }
                for (base_ptr = base_defs; base_ptr->name; base_ptr++) {
                        if (strcmp(cur_argv, base_ptr->name) == 0) {
                                ext_reg_data = 0;
                                base_addr  = base_ptr->addr;
                                reg_size   = base_ptr->size;
                                if (base_addr != RAD_SAVE_CMD && base_addr != RAD_CLR_CMD)
                                        goto next_arg;
                        }
                }
                for (ext_base_ptr = ext_base_defs, index = 0; ext_base_ptr->name; ext_base_ptr++, index++) {
                        if (strcmp(cur_argv, ext_base_ptr->name) == 0) {
                                ext_reg_data  = 1;
                                base_addr     = ext_base_ptr->addr;
                                reg_size      = ext_base_ptr->size;
                                reg_bit_mask  = ext_base_ptr->bit_mask;
                                reg_bit_num   = ext_base_ptr->bit_num;
                                ext_reg_index = index;
                                if (base_addr != RAD_EXT_SAVE_CMD && base_addr != RAD_EXT_CLR_CMD)
                                        goto next_arg;
                        }
                }
                for (offset_ptr = offset_defs; offset_ptr->name; offset_ptr++) {
                        if (strcmp(cur_argv, offset_ptr->name) == 0 && offset_ptr->offset >= 0) {
                                offset += offset_ptr->offset;
                                goto next_arg;
                        }
                }
                if (!reg_size) {
                        puts("Syntax error: register base not specified\n");
                        goto next_reg;
                }
                reg_num = base_addr + offset;
                if (reg_num >= RAD_CMD_CNT) {
                        printf("Syntax error: register number (%d) out of range\n", reg_num);
                        goto next_reg;
                }
                if (reg_num != RAD_RADIO_SEL && (radio_bd < 0 || radio_bd >= MAX_NUM_RADIOS)) {
                        printf("Syntax error: radio board number invalid\n");
                        goto next_reg;
                }
                switch (reg_num) {
                        case RAD_RADIO_SEL:
                                radio_bd = simple_strtoul(cur_argv, NULL, 0);
                                if (!radio_pcie_bar0_array[radio_bd]) {  /* check to see if radio has a non-zero BAR entry - i.e., if radio was enumerated */
                                        printf ("## radio board %d not detected during PCIe enumeration. ##\n", radio_bd);
                                        return (-1);
                                }
                                if (!(vendor = radio_devid_check(radio_bd,1)))
                                        return (-1);
#if 0 // this is temporary until we know where manufacturing data will be stored in eeprom on Broadcom radios
                                if (radio_devid_check(radio_bd,1) == BCM) {
                                        printf("### Broadcom radios not yet supported ###\n");
                                        return (0);
                                }
#endif
                                mfg_byte_offset = (vendor == QCA) ? AR_EEPROM_MFG_BYTE_OFFSET : BCM_EEPROM_MFG_BYTE_OFFSET;
                                ext_mfg_byte_offset = (vendor == QCA) ? AR_EEPROM_EXT_MFG_BYTE_OFFSET : BCM_EEPROM_EXT_MFG_BYTE_OFFSET;
                                goto next_reg;

                        case RAD_SAVE_CMD:
                                printf("Saving   Radio Board %d Manufacturing Data ", radio_bd);
                                if (vendor > 0 && radio_eeprom_read(radio_bd, mfg_byte_offset, (char *)&radio_data, sizeof(radio_data)) != EEPROM_SUCCESS) {
                                        puts(" ## Read failed ##\n");
                                        goto next_reg;
                                }
                                radio_data.chksum -= check_sum((uchar *)&radio_data, sizeof(radio_data));
                                value_ptr = (char *)&radio_data.chksum;
                                reg_size  = 1;
                                reg_num   = RAD_CHKSUM;
                                break;

                        case RAD_EXT_SAVE_CMD:
                                printf("Saving   Extended Radio Board %d Manufacturing Data ", radio_bd);
                                if (vendor > 0 && radio_eeprom_read(radio_bd, ext_mfg_byte_offset, (char *)&ext_radio_data, sizeof(ext_radio_data)) != EEPROM_SUCCESS) {
                                        puts(" ## Read failed ##\n");
                                        goto next_reg;
                                }
                                ext_radio_data.version = RAD_EXT_VER_NUM;
                                ext_radio_data.chksum -= check_sum((uchar *)&ext_radio_data, sizeof(ext_radio_data));
                                value_ptr = (char *)&ext_radio_data.version;  /* save version & checksum */
                                reg_size  = 2;
                                reg_num   = RAD_EXT_CHKSUM;
                                break;

                        case RAD_CLR_CMD:
                                printf("Clearing Radio Board %d Manufacturing Data ", radio_bd);
                                memset(&radio_data, 0xff, sizeof(radio_data));
                                if (vendor > 0 && radio_eeprom_write(radio_bd, mfg_byte_offset, (char *)&radio_data, sizeof(radio_data)) != EEPROM_SUCCESS) {
                                        puts(" ## Write failed ##\n");
                                        goto next_reg;
                                }
                                radio_data.chksum -= check_sum((uchar *)&radio_data, sizeof(radio_data));
                                value_ptr = (char *)&radio_data.chksum;
                                reg_size  = 1;
                                reg_num   = RAD_CHKSUM;
                                break;

                        case RAD_EXT_CLR_CMD:
                                printf("Clearing Extended Radio Board %d Manufacturing Data ", radio_bd);
                                memset(&ext_radio_data, 0xff, sizeof(ext_radio_data));
                                if (vendor > 0 && radio_eeprom_write(radio_bd, ext_mfg_byte_offset, (char *)&ext_radio_data, sizeof(ext_radio_data)) != EEPROM_SUCCESS) {
                                        puts(" ## Write failed ##\n");
                                        goto next_reg;
                                }
                                ext_radio_data.version = RAD_EXT_VER_NUM;
                                ext_radio_data.chksum -= check_sum((uchar *)&ext_radio_data, sizeof(ext_radio_data));
                                value_ptr = (char *)&ext_radio_data.version;  /* save version & checksum */
                                reg_size  = 2;
                                reg_num   = RAD_EXT_CHKSUM;
                                break;

                        case RAD_BOARD_ID:
                                board.type     = simple_strtoul(cur_argv, &s, 10);
                                if (*s) s++;
                                board.version  = simple_strtoul(s, &s, 10);
                                if (*s) s++;
                                board.revision = RAD_BOARD_SET_REV(s);
                                printf("Setting  Radio Board %d %-20s to %04u-%03u.%c", radio_bd, reg_defs[reg_num].reg_name,
                                       board.type, board.version, RAD_BOARD_MAJ_REV(board.revision));
                                printf(RAD_BOARD_MIN_REV(board.revision) ? "%-2d " : "   ", RAD_BOARD_MIN_REV(board.revision));
                                board.type = byte_swap16(board.type);
                                value_ptr  = (char *)&board;
                                reg_size   = sizeof(board);
                                break;

                        case RAD_MAN_TIME:
                        case RAD_MAN_DATE:
                                if (strcmp(cur_argv, "now") == 0) {
                                        rtc_get(&rtc);
                                        mfg.time.hour = rtc.tm_hour;
                                        mfg.time.min  = rtc.tm_min;
                                        mfg.date.cent = rtc.tm_year/100;
                                        mfg.date.year = rtc.tm_year%100;
                                        mfg.date.mon  = rtc.tm_mon;
                                        mfg.date.mday = rtc.tm_mday;
                                        printf("Setting  Radio Board %d %-20s to %d:%02d and\n",   radio_bd, reg_defs[RAD_MAN_TIME].reg_name,
                                               mfg.time.hour, mfg.time.min);
                                        printf("Setting  Radio Board %d %-20s to %d%02d-%s-%02d ", radio_bd, reg_defs[RAD_MAN_DATE].reg_name,
                                               mfg.date.cent, mfg.date.year, mon_strs[mfg.date.mon & 0xf], mfg.date.mday);
                                        value_ptr = (char *)&mfg;
                                        reg_size  = sizeof(mfg);
                                        reg_num   = RAD_MAN_DATE;
                                }
                                else if (reg_num == RAD_MAN_TIME) {
                                        mfg.time.hour = simple_strtoul(cur_argv, &s, 10);
                                        if (*s) s++;
                                        mfg.time.min  = simple_strtoul(s, &s, 10);
                                        printf("Setting  Radio Board %d %-20s to %d:%02d ", radio_bd, reg_defs[reg_num].reg_name,
                                               mfg.time.hour, mfg.time.min);
                                        value_ptr = (char *)&mfg.time;
                                        reg_size  = sizeof(mfg.time);
                                }
                                else {
                                        year = simple_strtoul(cur_argv, &s, 10);
                                        mfg.date.cent = year/100;
                                        mfg.date.year = year%100;
                                        if (*s) s++;
                                        mfg.date.mon  = simple_strtoul(s, &s, 10);
                                        if (*s) s++;
                                        mfg.date.mday = simple_strtoul(s, NULL, 10);
                                        printf("Setting  Radio Board %d %-20s to %d%02d-%s-%02d ", radio_bd, reg_defs[reg_num].reg_name,
                                               mfg.date.cent, mfg.date.year, mon_strs[mfg.date.mon & 0xf], mfg.date.mday);
                                        value_ptr = (char *)&mfg.date;
                                        reg_size  = sizeof(mfg.date);
                                }
                                break;

                        default:
                                if (!ext_reg_data) {
                                       value = simple_strtoul(cur_argv, NULL, reg_defs[reg_num].base);
                                       printf(reg_defs[reg_num].base == 16 ? "Setting  Radio Board %d %-20s to %x " : "Setting  Radio Board %d %-20s to %u ",
                                              radio_bd, reg_defs[reg_num].reg_name, value);
                                       value /= reg_defs[reg_num].divisor;
                                       value = byte_swap32(value);
                                       value_ptr = (char *)&value;
                                }
                                else {
                                       value = simple_strtoul(cur_argv, NULL, ext_reg_defs[reg_num].base);
                                       if (reg_bit_mask) {
                                               if (value != 0 && value != 1) {
                                                       puts("Syntax error: value must be 0 or 1\n");
                                                       goto next_reg;
                                               }
                                               if (radio_eeprom_read(radio_bd, ext_mfg_byte_offset + reg_num, &flags, reg_size) != EEPROM_SUCCESS) {
                                                       puts(" ## Read failed ##\n");
                                                       goto next_reg;
                                               }
                                               printf("Setting  Radio Board %d %-20s to %x ", radio_bd, ext_base_defs[ext_reg_index].name, value);
                                               value = (flags & ~(1<<reg_bit_num)) | (value<<reg_bit_num);
                                       }
                                       else {
                                               printf(ext_reg_defs[reg_num].base == 16 ? "Setting  Radio Board %d %-20s to %x " : "Setting  Radio Board %d %-20s to %u ",
                                                      radio_bd, ext_reg_defs[reg_num].reg_name, value);
                                       }
                                       value /= ext_reg_defs[reg_num].divisor;
                                       value = byte_swap32(value);
                                       value_ptr = (char *)&value;
                                }
                }
                muxed_mfg_byte_offset = ext_reg_data ? ext_mfg_byte_offset : mfg_byte_offset;
                if (vendor > 0 && radio_eeprom_write(radio_bd, muxed_mfg_byte_offset + reg_num, value_ptr, reg_size) != EEPROM_SUCCESS)
                        puts(" ## Write failed ##\n");
                else if (vendor <= 0 || radio_eeprom_read(radio_bd, muxed_mfg_byte_offset + reg_num, verify, reg_size) != EEPROM_SUCCESS || memcmp(value_ptr, verify, reg_size) != 0) {
                        puts(" ## Verify failed ##\n");
                        //for (i=0;i<reg_size;i++) {
                        //  printf("i=%d value=%02x verify=%02x\n",i,*(value_ptr+i),*(verify+i));
                        //}
                }
                else
                        puts("\n");
next_reg:
                offset = 0;
next_arg:
                value = 0;
        }
        return 0;
}

int do_ath_dump (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        DECLARE_GLOBAL_DATA_PTR;
        unsigned short mii_data;
        int i;

        if ((gd->arch.board_desc.rev_major != BD_XD4_130) &&
            (gd->arch.board_desc.rev_major != BD_XR500)   &&
            (gd->arch.board_desc.rev_major != BD_XR600)   &&
            (gd->arch.board_desc.rev_major != BD_XR700)   &&
            (gd->arch.board_desc.rev_major != BD_XR2100)) {
                printf ("This platform does not have an AR8033 PHY.\n");
                return 1;
        }

        printf ("\n");
        printf ("-----------------------------------------\n");
        printf ("  AR8033 Register Dump (address : data)  \n");
        printf ("-----------------------------------------\n");

        printf ("## MII Registers ##\n");
        for (i=0; i<=31; i++) {
                xir_miiphy_read(0x1, i, &mii_data);
                printf ("0x%04x : 0x%04x\n", i, mii_data);
        }

        printf ("## Debug Registers ##\n");
        unsigned short debug_addr[] = {0x00, 0x05, 0x0b, 0x10, 0x11, 0x12, 0x3d};
        for (i=0; i<(sizeof(debug_addr)/sizeof(debug_addr[0])); i++) {
                mii_data = debug_addr[i];
                xir_miiphy_write(0x1, 0x1d,  mii_data);  // debug register address
                xir_miiphy_read( 0x1, 0x1e, &mii_data);  // debug register data
                printf ("0x%04x : 0x%04x\n", debug_addr[i], mii_data);
        }

        printf ("## MMD3 Registers ##\n");

        unsigned short mmd3_addr[] = {0x00, 0x01, 0x14, 0x16, 0x804a, 0x804b, 0x804c, 0x805a, 0x805b, 0x805c, 0x805d};

        for (i=0; i<(sizeof(mmd3_addr)/sizeof(mmd3_addr[0])); i++) {
                mii_data = 0x3;
                xir_miiphy_write(0x1, 0xd,  mii_data);  // set device address
                mii_data = mmd3_addr[i];
                xir_miiphy_write(0x1, 0xe,  mii_data);  // set register offset
                mii_data = 0x4003;
                xir_miiphy_write(0x1, 0xd,  mii_data);  // set device data access
                xir_miiphy_read( 0x1, 0xe, &mii_data);  // register data
                printf ("0x%04x : 0x%04x\n", mmd3_addr[i], mii_data);
        }

        printf ("## MMD7 Registers ##\n");

        unsigned short mmd7_addr[] = {0x00, 0x01, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x3c, 0x3d, 0x8000, 0x8011, 0x8016};

        for (i=0; i<(sizeof(mmd7_addr)/sizeof(mmd7_addr[0])); i++) {
                mii_data = 0x7;
                xir_miiphy_write(0x1, 0xd,  mii_data);  // set device address
                mii_data = mmd7_addr[i];
                xir_miiphy_write(0x1, 0xe,  mii_data);  // set register offset
                mii_data = 0x4007;
                xir_miiphy_write(0x1, 0xd,  mii_data);  // set device data access
                xir_miiphy_read( 0x1, 0xe, &mii_data);  // register data
                printf ("0x%04x : 0x%04x\n", mmd7_addr[i], mii_data);
        }

        printf ("\n");
        return(0);
}

/************************************************************************************************************************
 * Radio Command Definitions                                                                                            *
 ************************************************************************************************************************/

U_BOOT_CMD(
        red,    4,     1,      do_radio_eeprom_display,
        "read 16-bit words from radio eeprom",
	"board offset [# of objects]\n    - radio eeprom display"
);

#ifdef CFG_RADIO_EEPROM_TEST
U_BOOT_CMD(
        rew,    4,     1,      do_radio_eeprom_write,
        "write 16-bit words to radio eeprom",
	"board offset value\n    - radio eeprom write"
);

U_BOOT_CMD(
        rewt,   4,     1,      do_radio_eeprom_write_test,
        "write fixed data to radio eeprom to test byte alignment",
	"board addr len\n    - radio eeprom write byte alignment test"
);
#endif

U_BOOT_CMD(
        ri,     1,     0,      do_radio_ri,
        "radio information",
        "\n"
);

U_BOOT_CMD(
        rs,     CONFIG_SYS_MAXARGS,  0,      do_radio_rs,
        "radio set manufacturing information",
        "[basename offset [ ... offset] value] ...\n"
        "    - type 'rs help' for complete list of valid basenames and offsets"
);

U_BOOT_CMD(
        ath_dump,   1,     0,      do_ath_dump,
        "dump all documented registers in Atheros AR8033 Ethernet PHY",
        "\n"
);
