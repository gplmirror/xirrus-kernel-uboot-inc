
/*********************/
/** digital compass **/
/*********************/

#include <common.h>
#include <command.h>
#include <i2c.h>
#include <otis.h>
#include <watchdog.h>

#define COMPASS_I2C_ADDR       0x1e
#define COMPASS_CTLA_REG_ADDR  0x0
#define COMPASS_CTLB_REG_ADDR  0x1
#define COMPASS_MODE_REG_ADDR  0x2
#define COMPASS_DATA_XA_ADDR   0x3
#define COMPASS_DATA_XB_ADDR   0x4
#define COMPASS_DATA_ZA_ADDR   0x5
#define COMPASS_DATA_ZB_ADDR   0x6
#define COMPASS_DATA_YA_ADDR   0x7
#define COMPASS_DATA_YB_ADDR   0x8
#define COMPASS_STATUS_ADDR    0x9
#define SAMPLES_TO_AVERAGE     10

#define absv(x) ((x)>0) ? (x) : -(x)
#define	endtime(seconds) (get_timer(0) + t*1000)

extern uint16_t byte_swap16(uint16_t data);
extern int scd_read (unsigned long offset, uchar *buf, unsigned long len);
extern int scd_write (unsigned long offset, uchar *buf, unsigned long len);
extern int xir_miiphy_read (unsigned char addr, unsigned char reg, unsigned short * value);
extern int xir_miiphy_write (unsigned char addr, unsigned char reg, unsigned short value);

struct compass_data_t {
  short  x_data;
  short  z_data;
  short  y_data;
};

struct compass_info_t {
  int x_data;
  int y_data;
  int x_data_min;
  int x_data_max;
  int y_data_min;
  int y_data_max;
  int x_data_min_cal;
  int x_data_max_cal;
  int y_data_min_cal;
  int y_data_max_cal;
  int degrees;
};

// 0 to 89 degrees arctan lookup table scaled by 1000
int atan_table[90] = {0,17,35,52,70,87,105,123,141,158,
                      176,194,213,231,249,268,287,306,325,344,
                      364,384,404,424,445,466,488,510,532,554,
                      577,601,625,649,675,700,727,754,781,810,
                      839,869,900,933,966,1000,1036,1072,1111,1150,
                      1192,1235,1280,1327,1376,1428,1483,1540,1600,1664,
                      1732,1804,1881,1963,2050,2145,2246,2356,2475,2605,
                      2747,2904,3078,3271,3487,3732,4011,4331,4705,5145,
                      5671,6314,7115,8144,9514,11430,14300,19081,28635,57286};

int x_data_min_cal = -100;
int x_data_max_cal = 100;
int y_data_min_cal = -100;
int y_data_max_cal = 100;

int compass_init (struct compass_info_t *info) {
        uchar  data;

        data = 0xf8;  // average 8 samples; 75Hz
        if (i2c_write(COMPASS_I2C_ADDR, COMPASS_CTLA_REG_ADDR, 1, &data, 1) != 0) {
                return 1;
        }

        data = 0x20;  // gain = 1090
        if (i2c_write(COMPASS_I2C_ADDR, COMPASS_CTLB_REG_ADDR, 1, &data, 1) != 0) {
                return 1;
        }

        data = 0x00;  // continuous measurement mode
        if (i2c_write(COMPASS_I2C_ADDR, COMPASS_MODE_REG_ADDR, 1, &data, 1) != 0) {
                return 1;
        }

        info->x_data_min = 1000;
        info->x_data_max = -1000;
        info->y_data_min = 1000;
        info->y_data_max = -1000;

        return 0;
}

int compass_exit (void) {
        uchar  data;

        data = 0x03;  // idle mode
        if (i2c_write(COMPASS_I2C_ADDR, COMPASS_MODE_REG_ADDR, 1, &data, 1) != 0) {
                return 1;
        }

        return 0;
}

// put ethernet phy in power down state while compass measurements are made since the ethernet magnetics affect the measurement
int set_miiphy_state (uchar enable) {

        int i;
        uchar  addr[] = {0x01, 0x03, 0x82, 0x84};
        ushort phy_data;

        for (i=0; i<4; i++) {
                if (xir_miiphy_read (addr[i], 0x00, &phy_data) != 0) {
                        return 1;
                }
                if (phy_data != 0xffff) {
                        phy_data = enable ? ((phy_data & ~0x0800) | 0x0200) : (phy_data | 0x0800);
                        if (xir_miiphy_write (addr[i], 0x00, phy_data) != 0) {
                                return 1;
                        }
                        //printf ("write - addr %x phy_data %x\n", addr[i], phy_data);
                }
        }
        return 0;
}

int get_miiphy_state (void) {

        int i;
        uchar  addr[] = {0x01, 0x03, 0x82, 0x84};
        ushort phy_data;

        for (i=0; i<4; i++) {
                if (xir_miiphy_read (addr[i], 0x01, &phy_data) != 0)  return 0;
                if (phy_data != 0xffff && (phy_data & 0x0004))    return 1;
		}
		return 0;
}

void get_compass_data (struct compass_info_t *info, int num2avg) {
        uchar  data;
        int    i;
        int    quotient;
        int x_scale_factor;
        int y_scale_factor;
        int x_offset;
        int y_offset;

        struct compass_data_t compass_data;

        info->x_data = 0;
        info->y_data = 0;
        for (i=0; i<num2avg; i++) {  // average over N samples
                data = 0;
                while (!(data & 0x1)) {  // wait until data is ready
                        i2c_read(COMPASS_I2C_ADDR, COMPASS_STATUS_ADDR, 1, &data, 1);
                }
                i2c_read(COMPASS_I2C_ADDR, COMPASS_DATA_XA_ADDR, 1, (uchar *)&compass_data, sizeof(compass_data));
                info->x_data += compass_data.x_data;
                info->y_data += compass_data.y_data;
                //printf ("x_data %d y_data %d status data %x\n", compass_data.x_data, compass_data.y_data, data);
                udelay (20000);  // throttle since compass updates at 75Hz
        }
        info->x_data = info->x_data/num2avg;
        info->y_data = info->y_data/num2avg;

        if ((info->x_data_max_cal - info->x_data_min_cal) < (info->y_data_max_cal - info->y_data_min_cal)) {
                x_scale_factor = ((info->y_data_max_cal - info->y_data_min_cal) * 1000)/(info->x_data_max_cal - info->x_data_min_cal);
                y_scale_factor = 1000;
        }
        else {
                x_scale_factor = 1000;
                y_scale_factor = ((info->x_data_max_cal - info->x_data_min_cal) * 1000)/(info->y_data_max_cal - info->y_data_min_cal);
        }

        x_offset = (((info->x_data_max_cal - info->x_data_min_cal)/2 - info->x_data_max_cal) * x_scale_factor)/1000;
        y_offset = (((info->y_data_max_cal - info->y_data_min_cal)/2 - info->y_data_max_cal) * y_scale_factor)/1000;

        info->x_data = (info->x_data*x_scale_factor)/1000 + x_offset;
        info->y_data = (info->y_data*y_scale_factor)/1000 + y_offset;

        //printf ("x_data %d y_data %d\n", info->x_data, info->y_data);
        //printf ("xsf %d ysf %d xoff %d yoff %d\n", x_scale_factor, y_scale_factor, x_offset, y_offset);

        if (info->x_data < info->x_data_min)
                info->x_data_min = info->x_data;
        if (info->x_data > info->x_data_max)
                info->x_data_max = info->x_data;
        if (info->y_data < info->y_data_min)
                info->y_data_min = info->y_data;
        if (info->y_data > info->y_data_max)
                info->y_data_max = info->y_data;

        if (info->x_data == 0)
                info->degrees = (info->y_data > 0) ? 90 : 270;
        else if (info->y_data == 0)
                info->degrees = (info->x_data > 0) ? 0 : 180;
        else {
                quotient = absv((info->x_data * 1000)/info->y_data);

                for (i=0; i<89; i++) {
                        if ((quotient >= atan_table[i]) && (quotient < atan_table[i+1])) {
                                //printf ("i %d quotient %d atan_table[i] %d atan_table[i+1] %d\n", i, quotient, atan_table[i], atan_table[i+1]);
                                break;
                        }
                }
                if (info->x_data > 0)
                        info->degrees = (info->y_data > 0) ? (90 - i) : (270 + i);
                else
                        info->degrees = (info->y_data > 0) ? (90 + i) : (270 - i);
        }
}

int do_compass (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        struct compass_info_t compass_info;
        struct compass_info_t *info = &compass_info;
        uint64_t t;
        char *s;
        int  i, x_avg, y_avg, d_min, d_max, d_avg;

        if ((argc > 2)) {
                printf ("Usage:\n%s\n", cmdtp->usage);
                return 1;
        }

        if (compass_init(info)) {
                puts ("## error accessing digital compass ##\n");
                return 1;
        }

        if ((s = getenv("compass_time")) == NULL)
                t = 15;
        else
                t = simple_strtoull(s, &s, 10);

        printf("\nRunning for %llu seconds. This time can be changed with the environment variable `compass_time`.\n", t);
        printf("You will not see any output during this period if you are using xircon.\n");
        printf("The ethernet may take up to an additional 5 seconds to come up.\n");

        //disable ethernet phy since it can cause heading measurement to vary by several degrees
        if (set_miiphy_state(0)) {
                puts ("## error accessing ethernet phy ##\n");
                return 1;
        }

        x_avg = y_avg = d_avg = d_max = 0;
        d_min = 360;

        if (argc == 2) {
                printf ("\nx_data  y_data  x_data_min  x_data_max  y_data_min  y_data_max\n");
                printf (  "------  ------  ----------  ----------  ----------  ----------\n");

                info->x_data_min_cal = -100;
                info->x_data_max_cal =  100;
                info->y_data_min_cal = -100;
                info->y_data_max_cal =  100;

                for (i = 0, t = endtime(t); !tstc() && get_timer(0) < t; i++) {
                        get_compass_data (info, 1);

                        x_avg += info->x_data;
                        y_avg += info->y_data;

                        if (!(i & 0xf))  // throttle display
                                printf ("%6d  %6d      %6d      %6d      %6d      %6d\r",
                                        info->x_data, info->y_data, info->x_data_min, info->x_data_max, info->y_data_min, info->y_data_max);
                }
                if (i == 0) {
                        printf ("### error - no compass measurements taken due to early exit from loop ###\n");
                }
                else {
                        x_avg /= i;
                        y_avg /= i;
                }
                printf("\n");

                x_data_min_cal = info->x_data_min;
                x_data_max_cal = info->x_data_max;
                y_data_min_cal = info->y_data_min;
                y_data_max_cal = info->y_data_max;
        }
        else {
                if ((s = getenv("compass_xmin")) != NULL) x_data_min_cal = simple_strtol(s, &s, 10);
                if ((s = getenv("compass_xmax")) != NULL) x_data_max_cal = simple_strtol(s, &s, 10);
                if ((s = getenv("compass_ymin")) != NULL) y_data_min_cal = simple_strtol(s, &s, 10);
                if ((s = getenv("compass_ymax")) != NULL) y_data_max_cal = simple_strtol(s, &s, 10);

                printf ("\nusing the following calibration data ...\n");
                printf ("x_data_min = %d\n", x_data_min_cal);
                printf ("x_data_max = %d\n", x_data_max_cal);
                printf ("y_data_min = %d\n", y_data_min_cal);
                printf ("y_data_max = %d\n", y_data_max_cal);
                printf ("\n");
                printf ("x_data  y_data  x_data_min  x_data_max  y_data_min  y_data_max  degrees\n");
                printf ("------  ------  ----------  ----------  ----------  ----------  -------\n");

                info->x_data_min_cal = x_data_min_cal;
                info->x_data_max_cal = x_data_max_cal;
                info->y_data_min_cal = y_data_min_cal;
                info->y_data_max_cal = y_data_max_cal;

                if ((info->x_data_max_cal == info->x_data_min_cal) || (info->y_data_max_cal == info->y_data_min_cal)) {
                        puts ("## error - min & max calibration values cannot be equal ##");
                        goto clean_exit;
                }

                for (i = 0, t = endtime(t); !tstc() && get_timer(0) < t; i++) {
                        get_compass_data (info, 8);

                        x_avg += info->x_data;
                        y_avg += info->y_data;
                        d_avg += info->degrees;
                        d_min  = MIN(d_min, info->degrees);
                        d_max  = MAX(d_max, info->degrees);

                        printf ("%6d  %6d      %6d      %6d      %6d      %6d   %6d\r",
                                info->x_data, info->y_data, info->x_data_min, info->x_data_max, info->y_data_min, info->y_data_max, info->degrees);
                }
                if (i == 0) {
                        printf ("### error - no compass measurements taken due to early exit from loop ###\n");
                }
                else {
                        x_avg /= i;
                        y_avg /= i;
                        d_avg /= i;
                }
                printf("\n");
        }

    clean_exit:
        if (set_miiphy_state(1)) {
                puts ("## error accessing ethernet phy ##\n");
                return 1;
        }

        for (i = 0; i < 100 && !get_miiphy_state(); i++) {
                WATCHDOG_RESET();
                udelay(100000);
        }
		udelay(250000);
		printf("phy up took %d ms\n", i * 100 + 250);

        if (argc == 2) {
                u16 val;

                val = 0xffff; scd_write(SCD_XMIN, (u8 *)&val, 2);
                val = 0xffff; scd_write(SCD_XMAX, (u8 *)&val, 2);
                val = 0xffff; scd_write(SCD_YMIN, (u8 *)&val, 2);
                val = 0xffff; scd_write(SCD_YMAX, (u8 *)&val, 2);

                val = x_data_min_cal; val = byte_swap16(val); scd_write(SCD_XMIN, (u8 *)&val, 2);
                val = x_data_max_cal; val = byte_swap16(val); scd_write(SCD_XMAX, (u8 *)&val, 2);
                val = y_data_min_cal; val = byte_swap16(val); scd_write(SCD_YMIN, (u8 *)&val, 2);
                val = y_data_max_cal; val = byte_swap16(val); scd_write(SCD_YMAX, (u8 *)&val, 2);

                printf ("\nCalibration complete, remember to `scd save`\n\n");
                printf ("x_min  x_max  y_min  y_max\n");
                printf ("-----  -----  -----  -----\n");
                printf (" %4d   %4d   %4d   %4d\n", x_data_min_cal, x_data_max_cal, y_data_min_cal, y_data_max_cal);

        } else {
                printf ("\nx_avg  y_avg  d_avg  x_min  x_max  y_min  y_max  d_min  d_max\n");
                printf (  "-----  -----  -----  -----  -----  -----  -----  -----  -----\n");
                printf (" %4d   %4d   %4d   %4d   %4d   %4d   %4d   %4d   %4d\n",
                        x_avg, y_avg, d_avg, info->x_data_min, info->x_data_max, info->y_data_min, info->y_data_max, d_min, d_max);
        }

        if (compass_exit()) {
                puts ("## error accessing digital compass ##\n");
                return 1;
        }
        printf ("\n");

        return(0);
}

#define NUM_TO_AVG 8
int get_compass_heading (void) {
        scd_cmps_cal_t cmps;
        struct compass_info_t compass_info;
        struct compass_info_t *info = &compass_info;

        if (scd_read(SCD_XMIN, (uchar *)&cmps, sizeof(cmps)) == 0) {
             // check to see if calibration data is programmed in scd
                if (((cmps.x_data_min == 0xffff) && (cmps.x_data_max == 0xffff) && (cmps.y_data_min == 0xffff) && (cmps.y_data_max == 0xffff)) ||
                    ((cmps.x_data_min == 0x0000) && (cmps.x_data_max == 0x0000) && (cmps.y_data_min == 0x0000) && (cmps.y_data_max == 0x0000)))
                        return -1;
             // compass calibration data from scd
                info->x_data_min_cal = (short)byte_swap16(cmps.x_data_min);
                info->x_data_max_cal = (short)byte_swap16(cmps.x_data_max);
                info->y_data_min_cal = (short)byte_swap16(cmps.y_data_min);
                info->y_data_max_cal = (short)byte_swap16(cmps.y_data_max);
             // calibration data check
                if ((info->x_data_min_cal == info->x_data_max_cal) || (info->y_data_min_cal == info->y_data_max_cal))
                        return -2;
             // initialize compass
                if (compass_init(info))
                        return -1;
             // disable ethernet phy since it can cause heading measurement to vary by several degrees
                if (set_miiphy_state(0))
                        return -1;
             // get compass heading
                get_compass_data(info, 8);
             // enable ethernet phy
                if (set_miiphy_state(1))
                        return -1;
             // put compass in idle mode
                if (compass_exit())
                        return -1;
                return info->degrees;
        }
        return -1;
}

U_BOOT_CMD(
        compass,   2,     0,      do_compass,
        "compass calibration or heading in degrees",
        "[calibrate]\n    - compass calibration or heading"
);
