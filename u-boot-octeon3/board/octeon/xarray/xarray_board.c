/*
 * (C) Copyright 2013 Cavium, Inc. <support@cavium.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <command.h>
#include <asm/mipsregs.h>
#include <asm/arch/octeon_boot.h>
#include <asm/arch/octeon_board_common.h>
#include <asm/arch/octeon_fdt.h>
#include <asm/arch/lib_octeon.h>
#include <asm/gpio.h>
#include <asm/arch/cvmx-pcie.h>
#include <asm/arch/cvmx-qlm.h>
#include <asm/arch/cvmx-mdio.h>
#include <i2c.h>
#include <rtc.h>
#include <otis.h>
#include <radio_info.h>
#include <watchdog.h>

DECLARE_GLOBAL_DATA_PTR;

void reset_button_init (void);
int reset_button_active (void);
void check_reset_button(void);
int get_reset_status (uchar *stat);
void set_reset_status (void);
void init_reset_status (void);
void display_reset_status (void);
int avr_present (void);
int eeprom_present (void);
int compass_present (void);
int dtt_present (void);
int aquantia_phy_present (void);
void reset_all_radios (void);
void reset_bluetooth (void);
void ar9890_init (void);
void ar8033_phy_fixes (void);
void xir_miiphy_init (void);
void fan_ctrl_check(void);
int xir_miiphy_read (unsigned char addr, unsigned char reg, unsigned short * value);
int xir_miiphy_write (unsigned char addr, unsigned char reg, unsigned short value);
int xir_miiphy_read45(unsigned char addr, unsigned char dev, unsigned short reg, unsigned short * value);
int xir_miiphy_write45(unsigned char addr, unsigned char dev, unsigned short reg, unsigned short value);

extern uint pcie_port_with_radios;
extern uint radio_pcie_bar0_array[20];
extern unsigned short radio_pcie_device_id_array[20];
extern int led_spin_state;

extern void radio_leds_start(int num_radios);
extern void radio_set_leds(int mask);
extern void timer_hook(int timestamp);
extern int do_reset(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]);
extern int scd_read (unsigned long offset, uchar *buf, unsigned long len);
extern int scd_write (unsigned long offset, uchar *buf, unsigned long len);
extern uchar scd_reg_read (unsigned long reg);
extern void scd_reg_write (unsigned long reg, uchar val);
extern void bitswap(ulong, int, int);
extern uint16_t byte_swap16(uint16_t data);

#define PLX861X_SERDES_DRIVE_LEVEL0_ADDR 0xb98
#define PLX861X_SERDES_DRIVE_LEVEL1_ADDR 0xb9c
#define PLX861X_SERDES_DRIVE_LEVEL2_ADDR 0xba0
#define PLX861X_SERDES_DRIVE_LEVEL3_ADDR 0xba4
#define PLX861X_SERDES_DRIVE_LEVEL_DATA  0x0b0b0b0b

#define PLX861X_PC_EMPHASIS_LEVEL0_ADDR  0xba8
#define PLX861X_PC_EMPHASIS_LEVEL1_ADDR  0xbac
#define PLX861X_PC_EMPHASIS_LEVEL2_ADDR  0xbb0
#define PLX861X_PC_EMPHASIS_LEVEL3_ADDR  0xbb4
#define PLX861X_PC_EMPHASIS_LEVEL_DATA   0x05050505

void board_net_preinit(void)
{
	mdelay(120);
}
/**
 * Modify the device tree to remove all unused interface types.
 */
int board_fixup_fdt(void)
{
	const char *fdt_key;

	if (gpio_get_value(3))
		fdt_key = "2,sata";
	else
		fdt_key = "2,pcie";
	octeon_fdt_patch(working_fdt, fdt_key, NULL);


	if (gpio_get_value(1) == 0)
		fdt_key = "0,qsgmii";
	else
		fdt_key = "0,xaui";

	octeon_fdt_patch(working_fdt, fdt_key, NULL);

	return 0;
}

#define FLASH_RoundUP(_Dividend, _Divisor) (((_Dividend)+(_Divisor))/(_Divisor))
void octeon_boot_bus_board_init(void)
{
        cvmx_mio_boot_reg_cfgx_t __attribute__((unused)) reg_cfg;
	cvmx_mio_boot_reg_timx_t __attribute__((unused)) reg_tim;
	uint64_t clock_mhz, clock_period;

        /* set initial global data values */
	gd->arch.flash_base_address = CONFIG_SYS_FLASH_BASE;
	gd->arch.flash_size = CONFIG_SYS_FLASH_SIZE;

        /* Setup boot flash access window */
        reg_cfg.u64 = cvmx_read_csr(CVMX_MIO_BOOT_REG_CFG0);
	debug("CVMX_MIO_BOOT_REG_CFG0 original: 0x%llx\n", reg_cfg.u64);
        reg_cfg.s.size = ((CONFIG_SYS_FLASH_SIZE + 0x400000) >> 16) - 1;
        reg_cfg.s.base = (CONFIG_SYS_FLASH_BASE >> 16) & 0x1fff;
	debug("writing CVMX_MIO_BOOT_REG_CFG0: 0x%llx\n", reg_cfg.u64);
        cvmx_write_csr(CVMX_MIO_BOOT_REG_CFG0, reg_cfg.u64);

        /* Setup control register access on the boot bus byte offset:  0 = green LEDs, 2 = orange LEDs, 4 = radio resets */
        reg_cfg.u64     = 0;
        reg_cfg.s.en    = 1;
        reg_cfg.s.width = 1;
        reg_cfg.s.base  = (CONFIG_XIRRUS_CONTROL_BASE >> 16) & 0x1fff;
        cvmx_write_csr(CVMX_MIO_BOOT_REG_CFGX(CONFIG_XIRRUS_CONTROL_CHIP_SEL), reg_cfg.u64);
        *((u16 *) (CONFIG_XIRRUS_CONTROL_BASE + CONFIG_XIRRUS_CONTROL_GRN_LED_OFFSET  )) = 0xffff;
        *((u16 *) (CONFIG_XIRRUS_CONTROL_BASE + CONFIG_XIRRUS_CONTROL_ORG_LED_OFFSET  )) = 0xffff;

        /* Setup fpga programming window on the boot bus */
        reg_cfg.u64     = 0;
        reg_cfg.s.en    = 1;
        reg_cfg.s.width = 1;
        reg_cfg.s.size  = (CONFIG_AVALON_WINDOW_SIZE >> 16) - 1;
        reg_cfg.s.base  = (CONFIG_AVALON_BASE >> 16) & 0x1fff;
        cvmx_write_csr(CVMX_MIO_BOOT_REG_CFGX(CONFIG_AVALON_CHIP_SEL), reg_cfg.u64);

        /* Setup boot flash timing */
	clock_mhz = cvmx_clock_get_rate(CVMX_CLOCK_SCLK)/1000000;
	clock_period = 1000000 / clock_mhz; /* clk period (psecs) */
	debug("%s: clock rate %llu MHz\n", __func__, clock_mhz);

	reg_tim.u64 = cvmx_read_csr(CVMX_MIO_BOOT_REG_TIM0);
	debug("MIO_BOOT_REG_TIM0 original: 0x%llx\n", reg_tim.u64);

#ifdef CONFIG_OCTEON_FLASH_PAGE_MODE
	reg_tim.s.pagem = 1;
#else
	reg_tim.s.pagem = 0;
#endif
	/* ALE timing is preserved */
	reg_tim.s.pause  = 0;
	reg_tim.s.wait   = 0x3f;
        if (OCTEON_IS_MODEL(OCTEON_CN50XX))
            reg_tim.s.ale = FLASH_RoundUP((FLASH_RoundUP(100000ULL, clock_period) - 1), 4);  //XR500 uses CPLD to latch data w/ 20ns clock
        else
            reg_tim.s.ale = FLASH_RoundUP((FLASH_RoundUP( 25000ULL, clock_period) - 1), 4);
	reg_tim.s.adr     = FLASH_RoundUP((FLASH_RoundUP( 25000ULL, clock_period) - 1), 4);
	reg_tim.s.ce      = FLASH_RoundUP((FLASH_RoundUP( 50000ULL, clock_period) - 1), 4);;
	reg_tim.s.oe      = FLASH_RoundUP((FLASH_RoundUP( 50000ULL, clock_period) - 1), 4);
	reg_tim.s.rd_hld  = FLASH_RoundUP((FLASH_RoundUP( 25000ULL, clock_period) - 1), 4);
	reg_tim.s.wr_hld  = FLASH_RoundUP((FLASH_RoundUP( 35000ULL, clock_period) - 1), 4);
	reg_tim.s.we      = FLASH_RoundUP((FLASH_RoundUP( 75000ULL, clock_period) - 1), 4);
	reg_tim.s.page    = FLASH_RoundUP((FLASH_RoundUP( 25000ULL, clock_period) - 1), 4);
	cvmx_write_csr(CVMX_MIO_BOOT_REG_TIM0, reg_tim.u64);

}

void aquantia_reset_start(void) {

    gpio_set_value(4,0);
    mdelay(120); // must be >=100ms
    gpio_set_value(4,1);
    mdelay(100); // wait for aquantia phy to be ready for mii45 access: >=40ms

}

void aquantia_reset(void) {
    int i;
    int val;
    int reset_done = 0;

    aquantia_reset_start();

    i = 0;
    do {
        val = cvmx_mdio_45_read(0, 0, 0x1e, 0xcc00);
        if (val < 0 || val == 0xffff) {
            printf("### Warning: Cannot read Aquantia PHY (%s) ###\n", __func__);
            break;
        }
        reset_done = val & 0x40;
        WATCHDOG_RESET();
        mdelay (100);
        i++;
    } while (!reset_done && i < 50);

    if (i == 50) {
        puts("### Warning: Aquantia PHY 'Reset Completed' Bit not Set ###\n");
    }
}

void aquantia_set_led_activity(int speed, int enable)
{
    int  addr;
    int  data;

    switch (speed) {
        case 2500:
            addr = 0xc430; //blue
            break;
        case 1000:
            addr = 0xc431; //orange
            break;
        default:
            addr = 0xc432; //green
            break;
    }

    data = cvmx_mdio_45_read(0, 0, 0x1e, addr);
    if (enable)
        data |= 0x000c;
    else
        data &= 0xfff3;
    cvmx_mdio_45_write(0, 0, 0x1e, addr, data);
}

int aquantia_get_link_speed(bool verbose)
{
    int i;
    int val;
    int con_state = -1;
    bool link_up = false;
    int link_up_wait = 50; //5 seconds
    char *s;

    if ((s = getenv("aquantia_wait")) != NULL) {
        link_up_wait = (int)simple_strtoul(s, NULL, 10) * 10;
    }
    if (verbose)
        printf("%s: link_up_wait=%0d\n", __func__, link_up_wait);

    if (link_up_wait < 10)
        link_up_wait = 10;
    else if (link_up_wait > 400)
        link_up_wait = 400;

    if (verbose)
        printf("%s: Waiting for link up...\n", __func__);

    i = 0;
    do {
        val = cvmx_mdio_45_read(0, 0, 1, 0xe800);
        link_up = !!(val & 1);
        WATCHDOG_RESET();
        mdelay (100);
        i++;
    } while (!link_up && i < link_up_wait);

    if (verbose) {
        if (link_up)
            printf("%s: link up after waiting %0d msec\n", __func__, i*100);
    }

    if (link_up) {
        /* If we're in autonegotiation or training, retry until it finishes */
        i = 0;
        do {
            val = cvmx_mdio_45_read(0, 0, 7, 0xc810);
            /* Get the connection state, poll for a while if the
             * state is in autonegotiation or calibrating
             */
            con_state = (val >> 9) & 0x1f;
            if (con_state != 2 && con_state != 3 && con_state != 0xa)
                break;
            WATCHDOG_RESET();
            mdelay (100);
            i++;
        } while (i < 50);

        if (verbose) {
            printf("%s: connection state: 0x%x\n", __func__, con_state);
            printf("%s: Autonegotiation standard status 1: 0x%x, link is %s\n",
                    __func__, val, link_up ? "up" : "down");
        }
    } else {
        if (verbose)
            printf("%s: link is down, enabling SGMII mode\n", __func__);
    }
    if (!link_up || con_state != 4) {
        if (verbose)
            printf("%s, assuming SGMII (1000Base-T)\n",
                    link_up ? "Ethernet connection state not connected" : "No Ethernet link detected");
        return 1000;
    }

    val = cvmx_mdio_45_read(0, 0, 7, 0xc800);

    if (verbose) {
        printf("%s: Autonegotiation Vendor Status 1: 0x%x\n", __func__, val);
        printf("%s: Connection rate: 0x%x\n", __func__, (val >> 1) & 7);
    }

    switch ((val >> 1) & 0x7) {
    case 0:
        if (verbose)
            printf("%s: Detected 10Base-T, SGMII mode. This speed is not supported. Defaulting to 100Base-TX/1000Base-T mode\n", __func__);
        return 10;
    case 1:
        if (verbose)
            printf("%s: Detected 100Base-TX, SGMII mode\n", __func__);
        return 100;
    case 2:
        if (verbose)
            printf("%s: Detected 1000Base-T, SGMII mode\n", __func__);
        return 1000;
    case 3:
        if (verbose)
            printf("%s: Detected 10GBase-T, RXAUI mode\n", __func__);
        return 10000;
    case 4:
        if (verbose)
            printf("%s: Detected 2.5G, SGMII mode\n", __func__);
        return 2500;
        break;
    case 5:
        if (verbose)
            printf("%s: Detected 5G, RXAUI mode\n", __func__);
        return 5000;
        break;
    default:
        printf("Unknown connection rate, Autonegotiation Vendor Status 1: 0x%x\n", val);
        return 1000;
    }
}

void aquantia_init(int *eth_phy_2_5G, bool aquantia_verbose)
{
    int i;
    int speed;
    u16 val;

    /* initialize leds */
    cvmx_mdio_45_write(0, 0, 0x1e, 0xc430, 0x400e); // 2.5G (blue)
    cvmx_mdio_45_write(0, 0, 0x1e, 0xc431, 0x004e); //   1G (orange)
    cvmx_mdio_45_write(0, 0, 0x1e, 0xc432, 0x002e); // 100M (green)

    /* Enable 2.5Gbps */
    val = cvmx_mdio_45_read(0, 0, 7, 0xc400);
    val |= (1 << 10);
    /* Enable 1Gbps */
    val |= (1 << 15);
    cvmx_mdio_45_write(0, 0, 7, 0xc400, val);
    /* Enable 100Mbps (10Mbps not supported)*/
    val = cvmx_mdio_45_read(0, 0, 7, 0x10);
    val |= (0xc << 5);
    cvmx_mdio_45_write(0, 0, 7, 0x10, val);

    /* Restart autonegotiation */
    val = cvmx_mdio_45_read(0, 0, 7, 0);
    val |= (1 << 9);
    cvmx_mdio_45_write(0, 0, 7, 0, val);

    if (aquantia_verbose) {
        val = cvmx_mdio_45_read(0, 0, 7, 0xc400);
        printf("%s: 7.c400=%x\n", __func__, val);
        val = cvmx_mdio_45_read(0, 0, 7, 0x0010);
        printf("%s: 7.0010=%x\n", __func__, val);
        printf("%s: Waiting for autonegotiation to restart\n", __func__);
    }

    mdelay (2); /* delay prior to reading 1e.c831 */
    i = 0;
    do {
        val = cvmx_mdio_45_read(0, 0, 0x1e, 0xc831);
        WATCHDOG_RESET();
        mdelay (100);
        i++;
    } while ((val & (1 << 15)) && i < 50);

    if (val & (1 << 15)) {
        if (aquantia_verbose) {
            printf("%s: Ethernet autonegotiation timed out.  Assuming SGMII (1000Base-T)\n", __func__);
        }
        speed = 1000;
    } else {
        speed = aquantia_get_link_speed(aquantia_verbose);
    }

    /* Turn off blinking for tx/rx activity for all LEDs */
    aquantia_set_led_activity( 100, 0);
    aquantia_set_led_activity(1000, 0);
    aquantia_set_led_activity(2500, 0);
    /* Turn on blinking for tx/rx activity for specific LED */
    aquantia_set_led_activity(speed, 1);

    /* Don't disable the speeds we are not currently using.  If either end tries to renegotiate
     * the link, we want it to settle on a new value so we can detect this change in AOS.
     */
    if (speed == 2500) {
        /* Disable 1Gbps */
        //val = cvmx_mdio_45_read(0, 0, 7, 0xc400);
        //val &= ~(1 << 15);
        //cvmx_mdio_45_write(0, 0, 7, 0xc400, val);

        /* Disable 10 and 100Mbps */
        //val = cvmx_mdio_45_read(0, 0, 7, 0x10);
        //val &= ~(0xf << 5);
        //cvmx_mdio_45_write(0, 0, 7, 0x10, val);
        *eth_phy_2_5G = 1;
    }
    //else { //speed <= 1000
        /* Disable 2.5 */
        //val = cvmx_mdio_45_read(0, 0, 7, 0xc400);
        //val &= ~(1 << 10);
        //cvmx_mdio_45_write(0, 0, 7, 0xc400, val);
    //}

    if (aquantia_verbose) {
        val = cvmx_mdio_45_read(0, 0, 7, 0xc400);
        printf("%s: 7.c400=%x\n", __func__, val);
        val = cvmx_mdio_45_read(0, 0, 7, 0x0010);
        printf("%s: 7.0010=%x\n", __func__, val);
    }
}

static void gpio_init(void)
{
    /* initialize gpio pins */

    if ((gd->arch.board_desc.rev_major == BD_XR500 ) ||
        (gd->arch.board_desc.rev_major == BD_XR1000)) {
        gpio_direction_output(4, 1);  //rf0 reset
        gpio_direction_output(5, 1);  //rf1 reset
        gpio_direction_output(6, 1);  //rf0 green led
        gpio_direction_output(7, 1);  //rf1 green led
        gpio_direction_output(8, 1);  //rf0 orange led
        gpio_direction_output(9, 1);  //rf1 orange led
    }
    else if ((gd->arch.board_desc.rev_major == BD_XR600  ) ||
             (gd->arch.board_desc.rev_major == BD_XR700  )) {
        gpio_direction_output(4, 1);  //rf0 reset
        gpio_direction_output(5, 1);  //rf1 reset
        gpio_direction_output(6, 1);  //rf0 green led
        gpio_direction_output(7, 1);  //rf1 green led
        gpio_direction_output(9, 1);  //rf0 orange led
        gpio_direction_output(10,1);  //rf1 orange led
        gpio_direction_output(14,0);  //fault
        gpio_direction_output(15,1);  //status
    }
    else if (gd->arch.board_desc.rev_major == BD_XD2_230) {
        gpio_direction_output(4, 1);  //rf0 reset
        gpio_direction_output(5, 1);  //rf1 reset
        gpio_direction_output(6, 1);  //rf0 green led
        gpio_direction_output(7, 1);  //rf1 green led
        gpio_direction_output(9, 1);  //rf0 orange led
        gpio_direction_output(10,1);  //rf1 orange led
        gpio_direction_output(14,0);  //fault
        gpio_direction_output(15,1);  //status
        gpio_direction_output(0, 0);  //bluetooth reset
        gpio_direction_output(13,0);  //bluetooth dev_wake
    }
    else if (gd->arch.board_desc.rev_major == BD_XD2_240) {
        gpio_direction_output(4, 1);  //rf0 reset
        gpio_direction_output(5, 1);  //rf1 reset
        gpio_direction_output(12,1);  //rf0 green led
        gpio_direction_output(13,1);  //rf1 green led
        gpio_direction_output(16,1);  //rf0 orange led
        gpio_direction_output(17,1);  //rf1 orange led
        gpio_direction_output(9, 0);  //fault
        gpio_direction_output(10,1);  //status
        gpio_direction_output(6, 0);  //bluetooth reset
        gpio_direction_output(14,0);  //bluetooth dev_wake
        gpio_direction_input (19);    //cost-reduced model indicator
    }
    else if (gd->arch.board_desc.rev_major == BD_XD3_230) {
        gpio_direction_output(4, 1);  //rf0 reset
        gpio_direction_output(5, 1);  //rf1 reset
        gpio_direction_output(7, 1);  //rf2 reset
        gpio_direction_output(12,1);  //rf0 green led
        gpio_direction_output(13,1);  //rf1 orange led
        gpio_direction_output(16,1);  //rf0 orange led
        gpio_direction_output(17,1);  //rf2 orange led
        gpio_direction_output(9, 0);  //fault
        gpio_direction_output(10,1);  //status
        gpio_direction_output(6, 0);  //bluetooth reset
        gpio_direction_output(14,0);  //bluetooth dev_wake
    }
    else if (gd->arch.board_desc.rev_major == BD_XD4_130) {
        gpio_direction_output(4, 1);  //rf0 reset
        gpio_direction_output(5, 1);  //rf1 reset
        gpio_direction_output(6, 1);  //rf2 reset
        gpio_direction_output(7, 1);  //rf3 reset
        gpio_direction_output(9 ,0);  //fault
        gpio_direction_output(10,1);  //status
        gpio_direction_output(12,1);  //rf0 green led
        gpio_direction_output(13,1);  //rf1 green led
        gpio_direction_output(14,1);  //rf2 green led
        gpio_direction_output(15,1);  //rf3 green led
        gpio_direction_output(16,1);  //rf0 orange led
        gpio_direction_output(17,1);  //rf1 orange led
        gpio_direction_output(18,1);  //rf2 orange led
        gpio_direction_output(19,1);  //rf3 orange led
    }
    else if (gd->arch.board_desc.rev_major == BD_XA4_240) {
        gpio_direction_output(4, 1);  //aquantia phy reset
        gpio_direction_output(9 ,0);  //fault
        gpio_direction_output(10,1);  //status
    }
    else if (gd->arch.board_desc.rev_major == BD_XD4_240) {
        gpio_direction_output(4, 1);  //aquantia phy reset
        gpio_direction_output(6, 0);  //bluetooth reset (hold in reset for now)
        gpio_direction_output(9 ,0);  //fault
        gpio_direction_output(10,1);  //status
        gpio_direction_output(14,0);  //bluetooth dev_wake
    }
    else if (gd->arch.board_desc.rev_major == BD_XR2100) {
        gpio_direction_output(14,0);  //fault
        gpio_direction_output(15,1);  //status
    }

    if ((gd->arch.board_desc.rev_major == BD_XR4000) ||
        (gd->arch.board_desc.rev_major == BD_XR6000)) {
        gpio_direction_output(8, 1); // XLX_PROGRAM  - deassert
        gpio_direction_output(9, 0); // XLX_PGM_CS_L - always assert since there's only 1 FPGA
        gpio_direction_output(10,0); // XLX_PGM_RDWR - always assert since we are not doing FPGA readback
    }

#ifdef CONFIG_FLASH_HW_WR_PROTECT    // bootflash write protect input is low to protect
    gpio_direction_output(11,0);  // protect bootflash
#else
    gpio_direction_output(11,1);  // don't protect bootflash
#endif
#ifdef CONFIG_EEPROM_HW_WR_PROTECT   // eeprom write protect input is high to protect
    if ((gd->arch.board_desc.rev_major != BD_XA4_240)  &&
        (gd->arch.board_desc.rev_major != BD_XD4_130)  &&
        (gd->arch.board_desc.rev_major != BD_XD4_240)) {
        gpio_direction_output(12,1);  // protect motherboard eeprom
    }
#else
    if ((gd->arch.board_desc.rev_major != BD_XA4_240)  &&
        (gd->arch.board_desc.rev_major != BD_XD4_130)  &&
        (gd->arch.board_desc.rev_major != BD_XD4_240)) {
        gpio_direction_output(12,0);  // don't protect motherboard eeprom
    }
#endif
}

int checkboard(void)
{
    int eth_phy_2_5G = 0;
    int dlm0_speed;
    bool aquantia_verbose = false;

    /* initialize gpio pins */
    gpio_init();

    if (OCTEON_IS_MODEL(OCTEON_CN70XX)) {
        if (gd->arch.board_desc.rev_major == BD_XA4_240  ||
            gd->arch.board_desc.rev_major == BD_XD4_240) {
            aquantia_reset();
            if (getenv("aquantia_verbose")) aquantia_verbose = true;
            aquantia_init(&eth_phy_2_5G, aquantia_verbose);
            dlm0_speed = eth_phy_2_5G ? 3125 : 2500;
            if (aquantia_verbose) printf("dlm0_speed=%0d\n", dlm0_speed);
            octeon_configure_qlm(0, dlm0_speed, CVMX_QLM_MODE_SGMII_DISABLED, 0, 0, 0, 0);
        }
        else
            octeon_configure_qlm(0, 2500, CVMX_QLM_MODE_SGMII_SGMII, 0, 0, 1, 0);
        if (gd->arch.board_desc.rev_major == BD_XD2_240) {
            octeon_configure_qlm(1, 5000, CVMX_QLM_MODE_PCIE_2X1, 1, 1, 1, 0);  // Gen 2 Speeds, w00t w00t!
            octeon_configure_qlm(2, 3125, CVMX_QLM_MODE_DISABLED, 0, 0, 1, 0);
        }
        else if (gd->arch.board_desc.rev_major == BD_XR700 ||
                 gd->arch.board_desc.rev_major == BD_XD2_230) {
            octeon_configure_qlm(1, 5000, CVMX_QLM_MODE_PCIE_2X1, 1, 0, 1, 0);  // Gen1 Speeds because XD2-230 errors at Gen2 speeds
            octeon_configure_qlm(2, 3125, CVMX_QLM_MODE_DISABLED, 0, 0, 1, 0);  // XR-600 probably would too if Atheros supported Gen2
        }
        else if (gd->arch.board_desc.rev_major == BD_XD3_230) {
            octeon_configure_qlm(1, 5000, CVMX_QLM_MODE_PCIE_1X1, 1, 1, 1, 0);  // Never built this model
            octeon_configure_qlm(2, 5000, CVMX_QLM_MODE_PCIE_2X1, 1, 1, 1, 0);
        }
        else {
            octeon_configure_qlm(1, 5000, CVMX_QLM_MODE_PCIE_1X2, 1, 1, 1, 0);
            octeon_configure_qlm(2, 3125, CVMX_QLM_MODE_DISABLED, 0, 0, 1, 0);
        }
    }
    else if (OCTEON_IS_MODEL(OCTEON_CN60XX) || OCTEON_IS_MODEL(OCTEON_CN61XX)) {
        int p = (gd->arch.board_desc.rev_major == BD_XR600);
        octeon_configure_qlm(0, 1250, 2, 0, 0, 0, 0);  //sgmii
        octeon_configure_qlm(1, 2500, p, 1, 0, 0, 0);  //pcie --> p = 0 = pcie 1x2; p = 1 = pcie 2X1
        octeon_configure_qlm(2, 2500, 0, 0, 1, 0, 0);  //disabled
    }

    if (aquantia_verbose) {
        printf("%s: DLM 0 rate: %d, DLM 1 rate: %d, DLM 2 rate: %d\n",
                __func__, cvmx_qlm_measure_clock(0), cvmx_qlm_measure_clock(1), cvmx_qlm_measure_clock(2));
    }

    if (OCTEON_IS_MODEL(OCTEON_CN50XX)) {
        cvmx_write_csr (CVMX_GMXX_INF_MODE (0), 0x3);  // enable port 0 in RGMII mode
    }

    if (OCTEON_IS_MODEL(OCTEON_CN66XX)) {
        // reset 10GE port
        *((u16 *) (CONFIG_XIRRUS_CONTROL_BASE + CONFIG_XIRRUS_CONTROL_10G_RESET_OFFSET)) = 0x0000;  /* assert 10GE phy reset */
        udelay(1200);
        *((u16 *) (CONFIG_XIRRUS_CONTROL_BASE + CONFIG_XIRRUS_CONTROL_10G_RESET_OFFSET)) = 0xffff;  /* deassert 10GE phy reset */

        // enable 10GE transmit
        gpio_direction_output(5,1);

        // invert 10GE PCS TX Output to PMD in Broadcom chip due to board routing
        unsigned short mii45_data;
        xir_miiphy_read45( 0x0, 0x1, 0xc808, &mii45_data);
        xir_miiphy_write45(0x0, 0x1, 0xc808, (mii45_data | 0x400));
    }

    // turn on the 5 Volt supply to the PA's on the radio boards - do 1 at a time to prevent a power surge
    if (gd->arch.board_desc.rev_major == BD_XR2100) {
        *((u16 *) (CONFIG_XIRRUS_CONTROL_BASE + CONFIG_XIRRUS_CONTROL_5V_SUPPLY_OFFSET)) = 0x1;
        udelay(10000);
        *((u16 *) (CONFIG_XIRRUS_CONTROL_BASE + CONFIG_XIRRUS_CONTROL_5V_SUPPLY_OFFSET)) = 0x5;
        udelay(10000);
        *((u16 *) (CONFIG_XIRRUS_CONTROL_BASE + CONFIG_XIRRUS_CONTROL_5V_SUPPLY_OFFSET)) = 0x15;
        udelay(10000);
        *((u16 *) (CONFIG_XIRRUS_CONTROL_BASE + CONFIG_XIRRUS_CONTROL_5V_SUPPLY_OFFSET)) = 0x55;
    }

    /* display the current reset status */
    display_reset_status();

    /* initialize the reset button */
    reset_button_init();

    /* Perform ar8033 ethernet phy fixes, if necessary */
    ar8033_phy_fixes();

    /* reset all of the radios */
    reset_all_radios();

    /* reset bluetooth */
    reset_bluetooth();

    return 0;
}

void determine_board_type(void)
{
        int plx_present;
        unsigned short mii_data;

        // NOTE: I've removed BD_XD3_230 from this logic and replaced it with BD_XD2_230.  If we resurrect the XD3-230, we'll have to figure
        //       out how to differentiate this board.  A couple of ideas are:  (1) lack of RTC on XD3-230, or (2) strapping of GPIO-19.
        // UPDATE 08/02/2017: Thanks Drew! Strapping gpio-19 is needed to distinguish XD2-240 original and XD2-240 cost reduced

        gd->board_type = gd->arch.board_desc.board_type = CVMX_BOARD_TYPE_XARRAY;
        gd->arch.board_desc.rev_minor = 0;
        gd->arch.board_desc.rev_major  = OCTEON_IS_MODEL(OCTEON_CN66XX) ? BD_XR6000 :
                                         OCTEON_IS_MODEL(OCTEON_CN63XX) ? BD_XR6000 :
                                         OCTEON_IS_MODEL(OCTEON_CN50XX) ? BD_XR500  :
                                         (cvmx_octeon_num_cores() == 4) ? (OCTEON_IS_MODEL(OCTEON_CN52XX) ? BD_XR4000 : BD_XR2100) :
                                         (cvmx_octeon_num_cores() == 2) ? (OCTEON_IS_MODEL(OCTEON_CN52XX) ? BD_XR2000 : BD_XR2100) : 1;

        plx_present = (i2c_probe(CONFIG_PLX0_I2C_ADDR) == 0);

        if (OCTEON_IS_MODEL(OCTEON_CN70XX)) {
            int cpu_clk_1ghz;
            cvmx_rst_boot_t rst_boot;
            rst_boot.u64 = cvmx_read_csr_node(0, CVMX_RST_BOOT);
            cpu_clk_1ghz = (rst_boot.s.c_mul == 20);
            // can't use plx here since it's in reset on the XD4
            gd->arch.board_desc.rev_major = aquantia_phy_present() ? (dtt_present() ? BD_XD4_240 : BD_XA4_240) : (dtt_present() ? BD_XD4_130 : BD_XR700);

            // Distinguish between CN7020-based XR-600 (aka BD_XR700), XD2-230, XD2-240 (dual core), and XD2-240 (quad core)
            if (gd->arch.board_desc.rev_major == BD_XR700) {
                if (cpu_clk_1ghz) {
                    gpio_direction_input(19);
                    gd->arch.board_desc.rev_major = (gpio_get_value(19)) ? BD_XD2_240 : BD_XD2_230;
                }
                else { // Clock is 800 MHz or 1.2 GHz
                    xir_miiphy_read(0x2, 0x0, &mii_data);
                    gd->arch.board_desc.rev_major = (mii_data == 0xffff) ? BD_XR700 : BD_XD2_240;
                }
            }
        }
        else if (!plx_present && (gd->arch.board_desc.rev_major != BD_XR500)) {
            gd->arch.board_desc.rev_major = OCTEON_IS_MODEL(OCTEON_CN52XX) ? BD_XR1000 : BD_XR600;
        }

        debug("rev_major = %d\n", gd->arch.board_desc.rev_major);

}

int early_board_init(void)
{
        int plx_present;
        int plx_reg_data;

        gd->avr_present = (i2c_probe(SYS_CONFIG_I2C_ADDR) == 0);
        gd->dtt_present = ((i2c_probe(CONFIG_SYS_DTT_LM75_ADDR) == 0) || (i2c_probe(CONFIG_SYS_DTT_TMP42X_ADDR) == 0));
        gd->eeprom_present = (i2c_probe(CONFIG_SYS_I2C_EEPROM_ADDR) == 0);
        gd->compass_present = (i2c_probe(CONFIG_SYS_COMPASS_I2C_ADDR) == 0);

	gd->arch.ddr_ref_hertz = 50000000;

        if (OCTEON_IS_MODEL(OCTEON_CN70XX)) {
            gd->mem_clk = XARRAY_CN70XX_DEF_DRAM_FREQ;
        }
        else if (OCTEON_IS_MODEL(OCTEON_CN6XXX)) {
            if (OCTEON_IS_MODEL(OCTEON_CN63XX) || OCTEON_IS_MODEL(OCTEON_CN66XX))
	        gd->mem_clk = XARRAY_CN63XX_DEF_DRAM_FREQ;
	    else
                gd->mem_clk = XARRAY_CN60XX_DEF_DRAM_FREQ;
	}
        else {
	    if (OCTEON_IS_MODEL(OCTEON_CN52XX))
                gd->mem_clk = XARRAY_CN52XX_DEF_DRAM_FREQ;
	    else
                gd->mem_clk = XARRAY_CN50XX_DEF_DRAM_FREQ;
        }

        xir_miiphy_init();

        /* determine which cpu board we are booting on */
        determine_board_type();

        plx_present = (i2c_probe(CONFIG_PLX0_I2C_ADDR) == 0);

        if (plx_present) {
           /* program PLX serdes drive level and post-cursor emphasis level */
            plx_reg_data = PLX861X_SERDES_DRIVE_LEVEL_DATA;
            i2c_write(CONFIG_PLX0_I2C_ADDR,  PLX861X_SERDES_DRIVE_LEVEL0_ADDR, 2, (uchar *)&plx_reg_data, 4);
            i2c_write(CONFIG_PLX0_I2C_ADDR,  PLX861X_SERDES_DRIVE_LEVEL1_ADDR, 2, (uchar *)&plx_reg_data, 4);
            i2c_write(CONFIG_PLX0_I2C_ADDR,  PLX861X_SERDES_DRIVE_LEVEL2_ADDR, 2, (uchar *)&plx_reg_data, 4);
            if (gd->arch.board_desc.rev_major == BD_XR6000) {
                i2c_write(CONFIG_PLX0_I2C_ADDR,  PLX861X_SERDES_DRIVE_LEVEL3_ADDR, 2, (uchar *)&plx_reg_data, 4);
                i2c_write(CONFIG_PLX1_I2C_ADDR,  PLX861X_SERDES_DRIVE_LEVEL0_ADDR, 2, (uchar *)&plx_reg_data, 4);
                i2c_write(CONFIG_PLX1_I2C_ADDR,  PLX861X_SERDES_DRIVE_LEVEL1_ADDR, 2, (uchar *)&plx_reg_data, 4);
                i2c_write(CONFIG_PLX1_I2C_ADDR,  PLX861X_SERDES_DRIVE_LEVEL2_ADDR, 2, (uchar *)&plx_reg_data, 4);
                i2c_write(CONFIG_PLX1_I2C_ADDR,  PLX861X_SERDES_DRIVE_LEVEL3_ADDR, 2, (uchar *)&plx_reg_data, 4);
            }
            plx_reg_data = PLX861X_PC_EMPHASIS_LEVEL_DATA;
            i2c_write(CONFIG_PLX0_I2C_ADDR,  PLX861X_PC_EMPHASIS_LEVEL0_ADDR, 2, (uchar *)&plx_reg_data, 4);
            i2c_write(CONFIG_PLX0_I2C_ADDR,  PLX861X_PC_EMPHASIS_LEVEL1_ADDR, 2, (uchar *)&plx_reg_data, 4);
            i2c_write(CONFIG_PLX0_I2C_ADDR,  PLX861X_PC_EMPHASIS_LEVEL2_ADDR, 2, (uchar *)&plx_reg_data, 4);
            if (gd->arch.board_desc.rev_major == BD_XR6000) {
                i2c_write(CONFIG_PLX0_I2C_ADDR,  PLX861X_PC_EMPHASIS_LEVEL3_ADDR, 2, (uchar *)&plx_reg_data, 4);
                i2c_write(CONFIG_PLX1_I2C_ADDR,  PLX861X_PC_EMPHASIS_LEVEL0_ADDR, 2, (uchar *)&plx_reg_data, 4);
                i2c_write(CONFIG_PLX1_I2C_ADDR,  PLX861X_PC_EMPHASIS_LEVEL1_ADDR, 2, (uchar *)&plx_reg_data, 4);
                i2c_write(CONFIG_PLX1_I2C_ADDR,  PLX861X_PC_EMPHASIS_LEVEL2_ADDR, 2, (uchar *)&plx_reg_data, 4);
                i2c_write(CONFIG_PLX1_I2C_ADDR,  PLX861X_PC_EMPHASIS_LEVEL3_ADDR, 2, (uchar *)&plx_reg_data, 4);
            }
        }

	return 0;
}

int late_board_init(void)
{
    int num_radios = 16;

    num_radios = (gd->arch.board_desc.rev_major == BD_XR6000 ) ? 16 :
                 (gd->arch.board_desc.rev_major == BD_XR4000 ) ?  8 :
                 (gd->arch.board_desc.rev_major == BD_XD4_130) ?  4 :
                 (gd->arch.board_desc.rev_major == BD_XD4_240) ?  4 :
                 (gd->arch.board_desc.rev_major == BD_XA4_240) ?  4 :
                 (gd->arch.board_desc.rev_major == BD_XR2000 ) ?  4 :
                 (gd->arch.board_desc.rev_major == BD_XR2100 ) ?  4 :
                 (gd->arch.board_desc.rev_major == BD_XD3_230) ?  3 :
                 (gd->arch.board_desc.rev_major == BD_XD2_230) ?  2 :
                 (gd->arch.board_desc.rev_major == BD_XD2_240) ?  2 :
                 (gd->arch.board_desc.rev_major == BD_XR500  ) ?  2 :
                 (gd->arch.board_desc.rev_major == BD_XR600  ) ?  2 :
                 (gd->arch.board_desc.rev_major == BD_XR700  ) ?  2 :
                 (gd->arch.board_desc.rev_major == BD_XR1000 ) ?  2 : 0;

    radio_leds_start(num_radios);

    put_bootmsg_label("Environment");
    initialize_environment();
    printf("Initialized\n");

    init_reset_status();

#if defined(CONFIG_RTC_DS1338)
    i2c_reg_write(CONFIG_SYS_RTC_DS1338_ADDR, 7, 0);
#endif

    return(1);
}

#if defined(CONFIG_PCI)
extern void init_octeon_pci  (void);
extern void init_octeon_pcie (void);

void pci_init_board (void)
{
    int plx_reg_data;

    /* The plx pci express bus ltssm appears to eventually get into a bad state if you repeatedly execute a
     * soft reset in u-boot.  After this occurs, the pci express bus will never come up between the cpu and
     * the plx unless you cycle the power.  This sounds like the issue described in the PEX 8608 Errata ...
     * "The PHY Polling State Machine Can Get Stuck Under Noisy Links".  I changed the target link speed to
     * 2.5 Gbps and that appears to fix the issue.
     */
    if (gd->arch.board_desc.rev_major == BD_XD4_130 ||
        gd->arch.board_desc.rev_major == BD_XD4_240 ||
        gd->arch.board_desc.rev_major == BD_XA4_240 ||
        gd->arch.board_desc.rev_major == BD_XR2100 ) {
        i2c_read (CONFIG_PLX0_I2C_ADDR, 0x98, 2, (uchar *)&plx_reg_data, 4);
        plx_reg_data &= ~0xf;
        plx_reg_data |=  0x1;  //set target link speed to 2.5 Gbps
        i2c_write(CONFIG_PLX0_I2C_ADDR, 0x98, 2, (uchar *)&plx_reg_data, 4);
    }

    if (gd->arch.board_desc.rev_major == BD_XR500)
        init_octeon_pci();
    else {
        /* bypass pcie receiver detection */
        if (gd->arch.board_desc.rev_major == BD_XD2_230 ||
            gd->arch.board_desc.rev_major == BD_XD2_240 ||
            gd->arch.board_desc.rev_major == BD_XD3_230 ||
            gd->arch.board_desc.rev_major == BD_XD4_130 ||
            gd->arch.board_desc.rev_major == BD_XD4_240 ||
            gd->arch.board_desc.rev_major == BD_XA4_240 ||
            gd->arch.board_desc.rev_major == BD_XR700  ) {
            cvmx_gserx_phyx_lane0_txdebug_t txdebug;

            txdebug.u64 = cvmx_read_csr(CVMX_GSERX_PHYX_LANE0_TXDEBUG(0,0));
            txdebug.s.detrx_always = 1;
            cvmx_write_csr(CVMX_GSERX_PHYX_LANE0_TXDEBUG(0,0),txdebug.u64);
            txdebug.u64 = cvmx_read_csr(CVMX_GSERX_PHYX_LANE0_TXDEBUG(1,0));
            txdebug.s.detrx_always = 1;
            cvmx_write_csr(CVMX_GSERX_PHYX_LANE0_TXDEBUG(1,0),txdebug.u64);
            txdebug.u64 = cvmx_read_csr(CVMX_GSERX_PHYX_LANE0_TXDEBUG(2,0));
            txdebug.s.detrx_always = 1;
            cvmx_write_csr(CVMX_GSERX_PHYX_LANE0_TXDEBUG(2,0),txdebug.u64);

            txdebug.u64 = cvmx_read_csr(CVMX_GSERX_PHYX_LANE1_TXDEBUG(0,0));
            txdebug.s.detrx_always = 1;
            cvmx_write_csr(CVMX_GSERX_PHYX_LANE1_TXDEBUG(0,0),txdebug.u64);
            txdebug.u64 = cvmx_read_csr(CVMX_GSERX_PHYX_LANE1_TXDEBUG(1,0));
            txdebug.s.detrx_always = 1;
            cvmx_write_csr(CVMX_GSERX_PHYX_LANE1_TXDEBUG(1,0),txdebug.u64);
            txdebug.u64 = cvmx_read_csr(CVMX_GSERX_PHYX_LANE1_TXDEBUG(2,0));
            txdebug.s.detrx_always = 1;
            cvmx_write_csr(CVMX_GSERX_PHYX_LANE1_TXDEBUG(2,0),txdebug.u64);
        }
        init_octeon_pcie();
    }
}
#endif

int avr_present (void)
{
    return (gd->avr_present);
}

int eeprom_present (void)
{
    return (gd->eeprom_present);
}

int compass_present (void)
{
    return (gd->compass_present);
}

int dtt_present (void)
{
    return (gd->dtt_present);
}

int aquantia_phy_present (void)
{
    int val;

    /* This is only intended to be used before the Aquantia PHY is initialized */
    cvmx_write_csr(CVMX_SMIX_EN(0), 1);
    gpio_direction_output(4, 1);
    aquantia_reset_start();
    val = cvmx_mdio_45_read(0, 0, 0x1e, 0x0002);
    gpio_direction_input(4);
    return ((val & 0x0000ffff) == 0x03A1);
}

void reset_all_radios (void) {

  if ((gd->arch.board_desc.rev_major == BD_XD2_230) ||
      (gd->arch.board_desc.rev_major == BD_XD2_240) ||
      (gd->arch.board_desc.rev_major == BD_XR500  ) ||
      (gd->arch.board_desc.rev_major == BD_XR600  ) ||
      (gd->arch.board_desc.rev_major == BD_XR700  )) {
       gpio_set_value(4,1);
       gpio_set_value(5,1);
       udelay (10);
       gpio_set_value(4,0);  // assert reset for rf0
       gpio_set_value(5,0);  // assert reset for rf1
       udelay (1200);
       gpio_set_value(4,1);  // deassert reset for rf0
       gpio_set_value(5,1);  // deassert reset for rf1
   }
   else if (gd->arch.board_desc.rev_major == BD_XD3_230) {
       gpio_set_value(4,1);
       gpio_set_value(5,1);
       gpio_set_value(7,1);
       udelay (10);
       gpio_set_value(4,0);  // assert reset for rf0
       gpio_set_value(5,0);  // assert reset for rf1
       gpio_set_value(7,0);  // assert reset for rf2
       udelay (1200);
       gpio_set_value(4,1);  // deassert reset for rf0
       gpio_set_value(5,1);  // deassert reset for rf1
       gpio_set_value(7,1);  // deassert reset for rf2
   }
   else if (gd->arch.board_desc.rev_major == BD_XD4_130) {
       gpio_set_value(4,1);
       gpio_set_value(5,1);
       gpio_set_value(6,1);
       gpio_set_value(7,1);
       udelay (10);
       gpio_set_value(4,0);  // assert reset for rf0
       gpio_set_value(5,0);  // assert reset for rf1
       gpio_set_value(6,0);  // assert reset for rf2
       gpio_set_value(7,0);  // assert reset for rf3
       udelay (1200);
       gpio_set_value(4,1);  // deassert reset for rf0
       gpio_set_value(5,1);  // deassert reset for rf1
       gpio_set_value(6,1);  // deassert reset for rf2
       gpio_set_value(7,1);  // deassert reset for rf3
   }
   else if (gd->arch.board_desc.rev_major == BD_XR1000) {
       udelay (1200);
   }
   else {
       *((u16 *) (CONFIG_XIRRUS_CONTROL_BASE + CONFIG_XIRRUS_CONTROL_INT_RESET_OFFSET)) = 0x0000;  // assert radio resets
       udelay (1200);
       *((u16 *) (CONFIG_XIRRUS_CONTROL_BASE + CONFIG_XIRRUS_CONTROL_INT_RESET_OFFSET)) = 0xffff;  // deassert radio resets
   }

}

void reset_bluetooth(void) {

   if ((gd->arch.board_desc.rev_major == BD_XD2_240 ) ||
       (gd->arch.board_desc.rev_major == BD_XD3_230 ) ||
       (gd->arch.board_desc.rev_major == BD_XD4_240 )) {
       gpio_set_value(6,0);
       udelay (1000);
       gpio_set_value(6,1);
   }
   else if (gd->arch.board_desc.rev_major == BD_XD2_230 ) {
       gpio_set_value(0,0);
       udelay (1000);
       gpio_set_value(0,1);
   }

}

void ar9890_init (void) {

   int i;
   int pcie_port;
   uint64_t pcie_addr_base;

 /*
   if the radio is a AR9890, the LF timer 0 interrupt enable bit must get cleared within ~30 seconds or else all
   register accesses will return 0xdeadbeef.  Also, initialize i2c access to the eeprom here.
 */
   for (i = 0; i < 16; i++) {
       if (radio_pcie_device_id_array[i] == AR9890_DEVICE_ID_DEFAULT) {
           pcie_port = (gd->arch.board_desc.rev_major == BD_XR600 ||
                        gd->arch.board_desc.rev_major == BD_XR700 ||
                        gd->arch.board_desc.rev_major == BD_XR1000) ? i : pcie_port_with_radios;
           pcie_addr_base = (cvmx_pcie_get_mem_base_address(pcie_port)  | (1ull << 63)) + radio_pcie_bar0_array[i];

        // disable timer interrupt so register accesses don't produce 0xdeadbeef after ~30 seconds
           cvmx_write64_uint32((pcie_addr_base + AR9890_SOC_LF_TIMER_CONTROL0), 0x0);

        // set up gpio for i2c access to the eeprom
           cvmx_write64_uint32((pcie_addr_base + AR9890_WLAN_GPIO_PIN2), 0x08180000);  // gpio 2 - sda
           cvmx_write64_uint32((pcie_addr_base + AR9890_WLAN_GPIO_PIN4), 0x48200000);  // gpio 4 scl

        // enable write protect for the radio eeprom
           cvmx_write64_uint32((pcie_addr_base + AR9890_WLAN_GPIO_OUT_W1TS), 0x02000000);  // gpio 1

        // configure gpio 1,2,4 as outputs (wp, sda, scl)
           cvmx_write64_uint32((pcie_addr_base + AR9890_WLAN_GPIO_ENABLE), 0x16000000);

        // configure i2c clock to ~300kHz
           cvmx_write64_uint32((pcie_addr_base + AR9890_SI_CONFIG), 0xb6000500);

       }
   }
}

void ar8033_phy_fixes (void) {
   unsigned short mii_data;
   int i;
   int num_phys;
   int phy_addr;

   if ((gd->arch.board_desc.rev_major == BD_XR500  ) ||
       (gd->arch.board_desc.rev_major == BD_XA4_240) ||
       (gd->arch.board_desc.rev_major == BD_XD4_240)) {

       // disable the rgmii rx clock delay (~2ns) so the clock and data are aligned going into the cn5020.
       // this was empirically shown to be necessary for the interface timing to work at 1Gbps.
       phy_addr = (gd->arch.board_desc.rev_major == BD_XR500) ? 0x01 : 0x02;
       mii_data = 0x0;
       xir_miiphy_write(phy_addr, 0x1d,  mii_data);  // debug port register -> set to access analog test control register
       xir_miiphy_read( phy_addr, 0x1e, &mii_data);  // debug2 port register -> read analog test control
       xir_miiphy_write(phy_addr, 0x1e, (mii_data & ~0x8000));  // turn off rx clock delay to fix setup/hold to cn50xx
   }

   if ((gd->arch.board_desc.rev_major == BD_XD2_230) ||
       (gd->arch.board_desc.rev_major == BD_XD2_240) ||
       (gd->arch.board_desc.rev_major == BD_XD3_230) ||
       (gd->arch.board_desc.rev_major == BD_XD4_130) ||
       (gd->arch.board_desc.rev_major == BD_XD4_240) ||
       (gd->arch.board_desc.rev_major == BD_XA4_240) ||
       (gd->arch.board_desc.rev_major == BD_XR500)   ||
       (gd->arch.board_desc.rev_major == BD_XR600)   ||
       (gd->arch.board_desc.rev_major == BD_XR700)   ||
       (gd->arch.board_desc.rev_major == BD_XR2100))  {

       num_phys = ((gd->arch.board_desc.rev_major == BD_XR500  ) ||
                   (gd->arch.board_desc.rev_major == BD_XA4_240) ||
                   (gd->arch.board_desc.rev_major == BD_XD4_240)) ? 1 : 2;

       for (i=0; i<num_phys; i++) {

           if (gd->arch.board_desc.rev_major == BD_XA4_240 ||
               gd->arch.board_desc.rev_major == BD_XD4_240)
               phy_addr = 0x02;
           else if ((gd->arch.board_desc.rev_major == BD_XD2_230) ||
                    (gd->arch.board_desc.rev_major == BD_XD2_240) ||
                    (gd->arch.board_desc.rev_major == BD_XD3_230) ||
                    (gd->arch.board_desc.rev_major == BD_XD4_130))
               phy_addr = (i==0) ? 0x01 : 0x02;
           else
               phy_addr = (i==0) ? 0x01 : 0x82;

           // disable EEE and smartEEE modes to address AR8033 errata 2.4
           mii_data = 0x3;
           xir_miiphy_write(phy_addr, 0x0d,  mii_data);  // MMD3 select
           mii_data = 0x805d;
           xir_miiphy_write(phy_addr, 0x0e,  mii_data);  // MMD3 register offset
           mii_data = 0x4003;
           xir_miiphy_write(phy_addr, 0x0d,  mii_data);
           xir_miiphy_read (phy_addr, 0x0e, &mii_data);
           xir_miiphy_write(phy_addr, 0x0e, (mii_data & ~0x0100));  // disable smartEEE mode

           mii_data = 0x7;
           xir_miiphy_write(phy_addr, 0x0d,  mii_data);  // MMD7 select
           mii_data = 0x3c;
           xir_miiphy_write(phy_addr, 0x0e,  mii_data);  // MMD7 register offset
           mii_data = 0x4007;
           xir_miiphy_write(phy_addr, 0x0d,  mii_data);
           xir_miiphy_read (phy_addr, 0x0e, &mii_data);
           xir_miiphy_write(phy_addr, 0x0e, (mii_data & ~0x0006));  // disable EEE mode

           xir_miiphy_read (phy_addr, 0x00, &mii_data);  // read MII register 0
           xir_miiphy_write(phy_addr, 0x00, (mii_data | 0x8000));   // write soft reset bit in MII register 0

           /* NOTE:  commenting out this code because it was causing tftp failures --> see PR 13107
           // implement AR8033 errata 2.4 (rev 0.1 - 11/14/2011)
           mii_data = 0x3d;
           xir_miiphy_write(phy_addr, 0x1d,  mii_data);  // debug port register -> set to access debug register 0x3d
           xir_miiphy_read (phy_addr, 0x1e, &mii_data);  // debug2 port register -> read debug register 0x3d
           xir_miiphy_write(phy_addr, 0x1e, (mii_data & ~0x0080));  // clear bit 7
           xir_miiphy_read (phy_addr, 0x00, &mii_data);  // read register 0
           xir_miiphy_write(phy_addr, 0x00, (mii_data | 0x8000));   // write soft reset bit in register 0
           */
       }
   }

   if (gd->arch.board_desc.rev_major == BD_XD4_130 ||
       gd->arch.board_desc.rev_major == BD_XD4_240 ||
       gd->arch.board_desc.rev_major == BD_XA4_240 ||
       gd->arch.board_desc.rev_major == BD_XR2100 ) {
       xir_miiphy_read( 0x82, 0x19, &mii_data);
       xir_miiphy_write(0x82, 0x19, (mii_data & ~0x1000));  // led ON when ethernet is idle
   }

}

/************************************************************************
 *  Reset status board specific routines                                *
 ************************************************************************/

/*
   Bits [1:0] in register 0x18 of the ethernet phy (AR8033) are used to determine the reset status on
   the XR500, XR600, and XR2100.  Both bits will clear on a power-on reset.  U-boot will always set
   bit 0 at the end of the boot and will additionally set bit 1 prior to a soft reset.  Based on these
   2 bits, u-boot will update the flash (SCD_STAT_LO) with the latest reset reason.  AOS will simply
   read this byte in the flash to get the reset reason.  AOS will also write bit 1 prior to a requested
   reboot.
*/

int get_reset_status (uchar *stat) {
    unsigned short mii_data;

    if ((gd->arch.board_desc.rev_major == BD_XD2_230) ||
        (gd->arch.board_desc.rev_major == BD_XD2_240) ||
        (gd->arch.board_desc.rev_major == BD_XD3_230) ||
        (gd->arch.board_desc.rev_major == BD_XD4_130) ||
        (gd->arch.board_desc.rev_major == BD_XD4_240) ||
        (gd->arch.board_desc.rev_major == BD_XA4_240) ||
        (gd->arch.board_desc.rev_major == BD_XR500  ) ||
        (gd->arch.board_desc.rev_major == BD_XR600  ) ||
        (gd->arch.board_desc.rev_major == BD_XR700  ) ||
        (gd->arch.board_desc.rev_major == BD_XR2100 )) {

        unsigned char phy_addr = (gd->arch.board_desc.rev_major == BD_XA4_240 || gd->arch.board_desc.rev_major == BD_XD4_240) ? 0x02 : 0x01;

        if (xir_miiphy_read (phy_addr, 0x18, &mii_data) == 0) {
            switch (mii_data & RESET_CODE_MASK) {
                case RESET_CODE_PON : *stat = SRL_PON_RESET; break;
                case RESET_CODE_MWD : *stat = SRL_MWD_RESET; break;
                case RESET_CODE_EXT : *stat = SRL_EXT_RESET; break;
                default             : *stat = 0;
            }
            return (mii_data & RESET_CODE_MASK);
        }
        else
            return -1;
    }
    return -1;
}

void set_reset_status (void) {
    unsigned short mii_data;

    if ((gd->arch.board_desc.rev_major == BD_XD2_230) ||
        (gd->arch.board_desc.rev_major == BD_XD2_240) ||
        (gd->arch.board_desc.rev_major == BD_XD3_230) ||
        (gd->arch.board_desc.rev_major == BD_XD4_130) ||
        (gd->arch.board_desc.rev_major == BD_XD4_240) ||
        (gd->arch.board_desc.rev_major == BD_XA4_240) ||
        (gd->arch.board_desc.rev_major == BD_XR500  ) ||
        (gd->arch.board_desc.rev_major == BD_XR600  ) ||
        (gd->arch.board_desc.rev_major == BD_XR700  ) ||
        (gd->arch.board_desc.rev_major == BD_XR2100 )) {

        unsigned char phy_addr = (gd->arch.board_desc.rev_major == BD_XA4_240 || gd->arch.board_desc.rev_major == BD_XD4_240) ? 0x02 : 0x01;

        if (xir_miiphy_read  (phy_addr, 0x18, &mii_data) == 0) {
            xir_miiphy_write (phy_addr, 0x18, mii_data | 0x3);
        }
    }
}

void init_reset_status (void) {
    unsigned short mii_data;

    if ((gd->arch.board_desc.rev_major == BD_XD2_230) ||
        (gd->arch.board_desc.rev_major == BD_XD2_240) ||
        (gd->arch.board_desc.rev_major == BD_XD3_230) ||
        (gd->arch.board_desc.rev_major == BD_XD4_130) ||
        (gd->arch.board_desc.rev_major == BD_XD4_240) ||
        (gd->arch.board_desc.rev_major == BD_XA4_240) ||
        (gd->arch.board_desc.rev_major == BD_XR500  ) ||
        (gd->arch.board_desc.rev_major == BD_XR600  ) ||
        (gd->arch.board_desc.rev_major == BD_XR700  ) ||
        (gd->arch.board_desc.rev_major == BD_XR2100 )) {

        unsigned char phy_addr = (gd->arch.board_desc.rev_major == BD_XA4_240 || gd->arch.board_desc.rev_major == BD_XD4_240) ? 0x02 : 0x01;

        if (xir_miiphy_read  (phy_addr, 0x18, &mii_data) == 0) {
            xir_miiphy_write (phy_addr, 0x18, (mii_data & ~0x3) | 0x1);
        }
    }
}

void display_reset_status (void) {
    uchar stat;
    int   print_reset_info = 0;

    if (avr_present()) {
        if (scd_read(SCD_STAT_LO, &stat, 1) == 0) {
            if (stat) print_reset_info = 1;
        }
    }
    else {
        if ((gd->reset_code = get_reset_status(&stat)) != -1) {
            if (stat) print_reset_info = 1;
        }
        else {
            gd->reset_code = RESET_CODE_UNUSED;
        }
    }

    if (print_reset_info) {
        put_bootmsg_label("Reset");
             if (stat & SRL_EEP_FAIL ) printf("SCD EEPROM check failed""\n");
        else if (stat & SRL_PON_RESET) printf("Power on"               "\n");
        else if (stat & SRL_BRN_RESET) printf("Brown out"              "\n");
        else if (stat & SRL_AWD_RESET) printf("SCD Watchdog"           "\n");
        else if (stat & SRL_MWD_RESET) printf("Processor Watchdog"     "\n");
        else if (stat & SRL_EXT_RESET) printf("Reset requested"        "\n");
        else if (stat & SRL_IRQ_REQ  ) printf("Watchdog IRQ requested" "\n");
    }
}

/************************************************************************
 *  Reset button board specific routines                                *
 ************************************************************************/

void reset_button_init (void) {

    if (reset_button_active()) {
        gd->reset_button_timeout = 1;
        gd->reset_button_disable = (getenv("reset_button") != NULL);
    }
    else
        gd->reset_button_timeout = 0;
}

int reset_button_active (void) {

    return (((gd->arch.board_desc.rev_major == BD_XD2_230) ||
             (gd->arch.board_desc.rev_major == BD_XD2_240) ||
             (gd->arch.board_desc.rev_major == BD_XD3_230) ||
             (gd->arch.board_desc.rev_major == BD_XD4_130) ||
             (gd->arch.board_desc.rev_major == BD_XD4_240) ||
             (gd->arch.board_desc.rev_major == BD_XA4_240) ||
             (gd->arch.board_desc.rev_major == BD_XR500)   ||
             (gd->arch.board_desc.rev_major == BD_XR600)   ||
             (gd->arch.board_desc.rev_major == BD_XR700))
            && !gpio_get_value(2));
}

void check_reset_button (void) {
    char buffer[64];
    char *s = &buffer[0];
    char *tmp;
    int  i;

    if (gd->reset_button_timeout) {
        if (reset_button_active()) {
            if (gd->flags & GD_FLG_MAIN) {
                if (gd->reset_button_disable) {
                    gd->reset_button_timeout = 0;
                    led_spin_state = LEDS_NOT_SPINNING;
                    for (i=0; i<7; i++) {  // flash orange leds 3 times -> faster than if we were actually resetting the environment
                        radio_set_leds((i & 0x1) ? 0xff : 0x0);
                        udelay(200000);
                        WATCHDOG_RESET();
                    }
                }
                else {
                    gd->reset_button_timeout = 0;
                    putc ('\n');
                    printf ("Reset Button Pressed ... Resetting Environment to Factory Defaults\n");

                    led_spin_state = LEDS_NOT_SPINNING;
                    for (i=0; i<7; i++) {  // flash orange leds 3 times
                        radio_set_leds((i & 0x1) ? 0xff : 0x0);
                        udelay(1000000);
                        WATCHDOG_RESET();
                    }

                    put_cmd_label("Flash", "Clearing");
                    printf ("Environment");
                    tmp = getenv("bootfile_active");
                    memcpy (s, tmp, sizeof(buffer));
                    env_reset();
                    setenv("bootfile_active", s);
                    putc ('\n');
                    saveenv();
                    putc ('\r');
                    do_reset (NULL,0,0,NULL);
                }
            }
        }
        else
            gd->reset_button_timeout = 0;
    }
}

/************************************************************************
 *  Hardware Watchdog & Reset board specific routines                   *
 ************************************************************************/
#ifdef CONFIG_HW_WATCHDOG
unsigned long hw_watchdog_init(unsigned long period)
{
        unsigned short val;
        uchar ctrl;
	uint len = 0;
	union cvmx_ciu_wdogx ciu_wdog;

        if (period != 0) {
#ifdef CONFIG_WATCHDOG_WORKAROUND
            if (!avr_present() || !(gd->flags & GD_FLG_RELOC)) {
#else
            if (!avr_present()) {
#endif
                gd->hw_watchdog_internal = 1;
                if (period > 10)  period = 10;  /* this should prevent len from overflowing given the i/o clock rates we use */
                gd->hw_watchdog_period = period*100;
	        len = (period * octeon_get_ioclk_hz()) >> 16;
	        len >>= 1;  /* divide by 2 since len counts down twice before hitting NMI */
	        len = (len > 65535) ? 65535 : len;  /* just to be safe */
	        ciu_wdog.u64 = 0;
	        ciu_wdog.s.len = len;
	        ciu_wdog.s.mode = 3;  /* Interrupt + NMI + soft-reset */
	        cvmx_write_csr(CVMX_CIU_WDOGX(0),ciu_wdog.u64);
#ifdef CONFIG_WATCHDOG_WORKAROUND
                if (avr_present()) {
                    val = byte_swap16(30*100);  /* set to 30 seconds */
                    scd_write(SCD_WD_TMR_CNT, (unsigned char *)&val, 2);
                }
#endif
	    }
	    else {
                gd->hw_watchdog_internal = 0;
                val = byte_swap16(period*100);
                if (scd_write(SCD_WD_TMR_CNT, (unsigned char *)&val, 2) == 0) {
                    gd->hw_watchdog_period = period*100;
                    gd->hw_wd_refresh_msec = period*200;
                    gd->hw_wd_next_refresh = get_timer(0) + gd->hw_wd_refresh_msec;
                    gd->hw_wd_cnt = 0;

                    scd_read (SCD_CTRL_LO, &ctrl, 1);       /* enable watchdog in SCD */
                    ctrl &= ~CRL_MWD_DIS;
                    scd_write(SCD_CTRL_LO, &ctrl, 1);
                }
                else
                    gd->hw_watchdog_period = 0;
            }
	}
        return (gd->hw_watchdog_period/100);
}

void hw_watchdog_off(void)
{
        uchar ctrl;
	union cvmx_ciu_wdogx ciu_wdog;

        if (gd->hw_watchdog_internal) {
	    ciu_wdog.u64 = cvmx_read_csr(CVMX_CIU_WDOGX(0));
	    ciu_wdog.s.mode = 0;
	    cvmx_write_csr(CVMX_CIU_WDOGX(0),ciu_wdog.u64);
	}
	else {
            scd_read (SCD_CTRL_LO, &ctrl, 1);       /* disable watchdog in SCD */
            ctrl |= CRL_MWD_DIS;
            scd_write(SCD_CTRL_LO, &ctrl, 1);
	}
}

void hw_watchdog_reset(void)
{
        ulong now;
        unsigned short val;
        union cvmx_ciu_pp_pokex;

        if (!gd->hw_watchdog_period)
            return;

        timer_hook(0);

        if (gd->hw_watchdog_internal) {
	    cvmx_write_csr(CVMX_CIU_PP_POKEX(0), 1);
	    check_reset_button();
	    if ((gd->arch.board_desc.rev_major == BD_XD4_130) || (gd->arch.board_desc.rev_major == BD_XD4_240))
	        fan_ctrl_check();
	}
	else {
            fan_ctrl_check();
            if (gd->hw_watchdog_period && (now = get_timer(0)) > gd->hw_wd_next_refresh) {
                gd->hw_wd_next_refresh = now + gd->hw_wd_refresh_msec;
                gd->hw_wd_cnt++;
                val = byte_swap16(gd->hw_watchdog_period);
                scd_write(SCD_WD_TMR_CNT, (unsigned char *)&val, 2);
            }
	}
}

uint watchdog_enabled(void)
{
        uchar ctrl;
        uint  retval;
	union cvmx_ciu_wdogx ciu_wdog;

        if (!avr_present()) {
	    ciu_wdog.u64 = cvmx_read_csr(CVMX_CIU_WDOGX(0));
	    retval = (ciu_wdog.s.mode != 0);
	}
	else {
            scd_read (SCD_CTRL_LO, &ctrl, 1);
            retval = ~ctrl & CRL_MWD_DIS;  /* = 0 if disable bit is set */
	}
        return (retval);
}

void hw_watchdog_start(int msecs)
{
}

void hw_watchdog_disable(void)
{
}
#endif  /* CONFIG_HW_WATCHDOG */

#ifdef CONFIG_WATCHDOG
void watchdog_reset(void)
{
}
#endif

#ifdef CONFIG_HW_RESET
void hw_reset(void)
{
        scd_reg_write(SCD_CTRL_HI, 0xff);       /* flush fpgas, force write protects */
        scd_reg_write(SCD_CTRL_LO, CRL_RESET);  /* assert reset                      */
}
#endif

/************************************************************************
 *  Write Protect board specific routines                               *
 ************************************************************************/
#ifdef CONFIG_EEPROM_HW_WR_PROTECT
void hw_eeprom_protect(int protect)  // eeprom write protect input is high to protect
{
        if (protect)
                gpio_set_value(12, 1);
        else
                gpio_set_value(12, 0);
}
#endif


#ifdef CONFIG_FLASH_HW_WR_PROTECT
void hw_flash_protect(int protect)  // flash write protect input is low to protect
{
        if (protect)
                gpio_set_value(11, 0);
        else
                gpio_set_value(11, 1);
}
#endif

/************************************************************************
 *  CPU temperature correction routines                                 *
 ************************************************************************/
#ifdef CONFIG_TEMP_CORRECTION
struct temp_adjust_info {
    scd_board_id_t board;
    int temp_offset;
} temp_adj_tbl[] = {
    {{114, 1, '0'}, -15}, {{114, 1, '6'}, -15}, {{114, 1, '7'}, -15},                       // early XR-4000
    {{117, 1, '0'}, -15}, {{117, 1, '1'}, -15}, {{117, 1, '2'}, -15},                       // early XR-6000
    {{143, 1, '0'}, -15}, {{143, 1, '1'}, -15}, {{143, 1, 'A'}, -15}, {{143, 1, 'B'}, -15}, // early XR-1000
    {{144, 1, '0'}, -15}, {{144, 1, '1'}, -15}, {{144, 1, 'A'}, -15}                        // early XR-2000
};

//----------------------------------------------------------------
// lookup and set temperature reading offset
//----------------------------------------------------------------
void lookup_temp_correction(void)
{
        scd_board_id_t board;
        int i;

        scd_read(SCD_BOARD_ID, (unsigned char *)&board, 4);
        board.type     = byte_swap16(board.type);
        board.revision = SCD_BOARD_MAJ_REV(board.revision);

        //printf("\nboard: type=%d, version=%d, revision=%c ... ", board.type, board.version, board.revision);
        for (i = 0; i < sizeof(temp_adj_tbl)/sizeof(temp_adj_tbl[0]); ++i) {
                if (memcmp(&board, &temp_adj_tbl[i].board, sizeof(board)) == 0) {
                        gd->temp_correction = temp_adj_tbl[i].temp_offset;
                        break;
                }
        }
        // if we didn't find an exact match, search backwards for match w/o rev letter
        if (i == sizeof(temp_adj_tbl)/sizeof(temp_adj_tbl[0])) {
                for (--i; i >= 0; --i) {
                        if (memcmp(&board, &temp_adj_tbl[i].board, sizeof(board) - 1) == 0) {
                                gd->temp_correction = temp_adj_tbl[i].temp_offset;
                                break;
                        }
                }
        }
        //printf("correction=%d\n", gd->temp_correction);
}
#endif

/************************************************************************
 *  Hardware Fan Control specific routines                              *
 ************************************************************************/
#ifdef CONFIG_FAN_CONTROL

// new variable speed fan control logic
#define MAX_SPEED               100
#define MIN_SPEED               20
#define INTERCEPT               25      // this equates to a 20% speed increase every 8C, from 20% @ 33C, going to 100% @ 65C
#define SLOPE                   5 / 2   // (t <= 33C -> 20%; t = 41C -> 40%; t = 49C -> 60%; t = 57C -> 80%; t >= 65C -> 100%)
#define TEMP_TO_FANSPEED(t)     max(min(((t) - INTERCEPT) * SLOPE, MAX_SPEED), MIN_SPEED)

// old school 4-speed fan control set points
#define CRH_FAN_MASK    (  CRH_FANSPEED_1  |   CRH_FANSPEED_0 )
#define CRH_FAN_SET_0   ((!CRH_FANSPEED_1) | (!CRH_FANSPEED_0))
#define CRH_FAN_SET_1   ((!CRH_FANSPEED_1) |   CRH_FANSPEED_0 )
#define CRH_FAN_SET_2   (  CRH_FANSPEED_1  | (!CRH_FANSPEED_0))
#define CRH_FAN_SET_3   (  CRH_FANSPEED_1  |   CRH_FANSPEED_0 )

#define HYSTERESIS       2
#define TEMP0           60
#define TEMP1           65
#define TEMP2           70

void fan_ctrl_check(void)
{
        ulong now;
        uchar this_fan_setting;
        int   fan_setting_next;
        int   xd4_130;
        int   xd4_240;
        int   any_xd4;

        if (gd->fan_refresh_msec && (now = get_timer(0)) > gd->fan_next_refresh) {
	        gd->fan_next_refresh  = now + gd->fan_refresh_msec;

                extern int dtt_get_temp(int sensor);
                int cpu_temp = dtt_get_temp(0);

                xd4_130 = (gd->arch.board_desc.rev_major == BD_XD4_130);
                xd4_240 = (gd->arch.board_desc.rev_major == BD_XD4_240);
                any_xd4 = xd4_130 | xd4_240;
                //printf("\nctrl=%d, temp=%d, setting=%d ...", gd->fan_temp_control, cpu_temp, gd->fan_current_setting);
                if (gd->fan_temp_control > 3 || any_xd4) {
                        // for scd 4+ use new fan control logic
                        if (xd4_130)
                                fan_setting_next = TEMP_TO_FANSPEED(cpu_temp) + 104;  // shift by 104
                        else if (xd4_240)
                                fan_setting_next = TEMP_TO_FANSPEED(cpu_temp) * 255 / 100;  // scale so value is 255 for fan speed at 100%
                        else
                                fan_setting_next = TEMP_TO_FANSPEED(cpu_temp);
                        if (fan_setting_next != gd->fan_current_setting) {
                                if (any_xd4) {
                                        gd->fan_current_setting = fan_setting_next;
                                        this_fan_setting = (uchar)fan_setting_next;
                                        i2c_write(CONFIG_SYS_DTT_LM63_ADDR, 0x4c, 1, &this_fan_setting, 1);
                                }
                                else
                                        scd_reg_write(SCD_FAN_SETTING, gd->fan_current_setting = fan_setting_next);
                        }
                }
                else {
                        // for scd 3- use older fan control bits
                        int fan_control_next = gd->fan_current_setting; // default case

                        switch (gd->fan_current_setting) {
                            case 0:
                                if      (cpu_temp >  TEMP2           ) fan_control_next = CRH_FAN_SET_3;
                                else if (cpu_temp >  TEMP1           ) fan_control_next = CRH_FAN_SET_2;
                                else if (cpu_temp >  TEMP0           ) fan_control_next = CRH_FAN_SET_1;
                                break;
                            case 1:
                                if      (cpu_temp >  TEMP2           ) fan_control_next = CRH_FAN_SET_3;
                                else if (cpu_temp >  TEMP1           ) fan_control_next = CRH_FAN_SET_2;
                                else if (cpu_temp <= TEMP0-HYSTERESIS) fan_control_next = CRH_FAN_SET_0;
                                break;
                            case 2:
                                if      (cpu_temp >  TEMP2           ) fan_control_next = CRH_FAN_SET_3;
                                else if (cpu_temp <= TEMP0           ) fan_control_next = CRH_FAN_SET_0;
                                else if (cpu_temp <= TEMP1-HYSTERESIS) fan_control_next = CRH_FAN_SET_1;
                                break;
                            case 3:
                                if      (cpu_temp <= TEMP0           ) fan_control_next = CRH_FAN_SET_0;
                                else if (cpu_temp <= TEMP1           ) fan_control_next = CRH_FAN_SET_1;
                                else if (cpu_temp <= TEMP2-HYSTERESIS) fan_control_next = CRH_FAN_SET_2;
                                break;
                        }
                        if (fan_control_next != gd->fan_current_setting)
                                scd_reg_write(SCD_CTRL_HI, gd->fan_current_setting = (scd_reg_read(SCD_CTRL_HI) & ~CRH_FAN_MASK) | fan_control_next);
                }
                //printf(" new setting=%d\n", gd->fan_current_setting);
        }
}

void fan_ctrl_init(int period)
{
        gd->fan_temp_control    = scd_reg_read(SCD_MAJ_REV);
        gd->fan_refresh_msec    = period*1000;
        gd->fan_next_refresh    = 0;
        gd->fan_current_setting = 0;

        fan_ctrl_check();
}
#endif  /* CONFIG_FAN_CONTROL */

/* The mdio bus does not normally get initialized until eth_initialize() is called.
 * Use these routines to have early access to the mdio bus.
 */

void xir_miiphy_init (void) {
    cvmx_write_csr(CVMX_SMIX_EN(0), 1);
    cvmx_write_csr(CVMX_SMIX_EN(1), 1);
}

int xir_miiphy_read (unsigned char addr, unsigned char reg, unsigned short * value)
{
    int bus_id = ((gd->arch.board_desc.rev_major == BD_XD4_130) ||
                  (gd->arch.board_desc.rev_major == BD_XA4_240) ||
                  (gd->arch.board_desc.rev_major == BD_XD4_240)) ? 0 : !!(addr & 0x80);
    int retval = cvmx_mdio_read(bus_id, addr, reg);
    if (retval < 0)
        return -1;
    *value = (unsigned short)retval;
    return 0;
}

int xir_miiphy_write (unsigned char addr, unsigned char reg, unsigned short value)
{
    int bus_id = ((gd->arch.board_desc.rev_major == BD_XD4_130) ||
                  (gd->arch.board_desc.rev_major == BD_XA4_240) ||
                  (gd->arch.board_desc.rev_major == BD_XD4_240)) ? 0 : !!(addr & 0x80);
    if (cvmx_mdio_write(bus_id, addr, reg, value) < 0)
        return(-1);
    else
        return(0);
}

int xir_miiphy_read45(unsigned char addr, unsigned char dev, unsigned short reg, unsigned short * value)
{
    int retval = cvmx_mdio_45_read(0, addr, dev, reg);
    if (retval < 0)
    {
        *value = 0xBADD;
        return -1;
    }
    *value = (unsigned short)retval;
    return 0;
}
int xir_miiphy_write45(unsigned char addr, unsigned char dev, unsigned short reg, unsigned short value)
{
    if (cvmx_mdio_45_write(0, addr, dev, reg, value) < 0)
        return -1;
    else
        return 0;
}

/************************************************************************
 *  FPGA board specific routines                                        *
 ************************************************************************/
#ifdef CONFIG_FPGA_FLASH_LOAD

#define CONFIG_MAX_FPGA_DEVICES   1
#define FPGA_INIT_TIMEOUT         100000  /* usec (100 ms) */
#define FPGA_PROG_TIMEOUT         500000  /* usec (500 ms) */
#define SPARTAN6_SIGNATURE_SWAP   0x5599aa66

struct fpga_config {
        char  *base;
        char  *image;
        char  *temp;
        ulong *crc;
        ulong *size;
};

struct fpga_config fpga_config_table[CONFIG_MAX_FPGA_DEVICES+1] = {

        { (char *)CONFIG_AVALON_BASE, (char *)(CONFIG_AVALON_IMAGE + 8), (char *)CONFIG_SYS_LOAD_ADDR, (ulong *)(CONFIG_AVALON_IMAGE + 4), (ulong *)CONFIG_AVALON_IMAGE },
        { NULL,                       NULL,                              NULL,                          NULL,                               NULL                         }
};

int set_fpga_prog(int prog)
{
        gpio_set_value(8,prog);
        return (gpio_get_value(8));
}

int get_fpga_init(void)
{
        return(gpio_get_value(6));
}

int get_fpga_done(void)
{
        return(gpio_get_value(7));
}

int reset_fpga(void)
{
        unsigned long long fpga_timeout = set_timeout(FPGA_INIT_TIMEOUT);
        int ret_value;

        set_fpga_prog(0);                                               /* assert program */
        udelay(100);                                                    /* 500ns min program_b pulse width for spartan6 */

        while (get_fpga_init() && !has_expired(fpga_timeout))           /* wait for init to be asserted */
                ;
        ret_value = ~get_fpga_init();

        set_fpga_prog(1);                                               /* deassert program */
        udelay(10000);                                                  /* 4ms max from program_b high to init_b high for spartan 6 */
        return (ret_value);
}

int fpga_load_images(int boot, char *image_addr, ulong image_size)
{
        unsigned long long fpga_timeout;
        struct fpga_config *fcp, *fcp_table;
        ulong signature6 = SPARTAN6_SIGNATURE_SWAP;
        char *image;
        int num_prog;
        char *temp_ptr;
        char *s;

        if (!boot) put_bootmsg_label("FPGA");                             /* called during power-up, load from flash */
        else       put_cmd_label("Boot", "FPGA");                         /* called during boot, load from multi-image file */

        for (fcp = fcp_table = fpga_config_table; fcp->base; ++fcp) {
                if (*(ulong *)(fcp->image + SPARTAN6_SIGNATURE_OFFSET ) == signature6)
                        break;
                //printf("%8x  %8x  %8x  %8x  %8x\n",fcp,fcp->base,fcp->image,(fcp->image+SPARTAN6_SIGNATURE_OFFSET),(*(ulong *)(fcp->image + SPARTAN6_SIGNATURE_OFFSET )));
        }

        if (!fcp->base) {
                puts("No Images\n");
                return (1);
        }

        if (!reset_fpga()) {
                puts("No Devices\n");
                return (1);
        }

        fpga_timeout = set_timeout(FPGA_INIT_TIMEOUT);
        while (!get_fpga_init() && !has_expired(fpga_timeout))           /* wait for init to be deasserted */
                ;
        if (!get_fpga_init()) {
                puts("### Devices failed initialization ###\n");
                return (-1);
        }

        puts("Programming Device:  ");

        for (fcp = fcp_table, num_prog = 0; fcp->base; ++fcp) {

                if (*(ulong *)(fcp->image + SPARTAN6_SIGNATURE_OFFSET) == signature6) { /* program device */
                        printf("\b%d", ++num_prog);

                        WATCHDOG_RESET();

                        if ((s = getenv("loadaddr")) != NULL) {
                                temp_ptr = (char *) simple_strtoul (s, NULL, 16);
                        }
                        else
                                temp_ptr = fcp->temp;

                        if (temp_ptr) {
                                memcpy (temp_ptr, fcp->image, *fcp->size);     /* make temp copy from rom */
                                image = temp_ptr;
                        }
                        else
                                image = fcp->image;

                        WATCHDOG_RESET();

                        if (fcp->crc && *(fcp->crc) != crc32(0, (uchar *)image, *fcp->size)) {
                                puts("  ### Invalid CRC ###\n");
                                return (-1);
                        }

                        WATCHDOG_RESET();

                        memcpy(fcp->base, image, *fcp->size);

                        if (!get_fpga_init()) {                                  /* fail if init asserted */
                                puts("  ### Failed ###\n");                      /* note:  this pin has a pulldown since it's unused in functional mode, so check it quick */
                                return (-1);
                        }

                        WATCHDOG_RESET();

                        udelay(1000);
                        if (get_fpga_done()) {                                  /* check done line */
                                //printf("Done is High!!\n");
                                break;
                        }
                        else {
                                printf("Done is Low!!\n");
                        }

                }
        }

        puts("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");

        if (!get_fpga_done()) {                                                 /* check done line */
                puts("### Programming failed to complete ###\n");
                return (-1);
        }

        printf(num_prog==1 ? "%*d Device programmed  \n" : "%*d Devices programmed  \n", !boot ? 3 : 1, num_prog);

        return (0);
}

ulong fpga_config_sizes[CONFIG_MAX_FPGA_DEVICES];

int fpga_reset_images(int image_num, char *image_addr, ulong image_size)
{
        struct fpga_config *fcp;

        for (fcp = fpga_config_table; fcp->base; ++fcp) {
                fcp->image = NULL;
                fcp->temp  = NULL;
                fcp->crc   = NULL;
                fcp->size  = NULL;
        }
        return(0);
}

int fpga_set_image(int image_num, char *image_addr, ulong image_size)
{
        image_num -= 2;                                         /* generate index into fpga table */
        fpga_config_table[image_num].image =  image_addr;       /* (kernel & initrd are 0 & 1)    */
        fpga_config_sizes[image_num]       =  image_size;
        fpga_config_table[image_num].size  = &fpga_config_sizes[image_num];

#ifdef DEBUG
        printf("   FPGA set image: address=%08x, size=%08x\n", image_addr, image_size);
#endif

        bitswap ((ulong)image_addr, 1, image_size);

        return(0);
}
#endif /* CONFIG_FPGA_XIRRUS */

int do_button (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        if ((argc > 1)) {
                printf ("Usage:\n%s\n", cmdtp->usage);
                return 1;
        }

        if (gd->arch.board_desc.rev_major != BD_XD2_230 &&
            gd->arch.board_desc.rev_major != BD_XD2_240 &&
            gd->arch.board_desc.rev_major != BD_XD3_230 &&
            gd->arch.board_desc.rev_major != BD_XD4_130 &&
            gd->arch.board_desc.rev_major != BD_XD4_240 &&
            gd->arch.board_desc.rev_major != BD_XA4_240 &&
            gd->arch.board_desc.rev_major != BD_XR500   &&
            gd->arch.board_desc.rev_major != BD_XR600   &&
            gd->arch.board_desc.rev_major != BD_XR700)   {
                printf ("This test is not valid on this platform.\n");
                return 1;
        }

        printf ("Press any key to exit ...\n\n");

        while (!tstc()) {
                printf (" Reset Button State: %s\r", reset_button_active() ? "PRESSED    " : "NOT PRESSED");
                udelay (100000);
        }

        printf ("\n");
        return(0);
}

U_BOOT_CMD(
        button,   1,     0,      do_button,
        "current reset button state",
        "\n"
);
