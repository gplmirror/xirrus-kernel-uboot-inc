/*
 * (C) Copyright 2003
 * Wolfgang Denk, DENX Software Engineering, <wd@denx.de>
 * (C) Copyright 2010 - 2013
 * Cavium Inc. Inc. <support@cavium.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <asm/mipsregs.h>

#include <asm/arch/octeon-boot-info.h>
#include <asm/arch/octeon_boot.h>

#ifdef CONFIG_OCTEON_XARRAY
#include <command.h>
#include <otis.h>

extern int suppress_usb_stop_messages;

extern void hw_reset(void);
extern void set_reset_status (void);
extern int  usb_stop (void);
extern int  scd_write (unsigned long offset, uchar *buf, unsigned long len);
#endif

#ifdef CONFIG_OCTEON_MGMT_ENET
int octeon_mgmt_eth_initialize(bd_t *);
#endif

#if defined(CONFIG_OCTEON_RGMII_ENET) || defined(CONFIG_OCTEON_XAUI_ENET) || \
	defined(CONFIG_OCTEON_SGMII_ENET)
int octeon_eth_initialize(bd_t *);
#endif
#ifdef CONFIG_OCTEON_SPI4000_ENET
int octeon_spi4000_initialize(bd_t *);
#endif
#ifdef CONFIG_OCTEON_SPI_IXF18201_ENET
int octeon_spi_ixf18201_initialize(bd_t *);
#endif

#ifdef CONFIG_E1000
int e1000_initialize(bd_t *);
#endif
#ifdef CONFIG_CMD_IDE
extern void ide_set_reset(int reset);
#endif
#if !defined(CONFIG_OCTEON_DISABLE_NETWORKING) && !defined(OCTEON_NO_NETWORK) \
    && (defined(CONFIG_MII) || defined(CONFIG_CMD_MII)) \
    && defined(CONFIG_OCTEON_MDIO)
extern int octeon_mdio_initialize(void);
#endif
#ifdef CONFIG_HW_WATCHDOG
extern void hw_watchdog_disable(void);
#endif

#if defined(__U_BOOT__)
int do_reset(cmd_tbl_t * cmdtp, int flag, int argc, char * const argv[])
{
#ifdef CONFIG_OCTEON_XARRAY
	/* shut down usb interface */
        suppress_usb_stop_messages = 1;
	usb_stop();
        suppress_usb_stop_messages = 0;

        /* prevent netconsole from checking for input during boot */
        NetProtocol = -1;
        eth_halt();

        /* indicate this is a soft reset and not a watchdog */
        set_reset_status();

        put_cmdmsg_label("RESET");
        putc('\n');

#ifdef CONFIG_HW_RESET
        hw_reset();
        udelay(100000);         /* wait 100ms for the reset to happen   */
#else
        uchar scd_buf;
        scd_buf = SRL_EXT_RESET;
        /* indicate to u-boot we're not wdog'd */
        scd_write(SCD_STAT_LO, &scd_buf, 1);
#endif
#endif  /* CONFIG_OCTEON_XARRAY */

#ifdef CONFIG_CMD_IDE
	mdelay (10);
	ide_set_reset(1);
	mdelay(20);
	ide_set_reset(0);
	mdelay(50);
#endif
	/* Flush the cache */
	CVMX_SYNC;
	CVMX_ICACHE_INVALIDATE;
	CVMX_DCACHE_INVALIDATE;
#ifdef CONFIG_HW_WATCHDOG
	hw_watchdog_disable();
#endif
	if (OCTEON_IS_OCTEON3()) {
		cvmx_write_csr(CVMX_RST_SOFT_RST, 1ull);
	} else {
		cvmx_write_csr(CVMX_CIU_SOFT_RST, 1ull);
	}
	fprintf (stderr, "*** reset failed ***\n");
	return 0;
}

void flush_cache (unsigned long start_addr, unsigned long size)
{
	CVMX_SYNC;
	CVMX_ICACHE_INVALIDATE;
	CVMX_DCACHE_INVALIDATE;
}
#endif

void write_one_tlb (int index, u32 pagemask, u32 hi, u32 low0, u32 low1)
{
	write_c0_entrylo0 (low0);
	write_c0_pagemask (pagemask);
	write_c0_entrylo1 (low1);
	write_c0_entryhi (hi);
	write_c0_index (index);
	tlb_write_indexed ();
}

void write_one_tlb64 (u32 index, u32 pagemask, u64 hi, u64 low0, u64 low1)
{
	write_64bit_c0_entrylo0 (low0);
	write_c0_pagemask (pagemask);
	write_64bit_c0_entrylo1 (low1);
	write_64bit_c0_entryhi (hi);
	write_c0_index (index);
	tlb_write_indexed ();
}

int get_num_tlb_entries (void)
{
	int val;
	int num_entries = (read_c0_config1 () >> 25) & 0x3f;

	val = read_c0_config3 ();
	if (val & (1 << 31)) {
		val = read_c0_config4 () & 0xff;
		num_entries |= val << 6;
	}
	return num_entries + 1;
}

void flush_dcache_range(ulong start_addr, ulong stop)
{
	CVMX_SYNCW;
	/* Don't need to do anything for OCTEON */
}

void invalidate_dcache_range(ulong start_addr, ulong stop)
{
	/* Don't need to do anything for OCTEON */
}

#ifdef CONFIG_CMD_NET
int cpu_eth_init(bd_t *bis)
{
	if (getenv("disable_networking"))
		return 0;
#if !defined(CONFIG_OCTEON_DISABLE_NETWORKING) && !defined(OCTEON_NO_NETWORK) \
    && (defined(CONFIG_MII) || defined(CONFIG_CMD_MII)) \
    && defined(CONFIG_OCTEON_MDIO)
	debug("Initializing MDIO bus\n");
	octeon_mdio_initialize();
#endif

#ifdef CONFIG_OCTEON_MGMT_ENET
	if (octeon_has_feature(OCTEON_FEATURE_MGMT_PORT))
		octeon_mgmt_eth_initialize(bis);
#endif
#if defined(CONFIG_OCTEON_RGMII_ENET) || defined(CONFIG_OCTEON_XAUI_ENET) \
		|| defined(CONFIG_OCTEON_SGMII_ENET)
	octeon_eth_initialize(bis);
#endif
#ifdef CONFIG_OCTEON_SPI4000_ENET
	if (!getenv("disable_spi"))
		octeon_spi4000_initialize(bis);
#endif
#ifdef CONFIG_OCTEON_SPI_IXF18201_ENET
	if ((!getenv("disable_spi_ixf18201") && !getenv("disable_spi"))
	    || getenv("enable_spi_ixf18201"))
		octeon_spi_ixf18201_initialize(bis);
#endif
#ifdef CONFIG_E1000
	e1000_initialize(bis);
#endif
	return 0;
}
#endif /* CONFIG_CMD_NET */


#ifdef CONFIG_OCTEON_XARRAY
/************************************************************************
 * Watchdog Utilities                                                   *
 ************************************************************************/

#if defined(CONFIG_HW_WATCHDOG)
extern unsigned long hw_watchdog_init(unsigned long period);
extern void hw_watchdog_off(void);

int do_watchdog (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        if (argc > 1) {
                if (argv[1][0] == 'o') {
                        if (argv[1][1] == 'n') {
                                hw_watchdog_init(argc == 3 ? simple_strtoul (argv[2], NULL, 0) : hw_watchdog_init(0));
                                put_cmd_label("System", "Watchdog");
                                printf("Enabled, period: %d secs\n", (int)hw_watchdog_init(0));
                                return (0);
                        }
                        else {
                                hw_watchdog_off();
                                put_cmd_label("System", "Watchdog");
                                printf("Disabled\n");
                                return (0);
                        }
                }
                else if (argv[1][0] == 't') {  /* test the watchdog */
                        while (1) {
                        }
                }
                else {
                        hw_watchdog_init(simple_strtoul (argv[1], NULL, 0));
                }
        }
        put_cmd_label("System", "Watchdog");
        printf("Period = %d secs\n", (int)hw_watchdog_init(0));
        return (0);
}

U_BOOT_CMD(
        watchdog, 2, 0, do_watchdog,
        "enable, disable, set or report the watchdog timer period",
        "[ on | off ] [ period ]\n"
        "    - 'on' or 'off' enables or disables watchdog timer\n"
        "    - 'period' sets watchdog timer period in seconds\n"
        "    -  no argument reports current watchdog period"
);

#endif  /* CONFIG_HW_WATCHDOG */
#endif  /* CONFIG_OCTEON_XARRAY */
