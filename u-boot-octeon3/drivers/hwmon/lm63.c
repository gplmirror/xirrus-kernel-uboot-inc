/*
 * (C) Copyright 2007-2008
 * Dirk Eibach,  Guntermann & Drunck GmbH, eibach@gdsys.de
 * based on lm75.c by Bill Hunter
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

/*
 * National LM63/LM64 Temperature Sensor
 * Main difference: LM 64 has -16 Kelvin temperature offset
 */

#include <common.h>
#include <i2c.h>
#include <dtt.h>

#ifdef CONFIG_OCTEON_XARRAY
#ifdef CONFIG_MULTI_DTT
#define dtt_call(name)  LM63_##name
#else
#define dtt_call(name)  dtt_##name
#endif
#endif

#if defined(CONFIG_OCTEON_XARRAY) && defined(CONFIG_SYS_DTT_LM63_ADDR)
#define DTT_I2C_LM63_ADDR	CONFIG_SYS_DTT_LM63_ADDR
#else
#define DTT_I2C_LM63_ADDR	0x4C	/* National LM63 device */
#endif

#ifdef CONFIG_OCTEON_XARRAY
#define DTT_READ_TEMP_LOC_MSB	0x00
#define DTT_TRUTHERM_EN         0x30
#define DTT_ENHANCED_CONFIG     0x45
#define DTT_PWM_VALUE           0x4C
#endif
#define DTT_READ_TEMP_RMT_MSB	0x01
#define DTT_CONFIG		0x03
#define DTT_READ_TEMP_RMT_LSB	0x10
#define DTT_TACHLIM_LSB		0x48
#define DTT_TACHLIM_MSB		0x49
#define DTT_FAN_CONFIG		0x4A
#define DTT_PWM_FREQ		0x4D
#define DTT_PWM_LOOKUP_BASE	0x50

struct pwm_lookup_entry {
	u8 temp;
	u8 pwm;
};

#ifdef CONFIG_OCTEON_XARRAY
#define CONFIG_DTT_TACH_LIMIT       0xffff
#define CONFIG_DTT_PWM_LOOKUPTABLE  {{0,0xf},{20,0xf},{40,0xf}}
#endif

/*
 * Device code
 */

#ifdef CONFIG_OCTEON_XARRAY
static
#endif
int dtt_read(int sensor, int reg)
{
	int dlen;
	uchar data[2];

	/*
	 * Calculate sensor address and register.
	 */
	if (!sensor)
		sensor = DTT_I2C_LM63_ADDR;	/* legacy config */

	dlen = 1;

	/*
	 * Now try to read the register.
	 */
	if (i2c_read(sensor, reg, 1, data, dlen) != 0)
		return -1;

	return (int)data[0];
}				/* dtt_read() */

#ifdef CONFIG_OCTEON_XARRAY
static
#endif
int dtt_write(int sensor, int reg, int val)
{
	int dlen;
	uchar data[2];

	/*
	 * Calculate sensor address and register.
	 */
	if (!sensor)
		sensor = DTT_I2C_LM63_ADDR;	/* legacy config */

	dlen = 1;
	data[0] = (char)(val & 0xff);

	/*
	 * Write value to register.
	 */
	if (i2c_write(sensor, reg, 1, data, dlen) != 0)
		return 1;

	return 0;
}				/* dtt_write() */

static int is_lm64(int sensor)
{
	return sensor && (sensor != DTT_I2C_LM63_ADDR);
}

#ifdef CONFIG_OCTEON_XARRAY
int dtt_call(init_one) (int sensor)
#else
int dtt_init_one(int sensor)
#endif
{
	int i;
	int val;
        int temp;

	struct pwm_lookup_entry pwm_lookup[] = CONFIG_DTT_PWM_LOOKUPTABLE;

#ifdef CONFIG_OCTEON_XARRAY
	/* Enable Tach input */
	val = 0x04;
	if (dtt_write(sensor, DTT_CONFIG, val) != 0)
		return 1;
        /* Enable diode TruTemp sensing for CPU */
	val = 0x02;
	if (dtt_write(sensor, DTT_TRUTHERM_EN, val) != 0)
		return 1;
        /* Enable PWM programming, best Tach accuracy */
	val = 0x22;
	if (dtt_write(sensor, DTT_FAN_CONFIG, val) != 0)
		return 1;
        /* Set PWM clock frequency to 22KHz */
	val = 0x08;
	if (dtt_write(sensor, DTT_PWM_FREQ, val) != 0)
		return 1;
        /* Enable high-freq PWM with PWM smoothing at 0.91s per step */
	val = 0x15;
	if (dtt_write(sensor, DTT_ENHANCED_CONFIG, val) != 0)
		return 1;
        /* Set initial fan PWM% to 90/ff (~57%) */
	val = 0x90;
	if (dtt_write(sensor, DTT_PWM_VALUE, val) != 0)
		return 1;

        i = 0;
        temp = 0;
        while (temp == 0 && i<200) {
                udelay(10000);
                i++;
	        temp = dtt_get_temp(0);
        }
        put_bootmsg_label ("DTT");
        printf("Ambient: %iC, ", temp);

        i = 0;
        temp = 0;
        while (temp == 0 && i<200) {
                udelay(10000);
                i++;
	        temp = dtt_get_temp(1);
        }
        printf("Junction: %iC\n", temp);
#else
	/*
	 * Set PWM Frequency to 2.5% resolution
	 */
	val = 20;
	if (dtt_write(sensor, DTT_PWM_FREQ, val) != 0)
		return 1;

	/*
	 * Set Tachometer Limit
	 */
	val = CONFIG_DTT_TACH_LIMIT;
	if (dtt_write(sensor, DTT_TACHLIM_LSB, val & 0xff) != 0)
		return 1;
	if (dtt_write(sensor, DTT_TACHLIM_MSB, (val >> 8) & 0xff) != 0)
		return 1;

	/*
	 * Make sure PWM Lookup-Table is writeable
	 */
	if (dtt_write(sensor, DTT_FAN_CONFIG, 0x20) != 0)
		return 1;

	/*
	 * Setup PWM Lookup-Table
	 */
	for (i = 0; i < ARRAY_SIZE(pwm_lookup); i++) {
		int address = DTT_PWM_LOOKUP_BASE + 2 * i;
		val = pwm_lookup[i].temp;
		if (is_lm64(sensor))
			val -= 16;
		if (dtt_write(sensor, address, val) != 0)
			return 1;
		val = dtt_read(sensor, address);
		val = pwm_lookup[i].pwm;
		if (dtt_write(sensor, address + 1, val) != 0)
			return 1;
	}

	/*
	 * Enable PWM Lookup-Table, PWM Clock 360 kHz, Tachometer Mode 2
	 */
	val = 0x02;
	if (dtt_write(sensor, DTT_FAN_CONFIG, val) != 0)
		return 1;

	/*
	 * Enable Tach input
	 */
	val = dtt_read(sensor, DTT_CONFIG) | 0x04;
	if (dtt_write(sensor, DTT_CONFIG, val) != 0)
		return 1;
#endif

	return 0;
}

#ifdef CONFIG_OCTEON_XARRAY
int dtt_call(get_temp) (int sensor)
#else
int dtt_get_temp(int sensor)
#endif
{
#ifdef CONFIG_OCTEON_XARRAY
        s16 temp;

        if (sensor)
                temp = (dtt_read(0, DTT_READ_TEMP_RMT_MSB) << 8)
                     | (dtt_read(0, DTT_READ_TEMP_RMT_LSB));
        else
                temp = (dtt_read(0, DTT_READ_TEMP_LOC_MSB) << 8);
#else
	s16 temp = (dtt_read(sensor, DTT_READ_TEMP_RMT_MSB) << 8)
	    | (dtt_read(sensor, DTT_READ_TEMP_RMT_LSB));

	if (is_lm64(sensor))
		temp += 16 << 8;
#endif

	/* Ignore LSB for now, U-Boot only prints natural numbers */
	return temp >> 8;
}
