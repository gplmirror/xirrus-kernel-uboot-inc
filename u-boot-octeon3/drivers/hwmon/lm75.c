/*
 * (C) Copyright 2001
 * Bill Hunter,  Wave 7 Optics, williamhunter@mediaone.net
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

/*
 * On Semiconductor's LM75 Temperature Sensor
 */

#include <common.h>
#include <i2c.h>
#include <dtt.h>

#ifdef CONFIG_OCTEON_XARRAY
#ifdef CONFIG_MULTI_DTT
#define dtt_call(name)  LM75_##name
#else
#define dtt_call(name)  dtt_##name
#endif
#endif

/*
 * Device code
 */
#ifdef CONFIG_OCTEON_XARRAY
#if defined(CONFIG_SYS_DTT_LM75_ADDR)
#define DTT_I2C_DEV_CODE CONFIG_SYS_DTT_LM75_ADDR
#else
#define DTT_I2C_DEV_CODE 0x48			/* ON Semi's LM75 device */
#endif
#else //!CONFIG_OCTEON_XARRAY
#if defined(CONFIG_SYS_I2C_DTT_ADDR)
#define DTT_I2C_DEV_CODE CONFIG_SYS_I2C_DTT_ADDR
#else
#define DTT_I2C_DEV_CODE 0x48			/* ON Semi's LM75 device */
#endif
#endif //CONFIG_OCTEON_XARRAY

#define DTT_READ_TEMP		0x0
#define DTT_CONFIG		0x1
#define DTT_TEMP_HYST		0x2
#define DTT_TEMP_SET		0x3

#ifdef CONFIG_OCTEON_XARRAY
static
#endif
int dtt_read(int sensor, int reg)
{
	int dlen;
	uchar data[2];

#ifdef CONFIG_DTT_AD7414
	/*
	 * On AD7414 the first value upon bootup is not read correctly.
	 * This is most likely because of the 800ms update time of the
	 * temp register in normal update mode. To get current values
	 * each time we issue the "dtt" command including upon powerup
	 * we switch into one-short mode.
	 *
	 * Issue one-shot mode command
	 */
	dtt_write(sensor, DTT_CONFIG, 0x64);
#endif

	/* Validate 'reg' param */
	if((reg < 0) || (reg > 3))
		return -1;

	/* Calculate sensor address and register. */
	sensor = DTT_I2C_DEV_CODE + (sensor & 0x07);

	/* Prepare to handle 2 byte result. */
	if ((reg == DTT_READ_TEMP) ||
		(reg == DTT_TEMP_HYST) ||
		(reg == DTT_TEMP_SET))
			dlen = 2;
	else
		dlen = 1;

	/* Now try to read the register. */
	if (i2c_read(sensor, reg, 1, data, dlen) != 0)
		return -1;

	/* Handle 2 byte result. */
	if (dlen == 2)
		return ((int)((short)data[1] + (((short)data[0]) << 8)));

	return (int)data[0];
} /* dtt_read() */


#ifdef CONFIG_OCTEON_XARRAY
static
#endif
int dtt_write(int sensor, int reg, int val)
{
	int dlen;
	uchar data[2];

	/* Validate 'reg' param */
	if ((reg < 0) || (reg > 3))
		return 1;

	/* Calculate sensor address and register. */
	sensor = DTT_I2C_DEV_CODE + (sensor & 0x07);

	/* Handle 2 byte values. */
	if ((reg == DTT_READ_TEMP) ||
		(reg == DTT_TEMP_HYST) ||
		(reg == DTT_TEMP_SET)) {
			dlen = 2;
		data[0] = (char)((val >> 8) & 0xff);	/* MSB first */
		data[1] = (char)(val & 0xff);
	} else {
		dlen = 1;
		data[0] = (char)(val & 0xff);
	}

	/* Write value to register. */
	if (i2c_write(sensor, reg, 1, data, dlen) != 0)
		return 1;

	return 0;
} /* dtt_write() */


#ifdef CONFIG_OCTEON_XARRAY
int dtt_call(init_one) (int sensor)
#else
int dtt_init_one(int sensor)
#endif
{
	int val;

	/* Setup TSET ( trip point ) register */
	val = ((CONFIG_SYS_DTT_MAX_TEMP * 2) << 7) & 0xff80; /* trip */
	if (dtt_write(sensor, DTT_TEMP_SET, val) != 0)
#ifdef CONFIG_OCTEON_XARRAY
		return -1;
#else
		return 1;
#endif

	/* Setup THYST ( untrip point ) register - Hysteresis */
	val = (((CONFIG_SYS_DTT_MAX_TEMP - CONFIG_SYS_DTT_HYSTERESIS) * 2) << 7) & 0xff80;
	if (dtt_write(sensor, DTT_TEMP_HYST, val) != 0)
#ifdef CONFIG_OCTEON_XARRAY
		return -1;
#else
		return 1;
#endif

	/* Setup configuraton register */
#ifdef CONFIG_DTT_AD7414
	/* config = alert active low and disabled */
	val = 0x60;
#else
	/* config = 6 sample integration, int mode, active low, and enable */
	val = 0x18;
#endif
	if (dtt_write(sensor, DTT_CONFIG, val) != 0)
#ifdef CONFIG_OCTEON_XARRAY
		return -1;
#else
		return 1;
#endif

#ifdef CONFIG_OCTEON_XARRAY

#ifdef CONFIG_DTT_SENSOR_NAMES
        static char *sensor_names[] = CONFIG_DTT_SENSOR_NAMES;
#endif
        put_bootmsg_label ("DTT");
	printf("%s:", sensor_names[sensor]);
	printf("%2dC", dtt_get_temp(sensor));
        putc('\n');
#endif //CONFIG_OCTEON_XARRAY

	return 0;
} /* dtt_init_one() */

#ifdef CONFIG_OCTEON_XARRAY
int dtt_call(get_temp) (int sensor)
#else
int dtt_get_temp(int sensor)
#endif
{
	int const ret = dtt_read(sensor, DTT_READ_TEMP);

	if (ret < 0) {
#ifdef CONFIG_OCTEON_XARRAY
                return -1;
#else
		printf("DTT temperature read failed.\n");
		return 0;
#endif
	}
	return (int)((int16_t) ret / 256);
} /* dtt_get_temp() */
