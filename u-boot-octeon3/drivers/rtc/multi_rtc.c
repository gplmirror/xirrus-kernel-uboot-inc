/*
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * Multi RTC support.
 *
 * Allows code to be compiled with multiple RTC drivers to support
 * multiple versions of hardware.  THe code uses the first RTC found.
 * To add support for other RTC's, add the rtc_call macro to the original
 * driver file and then add the appropriate sections here.
 */

#include <common.h>
#include <command.h>
#include <rtc.h>
#include <i2c.h>

#if defined(CONFIG_MULTI_RTC) && defined(CONFIG_CMD_DATE)

/*---------------------------------------------------------------------*/
#undef DEBUG_RTC

#ifdef DEBUG_RTC
#define DEBUGR(fmt,args...) printf(fmt ,##args)
#else
#define DEBUGR(fmt,args...)
#endif
/*---------------------------------------------------------------------*/
static uchar test_data;

#define rtc_test(addr, alen)	(i2c_read (addr, 0, alen, &test_data, 1) == 0)


#ifdef CONFIG_RTC_DS1338
   int  DS1338_get(struct rtc_time *tmp);
   int  DS1338_set(struct rtc_time *tmp);
   void DS1338_reset(void);
#define DS1338_test()	rtc_test(CONFIG_SYS_RTC_DS1338_ADDR, 1)
#endif

#ifdef CONFIG_RTC_PCF8563
   int  PCF8563_get(struct rtc_time *tmp);
   int  PCF8563_set(struct rtc_time *tmp);
   void PCF8563_reset(void);
#define PCF8563_test()	rtc_test(CONFIG_SYS_RTC_PCF8563_ADDR, 1)
#endif


/*
 * Get the current time from the RTC
 */
int rtc_get (struct rtc_time *tmp)
{
#ifdef CONFIG_RTC_DS1338
	if (DS1338_test()) return DS1338_get(tmp);
#endif
#ifdef CONFIG_RTC_PCF8563
	if (PCF8563_test())  return PCF8563_get(tmp);
#endif
        return -1;
}


/*
 * Set the RTC
 */
int rtc_set (struct rtc_time *tmp)
{
#ifdef CONFIG_RTC_DS1338
	if (DS1338_test()) return DS1338_set(tmp);
#endif
#ifdef CONFIG_RTC_PCF8563
	if (PCF8563_test())  return PCF8563_set(tmp);
#endif
        return -1;
}


/*
 * Reset the RTC
 */
void rtc_reset (void)
{
#ifdef CONFIG_RTC_DS1338
	if (DS1338_test()) return DS1338_reset();
#endif
#ifdef CONFIG_RTC_PCF8563
	if (PCF8563_test())  return PCF8563_reset();
#endif
}

#endif /* CONFIG_MULTI_RTC && CONFIG_CMD_DATE */
